-- Builds on patch 66's inspectedspace fields

-- REMOTE CURSOR

ALTER TABLE public.occinspectedspace ADD COLUMN floorno INTEGER;

-- move floor numbers from loc descriptors to the occ inspected spaces
-- then drop this col on the loc
ALTER TABLE public.occlocationdescriptor DROP COLUMN buildingfloorno;

ALTER TABLE public.occlocationdescriptor ADD COLUMN muni_municode INTEGER
	CONSTRAINT occlocationdescriptor_municode_fk REFERENCES municipality (municode);


-- undo changes from patch 66
ALTER TABLE public.occinspectedspace DROP COLUMN IF EXISTS lengthft;
ALTER TABLE public.occinspectedspace DROP COLUMN IF EXISTS widthft;
ALTER TABLE public.occinspectedspace DROP COLUMN IF EXISTS ceilingheightft;

ALTER TABLE public.occinspectedspace DROP COLUMN IF EXISTS windowcount;


ALTER TABLE public.occinspectedspace ADD COLUMN maxsleepingpersons INTEGER;
ALTER TABLE public.occinspectedspace ADD COLUMN isfinishedspace BOOLEAN DEFAULT TRUE;

ALTER TABLE public.occchecklistspacetype ADD COLUMN exterior BOOLEAN;

ALTER TABLE public.occinspectedspace ADD COLUMN lengthin2 REAL;
ALTER TABLE public.occinspectedspace ADD COLUMN widthin2 REAL;
ALTER TABLE public.occinspectedspace ADD COLUMN totalsqft2 REAL;

ALTER TABLE	 public.occlocationdescriptor ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;

-- LOCAL CURSOR
-- REMOTE CURSOR (minus the DROP on loc descritpro)
-- We're gunna need umap specific user settings





INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (67, 'database/patches/dbpatch_beta67.sql', '02-AUG-2024', 'ecd', 'expanded location and space fields');