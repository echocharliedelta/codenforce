
-- Adjustments to aid in load time optimization through functions to reduce major swaths of DB Calls



CREATE OR REPLACE FUNCTION public.cnf_getparcelofeventid(
	in_eventid INTEGER)
    RETURNS INTEGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
    DECLARE
        event_record RECORD;
        located_parcelid INTEGER;
    BEGIN
    	-- look up our event
		SELECT * INTO STRICT event_record  FROM event WHERE eventid = in_eventid;

		IF event_record.cecase_caseid IS NOT NULL THEN
			EXECUTE 'SELECT parcel_parcelkey FROM public.cecase WHERE caseid = $1'
				INTO located_parcelid
				USING event_record.cecase_caseid;
		ELSIF event_record.occperiod_periodid IS NOT NULL THEN
			EXECUTE 'SELECT parcel_parcelkey FROM public.occperiod '
					'INNER JOIN public.parcelunit ON (occperiod.parcelunit_unitid = parcelunit.unitid) '
					'WHERE occperiod.periodid = $1'
				INTO located_parcelid
				USING event_record.occperiod_periodid;
		ELSE
			located_parcelid := event_record.parcel_parcelkey;
		END IF;
        RETURN located_parcelid;
    END;
$BODY$;




CREATE OR REPLACE FUNCTION public.cnf_getparcelofinspectionid(
	in_inspectionid INTEGER)
    RETURNS INTEGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
    DECLARE
        fin_record RECORD;
        located_parcelid INTEGER;
    BEGIN
    	-- look up our inspection
		SELECT * INTO STRICT fin_record  FROM public.occinspection WHERE occinspection.inspectionid = in_inspectionid;

		IF fin_record.occperiod_periodid IS NOT NULL THEN
			EXECUTE 'SELECT parcel_parcelkey FROM public.occperiod '
					'INNER JOIN public.parcelunit ON (occperiod.parcelunit_unitid = parcelunit.unitid) '
					'WHERE occperiod.periodid = $1'
				INTO located_parcelid
				USING fin_record.occperiod_periodid;

		ELSE 
			-- GO query CECase to get parcel
			EXECUTE 'SELECT parcel_parcelkey FROM public.cecase WHERE caseid = $1'
				INTO located_parcelid
				USING fin_record.cecase_caseid;
		END IF;
        RETURN located_parcelid;
    END;
$BODY$;




CREATE OR REPLACE FUNCTION public.cnf_buildparceladdressstring(
	in_parcelid INTEGER, in_includecsz BOOLEAN, in_includehtmlbreak BOOLEAN)
    RETURNS TEXT
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
    DECLARE
        address_record RECORD;
        addr_pretty TEXT := '';
    BEGIN
    	EXECUTE 'SELECT addressid, bldgno, mailingstreet.stname, default_city AS city, default_state AS state2char, zip_code AS zip '
				'FROM public.parcelmailingaddress '
				'INNER JOIN public.linkedobjectrole ON (linkedobjectrole_lorid = lorid) '
				'INNER JOIN public.mailingaddress ON (mailingaddress_addressid = addressid) '
				'LEFT OUTER JOIN public.mailingstreet ON (street_streetid = streetid) '
				'LEFT OUTER JOIN public.mailingcitystatezip ON (citystatezip_cszipid = id) '
				'WHERE parcel_parcelkey = $1 '
				'ORDER BY linkedobjectrole.sortorder ASC, parcelmailingaddress.priority ASC '
				'LIMIT 1'
			INTO address_record
			USING in_parcelid;

		-- Now assemble the actual address string
		addr_pretty := address_record.bldgno || ' ' || address_record.stname;
		
		IF in_includecsz THEN
			IF in_includehtmlbreak THEN
				addr_pretty := addr_pretty || '<br />';
			ELSE
				addr_pretty := addr_pretty || ', ';
			END IF;
			addr_pretty :=  addr_pretty || address_record.city || ', ' || address_record.state2char || ' ' || address_record.zip;
		END IF;
		RETURN addr_pretty;
    END;
$BODY$;








INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (59, 'database/patches/dbpatch_beta59.sql', '27-OCT-2023', 'ecd', 'load time work; functions; OCT 2023');


