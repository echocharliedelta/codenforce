-- **************************************************
-- PATCH BITS_002_1516
-- ID_num: 1348: Add an ordinance filter to the inspection page & inspection page cleanup
-- Reference: https://docs.google.com/spreadsheets/d/16Bp3f6j9KgzEtkYpcyW4bTDKUeuD4SQP7oBE7MxTchQ/edit#gid=407744701
-- Description: To add support for jpeg image for blob
-- **************************************************

UPDATE public.blobtype SET fileextensionsarr = '{"jpeg", "jpg", "JPEG", "JPG"}' WHERE typetitle = 'JPEG image'