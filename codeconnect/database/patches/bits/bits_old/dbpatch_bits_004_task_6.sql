-- **************************************************
-- PATCH BITS_004_TASK_6
-- Build OccInspection Determination Manage Page
-- Description: UMAP Tracking for OccInspectionDetermination
-- **************************************************

alter table occinspectiondetermination drop column active; 
ALTER TABLE occinspectiondetermination ADD COLUMN createdts TIMESTAMP WITH TIME ZONE;
ALTER TABLE occinspectiondetermination ADD COLUMN createdby_umapid INTEGER CONSTRAINT occinspectiondetermination_createdumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
ALTER TABLE occinspectiondetermination ADD COLUMN deactivatedby_umapid INTEGER CONSTRAINT occinspectiondetermination_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
ALTER TABLE occinspectiondetermination ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE occinspectiondetermination ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE occinspectiondetermination ADD COLUMN lastupdatedby_umapid INTEGER CONSTRAINT occinspectiondetermination_lastupdatedumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);


-- **************************************************
-- Build OccInspection Cause Manage Page
-- Description: UMAP Tracking for OccInspectionCause
-- **************************************************

alter table occinspectioncause drop column active; 
ALTER TABLE occinspectioncause ADD COLUMN createdts TIMESTAMP WITH TIME ZONE;
ALTER TABLE occinspectioncause ADD COLUMN createdby_umapid INTEGER CONSTRAINT occinspectioncause_createdumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
ALTER TABLE occinspectioncause ADD COLUMN deactivatedby_umapid INTEGER CONSTRAINT occinspectioncause_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
ALTER TABLE occinspectioncause ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE occinspectioncause ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE occinspectioncause ADD COLUMN lastupdatedby_umapid INTEGER CONSTRAINT occinspectioncause_lastupdatedumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);


-- **************************************************
-- Build PropertyUseType Manage Page
-- Description: UMAP Tracking for OccInspectionCause
-- **************************************************

ALTER TABLE propertyusetype ADD COLUMN createdts TIMESTAMP WITH TIME ZONE;
ALTER TABLE propertyusetype ADD COLUMN createdby_umapid INTEGER CONSTRAINT propertyusetype_createdumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
ALTER TABLE propertyusetype ADD COLUMN deactivatedby_umapid INTEGER CONSTRAINT propertyusetype_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
ALTER TABLE propertyusetype ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE propertyusetype ADD COLUMN lastupdatedby_umapid INTEGER CONSTRAINT propertyusetype_lastupdatedumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);



-- **************************************************
-- Build OccPermitType Manage Page
-- Description: UMAP Tracking for OccPermitType
-- **************************************************

ALTER TABLE occpermittype DROP COLUMN active;
ALTER TABLE occpermittype ADD COLUMN createdts TIMESTAMP WITH TIME ZONE;
ALTER TABLE occpermittype ADD COLUMN createdby_umapid INTEGER CONSTRAINT occpermittype_createdumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
ALTER TABLE occpermittype ADD COLUMN deactivatedby_umapid INTEGER CONSTRAINT occpermittype_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
ALTER TABLE occpermittype ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE occpermittype ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE occpermittype ADD COLUMN lastupdatedby_umapid INTEGER CONSTRAINT occpermittype_lastupdatedumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
