-- **************************************************
-- PATCH BITS_005_TASK_7
-- Build Icon Management Page
-- Description: Remove fontawesome column from table
-- **************************************************


alter table public.icon drop column fontawesome;
SELECT setval('iconid_seq', (SELECT MAX(iconid) FROM icon));

