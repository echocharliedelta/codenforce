-- **************************************************
-- PATCH BITS_TASK_5
-- Description: Add column to linkedobjectrole table.
-- **************************************************

ALTER TABLE linkedobjectrole ADD COLUMN createdby_umapid INTEGER CONSTRAINT linkedobjectrole_createdumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
ALTER TABLE linkedobjectrole ADD COLUMN deactivatedby_umapid INTEGER CONSTRAINT linkedobjectrole_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
ALTER TABLE linkedobjectrole ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE linkedobjectrole ADD COLUMN lastupdatedby_umapid INTEGER CONSTRAINT linkedobjectrole_lastupdatedumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
									