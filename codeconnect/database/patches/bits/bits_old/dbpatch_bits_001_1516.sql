-- **************************************************
-- PATCH BITS_001_1516
-- ID_num: 1516: Build court entity manage page for UI
-- Reference: https://docs.google.com/spreadsheets/d/16Bp3f6j9KgzEtkYpcyW4bTDKUeuD4SQP7oBE7MxTchQ/edit#gid=407744701
-- Description: UMAP Tracking for Court Entity
-- **************************************************

ALTER TABLE courtentity ADD COLUMN createdts TIMESTAMP WITH TIME ZONE;
ALTER TABLE courtentity ADD COLUMN createdby_umapid INTEGER
    CONSTRAINT courtentity_createdumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE courtentity ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE courtentity ADD COLUMN lastupdatedby_umapid INTEGER
    CONSTRAINT courtentity_lastupdatedumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE courtentity ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE courtentity ADD COLUMN deactivatedby_umapid INTEGER
    CONSTRAINT courtentity_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);