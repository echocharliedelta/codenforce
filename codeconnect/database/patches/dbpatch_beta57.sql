
-- repurpose clone column to work with aliases with a simple rename
ALTER TABLE public.human DROP COLUMN cloneof_humanid;

ALTER TABLE public.human ADD COLUMN aliasof_humanid INTEGER CONSTRAINT human_aliasof_fk REFERENCES human (humanid);
ALTER TABLE public.event ADD COLUMN adminevent BOOLEAN DEFAULT FALSE;

-- CLEANUP in preparation for workflow tool
DROP TABLE IF EXISTS occperiodpdfdoc;
DROP TABLE IF EXISTS propertypdfdoc;

-- BEGIN workflow builder framework
-- Now for the workflow task family of tables

------------------- NOTE: RUN CREATE TYPES ALONE ------------------------------
-- RUN THESE ALONE
CREATE TYPE tasktypeenum AS ENUM (	'OPENING', 
									'CLOSING',
									'MILESTONE',
									'BRANCHCHOICE',
									'PAYMENT',
									'INSPECTION',
									'PERMIT',
									'LETTER',
									'FILE',
									'DOCUMENTPERSONREVIEW',
									'COMMUNICATION',
									'EXTERNAL',
									'CUSTOM');

CREATE TYPE taskassigneeroleenum AS ENUM (	'CODEOFFICER', 
											'FILEMANAGEROFFICER', 
											'MUNICIPALMANAGER', 
											'MUNICIPALSTAFF', 
											'COGSTAFF', 
											'EXTERNALENTITY', 
											'USERSPECIFIC', 
											'UNSPECIFIED');

CREATE TYPE taskduedatetypeenum AS ENUM (	'ABSOLUTE', 
										 	'RELATIVETOTASK', 
									 		'RELATIVETOAPPLICATIONDOR', 
								 			'RELATIVETOINSPECTIONDOR', 
								 			'RELATIVETOPERMITDOR');

CREATE TYPE tasksequencetype AS ENUM (		'FINISHTOSTART', 
											'FINISHTOFINISH', 
											'STARTTOSTART', 
											'STARTTOFINISH', 
											'SEQUENTIALUNSPECIFIED',
											'CHOICEOUTCOMEBRANCHHEAD');



CREATE SEQUENCE IF NOT EXISTS taskspecid_seq 
    START WITH 1000
    INCREMENT BY 1
    MINVALUE 1000
    NO MAXVALUE
    CACHE 1;


CREATE TABLE public.taskspec
(
	taskspecid 						INTEGER PRIMARY KEY DEFAULT nextval('taskspecid_seq'),
	title							TEXT NOT NULL,
	description						TEXT,
	tasktype    					tasktypeenum NOT NULL,
	duetype							taskduedatetypeenum NOT NULL,
	assignmentrolesuggested 		taskassigneeroleenum,
	predecessor_predid				INTEGER CONSTRAINT taskspec_predecessor_fk REFERENCES taskspec (taskspecid),
	predecessorrole 				tasksequencetype,
	defaultpredecessorbufferdays 	INTEGER,
	eventcatcompletion_catid		INTEGER CONSTRAINT taskspec_eventcatcompletion_fk REFERENCES eventcategory (categoryid),
	governingordinance_elementid	INTEGER CONSTRAINT taskchain_ord_fk REFERENCES codeelement (elementid),
	displayorder					INTEGER,
	notificationdefault 			BOOLEAN,
	expecteddurationmins			INTEGER,
	active 							BOOLEAN DEFAULT TRUE

);

CREATE SEQUENCE IF NOT EXISTS taskchainid_seq 
    START WITH 1000
    INCREMENT BY 1
    MINVALUE 1000
    NO MAXVALUE
    CACHE 1;


CREATE TABLE public.taskchain
(
	chainid 				INTEGER PRIMARY KEY DEFAULT nextval('taskchainid_seq '),
	title					TEXT NOT NULL,
	description				TEXT,
	municipality_municode 	INTEGER CONSTRAINT taskchain_municipality_municode_fk REFERENCES municipality (municode),
	governingordinance_elementid INTEGER CONSTRAINT taskchain_ord_fk REFERENCES codeelement (elementid),
	active					BOOLEAN DEFAULT TRUE
);


CREATE TABLE public.taskspectaskchain
(
	taskspec_specid 		INTEGER NOT NULL CONSTRAINT taskspectaskchain_fk REFERENCES taskspec (taskspecid),
	taskchain_chainid 		INTEGER NOT NULL CONSTRAINT taskchainchainid_fk REFERENCES taskchain (chainid),
	active 					BOOLEAN DEFAULT TRUE,
	CONSTRAINT taskspectaskchain_pk PRIMARY KEY (taskspec_specid, taskchain_chainid)

);

CREATE SEQUENCE IF NOT EXISTS taskrolemapping_seq 
    START WITH 1000
    INCREMENT BY 1
    MINVALUE 1000
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.taskrolemapping
(
	rolemappingid 			INTEGER PRIMARY KEY DEFAULT nextval('taskrolemapping_seq'),
	tasklrole 				taskassigneeroleenum NOT NULL,
	user_umapid 			INTEGER NOT NULL CONSTRAINT taskrolemapping_umap_fk REFERENCES loginmuniauthperiod (muniauthperiodid),
	municipality_municode 	INTEGER NOT NULL CONSTRAINT taskrolemapping_municode REFERENCES municipality (municode),
	notes 					TEXT,
	active 					BOOLEAN DEFAULT TRUE

);

-- tweak used to adjust old version of task
--ALTER TABLE taskrolemapping ADD COLUMN user_umapid INTEGER CONSTRAINT taskrolemapping_umap_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
-- ALTER TABLE taskrolemapping ADD COLUMN municipality_municode 	INTEGER NOT NULL CONSTRAINT taskrolemapping_municode REFERENCES municipality (municode);


CREATE SEQUENCE IF NOT EXISTS taskassignedid_seq 
    START WITH 1000
    INCREMENT BY 1
    MINVALUE 1000
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.taskassigned
(
	assignmentid 			INTEGER PRIMARY KEY DEFAULT nextval('taskassignedid_seq'),
	occperiod_periodid 		INTEGER NOT NULL CONSTRAINT taskassigned_occperiodid_fk REFERENCES occperiod (periodid),
	displayorderactual 		INTEGER,
	startby 				TIMESTAMP WITH TIME ZONE,
	predecessorbufferdays	INTEGER,
	startactualts 			TIMESTAMP WITH TIME ZONE,
	dueby 					TIMESTAMP WITH TIME ZONE,
	assignedtorole_mappingid			INTEGER CONSTRAINT taskassigned_rolemappingid_fk REFERENCES taskrolemapping (rolemappingid),
	remindersenabled 		BOOLEAN,
	completedts 			TIMESTAMP WITH TIME ZONE,
	completioncertifiedby_umapid 	INTEGER CONSTRAINT taskassigned_completioncertifiedby_umapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid),
	notes 					TEXT,
	nullifiedts 			TIMESTAMP WITH TIME ZONE,
	nullifiedby_umapid 		INTEGER CONSTRAINT taskassigned_nullifiedby_umapid REFERENCES loginmuniauthperiod (muniauthperiodid),
	deactivatedts 			TIMESTAMP WITH TIME ZONE,
	deactivatedby_umapid 	INTEGER CONSTRAINT tasksassigned_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid)
);

CREATE SEQUENCE IF NOT EXISTS taskassignedlinkid_seq 
    START WITH 1000
    INCREMENT BY 1
    MINVALUE 1000
    NO MAXVALUE
    CACHE 1;


CREATE TABLE IF NOT EXISTS taskassignedlinkage
(
	linkid 					INTEGER PRIMARY KEY DEFAULT nextval('taskassignedlinkid_seq'),
	taskassigned_taskid 	INTEGER NOT NULL CONSTRAINT taskassignedlinkages_taskassignedid_fk REFERENCES taskassigned (assignmentid),
	occapplication_appid 	INTEGER CONSTRAINT taskassignedlinkages_occpermitapplicationid_fk REFERENCES occpermitapplication (applicationid),
	event_eventid 			INTEGER CONSTRAINT taskassignedlinkages_eventid_fk REFERENCES event (eventid),
	inspection_inspectionid INTEGER CONSTRAINT taskassignedlinkages_inspectionid_fk REFERENCES occinspection (inspectionid),
	permit_permitid			INTEGER CONSTRAINT taskassignedlinkages_permitid_fk REFERENCES occpermit (permitid),
	document_photodocid 	INTEGER CONSTRAINT taskassignedlinkages_photodocid_fk REFERENCES photodoc (photodocid),
	cecase_caseid			INTEGER CONSTRAINT taskassignedlinkages_caseid_fk REFERENCES cecase (caseid),
	human_humanid 			INTEGER CONSTRAINT taskassignedlinkages_humanid_fk REFERENCES human (humanid),
	deactivatedts 			TIMESTAMP WITH TIME ZONE,
	deactivatedby_umapid 	INTEGER CONSTRAINT taskassignedlinkages_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid)
);

-- ECD couldn't get payment table muck unmucked on the server, so we're leaving out this llink field from the production server
-- ECD had to adjust local java to not query payment yet
-- ALTER TABLE taskassignedlinkage ADD COLUMN payment_paymentid 		INTEGER CONSTRAINT taskassignedlinkages_paymentid_fk REFERENCES moneyledger (transactionid);

-- Occ periods now need a type
-- this already exists in sylvia's local copy
CREATE SEQUENCE IF NOT EXISTS occperiodtypeid_seq 
    START WITH 1000
    INCREMENT BY 1
    MINVALUE 1000
    NO MAXVALUE
    CACHE 1;


CREATE TABLE public.occperiodtype
(
	typeid 		INTEGER PRIMARY KEY DEFAULT nextval('occperiodtypeid_seq'),
	title		TEXT NOT NULL,
	description	TEXT,
	taskchain_chainid INTEGER NOT NULL CONSTRAINT occperiodtype_chainid_fk REFERENCES taskchain (chainid),
	muni_municode INTEGER CONSTRAINT occperiodtype_municode_fk REFERENCES municipality (municode),
	active 		BOOLEAN DEFAULT TRUE,
	reinspectionwindowdays INTEGER DEFAULT 30
);
-- For late add
-- ALTER TABLE occperiodtype ADD COLUMN reinspectionwindowdays INTEGER DEFAULT 30;

-- now allow mapping of occ periods to a type
ALTER TABLE occperiod ADD COLUMN periodtype_typeid INTEGER 
	CONSTRAINT occperiod_periodtypeid_fk REFERENCES occperiodtype (typeid);

-- Declare allowable permit types by period type

CREATE TABLE public.occperiodtypepermittype
(
	periodtype_typeid INTEGER NOT NULL 
		CONSTRAINT occperiodtypepermittype_periodtypeid_fk REFERENCES occperiodtype (typeid),
	permittype_typeid INTEGER NOT NULL CONSTRAINT occperiodtypepermittype_permittypeid_fk REFERENCES occpermittype (typeid),
		CONSTRAINT occperiodtypepermittype_pk PRIMARY KEY (periodtype_typeid, permittype_typeid)
);


-- I forgot the all important link back up to task spec in taskassigned
ALTER TABLE taskassigned ADD COLUMN taskspec_specid INTEGER NOT NULL CONSTRAINT taskassigned_specid_fk REFERENCES taskspec (taskspecid);
ALTER TABLE taskassigned ADD COLUMN completiondateofrecord TIMESTAMP WITH TIME ZONE;




INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (57, 'database/patches/dbpatch_beta57.sql', '25-SEP-2023', 'ecd', 'scrape sync July 2023 deltas and task backbone of tables');