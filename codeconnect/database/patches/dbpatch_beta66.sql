
-- Permitting upgrades

ALTER TABLE public.occpermit ADD COLUMN staticdisplaymuniaddress BOOLEAN DEFAULT TRUE;

UPDATE public.occpermit SET staticdisplaymuniaddress = TRUE;

-- upgrades to space types
ALTER TABLE public.occinspectedspace ADD COLUMN lengthft REAL;
ALTER TABLE public.occinspectedspace ADD COLUMN lengthin REAL;
ALTER TABLE public.occinspectedspace ADD COLUMN widthft REAL;
ALTER TABLE public.occinspectedspace ADD COLUMN widthin REAL;
ALTER TABLE public.occinspectedspace ADD COLUMN totalsqft REAL;

ALTER TABLE public.occinspectedspace ADD COLUMN ceilingheightft REAL;
ALTER TABLE public.occinspectedspace ADD COLUMN ceilingheightin REAL;

ALTER TABLE public.occinspectedspace ADD COLUMN maxspaceoccupancypersons INTEGER;

ALTER TABLE public.occinspectedspace ADD COLUMN isbedroom BOOLEAN DEFAULT FALSE;
ALTER TABLE public.occinspectedspace ADD COLUMN islivingroom BOOLEAN DEFAULT FALSE;
ALTER TABLE public.occinspectedspace ADD COLUMN iskitchen BOOLEAN DEFAULT FALSE;
ALTER TABLE public.occinspectedspace ADD COLUMN isbathroom BOOLEAN DEFAULT FALSE;

ALTER TABLE public.occinspectedspace ADD COLUMN egresscount INTEGER;
ALTER TABLE public.occinspectedspace ADD COLUMN windowcount INTEGER;

ALTER TABLE public.occinspectedspace ADD COLUMN notes TEXT;

-- save canned remarks by permit type
ALTER TABLE public.occpermittype ADD COLUMN defaulttextblocksstipulations_textblockid INTEGER[];
ALTER TABLE public.occpermittype ADD COLUMN defaulttextblocksnotices_textblockid INTEGER[];
ALTER TABLE public.occpermittype ADD COLUMN defaulttextblockscomments_textblockid INTEGER[];


-- permit revocation
ALTER TABLE public.occpermit ADD COLUMN revokedreason TEXT;
ALTER TABLE public.occpermit ADD COLUMN revokedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE public.occpermit ADD COLUMN revokedby_umapid INTEGER 
	CONSTRAINT occpermit_revokedby_umapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

INSERT INTO public.eventcategory(
	categoryid, categorytype, title, 
	description, notifymonitors, hidable, 
	icon_iconid, relativeorderwithintype, relativeorderglobal, 
	hosteventdescriptionsuggtext, directive_directiveid, defaultdurationmins, 
	active, rolefloorenact, rolefloorview, 
	rolefloorupdate, prioritygreenbufferdays, alertevent, 
	alertevent_stopcategoryid, followuprequired, adminevent)
	VALUES (	3002, 'Occupancy'::eventtype, 'Certificate revoked', 
				'Officer revoked a previously issued certificate', FALSE, FALSE, 
				27, 2, 99, 
				NULL, NULL, 15, 
				TRUE, 'MuniStaff'::role, 'MuniReader'::role, 
				'MuniStaff'::role, 0, FALSE, 
				NULL, TRUE, FALSE);


-- permit customizations: custom person link labels
-- these will be used in permit auditing


ALTER TABLE	 public.occpermittype ADD COLUMN occupancyfamilypermit BOOLEAN DEFAULT TRUE;

ALTER TABLE	 public.occpermittype ADD COLUMN personlinkleftcolactivate  BOOLEAN;
ALTER TABLE	 public.occpermittype ADD COLUMN personlinkleftcolrequire  BOOLEAN;
ALTER TABLE	 public.occpermittype ADD COLUMN personlinkleftcolcustomheader TEXT;

ALTER TABLE	 public.occpermittype ADD COLUMN personlinkrightcolactivate  BOOLEAN;
ALTER TABLE	 public.occpermittype ADD COLUMN personlinkrightcolrequire  BOOLEAN;
ALTER TABLE	 public.occpermittype ADD COLUMN personlinkrightcolcustomheader TEXT;

ALTER TABLE	 public.occpermittype ADD COLUMN personlinkmanageractivate BOOLEAN;

ALTER TABLE	 public.occpermittype ADD COLUMN personlinktenantactivate BOOLEAN;


ALTER TABLE	 public.occpermittype ADD COLUMN displaymuniaddress BOOLEAN DEFAULT TRUE;

-- ******************************
-- RUN AFTER upgrade
-- this was moved over to occ permit type where it belongs not muniprofile
ALTER TABLE public.muniprofile DROP COLUMN permitdisplaymuniaddress;

-- not used as of July 2024
ALTER TABLE IF EXISTS public.occpermittype DROP COLUMN IF EXISTS permittitlesub;
ALTER TABLE IF EXISTS public.occpermittype DROP COLUMN IF EXISTS userassignable;
-- ******************************


ALTER TABLE public.occpermit ADD COLUMN staticdisplayperslinkleftcol BOOLEAN DEFAULT TRUE;
ALTER TABLE public.occpermit ADD COLUMN staticdisplayperslinkrightcol BOOLEAN DEFAULT TRUE;
ALTER TABLE public.occpermit ADD COLUMN staticdisplayperslinkmanagers BOOLEAN DEFAULT TRUE;
ALTER TABLE public.occpermit ADD COLUMN staticdisplayperslinktenants BOOLEAN DEFAULT TRUE;


-- update our existing permit and types
UPDATE public.occpermit SET staticdisplayperslinkleftcol=TRUE, staticdisplayperslinkrightcol=TRUE, staticdisplayperslinkmanagers=TRUE, staticdisplayperslinktenants=TRUE;

UPDATE public.occpermittype SET personlinkleftcolactivate=TRUE, personlinkleftcolrequire=FALSE, personlinkrightcolactivate=TRUE, personlinkrightcolrequire=FALSE, personlinkmanageractivate=TRUE, personlinktenantactivate=TRUE, displaymuniaddress=TRUE;

ALTER TABLE	 public.occpermittype ADD COLUMN personlinkmanagerrequire  BOOLEAN DEFAULT FALSE;
ALTER TABLE	 public.occpermittype ADD COLUMN personlinktenantrequire  BOOLEAN DEFAULT FALSE;

ALTER TABLE public.occpermit ADD COLUMN adminoverridets TIMESTAMP WITH TIME ZONE;
ALTER TABLE public.occpermit ADD COLUMN adminoverride_umapid INTEGER CONSTRAINT occpermit_adminoverride_umapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

-- REMOTE CURSOR

-- LOCAL CURSOR

INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (66, 'database/patches/dbpatch_beta66.sql', '20-JUL-2024', 'ecd', 'muni profile update for muni address on permit');