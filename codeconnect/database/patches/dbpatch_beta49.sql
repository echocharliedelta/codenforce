
-- LOCAL CURSOR
-- REMOTE CURSOR

-- worried about name clashing in Python, and so this is replaced by stname and old data copied over
ALTER TABLE mailingstreet DROP COLUMN name;

-- First patch from Benchmark work on CourtEntities
-- **************************************************
-- PATCH BITS_001_1516
-- ID_num: 1516: Build court entity manage page for UI
-- Reference: https://docs.google.com/spreadsheets/d/16Bp3f6j9KgzEtkYpcyW4bTDKUeuD4SQP7oBE7MxTchQ/edit#gid=407744701
-- Description: UMAP Tracking for Court Entity
-- **************************************************

ALTER TABLE courtentity ADD COLUMN createdts TIMESTAMP WITH TIME ZONE;
ALTER TABLE courtentity ADD COLUMN createdby_umapid INTEGER
    CONSTRAINT courtentity_createdumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE courtentity ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE courtentity ADD COLUMN lastupdatedby_umapid INTEGER
    CONSTRAINT courtentity_lastupdatedumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE courtentity ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE courtentity ADD COLUMN deactivatedby_umapid INTEGER
    CONSTRAINT courtentity_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);



INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (49, 'database/patches/dbpatch_beta49.sql',NULL, 'ecd', 'Part of Hillman 3 and 4, court entities');