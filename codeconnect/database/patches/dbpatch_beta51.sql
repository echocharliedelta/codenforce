
-- For resurrection of CEARs

ALTER TABLE ceactionrequest DROP COLUMN actrequestor_requestorid;

ALTER TABLE ceactionrequest ADD COLUMN requestorname TEXT;
ALTER TABLE ceactionrequest ADD COLUMN requestorphone TEXT;
ALTER TABLE ceactionrequest ADD COLUMN requestorphone_typeid INTEGER CONSTRAINT cear_requestorphonetype_fk REFERENCES contactphonetype (phonetypeid);
ALTER TABLE ceactionrequest ADD COLUMN requestoremail TEXT;
ALTER TABLE ceactionrequest ADD COLUMN requestorupdatesrequested BOOLEAN;

ALTER TABLE ceactionrequest RENAME COLUMN submittedtimestamp TO createdts;
ALTER TABLE ceactionrequest RENAME COLUMN usersubmitter_userid TO createdby_userid;
ALTER TABLE ceactionrequest DROP COLUMN active;

ALTER TABLE ceactionrequest ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE ceactionrequest ADD COLUMN lastupdatedby_userid INTEGER CONSTRAINT ceactionrequest_lastupdatdby_userid_fk REFERENCES login (userid);
ALTER TABLE ceactionrequest ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE ceactionrequest ADD COLUMN deactivatedby_userid INTEGER CONSTRAINT ceactionrequest_deactivatedby_userid_fk REFERENCES login (userid);

ALTER TABLE ceactionrequest DROP COLUMN property_propertyid;
ALTER TABLE ceactionrequeststatus ADD COLUMN active BOOLEAN DEFAULT TRUE;
ALTER TABLE ceactionrequest ADD COLUMN requestor_humanid INTEGER CONSTRAINT ceactionrequest_submitter_humanidfk REFERENCES human (humanid);

ALTER TABLE ceactionrequest ADD COLUMN requestorpersontype persontype;

INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (51, 'database/patches/dbpatch_beta51.sql', '16-MAY-2023', 'ecd', 'Second Hillman 5 deltas');