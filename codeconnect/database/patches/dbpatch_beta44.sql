-- UMAP and user beefing up

ALTER TABLE loginmuniauthperiod ADD COLUMN oathdoc_blobid INTEGER 
	CONSTRAINT loginmuniauthperiod_oathdoc_blobid_fk REFERENCES photodoc (photodocid);

-- Getting snappers code working
-- Notice these changes follow from DB patch 40 that was written during alpha phase

ALTER TABLE IF EXISTS public.mailingstreet
    ALTER COLUMN pobox DROP NOT NULL;


-- REMOTE CURSOR

ALTER TABLE loginmuniauthperiod ADD COLUMN codeofficer boolean DEFAULT FALSE;



-- Muni profile TOODs for permissions
-- ****************** Code Officer Flag Required to: *******************
-- 1. Conduct inspections- customizable by town
-- 2. Finalize inspections - customizable by town
-- 3. Issue permits - customizable by town - if signature is in system, there should be a disclosure about the risk of that
-- 3.5 - Attach violation to case
-- 4. Finalize and lock NOV - customizable by town
-- 5. Close case- customizable by town
-- 6. Log citation- customizable by town
-- 7. Update citation- customizable by town
-- ***************************************************************************
ALTER TABLE muniprofile ADD COLUMN ceoreqconductinspections boolean DEFAULT TRUE;
ALTER TABLE muniprofile ADD COLUMN ceoreqfinalizeinspections boolean  DEFAULT TRUE;
ALTER TABLE muniprofile ADD COLUMN ceoreqissuepermits boolean  DEFAULT TRUE;
ALTER TABLE muniprofile ADD COLUMN ceoreqattachviolationtocase boolean DEFAULT TRUE;
ALTER TABLE muniprofile ADD COLUMN ceoreqfinalizenov boolean DEFAULT TRUE;
ALTER TABLE muniprofile ADD COLUMN ceoreqclosecase boolean DEFAULT TRUE;
ALTER TABLE muniprofile ADD COLUMN ceoreqlogcitation boolean DEFAULT TRUE;
ALTER TABLE muniprofile ADD COLUMN ceorequpdatecitation boolean DEFAULT TRUE;


-- ADDED during the scrape test in Python land
-- 
INSERT INTO linkedobjectrole (lorid, lorschema, title, description, createdts, deactivatedts, notes) VALUES
	(235, 'ParcelMailingaddress', 'property address', null, now(), null, null);


ALTER TABLE muniprofile ADD COLUMN managerreqabandonement boolean DEFAULT TRUE;
ALTER TABLE muniprofile ADD COLUMN managerreqclosececase boolean DEFAULT TRUE;
ALTER TABLE muniprofile ADD COLUMN managerreqcitationopenclose boolean DEFAULT TRUE;
ALTER TABLE muniprofile ADD COLUMN managerreqpermitfinalize boolean DEFAULT TRUE;
ALTER TABLE muniprofile ADD COLUMN managerreqinspectionfinalize boolean DEFAULT TRUE;
ALTER TABLE muniprofile ADD COLUMN managerreqnovfinalize boolean DEFAULT TRUE;
ALTER TABLE muniprofile ADD COLUMN managerreqviolationextstipcomp boolean DEFAULT TRUE;
ALTER TABLE muniprofile ADD COLUMN managerreqchecklistedit boolean DEFAULT TRUE;
ALTER TABLE muniprofile ADD COLUMN managerreqcodesourcemanage boolean DEFAULT TRUE;
ALTER TABLE muniprofile ADD COLUMN managerreqcodebookmanage boolean DEFAULT TRUE;

ALTER TYPE role ADD VALUE 'MuniManager';

ALTER TABLE codesource ADD COLUMN muni_municode INTEGER 
	CONSTRAINT codesource_municode_fk REFERENCES municipality (municode);
ALTER TABLE codesource ADD COLUMN crossmuni BOOLEAN DEFAULT FALSE; 



-- THIRD generation of tracked entities that key to a UMAP and not a user
-- WHY the fuck I didn't key all the user tracking fields to the user's UMAP from
-- the very start I have no idea

ALTER TABLE codesource ADD COLUMN createdts TIMESTAMP WITH TIME ZONE;
ALTER TABLE codesource ADD COLUMN createdby_umapid INTEGER 
	CONSTRAINT codesource_createdumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE codesource ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE codesource ADD COLUMN lastupdatedby_umapid INTEGER 
	CONSTRAINT codesource_lastupdatedumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE codesource ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE codesource ADD COLUMN deactivatedby_umapid INTEGER 
	CONSTRAINT codesource_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE codeset ADD COLUMN createdts TIMESTAMP WITH TIME ZONE;
ALTER TABLE codeset ADD COLUMN createdby_umapid INTEGER 
	CONSTRAINT codeset_createdumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE codeset ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE codeset ADD COLUMN lastupdatedby_umapid INTEGER 
	CONSTRAINT codeset_lastupdatedumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE codeset ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE codeset ADD COLUMN deactivatedby_umapid INTEGER 
	CONSTRAINT codeset_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

-- Setting default TS for tables that python updates

ALTER TABLE mailingstreet ALTER COLUMN lastupdatedts SET DEFAULT now();

ALTER TABLE mailingaddress ALTER COLUMN createdts SET DEFAULT now();
ALTER TABLE mailingaddress ALTER COLUMN lastupdatedts SET DEFAULT now();

ALTER TABLE human ALTER COLUMN createdts SET DEFAULT now();
ALTER TABLE human ALTER COLUMN lastupdatedts SET DEFAULT now();

ALTER TABLE parcel ALTER COLUMN createdts SET DEFAULT now();
ALTER TABLE parcel ALTER COLUMN lastupdatedts SET DEFAULT now();



ALTER TABLE parcelmailingaddress ALTER COLUMN createdts SET DEFAULT now();
ALTER TABLE parcelmailingaddress ALTER COLUMN lastupdatedts SET DEFAULT now();

ALTER TABLE humanmailingaddress ALTER COLUMN createdts SET DEFAULT now();
ALTER TABLE humanmailingaddress ALTER COLUMN lastupdatedts SET DEFAULT now();

ALTER TABLE humanparcel ALTER COLUMN createdts SET DEFAULT now();
ALTER TABLE humanparcel ALTER COLUMN lastupdatedts SET DEFAULT now();

ALTER TABLE mailingstreet ADD COLUMN source_sourceid INTEGER REFERENCES public.bobsource (sourceid);

UPDATE loginmuniauthperiod SET authorizedrole = 'SysAdmin'::role WHERE authorizedrole='Developer'::role;
UPDATE loginmuniauthperiod SET authorizedrole = 'SysAdmin'::role WHERE authorizedrole='EnforcementOfficial'::role;

UPDATE eventcategory SET rolefloorenact = 'MuniStaff'::role WHERE rolefloorenact='Developer'::role;
UPDATE eventcategory SET rolefloorenact = 'MuniStaff'::role WHERE rolefloorenact='EnforcementOfficial'::role;

UPDATE eventcategory SET rolefloorview = 'MuniStaff'::role WHERE rolefloorview='Developer'::role;
UPDATE eventcategory SET rolefloorview = 'MuniStaff'::role WHERE rolefloorview='EnforcementOfficial'::role;

UPDATE eventcategory SET rolefloorupdate = 'MuniStaff'::role WHERE rolefloorupdate='Developer'::role;
UPDATE eventcategory SET rolefloorupdate = 'MuniStaff'::role WHERE rolefloorupdate='EnforcementOfficial'::role;


--remote cursor

-- LOCAL CURSOR

INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (44, 'database/patches/dbpatch_beta44.sql', '01-06-2023', 'ecd', 'second patch for 1.x.y');