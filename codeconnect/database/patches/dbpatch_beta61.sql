
-- One touch cases point to a municipality

CREATE SEQUENCE IF NOT EXISTS municipalityoneclickcase_seq
    START WITH 2000
    INCREMENT BY 1
    MINVALUE 2000
    NO MAXVALUE
    CACHE 1;



CREATE TABLE public.municipalityoneclickcase
(
	oneclickid 					INTEGER PRIMARY KEY DEFAULT nextval('municipalityoneclickcase_seq'),
	muni_municode 				INTEGER NOT NULL CONSTRAINT municipalityoneclickcase_municode_fk REFERENCES municipality (municode),
	title						TEXT NOT NULL,
	ordinance_codeseteleid		INTEGER NOT NULL CONSTRAINT municipalityoneclickcase_cseleid_fk REFERENCES codesetelement (codesetelementid),
	defaultcasename 			TEXT NOT NULL,
	orginationevent_catid 		INTEGER NOT NULL CONSTRAINT municipalityoneclickcase_origeventcatid_fk REFERENCES eventcategory (categoryid),
	novtemplate_templateid		INTEGER NOT NULL CONSTRAINT municipalityoneclickcase_templateid_fk	REFERENCES noticeofviolationtemplate (templateid),
	icon_iconid					INTEGER NOT NULL CONSTRAINT municipalityoneclickcase_iconid_fk REFERENCES icon (iconid),
	deactivatedts 				TIMESTAMP WITH TIME ZONE,
	notes						TEXT
);


-- this switch on the muni profile controls the default filter on address links during address search
ALTER TABLE muniprofile ADD COLUMN parcelmadlinkrolefilter_lorid INTEGER CONSTRAINT muniprofile_parcelmadlinkrolefilter_fk REFERENCES linkedobjectrole (lorid);

ALTER TABLE public.cecase ADD COLUMN oneclick_clickid INTEGER CONSTRAINT cecase_oneclickid_fk REFERENCES municipalityoneclickcase (oneclickid);

ALTER TABLE IF EXISTS public.occinspectedspace
    ALTER COLUMN occlocationdescription_descid DROP NOT NULL;


-- What about a user active property list that is actually curatable by user, and initially populated with sensible parcel 
-- links based on open cases, upcoming events, etc.



-- Updated to use HTML non-self-closing break break

CREATE OR REPLACE FUNCTION public.cnf_buildparceladdressstring(
	in_parcelid INTEGER, in_includecsz BOOLEAN, in_includehtmlbreak BOOLEAN)
    RETURNS TEXT
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
    DECLARE
        address_record RECORD;
        addr_pretty TEXT := '';
    BEGIN
    	EXECUTE 'SELECT addressid, bldgno, mailingstreet.stname, default_city AS city, default_state AS state2char, zip_code AS zip '
				'FROM public.parcelmailingaddress '
				'INNER JOIN public.linkedobjectrole ON (linkedobjectrole_lorid = lorid) '
				'INNER JOIN public.mailingaddress ON (mailingaddress_addressid = addressid) '
				'LEFT OUTER JOIN public.mailingstreet ON (street_streetid = streetid) '
				'LEFT OUTER JOIN public.mailingcitystatezip ON (citystatezip_cszipid = id) '
				'WHERE parcel_parcelkey = $1 '
				'ORDER BY linkedobjectrole.sortorder ASC, parcelmailingaddress.priority ASC '
				'LIMIT 1'
			INTO address_record
			USING in_parcelid;

		-- Now assemble the actual address string
		addr_pretty := address_record.bldgno || ' ' || address_record.stname;
		
		IF in_includecsz THEN
			IF in_includehtmlbreak THEN
				addr_pretty := addr_pretty || '<br>';
			ELSE
				addr_pretty := addr_pretty || ', ';
			END IF;
			addr_pretty :=  addr_pretty || address_record.city || ', ' || address_record.state2char || ' ' || address_record.zip;
		END IF;
		RETURN addr_pretty;
    END;
$BODY$;



INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (61, 'database/patches/dbpatch_beta61.sql', '30-NOV-2023', 'ecd', 'one touch case buttons and other junk');
