-- Builds on patch 67's inspectedspace fields


ALTER TABLE public.occspacetype ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;

ALTER TABLE public.occinspectedspace ADD COLUMN spacelocation TEXT;



ALTER TABLE public.occinspectedspace DROP COLUMN totalsqft;
ALTER TABLE public.occinspectedspace DROP COLUMN totalsqft2;


ALTER TABLE public.occinspectedspace ADD COLUMN lengthft REAL;
ALTER TABLE public.occinspectedspace ADD COLUMN widthft REAL;
ALTER TABLE public.occinspectedspace ADD COLUMN lengthft2 REAL;
ALTER TABLE public.occinspectedspace ADD COLUMN widthft2 REAL;





ALTER TABLE codeviolation RENAME COLUMN entrytimestamp TO createdts;
ALTER TABLE codeviolation RENAME COLUMN createdby TO createdby_userid;

ALTER TABLE codeviolation RENAME COLUMN lastupdated_userid TO lastupdatedby_userid;


ALTER TABLE codeviolation ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE codeviolation ADD COLUMN deactivatedby_userid INTEGER CONSTRAINT codeviolation_deacby_userid_fk REFERENCES login (userid);

UPDATE codeviolation
SET deactivatedts = now(),
    deactivatedby_userid = 99
WHERE active = false;


ALTER TYPE eventtype RENAME VALUE 'Compliance' TO 'Violation';




INSERT INTO public.eventcategory(
	categoryid, categorytype, title, 
	description, notifymonitors, hidable, 
	icon_iconid, relativeorderwithintype, relativeorderglobal, 
	hosteventdescriptionsuggtext, directive_directiveid, defaultdurationmins, 
	active, rolefloorenact, rolefloorview, 
	rolefloorupdate, prioritygreenbufferdays, alertevent, 
	alertevent_stopcategoryid, followuprequired, adminevent)
	VALUES (	330, 'Violation'::eventtype, 'Violation(s) nullified', 
				'One or more code violation has been nullified', FALSE, FALSE, 
				27, 2, 99, 
				NULL, NULL, 15, 
				TRUE, 'MuniStaff'::role, 'MuniReader'::role, 
				'MuniStaff'::role, 0, FALSE, 
				NULL, FALSE, FALSE);

INSERT INTO public.eventcategory(
	categoryid, categorytype, title, 
	description, notifymonitors, hidable, 
	icon_iconid, relativeorderwithintype, relativeorderglobal, 
	hosteventdescriptionsuggtext, directive_directiveid, defaultdurationmins, 
	active, rolefloorenact, rolefloorview, 
	rolefloorupdate, prioritygreenbufferdays, alertevent, 
	alertevent_stopcategoryid, followuprequired, adminevent)
	VALUES (	331, 'Violation'::eventtype, 'Compliance due date extended', 
				'Stipulated compliance due date has been extended on one or more violations', FALSE, FALSE, 
				27, 3, 99, 
				NULL, NULL, 15, 
				TRUE, 'MuniStaff'::role, 'MuniReader'::role, 
				'MuniStaff'::role, 0, FALSE, 
				NULL, FALSE, FALSE);

-- LOCAL CURSOR
-- REMOTE CURSOR


INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (68, 'database/patches/dbpatch_beta68.sql', '27-SEP-2024', 'ecd', 'OIS upgrades and batch violation stuff');
