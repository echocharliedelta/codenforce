-- MISC upgrades, mostly related to dispatching of field inspections to handheld and lighning case tweaks


-- dispatch features
ALTER TABLE public.occchecklistspacetype ADD COLUMN autoaddoninspectioninit BOOLEAN DEFAULT FALSE;

ALTER TABlE public.occperiodtype ADD COLUMN defaultchecklist INTEGER CONSTRAINT occperiodtype_defaultchecklist_fk REFERENCES public.occchecklist (checklistid);
ALTER TABlE public.municipality ADD COLUMN defaultcodeenfchecklist INTEGER CONSTRAINT muni_defaultchecklist_fk REFERENCES public.occchecklist (checklistid);


ALTER TABLE public.municipalityoneclickcase ADD COLUMN autofinalizenov BOOLEAN DEFAULT TRUE;

ALTER TABLE public.occchecklist ADD COLUMN defaultinspectioncause INTEGER CONSTRAINT occhecklist_defaultcause_fk 
	REFERENCES public.occinspectioncause (causeid);

ALTER TABLE public.muniprofile ADD COLUMN dispatchfieldinspectionbydefault BOOLEAN DEFAULT TRUE;

ALTER TABLE public.occinspectiondetermination ADD COLUMN documentationonly BOOLEAN DEFAULT FALSE;

ALTER TABLE public.occinspectiondetermination ADD COLUMN icon_iconid INTEGER CONSTRAINT occdetermination_iconid_fk REFERENCES public.icon (iconid);

ALTER TABLE public.muniprofile ADD COLUMN defaultcecaseoriginationevent_catid INTEGER 
	CONSTRAINT muniprofile_defaultcecaseorigincatid_fk REFERENCES public.eventcategory (categoryid);

ALTER TABLE IF EXISTS public.occchecklist DROP COLUMN IF EXISTS governingcodesource_sourceid;
-- LOCAL CURSOR
-- REMOTE CURSOR


INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (66, 'database/patches/dbpatch_beta65.sql', '16-MAY-2024', 'ecd', 'dispatch functionality and lightning case tweaks');