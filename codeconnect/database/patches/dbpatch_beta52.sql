-- PATCH for BITS 6 and Eric's CEAction Request build-out
-- NOTE that this frist ALTER TYPE must be run as its own transaction

------------------------ RUN THIS LINE BY ITSELF ----------------------------------
--ALTER TYPE linkedobjectroleschema ADD VALUE 'CEACtionRequestHuman';
------------------------ RUN THIS LINE BY ITSELF ----------------------------------

CREATE SEQUENCE IF NOT EXISTS ceactionrequesthuman_seq 
    START WITH 1000
    INCREMENT BY 1
    MINVALUE 1000
    NO MAXVALUE
    CACHE 1;

CREATE TABLE IF NOT EXISTS public.ceactionrequesthuman
(
    linkid integer NOT NULL DEFAULT nextval('ceactionrequesthuman_seq'::regclass),
    human_humanid integer,
    ceactionrequest_requestid integer,
    source_sourceid integer,
    createdts timestamp with time zone,
    createdby_userid integer,
    lastupdatedts timestamp with time zone,
    lastupdatedby_userid integer,
    deactivatedts timestamp with time zone,
    deactivatedby_userid integer,
    notes text COLLATE pg_catalog."default",
    linkedobjectrole_lorid integer,
    CONSTRAINT ceactionrequesthuman_pkey PRIMARY KEY (linkid),
    CONSTRAINT ceactionrequesthuman_lorid_fk FOREIGN KEY (linkedobjectrole_lorid)
        REFERENCES public.linkedobjectrole (lorid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT ceactionrequesthuman_citationid_fk FOREIGN KEY (ceactionrequest_requestid)
        REFERENCES public.ceactionrequest (requestid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT ceactionrequesthuman_createdby_userid_fk FOREIGN KEY (createdby_userid)
        REFERENCES public.login (userid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT ceactionrequesthuman_deactivatedby_userid_fk FOREIGN KEY (deactivatedby_userid)
        REFERENCES public.login (userid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT ceactionrequesthuman_humanid_fk FOREIGN KEY (human_humanid)
        REFERENCES public.human (humanid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT ceactionrequesthuman_lastupdatdby_userid_fk FOREIGN KEY (lastupdatedby_userid)
        REFERENCES public.login (userid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT ceactionrequesthuman_sourceid_fk FOREIGN KEY (source_sourceid)
        REFERENCES public.bobsource (sourceid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);


--THE Creator and updator UMAP ID must be in the table loginmuniauthperiod and is not logic restricted beyond a simple FK (i.e. UMAP rank and validity not audited)
INSERT INTO public.linkedobjectrole(
	lorid, lorschema, title, description, createdts, deactivatedts, notes, createdby_umapid, deactivatedby_umapid, lastupdatedts, lastupdatedby_umapid)
	VALUES (300, CAST('CEACtionRequestHuman' AS linkedobjectroleschema), 'Request submittor', 'The person who submits a CEAction Request', now(), NULL, NULL, 1047, NULL, now(), 1047);

-- Roles injected by the GUI get IDs 2200+ leaving room on the bottom for manual inserts with known keys
SELECT setval('public.linkedobjectrole_seq', 2200, true);

-- **************************************************
-- PATCH BITS_004_TASK_6
-- Build OccInspection Determination Manage Page
-- Description: UMAP Tracking for OccInspectionDetermination
-- **************************************************

alter table occinspectiondetermination drop column active; 
ALTER TABLE occinspectiondetermination ADD COLUMN createdts TIMESTAMP WITH TIME ZONE;
ALTER TABLE occinspectiondetermination ADD COLUMN createdby_umapid INTEGER CONSTRAINT occinspectiondetermination_createdumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
ALTER TABLE occinspectiondetermination ADD COLUMN deactivatedby_umapid INTEGER CONSTRAINT occinspectiondetermination_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
ALTER TABLE occinspectiondetermination ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE occinspectiondetermination ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE occinspectiondetermination ADD COLUMN lastupdatedby_umapid INTEGER CONSTRAINT occinspectiondetermination_lastupdatedumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);


-- **************************************************
-- Build OccInspection Cause Manage Page
-- Description: UMAP Tracking for OccInspectionCause
-- **************************************************

alter table occinspectioncause drop column active; 
ALTER TABLE occinspectioncause ADD COLUMN createdts TIMESTAMP WITH TIME ZONE;
ALTER TABLE occinspectioncause ADD COLUMN createdby_umapid INTEGER CONSTRAINT occinspectioncause_createdumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
ALTER TABLE occinspectioncause ADD COLUMN deactivatedby_umapid INTEGER CONSTRAINT occinspectioncause_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
ALTER TABLE occinspectioncause ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE occinspectioncause ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE occinspectioncause ADD COLUMN lastupdatedby_umapid INTEGER CONSTRAINT occinspectioncause_lastupdatedumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);


-- **************************************************
-- Build PropertyUseType Manage Page
-- Description: UMAP Tracking for OccInspectionCause
-- **************************************************

ALTER TABLE propertyusetype ADD COLUMN createdts TIMESTAMP WITH TIME ZONE;
ALTER TABLE propertyusetype ADD COLUMN createdby_umapid INTEGER CONSTRAINT propertyusetype_createdumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
ALTER TABLE propertyusetype ADD COLUMN deactivatedby_umapid INTEGER CONSTRAINT propertyusetype_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
ALTER TABLE propertyusetype ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE propertyusetype ADD COLUMN lastupdatedby_umapid INTEGER CONSTRAINT propertyusetype_lastupdatedumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);



-- **************************************************
-- Build OccPermitType Manage Page
-- Description: UMAP Tracking for OccPermitType
-- **************************************************

ALTER TABLE occpermittype DROP COLUMN active;
ALTER TABLE occpermittype ADD COLUMN createdts TIMESTAMP WITH TIME ZONE;
ALTER TABLE occpermittype ADD COLUMN createdby_umapid INTEGER CONSTRAINT occpermittype_createdumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
ALTER TABLE occpermittype ADD COLUMN deactivatedby_umapid INTEGER CONSTRAINT occpermittype_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
ALTER TABLE occpermittype ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE occpermittype ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE occpermittype ADD COLUMN lastupdatedby_umapid INTEGER CONSTRAINT occpermittype_lastupdatedumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);




INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (52, 'database/patches/dbpatch_beta52.sql', '19-MAY-2023', 'ecd', 'Hillman 5-6 deltas and BITS 6');