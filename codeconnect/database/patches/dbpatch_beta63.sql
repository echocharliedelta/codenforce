-- DB PATCH 63: includes Hillman sprint 11 and 12 deltas 
-- RELEASED: 13 Feb 2024
-- also includes batch updates to the lastupdatedts for intensityclass, occchecklistspacetype and occchecklistspacetypeelement



-- *********************************************************************
-- *********************************************************************
-- >>>>>>>>>>>>>>>>>>>>>   RUN ALTER TYPES  ALONE <<<<<<<<<<<<<<<<<<<<<<<
-- *********************************************************************
-- *********************************************************************
-- Uncomment and RUN ALONE! 
-- Postgres doesn't let alter types run with other DDL commands
-- OR at least ECD is too lazy to figure out how

-- ALTER TYPE public.eventtype
--     ADD VALUE 'PropertyAlert' AFTER 'PropertyInfoCase';
-- ALTER TYPE public.linkedobjectroleschema
--     ADD VALUE 'OccPermitHuman' AFTER 'OccPeriodHuman';
--

-- Hillman 11 deltas

ALTER TABLE public.muniprofile ADD COLUMN defaultinspectionfollowupwindow INTEGER DEFAULT 30;

ALTER TABLE public.muniprofile ADD COLUMN followupeventcatfailedfin_categoryid INTEGER 
	CONSTRAINT muniprofile_failedfinfollowupeventcat_fk REFERENCES eventcategory (categoryid);

ALTER TABLE public.muniprofile ADD COLUMN followupeventcatexpiredtco_categoryid INTEGER 
	CONSTRAINT muniprofile_expiredtcofollowupeventcat_fk REFERENCES eventcategory (categoryid);


ALTER TABLE public.muniprofile ADD COLUMN eventpermitissued_categoryid INTEGER 
	CONSTRAINT muniprofile_eventpermitissuedeventcat_fk REFERENCES eventcategory (categoryid);

ALTER TABLE public.muniprofile ADD COLUMN eventinspectionpassedperformed_categoryid INTEGER 
	CONSTRAINT muniprofile_eventinspectionpassedeventcat_fk REFERENCES eventcategory (categoryid);

ALTER TABLE public.muniprofile ADD COLUMN eventinspectionfailedperformed_categoryid INTEGER 
	CONSTRAINT muniprofile_eventinspectionfailedeventcat_fk REFERENCES eventcategory (categoryid);

ALTER TABLE public.muniprofile ADD COLUMN eventcitationissued_categoryid INTEGER 
	CONSTRAINT muniprofile_eventcitationissuedeventcat_fk REFERENCES eventcategory (categoryid);


-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- READ ME
-- Possible discrepancy during 2.1.6 upgrade: Prod system required the icon_iconid FK field on codeelmentguide
-- ECD Doesn't readily know how the prod sytem missed this field, since it wasn't a new add in recent patches
-- although the guide table did get standad fields

-- ALTER TABLE public.codeelementguide ADD COLUMN icon_iconid INTEGER CONSTRAINT codeelementguide_icondid_fk REFERENCES public.icon (iconid);

-- END READ ME <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

ALTER TABLE IF EXISTS public.codeelementguide
    RENAME CONSTRAINT textblockcategory_iconid_fk TO codeelementguide_icondid_fk;

-- UPDATED to not SELECT Deactivated Addresses!
CREATE OR REPLACE FUNCTION public.cnf_buildparceladdressstring(
	in_parcelid integer,
	in_includecsz boolean,
	in_includehtmlbreak boolean)
    RETURNS text
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
    DECLARE
        address_record RECORD;
        addr_pretty TEXT := '';
    BEGIN
    	EXECUTE 'SELECT addressid, bldgno, mailingstreet.stname, default_city AS city, default_state AS state2char, zip_code AS zip '
				'FROM public.parcelmailingaddress '
				'INNER JOIN public.linkedobjectrole ON (linkedobjectrole_lorid = lorid) '
				'INNER JOIN public.mailingaddress ON (mailingaddress_addressid = addressid) '
				'LEFT OUTER JOIN public.mailingstreet ON (street_streetid = streetid) '
				'LEFT OUTER JOIN public.mailingcitystatezip ON (citystatezip_cszipid = id) '
				'WHERE parcelmailingaddress.deactivatedts IS NULL AND linkedobjectrole.deactivatedts IS NULL AND mailingaddress.deactivatedts IS NULL AND mailingstreet.deactivatedts IS NULL AND mailingcitystatezip.deactivatedts IS NULL '
				'AND parcel_parcelkey = $1 '
				'ORDER BY linkedobjectrole.sortorder ASC, parcelmailingaddress.priority ASC '
				'LIMIT 1'
			INTO address_record
			USING in_parcelid;

		-- Now assemble the actual address string
		addr_pretty := address_record.bldgno || ' ' || address_record.stname;
		
		IF in_includecsz THEN
			IF in_includehtmlbreak THEN
				addr_pretty := addr_pretty || '<br>';
			ELSE
				addr_pretty := addr_pretty || ', ';
			END IF;
			addr_pretty :=  addr_pretty || address_record.city || ', ' || address_record.state2char || ' ' || address_record.zip;
		END IF;
		RETURN addr_pretty;
    END;
$BODY$;

ALTER FUNCTION public.cnf_buildparceladdressstring(integer, boolean, boolean)
    OWNER TO sylvia;


ALTER TABLE public.citationstatus ADD COLUMN eventcategory_categoryid INTEGER CONSTRAINT citationstatus_eventcategoryid_fk REFERENCES eventcategory (categoryid);

ALTER TABLE IF EXISTS public.eventlinkage DROP COLUMN IF EXISTS citation_citationid;

ALTER TABLE public.muniprofile DROP COLUMN IF EXISTS eventcitationissued_categoryid; 


-- These fields are for allowing saving of permit configuration 
--fields BEFORE they get written to the static fields

ALTER TABLE public.occpermit ADD COLUMN dynamiclastsavedflowstep INTEGER DEFAULT 0;
ALTER TABLE public.occpermit ADD COLUMN dynamiclastsavedts timestamp with time zone;

ALTER TABLE public.occpermit ADD COLUMN dynamicparcelinfo INTEGER CONSTRAINT occpermitdynamic_parcelinfo_fk REFERENCES parcelinfo (parcelinfoid);

ALTER TABLE public.occpermit ADD COLUMN dynamicownersellertype TEXT;
ALTER TABLE public.occpermit ADD COLUMN dynamicownersellerlinks INTEGER[];

ALTER TABLE public.occpermit ADD COLUMN dynamicbuyertenanttype TEXT;
ALTER TABLE public.occpermit ADD COLUMN dynamicbuyersellercolumnlink TEXT;
ALTER TABLE public.occpermit ADD COLUMN dynamicbuyertenantlinks INTEGER[];

ALTER TABLE public.occpermit ADD COLUMN dynamicmanagerlinks INTEGER[];
ALTER TABLE public.occpermit ADD COLUMN dynamictenantlinks INTEGER[];

ALTER TABLE public.occpermit ADD COLUMN dynamicissuingofficer INTEGER CONSTRAINT occpermitdynaic_officer_userid_fk REFERENCES login (userid);
ALTER TABLE public.occpermit ADD COLUMN dynamiccodesources INTEGER[];

ALTER TABLE public.occpermit ADD COLUMN dynamicstipulationsfreeform TEXT;
ALTER TABLE public.occpermit ADD COLUMN dynamicnoticesfreeform TEXT;
ALTER TABLE public.occpermit ADD COLUMN dynamiccommentsfreeform TEXT;

ALTER TABLE public.occpermit ADD COLUMN dynamicnoticesblocks INTEGER[];
ALTER TABLE public.occpermit ADD COLUMN dynamiccommentsblocks INTEGER[];
ALTER TABLE public.occpermit ADD COLUMN dynamicstipulationblocks INTEGER[];

ALTER TABLE public.occpermit ADD COLUMN dynamicdateofapplication timestamp with time zone;
ALTER TABLE public.occpermit ADD COLUMN dynamicinitialinspection timestamp with time zone;
ALTER TABLE public.occpermit ADD COLUMN dynamicreinspectiondate timestamp with time zone;
ALTER TABLE public.occpermit ADD COLUMN dynamicfinalinspection timestamp with time zone;
ALTER TABLE public.occpermit ADD COLUMN dynamicdateofissue timestamp with time zone;
ALTER TABLE public.occpermit ADD COLUMN dynamicdateexpiry timestamp with time zone;


ALTER TABLE public.occpermit ADD COLUMN dynamicinitialinspection_inspectionid INTEGER CONSTRAINT dynamicinitfin_finid_fk REFERENCES occinspection (inspectionid);
ALTER TABLE public.occpermit ADD COLUMN dynamicreinspectiondate_inspectionid INTEGER CONSTRAINT dynamicreinspectfin_finid_fk REFERENCES occinspection (inspectionid);
ALTER TABLE public.occpermit ADD COLUMN dynamicfinalinspection_inspectionid INTEGER CONSTRAINT dynamicfinalfin_finid_fk REFERENCES occinspection (inspectionid);

ALTER TABLE public.occpermit ADD COLUMN dynamicapplication_applictionid INTEGER CONSTRAINT dynamicapplication_appidfk REFERENCES occpermitapplication (applicationid);



ALTER TABLE public.occpermittype ADD COLUMN personcolumnleftenum TEXT;
ALTER TABLE public.occpermittype ADD COLUMN personcolumnrightenum TEXT;
ALTER TABLE public.occpermittype ADD COLUMN personcolumseparator TEXT;




CREATE SEQUENCE IF NOT EXISTS occpermithuman_linkid_seq
    START WITH 2000
    INCREMENT BY 1
    MINVALUE 2000
    NO MAXVALUE
    CACHE 1;



CREATE TABLE IF NOT EXISTS public.occpermithuman
(
    linkid integer NOT NULL DEFAULT nextval('occpermithuman_linkid_seq'::regclass),
    human_humanid integer,
    createdts timestamp with time zone,
    createdby_userid integer,
    lastupdatedts timestamp with time zone,
    lastupdatedby_userid integer,
    deactivatedts timestamp with time zone,
    deactivatedby_userid integer,
    notes text COLLATE pg_catalog."default",
    occpermit_permitid integer,
    linkedobjectrole_lorid integer,
    source_sourceid integer,
    CONSTRAINT occpermithuman_pkey PRIMARY KEY (linkid),
    CONSTRAINT occpermithuman_createdby_userid_fk FOREIGN KEY (createdby_userid)
        REFERENCES public.login (userid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT occpermithuman_deactivatedby_userid_fk FOREIGN KEY (deactivatedby_userid)
        REFERENCES public.login (userid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT occpermithuman_lastupdatdby_userid_fk FOREIGN KEY (lastupdatedby_userid)
        REFERENCES public.login (userid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT occpermithuman_muniid_fk FOREIGN KEY (human_humanid)
        REFERENCES public.human (humanid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT occpermithuman_lorid_fk FOREIGN KEY (linkedobjectrole_lorid)
        REFERENCES public.linkedobjectrole (lorid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT occpermithuman_permitid_fk FOREIGN KEY (occpermit_permitid)
        REFERENCES public.occpermit (permitid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT occpermithuman_sourceid_fk FOREIGN KEY (source_sourceid)
        REFERENCES public.bobsource (sourceid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);





INSERT INTO public.linkedobjectrole(
	lorid, lorschema, title, 
	description, createdts, deactivatedts, 
	notes, createdby_umapid, deactivatedby_umapid, 
	lastupdatedts, lastupdatedby_umapid, sortorder)
	VALUES (400, 'OccPermitHuman'::linkedobjectroleschema, 'Permit person link: owner-seller', 
		'Human linked to the left person column on an occ permit', now(), NULL, 
		NULL, 98, NULL, 
		now(), 98, 1);

INSERT INTO public.linkedobjectrole(
	lorid, lorschema, title, 
	description, createdts, deactivatedts, 
	notes, createdby_umapid, deactivatedby_umapid, 
	lastupdatedts, lastupdatedby_umapid, sortorder)
	VALUES (401, 'OccPermitHuman'::linkedobjectroleschema, 'Permit person link: buyer-tenant', 
		'Human linked to the right person column on an occ permit for buyers and tenants', now(), NULL, 
		NULL, 98, NULL, 
		now(), 98, 1);

INSERT INTO public.linkedobjectrole(
	lorid, lorschema, title, 
	description, createdts, deactivatedts, 
	notes, createdby_umapid, deactivatedby_umapid, 
	lastupdatedts, lastupdatedby_umapid, sortorder)
	VALUES (402, 'OccPermitHuman'::linkedobjectroleschema, 'Permit person link: manager', 
		'Human linked on an occ permit as a manager', now(), NULL, 
		NULL, 98, NULL, 
		now(), 98, 1);

INSERT INTO public.linkedobjectrole(
	lorid, lorschema, title, 
	description, createdts, deactivatedts, 
	notes, createdby_umapid, deactivatedby_umapid, 
	lastupdatedts, lastupdatedby_umapid, sortorder)
	VALUES (403, 'OccPermitHuman'::linkedobjectroleschema, 'Permit person link: sale with tenant', 
		'Human linked to an occ permit sale with tenant', now(), NULL, 
		NULL, 98, NULL, 
		now(), 98, 1);


ALTER TABLE public.occperiodtype ADD COLUMN defaultoriginationeventcat_categoryid INTEGER 
	CONSTRAINT occperiodtype_defaultopeningeventcat_catid_fk REFERENCES eventcategory (categoryid);



ALTER TABLE public.printstyle RENAME COLUMN headerheight TO headerwidth;
ALTER TABLE public.occpermit RENAME COLUMN staticheaderheightpx TO staticheaderwidthpx;



-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- BITS updates: writing lastupdatedts to tables that the handheld software read

 UPDATE public.occchecklistspacetype SET lastupdatedts = now();
 -- fix outrageous typo!!
 ALTER TABLE public.occchecklistspacetypeelement RENAME COLUMN lastudpatedts TO lastupdatedts;
 UPDATE public.occchecklistspacetypeelement SET lastupdatedts = now();
 UPDATE public.intensityclass SET lastupdatedts = now();


-- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
-- REMOTE CURSOR

-- LOCAL CURSOR
    
INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (63, 'database/patches/dbpatch_beta63.sql', '13-FEB-2024', 'ecd', 'event overhaul; dynamic permit saving; bits updates on checklists');


