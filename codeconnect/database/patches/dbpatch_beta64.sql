-- Patch 64

    
INSERT INTO public.eventcategory(
	categoryid, categorytype, title, 
	description, notifymonitors, hidable, 
	icon_iconid, relativeorderwithintype, relativeorderglobal, 
	hosteventdescriptionsuggtext, directive_directiveid, defaultdurationmins, 
	active, rolefloorenact, rolefloorview, 
	rolefloorupdate, prioritygreenbufferdays, alertevent, 
	alertevent_stopcategoryid, followuprequired, adminevent)
	VALUES (	3001, 'Occupancy'::eventtype, 'Reminder to issue certificate', 
				'Reminder event', FALSE, FALSE, 
				27, 1, 99, 
				'Issue finalized occupancy certificate to the appropriate recipient', NULL, 15, 
				TRUE, 'MuniStaff'::role, 'MuniStaff'::role, 
				'MuniStaff'::role, 0, FALSE, 
				NULL, FALSE, FALSE);


DROP TABLE IF EXISTS munilogin;


ALTER TABLE municipality DROP COLUMN IF EXISTS munimanager_userid;
ALTER TABLE municipality DROP COLUMN IF EXISTS primarystaffcontact_userid;
ALTER TABLE municipality DROP COLUMN IF EXISTS contact_personid;

ALTER TABLE municipality ADD COLUMN munimnager_umap INTEGER CONSTRAINT municipality_manager_umap_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
ALTER TABLE municipality ADD COLUMN primarycodeofficer_umap INTEGER CONSTRAINT municipality_officer_umap_fk REFERENCES loginmuniauthperiod (muniauthperiodid);
ALTER TABLE municipality ADD COLUMN admincontact_umap INTEGER CONSTRAINT municipality_admin_umap_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE municipality RENAME COLUMN defaultheaderimageheightpx TO defaultheaderimagewidthpx;



ALTER TABLE public.noticeofviolation ADD COLUMN fixedrecipientmailingaddresscombined TEXT DEFAULT NULL;



ALTER TABLE public.occpermit ADD COLUMN amendmentinitts TIMESTAMP WITH TIME ZONE;
ALTER TABLE public.occpermit ADD COLUMN amendmentcommitts TIMESTAMP WITH TIME ZONE;
ALTER TABLE public.occpermit ADD COLUMN amendedby_umapid INTEGER CONSTRAINT occpermit_amendedby_umap_fk REFERENCES loginmuniauthperiod (muniauthperiodid);


ALTER TABLE public.occpermit DROP COLUMN staticadditionaltext;

-- *********************************************************************
-- *********************************************************************
-- >>>>>>>>>>>>>>>>>>>>>   RUN ALTER TYPES  ALONE <<<<<<<<<<<<<<<<<<<<<<<
-- *********************************************************************
-- *********************************************************************
-- Uncomment and RUN ALONE! 
-- Postgres doesn't let alter types run with other DDL commands
-- OR at least ECD is too lazy to figure out how

 -- ALTER TYPE public.linkedobjectroleschema
 --     ADD VALUE 'HumanHuman';






CREATE SEQUENCE public.humanhuman_linkid_seq
  INCREMENT 1
  MINVALUE 100
  MAXVALUE 9223372036854775807
  START 100
  CACHE 1;


CREATE TABLE IF NOT EXISTS public.humanhuman
(
    linkid integer NOT NULL DEFAULT nextval('humanhuman_linkid_seq'::regclass),
    human_humanid integer,
    humantarget_humanid integer,
    source_sourceid integer,
    createdts timestamp with time zone DEFAULT now(),
    createdby_userid integer,
    lastupdatedts timestamp with time zone DEFAULT now(),
    lastupdatedby_userid integer,
    deactivatedts timestamp with time zone,
    deactivatedby_userid integer,
    notes text COLLATE pg_catalog."default",
    linkedobjectrole_lorid integer,
    CONSTRAINT humanhuman_pkey PRIMARY KEY (linkid),
    
    CONSTRAINT human_human_fk FOREIGN KEY (human_humanid)
        REFERENCES public.human (humanid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT humantarget_human_fk FOREIGN KEY (humantarget_humanid)
        REFERENCES public.human (humanid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT human_sourceid_fk FOREIGN KEY (source_sourceid)
        REFERENCES public.bobsource (sourceid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT humanhuman_createdby_userid_fk FOREIGN KEY (createdby_userid)
        REFERENCES public.login (userid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT humanhuman_deactivatedby_userid_fk FOREIGN KEY (deactivatedby_userid)
        REFERENCES public.login (userid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT humanhuman_humanid_fk FOREIGN KEY (human_humanid)
        REFERENCES public.human (humanid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT humanhuman_lastupdatdby_userid_fk FOREIGN KEY (lastupdatedby_userid)
        REFERENCES public.login (userid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);


ALTER TABLE human DROP COLUMN IF EXISTS aliasof_humanid;

CREATE SEQUENCE public.humanalias_aliasid_seq
  INCREMENT 1
  MINVALUE 100
  MAXVALUE 9223372036854775807
  START 100
  CACHE 1;



CREATE TYPE humanaliasrole AS ENUM ('CountyOfficialName','SpellingVariant','NickName','GeneralAlias');


CREATE TABLE IF NOT EXISTS public.humanalias 
(
	aliasid 			INTEGER PRIMARY KEY DEFAULT nextval('humanalias_aliasid_seq'::regclass),
	human_humanid 		INTEGER NOT NULL CONSTRAINT humanalias_humanid_fk REFERENCES human (humanid),
	aliasname 				TEXT NOT NULL,
	aliasrole 			humanaliasrole NOT NULL,
	source_sourceid 	integer,
    createdts 			timestamp with time zone DEFAULT now(),
    createdby_userid 	integer,
    lastupdatedts 		timestamp with time zone DEFAULT now(),
    lastupdatedby_userid integer,
    deactivatedts 			timestamp with time zone,
    deactivatedby_userid integer,
    notes text,
    CONSTRAINT humanalias_sourceid_fk FOREIGN KEY (source_sourceid)
        REFERENCES public.bobsource (sourceid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT humanalias_createdby_userid_fk FOREIGN KEY (createdby_userid)
        REFERENCES public.login (userid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT humanalias_deactivatedby_userid_fk FOREIGN KEY (deactivatedby_userid)
        REFERENCES public.login (userid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT humanalias_lastupdatdby_userid_fk FOREIGN KEY (lastupdatedby_userid)
        REFERENCES public.login (userid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION


);



CREATE SEQUENCE public.humanmerge_mergid_seq
  INCREMENT 1
  MINVALUE 100
  MAXVALUE 9223372036854775807
  START 100
  CACHE 1;


CREATE TABLE IF NOT EXISTS public.humanmerge
(
	mergeid 						INTEGER PRIMARY KEY DEFAULT nextval('humanmerge_mergid_seq'::regclass),
	primaryhuman_humanid 			INTEGER NOT NULL CONSTRAINT primary_humanid_fk REFERENCES human (humanid),
	subordinatehuman_humanid 		INTEGER NOT NULL CONSTRAINT subordinate_humanid_fk REFERENCES human (humanid),
    mergets 						timestamp with time zone NOT NULL DEFAULT now(),
    merger_userid 					INTEGER NOT NULL CONSTRAINT humanalias_merginguser_userid_fk REFERENCES login (userid),
    notes 							text,
    mergelog 						text NOT NULL

    
);

-- sylvia needs UMAPS

INSERT INTO public.loginmuniauthperiod(
    muniauthperiodid, muni_municode, authuser_userid, accessgranteddatestart, accessgranteddatestop, recorddeactivatedts, authorizedrole, createdts, createdby_userid, notes, supportassignedby, assignmentrank, oathts, oathcourtentity_entityid, oathdoc_blobid, codeofficer)
    VALUES (90, 999, 99, '01-01-1970', '01-01-2100', NULL, 'MuniStaff'::role, now(), 99, 'Robot user UMAP for cogstaff rank', NULL, 0, NULL, NULL, NULL, FALSE);

INSERT INTO public.loginmuniauthperiod(
    muniauthperiodid, muni_municode, authuser_userid, accessgranteddatestart, accessgranteddatestop, recorddeactivatedts, authorizedrole, createdts, createdby_userid, notes, supportassignedby, assignmentrank, oathts, oathcourtentity_entityid, oathdoc_blobid, codeofficer)
    VALUES (91, 999, 99, '01-01-1970', '01-01-2100', NULL, 'MuniManager'::role, now(), 99, 'Robot user UMAP for maanger ceo rank', NULL, 0, '01-01-1970', 101, NULL, TRUE);

INSERT INTO public.loginmuniauthperiod(
    muniauthperiodid, muni_municode, authuser_userid, accessgranteddatestart, accessgranteddatestop, recorddeactivatedts, authorizedrole, createdts, createdby_userid, notes, supportassignedby, assignmentrank, oathts, oathcourtentity_entityid, oathdoc_blobid, codeofficer)
    VALUES (92, 999, 99, '01-01-1970', '01-01-2100', NULL, 'SysAdmin'::role, now(), 99, 'Robot user UMAP for systemadmin rank', NULL, 0, '01-01-1970', 101, NULL, FALSE);


ALTER TABLE public.humanmerge ADD COLUMN outcomesuccessts TIMESTAMP WITH TIME ZONE;
ALTER TABLE public.humanmerge ADD COLUMN outcomefatalfailurets TIMESTAMP WITH TIME ZONE;

-- ********************************************************
-- RUN ALTER TYPE ALONE
-- ********************************************************
-- ALTER TYPE humanaliasrole ADD VALUE 'MergedSubordianteName';


-- LOCAL CURSOR
-- REMOTE CURSOR

INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (64, 'database/patches/dbpatch_beta64.sql', '11-APR-2024', 'ecd', 'person-person linking and person collapsing');


