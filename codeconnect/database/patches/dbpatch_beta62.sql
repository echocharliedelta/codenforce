
-- PATCH 62: Mailing addresses linkable to units and Event overhaul 
-- Lots of cleanup of deprecated tables


-- Allowing linking of parcel units to mailing addresses

CREATE SEQUENCE IF NOT EXISTS parcelunitmailingaddress_linkid_seq
    START WITH 2000
    INCREMENT BY 1
    MINVALUE 2000
    NO MAXVALUE
    CACHE 1;



CREATE TABLE IF NOT EXISTS public.parcelunitmailingaddress
(
    linkid integer NOT NULL DEFAULT nextval('parcelunitmailingaddress_linkid_seq'::regclass),
    parcelunit_unitid integer,
    mailingaddress_addressid integer,
    source_sourceid integer,
    createdts timestamp with time zone DEFAULT now(),
    createdby_userid integer,
    lastupdatedts timestamp with time zone DEFAULT now(),
    lastupdatedby_userid integer,
    deactivatedts timestamp with time zone,
    deactivatedby_userid integer,
    notes text COLLATE pg_catalog."default",
    linkedobjectrole_lorid integer,
    priority integer DEFAULT 1,
    
    CONSTRAINT parcelunitmailingaddress_linkid_pk PRIMARY KEY (linkid),

    CONSTRAINT parcelunitmailingaddress_createdby_userid_fk FOREIGN KEY (createdby_userid)
        REFERENCES public.login (userid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT parcelunitmailingaddress_deactivatedby_userid_fk FOREIGN KEY (deactivatedby_userid)
        REFERENCES public.login (userid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT parcelunitmailingaddress_lastupdatdby_userid_fk FOREIGN KEY (lastupdatedby_userid)
        REFERENCES public.login (userid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT parcelunitmailingaddress_parcelunitid_fk FOREIGN KEY (parcelunit_unitid)
        REFERENCES public.parcelunit (unitid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT parcelunitmailingaddress_sourceid_fk FOREIGN KEY (source_sourceid)
        REFERENCES public.bobsource (sourceid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT parcelunitmailingaddress_lorid_fk FOREIGN KEY (linkedobjectrole_lorid)
        REFERENCES public.linkedobjectrole (lorid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT parcelunitmailingaddress_mailingaddressid_fk FOREIGN KEY (mailingaddress_addressid)
        REFERENCES public.mailingaddress (addressid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)




-- uncomment and RUN ALONE! 
-- Postgres doesn't let alter types run with other DDL commands

-- ALTER TYPE public.linkedobjectroleschema
--     ADD VALUE 'ParcelUnitMailingAddress' AFTER 'CEACtionRequestHuman';

DROP TABLE public.eventemission;
DROP SEQUENCE eventemission_emissionid_seq;

DROP TABLE public.listchangerequest





ALTER TABLE public.eventcategory DROP COLUMN   userrankminimumtoenact;
ALTER TABLE public.eventcategory DROP COLUMN   userrankminimumtoview;
ALTER TABLE public.eventcategory DROP COLUMN   userrankminimumtoupdate;


ALTER TABLE public.event DROP COLUMN adminevent;

ALTER TABLE public.eventcategory ADD COLUMN alertevent BOOLEAN DEFAULT FALSE;
ALTER TABLE public.eventcategory ADD COLUMN alertevent_stopcategoryid INTEGER CONSTRAINT eventcategory_alerteventsstopcatid_fk REFERENCES eventcategory (categoryid);

ALTER TABLE public.eventcategory ADD COLUMN followuprequired BOOLEAN DEFAULT FALSE;
ALTER TABLE public.eventcategory ADD COLUMN adminevent BOOLEAN DEFAULT FALSE;

DROP TABLE citationevent;

CREATE SEQUENCE IF NOT EXISTS eventlinkage_linkid_seq
    START WITH 2100
    INCREMENT BY 1
    MINVALUE 2100
    NO MAXVALUE
    CACHE 1;



CREATE TABLE IF NOT EXISTS public.eventlinkage
(
    linkid                          INTEGER NOT NULL DEFAULT nextval('eventlinkage_linkid_seq'::regclass),
    event_eventid                   INTEGER NOT NULL CONSTRAINT eventlinkage_eventid_fk REFERENCES event (eventid),
    eventlink_eventid               INTEGER CONSTRAINT eventlinkage_eventlink_eventid_fk REFERENCES event (eventid),
    codeviolation_violationid       INTEGER CONSTRAINT eventlinkage_codeviolation_fk REFERENCES codeviolation(violationid),
    noticeofviolation_noticeid      INTEGER CONSTRAINT eventlinkage_novid_fk REFERENCES noticeofviolation (noticeid),
    citation_citationid             INTEGER CONSTRAINT eventlinkage_citationid_fk REFERENCES citation (citationid),
    citationstatus_citstatid        INTEGER CONSTRAINT eventlinkage_citationstatus_fk REFERENCES citationcitationstatus (citationstatusid),
    inspection_inspectionid         INTEGER CONSTRAINT eventlinkage_occinspection_fk REFERENCES occinspection (inspectionid),
    occpermit_permitid              INTEGER CONSTRAINT eventlinkage_occpermit_fk REFERENCES occpermit (permitid)
);

ALTER TABLE public.eventlinkage ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE DEFAULT NULL;


ALTER TABLE public.event DROP COLUMN followupevent_eventid;


ALTER TABLE public.municipalityoneclickcase ADD COLUMN displayorder INTEGER DEFAULT 0;

ALTER TABLE public.occinspection ADD COLUMN fieldinitroutingts TIMESTAMP WITH TIME ZONE;
ALTER TABLE public.occinspection ADD COLUMN fieldinitroutingby_userid INTEGER CONSTRAINT occinspection_fieldroutingby_useridfk REFERENCES login (userid);

ALTER TABLE public.codeelement ADD COLUMN headerstringstatic TEXT;

ALTER TABLE public.occchecklist RENAME COLUMN lastupdated_umapid TO lastupdatedby_umapid;

-- upgrading occchecklist to umaptracking, so old inactives need a deactivatedts and deactivatedby umap_id
UPDATE public.occchecklist SET deactivatedts=now(), deactivatedby_umapid=98 WHERE active=FALSE;
ALTER TABLE public.occchecklist DROP COLUMN active;
-- LOCAL RUN CURSOR
-- REMOTE RUN CURSOR


INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (62, 'database/patches/dbpatch_beta62.sql', '15-DEC-2023', 'ecd', 'event overhaul; bits handheld software tool updates');


