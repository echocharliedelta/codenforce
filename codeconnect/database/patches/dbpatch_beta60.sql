
-- These adjustments came from the handheld app specificiations


ALTER TABLE occchecklist ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE DEFAULT now();

-- no records are in occchecklistphotorequirement consider dropping

CREATE TABLE public.municipalitycodesource
(
	muni_municode 	INTEGER CONSTRAINT municipalitycodesource_municode_fk REFERENCES municipality (municode),
	codesource_sourceid INTEGER CONSTRAINT municipalitycodesource_sourcid_fk REFERENCES codesource (sourceid),
	CONSTRAINT municipalitycodesource_pk PRIMARY KEY (muni_municode, codesource_sourceid)
);


-- optional removal of this deprecated field; replaced by municipalitycodesource
ALTER TABLE public.municipality DROP COLUMN occpermitissuingsource_sourceid;

ALTER TABLE public.occinspection ADD COLUMN mobilerawpropertystring TEXT;
ALTER TABLE public.occinspection ADD COLUMN mobilerawpropertyunitstring TEXT;


-- Need lastupdatedts intensity classes, checklist, occchecklistspacetype,, codeelementguide
ALTER TABLE public.intensityclass ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE;


-- bring occchecklist into modern tracking fields by TS and umap FK (user in the login table is a legacy tracking strategy)
ALTER TABLE public.occchecklist ADD COLUMN createdby_umapid INTEGER 
CONSTRAINT occhecklist_createdbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE public.occchecklist ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE public.occchecklist ADD COLUMN lastupdated_umapid INTEGER 
	CONSTRAINT occhecklist_lastupdatedumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE public.occchecklist ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE public.occchecklist ADD COLUMN deactivatedby_umapid INTEGER
    CONSTRAINT occchecklist_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

-- bring spacetype into partial tracking. Since this is a linking table, we wont' do all the umap FKs
ALTER TABLE public.occchecklistspacetype ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE;


-- Standard fields

ALTER TABLE codeelementguide ADD COLUMN createdts TIMESTAMP WITH TIME ZONE;
ALTER TABLE codeelementguide ADD COLUMN createdby_umapid INTEGER
    CONSTRAINT codeelementguide_createdumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE codeelementguide ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE codeelementguide ADD COLUMN lastupdatedby_umapid INTEGER
    CONSTRAINT codeelementguide_lastupdatedumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE codeelementguide ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE codeelementguide ADD COLUMN deactivatedby_umapid INTEGER
    CONSTRAINT codeelementguide_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);



INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (60, 'database/patches/dbpatch_beta60.sql', '02-NOV-2023', 'ecd', 'Handheld app deltas and various Hillman 10 deltas');


