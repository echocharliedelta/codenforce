

-- Cross muni mode activation logging

CREATE SEQUENCE IF NOT EXISTS xmuniactivationid_seq 
    START WITH 1000
    INCREMENT BY 1
    MINVALUE 1000
    NO MAXVALUE
    CACHE 1;


CREATE TABLE public.xmuniactivation
(
	activationid INTEGER PRIMARY KEY DEFAULT nextval('xmuniactivationid_seq'),
	xmuni_municode INTEGER NOT NULL REFERENCES municipality (municode),
	humanconduit_humanid INTEGER NOT NULL REFERENCES human (humanid),
	linkrole_roleid INTEGER NOT NULL REFERENCES linkedobjectrole (lorid),
	parentkey INTEGER NOT NULL,
	activationts TIMESTAMP WITH TIME ZONE NOT NULL,
	activationby_umapid INTEGER NOT NULL
		CONSTRAINT xmuniactivation_createdumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid),
	exitts TIMESTAMP WITH TIME ZONE
);


INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (55, 'database/patches/dbpatch_beta55.sql', '19-JUN-2023', 'ecd', 'Xmuni tracking, et al');