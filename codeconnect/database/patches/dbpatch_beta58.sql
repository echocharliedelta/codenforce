
-- Finally, an overhaul of the convoluted letter types and text blocks. Basically detach NOVs from text block types entirely


ALTER TABLE noticeofviolation DROP COLUMN printstyle_styleid;

ALTER TABLE noticeofviolationtype DROP COLUMN textblockcategory_catid;




CREATE SEQUENCE IF NOT EXISTS noticeofviolationtemplate_seq 
    START WITH 10
    INCREMENT BY 1
    MINVALUE 10
    NO MAXVALUE
    CACHE 1;



CREATE TABLE public.noticeofviolationtemplate
(
	templateid 			INTEGER PRIMARY KEY DEFAULT nextval('noticeofviolationtemplate_seq'),
	templatetype_typeid INTEGER CONSTRAINT noticeofviolationtemplate_typeid_fk REFERENCES noticeofviolationtype (novtypeid),
	title				TEXT NOT NULL,
	muni_municode 		INTEGER CONSTRAINT noticeofviolationtemplate_municode_fk REFERENCES municipality (municode),
	templatetext 		TEXT NOT NULL,
	deactivatedts 		TIMESTAMP WITH TIME ZONE
);

ALTER TABLE public.noticeofviolation ADD COLUMN lettertemplate_templateid INTEGER CONSTRAINT noticeofviolation_templateid_fk REFERENCES public.noticeofviolationtemplate (templateid);

-- THis is to reverse my deletion of the muni in NOV type
-- did not run on remote
ALTER TABLE public.noticeofviolationtype ADD COLUMN muni_municode INTEGER CONSTRAINT noticeofviolationtype_municode_fk REFERENCES municipality (municode);





INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (58, 'database/patches/dbpatch_beta58.sql', '05-OCT-2023', 'ecd', 'simplification of the letter types and templates, at long last');