-- case priority upgrades


ALTER TYPE citationviolationstatus ADD VALUE 'UNKNOWN';

UPDATE public.citationviolation SET status = 'UNKOWN'::citationviolationstatus WHERE status IS NULL;



ALTER TABLE public.noticeofviolation ADD COLUMN actiondueby TIMESTAMP WITH TIME ZONE;

ALTER TABLE public.muniprofile ADD COLUMN priorityparamcaseopeningadminbufferdays DEFAULT 3;

-- cecase priority DB fields for 3.1.x
ALTER TABLE public.cecase ADD COLUMN actionduebydate TIMESTAMP WITH TIME ZONE;

CREATE TYPE cecaseactiondatesource AS ENUM ('LETTER','VIOLATION','CITATION','EVENT', 'CUSTOM');
CREATE TYPE cecaseactiondateurgency AS ENUM ('TIME_CRITICAL','ADVISORY');

ALTER TABLE public.cecase ADD COLUMN actionduebysource cecaseactiondatesource;
ALTER TABLE public.cecase ADD COLUMN actionduebyurgency cecaseactiondateurgency;
ALTER TABLE public.cecase ADD COLUMN actionduebydescription TEXT;
ALTER TABLE public.cecase ADD COLUMN actionduebyrelatedsubobject INTEGER;
ALTER TABLE public.cecase ADD COLUMN actionduebylog TEXT;
ALTER TABLE public.cecase ADD COLUMN actionduebylastupdatets TIMESTAMP WITH TIME ZONE;

ALTER TABLE public.cecase ADD COLUMN casestatus XML;

CREATE SEQUENCE public.citationpenalty_penaltyid_seq
  INCREMENT 1
  MINVALUE 1000
  MAXVALUE 9223372036854775807
  START 1000
  CACHE 1;

CREATE TYPE penaltytype AS ENUM ('COURTFEECOST','FINEOFFICERREQUESTED', 'FINEDISTRICTCOURTIMPOSED','FINEAPPEALIMPOSED','FINEJAILDAYS','OTHER');


CREATE TABLE IF NOT EXISTS public.citationpenalty
(
    penaltyid integer PRIMARY KEY DEFAULT nextval('citationpenalty_penaltyid_seq'::regclass),
    penaltytype penaltytype NOT NULL,
    penaltyamount numeric,
    jaildays integer,
    notes text,
    dateofrecord TIMESTAMP WITH TIME ZONE,
    citation_citationid integer CONSTRAINT citationpenalty_citationid_fk REFERENCES citation (citationid),
    citationviolation_citvid integer CONSTRAINT citationviolation_citvid_fk REFERENCES citationviolation (citationviolationid),
    createdts 			timestamp with time zone DEFAULT now(),
    createdby_userid 	integer,
    lastupdatedts 		timestamp with time zone DEFAULT now(),
    lastupdatedby_userid integer,
    deactivatedts 			timestamp with time zone,
    deactivatedby_userid integer
);


ALTER TABLE public.citationstatus ADD COLUMN batchupdatecitationviolations citationviolationstatus;

-- let's not use these
-- ALTER TABLE public.codeviolation ADD COLUMN adjudicatedts TIMESTAMP WITH TIME ZONE;
-- ALTER TABLE public.codeviolation ADD COLUMN adjudicatedby_userid INTEGER CONSTRAINT codeviolation_adjudicatedby_userid_fk REFERENCES login (userid);

ALTER TABLE public.citationstatus ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;

ALTER TABLE public.citationstatus ADD COLUMN appellate BOOLEAN;
ALTER TYPE citationviolationstatus ADD VALUE 'APPEALED';
ALTER TYPE citationviolationstatus ADD VALUE 'WITHDRAWN_COMPLIANCE';
-- LOCAL CURSOR
-- REMOTE CURSOR



INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (69, 'database/patches/dbpatch_beta69.sql', '18-NOV-2024', 'ecd', '');
