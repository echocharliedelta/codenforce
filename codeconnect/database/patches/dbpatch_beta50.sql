
ALTER TABLE public.occchecklist ADD COLUMN notes TEXT;

ALTER TABLE public.mailingaddress DROP COLUMN mailingunit_unitid;
DROP TABLE mailingaddressunit;

ALTER TABLE public.mailingaddress ADD COLUMN usesecondary BOOLEAN DEFAULT FALSE;

ALTER TABLE public.mailingaddress ADD COLUMN usepobox BOOLEAN DEFAULT FALSE;
ALTER TABLE public.mailingaddress ADD COLUMN poboxnum INTEGER; 

ALTER TABLE public.mailingaddress ADD COLUMN useattention BOOLEAN DEFAULT FALSE;


ALTER TABLE public.mailingaddress ADD COLUMN usebuildingno BOOLEAN DEFAULT TRUE;

UPDATE public.mailingaddress SET usebuildingno = TRUE;

INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (50, 'database/patches/dbpatch_beta50.sql', '04-MAY-2023', 'ecd', 'Hillman 5 deltas');