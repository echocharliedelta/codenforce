ALTER TABLE muniprofile ADD COLUMN autoloadopencases boolean DEFAULT TRUE;
ALTER TABLE muniprofile ADD COLUMN autoloadopenoccperiods boolean DEFAULT TRUE;
UPDATE muniprofile set autoloadopencases = TRUE;
UPDATE muniprofile set autoloadopenoccperiods = FALSE;

INSERT INTO public.linkedobjectrole(
	lorid, lorschema, title, description, createdts, deactivatedts, notes, createdby_umapid, deactivatedby_umapid, lastupdatedts, lastupdatedby_umapid)
	VALUES (301, 'CECaseHuman'::linkedobjectroleschema, 'NOV Recipient', 'Link extracted directly from NOV', now(), NULL, NULL, 1040, NULL, now(), 1040);

ALTER TABLE photodoc ADD COLUMN bobsource_sourceid INTEGER CONSTRAINT photodoc_bobsource_fk REFERENCES bobsource (sourceid);

ALTER TABLE blobbytes ADD COLUMN bobsource_sourceid INTEGER CONSTRAINT blobbytes_bobsource_fk REFERENCES bobsource (sourceid);

-- PATCH ITEMS for county data sync late JULY 2023

ALTER TABLE event ADD COLUMN adminevent BOOLEAN DEFAULT FALSE;

ALTER TABLE IF EXISTS public.human
    ADD COLUMN aliasof_humanid integer;

ALTER TABLE IF EXISTS public.human
    ADD CONSTRAINT human_aliasof_fk FOREIGN KEY (aliasof_humanid)
    REFERENCES public.human (humanid) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

ALTER TABLE occinspectiondetermination ADD COLUMN muni_municode INTEGER CONSTRAINT occinspectiondetermination_municode_fk REFERENCES municipality (municode);
ALTER TABLE occinspectioncause ADD COLUMN muni_municode INTEGER CONSTRAINT occinspectioncause_municode_fk REFERENCES municipality (municode);

ALTER TABLE linkedobjectrole ADD COLUMN sortorder INTEGER;
-- since sort will prioritize lower numbers first, set all LORs to a high value to appear lower on sorted lists
UPDATE linkedobjetrole SET sortorder = 50;


INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (56, 'database/patches/dbpatch_beta56.sql', '8-AUG-2023', 'ecd', 'tweak to muni profile- autload cecases and permit files switch');