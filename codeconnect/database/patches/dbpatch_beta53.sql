
ALTER TABLE ceactionrequest DROP COLUMN requestorupdatesrequested;

ALTER TABLE ceactionrequest DROP COLUMN requestor_humanid;


-- **************************************************
-- PATCH BITS_005_TASK_7
-- Build Icon Management Page
-- Description: Remove fontawesome column from table
-- **************************************************


ALTER TABLE icon DROP COLUMN fontawsome;
SELECT setval('iconid_seq', (SELECT MAX(iconid) FROM icon));


INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (53, 'database/patches/dbpatch_beta53.sql', '01-JUN-2023', 'ecd', 'Hillman 5-6 deltas');