-- Patch for work on linking and permissions: hillman 3
-- NOTE this ALTER TYPE NEEDS TO BE RUN MANUALLY, BY ITSELF

-- ********************************notice********************************
-- (Uncomment) and RUN THIS alter type BY ITSELF
-- ALTER TYPE linkedobjectroleschema ADD VALUE 'MuniHuman';
-- **********************************************************************

ALTER TABLE public.occchecklistspacetypeelement ADD COLUMN createdts TIMESTAMP WITH TIME ZONE;
ALTER TABLE public.occchecklistspacetypeelement ADD COLUMN createdby_umapid INTEGER 
	CONSTRAINT occchecklistspacetypeelement_createdumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE public.occchecklistspacetypeelement ADD COLUMN lastudpatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE public.occchecklistspacetypeelement ADD COLUMN lastupdatedby_umapid INTEGER 
	CONSTRAINT occchecklistspacetypeelement_lastupdatedumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE public.occchecklistspacetypeelement ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE public.occchecklistspacetypeelement ADD COLUMN deactivatedby_umapid INTEGER 
	CONSTRAINT occchecklistspacetypeelement_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);



ALTER TABLE public.occchecklistspacetype ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;


-- this drop wont run on production due to dependencies; ECD leaving for later
-- ALTER TABLE public.cecase DROP COLUMN property_propertyid;
ALTER TABLE public.cecase DROP COLUMN propertyinfocase;
ALTER TABLE public.cecase DROP COLUMN personinfocase_personid;
ALTER TABLE public.cecase DROP COLUMN active;

/*
EXTRA cleanup stuff for production disaster:

begin;
DELETE from cecase where propertyinfocase = true;
rollback;
COMMIT;



begin;
DELETE FROM public.event WHERE eventid IN (SELECT eventid FROM public.event LEFT OUTER JOIN public.cecase ON (event.cecase_caseid = cecase.caseid) 
WHERE cecase.propertyinfocase = TRUE);
COMMIT;
*/




DROP TABLE IF EXISTS citationdocketnohuman;

CREATE TABLE IF NOT EXISTS public.citationdocketnohuman
(
    linkid integer NOT NULL DEFAULT nextval('citationdockethuman_linkid_seq'::regclass),
    human_humanid integer,
    docketno_docketid integer,
    source_sourceid integer,
    createdts timestamp with time zone,
    createdby_userid integer,
    lastupdatedts timestamp with time zone,
    lastupdatedby_userid integer,
    deactivatedts timestamp with time zone,
    deactivatedby_userid integer,
    notes text COLLATE pg_catalog."default",
    linkedobjectrole_lorid integer,
    CONSTRAINT citationdockethuman_pkey PRIMARY KEY (linkid),
    CONSTRAINT citationdockethuman_lorid_fk FOREIGN KEY (linkedobjectrole_lorid)
        REFERENCES public.linkedobjectrole (lorid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT citationdockethuman_docket_fk FOREIGN KEY (docketno_docketid)
        REFERENCES public.citationdocketno (docketid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT citationdockethuman_createdby_userid_fk FOREIGN KEY (createdby_userid)
        REFERENCES public.login (userid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT citationdockethuman_deactivatedby_userid_fk FOREIGN KEY (deactivatedby_userid)
        REFERENCES public.login (userid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT citationdockethuman_humanid_fk FOREIGN KEY (human_humanid)
        REFERENCES public.human (humanid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT citationdockethuman_lastupdatdby_userid_fk FOREIGN KEY (lastupdatedby_userid)
        REFERENCES public.login (userid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT citationdockethuman_sourceid_fk FOREIGN KEY (source_sourceid)
        REFERENCES public.bobsource (sourceid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);


INSERT INTO public.linkedobjectrole(
	lorid, lorschema, title, description, createdts, deactivatedts, notes)
	VALUES (DEFAULT, 'MuniHuman'::linkedobjectroleschema, 'Muni Manager', 'The boss at the muni', now(), NULL, NULL);


INSERT INTO public.linkedobjectrole(
	lorid, lorschema, title, description, createdts, deactivatedts, notes)
	VALUES (DEFAULT, 'MuniHuman'::linkedobjectroleschema, 'Muni Staff', 'Employee of the muni', now(), NULL, NULL);


INSERT INTO public.linkedobjectrole(
	lorid, lorschema, title, description, createdts, deactivatedts, notes)
	VALUES (DEFAULT, 'MuniHuman'::linkedobjectroleschema, 'Former Staff', 'Former employee of the muni', now(), NULL, NULL);


INSERT INTO public.linkedobjectrole(
	lorid, lorschema, title, description, createdts, deactivatedts, notes)
	VALUES (DEFAULT, 'MuniHuman'::linkedobjectroleschema, 'Contractor', 'Independent contractor with muni', now(), NULL, NULL);

INSERT INTO public.linkedobjectrole(
	lorid, lorschema, title, description, createdts, deactivatedts, notes)
	VALUES (DEFAULT, 'MuniHuman'::linkedobjectroleschema, 'Current councilperson', 'Currently serving on the muni council', now(), NULL, NULL);

INSERT INTO public.linkedobjectrole(
	lorid, lorschema, title, description, createdts, deactivatedts, notes)
	VALUES (DEFAULT, 'MuniHuman'::linkedobjectroleschema, 'Former councilperson', 'Used to serve on the muni council', now(), NULL, NULL);


INSERT INTO public.linkedobjectrole(
	lorid, lorschema, title, description, createdts, deactivatedts, notes)
	VALUES (DEFAULT, 'CitationDocketHuman'::linkedobjectroleschema, 'Defendant', 'Single defendant on docket', now(), NULL, NULL);

INSERT INTO public.linkedobjectrole(
	lorid, lorschema, title, description, createdts, deactivatedts, notes)
	VALUES (DEFAULT, 'CitationDocketHuman'::linkedobjectroleschema, 'Co-Defendant', 'Co-defendant on docket', now(), NULL, NULL);




CREATE SEQUENCE IF NOT EXISTS mailingaddressunitid_seq 
    START WITH 1000
    INCREMENT BY 1
    MINVALUE 1000
    NO MAXVALUE
    CACHE 1;


CREATE TABLE public.mailingaddressunit
(
	mailingunitid INTEGER PRIMARY KEY DEFAULT nextval('mailingaddressunitid_seq'),
	unitnumber TEXT NOT NULL,
	mailingaddress_addressid INTEGER NOT NULL
		CONSTRAINT mailingaddressunit_mailingaddressid_fk REFERENCES mailingaddress (addressid),
	createdts TIMESTAMP WITH TIME ZONE,
	createdby_umapid INTEGER 
		CONSTRAINT mailingaddressunit_createdumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid),
	lastupdatedts TIMESTAMP WITH TIME ZONE,
	lastupdatedby_umapid INTEGER 
		CONSTRAINT mailingaddressunit_lastupdatedumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid),
	deactivatedts TIMESTAMP WITH TIME ZONE,
	deactivatedby_umapid INTEGER 
		CONSTRAINT mailingaddressunit_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid)


);

INSERT INTO public.blobtype(
	typeid, typetitle, icon_iconid, contenttypestring, browserviewable, notes, fileextensionsarr)
	VALUES (204, 'Microsoft Coporation proprietary Word application file', 18, 'application/doc', FALSE, NULL, '{doc}');

INSERT INTO public.blobtype(
	typeid, typetitle, icon_iconid, contenttypestring, browserviewable, notes, fileextensionsarr)
	VALUES (205, 'Microsoft Coporation proprietary Excel application file', 18, 'application/excel', FALSE, NULL, '{xls, xlsx}');

INSERT INTO public.blobtype(
	typeid, typetitle, icon_iconid, contenttypestring, browserviewable, notes, fileextensionsarr)
	VALUES (206, 'Raw text file; format unrestricted', 18, 'application/text', FALSE, NULL, '{txt}');

INSERT INTO public.blobtype(
	typeid, typetitle, icon_iconid, contenttypestring, browserviewable, notes, fileextensionsarr)
	VALUES (207, 'Comma separated value file; delimiter and structure unspecified', 18, 'application/csv', FALSE, NULL, '{csv}');



INSERT INTO public.blobtype(
	typeid, typetitle, icon_iconid, contenttypestring, browserviewable, notes, fileextensionsarr)
	VALUES (208, 'Open document spreadsheet', 18, 'application/ods', FALSE, NULL, '{ods}');


INSERT INTO public.blobtype(
	typeid, typetitle, icon_iconid, contenttypestring, browserviewable, notes, fileextensionsarr)
	VALUES (209, 'Open document text', 18, 'application/odt', FALSE, NULL, '{odt}');


ALTER TABLE mailingaddress ADD COLUMN mailingunit_unitid INTEGER
	CONSTRAINT mailingaddress_mailingunit_fk REFERENCES mailingaddressunit (mailingunitid);

-- LOCAL Cursor
-- REMOTE cursor



INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (48, 'database/patches/dbpatch_beta48.sql', '03-17-2023', 'ecd', 'Part of Hillman 2');