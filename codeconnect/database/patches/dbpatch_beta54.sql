
ALTER TABLE ceactionrequest DROP COLUMN coginternalnotes;

-- **************************************************
-- PATCH BITS_006_TASK_8
-- Inspection Workflow
-- Description: Design responsive interface for field inspection workflow
-- & additional inspection tweaks
-- **************************************************
ALTER TABLE occinspectedspace ADD COLUMN deactivatedby_userid INTEGER CONSTRAINT occinspectedspace_deactivatedbyuserid_fk REFERENCES login (userid);
ALTER TABLE occinspectedspace ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE occinspectedspaceelement ADD COLUMN deactivatedby_userid INTEGER CONSTRAINT occinspectedspaceelement_deactivatedbyuserid_fk REFERENCES login (userid);
ALTER TABLE occinspectedspaceelement ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;


INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (54, 'database/patches/dbpatch_beta54.sql', '12-JUN-2023', 'ecd', 'Hillman 6 deltas and BITS 0006_TASK8');