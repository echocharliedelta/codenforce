
-- REMOTE CURSOR

ALTER TABLE cecase ADD COLUMN createdts TIMESTAMP WITH TIME ZONE;
ALTER TABLE cecase ADD COLUMN createdby_umapid INTEGER 
	CONSTRAINT cecase_createdumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);


ALTER TABLE cecase ADD COLUMN lastupdatedby_umapid INTEGER 
	CONSTRAINT cecase_lastupdatedumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE cecase ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE cecase ADD COLUMN deactivatedby_umapid INTEGER 
	CONSTRAINT cecase_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);


UPDATE cecase SET deactivatedts=now() WHERE active = FALSE;
UPDATE cecase SET deactivatedby_umapid=1021 WHERE active = FALSE;


ALTER TABLE cecase DROP COLUMN lastupdatedby_userid;

ALTER TABLE cecase DROP COLUMN createdts;
ALTER TABLE cecase RENAME COLUMN creationtimestamp TO createdts;

ALTER TABLE codesource DROP COLUMN isactive;
-- LOCAL CURSOR
INSERT INTO public.eventcategory(
	categoryid, categorytype, title, 
	description, notifymonitors, hidable, 
	icon_iconid, relativeorderwithintype, relativeorderglobal, 
	hosteventdescriptionsuggtext, directive_directiveid, defaultdurationmins, 
	active, userrankminimumtoenact, userrankminimumtoview, 
	userrankminimumtoupdate, rolefloorenact, rolefloorview, 
	rolefloorupdate, prioritygreenbufferdays)
	VALUES (18100, 'CaseAdmin'::eventtype, 'Case Manager Change', 
		'Case manager updated', TRUE, TRUE, 
		23, 1, 1, 
		'Case Manager changed', NULL, 15, 
		TRUE, 3, 0, 
		3, 'MuniStaff'::role, 'MuniReader'::role, 
		'MuniStaff'::role, 0);




INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (47, 'database/patches/dbpatch_beta47.sql', '20-MAR-2023', 'ecd', 'CECase upgrade to trackedEntity');