
-- Case management upgrade patches MISC Nov 2024


ALTER TYPE eventtype ADD VALUE 'Casereopen';



ALTER TYPE citationviolationstatus ADD VALUE 'NOT_CITED';
ALTER TYPE citationviolationstatus ADD VALUE 'UNKNOWN_TERMINAL';
ALTER TYPE citationviolationstatus ADD VALUE 'HEARING_SCHEDULED';


CREATE TABLE IF NOT EXISTS public.codesetelementtextblock
(
    codesetelement_cseid INTEGER  REFERENCES public.codesetelement (codesetelementid),
    textblock_blockid INTEGER REFERENCES public.textblock (blockid),
    PRIMARY KEY (codesetelement_cseid,textblock_blockid)
);

CREATE INDEX idx_codesetelementtextblock_cseid
    ON public.codesetelementtextblock (codesetelement_cseid);

INSERT INTO public.textblockcategory(
	categoryid, categorytitle, icon_iconid, muni_municode, deactivatedts)
	VALUES (300, 'Canned violation findings', 10, 999, null);

ALTER TABLE IF EXISTS public.textblock
    ALTER COLUMN muni_municode DROP NOT NULL;


ALTER TABLE IF EXISTS public.codeviolation
    ALTER COLUMN description DROP NOT NULL;
-- remote cursor

-- Local cursor
-- leftover cleanup



CREATE TABLE IF NOT EXISTS public.parcelparcellink
(
    parent_parcelid     integer NOT NULL CONSTRAINT parcelparcellink_parentparcelid_fk REFERENCES parcel (parcelkey),
    child_parcelid      integer NOT NULL CONSTRAINT parcelparcellink_childparcelid_fk REFERENCES parcel (parcelkey),
    role_lorid          integer NOT NULL CONSTRAINT parcelparcellink_lorid_fk REFERENCES linkedobjectrole (lorid),   
    notes               text,
    createdts 			timestamp with time zone DEFAULT now(),
    createdby_userid 	integer,
    lastupdatedts 		timestamp with time zone DEFAULT now(),
    lastupdatedby_userid integer,
    deactivatedts 			timestamp with time zone,
    deactivatedby_userid integer,
    PRIMARY KEY (parent_parcelid, child_parcelid)
);

ALTER TYPE linkedobjectroleschema ADD VALUE 'ParcelParcel';

CREATE SEQUENCE public.parcelgroupid_seq
  INCREMENT 1
  MINVALUE 1000
  MAXVALUE 9223372036854775807
  START 1000
  CACHE 1;

CREATE TABLE IF NOT EXISTS public.parcelgroup
(
    parcelgroupid       INTEGER PRIMARY KEY DEFAULT nextval('parcelgroupid_seq'),
    groupname           TEXT NOT NULL,
    muni_municode       INTEGER CONSTRAINT parcelgroup_municodefk REFERENCES municipality (municode),
    notes               TEXT,
    createdts 			timestamp with time zone DEFAULT now(),
    createdby_userid 	integer,
    lastupdatedts 		timestamp with time zone DEFAULT now(),
    lastupdatedby_userid integer,
    deactivatedts 			timestamp with time zone,
    deactivatedby_userid integer
);


ALTER TABLE public.parcel ADD COLUMN parcelgroup_groupid INTEGER CONSTRAINT parcel_groupidfk REFERENCES parcelgroup (parcelgroupid);







ALTER TABLE public.citationstatus DROP COLUMN eventrule_ruleid;


INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (70, 'database/patches/dbpatch_beta70.sql', NULL, 'ecd', '');
