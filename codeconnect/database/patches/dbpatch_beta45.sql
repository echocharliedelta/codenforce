


-- messy street dupes work
ALTER TABLE mailingstreet ADD COLUMN stname TEXT;
UPDATE mailingstreet SET stname=name;

-- wipe out all scraped address data and their links
select * from parcelmailingaddress where parcel_parcelkey IN (SELECT parcelkey FROM parcel where muni_municode = 829);

-- I had forgotten this!!
ALTER TABLE humanmailingaddress 
	ADD CONSTRAINT humanmailing_mailingaddress_addrid 
	FOREIGN KEY (humanmailing_addressid )
	REFERENCES mailingaddress (addressid);

-- REMOTE CURSOR

ALTER TABLE municipality DROP COLUMN lastupdatedts;
ALTER TABLE municipality DROP COLUMN lastudpated_userid;

ALTER TABLE municipality ADD COLUMN createdts TIMESTAMP WITH TIME ZONE;
ALTER TABLE municipality ADD COLUMN createdby_umapid INTEGER 
	CONSTRAINT municipality_createdumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE municipality ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE municipality ADD COLUMN lastupdatedby_umapid INTEGER 
	CONSTRAINT municipality_lastupdatedumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE municipality ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE municipality ADD COLUMN deactivatedby_umapid INTEGER 
	CONSTRAINT municipality_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);



ALTER TABLE muniprofile DROP COLUMN lastupdatedts;
ALTER TABLE muniprofile DROP COLUMN lastudpatedby_userid;

ALTER TABLE muniprofile ADD COLUMN createdts TIMESTAMP WITH TIME ZONE;
ALTER TABLE muniprofile ADD COLUMN createdby_umapid INTEGER 
	CONSTRAINT muniprofile_createdumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE muniprofile ADD COLUMN lastupdatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE muniprofile ADD COLUMN lastupdatedby_umapid INTEGER 
	CONSTRAINT muniprofile_lastupdatedumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE muniprofile ADD COLUMN deactivatedts TIMESTAMP WITH TIME ZONE;
ALTER TABLE muniprofile ADD COLUMN deactivatedby_umapid INTEGER 
	CONSTRAINT muniprofile_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid);

ALTER TABLE IF EXISTS public.muniprofile DROP COLUMN IF EXISTS minimumuserranktodeclarerentalintent;
ALTER TABLE IF EXISTS public.muniprofile DROP COLUMN IF EXISTS minimumuserrankforinspectionoverrides;


ALTER TABLE public.municipality DROP COLUMN address_street;
ALTER TABLE public.municipality DROP COLUMN address_city;
ALTER TABLE public.municipality DROP COLUMN address_state;
ALTER TABLE public.municipality DROP COLUMN address_zip;
-- RUN these two as LOCAL ONLY
-- ALTER TABLE public.municipality DROP COLUMN lastupdatedby_userid;
-- ALTER TABLE public.municipality DROP COLUMN lastupdated_userid;

ALTER TABLE public.municipality DROP COLUMN phone;
ALTER TABLE public.municipality DROP COLUMN email;
ALTER TABLE public.municipality DROP COLUMN fax;

ALTER TABLE public.municipality ADD COLUMN contact_personid INTEGER
	CONSTRAINT muni_contactpersonid_fk REFERENCES human (humanid);

ALTER TABLE public.municipality  ADD COLUMN mailingaddress_addressid INTEGER
	CONSTRAINT muni_mailingaddressid_fk REFERENCES mailingaddress (addressid);

ALTER TABLE public.municipality ADD COLUMN populationyear integer;


CREATE SEQUENCE IF NOT EXISTS countyid_seq 
    START WITH 1000
    INCREMENT BY 1
    MINVALUE 1000
    NO MAXVALUE
    CACHE 1;


CREATE TABLE public.county
(
	countyid INTEGER PRIMARY KEY DEFAULT nextval('countyid_seq'),
	countyname TEXT NOT NULL,
	stateabrev TEXT NOT NULL,
	countyexternalcode TEXT

);

CREATE TABLE public.municipalitycounty
(
	county_countyid INTEGER NOT NULL,
	muni_municode INTEGER NOT NULL,
	CONSTRAINT munipalitycounty_pk PRIMARY KEY (county_countid, muni_municode)
);

ALTER TABLE public.county ADD COLUMN active boolean DEFAULT TRUE;

-- LOCAL CURSOR

ALTER TABLE public.printstyle DROP COLUMN headerimage_photodocid;

-- NOTE: not using municipality.contact_personid
-- primarystaffcontact_userid is the default code officer user


INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (45, 'database/patches/dbpatch_beta45.sql', null, 'ecd', 'third patch for 1.x.y');