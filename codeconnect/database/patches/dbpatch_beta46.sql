-- HILLMAN sprint #1

ALTER TABLE occpermit ADD COLUMN staticownersellerlabel TEXT;
ALTER TABLE occpermit ADD COLUMN staticbuyertenantlabel TEXT;

ALTER TABLE occpermit ADD COLUMN staticheader_photodocid INTEGER 
	CONSTRAINT occpermit_headerimageid_fk REFERENCES photodoc (photodocid);

ALTER TABLE occpermit ADD COLUMN staticheaderheightpx INTEGER;


-- REMOTE CURSOR
-- LOCAL CURSOR

INSERT INTO public.dbpatch(patchnum, patchfilename, datepublished, patchauthor, notes)
    VALUES (46, 'database/patches/dbpatch_beta46.sql', '17-FEB-2023', 'ecd', '4th patch for 1.x.y');