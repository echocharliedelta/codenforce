SELECT addressid, bldgno, mailingstreet.stname, default_city AS city, default_state AS state2char, zip_code AS zip
	FROM public.parcelmailingaddress 
	INNER JOIN public.linkedobjectrole ON (linkedobjectrole_lorid = lorid)
	INNER JOIN public.mailingaddress ON (mailingaddress_addressid = addressid)
	LEFT OUTER JOIN public.mailingstreet ON (street_streetid = streetid)
	LEFT OUTER JOIN public.mailingcitystatezip ON (citystatezip_cszipid = id)
	WHERE parcel_parcelkey = 130970
	ORDER BY linkedobjectrole.sortorder ASC, parcelmailingaddress.priority ASC
	LIMIT 1;
	