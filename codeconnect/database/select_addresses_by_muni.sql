SELECT bldgno, stname, parcelidcnty  from parcel 
	LEFT OUTER JOIN public.parcelmailingaddress ON parcelmailingaddress.parcel_parcelkey=parcel.parcelkey  
	LEFT OUTER JOIN public.mailingaddress ON mailingaddress.addressid=parcelmailingaddress.mailingaddress_addressid
	LEFT OUTER JOIN public.mailingstreet ON mailingaddress.street_streetid=mailingstreet.streetid
	WHERE muni_municode = 821 AND parcelmailingaddress.linkedobjectrole_lorid != 233
	ORDER BY stname, bldgno;