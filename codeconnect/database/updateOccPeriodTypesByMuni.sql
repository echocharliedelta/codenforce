-- sets the occ period type to a given value for all periods in a given muni

UPDATE public.occperiod 
SET periodtype_typeid = 1006
WHERE occperiod.periodid IN (
SELECT periodid FROM occperiod 
INNER JOIN parcelunit ON occperiod.parcelunit_unitid = parcelunit.unitid 
INNER JOIN parcel ON parcel.parcelkey = parcelunit.parcel_parcelkey 
WHERE muni_municode = 999);