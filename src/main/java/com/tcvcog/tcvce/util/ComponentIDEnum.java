/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.util;

/**
 * Holds the view IDs of important components for use in validators and fancy Backing Bean methods
 * @author pierre15
 */
public enum ComponentIDEnum {
    
    // PERMITTING
    PERMIT_DATE_COMPONENT_ID_APPLICATION(":permitconfigflow-step-2-dates-form:occpermit-dynamic-date-application-flow"),
    PERMIT_DATE_COMPONENT_ID_FIN_INIT(":permitconfigflow-step-2-dates-form:occpermit-dynamic-date-initial-flow"),
    PERMIT_DATE_COMPONENT_ID_FIN_REINSPECT(":permitconfigflow-step-2-dates-form:occpermit-dynamic-date-reinspection-flow"),
    PERMIT_DATE_COMPONENT_ID_FIN_FINAL(":permitconfigflow-step-2-dates-form:occpermit-dynamic-date-final-flow"),
    PERMIT_DATE_COMPONENT_ID_ISSUANCE(":permitconfigflow-step-2-dates-form:occpermit-dynamic-date-issuance-flow"),
    PERMIT_DATE_COMPONENT_ID_EXPIRY(  ":permitconfigflow-step-2-dates-form:occpermit-dynamic-date-expiry-flow"),
    PERMIT_DATE_COMPONENT_ID_VALIDATION_OVERRIDE(  ":permitconfigflow-step-2-dates-form:permit-date-override-sbcb"),
    
    // FIELD INSPECTIONS
    FIN_INIT_COMPONENT_ID_TIME_START("inspection-time-start-datepicker"),
    FIN_INIT_COMPONENT_ID_TIME_END("inspection-time-end-datepicker");
    

    private final String componentID;
    
    private ComponentIDEnum(String id){
        componentID = id;
    }

    /**
     * @return the componentID
     */
    public String getComponentID() {
        return componentID;
    }

    
}
