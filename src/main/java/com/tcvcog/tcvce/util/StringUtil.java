package com.tcvcog.tcvce.util;

import jakarta.validation.constraints.NotNull;

public class StringUtil {

    /**
     * To perform case-insensitive search
     * @param source
     * @param findText
     * @return true if source string contains findText string otherwise returns false
     */
    public static boolean isStringContainsI(@NotNull String source, @NotNull String findText) {
        return source.toLowerCase().contains(findText.trim().toLowerCase());
    }
}
