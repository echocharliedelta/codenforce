/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.util;

import com.tcvcog.tcvce.entities.occupancy.OccInspectedSpaceElement;
import java.util.Comparator;

/**
 *Sorts OISEs based on their inspection status, sorting violated, then passed, then not inspected
 * @author pierre15
 */
public class OISEComparator implements Comparator<OccInspectedSpaceElement>{

    @Override
    public int compare(OccInspectedSpaceElement oise1, OccInspectedSpaceElement oise2) {
        if(oise1 == null || oise2 == null || oise1.getStatusEnum() == null || oise2.getStatusEnum() == null){    
            return 0;
        }
        return Integer.valueOf(oise2.getStatusEnum().getOrder()).compareTo(oise1.getStatusEnum().getOrder());
    }
    
}
