/*
 * Copyright (C) 2018 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.util;

import com.tcvcog.tcvce.entities.HumanLink;
import com.tcvcog.tcvce.entities.Person;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.FacesConverter;

/**
 * Converter for Person objects
 * @author Eric Darsow
 */

@FacesConverter(value="humanLinkConverter")
public class HumanLinkConverter extends EntityConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String pName) {
        if(pName.isEmpty()) {
            return null; 
        }
        HumanLink hl = (HumanLink) this.getViewMap(fc).get(pName); 
        return hl;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        
        if (o == null){
            return "";
        }
        HumanLink hl = (HumanLink) o;
        StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(hl.getHumanID()));
        sb.append(String.valueOf(hl.getLinkID()));
        String sid = sb.toString();
        if (sid != null){
            this.getViewMap(fc).put(sid, o);
            return sid;
            
        } else {
            return "humanlink conversion error";
        }
        
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
