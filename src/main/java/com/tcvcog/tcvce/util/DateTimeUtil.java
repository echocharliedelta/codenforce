package com.tcvcog.tcvce.util;

import jakarta.faces.application.FacesMessage;
import jakarta.faces.component.UIComponent;
import jakarta.faces.validator.ValidatorException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import org.primefaces.component.datepicker.DatePicker;

public class DateTimeUtil {
    private final int DAYS_IN_YEAR = 365;

    
    
    /**
     * Utility for pulling the String out of the guts of a DatePicker 
     * and returning the LocalDateTime
     * @param component
     * @return null if parsing is impossible
     */
    public LocalDate extractLocalDateFromUIComponentString(UIComponent component){
        LocalDate ldate = null;
        if (component != null && component instanceof DatePicker) {
            DatePicker appDateCal = (DatePicker) component;
            try{
                if(appDateCal.getSubmittedValue() != null && !appDateCal.getSubmittedValue().equals("")){
                    ldate = LocalDate.parse((String) appDateCal.getSubmittedValue(), DateTimeFormatter.ofPattern("MM/dd/yyyy"));
                }
            } catch (DateTimeParseException ex){
                System.out.println("DateTimeUtils.extractLocalDateTimeFromUIComponentString | could not parse incoming Date Time String formatted MM/dd/yyyy HH:mm");
            }
        }
        return ldate;
    }
    
    /**
     * Utility for pulling the String out of the guts of a DatePicker 
     * and returning the LocalDateTime
     * @param component
     * @return null if parsing is impossible
     */
    public LocalTime extractLocalTimeFromUIComponentString(UIComponent component){
        LocalTime ltime = null;
        if (component != null && component instanceof DatePicker) {
            DatePicker appDateCal = (DatePicker) component;
            try{
                if(appDateCal.getSubmittedValue() != null && !appDateCal.getSubmittedValue().equals("")){
                    ltime = LocalTime.parse((String) appDateCal.getSubmittedValue(), DateTimeFormatter.ofPattern("HH:mm"));
                }
            } catch (DateTimeParseException ex){
                System.out.println("DateTimeUtils.extractLocalTimeFromUIComponentString | could not parse incoming Time String formatted HH:mm");
            }
        }
        return ltime;
    }
    
    /**
     * Converts a date from LocalDateTime to a string using DateTimeFormatter, returns empty string if
     * input is null.
     *
     * @param input
     * @return
     */
    public static String getPrettyDate(LocalDateTime input) {
        String formattedDateTime = "";
        if(input != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE dd MMM yyyy, HH:mm");
            formattedDateTime = input.format(formatter);
        }
        return formattedDateTime;
    }
    
    /**
     * Experimental adaptor to avoid a getXXX method that isn't really a normal getter
     * @param input
     * @return 
     */
    public static String generatePrettyLocalDateTime(LocalDateTime input){
        return getPrettyDate(input);
        
    }
    
    /**
     * Builds a string of just HH:mm out of a LocalDateTime
     * @param input can be null
     * @return surely an empty string, and perhaps one of the input's the HH:mm
     */
    public static String getPrettyTimeOnly(LocalDateTime input) {
        String formattedDateTime = "";
        if(input != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
            formattedDateTime = input.format(formatter);
        }
        return formattedDateTime;
    }
    
    /**
     * Utility for converting an object LocalDate into a pretty String
     * @param t
     * @return 
     */
    public static String getPrettyTime(LocalTime t){
        String formattedTime = "";
        if(t != null){
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
            formattedTime = t.format(formatter);
        }
        
        return formattedTime;
    }
    
    
    /**
     * Utility for converting an object LocalDate into a pretty String
     * @param ld
     * @return 
     */
    public static String getPrettyLocalDate(LocalDate ld){
        String formattedTime = "";
        if(ld != null){
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE dd MMM yyyy");
            formattedTime = ld.format(formatter);
        }
        
        return formattedTime;
    }
    
    /**
     * Converts a local date to a no time string. 
     * @param input if null, empty string returned
     * @return String representing a date only
     */
    public static String getPrettyLocalDateNoTime(LocalDate input){
        String formattedDateTime = "";
        if(input != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE dd MMM yyyy");
            formattedDateTime = input.format(formatter);
        }
        return formattedDateTime;
    }

    /**
     * Converts a date from LocalDateTime to a string using DateTimeFormatter while omitting the time, returns null if
     * input is null.
     *
     * @param input
     * @return
     */
    public static String getPrettyDateNoTime(LocalDateTime input) {
        String formattedDateTime = "";
        if(input != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE dd MMM yyyy");
            formattedDateTime = input.format(formatter);
        }
        return formattedDateTime;
    }


   
    /**
     * Counts days between two LocalDateTimes and returns the primitive
     * @param from
     * @param to
     * @return
     */
    public static long getTimePeriodAsDays(LocalDateTime from, LocalDateTime to){
        if(from == null || to == null){
            return 0;
        }
        LocalDate dStart = from.toLocalDate();
        LocalDate dEnd = to.toLocalDate();
        long daysBetween = java.time.temporal.ChronoUnit.DAYS.between(dStart, dEnd);

//
//        days = java.time.Period.between(from.toLocalDate(), to.toLocalDate()).getDays();
//        java.time.Period.between(LocalDate.MIN, LocalDate.MAX)
//        years = java.time.Period.between(from.toLocalDate(), to.toLocalDate()).getYears();
//        totalDays = days + (years * DAYS_IN_YEAR);
        return daysBetween ;
    }
    
    // converters
    
    
    
}
