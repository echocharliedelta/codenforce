/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.session;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.coordinators.OccInspectionCoordinator;
import com.tcvcog.tcvce.coordinators.OccupancyCoordinator;
import com.tcvcog.tcvce.coordinators.SearchCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.domain.SearchException;
import com.tcvcog.tcvce.entities.IFace_inspectable;
import com.tcvcog.tcvce.entities.occupancy.FieldInspection;
import com.tcvcog.tcvce.entities.occupancy.FieldInspectionLight;
import com.tcvcog.tcvce.entities.occupancy.OccPeriodDataHeavy;
import com.tcvcog.tcvce.entities.search.QueryFieldInspection;
import com.tcvcog.tcvce.entities.search.QueryFieldInspectionEnum;
import com.tcvcog.tcvce.entities.search.SearchParamsFieldInspection;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Home of session level inspection tools
 * to organized inter-subsystem operations on OccPeriod objects and their entire family
 * 
 * @author Ellen Bascomb (of Aparment 31Y)
 */
public class SessionInspectionConductor extends BackingBeanUtils{

    private FieldInspection sessFieldInspection;
    private IFace_inspectable sessInspectable;
    
    private List<FieldInspectionLight> sessFieldInspectionResultList;
    private List<FieldInspectionLight> sessFieldInspectionListForRefresh;
    
    
    private List<FieldInspectionLight> sessFieldInspectionsAwaitingRoutingList;
    private FieldInspectionLight sessFieldInspectionAwaitingRouting;
    private List<FieldInspectionLight> sessFieldInspectionsDispatched;
    private List<FieldInspectionLight> sessFieldInspectionsRecentlyUploaded;
    
    // SEARCH
    
    private QueryFieldInspection querySelected;
    private List<QueryFieldInspection> queryList;
    private SearchParamsFieldInspection searchParamsSelected;
    private boolean appendResultsToList;
    
    
    /**
     * Creates a new instance of SessionOccConductor
     */
    public SessionInspectionConductor() {
    }

    
    
    @PostConstruct
    public void initBean()  {
        System.out.println("SessionInspectionConductor.initBean");
        sessFieldInspectionsAwaitingRoutingList = new ArrayList<>();
        sessFieldInspectionsDispatched = new ArrayList<>();
        refreshFieldFinLists(null);
        
        initSearchFacility();
    }
    
   @PreDestroy
    public void cleanup() {
        // Perform any cleanup tasks here
        System.out.println("SessionInspectionConductor.cleanup");
    }
    
    /**
     * Controlled "Setter" for this class's session inspectable
     * @param inspectable 
     */
    public void registerSessionInspectable(IFace_inspectable inspectable){
        this.sessInspectable = inspectable;
        System.out.println("SessionInspectionConductor.registerSessionInspectable | inspectable ID: " + inspectable.getHostPK() + " domain: " + inspectable.getDomainEnum().getTitle());
    }
    
    /**
     * Startup search components
     */
    private void initSearchFacility(){
         SearchCoordinator sc = getSearchCoordinator();
        try {
            setQueryList(sc.buildQueryFieldInspection(getSessionBean().getSessUser().getMyCredential()));
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }
        setAppendResultsToList(false);

        if (Objects.isNull(getQuerySelected()) && !queryList.isEmpty())
        {
            setQuerySelected(getQueryList().stream().findFirst().orElse(null));
            setSearchParamsSelected(getQuerySelected().getParamsList().get(0));
        }

        sessFieldInspectionResultList = new ArrayList<>();
        
    }
    
     /**
     * Listener for user requests to query field inspections that started on the 
     * handheld app without a proper parent home
     * @param ev 
     */
    public void refreshFieldInitInspectionList(ActionEvent ev){
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            OccPeriodDataHeavy muniOPDH = oc.assembleOccPeriodDataHeavy(oc.getOccPeriod(getSessionBean().getSessMuni().getDefaultOccPeriodID(), getSessionBean().getSessUser()), getSessionBean().getSessUser());
            sessFieldInspectionsAwaitingRoutingList = oic.getOccInspectionLightList(muniOPDH);
            System.out.println("SessionInspectionConductor.refreshFieldInitInspectionList | size: " + sessFieldInspectionsAwaitingRoutingList.size());
        } catch (BObStatusException | BlobException | IntegrationException ex) {
            System.out.println(ex);
        }
    }
    
    /**
     * Builds a fresh list of inspections that have been dispatched and not yet synchronized, meaning officers need to go out and do them
     * @param ev
     */
    public void refreshDispatchedInspectionList(ActionEvent ev){
        SearchCoordinator sc = getSearchCoordinator();
        try {
            QueryFieldInspection dispatchedFINQuery = sc.initQuery(QueryFieldInspectionEnum.DISPATCHED_FINS_NOT_SYNCHRONIZED, getSessionBean().getSessUser().getKeyCard());
            sessFieldInspectionsDispatched = sc.runQuery(dispatchedFINQuery).getResults();
            System.out.println("SessInspectionConductor.refreshDispatchedInspectionList | size: " + sessFieldInspectionsDispatched.size());
        } catch (IntegrationException | BObStatusException | SearchException ex) {
            System.out.println(ex);
        } 
        
    }
    /**
     * Builds a fresh list of inspections that have been dispatched and not yet synchronized, meaning officers need to go out and do them
     * @param ev
     */
    public void refreshRecentlyUploadedInspectionList(ActionEvent ev){
        SearchCoordinator sc = getSearchCoordinator();
        try {
            QueryFieldInspection dispatchedFINQuery = sc.initQuery(QueryFieldInspectionEnum.RECENTLY_UPLOADED, getSessionBean().getSessUser().getKeyCard());
            sessFieldInspectionsRecentlyUploaded = sc.runQuery(dispatchedFINQuery).getResults();
            System.out.println("SessInspectionConductor.recentlyUploaded| size: " + sessFieldInspectionsRecentlyUploaded.size());
        } catch (IntegrationException | BObStatusException | SearchException ex) {
            System.out.println(ex);
        } 
        
    }
    
    /**
     * reruns all session FIN queries to grab field init fins and dispatched.
     * Called on dashboard reload and completion of field fin routing.
     * @param ev 
     */
    public void refreshFieldFinLists(ActionEvent ev){
        refreshDispatchedInspectionList(null);
        refreshFieldInitInspectionList(null);
        refreshRecentlyUploadedInspectionList(null);
        
    }
    
    /**
     * listener for user to view the inspections dialog via dispatched
     * @param ev 
     */
    public void viewDispatchedInspections(ActionEvent ev){
        System.out.println("SessionInspectionConductor.viewDispatchedInspections");
        refreshFieldFinLists(null);
    }
    
    /**
     * listener for user to view the inspections dialog via field init
     * @param ev 
     */
    public void viewFieldInitInspections(ActionEvent ev){
        System.out.println("SessionInspectionConductor.viewFieldInitInspections");
        refreshFieldFinLists(null);
    }


    // SEARCH ORGANS
    
    /**
     * Action listener for the user's request to run the query
     *
     * @param event
     */
    public void executeQuery(ActionEvent event) {
        SearchCoordinator sc = getSearchCoordinator();
        if (Objects.nonNull(getQuerySelected()))
        {
            System.out.println("FieldInspectionSearchBB.executeQuery | querySelected: " + getQuerySelected().getQueryTitle());
        }
        List<FieldInspectionLight> fiList;
        try
        {
            fiList = sc.runQuery(getQuerySelected()).getBOBResultList();
            if (!isAppendResultsToList() && Objects.nonNull(getSessFieldInspectionResultList()))
            {
                sessFieldInspectionResultList.clear();
            }
            if (Objects.nonNull(fiList) && !fiList.isEmpty())
            {
                sessFieldInspectionResultList.addAll(fiList);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Your search completed with " + fiList.size() + " results", ""));
            } else
            {
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Your search had no results", ""));
            }
            
        } catch (SearchException ex)
        {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to complete search! ", ""));
        }
    }

    /**
     * Listener method for changes in the selected query; Updates search params
     * and UI updates based on this changed value
     */
    public void changeQuerySelected() {
        System.out.println("FieldInspectionSearchBB.changeQuerySelected | querySelected: " + getQuerySelected().getQueryTitle());
        configureParameters();

    }

    /**
     * Sets up search parameters for properties
     */
    private void configureParameters() {
        if (Objects.nonNull(getQuerySelected())
                && Objects.nonNull(getQuerySelected().getParamsList())
                && !querySelected.getParamsList().isEmpty())
        {
            setSearchParamsSelected(getQuerySelected().getParamsList().get(0));
        }

    }

    /**
     * Event listener for resetting a query after it's run
     *
     * @param event
     */
    public void resetQuery(ActionEvent event) {
        SearchCoordinator sc = getSearchCoordinator();
        try {
            setQueryList(sc.buildQueryFieldInspection(getSessionBean().getSessUser().getMyCredential()));
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        } 
        if (Objects.nonNull(getQueryList()) && !queryList.isEmpty())
        {
            setQuerySelected(getQueryList().stream().findFirst().orElse(null));
        }
        if (isAppendResultsToList() == false)
        {
            if (Objects.nonNull(getSessFieldInspectionResultList()) && !sessFieldInspectionResultList.isEmpty())
            {
                getSessFieldInspectionResultList().clear();
            }
        }
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Query reset ", ""));

        configureParameters();
    }

    /**
     * Event listener for clear field inspections list
     *
     * @param ev
     */
    public void clearFieldInspectionList(ActionEvent ev) {
        if (Objects.nonNull(getSessFieldInspectionResultList()) && !sessFieldInspectionResultList.isEmpty())
        {
            getSessFieldInspectionResultList().clear();
            sessFieldInspectionResultList = new ArrayList<>();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Event List Reset!", ""));
        }

    }
    
    
    /**
     * Listener for user clicks to view the property of the parent inspection
     * @param finLight
     * @return 
     */
    public String onNavigateToFINProperty(FieldInspectionLight finLight){
        try {
            OccInspectionCoordinator oic = getOccInspectionCoordinator();
            if(finLight == null){
                return "";
            }
            return getSessionBean().navigateToPageCorrespondingToObject(oic.getInspectionParentProperty(finLight, getSessionBean().getSessUser()));
        } catch (BObStatusException | AuthorizationException | BlobException | IntegrationException | SearchException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Could not load the parent property of this inspection, sorry. You'll have to search for the property manually using dash or dialog property search.", ""));
            
        } 
        return "";
    }
    

    /**
     * method for navigating page to parent CECase or permit file
     *
     * @param heavy
     * @return navTo
     */
    public String onInspectionViewParentObject(FieldInspectionLight heavy) {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        SessionBean sb = getSessionBean();
        String navTo = "";
        try {
            navTo = sb.navigateToPageCorrespondingToObject(oic.extractInspectionParentInspectable(heavy, getSessionBean().getSessUser()));
        } catch (IntegrationException | BObStatusException | AuthorizationException | BlobException | SearchException ex) {
            System.out.println(ex.toString());
        }
        if (navTo.equals("")) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Cannot load CECase or Permit File for Inspection ID:" + heavy.getInspectionID(), ""));
        }
        return navTo;
    }

    /**
     * Listener for user requests to view the field inspection log
     *
     * @param ev
     */
    public void onViewFieldInspectionLogLinkClick(ActionEvent ev) {
        System.out.println("FieldInspectionSearchBB.onViewFieldInspectionLogLinkClick");
    }
    
    
    
    // *************************************************************************
    // ********************    getters and setters          ********************
    // *************************************************************************
    
    
    
    

    /**
     * @return the sessFieldInspectionAwaitingRouting
     */
    public FieldInspectionLight getSessFieldInspectionAwaitingRouting() {
        return sessFieldInspectionAwaitingRouting;
    }

    /**
     * @param sessFieldInspectionAwaitingRouting the sessFieldInspectionAwaitingRouting to set
     */
    public void setSessFieldInspectionAwaitingRouting(FieldInspectionLight sessFieldInspectionAwaitingRouting) {
        this.sessFieldInspectionAwaitingRouting = sessFieldInspectionAwaitingRouting;
    }

    /**
     * @return the sessFieldInspectionsAwaitingRoutingList
     */
    public List<FieldInspectionLight> getSessFieldInspectionsAwaitingRoutingList() {
        return sessFieldInspectionsAwaitingRoutingList;
    }

    /**
     * @param sessFieldInspectionsAwaitingRoutingList the sessFieldInspectionsAwaitingRoutingList to set
     */
    public void setSessFieldInspectionsAwaitingRoutingList(List<FieldInspectionLight> sessFieldInspectionsAwaitingRoutingList) {
        this.sessFieldInspectionsAwaitingRoutingList = sessFieldInspectionsAwaitingRoutingList;
    }

    /**
     * @return the sessFieldInspectionsDispatched
     */
    public List<FieldInspectionLight> getSessFieldInspectionsDispatched() {
        return sessFieldInspectionsDispatched;
    }

    /**
     * @param sessFieldInspectionsDispatched the sessFieldInspectionsDispatched to set
     */
    public void setSessFieldInspectionsDispatched(List<FieldInspectionLight> sessFieldInspectionsDispatched) {
        this.sessFieldInspectionsDispatched = sessFieldInspectionsDispatched;
    }

    /**
     * @return the sessFieldInspection
     */
    public FieldInspection getSessFieldInspection() {
        return sessFieldInspection;
    }

    /**
     * @param sessFieldInspection the sessFieldInspection to set
     */
    public void setSessFieldInspection(FieldInspection sessFieldInspection) {
        this.sessFieldInspection = sessFieldInspection;
    }

    /**
     * @return the sessFieldInspectionResultList
     */
    public List<FieldInspectionLight> getSessFieldInspectionResultList() {
        return sessFieldInspectionResultList;
    }

    /**
     * @param sessFieldInspectionResultList the sessFieldInspectionResultList to set
     */
    public void setSessFieldInspectionResultList(List<FieldInspectionLight> sessFieldInspectionResultList) {
        this.sessFieldInspectionResultList = sessFieldInspectionResultList;
    }

    /**
     * @return the sessFieldInspectionListForRefresh
     */
    public List<FieldInspectionLight> getSessFieldInspectionListForRefresh() {
        return sessFieldInspectionListForRefresh;
    }

    /**
     * @param sessFieldInspectionListForRefresh the sessFieldInspectionListForRefresh to set
     */
    public void setSessFieldInspectionListForRefresh(List<FieldInspectionLight> sessFieldInspectionListForRefresh) {
        this.sessFieldInspectionListForRefresh = sessFieldInspectionListForRefresh;
    }

    /**
     * @return the querySelected
     */
    public QueryFieldInspection getQuerySelected() {
        return querySelected;
    }

    /**
     * @param querySelected the querySelected to set
     */
    public void setQuerySelected(QueryFieldInspection querySelected) {
        this.querySelected = querySelected;
    }

    /**
     * @return the queryList
     */
    public List<QueryFieldInspection> getQueryList() {
        return queryList;
    }

    /**
     * @param queryList the queryList to set
     */
    public void setQueryList(List<QueryFieldInspection> queryList) {
        this.queryList = queryList;
    }

    /**
     * @return the searchParamsSelected
     */
    public SearchParamsFieldInspection getSearchParamsSelected() {
        return searchParamsSelected;
    }

    /**
     * @param searchParamsSelected the searchParamsSelected to set
     */
    public void setSearchParamsSelected(SearchParamsFieldInspection searchParamsSelected) {
        this.searchParamsSelected = searchParamsSelected;
    }

    /**
     * @return the appendResultsToList
     */
    public boolean isAppendResultsToList() {
        return appendResultsToList;
    }

    /**
     * @param appendResultsToList the appendResultsToList to set
     */
    public void setAppendResultsToList(boolean appendResultsToList) {
        this.appendResultsToList = appendResultsToList;
    }

    /**
     * @return the sessInspectable
     */
    public IFace_inspectable getSessInspectable() {
        return sessInspectable;
    }

    /**
     * @return the sessFieldInspectionsRecentlyUploaded
     */
    public List<FieldInspectionLight> getSessFieldInspectionsRecentlyUploaded() {
        return sessFieldInspectionsRecentlyUploaded;
    }

    /**
     * @param sessFieldInspectionsRecentlyUploaded the sessFieldInspectionsRecentlyUploaded to set
     */
    public void setSessFieldInspectionsRecentlyUploaded(List<FieldInspectionLight> sessFieldInspectionsRecentlyUploaded) {
        this.sessFieldInspectionsRecentlyUploaded = sessFieldInspectionsRecentlyUploaded;
    }

   
    
    
}
