/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.session;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.coordinators.EventCoordinator;
import com.tcvcog.tcvce.coordinators.OccupancyCoordinator;
import com.tcvcog.tcvce.coordinators.PropertyCoordinator;
import com.tcvcog.tcvce.coordinators.SearchCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.domain.SearchException;
import com.tcvcog.tcvce.entities.EventCategory;
import com.tcvcog.tcvce.entities.EventRealm;
import com.tcvcog.tcvce.entities.EventCnF;
import static com.tcvcog.tcvce.entities.EventRealm.CODE_ENFORCEMENT;
import static com.tcvcog.tcvce.entities.EventRealm.OCCUPANCY;
import static com.tcvcog.tcvce.entities.EventRealm.PARCEL;
import static com.tcvcog.tcvce.entities.EventRealm.UNIVERSAL;
import com.tcvcog.tcvce.entities.EventType;
import com.tcvcog.tcvce.entities.RoleType;
import com.tcvcog.tcvce.entities.reports.ReportConfigCEEventList;
import com.tcvcog.tcvce.entities.search.QueryEvent;
import com.tcvcog.tcvce.entities.search.QueryEventEnum;
import com.tcvcog.tcvce.entities.search.SearchParamsEvent;
import com.tcvcog.tcvce.session.entities.EventCalendarDay;
import com.tcvcog.tcvce.util.viewoptions.ViewOptionsActiveHiddenListsEnum;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;
import java.util.Arrays;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The premier session-scoped, object family specific bean
 * whose job is the mighty session conducting of event related
 * tasks, including the dashboard calendar whose objects are managed
 * by this class.
 * 
 * This class exists as a test case for having object family specific
 * session-scoped beans. As of Dec 2023, only this EventConductor 
 * follows this pattern. All the rest of the session objects are
 * still managed by the SessionBean
 * 
 * @author Ellen Bascomb (of Apartment 31Y)
 */
public class SessionEventConductor extends BackingBeanUtils{
    
    static final int HOURS_IN_1_DAY = 24;
    static final int MAX_EVENTS_TO_SHOW_PER_DAY = 5;
    static final String GENERAL_EVENT_STRING = "**GENERAL EVENT**";
    
    // CALENDAR TOOLS
    private List<EventCalendarDay> eventCalendar5Day;
    
    private List<EventCnF> sessEventListForRefreshUptake;
    private List<EventCnF> sessEventList;
    private EventCnF sessEvent;
    
    /**
     * I store events that require follow up that were not followed up on their 
     * declared follow-up date
     * 
     */
    private List<EventCnF> followUpEventsBackLog;
    
    /* >>> -------------------------------------------------------------- <<< */
    /* >>>                   V Event                                      <<< */
    /* >>> -------------------------------------------------------------- <<< */
    /**
     * There is no longer a notion of a session event domain, only a requested
     * event domain for event manipulators to use as guidance to figure out
     * if the relevant event list is extracted from the proper session business
     * objects.
     * 
     * ECD on event overhaul DEC-JAN 2024: that's a bunch of hooey 
     */
    private EventRealm sessEventsPageEventDomainRequest;
    private QueryEvent queryEventFuture7Days;
    /* >>> QUERY EVENT <<< */
    private QueryEvent queryEvent;
    private List<QueryEvent> queryEventList;
    
    // Migrated organs from the EventSearchBB
    
    private QueryEvent querySelected;
    private List<QueryEvent> queryList;
    private SearchParamsEvent searchParamsSelected;
    private boolean appendResultsToList;
    
    private EventCnF currentEvent;
    
    private List<EventCnF> eventListManaged;
    private List<EventCnF> eventListFiltered;
    private List<ViewOptionsActiveHiddenListsEnum> eventViewList;
    private ViewOptionsActiveHiddenListsEnum eventViewSelected;
    
    private ReportConfigCEEventList reportConfig;
    
    private List<EventType> eventTypeList;
    private List<EventCategory> eventCategoryList;
    private List<EventCnF> selectedEvents;
    private boolean selectionMode;
    
    
    /**
     * Creates a new instance of SessionEvents
     */
    public SessionEventConductor() {
        
    }
    
    @PostConstruct
    public void initBean()  {
        SearchCoordinator sc = getSearchCoordinator();
        EventCoordinator ec = getEventCoordinator();
        
        System.out.println("SessionEventConductor.initBean");
        
        configureEventQueryList();
        
        // start with default CE domain
        sessEventsPageEventDomainRequest = getSessionBean().getSessionDomain().getEventRealmMapping();
        
        queryList = sc.buildQueryEventList(getSessionBean().getSessUser().getMyCredential());
        appendResultsToList = false;
        
        if(querySelected == null && !queryList.isEmpty()){
            querySelected = queryList.get(0);
        }
      
        // setup search
        configureParameters();
             
        
        if(sessEventList == null){
            sessEventList = new ArrayList<>();
        }
        
        eventListManaged = new ArrayList<>();
        
        manageRawEventList();
        eventListFiltered = new ArrayList<>();
        eventViewList = Arrays.asList(ViewOptionsActiveHiddenListsEnum.values());
        eventViewSelected = ViewOptionsActiveHiddenListsEnum.VIEW_ACTIVE_NOTHIDDEN;
        
        eventTypeList = Arrays.asList(EventType.values());
        Collections.sort(eventTypeList);
        try {
            eventCategoryList = ec.getEventCategoryList();
            Collections.sort(eventCategoryList); 
        } catch (IntegrationException ex) {
            System.out.println(ex);
        }
        
//        QueryEvent futureEvents = sc.initQuery(QueryEventEnum.MUINI_FUTURE_7DAYS, cred);
//        try {
            // TODO: Debug hanging issues
//            sb.setQueryEventFuture7Days(sc.runQuery(futureEvents));
//        } catch (SearchException ex) {
//            System.out.println(ex);
//        }

    }
    
    /**
     * Sets up search parameters for properties
     */
    private void configureParameters(){
        SystemCoordinator sc = getSystemCoordinator();
        if(querySelected != null 
                && 
            querySelected.getParamsList() != null 
                && 
            !querySelected.getParamsList().isEmpty()){
            
            searchParamsSelected = querySelected.getParamsList().get(0);
        }
      
    }
    
    public void clearEventList(ActionEvent ev){
        if(eventListManaged != null && !eventListManaged.isEmpty()){
            eventListManaged.clear();
              getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, 
                        "Event List Reset!", ""));
        }
        
    }
    
    
      /**
     * Action listener for the user's request to run the query
     * @param event 
     */
    public void executeQuery(ActionEvent event){
        SearchCoordinator sc = getSearchCoordinator();
        if(querySelected != null){
            System.out.println("SessionEventConductor.executeQuery | querySelected: " + querySelected.getQueryTitle());
        }
        List<EventCnF> evList;
        try {
            evList = sc.runQuery(querySelected, getSessionBean().getSessUser()).getBOBResultList();
            if(!appendResultsToList && eventListManaged != null){
                sessEventList.clear();
            } 
            if(evList != null && !evList.isEmpty()){
                sessEventList.addAll(evList);
                
                eventViewSelected = ViewOptionsActiveHiddenListsEnum.VIEW_ALL;
                manageRawEventList();
                getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, 
                            "Your search completed with " + evList.size() + " results", ""));
            }else {
                getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, 
                            "Your search had no results", ""));
            }
        } catch (SearchException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                        "Unable to complete search! ", ""));
        }
    }
    
    /**
     * listens for user views of a event
     * @param evpucph 
     */
    public void onViewEvent(EventCnF evpucph){
        currentEvent = evpucph;
        getSessionEventConductor().setSessEvent(currentEvent);
    }
    
    /**
     * Listener for user requests to view the event log
     * @param ev 
     */
    public void onViewEventLogLinkClick(ActionEvent ev){
        System.out.println("SessionEventConductor.onViewEventLogLinkClick");
    }
    
    /**
     * Listener to go see the property on which an event is attached
     * @param ev 
     * @return  page ID for nav
     */
    public String onViewEventProperty(EventCnF ev){
        PropertyCoordinator pc = getPropertyCoordinator();
        EventCoordinator ec = getEventCoordinator();
        try {
            return getSessionBean().navigateToPageCorrespondingToObject(pc.getProperty(ec.getEventParcelKey(ev)));
        } catch (BObStatusException | AuthorizationException | IntegrationException ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                        ex.getMessage(), ""));
            return "";
        }
        
    }
    
    /**
     * Listener to go see the cecase or 
     * @param ev 
     * @return  page ID for navigation
     */
    public String onViewEventHolder(EventCnF ev){
        
         try {
            switch(ev.getDomain()){
                case CODE_ENFORCEMENT:
                    CaseCoordinator cc = getCaseCoordinator();
                    return getSessionBean().navigateToPageCorrespondingToObject(cc.cecase_getCECase(ev.getCeCaseID(), getSessionBean().getSessUser()));
                case OCCUPANCY:
                    OccupancyCoordinator oc = getOccupancyCoordinator();
                    return getSessionBean().navigateToPageCorrespondingToObject(oc.getOccPeriod(ev.getOccPeriodID(), getSessionBean().getSessUser()));
                case PARCEL:
                    PropertyCoordinator pc = getPropertyCoordinator();
                    return getSessionBean().navigateToPageCorrespondingToObject(pc.getProperty(ev.getParcelKey()));
                case UNIVERSAL:
                    getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                               "FATAL ERROR: Unable to navigate to case or file, sorry! ", ""));
                    return "";
                default:
                    getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                               "FATAL ERROR: Unable to navigate to case or file, sorry! ", ""));
                    return "";
            }
        } catch (IntegrationException | BObStatusException | AuthorizationException | BlobException  ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                        ex.getMessage(), ""));
            return "";
        }
        
    }
    
    /**
     * Listener for the event list view options drop down list change
     */
    public void manageRawEventList(){
        EventCoordinator ec = getEventCoordinator();
        if(eventListManaged != null && sessEventList != null && !sessEventList.isEmpty() && eventViewSelected != null){
            System.out.println("SessionEventConductor.filterEventList | selected view " + eventViewSelected.name());
            eventListManaged = ec.filterEventListForViewFloor(ec.filterEventPropUnitCasePeriodHeavyList(sessEventList, eventViewSelected), getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod().getRole());
        }
    }
    
    /**
     * Listener method for changes in the selected query;
     * Updates search params and UI updates based on this changed value
     */
    public void changeQuerySelected(){
        System.out.println("SessionEventConductor.changeQuerySelected | querySelected: " + querySelected.getQueryTitle());
        configureParameters();
        
    }
    
    /**
     * Listener for hides
     * @param evpucph 
     */
    public void onEventHide(EventCnF evpucph){
        evpucph.setHidden(true);
        manageRawEventList();
        
        
    }
    
    /**
     * Listener for unhides
     * @param evpucph 
     */
    public void onEventUnHide(EventCnF evpucph){
        evpucph.setHidden(false);
        manageRawEventList();
        
    }
    
    
    
    /**
     * Event listener for resetting a query after it's run
     * @param event 
     */
    public void resetQuery(ActionEvent event){
        SearchCoordinator sc = getSearchCoordinator();
        //        querySelected = sc.initQuery(querySelected.getQueryName(), getSessionBean().getSessUser().getMyCredential());
        queryList = sc.buildQueryEventList(getSessionBean().getSessUser().getMyCredential());
        if(queryList != null && !queryList.isEmpty()){
            querySelected = queryList.get(0);
        }
        if(appendResultsToList == false){
            if(eventListManaged != null && !eventListManaged.isEmpty()){
                eventListManaged.clear();
            }
        }
        getFacesContext().addMessage(null,
            new FacesMessage(FacesMessage.SEVERITY_INFO, 
                    "Query reset ", ""));

     
        configureParameters();
    }
    
    
    /**
     * Starts the report building process
     * @param ev 
     */
    public void prepareEventReport(ActionEvent ev){
        EventCoordinator ec = getEventCoordinator();
        reportConfig = ec.initDefaultReportConfigEventList();
        
        
    }
    
    /**
     * Listener to view the final report
     * @return 
     */
    public String generateEventReport(){
        EventCoordinator ec = getEventCoordinator();
        getSessionBean().setReportConfigEventList(reportConfig);
        
        return "reportEventList";
        
    }
   
    /**
     * This function exports selected events as an .ics file for download.
     */
    public void exportCalendar() {
        List<EventCnF> selEvents = getSelectedEvents();
        EventCoordinator ec = getEventCoordinator();
        if (!selEvents.isEmpty())
        {
            ec.exportCalendar(selEvents);
            System.out.println("Successfully generated events.ics file");
            
        } else
        {
            selectionMode = false;
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "No Events Selected To Export!", ""));
        }
    }

    public void changeSelectionMode() {
        selectionMode = true;
        selectedEvents = null;
    }
    
    public void abortExportOperation() {
        selectionMode = false;
        selectedEvents = null;
    }

    
    private void configureEventQueryList(){
        SearchCoordinator sc = getSearchCoordinator();
        queryEventList= sc.buildQueryEventList(getSessionBean().getSessUser().getKeyCard());
        if(!queryEventList.isEmpty()){
            queryEvent = queryEventList.get(0);
        }
    }
    
    /**
     * Sentinel pathway for triggering an event list refresh for properties, cases, 
     * and occ periods. Internally I write the given event list to this class's
     * sessEventListForRefreshUptake member
     * @param evList 
     */
    public void triggerEventReload(List<EventCnF> evList){
        sessEventListForRefreshUptake = evList;
    }
    
    /**
     * Listener for user requests to run calendar event query again
     * @param ev 
     */
    public void refreshCalendarAndFollowupBacklog(ActionEvent ev){
        try {
            initEventCalendar5Day();
            loadFollowupBacklog();
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                        "Calendar refreshed!", ""));
        } catch (SearchException ex) {
            System.out.println(ex);
              getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                        "Could not load calendar: " + ex.getMessage(), ""));
        }
    }

    /**
     * Queries for events requiring follow up but which have not been followed up on
     * and loads these into the member followUpEventsBackLog
     */
    private void loadFollowupBacklog(){
        SearchCoordinator sc = getSearchCoordinator();
        EventCoordinator ec = getEventCoordinator();
        try {
            QueryEvent backlogQuery = sc.initQuery(QueryEventEnum.FOLLOWUP_BACKLOG, getSessionBean().getSessUser().getKeyCard());
            followUpEventsBackLog = ec.filterEventListForViewFloor(sc.runQuery(backlogQuery, getSessionBean().getSessUser()).getBOBResultList(), getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod().getRole());
            configureGeneralEvents(sessEventList);
        } catch (SearchException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                        "Could not load backlog: " + ex.getMessage(), ""));
        }
        
        
    }
    
    
    /**
     * Conducts a day-by-day event query for yesterday, today, and 4 future days
     * and injects the resulting composite object into this bean's member
     * 
     */
    private void initEventCalendar5Day() throws SearchException{
        SearchCoordinator sc = getSearchCoordinator();
        EventCoordinator ec = getEventCoordinator();
 
        List<EventCalendarDay> dayList = new ArrayList<>();
        Map<Integer, String> prefixMap = new HashMap<>();

        prefixMap.put(0, "Today: ");
        prefixMap.put(1, "Tomorrow: : ");
        // make a 5-day calendar
        for(int dayCount = 0; dayCount <=4; dayCount++){
            EventCalendarDay day = new EventCalendarDay();
            LocalDate ld;
            
            if(dayCount < 0){
                int minusOffset = dayCount * -1; // make day count a positive int for .minusDays()
                ld = LocalDate.now().minusDays(minusOffset);
            } else if (dayCount == 0){
                ld = LocalDate.now();
            } else {
                ld = LocalDate.now().plusDays(dayCount);
            }
            
            day.setDay(ld);
            day.setLdtLowerBound(ld.atStartOfDay());
            day.setLdtUpperBound(day.getLdtLowerBound().plusHours(HOURS_IN_1_DAY));
           
            // set the prefix if we have one
            String s = prefixMap.get(dayCount);
            if(s != null){
                day.setDayPrettyPrefix(s);
            } else {
                day.setDayPrettyPrefix("");
            }
                        
            QueryEvent evq = sc.initQuery(QueryEventEnum.CALENDAR, getSessionBean().getSessUser().getKeyCard());
            evq.getPrimaryParams().setDate_start_val(day.getLdtLowerBound());
            evq.getPrimaryParams().setDate_end_val(day.getLdtUpperBound());
            evq.getPrimaryParams().setMuni_val(getSessionBean().getSessMuni());
           
            List<EventCnF> evListTotal = ec.filterEventListForViewFloor(sc.runQuery(evq, getSessionBean().getSessUser()).getBOBResultList(), getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod().getRole());
            configureGeneralEvents(evListTotal);
            
            // separate longer lists into overflow if our list exceeds the display threshold
            if(evListTotal != null && evListTotal.size() > MAX_EVENTS_TO_SHOW_PER_DAY){
                day.setEvList(evListTotal.subList(0, MAX_EVENTS_TO_SHOW_PER_DAY));
                day.setEvListOverflow(evListTotal.subList(MAX_EVENTS_TO_SHOW_PER_DAY, evListTotal.size()));
            } else {
                day.setEvList(evListTotal);
            }
            
            dayList.add(day);
           
        } // end for() over each day in the calendar
       
       eventCalendar5Day = dayList;
        
    }
    
    /**
     * GEneral events are attached to the muni property, so they shouldn't display a full property and case string
     * @param evList 
     */
    private void configureGeneralEvents(List<EventCnF> evList){
        if(evList != null && !evList.isEmpty()){
            int muniParcelKey = getSessionBean().getSessMuni().getMuniPropertyDH().getParcelKey();
            
            for(EventCnF ev: evList){
                if(ev.getParcelKey() == muniParcelKey){
                    // adjust our strings to reflect general event
                    ev.setCasePeriodPropertyString(GENERAL_EVENT_STRING);
                    ev.setParcelAddressOneLine(null);
                }
            }
        }
    }

    /**
     *
     *
     * @param sessEventList the sessEventList to set
     */
    public void setSessEventList(List<EventCnF> sessEventList) {
        this.sessEventList = sessEventList;
    }

  
    /**
     *
     *
     * @return the sessEvent
     */
    public EventCnF getSessEvent() {
        return sessEvent;
    }

    /**
     *
     *
     * @param sessEventsPageEventDomainRequest the sessEventsPageEventDomainRequest to set
     */
    public void setSessEventsPageEventDomainRequest(EventRealm sessEventsPageEventDomainRequest) {
        this.sessEventsPageEventDomainRequest = sessEventsPageEventDomainRequest;
    }

    /**    
     * @param sessEventListForRefreshUptake the sessEventListForRefreshUptake to set
     */
    public void setSessEventListForRefreshUptake(List<EventCnF> sessEventListForRefreshUptake) {
        this.sessEventListForRefreshUptake = sessEventListForRefreshUptake;
    }

    /**
     *
     *
     * @return the sessEventList
     */
    public List<EventCnF> getSessEventList() {
        return sessEventList;
    }

   

    /**
     *
     *
     * @return the sessEventListForRefreshUptake
     */
    public List<EventCnF> getSessEventListForRefreshUptake() {
        return sessEventListForRefreshUptake;
    }

    /**
     *
     *
     * @param sessEvent the sessEvent to set
     */
    public void setSessEvent(EventCnF sessEvent) {
        this.sessEvent = sessEvent;
    }

    /**
     *
     *
     * @return the sessEventsPageEventDomainRequest
     */
    public EventRealm getSessEventsPageEventDomainRequest() {
        return sessEventsPageEventDomainRequest;
    }

    /**
     * @return the eventCalendar5Day
     */
    public List<EventCalendarDay> getEventCalendar5Day() {
        return eventCalendar5Day;
    }

    /**
     * @param eventCalendar5Day the eventCalendar5Day to set
     */
    public void setEventCalendar5Day(List<EventCalendarDay> eventCalendar5Day) {
        this.eventCalendar5Day = eventCalendar5Day;
    }

    /**
     *
     *
     * @param queryEvent the queryEvent to set
     */
    public void setQueryEvent(QueryEvent queryEvent) {
        this.queryEvent = queryEvent;
    }

    /**
     *
     *
     * @return the queryEventList
     */
    public List<QueryEvent> getQueryEventList() {
        return queryEventList;
    }

    /**
     *
     *
     * @return the queryEventFuture7Days
     */
    public QueryEvent getQueryEventFuture7Days() {
        return queryEventFuture7Days;
    }

    /**
     *
     *
     * @param queryEventList the queryEventList to set
     */
    public void setQueryEventList(List<QueryEvent> queryEventList) {
        this.queryEventList = queryEventList;
    }

    /**
     *
     *
     * @param queryEventFuture7Days the queryEventFuture7Days to set
     */
    public void setQueryEventFuture7Days(QueryEvent queryEventFuture7Days) {
        this.queryEventFuture7Days = queryEventFuture7Days;
    }

    /**
     *
     *
     * @return the queryEvent
     */
    public QueryEvent getQueryEvent() {
        return queryEvent;
    }

    /**
     * @return the followUpEventsBackLog
     */
    public List<EventCnF> getFollowUpEventsBackLog() {
        return followUpEventsBackLog;
    }

    /**
     * @param followUpEventsBackLog the followUpEventsBackLog to set
     */
    public void setFollowUpEventsBackLog(List<EventCnF> followUpEventsBackLog) {
        this.followUpEventsBackLog = followUpEventsBackLog;
    }

    
    
    // Migrated getters and setters
    

    /**
     * @return the querySelected
     */
    public QueryEvent getQuerySelected() {
        return querySelected;
    }

    /**
     * @return the queryList
     */
    public List<QueryEvent> getQueryList() {
        return queryList;
    }

    /**
     * @param querySelected the querySelected to set
     */
    public void setQuerySelected(QueryEvent querySelected) {
        this.querySelected = querySelected;
    }

    /**
     * @param queryList the queryList to set
     */
    public void setQueryList(List<QueryEvent> queryList) {
        this.queryList = queryList;
    }

    /**
     * @return the appendResultsToList
     */
    public boolean isAppendResultsToList() {
        return appendResultsToList;
    }

    /**
     * @param appendResultsToList the appendResultsToList to set
     */
    public void setAppendResultsToList(boolean appendResultsToList) {
        this.appendResultsToList = appendResultsToList;
    }

    /**
     * @return the searchParamsSelected
     */
    public SearchParamsEvent getSearchParamsSelected() {
        return searchParamsSelected;
    }

    /**
     * @param searchParamsSelected the searchParamsSelected to set
     */
    public void setSearchParamsSelected(SearchParamsEvent searchParamsSelected) {
        this.searchParamsSelected = searchParamsSelected;
    }

    /**
     * @return the eventListManaged
     */
    public List<EventCnF> getEventListManaged() {
        return eventListManaged;
    }

    /**
     * @return the eventListFiltered
     */
    public List<EventCnF> getEventListFiltered() {
        return eventListFiltered;
    }

    /**
     * @param eventListManaged the eventListManaged to set
     */
    public void setEventListManaged(List<EventCnF> eventListManaged) {
        this.eventListManaged = eventListManaged;
    }

    /**
     * @param eventListFiltered the eventListFiltered to set
     */
    public void setEventListFiltered(List<EventCnF> eventListFiltered) {
        this.eventListFiltered = eventListFiltered;
    }

    /**
     * @return the reportConfig
     */
    public ReportConfigCEEventList getReportConfig() {
        return reportConfig;
    }

    /**
     * @param reportConfig the reportConfig to set
     */
    public void setReportConfig(ReportConfigCEEventList reportConfig) {
        this.reportConfig = reportConfig;
    }

    /**
     * @return the eventViewList
     */
    public List<ViewOptionsActiveHiddenListsEnum> getEventViewList() {
        return eventViewList;
    }

    /**
     * @return the eventViewSelected
     */
    public ViewOptionsActiveHiddenListsEnum getEventViewSelected() {
        return eventViewSelected;
    }

    /**
     * @param eventViewList the eventViewList to set
     */
    public void setEventViewList(List<ViewOptionsActiveHiddenListsEnum> eventViewList) {
        this.eventViewList = eventViewList;
    }

    /**
     * @param eventViewSelected the eventViewSelected to set
     */
    public void setEventViewSelected(ViewOptionsActiveHiddenListsEnum eventViewSelected) {
        this.eventViewSelected = eventViewSelected;
    }

    /**
     * @return the currentEvent
     */
    public EventCnF getCurrentEvent() {
        return currentEvent;
    }

    /**
     * @param currentEvent the currentEvent to set
     */
    public void setCurrentEvent(EventCnF currentEvent) {
        this.currentEvent = currentEvent;
    }

    /**
     * @return the eventTypeList
     */
    public List<EventType> getEventTypeList() {
        return eventTypeList;
    }

    /**
     * @return the eventCategoryList
     */
    public List<EventCategory> getEventCategoryList() {
        return eventCategoryList;
    }

    /**
     * @param eventTypeList the eventTypeList to set
     */
    public void setEventTypeList(List<EventType> eventTypeList) {
        this.eventTypeList = eventTypeList;
    }

    /**
     * @param eventCategoryList the eventCategoryList to set
     */
    public void setEventCategoryList(List<EventCategory> eventCategoryList) {
        this.eventCategoryList = eventCategoryList;
    }

    public List<EventCnF> getSelectedEvents() {
        return selectedEvents;
    }

    public void setSelectedEvents(List<EventCnF> selectedEvents) {
        this.selectedEvents = selectedEvents;
    }

    public boolean isSelectionMode() {
        return selectionMode;
    }

    public void setSelectionMode(boolean selectionMode) {
        this.selectionMode = selectionMode;
    }
    
    
}
