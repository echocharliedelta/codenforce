/*
 * Copyright (C) 2018 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.session;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.coordinators.OccupancyCoordinator;
import com.tcvcog.tcvce.coordinators.PersonCoordinator;
import com.tcvcog.tcvce.coordinators.PropertyCoordinator;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.domain.SearchException;
import com.tcvcog.tcvce.entities.*;
import com.tcvcog.tcvce.entities.reports.*;
import com.tcvcog.tcvce.entities.search.*;
import com.tcvcog.tcvce.entities.occupancy.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import jakarta.annotation.PostConstruct;
import com.tcvcog.tcvce.coordinators.BlobCoordinator;
import com.tcvcog.tcvce.coordinators.CodeCoordinator;
import com.tcvcog.tcvce.coordinators.OccInspectionCoordinator;
import com.tcvcog.tcvce.coordinators.PermissionsCoordinator;
import com.tcvcog.tcvce.coordinators.SearchCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.coordinators.UserCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.EventException;
import com.tcvcog.tcvce.domain.SessionException;
import com.tcvcog.tcvce.integration.PropertyIntegrator;
import jakarta.faces.application.FacesMessage;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.tcvcog.tcvce.application.interfaces.IFaceActivatableBOB;

/**
 * Stores member vars of pretty much all our custom types
 * for persistence across an entire session (i.e. across page changes)
 * Many backing beans will grab this SessionBean in their initBean() method
 * and check for the presence of a session object. If not null, the method injects
 * those objects into its own members. If null, beans will decide if they need an object
 * and where to get it.
 * 
 * When many beans facilitate navigation to other pages, they will put their working
 * object on one of these session shelves for others to work with and to maintain
 * user state across page changes.
 * 
 * @author ellen bascomb of apt 31y
 */
public class    SessionBean 
        extends BackingBeanUtils {
     /**
     * Creates a new instance of getSessionBean()
     */
    public SessionBean() {
        System.out.println("SessionBean.SessionBean constructor");
    }
    
    
    @PostConstruct
    public void initBean(){
        System.out.println("SessionBean.initBean");
        navStack = new NavigationStack();
        initSessionFocusObject();
    }
    
    /**
     * Set our session Enum for Users land on the dashboard
     */
    private void initSessionFocusObject(){
        sessionDomain = FocusedObjectEnum.MUNI_DASHBOARD;
        System.out.println("SessionBean.initBean | Setting session focus ");
        
    }
    
    /* --- -------------------------------------------------------------- --- */
    /* ---            SESSION CONDUCTOR REGISTRY                           --- */
    /* --- -------------------------------------------------------------- --- */
    
    private List<IFaceSessionSynchronizer> sessionSyncRegistry = new ArrayList<>();
    
    /**
     * Must be called by all session conductors so they get notified when a Sync
     * target is passed into this bean for activation.
     * @param syncer 
     */
    public void registerSessionSynchronizer(IFaceSessionSynchronizer syncer){
        sessionSyncRegistry.add(syncer);
    }
    
    /* --- -------------------------------------------------------------- --- */
    /* ---            PRIMARY OBJECT AND XMUNI                       --- */
    /* --- -------------------------------------------------------------- --- */
    private FocusedObjectEnum sessionDomain;
    private boolean crossMuniViewMode;
    private Municipality crossMuniMuni;
    private XMuniActivationRecord xMuniRecord;
    
    /* --- -------------------------------------------------------------- --- */
    /* ---                      N Managed Lists                           --- */
    /* --- -------------------------------------------------------------- --- */
    
    /* --- -------------------------------------------------------------- --- */
    /* ---                      N User                                    --- */
    /* --- -------------------------------------------------------------- --- */
    
    private UserAuthorized sessUser;
    private User sessUserQueued;
    private User userForReInit;
    private UserAuthorizedForConfig userForConfig;
    
    private UserMuniAuthPeriod umapRequestedForReInit;
    private List<UserMuniAuthPeriod> sessUMAPListValidOnly;
    
    private LocalDateTime passwordResetNoticeTS;
    
    
    /* --- -------------------------------------------------------------- --- */
    /* ---                    I Municipality                              --- */
    /* --- -------------------------------------------------------------- --- */
    
    private MunicipalityDataHeavy sessMuni;
    private Municipality sessMuniQueued;
    private Municipality sessMuniLight;
    
    
    /* --- -------------------------------------------------------------- --- */
    /* ---                   II CodeBook                                 --- */
    /* --- -------------------------------------------------------------- --- */
    
    private CodeSet sessCodeSet;
    
    private CodeSource sessCodeSource;
    private CodeElementGuideEntry activeCodeElementGuideEntry;
    private List<CodeElementGuideEntry> sessCodeGuideList;
    private EnforceableCodeElement sessEnforcableCodeElement;
    private CodeElement activeCodeElement;
    
    private OccChecklistTemplate sessChecklistTemplate;
    private OccSpaceType sessOccSpaceType;
    
    /**
     * Listener for bean requests to get a fresh copy of the current code book
     */
    public void refreshSessionCodeBook(){
        CodeCoordinator codeCoor = getCodeCoordinator();
        if(sessMuni != null && sessMuni.getCodeSet() != null){
            try {
                System.out.println("SessionBean.refreshSessionCodeBook | refreshing");
                sessMuni.setCodeSet(codeCoor.getCodeSet(sessMuni.getCodeSet().getCodeSetID()));
                sessCodeSet = sessMuni.getCodeSet();
            } catch (BObStatusException | IntegrationException ex) {
                System.out.println(ex);
            } 
        }
    }
    
    
    /* --- -------------------------------------------------------------- --- */
    /* ---                   III Property                                 --- */
    /* --- -------------------------------------------------------------- --- */
    private PropertyDataHeavy sessProperty;
    
    private List<Property> sessPropertyList;

    private PropertyUnitDataHeavy sessPropertyUnit;
    
    private MailingCityStateZip sessMailingCityStateZip;
    private MailingAddress sessMailingAddress;
    private MailingAddressLink sessMailingAddressLink;
    private IFace_addressListHolder sessAddressListHolder;
    private List<MailingAddressLink> sessMailingAddressLinkRefreshedList;
    
    
    
    
    /* --- QUERY PROPERTY --- */
    private QueryProperty queryProperty;
    
    public String viewSessionProperty(){
        try {
            return navigateToPageCorrespondingToObject(sessProperty);
        } catch (BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                        ex.getMessage(), ""));
        } 
        return "";
    }
    
    /**
     * Convenience method for setting the session property and
     * property list with only an ID
     *
     * @param propID
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public void setSessProperty(int propID) throws BObStatusException, IntegrationException, BlobException, AuthorizationException{
        PropertyCoordinator pc = getPropertyCoordinator();
        if(propID == 0){
            throw new BObStatusException("Prop ID cannot be 0");
        }
        Property pr = pc.getProperty(propID);
        setSessProperty(pr);
    }
    
    /**
     * Convenience method for setting the session property and property list
     * with only a base class instance
     * 
     * @param prop 
     */
    public void setSessProperty(Property prop){
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            sessProperty = pc.assemblePropertyDataHeavy(prop, sessUser);
        } catch (BObStatusException | IntegrationException | SearchException | BlobException ex) {
            System.out.println("SessionBean.setSessionProperty: error setting session prop");
            System.out.println(ex);
        }
    }
    
     /**
     * @param sp
     */
    public void setSessProperty(PropertyDataHeavy sp) {
        this.sessProperty = sp;
    }
    
    
    /* --- -------------------------------------------------------------- --- */
    /* ---                     IV Person                                  --- */
    /* --- -------------------------------------------------------------- --- */
    
    
    private Person sessPerson;
    private IFace_humanListHolder sessHumanListHolder;
    private HumanLink sessHumanLink;
    private List<HumanLink> sessHumanListRefreshedList;
    
    private Person sessPersonQueued;
    private List<Person> sessPersonList;
    private boolean onPageLoad_sessionSwitch_viewProfile;
    /**
     * Shared switch used during person quick add in the certificate flow
     */
    private boolean certPersonQuickAddNewPersonPathway;
    
    
    /* --- QUERY PERSON --- */
    private QueryPerson queryPerson;
    private List<QueryPerson> queryPersonList;
    
    private String personNameForSearch;
    
    private boolean sessFlagIncludeDeactivatedEvents;
    
    
    
    /* --- -------------------------------------------------------------- --- */
    /* ---                  V  EVENT                                      --- */
    /* --- -------------------------------------------------------------- --- */
    
    // ALL THESE MEMBERS now live in SessionEventConductor
    
    
    /* --- -------------------------------------------------------------- --- */
    /* ---                  VI OccPeriod                                  --- */
    /* --- -------------------------------------------------------------- --- */
    
   // MIGRATED to SessionOccupancyConductor Jan 2025
    
    // adaptor methods
     /**
     * @return the sessOccPeriod
     */
    public OccPeriodDataHeavy getSessOccPeriod() {
        return getSessionOccupancyConductor().getSessOccPeriod();
    }

   
    
    /* --- -------------------------------------------------------------- --- */
    /* ---                  ?? Field inspection                                    --- */
    /* --- -------------------------------------------------------------- --- */
    
    // as of April 2024 these organs have been moved to SessionInspectionConductor
    
    
    /* --- -------------------------------------------------------------- --- */
    /* ---                  VII CECaseDataHeavy                                    --- */
    /* --- -------------------------------------------------------------- --- */
    
    private CECaseDataHeavy sessCECase;
    private CECase ceCaseForRefresh;
    private List<CECase> sessCECaseList;
    private List<CECaseDataHeavy> sessCECaseRefreshedList;
    
    private LocalDateTime sessCECaseRefreshTrigger;
    private LocalDateTime sessCECaseRefreshTriggerViolations;
    private LocalDateTime sessCECaseRefreshTriggerCitations;
    
    private CodeViolation sessCodeViolation;
    private List<CodeViolation> sessViolationList;
    
    private NoticeOfViolation sessNotice;
    private Citation sessCitation;
    
    /* --- QUERY CECASE --- */
    private QueryCECase queryCECase;
    private List<QueryCECase> queryCECaseList;
    
    
    /* --- -------------------------------------------------------------- --- */
    /* ---              VIII CEActionRequest                              --- */
    /* --- -------------------------------------------------------------- --- */
    
    private CEActionRequestPropertyHeavy sessCEAR;
    /**
     * Used by the CEActionRequestsBB and the CECaseAddBB for 
     * creating a new case during the CEAR processing cycle; 
     * When this field is NOT NULL, the CECaseAddBB will react to its 
     * state and undertake the CEAR linking process via the CaseCoordinator
     * during the CECaseAdd operation. CECaseAddBB nulls out this
     * value after completion of the linking operation 
     */
    private CEARProcessingRouteEnum activeCEARProcessingRoute;
    private List<QueryCEAR> queryCEARList;
    
    /**
     * The CEActionRequestsBB will run this query every 5 minutes to keep the
     * list freshish
     */
    private QueryCEAR queryCEARUnprocessed;
    private List<CEActionRequestPropertyHeavy> sessCEARListUnprocessed;
    
    
    /* *** Code Enf Action Request Session Shelves ***  */
    private Person personForCEActionRequestSubmission;
    private User utilityUserToUpdate;
    
    // --- QUERY CEAR ---
    
    /**
     * central calling point for updating this bean's list of CEARs requiring processing
     */
    public void refreshCEARUnprocessedList(){
        SearchCoordinator sc = getSearchCoordinator();
        CaseCoordinator cc = getCaseCoordinator();
        try {
            QueryCEAR selectedQueryCEAR = sc.initQuery(QueryCEAREnum.UNPROCESSED, getSessionBean().getSessUser().getKeyCard());
            getSessionBean().setQueryCEARUnprocessed(sc.runQuery(selectedQueryCEAR));
            sessCEARListUnprocessed = cc.cear_getCEARPropertyHeavyList(getSessionBean().getQueryCEARUnprocessed().getBOBResultList());
        } catch (SearchException | BObStatusException | IntegrationException ex) {
                System.out.println(ex);
        }
    }
    
    
    
    
    
    /* --- -------------------------------------------------------------- --- */
    /* ---                     VIV OccApp                                 --- */
    /* --- -------------------------------------------------------------- --- */
    
    //Fields used both externally and internally
    private OccPermitApplication sessOccPermitApplication;
    private PersonType occPermitAppActivePersonType;
    private OccPermitApplicationReason occPermitApplicationReason;
    
    //Fields used externally
    private PublicInfoBundleProperty occPermitAppActiveProp;
    private PublicInfoBundleProperty occPermitAppWorkingProp;
    private PublicInfoBundlePropertyUnit occPermitAppActivePropUnit;
    private List<PublicInfoBundlePerson> occPermitAttachedPersons;
    private PublicInfoBundlePerson occPermitApplicant;
    private PublicInfoBundlePerson occPermitPreferredContact;
    private Map<String,PublicInfoBundlePropertyUnit> occPermitAlreadyApplied;
    
    //Fields only used internally
    private boolean unitDetermined;
    
    /* --- -------------------------------------------------------------- --- */
    /* ---                        X Payment                               --- */
    /* --- -------------------------------------------------------------- --- */

    private Payment sessPayment;
    private String paymentRedirTo;
    
    private OccPeriod feeManagementOccPeriod;
    private String feeRedirTo;
    private Payment sessionPayment;
    
    private EventRealm feeManagementDomain;
    private CECase feeManagementCeCase;
    
    /* --- -------------------------------------------------------------- --- */
    /* ---                         XI Report                              --- */
    /* --- -------------------------------------------------------------- --- */

    private Report sessReport;
    
    private ReportConfigCECase reportConfigCECase;
    private ReportConfigCECaseList reportConfigCECaseList;
    
    private ReportConfigCEEventList reportConfigEventList;
    
    private ReportConfigOccInspection reportConfigFieldInspection;
    private ReportConfigOccPermit reportConfigOccPermit;
    private ReportConfigOccActivity reportConfigOccActivity;
    
    /* --- -------------------------------------------------------------- --- */
    /* ---                         XII Blob                                --- */
    /* --- -------------------------------------------------------------- --- */
    
    private Blob sessBlob;
    private BlobLight sessBlobLight;
    /**
     * Since blobs are managed by a unified set of tools but are held
     * by all sorts of parent objects, this member holds the updated list
     * of BlobLight objects IF the current blob holder's list has changed. 
     * Clients of iface_blobHolder are responsiblef or checking this
     * member and if not null, taking the blobs out and injecting them into their
     * parent and then nulling this field out. Works in conjunction with the 
     * sessBlobLightListHolderForRefreshUptake which users of this semaphore 
     * can use to verify if the refreshed list is in fact for them to slurp up.
     */
    private List<BlobLight> sessBlobLightListForRefreshUptake;
    /**
     * A ref to the blob holder who should take up list this is stored here.
     * Used to sort out whose new list of blobs are sitting in the 
     * member sessBlobLightListForRefreshUptake. 
     * 
     * This is a hacky solution that is far from elegant. It was implemented 
     * in July 2023 during field inspection blob overhauls to deal with
     * the managedCaseblob tool slurping up a field inspection's blob list 
     * inappropriately.
     */
    private IFace_BlobHolder sessBlobLightListHolderForRefreshUptake;
    private IFace_BlobHolder sessBlobHolder;
    private BlobPool sessBlobHolderPool;
    private List<Blob> blobList;
    private List<BlobType> blobTypeList;
    
    /**
     * Writes null to both the refresh uptake blob list and refresh uptake blob holder ref
     */
   public void resetBlobRefreshUptakeFields(){
      sessBlobLightListForRefreshUptake = null;
      sessBlobLightListHolderForRefreshUptake = null;
   }
    
    /**
     * If there's a session blob holder, I figure out what type it is
     * and then I get a new one and point our session blob holder to it
     * @param bh
     * @return the session's BlobHolder
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BlobException
     */
    public IFace_BlobHolder setAndRefreshSessionBlobHolderAndBuildUpstreamPool(IFace_BlobHolder bh) 
            throws BObStatusException, IntegrationException, BlobException{
        BlobCoordinator bc = getBlobCoordinator();
        System.out.println("SessionBean.setAndRefreshSessionBlobHolderAndBuildUpstreamPool " );
        if(bh != null){
            sessBlobHolder = bh;
            
            sessBlobHolder.setBlobList(bc.getBlobLightList(sessBlobHolder));
            sessBlobLightListForRefreshUptake = sessBlobHolder.getBlobList();
            sessBlobLightListHolderForRefreshUptake = bh;
            // TURNED OFF IN JULY 2023 for leak testing--this is a candidate component for the dreaded leak
            if(false){
                // update upstream pool
                BlobLinkEnum upstreamPool = bh.getBlobUpstreamPoolEnum();
                if(upstreamPool != null){

                    sessBlobHolderPool = bc.getUpstreamBlobPool(bh);
                }
            }
        }
        return sessBlobHolder;
    }
    
    /* --- -------------------------------------------------------------- --- */
    /* ---                  XIII PublicInfoBundle                          --- */
    /* --- -------------------------------------------------------------- --- */
    
    private List<PublicInfoBundle> infoBundleList;
    private PublicInfoBundleCECase pibCECase;
    
    /* *** Public Person Search/Edit Session Shelves *** */
    private Person activeAnonPerson;

    /* *** Blob Upload Session Shelves *** */ 
    
    /* --- -------------------------------------------------------------- --- */
    /* ---                  NAVIGATION                                    --- */
    /* --- -------------------------------------------------------------- --- */
    
    /* *** Navigation Shelves *** */
    private NavigationStack navStack;
    
    /* --- -------------------------------------------------------------- --- */
    /* ---                  MANAGED LIST MANAGEMENT                       --- */
    /* --- -------------------------------------------------------------- --- */
    
   
    
   
   
     
    /**
     * If the member on this class called ceCaseForRefresh is not null
     * and is contained in the session list, that object is refreshed.
     * 
     */
    public void refreshCaseInSessionListIfRequested(){
        CaseCoordinator cc = getCaseCoordinator();
        if(ceCaseForRefresh != null && sessCECaseList != null && !sessCECaseList.isEmpty()){
            System.out.println("sessionBean.refreshCaseInSessionListIfRequested | found case for refresh id: " + ceCaseForRefresh.getCaseID());
            try {
                CECase freshCase = cc.cecase_getCECase(ceCaseForRefresh.getCaseID(), sessUser);
                int idx = sessCECaseList.indexOf(freshCase);
                if(idx != -1){
                    sessCECaseList.remove(freshCase);
                    sessCECaseList.add(idx, freshCase);
                }
                // reset our trigger
                ceCaseForRefresh = null;
            } catch (IntegrationException  ex) {
                System.out.println(ex);
            }
        }
    }
    
    
    /* --- -------------------------------------------------------------- --- */
    /* ---                  SESSION SERVICES                              --- */
    /* --- -------------------------------------------------------------- --- */
    
    /**
     * ECD 22-JUN-2023: not used
     */
    private LocalDateTime noteholderRefreshTimestampTrigger;
    
    
    /**
     * Interrogates the given aob for its type and gets a fresh version
     * of the object from the db and installs in the session; This facility
     * is used by organs that manage objects that work across objects such as events
     * that can be attached to permit files, cecases, and properties. 
     * @param aob 
     */
    public void refreshActivatableObject(IFaceActivatableBOB aob){
        if(aob != null){
            try {
                if(aob instanceof CECase cse){
                    CaseCoordinator cc = getCaseCoordinator();
                    sessCECase = cc.cecase_assembleCECaseDataHeavy(cc.cecase_getCECase(cse.getCaseID(), sessUser), sessUser);
                } else if(aob instanceof OccPeriod op){
                    OccupancyCoordinator oc = getOccupancyCoordinator();
                } else if (aob instanceof Parcel pcl){
                    PropertyCoordinator pc = getPropertyCoordinator();
                    sessProperty = pc.assemblePropertyDataHeavy(pc.getProperty(pcl.getParcelKey()), sessUser);
                } else {
                    System.out.println("SessionBean.refreshActivatableObject | unsupported aob for refresh: " + aob.getClass().getName());
                }
            } catch (IntegrationException | BObStatusException | BlobException | SearchException | AuthorizationException ex) {
                System.out.println(ex);
            }
                
        } else {
            System.out.println("SessionBean.refreshActivatableObject | null aob");
        }
    }
    
    /**
     * Pathway to turn off cross muni view and navigate to our muni property page;
     * Also sets our cross muni muni and TS to null and logs the exit in the DB
     * @param ua
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public String onExitCrossMuniViewMode() throws BObStatusException{
        UserCoordinator uc = getUserCoordinator();
        if(crossMuniViewMode){
            try {
                recordXMuniModeExit();
                crossMuniViewMode = false;
                crossMuniMuni = null;
                uc.auth_deauthorizeCrossMuniView(sessUser);
                System.out.println("SessionBB.onExitCrossMuniViewMode | leaving xmuni view mode");
                return navigateToPageCorrespondingToObject(sessMuni.getMuniPropertyDH());
            } catch (BObStatusException | AuthorizationException | IntegrationException ex) {
                System.out.println(ex);
                return "";
            } 
        }
        System.out.println("SessionBB.onExitCrossMuniViewMode | not in xmuni mode");
        return "";
    }
    
    /**
     * writes an exit Timestamp from xmuni mode in the DB
     */
    public void recordXMuniModeExit() throws BObStatusException, IntegrationException{
        if(crossMuniViewMode && xMuniRecord != null){
            SystemCoordinator sc = getSystemCoordinator();
            sc.logXMuniExit(xMuniRecord);
        }
        
    }
    
    /**
     * Front door for navigating to object page from a human link. As of the June 2023
     * Cross muni viewing implementation, this facility allows limited access to cecase
     * and property info of objects outside the current session municipality and this
     * method checks for this state and if triggered, turns on the crossMuniView flag
     * on this class for signaling to the UI and a bunch of logic checks for object
     * CRUD operations.
     * 
     * Activation of xmuni mode is logged through the systemcoordinator.
     * 
     * @param hlpoih
     * @param ua
     * @return 
     */
    public String evaluateHumanLinkParentForCrossMuniViewing(HumanLinkParentObjectInfoHeavy hlpoih, UserAuthorized ua) throws BObStatusException{
        if(hlpoih == null || ua == null){
            throw new BObStatusException("Cannot route to object page with null human link or user");
        }
        PersonCoordinator pc = getPersonCoordinator();
        UserCoordinator uc = getUserCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        System.out.println("SessionBean.evaluateHumanLinkParentForCrossMuniViewing | hlpioh descr: " + hlpoih.getParentObjectDescription());
        // see if we have a cross-muni human link parent
        try {
            if(hlpoih.getMuiniParent() != null && hlpoih.getMuiniParent().getMuniCode() != ua.getKeyCard().getGoverningAuthPeriod().getMuni().getMuniCode()){
                if(hlpoih.getLinkedObjectRole().getSchema().isALLOW_CROSS_MUNI_VIEW()){
                    crossMuniViewMode = true;
                    crossMuniMuni = hlpoih.getMuiniParent();
                    sessPerson = hlpoih;
                    uc.auth_authorizeUserForCrossMuniView(ua);
                    xMuniRecord = sc.getXMuniActivationRecord(sc.insertXMuniActivationRecord(sc.getXMuniActivationRecordSkeleton(), hlpoih, ua));
                    if(xMuniRecord != null){
                        System.out.println("SessionBean.evaluateHumanLinkParentForCrossMuniViewing | xmrid: " + xMuniRecord.getActivationID());
                    } else {
                        System.out.println("SessionBean.evaluateHumanLinkParentForCrossMuniViewing | no xmuniRecord error");
                    }
                }
            } else {
                crossMuniViewMode = false;
            }
            return navigateToPageCorrespondingToObject((IFaceActivatableBOB) pc.getParentObjectOfHumanLink(hlpoih, ua));
        } catch(AuthorizationException | BObStatusException | BlobException | EventException | IntegrationException | SearchException ex){
            System.out.println(ex);
            throw new BObStatusException("SessionBean.evaluateHumanLinkParentForCrossMuniViewing | Caught any of a gazillion exceptions during cross-muni viewing evaluation of human link.");
        }
    }
    
    
    /**
     * Primary entrance point for ALL requests to make a given Business Object
     * the session active one.This means all other session active objects 
     *   must be synchronized against the requesting BOB, and in some cases 
     *   loading appropriate supporting objects. For example, choosing to make a case active will require checking the 
     *   current property to make sure it's the host of the case, if not, 
     *   the current property will be updated to be compatible with the session 
     *   active one. The session person will also be synchronized, along with the permit file;
     *   If the chosen property doesn't have a person link or a permit file, then 
     *   those session object fields will be nulled out
     * 
     * @param bob reqested object to become the session active one. This will
     * get flagged with a marker enum indicating that's it's the user chosen
     * object
     * @return the page to navigate to
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public String navigateToPageCorrespondingToObject(IFaceActivatableBOB bob) throws BObStatusException, AuthorizationException{
        PropertyCoordinator pc = getPropertyCoordinator();
        PersonCoordinator perc = getPersonCoordinator();
        CaseCoordinator cc = getCaseCoordinator();
        PermissionsCoordinator permCoor = getPermissionsCoordinator();
        OccupancyCoordinator oc = getOccupancyCoordinator();
        SessionEventConductor sec = getSessionEventConductor();
        // common alias for sessionUser
        UserAuthorized ua = sessUser;
        sessionDomain = FocusedObjectEnum.UNKNOWN;
        
        
        if(bob == null){
            throw new BObStatusException("Cannot activate a null Activatable bob");
        }
        
        try {

            // first check for muni fencing
            /*
            ********** BEGIN CASCADING LOGIC FOR COORDINATING SESSION OBJECTS ******
            */
            
            
            // ***************************************************************** 
            // ***************** ACTIVATE THE SESSION CONDUCTORS ***************
            // ***************************************************************** 
            if(bob instanceof  IFaceSessionSyncTarget target && sessionSyncRegistry != null && !sessionSyncRegistry.isEmpty()){
                for(IFaceSessionSynchronizer syncer: sessionSyncRegistry){
                    syncer.synchronizeToSessionFocusObject(target);
                }
            }
            
            // ************************************ 
            // ************* PROPERTY ************* 
            // ************************************ 
            if (bob instanceof Property prop) {
                PropertyDataHeavy pdh = pc.assemblePropertyDataHeavy(prop, ua);
                sessProperty = pdh;
                permCoor.enforceMuniViewFence(pdh, ua);
                // PERSONS
                sessPersonList = perc.getPersonListFromHumanLinkList(pdh.gethumanLinkList());
                
                // leave our session person alone if our property doesn't have a person list
                if(sessPersonList != null && !sessPersonList.isEmpty()){
                    sessPerson = perc.getPerson(sessPersonList.get(0));
                }
                
                if(sessProperty.getAddress() != null){
                    sessMailingAddress = sessProperty.getAddress();
                } else {
                    sessMailingAddress = null;
                }
                
                // CASES 
                sessCECaseList = cc.cecase_upcastCECaseDataHeavyList(pdh.getCeCaseList());
                if (sessCECaseList == null || sessCECaseList.isEmpty()) {
                    sessCECase = null;
                } else {
                    if (sessCECaseList != null && !sessCECaseList.isEmpty()) {
                        sessCECase = cc.cecase_assembleCECaseDataHeavy(cc.cecase_getCECase(sessCECaseList.get(0).getCaseID(),ua), ua); 
                    }
                }
                
                               
                // events
                getSessionEventConductor().setSessEventsPageEventDomainRequest(EventRealm.PARCEL);
                
                // blobs
                sessBlobHolder = sessProperty;
                
                // system switches
                sessionDomain = FocusedObjectEnum.PROPERTY;
                
            // ************************************ 
            // ************* PERSON************* 
            // ************************************ 
            } else if (bob instanceof Person) {
                // Peson selection doesn't trigger changes in session property, case, or permit file
               Person pers = perc.getPerson((Person) bob);
                
               sessPerson = pers;
               sessionDomain = FocusedObjectEnum.PERSON;
                
            // ************************************ 
            // ************* CE Case  ************* 
            // ************************************ 
            } else if (bob instanceof CECase cse) {
                // added during cecase violation citation update work to ensure that 
                // when we route to a cecase we're getting the fresh one
                cc.cecase_forceCaseCacheDump(cse);
                CECaseDataHeavy csedh = cc.cecase_assembleCECaseDataHeavy(cc.cecase_getCECase(cse.getCaseID(), ua), ua);
                // make sure property is the one hosting the case
                sessCECase = csedh;
                permCoor.enforceMuniViewFence(csedh, ua);
                
                sessProperty = pc.assemblePropertyDataHeavy(pc.getProperty(cse.getParcelKey()), ua);
                synchronizeOccPeriodAndPersonsFromSessProperty();
                getSessionEventConductor().setSessEventsPageEventDomainRequest(EventRealm.CODE_ENFORCEMENT);
                getSessionInspectionConductor().registerSessionInspectable(sessCECase);
                
                // blobs
                sessBlobHolder = sessCECase;
                
                // system switches
                sessionDomain = FocusedObjectEnum.CECASE;

            // ************************************ 
            // ***** Citation *******
            // ************************************ 
            } else if (bob instanceof Citation cit){
                sessCECase = cc.cecase_assembleCECaseDataHeavy(cc.cecase_getCECase(cit.getCecaseID(), ua), ua);
                sessProperty = pc.assemblePropertyDataHeavy(pc.getProperty(sessCECase.getParcelKey()), ua);
                synchronizeOccPeriodAndPersonsFromSessProperty();
                getSessionEventConductor().setSessEventsPageEventDomainRequest(EventRealm.CODE_ENFORCEMENT);
                getSessionInspectionConductor().registerSessionInspectable(sessCECase);
                
                // blobs
                sessBlobHolder = cit;
                
                // system switches
                sessionDomain = FocusedObjectEnum.CECASE;
            
            // ************************************ 
            // ***** Citation Docket *******
            // ************************************ 
            
            } else if(bob instanceof CitationDocketRecord cdr){
                sessCECase = cc.cecase_assembleCECaseDataHeavy(cc.cecase_getCECase(cc.citation_getCitation(cdr.getCitationID()).getCecaseID(), ua), ua); 
                sessProperty = pc.assemblePropertyDataHeavy(pc.getProperty(sessCECase.getParcelKey()), ua);
                synchronizeOccPeriodAndPersonsFromSessProperty();
                getSessionEventConductor().setSessEventsPageEventDomainRequest(EventRealm.CODE_ENFORCEMENT);
                sessionDomain = FocusedObjectEnum.CECASE;
                getSessionInspectionConductor().registerSessionInspectable(sessCECase);
            
                // blobs
                sessBlobHolder = sessCECase;
            
            // ************************************ 
            // ***** OCC Period/ Permit file*******
            // ************************************ 
            } else if (bob instanceof OccPeriod period) {
                sessionDomain = FocusedObjectEnum.OCCPERIOD;
                
                // Set current property to match the sessOccPeriod's propertyUnit
                setSessProperty(period.getParentParcelKey());
               
                try {
                    sessPropertyUnit = pc.getPropertyUnitDataHeavy(pc.getPropertyUnit(period.getPropertyUnitID()), ua);
                } catch (EventException ex) {
                    System.out.println(ex);
                }

                // Set CECase
                sessCECaseList = cc.cecase_upcastCECaseDataHeavyList(sessProperty.getCeCaseList());
                if (sessCECaseList == null || sessCECaseList.isEmpty()) {
                    sessCECase = null;
                } else {
                    if (sessCECaseList != null && !sessCECaseList.isEmpty()) {
                        setSessCECase(sessCECaseList.get(0));
                    }
                }
                
                // Events
                getSessionEventConductor().setSessEventsPageEventDomainRequest(EventRealm.OCCUPANCY);
                // Inspections
                getSessionInspectionConductor().registerSessionInspectable(getSessionOccupancyConductor().getSessOccPeriod());
                // blobs
                sessBlobHolder = getSessionOccupancyConductor().getSessOccPeriod();
                
                

            // ************************************ 
            // ************ Occ Permit ************
            // ************************************ 
                // as of March 2023, this route is not yet in service
            }  else if (bob instanceof OccPermit permit){
                OccPeriod period = oc.getOccPeriod(permit.getPeriodID(), ua);

                if (sessCECaseList != null && !sessCECaseList.isEmpty()) {
                    setSessCECase(sessCECaseList.get(0));
                }
                getSessionEventConductor().setSessEventsPageEventDomainRequest(EventRealm.OCCUPANCY);
                
                // blobs
                sessBlobHolder = getSessionOccupancyConductor().getSessOccPeriod();
                sessionDomain = FocusedObjectEnum.OCCPERIOD;
            }
            else {
                throw new BObStatusException("Unsupported instance of ActivatableBOB sent with call to setSessionActiveObject");
            }
        } catch (BObStatusException | IntegrationException | SearchException | BlobException  ex) {
            System.out.println(ex);
        } 
        sec.setSessEventsPageEventDomainRequest(sessionDomain.getEventRealmMapping());
        return sessionDomain.getNavString();
    }
    
    /**
     * Logic bundle for setting the session occ period and person based on the current session property 
     * which the caller should have set. When setting the session person
     * I will let the session person stand if cross muni mode is enabled since
     * my default switching of the session person to the first linked person will not
     * necessarily be the conduit for entering cross-muni mode
     * 
     */
    private void synchronizeOccPeriodAndPersonsFromSessProperty() throws IntegrationException, BObStatusException{
        PersonCoordinator perc = getPersonCoordinator();
        OccupancyCoordinator oc = getOccupancyCoordinator();
        // don't change my session person if I'm jumping into xmuni mode
        if(!crossMuniViewMode){
            sessPersonList = perc.getPersonListFromHumanLinkList(sessProperty.gethumanLinkList()); 
            // leave our session person alone unless our case has persons assocaited with it
            if(sessPersonList != null && !sessPersonList.isEmpty()){
                sessPerson = perc.getPerson(sessPersonList.get(0)); 
            }
            getSessionConductor().synchronizeSessionObjects(sessProperty);
        }
    }
    
    
   
    
     /**
     * Indicates if we're on the property profile page because our focus object is our property
     * @return 
     */
    public boolean isPropertyPageFocus(){
        return sessionDomain != null && sessionDomain == FocusedObjectEnum.PROPERTY;
    }

    /**
     * @return the sessProperty
     */
    public PropertyDataHeavy getSessProperty() {
        return sessProperty;
    }

    /**
     * @return the sessCECase
     */
    public CECaseDataHeavy getSessCECase() {
        return sessCECase;
        
    }
    
   
    /**
     * @return the sessNotice
     */
    public NoticeOfViolation getSessNotice() {
        return sessNotice;
    }
    
    public void setSessCodeSet(CodeSet cs){
        sessCodeSet = cs;
    }

    

    /**
     * Adaptor method to preserve backward compatability;
     * The MuniHeavy stores the active copy of these 
     * @return the activeCodeSet
     */
    public CodeSet getSessCodeSet() {
//        if(sessMuni != null){
//            activeCodeSet = sessMuni.getCodeSet();
//        }
        return sessCodeSet;
    }

    /**
     * @return the sessCitation
     */
    public Citation getSessCitation() {
        return sessCitation;
    }


    /**
     * @return the sessCodeViolation
     */
    public CodeViolation getSessCodeViolation() {
        return sessCodeViolation;
    }

    /**
     * @return the sessViolationList
     */
    public List<CodeViolation> getSessViolationList() {
        return sessViolationList;
    }

    /**
     * @return the activeCodeElementGuideEntry
     */
    public CodeElementGuideEntry getActiveCodeElementGuideEntry() {
        return activeCodeElementGuideEntry;
    }

   

    /**
     * @param sessCECase the sessCECase to set
     */
    public void setSessCECase(CECaseDataHeavy sessCECase) {
        this.sessCECase = sessCECase;
    }



   
    /**
     * @param sessNotice the sessNotice to set
     */
    public void setSessNotice(NoticeOfViolation sessNotice) {
        this.sessNotice = sessNotice;
    }


    /**
     * @param sessCitation the sessCitation to set
     */
    public void setSessCitation(Citation sessCitation) {
        this.sessCitation = sessCitation;
    }

    /**
     * @param sessEnforcableCodeElement the sessEnforcableCodeElement to set
     */
    public void setSessEnforcableCodeElement(EnforceableCodeElement sessEnforcableCodeElement) {
        this.sessEnforcableCodeElement = sessEnforcableCodeElement;
    }

    /**
     * @param sessCodeViolation the sessCodeViolation to set
     */
    public void setSessCodeViolation(CodeViolation sessCodeViolation) {
        this.sessCodeViolation = sessCodeViolation;
    }

    /**
     * @param sessViolationList the sessViolationList to set
     */
    public void setSessViolationList(List<CodeViolation> sessViolationList) {
        this.sessViolationList = sessViolationList;
    }

    /**
     * @param activeCodeElementGuideEntry the activeCodeElementGuideEntry to set
     */
    public void setActiveCodeElementGuideEntry(CodeElementGuideEntry activeCodeElementGuideEntry) {
        this.activeCodeElementGuideEntry = activeCodeElementGuideEntry;
    }

    /**
     * @return the utilityUserToUpdate
     */
    public User getUtilityUserToUpdate() {
        return utilityUserToUpdate;
    }

    /**
     * @return the sessCodeSource
     */
    public CodeSource getSessCodeSource() {
        return sessCodeSource;
    }

    /**
     * @param utilityUserToUpdate the utilityUserToUpdate to set
     */
    public void setUtilityUserToUpdate(User utilityUserToUpdate) {
        this.utilityUserToUpdate = utilityUserToUpdate;
    }

    /**
     * @param sessCodeSource the sessCodeSource to set
     */
    public void setSessCodeSource(CodeSource sessCodeSource) {
        this.sessCodeSource = sessCodeSource;
    }

    /**
     * @return the sessMuni
     */
    public MunicipalityDataHeavy getSessMuni() {
        return sessMuni;
    }

    /**
     * @param sessMuni the sessMuni to set
     */
    public void setSessMuni(MunicipalityDataHeavy sessMuni) {
        this.sessMuni = sessMuni;
    }

    /**
     * @return the activeCodeElement
     */
    public CodeElement getActiveCodeElement() {
        return activeCodeElement;
    }

    /**
     * @param activeCodeElement the activeCodeElement to set
     */
    public void setActiveCodeElement(CodeElement activeCodeElement) {
        this.activeCodeElement = activeCodeElement;
    }

  

    /**
     * @return the infoBundleList
     */
    public List<PublicInfoBundle> getInfoBundleList() {
        return infoBundleList;
    }

    /**
     * @param infoBundleList the infoBundleList to set
     */
    public void setInfoBundleList(List<PublicInfoBundle> infoBundleList) {
        this.infoBundleList = infoBundleList;
    }

    /**
     * @return the pibCECase
     */
    public PublicInfoBundleCECase getPibCECase() {
        return pibCECase;
    }

    /**
     * @param pibCECase the pibCECase to set
     */
    public void setPibCECase(PublicInfoBundleCECase pibCECase) {
        this.pibCECase = pibCECase;
    }

    /**
     * @return the sessCEARListUnprocessed
     */
    public List<CEActionRequestPropertyHeavy> getSessCEARListUnprocessed() {
        
        return sessCEARListUnprocessed;
    }

    /**Special getter that refreshes a case in this list
     * before returning the list if the ceCaseForRefresh
     * is not null and that case is in this list.
     * 
     * @return the sessCECaseList
     */
    public List<CECase> getSessCECaseList() {
        refreshCaseInSessionListIfRequested();
//        System.out.println("SessionBean.getSessCECaseList");
        return sessCECaseList;
    }

    /**
     * @param qc
     */
    public void setSessCEARListUnprocessed(List<CEActionRequestPropertyHeavy> qc) {
        this.sessCEARListUnprocessed = qc;
    }
    
    
    /**
     * Setter for session CE Case objects
     * @param cse which will be fetched from the DB again and then assembled
     * into a DataHeavy version
     */
    public void setSessCECase(CECase cse){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            sessCECase = cc.cecase_assembleCECaseDataHeavy(cc.cecase_getCECase(cse.getCaseID(),sessUser), sessUser);
        } catch (BObStatusException | IntegrationException | SearchException ex) {
            System.out.println(ex);
        }
        
        
    }
    
    /**
     * @param sessCECaseList the sessCECaseList to set
     */
    public void setSessCECaseList(List<CECase> sessCECaseList) {
        this.sessCECaseList = sessCECaseList;
    }

    /**
     * @return the sessCEAR
     */
    public CEActionRequestPropertyHeavy getSessCEAR() {
        return sessCEAR;
    }

    /**
     * @param sessCEAR the sessCEAR to set
     */
    public void setSessCEAR(CEActionRequestPropertyHeavy sessCEAR) {
        this.sessCEAR = sessCEAR;
    }

    /**
     * @return the sessUser
     */
    
    public UserAuthorized getSessUser() {
        return sessUser;
    }

    /**
     * @param sessUser the sessUser to set
     */
    
    public void setSessUser(UserAuthorized sessUser) {
        this.sessUser = sessUser;
    }

   
    /**
     * @return the personForCEActionRequestSubmission
     */
    public Person getPersonForCEActionRequestSubmission() {
        return personForCEActionRequestSubmission;
    }

    /**
     * @param personForCEActionRequestSubmission the personForCEActionRequestSubmission to set
     */
    public void setPersonForCEActionRequestSubmission(Person personForCEActionRequestSubmission) {
        this.personForCEActionRequestSubmission = personForCEActionRequestSubmission;
    }

    
    public OccPermitApplication getSessOccPermitApplication() {
        return sessOccPermitApplication;
    }

    public void setSessOccPermitApplication(OccPermitApplication sessOccPermitApplication) {
        this.sessOccPermitApplication = sessOccPermitApplication;
    }

    public PropertyUnitDataHeavy getSessPropertyUnit() {
        return sessPropertyUnit;
    }

    public void setSessPropertyUnit(PropertyUnitDataHeavy sessPropertyUnit) {
        this.sessPropertyUnit = sessPropertyUnit;
    }

  

    /**
     * @return the sessPropertyList
     */
    public List<Property> getSessPropertyList() {
        return sessPropertyList;
    }

    /**
     * @param sessPropertyList the sessPropertyList to set
     */
    public void setSessPropertyList(List<Property> sessPropertyList) {
        this.sessPropertyList = sessPropertyList;
    }


    /**
     * @return the reportConfigCECase
     */
    public ReportConfigCECase getReportConfigCECase() {
        return reportConfigCECase;
    }

    /**
     * @param reportConfigCECase the reportConfigCECase to set
     */
    public void setReportConfigCECase(ReportConfigCECase reportConfigCECase) {
        this.reportConfigCECase = reportConfigCECase;
    }

    /**
     * @return the sessReport
     */
    public Report getSessReport() {
        return sessReport;
    }

    /**
     * @param sessReport the sessReport to set
     */
    public void setSessReport(Report sessReport) {
        this.sessReport = sessReport;
    }

    /**
     * @return the reportConfigCECaseList
     */
    public ReportConfigCECaseList getReportConfigCECaseList() {
        return reportConfigCECaseList;
    }

    /**
     * @param reportConfigCECaseList the reportConfigCECaseList to set
     */
    public void setReportConfigCECaseList(ReportConfigCECaseList reportConfigCECaseList) {
        this.reportConfigCECaseList = reportConfigCECaseList;
    }

    /**
     * @return the reportConfigEventList
     */
    public ReportConfigCEEventList getReportConfigEventList() {
        return reportConfigEventList;
    }

    /**
     * @param reportConfigEventList the reportConfigEventList to set
     */
    public void setReportConfigEventList(ReportConfigCEEventList reportConfigEventList) {
        this.reportConfigEventList = reportConfigEventList;
    }

    /**
     * @return the queryCEARUnprocessed
     */
    public QueryCEAR getQueryCEARUnprocessed() {
        return queryCEARUnprocessed;
    }

    /**
     * @param queryCEARUnprocessed the queryCEARUnprocessed to set
     */
    public void setQueryCEARUnprocessed(QueryCEAR queryCEARUnprocessed) {
        this.queryCEARUnprocessed = queryCEARUnprocessed;
    }

  

   

    /**
     * @return the queryCECase
     */
    public QueryCECase getQueryCECase() {
        return queryCECase;
    }

    /**
     * @param queryCECase the queryCECase to set
     */
    public void setQueryCECase(QueryCECase queryCECase) {
        this.queryCECase = queryCECase;
    }

    /**
     * @return the activeAnonPerson
     */
    public Person getActiveAnonPerson() {
        return activeAnonPerson;
    }

    /**
     * @param activeAnonPerson the activeAnonPerson to set
     */
    public void setActiveAnonPerson(Person activeAnonPerson) {
        this.activeAnonPerson = activeAnonPerson;
    }

    /**
     * @return the occPermitApplicationReason
     */
    public OccPermitApplicationReason getOccPermitApplicationReason() {
        return occPermitApplicationReason;
    }

    /**
     * @param occPermitApplicationReason the occPermitApplicationReason to set
     */
    public void setOccPermitApplicationReason(OccPermitApplicationReason occPermitApplicationReason) {
        this.occPermitApplicationReason = occPermitApplicationReason;
    }

  
   
  
   

    /**
     * @return the queryProperty
     */
    public QueryProperty getQueryProperty() {
        return queryProperty;
    }

    /**
     * @return the queryPerson
     */
    public QueryPerson getQueryPerson() {
        return queryPerson;
    }


    /**
     * @param queryProperty the queryProperty to set
     */
    public void setQueryProperty(QueryProperty queryProperty) {
        this.queryProperty = queryProperty;
    }

    /**
     * @param queryPerson the queryPerson to set
     */
    public void setQueryPerson(QueryPerson queryPerson) {
        this.queryPerson = queryPerson;
    }


    /**
     * @return the occPermitAppActiveProp
     */
    public PublicInfoBundleProperty getOccPermitAppActiveProp() {
        return occPermitAppActiveProp;
    }

    /**
     * @return the occPermitAppWorkingProp
     */
    public PublicInfoBundleProperty getOccPermitAppWorkingProp() {
        return occPermitAppWorkingProp;
    }

    /**
     * @param activeProp the occPermitAppActiveProp to set
     */
    public void setOccPermitAppActiveProp(PublicInfoBundleProperty activeProp) {
        this.occPermitAppActiveProp = activeProp;
    }

    /**
     * @param workingProp the occPermitAppWorkingProp to set
     */
    public void setOccPermitAppWorkingProp(PublicInfoBundleProperty workingProp) {
        this.occPermitAppWorkingProp = workingProp;
    }

    /**
     * @return the occPermitAppActivePropUnit
     */
    public PublicInfoBundlePropertyUnit getOccPermitAppActivePropUnit() {
        return occPermitAppActivePropUnit;
    }

    /**
     * @param occPermitAppActivePropUnit the occPermitAppActivePropUnit to set
     */
    public void setOccPermitAppActivePropUnit(PublicInfoBundlePropertyUnit occPermitAppActivePropUnit) {
        this.occPermitAppActivePropUnit = occPermitAppActivePropUnit;
    }

    public Map<String, PublicInfoBundlePropertyUnit> getOccPermitAlreadyApplied() {
        return occPermitAlreadyApplied;
    }

    public void setOccPermitAlreadyApplied(Map<String, PublicInfoBundlePropertyUnit> occPermitAlreadyApplied) {
        this.occPermitAlreadyApplied = occPermitAlreadyApplied;
    }
    
    /**
     * @return the occPermitAppActivePersonType
     */
    public PersonType getOccPermitAppActivePersonType() {
        return occPermitAppActivePersonType;
    }

    /**
     * @param occPermitAppActivePersonType the occPermitAppActivePersonType to set
     */
    public void setOccPermitAppActivePersonType(PersonType occPermitAppActivePersonType) {
        this.occPermitAppActivePersonType = occPermitAppActivePersonType;
    }

    /**
     * @return the reportConfigOccPermit
     */
    public ReportConfigOccPermit getReportConfigOccPermit() {
        return reportConfigOccPermit;
    }

    /**
     * @param reportConfigOccPermit the reportConfigOccPermit to set
     */
    public void setReportConfigOccPermit(ReportConfigOccPermit reportConfigOccPermit) {
        this.reportConfigOccPermit = reportConfigOccPermit;
    }

    /**
     * @return the reportConfigFieldInspection
     */
    public ReportConfigOccInspection getReportConfigFieldInspection() {
        return reportConfigFieldInspection;
    }

    /**
     * @param reportConfigFieldInspection the reportConfigFieldInspection to set
     */
    public void setReportConfigFieldInspection(ReportConfigOccInspection reportConfigFieldInspection) {
        this.reportConfigFieldInspection = reportConfigFieldInspection;
    }

    /**
     * @return the userForReInit
     */
    public User getUserForReInit() {
        return userForReInit;
    }

    /**
     * @param userForReInit the userForReInit to set
     */
    public void setUserForReInit(User userForReInit) {
        this.userForReInit = userForReInit;
    }

    /**
     * @return the umapRequestedForReInit
     */
    public UserMuniAuthPeriod getUmapRequestedForReInit() {
        return umapRequestedForReInit;
    }

    /**
     * @param umapRequestedForReInit the umapRequestedForReInit to set
     */
    public void setUmapRequestedForReInit(UserMuniAuthPeriod umapRequestedForReInit) {
        this.umapRequestedForReInit = umapRequestedForReInit;
    }

    /**
     * @return the blobList
     */
    public List<Blob> getBlobList() {
        return blobList;
    }

    /**
     * @param blobList the blobList to set
     */
    public void setBlobList(List<Blob> blobList) {
        this.blobList = blobList;
    }

    /**
     * @return the sessPayment
     */
    public Payment getSessPayment() {
        return sessPayment;
    }

    /**
     * @return the feeManagementOccPeriod
     */
    public OccPeriod getFeeManagementOccPeriod() {
        return feeManagementOccPeriod;
    }

    /**
     * @return the feeRedirTo
     */
    public String getFeeRedirTo() {
        return feeRedirTo;
    }

    /**
     * @param sessPayment the sessPayment to set
     */
    public void setSessPayment(Payment sessPayment) {
        this.sessPayment = sessPayment;
    }
    /**
     * @param feeManagementOccPeriod the feeManagementOccPeriod to set
     */
    public void setFeeManagementOccPeriod(OccPeriod feeManagementOccPeriod) {
        this.feeManagementOccPeriod = feeManagementOccPeriod;
    }

    public CECase getFeeManagementCeCase() {
        return feeManagementCeCase;
    }

    public void setFeeManagementCeCase(CECase feeManagementCeCase) {
        this.feeManagementCeCase = feeManagementCeCase;
    }

    public EventRealm getFeeManagementDomain() {
        return feeManagementDomain;
    }

    public void setFeeManagementDomain(EventRealm feeManagementDomain) {
        this.feeManagementDomain = feeManagementDomain;
    }

    public NavigationStack getNavStack() {
        return navStack;
    }

    public void setNavStack(NavigationStack navStack) {
        this.navStack = navStack;
    }

    /**
     * @return the sessBlob
     */
    public Blob getSessBlob() {
        return sessBlob;
    }

    /**
     * @param sessBlob the sessBlob to set
     */
    public void setSessBlob(Blob sessBlob) {
        this.sessBlob = sessBlob;
    }

   

    /**
     * Sets the current session occupancy period to the heavy value of opBase
     * @param occPeriodBase
     */
    public void setSessOccPeriodFromPeriodBase(OccPeriod occPeriodBase) {
        
    }

   

    /**
     * @return the queryPersonList
     */
    public List<QueryPerson> getQueryPersonList() {
        return queryPersonList;
    }

    /**
     * @param queryPersonList the queryPersonList to set
     */
    public void setQueryPersonList(List<QueryPerson> queryPersonList) {
        this.queryPersonList = queryPersonList;
    }

   
    /**
     * @return the queryCECaseList
     */
    public List<QueryCECase> getQueryCECaseList() {
        return queryCECaseList;
    }

    /**
     * @param queryCECaseList the queryCECaseList to set
     */
    public void setQueryCECaseList(List<QueryCECase> queryCECaseList) {
        this.queryCECaseList = queryCECaseList;
    }

    /**
     * @return the queryCEARList
     */
    public List<QueryCEAR> getQueryCEARList() {
        return queryCEARList;
    }

    /**
     * @param queryCEARList the queryCEARList to set
     */
    public void setQueryCEARList(List<QueryCEAR> queryCEARList) {
        this.queryCEARList = queryCEARList;
    }



    /**
     * @return the sessUMAPListValidOnly
     */
    public List<UserMuniAuthPeriod> getSessUMAPListValidOnly() {
        return sessUMAPListValidOnly;
    }

    /**
     * @param sessUMAPListValidOnly the sessUMAPListValidOnly to set
     */
    public void setSessUMAPListValidOnly(List<UserMuniAuthPeriod> sessUMAPListValidOnly) {
        this.sessUMAPListValidOnly = sessUMAPListValidOnly;
    }

    /**
     * @return the sessPerson
     */
    public Person getSessPerson() {
        return sessPerson;
    }

    /**
     * @param sessPerson the sessPerson to set
     */
    public void setSessPerson(Person sessPerson) {
        this.sessPerson = sessPerson;
    }

    /**
     * @return the sessPersonQueued
     */
    public Person getSessPersonQueued() {
        return sessPersonQueued;
    }

    /**
     * @param sessPersonQueued the sessPersonQueued to set
     */
    public void setSessPersonQueued(Person sessPersonQueued) {
        this.sessPersonQueued = sessPersonQueued;
    }

    /**
     * @return the sessPersonList
     */
    public List<Person> getSessPersonList() {
        return sessPersonList;
    }

    /**
     * @param sessPersonList the sessPersonList to set
     */
    public void setSessPersonList(List<Person> sessPersonList) {
        this.sessPersonList = sessPersonList;
    }

    /**
     * @return the sessUserQueued
     */
    public User getSessUserQueued() {
        return sessUserQueued;
    }

    /**
     * @param sessUserQueued the sessUserQueued to set
     */
    public void setSessUserQueued(User sessUserQueued) {
        this.sessUserQueued = sessUserQueued;
    }

    /**
     * @return the sessMuniQueued
     */
    public Municipality getSessMuniQueued() {
        return sessMuniQueued;
    }

    /**
     * @param sessMuniQueued the sessMuniQueued to set
     */
    public void setSessMuniQueued(Municipality sessMuniQueued) {
        this.sessMuniQueued = sessMuniQueued;
    }

  


  
    /**
     * @return the paymentRedirTo
     */
    public String getPaymentRedirTo() {
        return paymentRedirTo;
    }

    /**
     * @return the sessionPayment
     */
    public Payment getSessionPayment() {
        return sessionPayment;
    }

    /**
     * @param paymentRedirTo the paymentRedirTo to set
     */
    public void setPaymentRedirTo(String paymentRedirTo) {
        this.paymentRedirTo = paymentRedirTo;
    }

    /**
     * @param sessionPayment the sessionPayment to set
     */
    public void setSessionPayment(Payment sessionPayment) {
        this.sessionPayment = sessionPayment;
    }


        
    public List<PublicInfoBundlePerson> getOccPermitAttachedPersons() {
        return occPermitAttachedPersons;
    }

    public void setOccPermitAttachedPersons(List<PublicInfoBundlePerson> occPermitAttachedPersons) {
        this.occPermitAttachedPersons = occPermitAttachedPersons;
    }

    public PublicInfoBundlePerson getOccPermitApplicant() {
        return occPermitApplicant;
    }

    public void setOccPermitApplicant(PublicInfoBundlePerson occPermitApplicant) {
        this.occPermitApplicant = occPermitApplicant;
    }

    public PublicInfoBundlePerson getOccPermitPreferredContact() {
        return occPermitPreferredContact;
    }

    public void setOccPermitPreferredContact(PublicInfoBundlePerson occPermitPreferredContact) {
        this.occPermitPreferredContact = occPermitPreferredContact;
    }

    public boolean isUnitDetermined() {
        return unitDetermined;
    }

    public void setUnitDetermined(boolean unitDetermined) {
        this.unitDetermined = unitDetermined;
    }
    
    /**
     * @return the userForConfig
     */
    public UserAuthorizedForConfig getUserForConfig() {
        return userForConfig;
    }

    /**
     * @param userForConfig the userForConfig to set
     */
    public void setUserForConfig(UserAuthorizedForConfig userForConfig) {
        this.userForConfig = userForConfig;
    }

    /**
     * @return the onPageLoad_sessionSwitch_viewProfile
     */
    public boolean isOnPageLoad_sessionSwitch_viewProfile() {
        return onPageLoad_sessionSwitch_viewProfile;
    }

    /**
     * @param onPageLoad_sessionSwitch_viewProfile the onPageLoad_sessionSwitch_viewProfile to set
     */
    public void setOnPageLoad_sessionSwitch_viewProfile(boolean onPageLoad_sessionSwitch_viewProfile) {
        this.onPageLoad_sessionSwitch_viewProfile = onPageLoad_sessionSwitch_viewProfile;
    }

    /**
     * @return the sessMailingCityStateZip
     */
    public MailingCityStateZip getSessMailingCityStateZip() {
        return sessMailingCityStateZip;
    }

    /**
     * @param sessMailingCityStateZip the sessMailingCityStateZip to set
     */
    public void setSessMailingCityStateZip(MailingCityStateZip sessMailingCityStateZip) {
        this.sessMailingCityStateZip = sessMailingCityStateZip;
    }

    /**
     * @return the sessBlobLight
     */
    public BlobLight getSessBlobLight() {
        return sessBlobLight;
    }

    /**
     * @return the sessBlobHolder
     */
    public IFace_BlobHolder getSessBlobHolder() {
        return sessBlobHolder;
    }

    /**
     * @param sessBlobLight the sessBlobLight to set
     */
    public void setSessBlobLight(BlobLight sessBlobLight) {
        this.sessBlobLight = sessBlobLight;
    }
    
    


    /**
     * I'm a normal setter
     * 
     * @param sessBlobHolder the sessBlobHolder to set
     */
    public void setSessBlobHolder(IFace_BlobHolder sessBlobHolder) {
       
        this.sessBlobHolder = sessBlobHolder;
    }

    /**
     * @return the blobTypeList
     */
    public List<BlobType> getBlobTypeList() {
        return blobTypeList;
    }

    /**
     * @param blobTypeList the blobTypeList to set
     */
    public void setBlobTypeList(List<BlobType> blobTypeList) {
        this.blobTypeList = blobTypeList;
    }

    
    /**
     * @return the sessBlobHolderPool
     */
    public BlobPool getSessBlobHolderPool() {
        return sessBlobHolderPool;
    }

    /**
     * @param sessBlobHolderPool the sessBlobHolderPool to set
     */
    public void setSessBlobHolderPool(BlobPool sessBlobHolderPool) {
        this.sessBlobHolderPool = sessBlobHolderPool;
    }

    /**
     * @return the sessChecklistTemplate
     */
    public OccChecklistTemplate getSessChecklistTemplate() {
        return sessChecklistTemplate;
    }

    /**
     * @param sessChecklistTemplate the sessChecklistTemplate to set
     */
    public void setSessChecklistTemplate(OccChecklistTemplate sessChecklistTemplate) {
        this.sessChecklistTemplate = sessChecklistTemplate;
    }

    /**
     * @return the sessOccSpaceType
     */
    public OccSpaceType getSessOccSpaceType() {
        return sessOccSpaceType;
    }

    /**
     * @param sessOccSpaceType the sessOccSpaceType to set
     */
    public void setSessOccSpaceType(OccSpaceType sessOccSpaceType) {
        this.sessOccSpaceType = sessOccSpaceType;
    }

    /**
     * @return the sessCodeGuideList
     */
    public List<CodeElementGuideEntry> getSessCodeGuideList() {
        return sessCodeGuideList;
    }

    /**
     * @param sessCodeGuideList the sessCodeGuideList to set
     */
    public void setSessCodeGuideList(List<CodeElementGuideEntry> sessCodeGuideList) {
        this.sessCodeGuideList = sessCodeGuideList;
    }

    /**
     * @return the sessMailingAddress
     */
    public MailingAddress getSessMailingAddress() {
        return sessMailingAddress;
    }

    /**
     * @param sessMailingAddress the sessMailingAddress to set
     */
    public void setSessMailingAddress(MailingAddress sessMailingAddress) {
        this.sessMailingAddress = sessMailingAddress;
    }

    /**
     * @return the sessHumanListHolder
     */
    public IFace_humanListHolder getSessHumanListHolder() {
        return sessHumanListHolder;
    }

    /**
     * @param sessHumanListHolder the sessHumanListHolder to set
     */
    public void setSessHumanListHolder(IFace_humanListHolder sessHumanListHolder) {
        this.sessHumanListHolder = sessHumanListHolder;
    }

    /**
     * @return the sessHumanListRefreshedList
     */
    public List<HumanLink> getSessHumanListRefreshedList() {
        return sessHumanListRefreshedList;
    }

    /**
     * @param sessHumanListRefreshedList the sessHumanListRefreshedList to set
     */
    public void setSessHumanListRefreshedList(List<HumanLink> sessHumanListRefreshedList) {
        this.sessHumanListRefreshedList = sessHumanListRefreshedList;
    }

    /**
     * @return the sessMailingAddressLink
     */
    public MailingAddressLink getSessMailingAddressLink() {
        return sessMailingAddressLink;
    }

    /**
     * @param sessMailingAddressLink the sessMailingAddressLink to set
     */
    public void setSessMailingAddressLink(MailingAddressLink sessMailingAddressLink) {
        this.sessMailingAddressLink = sessMailingAddressLink;
    }

    /**
     * @return the sessAddressListHolder
     */
    public IFace_addressListHolder getSessAddressListHolder() {
        return sessAddressListHolder;
    }

    /**
     * @param sessAddressListHolder the sessAddressListHolder to set
     */
    public void setSessAddressListHolder(IFace_addressListHolder sessAddressListHolder) {
        this.sessAddressListHolder = sessAddressListHolder;
    }

    /**
     * @return the sessMailingAddressLinkRefreshedList
     */
    public List<MailingAddressLink> getSessMailingAddressLinkRefreshedList() {
        return sessMailingAddressLinkRefreshedList;
    }

    /**
     * @param sessMailingAddressLinkRefreshedList the sessMailingAddressLinkRefreshedList to set
     */
    public void setSessMailingAddressLinkRefreshedList(List<MailingAddressLink> sessMailingAddressLinkRefreshedList) {
        this.sessMailingAddressLinkRefreshedList = sessMailingAddressLinkRefreshedList;
    }

    /**
     * @return the sessBlobLightListForRefreshUptake
     */
    public List<BlobLight> getSessBlobLightListForRefreshUptake() {
        return sessBlobLightListForRefreshUptake;
    }

    /**
     * @param sessBlobLightListForRefreshUptake the sessBlobLightListForRefreshUptake to set
     */
    public void setSessBlobLightListForRefreshUptake(List<BlobLight> sessBlobLightListForRefreshUptake) {
        this.sessBlobLightListForRefreshUptake = sessBlobLightListForRefreshUptake;
    }

    /**
     * @return the sessCECaseRefreshTrigger
     */
    public LocalDateTime getSessCECaseRefreshTrigger() {
        return sessCECaseRefreshTrigger;
    }

    /**
     * @param sessCECaseRefreshTrigger the sessCECaseRefreshTrigger to set
     */
    public void setSessCECaseRefreshTrigger(LocalDateTime sessCECaseRefreshTrigger) {
        this.sessCECaseRefreshTrigger = sessCECaseRefreshTrigger;
    }

    /**
     * @return the noteholderRefreshTimestampTrigger
     */
    public LocalDateTime getNoteholderRefreshTimestampTrigger() {
        return noteholderRefreshTimestampTrigger;
    }

    /**
     * @param noteholderRefreshTimestampTrigger the noteholderRefreshTimestampTrigger to set
     */
    public void setNoteholderRefreshTimestampTrigger(LocalDateTime noteholderRefreshTimestampTrigger) {
        this.noteholderRefreshTimestampTrigger = noteholderRefreshTimestampTrigger;
    }

   

    /**
     * @return the sessFlagIncludeDeactivatedEvents
     */
    public boolean isSessFlagIncludeDeactivatedEvents() {
        return sessFlagIncludeDeactivatedEvents;
    }

    /**
     * @param sessFlagIncludeDeactivatedEvents the sessFlagIncludeDeactivatedEvents to set
     */
    public void setSessFlagIncludeDeactivatedEvents(boolean sessFlagIncludeDeactivatedEvents) {
        this.sessFlagIncludeDeactivatedEvents = sessFlagIncludeDeactivatedEvents;
    }

    /**
     *
     *
     * @return the sessEnforcableCodeElement
     */
    public EnforceableCodeElement getSessEnforcableCodeElement() {
        return this.sessEnforcableCodeElement;
    }

    /**
     * @return the sessionDomain
     */
    public FocusedObjectEnum getSessionDomain() {
        return sessionDomain;
    }

    /**
     * @param sessionDomain the sessionDomain to set
     */
    public void setSessionDomain(FocusedObjectEnum sessionDomain) {
        this.sessionDomain = sessionDomain;
    }

    /**
     * @return the passwordResetNoticeTS
     */
    public LocalDateTime getPasswordResetNoticeTS() {
        return passwordResetNoticeTS;
    }

    /**
     * @param passwordResetNoticeTS the passwordResetNoticeTS to set
     */
    public void setPasswordResetNoticeTS(LocalDateTime passwordResetNoticeTS) {
        this.passwordResetNoticeTS = passwordResetNoticeTS;
    }

    /**
     * @return the sessCECaseRefreshTriggerCitations
     */
    public LocalDateTime getSessCECaseRefreshTriggerCitations() {
        return sessCECaseRefreshTriggerCitations;
    }

    /**
     * @param sessCECaseRefreshTriggerCitations the sessCECaseRefreshTriggerCitations to set
     */
    public void setSessCECaseRefreshTriggerCitations(LocalDateTime sessCECaseRefreshTriggerCitations) {
        this.sessCECaseRefreshTriggerCitations = sessCECaseRefreshTriggerCitations;
    }

    /**
     * @return the sessCECaseRefreshTriggerViolations
     */
    public LocalDateTime getSessCECaseRefreshTriggerViolations() {
        return sessCECaseRefreshTriggerViolations;
    }

    /**
     * @param sessCECaseRefreshTriggerViolations the sessCECaseRefreshTriggerViolations to set
     */
    public void setSessCECaseRefreshTriggerViolations(LocalDateTime sessCECaseRefreshTriggerViolations) {
        this.sessCECaseRefreshTriggerViolations = sessCECaseRefreshTriggerViolations;
    }

    /**
     * @return the sessMuniLight
     */
    public Municipality getSessMuniLight() {
        return sessMuniLight;
    }

    /**
     * @param sessMuniLight the sessMuniLight to set
     */
    public void setSessMuniLight(Municipality sessMuniLight) {
        this.sessMuniLight = sessMuniLight;
    }

    /**
     * @return the personNameForSearch
     */
    public String getPersonNameForSearch() {
        return personNameForSearch;
    }

    /**
     * @param personNameForSearch the personNameForSearch to set
     */
    public void setPersonNameForSearch(String personNameForSearch) {
        this.personNameForSearch = personNameForSearch;
    }

    /**
     * @return the activeCEARProcessingRoute
     */
    public CEARProcessingRouteEnum getActiveCEARProcessingRoute() {
        return activeCEARProcessingRoute;
    }

    /**
     * @param activeCEARProcessingRoute the activeCEARProcessingRoute to set
     */
    public void setActiveCEARProcessingRoute(CEARProcessingRouteEnum activeCEARProcessingRoute) {
        this.activeCEARProcessingRoute = activeCEARProcessingRoute;
    }

    /**
     * @return the crossMuniViewMode
     */
    public boolean isCrossMuniViewMode() {
        return crossMuniViewMode;
    }

    /**
     * @param crossMuniViewMode the crossMuniViewMode to set
     */
    public void setCrossMuniViewMode(boolean crossMuniViewMode) {
        this.crossMuniViewMode = crossMuniViewMode;
    }

    /**
     * @return the crossMuniMuni
     */
    public Municipality getCrossMuniMuni() {
        return crossMuniMuni;
    }

    /**
     * @param crossMuniMuni the crossMuniMuni to set
     */
    public void setCrossMuniMuni(Municipality crossMuniMuni) {
        this.crossMuniMuni = crossMuniMuni;
    }

    /**
     * @return the xMuniRecord
     */
    public XMuniActivationRecord getXMuniRecord() {
        return xMuniRecord;
    }

    /**
     * @param xMuniRecord the xMuniRecord to set
     */
    public void setXMuniRecord(XMuniActivationRecord xMuniRecord) {
        this.xMuniRecord = xMuniRecord;
    }

    /**
     * @return the ceCaseForRefresh
     */
    public CECase getCeCaseForRefresh() {
        return ceCaseForRefresh;
    }

    /**
     * @param ceCaseForRefresh the ceCaseForRefresh to set
     */
    public void setCeCaseForRefresh(CECase ceCaseForRefresh) {
        this.ceCaseForRefresh = ceCaseForRefresh;
    }

    /**
     * @return the sessBlobLightListHolderForRefreshUptake
     */
    public IFace_BlobHolder getSessBlobLightListHolderForRefreshUptake() {
        return sessBlobLightListHolderForRefreshUptake;
    }

    /**
     * @param sessBlobLightListHolderForRefreshUptake the sessBlobLightListHolderForRefreshUptake to set
     */
    public void setSessBlobLightListHolderForRefreshUptake(IFace_BlobHolder sessBlobLightListHolderForRefreshUptake) {
        this.sessBlobLightListHolderForRefreshUptake = sessBlobLightListHolderForRefreshUptake;
    }

  
    /**
     * @return the reportConfigOccActivity
     */
    public ReportConfigOccActivity getReportConfigOccActivity() {
        return reportConfigOccActivity;
    }

    /**
     * @param reportConfigOccActivity the reportConfigOccActivity to set
     */
    public void setReportConfigOccActivity(ReportConfigOccActivity reportConfigOccActivity) {
        this.reportConfigOccActivity = reportConfigOccActivity;
    }

    /**
     * @return the sessCECaseRefreshedList
     */
    public List<CECaseDataHeavy> getSessCECaseRefreshedList() {
        return sessCECaseRefreshedList;
    }

    /**
     * @param sessCECaseRefreshedList the sessCECaseRefreshedList to set
     */
    public void setSessCECaseRefreshedList(List<CECaseDataHeavy> sessCECaseRefreshedList) {
        this.sessCECaseRefreshedList = sessCECaseRefreshedList;
    }

   

   
    /**
     * @return the certPersonQuickAddNewPersonPathway
     */
    public boolean isCertPersonQuickAddNewPersonPathway() {
        return certPersonQuickAddNewPersonPathway;
    }

    /**
     * @param certPersonQuickAddNewPersonPathway the certPersonQuickAddNewPersonPathway to set
     */
    public void setCertPersonQuickAddNewPersonPathway(boolean certPersonQuickAddNewPersonPathway) {
        this.certPersonQuickAddNewPersonPathway = certPersonQuickAddNewPersonPathway;
    }

    /**
     * @return the sessHumanLink
     */
    public HumanLink getSessHumanLink() {
        return sessHumanLink;
    }

    /**
     * @param sessHumanLink the sessHumanLink to set
     */
    public void setSessHumanLink(HumanLink sessHumanLink) {
        this.sessHumanLink = sessHumanLink;
    }

     

    
}
