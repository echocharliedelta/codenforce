/*
 * Copyright (C) 2025 echocdelta
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.session;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.interfaces.IFaceActivatableBOB;
import com.tcvcog.tcvce.coordinators.OccupancyCoordinator;
import com.tcvcog.tcvce.coordinators.PropertyCoordinator;
import com.tcvcog.tcvce.coordinators.SearchCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.domain.SearchException;
import com.tcvcog.tcvce.entities.CECaseDataHeavy;
import com.tcvcog.tcvce.entities.IFaceSessionSyncTarget;
import com.tcvcog.tcvce.entities.IFaceSessionSyncVisitor;
import com.tcvcog.tcvce.entities.IFaceSessionSynchronizer;
import com.tcvcog.tcvce.entities.Person;
import com.tcvcog.tcvce.entities.Property;
import com.tcvcog.tcvce.entities.PropertyDataHeavy;
import com.tcvcog.tcvce.entities.PropertyUnitWithProp;
import com.tcvcog.tcvce.entities.occupancy.OccPeriod;
import com.tcvcog.tcvce.entities.occupancy.OccPeriodDataHeavy;
import com.tcvcog.tcvce.entities.occupancy.OccPeriodType;
import com.tcvcog.tcvce.entities.occupancy.OccPermit;
import com.tcvcog.tcvce.entities.occupancy.OccPermitPropUnitHeavy;
import com.tcvcog.tcvce.entities.occupancy.OccPermitType;
import com.tcvcog.tcvce.entities.reports.ReportConfigOccPermit;
import com.tcvcog.tcvce.entities.search.QueryOccPeriod;
import com.tcvcog.tcvce.entities.search.QueryOccPermit;
import com.tcvcog.tcvce.entities.search.SearchParamsOccPeriod;
import com.tcvcog.tcvce.entities.search.SearchParamsOccPermit;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Premier Session-scoped bean for session occupancy object
 * management AND occ period and occ permit searching and list
 * maintenance.
 * 
 * @author echocdelta
 */
public  class   SessionOccupancyConductor 
        extends BackingBeanUtils 
        implements IFaceSessionSynchronizer, IFaceSessionSyncVisitor{

//  ***************************************************
//  ************ CORE SESSION OBJECTS       ***********
//  ***************************************************
    // MIGRATED FROM SESSION BEAN
    private OccPeriodDataHeavy sessOccPeriod;
    /**
     * Holds session search results, not the periods on a given property
     */
    private List<OccPeriodDataHeavy> sessOccPeriodList;
    
    private OccPermit sessOccPermit;
    /**
     * Holds session search results not periods on prop
     */
    private List<OccPermitPropUnitHeavy> sessOccPermitList;
    
//  ***************************************************
//  ************ OCC PERIOD SEARCH FACILITY ***********
//  ***************************************************
    private List<QueryOccPeriod> occPeriodQueryList;
    private QueryOccPeriod occPeriodQuerySelected;
    private SearchParamsOccPeriod searchParamsOccPeriodSelected;

    private List<OccPeriodDataHeavy> occPeriodListFiltered;
    private boolean appendResultsToListOccPeriod;
    
    private List<OccPeriodType> search_occPeriodTypeList;
    private List<Property> search_propList;
    private List<Person> search_personList;
    
    
//  ***************************************************
//  ************ OCC PERMIT SEARCH FACILITY ***********
//  ***************************************************
    private List<QueryOccPermit> occPermitQueryList;
    private QueryOccPermit occPermitQuerySelected;
    private SearchParamsOccPermit searchParamsOccPermitSelected;
    
    private List<OccPermitPropUnitHeavy> occPermitListFiltered;
    private boolean appendResultsToListOccPermit;
    
    private List<OccPermitType> search_occPermitTypeList;

    
//  ***************************************************
//  ******************* BEAN INIT *********************
//  ***************************************************
    
    @PostConstruct
    public void initBean()  {
        try {
            System.out.println("SessionOccupancyConductor.initBean");
            // first attempt at using the visitor pattern for 
            // session object management
            getSessionBean().registerSessionSynchronizer(this);
            
            initOccPeriodSearch();
            initOccPermitSearch();
        } catch (IntegrationException ex) {
            System.out.println(ex);
        }
    }
    
    /**
     * Migrated steps from OccPeriodSearchBB
     */
    private void initOccPeriodSearch() throws IntegrationException{
        OccupancyCoordinator oc = getOccupancyCoordinator();
        SearchCoordinator sc = getSearchCoordinator();
        
        occPeriodQueryList = sc.buildQueryOccPeriodList(getSessionBean().getSessUser().getKeyCard());
        appendResultsToListOccPeriod = false;
        sessOccPeriodList = new ArrayList<>();
        
        search_occPeriodTypeList = oc.getOccPeriodTypeList(getSessionBean().getSessMuni(), false);
        
        if (occPeriodQueryList != null && !occPeriodQueryList.isEmpty()) {
            occPeriodQuerySelected = occPeriodQueryList.get(0);
        }

        configureOccPeriodSearchParameters();
    }
    
    /**
     * Migrated init bean steps from OccPermitSearchBB
     */
    private void initOccPermitSearch(){
        OccupancyCoordinator oc = getOccupancyCoordinator();
        SearchCoordinator sc = getSearchCoordinator();
        
        occPermitQueryList = sc.buildQueryOccPermitList(getSessionBean().getSessUser().getKeyCard());
        appendResultsToListOccPermit = false;
        sessOccPermitList = new ArrayList<>();
        
        if (occPermitQueryList != null && !occPermitQueryList.isEmpty()) {
            occPermitQuerySelected = occPermitQueryList.get(0);
        }

        search_occPermitTypeList = getSessionBean().getSessMuni().getProfile().getOccPermitTypeList();
        
        configureOccPermitSearchparameters();
        
    }
    
    
     /**
     * Sets up search fields for Occ PERIODS
     */
    private void configureOccPeriodSearchParameters() {
        if (occPeriodQuerySelected != null
                &&
                occPeriodQuerySelected.getParamsList() != null
                &&
                !occPeriodQuerySelected.getParamsList().isEmpty()) {

            searchParamsOccPeriodSelected = getOccPeriodQuerySelected().getParamsList().get(0);
        } else {
            searchParamsOccPeriodSelected = null;
        }
    }
    
    /**
     * Sets up search facility for Occ PERMITS
     */
     private void configureOccPermitSearchparameters() {
         if (occPermitQuerySelected != null
                &&
                occPermitQuerySelected.getParamsList() != null
                &&
                !occPermitQuerySelected.getParamsList().isEmpty()) {

            searchParamsOccPermitSelected = getOccPermitQuerySelected().getParamsList().get(0);
        } else {
            searchParamsOccPermitSelected = null;
        }
    }

  
     
     
//  ****************************************************************************
//  ******************* SESSION OBJECT REGISTRATION  ***************************
//  ****************************************************************************
     
     /**
      * Convenience method for registering the superclass light version of OPDH
      * @param period 
      * @return  the page ID for occ period profile
      */
     public OccPeriodDataHeavy registerSessionOccPeriod(OccPeriod period){
         OccupancyCoordinator oc = getOccupancyCoordinator();

        // Convert occPeriodBase to a heavy data class (because it can be modified, presumably)
        OccPeriodDataHeavy occPeriodHeavy = null;
        try {
            occPeriodHeavy = oc.assembleOccPeriodDataHeavy(period, getSessionBean().getSessUser());
        } catch (IntegrationException | BObStatusException  ex) {
            System.out.println(ex);
        }

        // Set the current session occ period to this converted "heavy" occ period
        // but go through the setter so we load the prop unit heavy version
         registerSessionOccPeriod(occPeriodHeavy);
         return occPeriodHeavy;
     }
     
     /**
      * Premier registration point for OccPeriodDataheavy Objects
      * Registration is the managed setting process of the official session
      * OccPeriod.Calling this registration method will trigger 
        - a reload of the object from the DB
        - coordination of all other session objects: property, person, and
      * case
      * @param perioddh
      * @return 
      */
     public OccPeriodDataHeavy registerSessionOccPeriod(OccPeriodDataHeavy perioddh){
         OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            sessOccPeriod = oc.assembleOccPeriodDataHeavy(perioddh, getSessionBean().getSessUser());
        } catch (IntegrationException | BObStatusException  ex) {
            System.out.println(ex);
        } 
        return sessOccPeriod;
     }
    
     /**
      * Called when any other core object is activated and we need to align our
      * session slots with this new focused object. So if we're a property, we'll 
      * choose the first period and permits to make our session objects.
      * @param abob 
      */
    @Override
    public void synchronizeToSessionFocusObject(IFaceSessionSyncTarget target) {
       if(target != null){
           target.accept(this);
       }
    }
    
    @Override
    public void visit(PropertyDataHeavy pdh) {
        activateFirstPeriodOnPropDH(pdh);
    }

    @Override
    public void visit(Person p) {
        // nothing to do here
    }
    
    @Override
    public void visit(OccPermit permit) {
    OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            System.out.println("SessionOccupancyConductor.visit | OccPermit");
            registerSessionOccPeriod(oc.getOccPeriod(permit.getPeriodID(), getSessionBean().getSessUser()));
            getSessionBean().setSessProperty(sessOccPeriod.getParentParcelKey());
            getSessionInspectionConductor().registerSessionInspectable(sessOccPeriod);
        } catch (IntegrationException | BlobException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
        }
    }

    /**
     * Utility method for extracting the first period on a PDH
     * @param pdh 
     */
    private void activateFirstPeriodOnPropDH(PropertyDataHeavy pdh){
        if(pdh.getCompletePeriodList() != null && !pdh.getCompletePeriodList().isEmpty()){
                sessOccPeriod = pdh.getCompletePeriodList().get(0);
            } else {
                sessOccPeriod = null;
            }
    }
    
    @Override
    public void visit(CECaseDataHeavy csedh) {
        try {
            PropertyCoordinator pc = getPropertyCoordinator();
            activateFirstPeriodOnPropDH(pc.assemblePropertyDataHeavy(pc.getProperty(csedh.getParcelKey()), getSessionBean().getSessUser()));
        } catch (SearchException | AuthorizationException | BObStatusException | BlobException | IntegrationException ex) {
            System.out.println(ex);
        } 
    }

    @Override
    public void visit(OccPeriodDataHeavy opdh) {
        sessOccPeriod = opdh;
    }
    
    // *************************************************************************
    // ******************       OCC PERIOD SEARCH ORGANS  **********************
    // *************************************************************************
      /**
     * Listener method for requests from the user to clear the results list
     *
     */
    public void clearOccPeriodList() {
        if (sessOccPeriodList != null) {
            sessOccPeriodList.clear();
        }
    }

    /**
     * Asks the Coordinator for the OccPeriod viewing history
     * @Deprecated : use search 
     */
    public void loadOccPeriodHistory() {
        System.out.println("OccPeriodSearchBB.loadOccPeriodHistory | DEPRECATED API USE");
    }

    /**
     * Entry way into the Query world via the SearchCoordinator who is responsible
     * for calling appropriate Coordinators and configuration methods and such
     */
    public void executeQueryOccPeriod() {
        System.out.println("occPeriodBB.executeQueryOccPeriod");
        SearchCoordinator sc = getSearchCoordinator();
        int listSize = 0;

        if (!isAppendResultsToListOccPermit()) {
            sessOccPeriodList.clear();
        }
        occPeriodQuerySelected.setRequestingUser(getSessionBean().getSessUser());
        try {
            sessOccPeriodList.addAll(sc.runQuery(getOccPeriodQuerySelected()).getBOBResultList());
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Your query completed with " + listSize + " results", ""));
        } catch (SearchException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Could not query for Periods due to search errors, sorry.", ""));
        }
    }

    /**
     * Listener for user requests to start a new query
     */
    public void changeQueryOccPeriodSelected() {

        configureOccPeriodSearchParameters();
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "New query loaded!", ""));
    }

    /**
     * Listener for user requests to clear out and restart the current Query
     */
    public void resetQueryOccPeriod(ActionEvent ev) {
        SearchCoordinator sc = getSearchCoordinator();
        sessOccPeriodList.clear();
        occPeriodQueryList = sc.buildQueryOccPeriodList(getSessionBean().getSessUser().getKeyCard());
        if(occPeriodQueryList != null && !occPeriodQueryList.isEmpty()){
            occPeriodQuerySelected = occPeriodQueryList.get(0);
        }
        configureOccPeriodSearchParameters();

    }
    
     /**
     * Listener for user requests to view property associated with CEAR
     * @param opdh
     * @return 
     */
    public String onViewPeriodProperty(OccPeriodDataHeavy opdh){
        if(opdh != null && opdh.getPropertyUnitID() != 0){
            PropertyCoordinator pc = getPropertyCoordinator();
            try {
                return getSessionBean().navigateToPageCorrespondingToObject(pc.getProperty(opdh.getParentParcelKey()));
            } catch (BObStatusException | AuthorizationException | IntegrationException ex) {
                System.out.println(ex);
                 getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                            ex.getMessage(), ""));
                return "";
            }
        }
        return "";
    }
    
    
    // *************************************************************************
    // ******************       OCC PERMIT SEARCH ORGANS  **********************
    // *************************************************************************
    
    
     /**
     * Listener method for requests from the user to clear the results list
     *
     */
    public void clearOccPermitList() {
        if (sessOccPermitList != null) {
            sessOccPermitList.clear();
        }
    }

   
    /**
     * Entry way into the Query world via the SearchCoordinator who is responsible
     * for calling appropriate Coordinators and configuration methods and such
     */
    public void executeQueryOccPermit() {
        SearchCoordinator sc = getSearchCoordinator();
        int listSize = 0;
        if(getOccPermitQuerySelected() != null){
            if (!isAppendResultsToListOccPermit()) {
                sessOccPermitList.clear();
            }
            occPermitQuerySelected.setRequestingUser(getSessionBean().getSessUser());
            try {
                sessOccPermitList.addAll(sc.runQuery(occPermitQuerySelected).getBOBResultList());

                System.out.println("SessionOccupancyConductor.executeQueryOccPermit | list size: " + sessOccPermitList.size());
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Your query completed with " + listSize + " results", ""));
            } catch (SearchException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Could not query for permits due to search errors, sorry.", ""));
            }
        }
    }

    /**
     * Listener for user requests to start a new query
     */
    public void changeQueryOccPermitSelected() {

        configureOccPermitSearchparameters();
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "New query loaded!", ""));
    }

    /**
     * Listener for user requests to clear out and restart the current Query
     */
    public void resetQueryOccPermit() {
        SearchCoordinator sc = getSearchCoordinator();
        occPermitQueryList = sc.buildQueryOccPermitList(getSessionBean().getSessUser().getMyCredential());
        if (occPermitQueryList != null && !occPermitQueryList.isEmpty()) {
            setOccPermitQuerySelected(occPermitQueryList.get(0));
        }
        configureOccPermitSearchparameters();

    }
    
    /**
     * Listener for user requests to view a permit for printing
     * @param permit
     * @return 
     */
    public String onViewPermitLinkClick(OccPermitPropUnitHeavy permit){
        
        OccupancyCoordinator oc = getOccupancyCoordinator();
        PropertyCoordinator pc = getPropertyCoordinator();
        try{
            OccPeriod op = oc.getOccPeriod(permit.getPeriodID(), getSessionBean().getSessUser());
            ReportConfigOccPermit rcop = oc.getOccPermitReportConfigDefault(
                    permit, 
                    op, 
                    pc.getPropertyUnit(op.getPropertyUnitID()),
                    getSessionBean().getSessUser());
            getSessionBean().setReportConfigOccPermit(rcop);
        } catch (BObStatusException | IntegrationException | BlobException ex){
            System.out.println(ex);
        }
        return "occPermit";
        
    }
    
    /**
     * Listener for user requests to view a permit's associated permit file
     * @param permit
     * @return 
     */
    public String onViewFileLinkClick(OccPermitPropUnitHeavy permit){
        
        try {
            return getSessionBean().navigateToPageCorrespondingToObject(permit);
        } catch (BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
              getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                ex.getMessage(), ""));
        }
        return "";
        
    }
    
    /**
     * Listener for user requests to view a permit's associated property
     * @param permit
     * @return 
     */
    public String onViewPropertyLinkClick(OccPermitPropUnitHeavy permit){
        
        try {
            
            PropertyCoordinator pc = getPropertyCoordinator();
            OccupancyCoordinator oc = getOccupancyCoordinator();
            OccPeriod period = oc.getOccPeriod(permit.getPeriodID(), getSessionBean().getSessUser());
            PropertyUnitWithProp puwp = pc.getPropertyUnitWithProp(period.getPropertyUnitID());
            return getSessionBean().navigateToPageCorrespondingToObject(puwp.getProperty());
        } catch (BObStatusException | IntegrationException  | AuthorizationException | BlobException  ex){
            System.out.println(ex);
              getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                ex.getMessage(), ""));
        }
        
        return "";
    }
    
    
    // *************************************************************************
    // ******************       GETTERS AND SETTERS       **********************
    // *************************************************************************

    /**
     * @return the sessOccPeriod
     */
    public OccPeriodDataHeavy getSessOccPeriod() {
        return sessOccPeriod;
    }

   
    /**
     * @return the sessOccPeriodList
     */
    public List<OccPeriodDataHeavy> getSessOccPeriodList() {
        return sessOccPeriodList;
    }

    /**
     * @param sessOccPeriodList the sessOccPeriodList to set
     */
    public void setSessOccPeriodList(List<OccPeriodDataHeavy> sessOccPeriodList) {
        this.sessOccPeriodList = sessOccPeriodList;
    }

    /**
     * @return the sessOccPermit
     */
    public OccPermit getSessOccPermit() {
        return sessOccPermit;
    }

    /**
     * @param sessOccPermit the sessOccPermit to set
     */
    public void setSessOccPermit(OccPermit sessOccPermit) {
        this.sessOccPermit = sessOccPermit;
    }

    /**
     * @return the sessOccPermitList
     */
    public List<OccPermitPropUnitHeavy> getSessOccPermitList() {
        return sessOccPermitList;
    }

    /**
     * @param sessOccPermitList the sessOccPermitList to set
     */
    public void setSessOccPermitList(List<OccPermitPropUnitHeavy> sessOccPermitList) {
        this.sessOccPermitList = sessOccPermitList;
    }

    /**
     * @return the occPeriodQueryList
     */
    public List<QueryOccPeriod> getOccPeriodQueryList() {
        return occPeriodQueryList;
    }

    /**
     * @param occPeriodQueryList the occPeriodQueryList to set
     */
    public void setOccPeriodQueryList(List<QueryOccPeriod> occPeriodQueryList) {
        this.occPeriodQueryList = occPeriodQueryList;
    }

    /**
     * @return the occPeriodQuerySelected
     */
    public QueryOccPeriod getOccPeriodQuerySelected() {
        return occPeriodQuerySelected;
    }

    /**
     * @param occPeriodQuerySelected the occPeriodQuerySelected to set
     */
    public void setOccPeriodQuerySelected(QueryOccPeriod occPeriodQuerySelected) {
        this.occPeriodQuerySelected = occPeriodQuerySelected;
    }

    /**
     * @return the searchParamsOccPeriodSelected
     */
    public SearchParamsOccPeriod getSearchParamsOccPeriodSelected() {
        return searchParamsOccPeriodSelected;
    }

    /**
     * @param searchParamsOccPeriodSelected the searchParamsOccPeriodSelected to set
     */
    public void setSearchParamsOccPeriodSelected(SearchParamsOccPeriod searchParamsOccPeriodSelected) {
        this.searchParamsOccPeriodSelected = searchParamsOccPeriodSelected;
    }

    /**
     * @return the occPeriodListFiltered
     */
    public List<OccPeriodDataHeavy> getOccPeriodListFiltered() {
        return occPeriodListFiltered;
    }

    /**
     * @param occPeriodListFiltered the occPeriodListFiltered to set
     */
    public void setOccPeriodListFiltered(List<OccPeriodDataHeavy> occPeriodListFiltered) {
        this.occPeriodListFiltered = occPeriodListFiltered;
    }

    /**
     * @return the appendResultsToListOccPeriod
     */
    public boolean isAppendResultsToListOccPeriod() {
        return appendResultsToListOccPeriod;
    }

    /**
     * @param appendResultsToListOccPeriod the appendResultsToListOccPeriod to set
     */
    public void setAppendResultsToListOccPeriod(boolean appendResultsToListOccPeriod) {
        this.appendResultsToListOccPeriod = appendResultsToListOccPeriod;
    }

    /**
     * @return the search_occPeriodTypeList
     */
    public List<OccPeriodType> getSearch_occPeriodTypeList() {
        return search_occPeriodTypeList;
    }

    /**
     * @param search_occPeriodTypeList the search_occPeriodTypeList to set
     */
    public void setSearch_occPeriodTypeList(List<OccPeriodType> search_occPeriodTypeList) {
        this.search_occPeriodTypeList = search_occPeriodTypeList;
    }

    /**
     * @return the search_propList
     */
    public List<Property> getSearch_propList() {
        return search_propList;
    }

    /**
     * @param search_propList the search_propList to set
     */
    public void setSearch_propList(List<Property> search_propList) {
        this.search_propList = search_propList;
    }

    /**
     * @return the search_personList
     */
    public List<Person> getSearch_personList() {
        return search_personList;
    }

    /**
     * @param search_personList the search_personList to set
     */
    public void setSearch_personList(List<Person> search_personList) {
        this.search_personList = search_personList;
    }

    /**
     * @return the occPermitQueryList
     */
    public List<QueryOccPermit> getOccPermitQueryList() {
        return occPermitQueryList;
    }

    /**
     * @param occPermitQueryList the occPermitQueryList to set
     */
    public void setOccPermitQueryList(List<QueryOccPermit> occPermitQueryList) {
        this.occPermitQueryList = occPermitQueryList;
    }

    /**
     * @return the occPermitQuerySelected
     */
    public QueryOccPermit getOccPermitQuerySelected() {
        return occPermitQuerySelected;
    }

    /**
     * @param occPermitQuerySelected the occPermitQuerySelected to set
     */
    public void setOccPermitQuerySelected(QueryOccPermit occPermitQuerySelected) {
        this.occPermitQuerySelected = occPermitQuerySelected;
    }

    /**
     * @return the searchParamsOccPermitSelected
     */
    public SearchParamsOccPermit getSearchParamsOccPermitSelected() {
        return searchParamsOccPermitSelected;
    }

    /**
     * @param searchParamsOccPermitSelected the searchParamsOccPermitSelected to set
     */
    public void setSearchParamsOccPermitSelected(SearchParamsOccPermit searchParamsOccPermitSelected) {
        this.searchParamsOccPermitSelected = searchParamsOccPermitSelected;
    }

    /**
     * @return the occPermitListFiltered
     */
    public List<OccPermitPropUnitHeavy> getOccPermitListFiltered() {
        return occPermitListFiltered;
    }

    /**
     * @param occPermitListFiltered the occPermitListFiltered to set
     */
    public void setOccPermitListFiltered(List<OccPermitPropUnitHeavy> occPermitListFiltered) {
        this.occPermitListFiltered = occPermitListFiltered;
    }

    /**
     * @return the appendResultsToListOccPermit
     */
    public boolean isAppendResultsToListOccPermit() {
        return appendResultsToListOccPermit;
    }

    /**
     * @param appendResultsToListOccPermit the appendResultsToListOccPermit to set
     */
    public void setAppendResultsToListOccPermit(boolean appendResultsToListOccPermit) {
        this.appendResultsToListOccPermit = appendResultsToListOccPermit;
    }

    /**
     * @return the search_occPermitTypeList
     */
    public List<OccPermitType> getSearch_occPermitTypeList() {
        return search_occPermitTypeList;
    }

    /**
     * @param search_occPermitTypeList the search_occPermitTypeList to set
     */
    public void setSearch_occPermitTypeList(List<OccPermitType> search_occPermitTypeList) {
        this.search_occPermitTypeList = search_occPermitTypeList;
    }

 
    
    
}
