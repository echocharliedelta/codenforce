 /*
 * Copyright (C) 2018 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.session;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.coordinators.BlobCoordinator;
import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.coordinators.MunicipalityCoordinator;
import com.tcvcog.tcvce.coordinators.SearchCoordinator;
import com.tcvcog.tcvce.coordinators.UserCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.EventException;
import com.tcvcog.tcvce.domain.ExceptionSeverityEnum;
import com.tcvcog.tcvce.domain.SessionException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.domain.SearchException;
import com.tcvcog.tcvce.entities.Credential;
import com.tcvcog.tcvce.entities.FocusedObjectEnum;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.MunicipalityDataHeavy;
import com.tcvcog.tcvce.entities.User;
import com.tcvcog.tcvce.entities.UserAuthorized;
import com.tcvcog.tcvce.entities.UserMuniAuthPeriod;
import com.tcvcog.tcvce.entities.UserMuniAuthPeriodLogEntry;
import com.tcvcog.tcvce.entities.UserMuniAuthPeriodLogEntryCatEnum;
import com.tcvcog.tcvce.entities.occupancy.OccPeriod;
import com.tcvcog.tcvce.entities.search.QueryCEAR;
import com.tcvcog.tcvce.entities.search.QueryCEAREnum;
import com.tcvcog.tcvce.entities.search.QueryCECase;
import com.tcvcog.tcvce.entities.search.QueryCECaseEnum;
import com.tcvcog.tcvce.entities.search.QueryOccPeriod;
import com.tcvcog.tcvce.entities.search.QueryOccPeriodEnum;
import com.tcvcog.tcvce.integration.UserIntegrator;
import java.io.Serializable;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.ExternalContext;
import jakarta.faces.context.FacesContext;
import jakarta.servlet.http.HttpServletRequest;
import com.tcvcog.tcvce.util.Constants;
import com.tcvcog.tcvce.util.SubSysEnum;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * *******************************
 * ***    SECURITY CRITICAL    ***
 * *******************************
 * 
 * Central backing bean and psuedo-coordinator at the Session level
 * that processes the requested UserMuniAuthPeriod and builds an entire
 * session full of business objects once security checks are passed
 * 
 * As expected, this class works in tandem with the UserCoordinator
 * to manage the UserMuniAuthorizationPeriod list, one of which is chosen
 * and becomes and television program!
 * 
 * Contents in brief:
 * <ul>
 * <li>User-centric login and session initialization methods</li>
 * <li>Subsystem initialization controller</li>
 * <li>A heap of subsystem-specific initialization methods</li>
 * </ul>
 * @author ellen bascomb of apt 31y
 */
public  class       SessionInitializer 
        extends     BackingBeanUtils 
        implements  Serializable {

    private String usernameQueuedForSession;
    private UserAuthorized userAuthorizedQueuedForSession;
    private List<UserMuniAuthPeriod> umapCandidateList;
    private UserMuniAuthPeriod umapQueuedForSession;
    private Municipality muniQueuedForSession;
    private SessionBean sb;
    
    /**
     * Creates a new instance of SessionInitializer
     */
    public SessionInitializer() {
    }
    
    @PostConstruct
    public void initBean(){
        sb = getSessionBean();
        System.out.println("SessionInitializer.initBean");
        userAuthorizedQueuedForSession = null;
        User tmpUser = null;
        UserCoordinator uc = getUserCoordinator();
        UserIntegrator ui = getUserIntegrator();
        // check to see if we have an internal session created already
        // to determine which user we authenticate with
        
        if(sb.getUserForReInit() != null){
            usernameQueuedForSession = sb.getUserForReInit().getUsername();
        } else if(usernameQueuedForSession == null){
            try {
                // we have a first init! Ask the container for its user
                usernameQueuedForSession = sessionInit_getContainerAuthenticatedUser();
            } catch (IntegrationException ex) {
                System.out.println(ex);
            }
        } 
        try {
            tmpUser = uc.user_getUser(ui.getUserID(usernameQueuedForSession));
            userAuthorizedQueuedForSession = uc.auth_prepareUserForSessionChoice(tmpUser);
            sb.setSessUser(userAuthorizedQueuedForSession);
        } catch (IntegrationException | BObStatusException  ex) {
            System.out.println(ex);
        }
        
        umapCandidateList = new ArrayList<>();
        
        List<Municipality> tempMuniList;
        if(userAuthorizedQueuedForSession != null && userAuthorizedQueuedForSession.getMuniAuthPeriodsMap() != null){
            
            Set<Municipality> muniSet = userAuthorizedQueuedForSession.getMuniAuthPeriodsMap().keySet();
            if(!muniSet.isEmpty()){
                tempMuniList = new ArrayList(muniSet);
                for(Municipality muni: tempMuniList){
                    umapCandidateList.addAll(userAuthorizedQueuedForSession.getMuniAuthPeriodsMap().get(muni));
                    
                }

            }
        }
        
    }
    
    
    /**
    * *******************************
    * ***    SECURITY CRITICAL    ***
    * *******************************
    * 
     * JBoss is responsible for the first query against the DB. If a username/pass
     * matches the query, this method will extract the username from any old request
     * @return the username string of an authenticated user from the container
     */
    private String sessionInit_getContainerAuthenticatedUser() throws IntegrationException {
        FacesContext fc = getFacesContext();
        ExternalContext ec = fc.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) ec.getRequest();
        String usr = request.getRemoteUser();
        System.out.println("SessionInitializer.getContainerAuthenticatedUser: " + usr);
        return usr;
    }
    
    /**
    * *******************************
    * ***    SECURITY CRITICAL    ***
    * *******************************
    * 
     * Processes the user's choice of their authorization period
     * and initiates the entire auth process to create a fully populated session
     * @param umap the chosen UMAP, which should be valid
     * @return 
     */
    public String sessionInit_credentializeUserMuniAuthPeriod(UserMuniAuthPeriod umap){
        if(userAuthorizedQueuedForSession.getForcePasswordResetTS() == null){
            // cleanup hacky way to display confirmation method on session reinit after password update
            getSessionBean().setPasswordResetNoticeTS(null);
            String page = sessionInit_configureSession(getSessionBean().getSessUser(), umap);
            return page;
        } else {
             FacesContext facesContext = getFacesContext();
             facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, 
                    "Reset your password to initiate a session", 
                    ""));
            return "";
        }
    }
    
    /**
    * *******************************
    * ***    SECURITY CRITICAL    ***
    * *******************************
    * 
     * Core configuration method for sessions that takes in a chosen UMAP from
     * a pre-assembled list of valid UMAPs
     * 
     * The logic work is passed up to the UserCoordinator
     * 
     * @param ua which is not quite true, since at this state the user is not Authorized, it is 
     * just about to be though!
     * @param umap to which the UserAuthorized should be bound
     * @return nav string
     */
    public String sessionInit_configureSession(UserAuthorized ua, UserMuniAuthPeriod umap) {
        FacesContext facesContext = getFacesContext();
        UserCoordinator uc = getUserCoordinator();
        System.out.println("SessionInitializer.configureSession()");
        
        try {
            // The central call which initiates the User's session for a particular municipality
            // Muni will be null when called from initiateInternalSession
            UserAuthorized authUser = uc.auth_authorizeUser_SECURITYCRITICAL(ua, umap);
            
            // as long as we have an actual user, proceed with session config
            if(authUser != null){
                initializeSubsystems(authUser);
                System.out.println("Init subsystem success!");
                getSessionBean().setSessionDomain(FocusedObjectEnum.MUNI_DASHBOARD);
               return "success";
            } else {
                System.out.println("Init subsystem no auth!");
                return "noAuth";
            }
        
        } catch (IntegrationException ex) {
//            getLogIntegrator().makeLogEntry(99, getSessionID(),2,"SessionInitializer.initiateInternalSession | user lookup integration error", 
//                    true, true);
            System.out.println("SessionInitializer.intitiateInternalSession | error getting facesUser");
            System.out.println(ex);
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, 
                    "Integration module error. Unable to connect your server user to the COG system user.", 
                    "Please contact TCVCOG at 412.858.5115"));
            return "";
        } catch (AuthorizationException ex) {
            System.out.println("SessionInitializer.intitiateInternalSession | Auth exception");
            System.out.println(ex);
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    ex.getMessage(), ""));
            return "";
        } catch (SessionException ex) {
            System.out.println("SessionInitializer.intitiateInternalSession | Session exception");
            System.out.println(ex);
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    ex.getMessage(), ""));
            return "";
        }
    }
    
    
    /**
    * *******************************
    * ***    SECURITY CRITICAL    ***
    * *******************************
    * 
     * Distributor method for all subsystem initialization routines.
     * Individual subsystems are initialized in an order which allows 
     * each to use objects generated during its predecessors sequences, such as
     * a UserAuthorized, a Credential, a MuniDataHeavy
     * @param ua
     * @throws SessionException 
     */
    private void initializeSubsystems(UserAuthorized ua) throws SessionException{
        if(ua == null){
            throw new SessionException( "No authorized user found for subsystem initialization", 
                                        SubSysEnum.N_USER, 
                                        ExceptionSeverityEnum.SESSION_FATAL);
        }
        
        Credential cred = ua.getMyCredential();
        System.out.println("SessionInitializer.initializeSubsystems for " + ua.getUsername());
        for(SubSysEnum ss: SubSysEnum.values()){
            
            if(ss.isInitialize()){
                printSubsystemInit(ss);
                switch(ss){
                    case SYSTEM_WIDE:
                        initSubsystem_system(ua, ss);
                    case N_USER:
                        initSubsystem_N_User(ua, ss);
                        break;
                    case I_MUNICIPALITY:
                        initSubsystem_I_Municipality(ua, ss);
                        break;
                    case II_CODEBOOK:
                        initSubsystem_II_CodeBook(cred, ss, sb.getSessMuni());
                        break;
                    case III_PROPERTY:
                        initSubsystem_III_Property(ua, ss);
                        break;
                    case IV_PERSON:
                        initSubsystem_IV_Person(cred, ss);
                        break;
                    case V_EVENT:
                        initSubsystem_V_Event(cred, ss);
                        break;
                    case VI_OCCPERIOD:
                        initSubsystem_VI_OccPeriod(ua, ss);
                        break;
                    case VII_CECASE:
                        initSubsystem_VII_CECase(ua, ss);
                        break;
                    case VIII_CEACTIONREQ:
                        initSubsystem_VIII_CEActionRequest(cred, ss);
                        break;
                    case VIV_OCCAPP:
                        initSubsystem_VIV_OccApp(cred, ss);
                        break;
                    case X_PAYMENT:
                        initSubsystem_X_Payment(cred, ss);
                        break;
                    case XI_REPORT:
                        initSubsystem_XI_Report(cred, ss);
                        break;
                    case XII_BLOB:
                        initSubsystem_XII_Blob(cred, ss);
                        break;
                    case XIII_PUBLICINFO:
                        initSubsystem_XIII_PublicInfoBundle(cred, ss);
                        break;
                } // close switch
                printSubsystemInitComplete(ss);
            } // close if
            
        } // close for over subsystem enum
        
        // record session data to DB
        initSubsystem_logSession(ua, SubSysEnum.N_USER);
        
    }
    
    /**
    * *******************************
    * ***    SECURITY CRITICAL    ***
    * *******************************
    * 
     * Designed for recording data about the human user's computer connected to our surver.
     * TODO: during early design tests, this method wasn't getting the UserAgent from the HTTP
     * headers like we wanted to
     * @param umaple
     * @return 
     */
    private UserMuniAuthPeriodLogEntry assembleSessionInfo(UserMuniAuthPeriodLogEntry umaple){
        FacesContext fc = getFacesContext();
        HttpServletRequest req = (HttpServletRequest) fc.getExternalContext().getRequest();
        HttpServletResponse res = (HttpServletResponse) fc.getExternalContext().getResponse();
        StringBuilder sb = null;

        Map<String, String[]> headMap = req.getParameterMap();

        umaple.setHeader_remoteaddr(req.getRemoteAddr());
        if(headMap != null && headMap.get(Constants.PARAM_USERAGENT) != null){
             sb = new StringBuilder();
            for(String s: headMap.get(Constants.PARAM_USERAGENT)){
                sb.append(s);
                sb.append("|");
            }
        }
        if(sb != null){
            umaple.setHeader_useragent(sb.toString());
        }
    
        umaple.setHeader_dateraw(res.getHeader(Constants.PARAM_DATERAW));
        
        Cookie[] cooks = req.getCookies();
        if(cooks != null){
            for(Cookie ckie: cooks){
                if(ckie.getName().equals(Constants.PARAM_JSESS)){
                    umaple.setCookie_jsessionid(ckie.getValue());
                    break;
                } // close inner if
            } // close for
        } // close cooks null check
        return umaple;
    }
    
        
        
    /**
     * Utility method for printing to the console notes about session init
     * @param ss 
     */
    private void printSubsystemInit(SubSysEnum ss){
        StringBuilder initString = new StringBuilder();
        initString.append("Initializing subsystem ");
        initString.append(ss.getSubSysID_Roman());
        initString.append(" ");
        initString.append(ss.getTitle());
        System.out.println(initString.toString());
        
    }
    /**
     * Utility method for printing to the console notes about session init
     * @param ss 
     */
    private void printSubsystemInitComplete(SubSysEnum ss){
        StringBuilder initString = new StringBuilder();
        initString.append("Initializing subsystem ");
        initString.append(ss.getSubSysID_Roman());
        initString.append(" ");
        initString.append(ss.getTitle());
        initString.append(": COMPLETE ");
        System.out.println(initString.toString());
        
    }

    /**
    * *******************************
    * ***    SECURITY CRITICAL    ***
    * *******************************
    * 
     * Subsystem initialization controller
     *
     * >>> -------------------------------------------------------------- <<<
     * >>>                   System                                       <<<
     * >>> -------------------------------------------------------------- <<<
     * 
     * System wide init 
     * 
     * @param cred of the requesting User
     * @param ss under current configuration
     * @throws SessionException for all initialization issues
     */
    private void initSubsystem_system(UserAuthorized authUser, SubSysEnum ss) throws SessionException{
        // nothing to do here yet, this is probably in the wrong spot
       
    }
    
    /* 
     * Subsystem initialization controller
     *
     * >>> -------------------------------------------------------------- <<<
     * >>>                   N User                                       <<<
     * >>> -------------------------------------------------------------- <<<
     * 
     * Populates relevant SessionBean members through calls to the governing
     * Coordinator classes. Also initializes query lists for query-able BObs
     * 
     * @param cred of the requesting User
     * @param ss under current configuration
     * @throws SessionException for all initialization issues
     */
    private void initSubsystem_N_User(UserAuthorized authUser, SubSysEnum ss) throws SessionException{
        UserCoordinator uc = getUserCoordinator();
        // injection of the fully authorized user
        sb.setSessUser(authUser);
       
    }
    
    /**
     * Subsystem initialization controller
     *
     * >>> -------------------------------------------------------------- <<<
     * >>>                   I Municipality                               <<<
     * >>> -------------------------------------------------------------- <<<
     * 
     * Populates relevant SessionBean members through calls to the governing
     * Coordinator classes. Also initializes query lists for query-able BObs
     * 
     * @param cred of the requesting User
     * @param ss under current configuration
     * @throws SessionException for all initialization issues
     */
    private void initSubsystem_I_Municipality(UserAuthorized ua, SubSysEnum ss) throws SessionException{
        MunicipalityCoordinator mc = getMuniCoordinator();
        
        MunicipalityDataHeavy muniHeavy;
        try {
            muniHeavy = mc.assembleMuniDataHeavy(ua.getMyCredential().getGoverningAuthPeriod().getMuni(), ua);
            System.out.println("Muni for auth: " + muniHeavy.getMuniName());
        } catch (IntegrationException | AuthorizationException | BObStatusException | EventException | BlobException ex) {
            System.out.println(ex);
            throw new SessionException("Error creating muni data heavy", ex, ss, ExceptionSeverityEnum.SESSION_FATAL);
        }
        sb.setSessMuni(muniHeavy);
        // fall back plan to get the UA a muni profile; this should be set when assembling the muni data heavy above
        if(sb.getSessMuni() != null && sb.getSessMuni().getProfile() != null && ua.getGoverningMuniProfile() == null){
            ua.setGoverningMuniProfile(sb.getSessMuni().getProfile());
            System.out.println("I:muni | setting governing muni profile to object ID: " + ua.getGoverningMuniProfile().getProfileID());
        } 
    }

     /**
     * Subsystem initialization controller
     *
     * >>> -------------------------------------------------------------- <<<
     * >>>                   II Codebook                                  <<<
     * >>> -------------------------------------------------------------- <<<
     * 
     * Populates relevant SessionBean members through calls to the governing
     * Coordinator classes. Also initializes query lists for query-able BObs
     * 
     * @param cred of the requesting User
     * @param ss under current configuration
     * @throws SessionException for all initialization issues
     */
    private void initSubsystem_II_CodeBook(Credential cred, SubSysEnum ss, MunicipalityDataHeavy mdh) throws SessionException{
        // the session code book is the current muni DH's codebook
        sb.setSessCodeSet(mdh.getCodeSet());
        System.out.println("CodeSet name: " + sb.getSessCodeSet().getDBTableName());
        
    }

     /**
     * Subsystem initialization controller
     *
     * >>> -------------------------------------------------------------- <<<
     * >>>                   III Property                                 <<<
     * >>> -------------------------------------------------------------- <<<
     * 
     * Populates relevant SessionBean members through calls to the governing
     * Coordinator classes. Also initializes query lists for query-able BObs
     * 
     * Revised for session load errors on 23-FEB-2023
     * 
     * @param cred of the requesting User
     * @param ss under current configuration
     * @throws SessionException for all initialization issues
     */  
    private void initSubsystem_III_Property(UserAuthorized ua, SubSysEnum ss) throws SessionException{
        SessionBean sessBean = getSessionBean();
        if(sessBean.getSessMuni() != null){
            sessBean.setSessProperty(sessBean.getSessMuni().getMuniPropertyDH());
            if(sessBean.getSessProperty() != null){
                System.out.println("Session Property ID: " + sessBean.getSessProperty().getParcelKey());
            } else {
                System.out.println("initSubsystem_III_Property | no session property");
            }
        } else {
            throw new SessionException("Property Subsystem Error: No session muni avail for assigning session property");
        }
        
        if(sessBean.getSessProperty() != null && sessBean.getSessProperty().getAddress() != null){
            sessBean.setSessMailingAddress(sessBean.getSessProperty().getAddress());
        } else {
            System.out.println("SessionInitializer.initSubsystem_III_Property | NO MAILING ADDRESS ON SESSION PROPERTY!");
        }
        // clear our list
        sessBean.setSessPropertyList(new ArrayList<>());
    }


     /**
     * Subsystem initialization controller
     *
     * >>> -------------------------------------------------------------- <<<
     * >>>                   IV Person                                    <<<
     * >>> -------------------------------------------------------------- <<<
     * 
     * Populates relevant SessionBean members through calls to the governing
     * Coordinator classes. Also initializes query lists for query-able BObs
     * 
     * @param cred of the requesting User
     * @param ss under current configuration
     * @throws SessionException for all initialization issues
     */
    private void initSubsystem_IV_Person(Credential cred, SubSysEnum ss) throws SessionException{
        sb.setSessHumanListRefreshedList(null);
  
    }

     /**
     * Subsystem initialization controller
     *
     * >>> -------------------------------------------------------------- <<<
     * >>>                   V Event                                     <<<
     * >>> -------------------------------------------------------------- <<<
     * 
     * Populates relevant SessionBean members through calls to the governing
     * Coordinator classes. Also initializes query lists for query-able BObs
     * 
     * @param cred of the requesting User
     * @param ss under current configuration
     * @throws SessionException for all initialization issues
     */
    private void initSubsystem_V_Event(Credential cred, SubSysEnum ss) throws SessionException{
       FacesContext context = getFacesContext();
       SessionEventConductor sec = context.getApplication()
                .evaluateExpressionGet(
                        context, 
                        "#{sessionEventConductor}", 
                        SessionEventConductor.class);
       sec.refreshCalendarAndFollowupBacklog(null);
       
    }


     /**
     * Subsystem initialization controller
     *
     * >>> -------------------------------------------------------------- <<<
     * >>>                   VI OccPeriod                                 <<<
     * >>> -------------------------------------------------------------- <<<
     * 
     * Populates relevant SessionBean members through calls to the governing
     * Coordinator classes. Also initializes query lists for query-able BObs
     * 
     * @param cred of the requesting User
     * @param ss under current configuration
     * @throws SessionException for all initialization issues
     */
    private void initSubsystem_VI_OccPeriod(UserAuthorized ua, SubSysEnum ss) throws SessionException{
        SearchCoordinator sc = getSearchCoordinator();
        
        // Commented out all this crap during upgrade to SessionOccupancyConductor and
        // session sticky searches
        
        // Session object init
//        if(ua.getGoverningMuniProfile().isAutoloadOpenOccPeriods()){
//            System.out.println("OccPeriod init: autoload occ periods: true");
//            QueryOccPeriod qop = null;
//            try {
//                qop = sc.initQuery(QueryOccPeriodEnum.ALL_PERIODS_IN_MUNI, ua.getKeyCard());
//                qop.setRequestingUser(ua);
//                qop = sc.runQuery(qop);
//            } catch (SearchException ex) {
//                System.out.println("OccPeriod init: exception in occ period list query but not aborting entire session init");
//            }
//            if(qop != null){
//                sb.setSessOccPeriodList(qop.getBOBResultList());
//            }
//        } else {
//            System.out.println("OccPeriod init: autoload occ periods: false");
//            sb.setSessOccPeriodList(new ArrayList<>());
//        }
//        sb.setQueryOccPeriodList(sc.buildQueryOccPeriodList(ua.getKeyCard()));
//        if(!sb.getQueryOccPeriodList().isEmpty()){
//            sb.setQueryOccPeriod(sb.getQueryOccPeriodList().get(0));
//        }
//        
//        sb.setQueryOccPermitList(sc.buildQueryOccPermitList(ua.getKeyCard()));
//        if(!sb.getQueryOccPermitList().isEmpty()){
//            sb.setQueryOccPermit(sb.getQueryOccPermitList().get(0));
//        }
    }
    
     /**
     * Subsystem initialization controller
     *
     * >>> -------------------------------------------------------------- <<<
     * >>>                   VII Cecase                                   <<<
     * >>> -------------------------------------------------------------- <<<
     * 
     * Populates relevant SessionBean members through calls to the governing
     * Coordinator classes. Also initializes query lists for query-able BObs
     * 
     * @param cred of the requesting User
     * @param ss under current configuration
     * @throws SessionException for all initialization issues
     */
    private void initSubsystem_VII_CECase(UserAuthorized ua, SubSysEnum ss) throws SessionException{
        SearchCoordinator sc = getSearchCoordinator();
        
        if(ua.getGoverningMuniProfile().isAutoloadOpenCECases()){
            System.out.println("CECase init: autoload cecases true; querying");
            QueryCECase q = sc.initQuery(QueryCECaseEnum.OPENCASES, ua.getKeyCard());
            try {
                sb.setSessCECaseList(sc.runQuery(q, ua).getResults());
            } catch (SearchException ex) {
                System.out.println("CECase init: error building case list but not aborting session load");
            }
        } else {
            System.out.println("CECase init: autoload cecases false; skipping query");
            sb.setSessCECaseList(new ArrayList<>());
        }
        sb.setQueryCECaseList(sc.buildQueryCECaseList(ua.getKeyCard()));
        if(sb.getQueryCECaseList() != null && !sb.getQueryCECaseList().isEmpty()){
            sb.setQueryCECase(sb.getQueryCECaseList().get(0));
        }
    }
    
     /**
     * Subsystem initialization controller
     *
     * >>> -------------------------------------------------------------- <<<
     * >>>                   VIII CEActionrequest                         <<<
     * >>> -------------------------------------------------------------- <<<
     * 
     * Populates relevant SessionBean members through calls to the governing
     * Coordinator classes. Also initializes query lists for query-able BObs
     * 
     * @param cred of the requesting User
     * @param ss under current configuration
     * @throws SessionException for all initialization issues
     */
    private void initSubsystem_VIII_CEActionRequest(Credential cred, SubSysEnum ss) throws SessionException{
        SearchCoordinator sc = getSearchCoordinator();
        CaseCoordinator cc = getCaseCoordinator();
        QueryCEAR qCear = sc.initQuery(QueryCEAREnum.UNPROCESSED, cred);
        qCear.getPrimaryParams().setMuni_ctl(true);
        qCear.getPrimaryParams().setMuni_val(cred.getGoverningAuthPeriod().getMuni());
        try {
            getSessionBean().setSessCEARListUnprocessed(cc.cear_getCEARPropertyHeavyList(sc.runQuery(qCear).getBOBResultList()));
        } catch (SearchException | BObStatusException | IntegrationException ex) {
            System.out.println(ex);
        }
    }


     /**
     * Subsystem initialization controller
     *
     * >>> -------------------------------------------------------------- <<<
     * >>>                   VIV Occapp                                   <<<
     * >>> -------------------------------------------------------------- <<<
     * 
     * Populates relevant SessionBean members through calls to the governing
     * Coordinator classes. Also initializes query lists for query-able BObs
     * 
     * @param cred of the requesting User
     * @param ss under current configuration
     * @throws SessionException for all initialization issues
     */
    private void initSubsystem_VIV_OccApp(Credential cred, SubSysEnum ss) throws SessionException{
        // nothing to do here yet
    }
    



     /**
     * Subsystem initialization controller
     *
     * >>> -------------------------------------------------------------- <<<
     * >>>                   X Payment                                    <<<
     * >>> -------------------------------------------------------------- <<<
     * 
     * Populates relevant SessionBean members through calls to the governing
     * Coordinator classes. Also initializes query lists for query-able BObs
     * 
     * @param cred of the requesting User
     * @param ss under current configuration
     * @throws SessionException for all initialization issues
     */
    private void initSubsystem_X_Payment(Credential cred, SubSysEnum ss) throws SessionException{
        // nothing to do here yet
    }

    
     /**
     * Subsystem initialization controller
     *
     * >>> -------------------------------------------------------------- <<<
     * >>>                   XI Report                                    <<<
     * >>> -------------------------------------------------------------- <<<
     * 
     * Populates relevant SessionBean members through calls to the governing
     * Coordinator classes. Also initializes query lists for query-able BObs
     * 
     * @param cred of the requesting User
     * @param ss under current configuration
     * @throws SessionException for all initialization issues
     */
    private void initSubsystem_XI_Report(Credential cred, SubSysEnum ss) throws SessionException{ 
        // nothing to do here yet
        
        
    }
    
    

     /**
     * Subsystem initialization controller
     *
     * >>> -------------------------------------------------------------- <<<
     * >>>                   XII Blob                                     <<<
     * >>> -------------------------------------------------------------- <<<
     * 
     * Populates relevant SessionBean members through calls to the governing
     * Coordinator classes. Also initializes query lists for query-able BObs
     * 
     * @param cred of the requesting User
     * @param ss under current configuration
     * @throws SessionException for all initialization issues
     */
    private void initSubsystem_XII_Blob(Credential cred, SubSysEnum ss) throws SessionException{
        BlobCoordinator bc = getBlobCoordinator();
        try {
            sb.setBlobTypeList(bc.getBlobTypeListComplete());
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
            System.out.println("initSubsystem_XII_Blob: Exception but i'm not going ot hold up the show for a blob type list");
            
        }
        
        
        
    }

    
    
     /**
     * Subsystem initialization controller
     *
     * >>> -------------------------------------------------------------- <<<
     * >>>                   XIII PublicInfo                              <<<
     * >>> -------------------------------------------------------------- <<<
     * 
     * Populates relevant SessionBean members through calls to the governing
     * Coordinator classes. Also initializes query lists for query-able BObs
     * 
     * @param cred of the requesting User
     * @param ss under current configuration
     * @throws SessionException for all initialization issues
     */
    private void initSubsystem_XIII_PublicInfoBundle(Credential cred, SubSysEnum ss) throws SessionException{
        
        
    }

    
    
    
     /**
     * Subsystem initialization controller
     *
     * >>> -------------------------------------------------------------- <<<
     * >>>                   XIV Logging                                  <<<
     * >>> -------------------------------------------------------------- <<<
     * 
     * Populates relevant SessionBean members through calls to the governing
     * Coordinator classes. Also initializes query lists for query-able BObs
     * 
     * @param cred of the requesting User
     * @param ss under current configuration
     * @throws SessionException for all initialization issues
     */
    private void initSubsystem_logSession(UserAuthorized authUser, SubSysEnum ss) throws SessionException{
         UserMuniAuthPeriodLogEntry umaple;
         UserCoordinator uc = getUserCoordinator();
         
        umaple = uc.auth_assembleUserMuniAuthPeriodLogEntrySkeleton(
                              authUser, 
                              UserMuniAuthPeriodLogEntryCatEnum.SESSION_INIT);

        umaple = assembleSessionInfo(umaple);

        if(umaple != null){
            umaple.setAudit_usersession_userid(sb.getSessUser().getUserID());
            umaple.setAudit_muni_municode(sb.getSessMuni().getMuniCode());
            umaple.setAudit_usercredential_userid(authUser.getMyCredential().getGoverningAuthPeriod().getUserID());
            try {
                uc.auth_logCredentialInvocation(umaple, authUser.getMyCredential().getGoverningAuthPeriod());
            } catch (IntegrationException | AuthorizationException ex) {
                System.out.println(ex);
                throw new SessionException( "Failure creating user muni auth period log entry", 
                                            ex, ss,
                                            ExceptionSeverityEnum.NONCRITICAL_FAILURE);
            }
        }
        
    }

    
    
    
    

    /**
     * @return the userAuthorizedQueuedForSession
     */
    public UserAuthorized getUserAuthorizedQueuedForSession() {
        return userAuthorizedQueuedForSession;
    }


    /**
     * @param userAuthorizedQueuedForSession the userAuthorizedQueuedForSession to set
     */
    public void setUserAuthorizedQueuedForSession(UserAuthorized userAuthorizedQueuedForSession) {
        this.userAuthorizedQueuedForSession = userAuthorizedQueuedForSession;
    }

    /**
     * @return the muniQueuedForSession
     */
    public Municipality getMuniQueuedForSession() {
        System.out.println("SessionInitializer.getMuniQueuedForSession");
        return muniQueuedForSession;
    }

    /**
     * @param muniQueuedForSession the muniQueuedForSession to set
     */
    public void setMuniQueuedForSession(Municipality muniQueuedForSession) {
        this.muniQueuedForSession = muniQueuedForSession;
    }

   

    /**

     * @return the usernameQueuedForSession
     */
    public String getUsernameQueuedForSession() {
        return usernameQueuedForSession;
    }

    /**
     * @param usernameQueuedForSession the usernameQueuedForSession to set
     */
    public void setUsernameQueuedForSession(String usernameQueuedForSession) {
        this.usernameQueuedForSession = usernameQueuedForSession;
    }

    /**
     * @return the umapQueuedForSession
     */
    public UserMuniAuthPeriod getUmapQueuedForSession() {
        return umapQueuedForSession;
    }

    /**
     * @param umapQueuedForSession the umapQueuedForSession to set
     */
    public void setUmapQueuedForSession(UserMuniAuthPeriod umapQueuedForSession) {
        this.umapQueuedForSession = umapQueuedForSession;
    }

    /**
     * @return the umapCandidateList
     */
    public List<UserMuniAuthPeriod> getUmapCandidateList() {
        return umapCandidateList;
    }

    /**
     * @param umapCandidateList the umapCandidateList to set
     */
    public void setUmapCandidateList(List<UserMuniAuthPeriod> umapCandidateList) {
        this.umapCandidateList = umapCandidateList;
    }

   
}
