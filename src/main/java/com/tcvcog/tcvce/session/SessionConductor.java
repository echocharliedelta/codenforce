/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.session;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.entities.IFaceSessionSyncTarget;
import jakarta.annotation.PostConstruct;
import jakarta.faces.context.FacesContext;
import java.util.Map;

/**
 * The Summer 2022 holder of Session-level methods that previously lived on the
 * dumping ground of the SessionBean. This class handles inter-subsystem issues 
 * like the current system domain (as of July2022 either CE or OCC) and user-level
 * inquiries and permissions. This class will collaborate with the SessionBean 
 * for the forseeable future until full SessionBean deprecation
 * 
 * @author Ellen Bascomb (of Apartment 31Y)
 */
public class SessionConductor extends BackingBeanUtils{

    /**
     * Creates a new instance of SessionConductor
     */
    public SessionConductor() {
    }
    
    private boolean flushViewScopedBeans;
    
    @PostConstruct
    public void initBean()  {
        System.out.println("SessionConductor.initBean");
        flushViewScopedBeans = false;
    }
    
    
     /**
     * Used when users stay authenticated but change session authorization and 
     * potentially muni as well
     */
    public void clearSessionObjectsForSessionReInit(){
        flushViewScopedBeans = true;
        removeSessionInfrastructureForSessionReinit();
        System.out.println("SessionConductor.clearSessionObjectsForSessionReInit");
        
    }
    
    /**
     * called after session reinit
     */
    public void resetFlushViewScopedBean(){
        flushViewScopedBeans = false;
    }
    
     /**
     * Tries to remove session objects from the map
     */
    private void removeSessionInfrastructureForSessionReinit(){
        // let's kill the entire session
        System.out.println("SessionConductor.invalidateSession()!");
        
        // as of Aug 2024 these bean removals seem to be working thanks to ChatGPT
        getFacesContext().getExternalContext().getSessionMap().remove("sessionBean");
        getFacesContext().getExternalContext().getSessionMap().remove("sessionEventConductor");
        getFacesContext().getExternalContext().getSessionMap().remove("sessionConductor");
        getFacesContext().getExternalContext().getSessionMap().remove("sessionOccupancyConductor");
        getFacesContext().getExternalContext().getSessionMap().remove("sessionUserConductor");
        getFacesContext().getExternalContext().getSessionMap().remove("sessionMuniConductor");
        getFacesContext().getExternalContext().getSessionMap().remove("sessionCodeConductor");
        getFacesContext().getExternalContext().getSessionMap().remove("sessionPropertyConductor");
        getFacesContext().getExternalContext().getSessionMap().remove("sessionPersonConductor");
        getFacesContext().getExternalContext().getSessionMap().remove("sessionInspectionConductor");
        getFacesContext().getExternalContext().getSessionMap().remove("sessionCECaseConductor");
        getFacesContext().getExternalContext().getSessionMap().remove("sessionPaymentConductor");
        getFacesContext().getExternalContext().getSessionMap().remove("ceCaseSearchAndReportBB");
        getFacesContext().getExternalContext().getSessionMap().remove("citationSearchBB");
        getFacesContext().getExternalContext().getSessionMap().remove("codeViolationSearchBB");
        getFacesContext().getExternalContext().getSessionMap().remove("cEActionRequestSubmitBB");
        getFacesContext().getExternalContext().getSessionMap().remove("searchCoordinator");
    }
    
    
    /**
     * @return the flushViewScopedBeans
     */
    public boolean isFlushViewScopedBeans() {
        return flushViewScopedBeans;
    }
    
    /**
     * Primary pathway for triggering synchronization of all the session conductor
     * members that require alignment to an activated focus object
     * such as a property or case
     * @param target 
     */
    public void synchronizeSessionObjects(IFaceSessionSyncTarget target){
        if(target != null){
            // call the synchronize method on all our implementing synchronizers
            getSessionOccupancyConductor().synchronizeToSessionFocusObject(target);
        }
    }
    
    
}
