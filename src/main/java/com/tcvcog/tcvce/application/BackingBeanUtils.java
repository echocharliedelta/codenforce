/*
 * Copyright (C) 2017 cedba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.session.SessionBean;
import com.tcvcog.tcvce.coordinators.*;

import java.io.Serializable;
import jakarta.faces.context.FacesContext;
import jakarta.faces.application.Application;

import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.User;
import com.tcvcog.tcvce.integration.*;

// occupancy integrators
import com.tcvcog.tcvce.integration.OccInspectionIntegrator;
import com.tcvcog.tcvce.integration.OccupancyIntegrator;
import com.tcvcog.tcvce.integration.PaymentIntegrator;

// system integrators
import com.tcvcog.tcvce.integration.TaskIntegrator;
import com.tcvcog.tcvce.integration.SystemIntegrator;
import com.tcvcog.tcvce.integration.LogIntegrator;
import com.tcvcog.tcvce.integration.OccChecklistIntegrator;
import com.tcvcog.tcvce.integration.OccInspectionCacheManager;
import com.tcvcog.tcvce.integration.SystemMuniUserCacheManager;
import com.tcvcog.tcvce.session.*;
import com.tcvcog.tcvce.util.Constants;
import com.tcvcog.tcvce.util.DateTimeUtil;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import jakarta.el.ValueExpression;
import jakarta.faces.context.ExternalContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import jakarta.servlet.http.HttpSession;
import javax.sql.DataSource;


/**
 * Collection of convenience methods for accessing application-level objects
 * that beans of various scopes use, most notably Coordinators and Integrators
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */

public class        BackingBeanUtils 
        extends     DateTimeUtil
        implements  Serializable{
    
//    @ManagedProperty(value="sessionBean")
    private SessionBean sessionBean;
    
    private UserCoordinator userCoordinator;
    private UserIntegrator userIntegrator;
    
    private MunicipalityIntegrator municipalityIntegrator;
    
    private CaseCoordinator caseCoordinator;
    private CaseIntegrator caseIntegrator;
    
    private EventCoordinator eventCoordinator;
    private EventIntegrator eventIntegrator;
    
    private CourtEntityIntegrator courtEntityIntegrator;
    
    private PropertyIntegrator propertyIntegrator;
    private CEActionRequestIntegrator ceActionRequestIntegrator;
    private PublicInfoCoordinator publicInfoCoordinator;
    private PersonCoordinator personCoordinator;
    private PropertyCoordinator propertyCoordinator;

    private OccupancyIntegrator occupancyIntegrator;
    private OccInspectionIntegrator occInspectionIntegrator;
    private OccChecklistIntegrator occChecklistIntegrator;

    private OccupancyCoordinator occupancyCoordinator;
    private OccInspectionCoordinator occInspectionCoordinator;

    private PaymentIntegrator paymentIntegrator;
    private PaymentCoordinator paymentCoordinator;
    
    private BlobCoordinator blobCoordinator;
    private BlobIntegrator blobIntegrator;
    private PermissionsCoordinator permissionsCoordinator;
    
    // system 
    private SystemIntegrator systemIntegrator;
    private SystemCoordinator systemCoordinator;
    private LogIntegrator logIntegrator;
    private MunicipalityCoordinator muniCoordinator;
    
    private SearchCoordinator searchCoordinator;
    
    private User facesUser;
    
    private DataSource dataSource;
    private Connection connx;
    
    // Cache managers
    private OccInspectionCacheManager occInspectionCacheManager;
    private SystemMuniUserCacheManager systemMuniCacheManager;
    private CodeCacheManager codeCacheManager;
    
    private PropertyCacheManager propertyCacheManager;
    private PersonCacheManager personCacheManager;
    private CECaseCacheManager ceCaseCacheManager;
    
    private OccupancyCacheManager occupancyCacheManager;
    private EventCacheManager eventCacheManager;
    private BlobCacheManager blobCacheManager;
    
    
   
    
    
    /**
     * Creates a new instance of BackingBeanUtils
     */
    public BackingBeanUtils() {
        
        // this creation of the usercorodinator should be managed by the 
        // MBCF but doesn't seem to be--this is not a solid object-oriented
        // design concept that works well with the bean model
        // it should be made by the MBCF
        //System.out.println("Constructing BackingBean Utils");
        //userCoordinator = new UserCoordinator();
    }
    
    public static java.sql.Timestamp getCurrentTimeStamp(){
        java.util.Date date = new java.util.Date();
        return new java.sql.Timestamp(date.getTime());
        
    }
    
    public FacesContext getFacesContext(){
        return FacesContext.getCurrentInstance();
    }
    
    public Application getApplication(){
        return getFacesContext().getApplication();
    }
    
    public ResourceBundle getResourceBundle(String bundleName){
        FacesContext context = getFacesContext();
        Application app = getApplication();
        ResourceBundle bundle = app.getResourceBundle(context, bundleName );
        return bundle;
    }
    
        
    public boolean isMunicodeInMuniList(int muniCode, List<Municipality> muniList){
        Municipality m;
        boolean isInList = false;
        Iterator<Municipality> iter = muniList.iterator();
        while(iter.hasNext()){
            m = iter.next();
            if(m.getMuniCode() == muniCode){
                isInList = true;
            }
        }
        return isInList;
    }
    
    
    public void setUserCoordinator(UserCoordinator userCoordinator){
        this.userCoordinator = userCoordinator;
    }

    /**
     * Cerates a PostgresConnectionFactory factory and calls getCon to get a handle on the
     * database connection
     * @return the postgresCon
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public Connection getPostgresCon() {
        String jndi_name = getResourceBundle(Constants.DB_CONNECTION_PARAMS).getString("jndi_name");
        
        Context initContext = null;
        try {
            initContext = new InitialContext();
            // updated for Wildfly PREVIEW 26.1.0
            Context envCtx = (Context) initContext.lookup("java:jboss/");
            dataSource = (DataSource) envCtx.lookup(jndi_name);
            connx = dataSource.getConnection();
        } catch (NamingException | SQLException ex) {
            System.out.println("BackingBeanUtils.getPostgresCon | ERROR in primary connection factory for postgres!");
            System.out.println(ex);
        }
        return connx;
      
    }

    /**
     * To release connection and associated resources
     * @param connection
     * @param statement
     * @param resultSet
     */
    public void releasePostgresConnection(Connection connection, PreparedStatement statement, ResultSet resultSet) {
        closeResultSet(resultSet);
        closeStatement(statement);
        closeConnection(connection);
    }

    public void releasePostgresConnection(Connection connection, PreparedStatement statement) {
        closeStatement(statement);
        closeConnection(connection);
    }

    /**
     * Utility used by integration classes for closing a connection object
     * @param connection 
     */
    private void closeConnection(Connection connection) {
        if (Objects.nonNull(connection)) {
            try {
                connection.close();
            } catch (SQLException e) { 
                System.out.println("BackingBeanUtils.closeConnection | EXCEPTION closing connection");
            }
        }
    }

    /**
     * Utility for integration classes for closing statements
     * @param preparedStatement 
     */
    private void closeStatement(PreparedStatement preparedStatement) {
        if (Objects.nonNull(preparedStatement)) {
            try {
                preparedStatement.close();
            } catch (SQLException e) { 
                System.out.println("BackingBeanUtils.closeStatement | EXCEPTION closing statememt");
            }
        }
    }

    /**
     * Utility for integration classes for closing result set objects
     * @param resultSet 
     */
    private void closeResultSet(ResultSet resultSet) {
        if (Objects.nonNull(resultSet)) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                System.out.println("BackingBeanUtils.resultSet | EXCEPTION closing ResultSet");
            }
        }
    }
    
    static final String EMPTY_STRING = "";
    static final String SINGLE_SPACE = " ";
    static final String DOUBLE_SPACE = " ";
    static final String LONE_BREAK = "<br>";
    static final String LONE_BREAK2 = "<br/>";
    static final String LONE_BREAK3 = "<br />";
    
    /**
     * Checks a string for being null, an empty string, or a blank space, or a double space
     * @param s
     * @return true if the string is null, an empty string, or a single or double space, otherwise returns false
     */
    public boolean isStringEmptyIsh(String s){
        if(s == null){
            return true;
        }
        if(s.equals(EMPTY_STRING)){
            return true;
        }
        if(s.equals(SINGLE_SPACE)){
            return true;
        }
        if(s.equals(DOUBLE_SPACE)){
            return true;
        }
        if(s.equals(LONE_BREAK)){
            return true;
        }
        if(s.equals(LONE_BREAK2)){
            return true;
        }
        if(s.equals(LONE_BREAK3)){
            return true;
        }
        if(s.length() == 0){
            return true;
        }
        return false;
    }
   
 
    
    /**
     * Checks if the given resultSet contains the given column. Can be used to prevent SQL errors.
     * @param rs The resultSet to be tested
     * @param column The column which will be checked for
     * @return True if resultSet contains the given column, false if not.
     * @throws SQLException
     */
    public boolean hasColumn(ResultSet rs, String column) throws SQLException {
        ResultSetMetaData meta = rs.getMetaData();
        int columns = meta.getColumnCount();
        for(int index = 1; index <= columns; index++){
            if(column.equals(meta.getColumnName(index))) {
                return true;
            }
        
        }
        return false;
    }
    
    /**
     * Executes a value expression to retrieve a reference to this session's
     * requested conductor object. Conductors manage non-business rule, session-applicable
     * business object operations, such as refreshing of session-level BOBs
     * 
     * @return the requested conductor
     */
    public SessionConductor getSessionConductor(){
        FacesContext context = getFacesContext();
        return context.getApplication()
                .evaluateExpressionGet(
                        context, 
                        "#{sessionConductor}", 
                        SessionConductor.class);
    }
    
    
    
    /**
     * Executes a value expression to retrieve a reference to this session's
     * requested conductor object. Conductors manage non-business rule, session-applicable
     * business object operations, such as refreshing of session-level BOBs
     * 
     * @return the requested conductor
     */
    public SessionOccupancyConductor getSessionOccupancyConductor(){
        FacesContext context = getFacesContext();
        return context.getApplication()
                .evaluateExpressionGet(
                        context, 
                        "#{sessionOccupancyConductor}", 
                        SessionOccupancyConductor.class);
    }
    
    /**
     * Executes a value expression to retrieve a reference to this session's
     * requested conductor object. Conductors manage non-business rule, session-applicable
     * business object operations, such as refreshing of session-level BOBs
     * 
     * @return the requested conductor
     */
    public SessionCECaseConductor getSessionCECaseConductor(){
        FacesContext context = getFacesContext();
        return context.getApplication()
                .evaluateExpressionGet(
                        context, 
                        "#{sessionCECaseConductor}", 
                        SessionCECaseConductor.class);
    }
    
    
    /**
     * Executes a value expression to retrieve a reference to this session's
     * requested conductor object. Conductors manage non-business rule, session-applicable
     * business object operations, such as refreshing of session-level BOBs
     * 
     * @return the requested conductor
     */
    public SessionCodeConductor getSessionCodeConductor(){
        FacesContext context = getFacesContext();
        return context.getApplication()
                .evaluateExpressionGet(
                        context, 
                        "#{sessionCodeConductor}", 
                        SessionCodeConductor.class);
    }
    
    /**
     * Executes a value expression to retrieve a reference to this session's
     * requested conductor object. Conductors manage non-business rule, session-applicable
     * business object operations, such as refreshing of session-level BOBs
     * 
     * @return the requested conductor
     */
    public SessionEventConductor getSessionEventConductor(){
        FacesContext context = getFacesContext();
        return context.getApplication()
                .evaluateExpressionGet(
                        context, 
                        "#{sessionEventConductor}", 
                        SessionEventConductor.class);
    }
    
    /**
     * Executes a value expression to retrieve a reference to this session's
     * requested conductor object. Conductors manage non-business rule, session-applicable
     * business object operations, such as refreshing of session-level BOBs
     * 
     * @return the requested conductor
     */
    public SessionMuniConductor getSessionMuniConductor(){
        FacesContext context = getFacesContext();
        return context.getApplication()
                .evaluateExpressionGet(
                        context, 
                        "#{sessionMuniConductor}", 
                        SessionMuniConductor.class);
    }
    
    /**
     * Executes a value expression to retrieve a reference to this session's
     * requested conductor object. Conductors manage non-business rule, session-applicable
     * business object operations, such as refreshing of session-level BOBs
     * 
     * @return the requested conductor
     */
    public SessionInspectionConductor getSessionInspectionConductor(){
        FacesContext context = getFacesContext();
        return context.getApplication()
                .evaluateExpressionGet(context, 
                        "#{sessionInspectionConductor}", 
                        SessionInspectionConductor.class);
    }
    
    /**
     * Executes a value expression to retrieve a reference to this session's
     * requested conductor object. Conductors manage non-business rule, session-applicable
     * business object operations, such as refreshing of session-level BOBs
     * 
     * @return the requested conductor
     */
    public SessionPaymentConductor getSessionPaymentConductor(){
        FacesContext context = getFacesContext();
        return context.getApplication()
                .evaluateExpressionGet(
                        context, 
                        "#{sessionPaymentConductor}", 
                        SessionPaymentConductor.class);
    }
    
    /**
     * Executes a value expression to retrieve a reference to this session's
     * requested conductor object. Conductors manage non-business rule, session-applicable
     * business object operations, such as refreshing of session-level BOBs
     * 
     * @return the requested conductor
     */
    public SessionPersonConductor getSessionPersonConductor(){
        FacesContext context = getFacesContext();
        return context.getApplication()
                .evaluateExpressionGet(
                        context, 
                        "#{sessionPersonConductor}", 
                        SessionPersonConductor.class);
    }
    
    /**
     * Executes a value expression to retrieve a reference to this session's
     * requested conductor object. Conductors manage non-business rule, session-applicable
     * business object operations, such as refreshing of session-level BOBs
     * 
     * @return the requested conductor
     */
    public SessionPropertyConductor getSessionPropertyConductor(){
        FacesContext context = getFacesContext();
        return context.getApplication()
                .evaluateExpressionGet(
                        context, 
                        "#{sessionPropertyConductor}", 
                        SessionPropertyConductor.class);
    }
    
    /**
     * Executes a value expression to retrieve a reference to this session's
     * requested conductor object. Conductors manage non-business rule, session-applicable
     * business object operations, such as refreshing of session-level BOBs
     * 
     * @return the requested conductor
     */
    public SessionUserConductor getSessionUserConductor(){
        FacesContext context = getFacesContext();
        return context.getApplication()
                .evaluateExpressionGet(
                        context, 
                        "#{sessionUserConductor}", 
                        SessionUserConductor.class);
    }
    
    
    
    
    public CodeCoordinator getCodeCoordinator() {
        FacesContext context = getFacesContext();
        CodeCoordinator coord = context.getApplication()
                .evaluateExpressionGet(
                        context, 
                        "#{codeCoordinator}", 
                        CodeCoordinator.class);
    return coord;
        
    }
    
    public CodeIntegrator getCodeIntegrator(){
        FacesContext context = getFacesContext();
        CodeIntegrator codeInt = context.getApplication()
                .evaluateExpressionGet(
                        context, 
                        "#{codeIntegrator}", 
                        CodeIntegrator.class);
        
        return codeInt;
        
    }
    
    public PersonIntegrator getPersonIntegrator(){
        FacesContext context = getFacesContext();
        PersonIntegrator personIntegrator = context.getApplication()
                .evaluateExpressionGet(
                        context, 
                        "#{personIntegrator}", 
                        PersonIntegrator.class);
        
        return personIntegrator;
        
    }
    
     
    
    public MunicipalityIntegrator getMunicipalityIntegrator(){
        FacesContext context = getFacesContext();
        municipalityIntegrator = context. getApplication()
                .evaluateExpressionGet(
                        context, 
                        "#{municipalityIntegrator}", 
                        MunicipalityIntegrator.class);
        return municipalityIntegrator;
    }
    
    
    /** ********************************************************************
    /** ********************************************************************
    /** ********************   CACHE MANAGERS  *****************************
    /** ********************************************************************
    /** ********************************************************************/
    
    
    
   
    /**
     * EL getter for cache manager
     * @return 
     */
    public OccInspectionCacheManager getOccInspectionCacheManager(){
        if(occInspectionCacheManager == null){
            FacesContext context = FacesContext.getCurrentInstance();
            occInspectionCacheManager = (OccInspectionCacheManager) context.getApplication().getELResolver()
                    .getValue(context.getELContext(), null, "occInspectionCacheManager");
            
        }
        return occInspectionCacheManager;
    }
    
    
    /**
     * EL getter for cache manager
     * @return 
     */
    public SystemMuniUserCacheManager getSystemMuniCacheManager(){
        if(systemMuniCacheManager == null){
            FacesContext context = FacesContext.getCurrentInstance();
            systemMuniCacheManager = (SystemMuniUserCacheManager) context.getApplication().getELResolver()
                    .getValue(context.getELContext(), null, "systemMuniUserCacheManager");
           
        }
        return systemMuniCacheManager;
    }
    
    
    /**
     * EL getter for cache manager
     * @return 
     */
    public CodeCacheManager getCodeCacheManager(){
        if(codeCacheManager == null){
            FacesContext context = FacesContext.getCurrentInstance();
            codeCacheManager = (CodeCacheManager) context.getApplication().getELResolver()
                    .getValue(context.getELContext(), null, "codeCacheManager");
            
        }
        return codeCacheManager;
    }
    
    /**
     * EL getter for cache manager
     * @return 
     */
    public PropertyCacheManager getPropertyCacheManager(){
        if(propertyCacheManager == null){
            FacesContext context = FacesContext.getCurrentInstance();
            propertyCacheManager = (PropertyCacheManager) context.getApplication().getELResolver()
                    .getValue(context.getELContext(), null, "propertyCacheManager");
            
        }
        return propertyCacheManager;
    }
    
    /**
     * EL getter for cache manager
     * @return 
     */
    public PersonCacheManager getPersonCacheManager(){
        if(personCacheManager == null){
            FacesContext context = FacesContext.getCurrentInstance();
            personCacheManager = (PersonCacheManager) context.getApplication().getELResolver()
                    .getValue(context.getELContext(), null, "personCacheManager");
            
        }
        return personCacheManager;
    }
    
    /**
     * EL getter for cache manager
     * @return 
     */
    public CECaseCacheManager getCECaseCacheManager(){
        if(ceCaseCacheManager == null){
            FacesContext context = FacesContext.getCurrentInstance();
            ceCaseCacheManager = (CECaseCacheManager) context.getApplication().getELResolver()
                    .getValue(context.getELContext(), null, "ceCaseCacheManager");
            
        }
        return ceCaseCacheManager;
    }
    
    /**
     * EL getter for cache manager
     * @return 
     */
    public OccupancyCacheManager getOccupancyCacheManager(){
        if(occupancyCacheManager == null){
            FacesContext context = FacesContext.getCurrentInstance();
            occupancyCacheManager = (OccupancyCacheManager) context.getApplication().getELResolver()
                    .getValue(context.getELContext(), null, "occupancyCacheManager");
            
        }
        return occupancyCacheManager;
    }
    
    /**
     * EL getter for cache manager
     * @return 
     */
    public EventCacheManager getEventCacheManager(){
        if(eventCacheManager == null){
            FacesContext context = FacesContext.getCurrentInstance();
            eventCacheManager = (EventCacheManager) context.getApplication().getELResolver()
                    .getValue(context.getELContext(), null, "eventCacheManager");
            
        }
        return eventCacheManager;
    }
    
    /**
     * EL getter for cache manager
     * @return 
     */
    public BlobCacheManager getBlobCacheManager(){
        if(blobCacheManager == null){
            FacesContext context = FacesContext.getCurrentInstance();
            blobCacheManager = (BlobCacheManager) context.getApplication().getELResolver()
                    .getValue(context.getELContext(), null, "blobCacheManager");
            
        }
        return blobCacheManager;
    }
    
    
    
    
    
  
    
    /** ********************************************************************
    /** ********************************************************************
    /** ********************   COORDINATORS    *****************************
    /** ********************************************************************
    /** ********************************************************************/
    
    
    

    /**
     * @return the userCoordinator
     */
    public UserCoordinator getUserCoordinator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(),
                "#{userCoordinator}", UserCoordinator.class);
       userCoordinator = (UserCoordinator) ve.getValue(context.getELContext());
        return userCoordinator;
    }



    /**
     * @param muniIntegrator the muniIntegrator to set
     */
    public void setMunicipalityIntegrator(MunicipalityIntegrator muniIntegrator) {
        this.municipalityIntegrator = muniIntegrator;
    }

    /**
     * @return the propertyIntegrator
     */
    public PropertyIntegrator getPropertyIntegrator() {
        
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), 
                        "#{propertyIntegrator}", PropertyIntegrator.class);
        propertyIntegrator = (PropertyIntegrator) ve.getValue(context.getELContext());
   
        return propertyIntegrator;
    }
    
   
    

    /**
     * @param propertyIntegrator the propertyIntegrator to set
     */
    public void setPropertyIntegrator(PropertyIntegrator propertyIntegrator) {
        this.propertyIntegrator = propertyIntegrator;
    }

    /**
     * @return the cEActionRequestIntegrator
     */
    public CEActionRequestIntegrator getcEActionRequestIntegrator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), 
                        "#{ceActionRequestIntegrator}", CEActionRequestIntegrator.class);
        ceActionRequestIntegrator = (CEActionRequestIntegrator) ve.getValue(context.getELContext());
        return ceActionRequestIntegrator;
    }

    /**
     * @param cEActionRequestIntegrator the cEActionRequestIntegrator to set
     */
    public void setcEActionRequestIntegrator(CEActionRequestIntegrator cEActionRequestIntegrator) {
        this.ceActionRequestIntegrator = cEActionRequestIntegrator;
    }

    /**
     * @return the userIntegrator
     */
    public UserIntegrator getUserIntegrator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{userIntegrator}", UserIntegrator.class);
        userIntegrator = (UserIntegrator) ve.getValue(context.getELContext());
        
        
        return userIntegrator;
    }

    /**
     * @param userIntegrator the userIntegrator to set
     */
    public void setUserIntegrator(UserIntegrator userIntegrator) {
        this.userIntegrator = userIntegrator;
    }

    /**
     * @return the caseCoordinator
     */
    public CaseCoordinator getCaseCoordinator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{caseCoordinator}", CaseCoordinator.class);
        caseCoordinator = (CaseCoordinator) ve.getValue(context.getELContext());
        return caseCoordinator;
    }

    /**
     * @param caseCoordinator the caseCoordinator to set
     */
    public void setCaseCoordinator(CaseCoordinator caseCoordinator) {
        this.caseCoordinator = caseCoordinator;
    }

    /**
     * @return the eventCoordinator
     */
    public EventCoordinator getEventCoordinator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{eventCoordinator}", EventCoordinator.class);
        eventCoordinator = (EventCoordinator) ve.getValue(context.getELContext());
        return eventCoordinator;
    }

    /**
     * @param eventCoordinator the eventCoordinator to set
     */
    public void setEventCoordinator(EventCoordinator eventCoordinator) {
        this.eventCoordinator = eventCoordinator;
    }

    /**
     * @return the eventIntegrator
     */
    public EventIntegrator getEventIntegrator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{eventIntegrator}", EventIntegrator.class);
        eventIntegrator = (EventIntegrator) ve.getValue(context.getELContext());
        
        return eventIntegrator;
    }

    /**
     * @param eventIntegrator the eventIntegrator to set
     */
    public void setEventIntegrator(EventIntegrator eventIntegrator) {
        this.eventIntegrator = eventIntegrator;
    }

    /**
     * @return the caseIntegrator
     */
    public CaseIntegrator getCaseIntegrator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{caseIntegrator}", CaseIntegrator.class);
        caseIntegrator = (CaseIntegrator) ve.getValue(context.getELContext());
        
        return caseIntegrator;
        
        
    }

    /**
     * @param caseIntegrator the caseIntegrator to set
     */
    public void setCaseIntegrator(CaseIntegrator caseIntegrator) {
        this.caseIntegrator = caseIntegrator;
    }
    
    /**
     * Calculates the number of days since a given date and the current system 
     * date.
     * @param pastDate
     * @return the number of calendar days since a given date 
     */
    public long getDaysSince(LocalDateTime pastDate){
        LocalDateTime currentDateTime = LocalDateTime.now();
        Long daysBetween = pastDate.atZone(ZoneId.systemDefault())
                .until(currentDateTime.atZone(ZoneId.systemDefault()), java.time.temporal.ChronoUnit.DAYS);
        return daysBetween;
        
        
    }

    
    public String getPrettyLocalDateOnlyNoTime(LocalDate d){
        return DateTimeUtil.getPrettyLocalDateNoTime(d);
    }
    
    
  
   
   
    /**
     * @return the courtEntityIntegrator
     */
    public CourtEntityIntegrator getCourtEntityIntegrator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{courtEntityIntegrator}", CourtEntityIntegrator.class);
        courtEntityIntegrator = (CourtEntityIntegrator) ve.getValue(context.getELContext());
        return courtEntityIntegrator;
    }

    
    /**
     * @return the paymentIntegrator
     */
    public PaymentIntegrator getPaymentIntegrator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{paymentIntegrator}", PaymentIntegrator.class);
        paymentIntegrator = (PaymentIntegrator) ve.getValue(context.getELContext());
        return paymentIntegrator;
    }

    public PaymentCoordinator getPaymentCoordinator(){
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{paymentCoordinator}", PaymentCoordinator.class);
        paymentCoordinator = (PaymentCoordinator) ve.getValue(context.getELContext());
        return paymentCoordinator;
    }
    
    /**
     * @param courtEntityIntegrator the courtEntityIntegrator to set
     */
    public void setCourtEntityIntegrator(CourtEntityIntegrator courtEntityIntegrator) {
        this.courtEntityIntegrator = courtEntityIntegrator;
    }

    

    /**
     * @param paymentIntegrator the paymentIntegrator to set
     */
    public void setPaymentIntegrator(PaymentIntegrator paymentIntegrator) {
        this.paymentIntegrator = paymentIntegrator;
    }


    /**
     * @return the occInspectionIntegrator
     */
    public OccInspectionIntegrator getOccInspectionIntegrator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{occInspectionIntegrator}", OccInspectionIntegrator.class);
        occInspectionIntegrator = (OccInspectionIntegrator) ve.getValue(context.getELContext());
        
        return occInspectionIntegrator;
    }

    /**
     * @param occInspectionIntegrator the occInspectionIntegrator to set
     */
    public void setOccInspectionIntegrator(OccInspectionIntegrator occInspectionIntegrator) {
        this.occInspectionIntegrator = occInspectionIntegrator;
    }

    /**
     * @return the occupancyIntegrator
     */
    public OccupancyIntegrator getOccupancyIntegrator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{occupancyIntegrator}", OccupancyIntegrator.class);
        occupancyIntegrator = (OccupancyIntegrator) ve.getValue(context.getELContext());
        
        return occupancyIntegrator;
    }

    /**
     * @param occupancyIntegrator the occupancyIntegrator to set
     */
    public void setOccupancyIntegrator(OccupancyIntegrator occupancyIntegrator) {
        this.occupancyIntegrator = occupancyIntegrator;
    }

    /**
     * @return the systemIntegrator
     */
    public SystemIntegrator getSystemIntegrator() {
        
        if(systemIntegrator == null){
            FacesContext context = getFacesContext();
            systemIntegrator = (SystemIntegrator) context.getApplication().getELResolver()
                    .getValue(context.getELContext(), null, "systemIntegrator");
        }
        
        return systemIntegrator;
    }

    /**
     * @return the logIntegrator
     */
    public LogIntegrator getLogIntegrator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{logIntegrator}", LogIntegrator.class);
        logIntegrator = (LogIntegrator) ve.getValue(context.getELContext());
        
        return logIntegrator;
    }
    
     /**
     * @return the sessionBean
     */
    public SearchCoordinator getSearchCoordinator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{searchCoordinator}", SearchCoordinator.class);
        searchCoordinator = (SearchCoordinator) ve.getValue(context.getELContext());
        return searchCoordinator;
    }

    /**
     * @return the sessionBean
     */
    public SessionBean getSessionBean() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{sessionBean}", SessionBean.class);
        sessionBean = (SessionBean) ve.getValue(context.getELContext());
        return sessionBean;
    }

    /**
     * @param sessionBean the sessionBean to set
     */
    public void setSessionBean(SessionBean sessionBean) {
        this.sessionBean = sessionBean;
    }

    /**
     * @return the facesUser
     */
    public User getJBOSSUser() {
        ExternalContext ec = getFacesContext().getExternalContext();
        facesUser = (User) ec.getSessionMap().get("facesUser");
        return facesUser;
    }

    /**
     * @param facesUser the facesUser to set
     */
    public void setSessionUser(User facesUser) {
        this.facesUser = facesUser;
    }
    
    public String getSessionID(){
        
        FacesContext fc = getFacesContext();
        HttpSession session = (HttpSession) fc.getExternalContext().getSession(false);
        // prints out the current session attributes to standard out
//        Enumeration e = session.getAttributeNames();
//        System.out.println("SessionInitailzier.getSessionID | Dumping lots of attrs");
//        while (e.hasMoreElements())
//        {
//          String attr = (String)e.nextElement();
//          System.out.println("      attr  = "+ attr);
//          Object value = session.getValue(attr);
//          System.out.println("      value = "+ value);
//        }
        String sessionID = session.getId();
        return sessionID;
    }

    /**
     * @return the publicInfoCoordinator
     */
    public PublicInfoCoordinator getPublicInfoCoordinator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{publicInfoCoordinator}", PublicInfoCoordinator.class);
        publicInfoCoordinator = (PublicInfoCoordinator) ve.getValue(context.getELContext());
        
        return publicInfoCoordinator;
    }

    /**
     * @param publicInfoCoordinator the publicInfoCoordinator to set
     */
    public void setPublicInfoCoordinator(PublicInfoCoordinator publicInfoCoordinator) {
        this.publicInfoCoordinator = publicInfoCoordinator;
    }

    /**
     * @return the dataSource
     */
    public DataSource getDataSource() {
        
        return dataSource;
    }

    /**
     * @param dataSource the dataSource to set
     */
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * @return the personCoordinator
     */
    public PersonCoordinator getPersonCoordinator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{personCoordinator}", PersonCoordinator.class);
        personCoordinator = (PersonCoordinator) ve.getValue(context.getELContext());
        
        return personCoordinator;
    }

    /**
     * @param personCoordinator the personCoordinator to set
     */
    public void setPersonCoordinator(PersonCoordinator personCoordinator) {
        this.personCoordinator = personCoordinator;
    }

    /**
     * @return the propertyCoordinator
     */
    public PropertyCoordinator getPropertyCoordinator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{propertyCoordinator}", PropertyCoordinator.class);
        propertyCoordinator = (PropertyCoordinator) ve.getValue(context.getELContext());
        
        return propertyCoordinator;
    }

    /**
     * @param propertyCoordinator the propertyCoordinator to set
     */
    public void setPropertyCoordinator(PropertyCoordinator propertyCoordinator) {
        this.propertyCoordinator = propertyCoordinator;
    }

    /**
     * @return the occupancyCoordiator
     */
    public OccupancyCoordinator getOccupancyCoordinator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{occupancyCoordinator}", OccupancyCoordinator.class);
        occupancyCoordinator = (OccupancyCoordinator) ve.getValue(context.getELContext());
        return occupancyCoordinator;
    }

    /**
     * @param occupancyCoordiator the occupancyCoordinator to set
     */
    public void setOccupancyCoordinator(OccupancyCoordinator occupancyCoordiator) {
        this.occupancyCoordinator = occupancyCoordiator;
    }

    /**
     * @return the occupancyCoordinator
     */
    public OccInspectionCoordinator getOccInspectionCoordinator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{occInspectionCoordinator}", OccInspectionCoordinator.class);
        occInspectionCoordinator = (OccInspectionCoordinator) ve.getValue(context.getELContext());
        return occInspectionCoordinator;
    }

    /**
     * @param occInspectionCoordinator the occInspectionCoordinator to set
     */
    public void setOccInspectionCoordinator(OccInspectionCoordinator occInspectionCoordinator) {
        this.occInspectionCoordinator = occInspectionCoordinator;
    }

    /**
     * @return the systemCoordinator
     */
    public SystemCoordinator getSystemCoordinator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{systemCoordinator}", SystemCoordinator.class);
        systemCoordinator = (SystemCoordinator) ve.getValue(context.getELContext());
        return systemCoordinator;
    }

    /**
     * @param systemCoordinator the systemCoordinator to set
     */
    public void setSystemCoordinator(SystemCoordinator systemCoordinator) {
        this.systemCoordinator = systemCoordinator;
    }
    
    
    

    /**
     * Special getter for the TaskCoordinator in application scope
     * @return 
     */
    public TaskCoordinator getTaskCoordinator(){
        TaskCoordinator tc;
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{taskCoordinator}", TaskCoordinator.class);
        tc = (TaskCoordinator) ve.getValue(context.getELContext());
        return tc;
    }
    
    /**
     * Special getter for the TaskIntegrator in the application scope
     * @return 
     */
    public TaskIntegrator getTaskIntegrator(){
        TaskIntegrator ti;
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{taskIntegrator}", TaskIntegrator.class);
        ti = (TaskIntegrator) ve.getValue(context.getELContext());
        return ti;
    }
    

    /**
     * Special getter for the workflow coordinator in application scope
     * @return 
     */
    public WorkflowCoordinator getWorkflowCoordinator(){
        WorkflowCoordinator cc;
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{workflowCoordinator}", WorkflowCoordinator.class);
        cc = (WorkflowCoordinator) ve.getValue(context.getELContext());
        return cc;
    }
    
    /**
     * Special getter for the WorkflowIntegrator in the application scope
     * @return 
     */
    public WorkflowIntegrator getWorkflowIntegrator(){
        WorkflowIntegrator ci;
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{workflowIntegrator}", WorkflowIntegrator.class);
        ci = (WorkflowIntegrator) ve.getValue(context.getELContext());
        return ci;
    }
    /**
     * @return the blobCoordinator
     */
    public BlobCoordinator getBlobCoordinator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{blobCoordinator}", BlobCoordinator.class);
        blobCoordinator = (BlobCoordinator) ve.getValue(context.getELContext());
        return blobCoordinator;
    }

    /**
     * @param blobCoordinator the blobCoordinator to set
     */
    public void setBlobCoordinator(BlobCoordinator blobCoordinator) {
        this.blobCoordinator = blobCoordinator;
    }

    /**
     * @return the blobIntegrator
     */
    public BlobIntegrator getBlobIntegrator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{blobIntegrator}", BlobIntegrator.class);
        blobIntegrator = (BlobIntegrator) ve.getValue(context.getELContext());
        return blobIntegrator;
    }

    /**
     * @param blobIntegrator the blobIntegrator to set
     */
    public void setBlobIntegrator(BlobIntegrator blobIntegrator) {
        this.blobIntegrator = blobIntegrator;
    }

    /**
     * @return the pdfCoordinator
     */
    public PermissionsCoordinator getPermissionsCoordinator() {
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{permissionsCoordinator}", PermissionsCoordinator.class);
        permissionsCoordinator = (PermissionsCoordinator) ve.getValue(context.getELContext());
        return permissionsCoordinator;
    }

   
    /**
     * @return the muniCoordinator
     */
    public MunicipalityCoordinator getMuniCoordinator() {
        
        FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{muniCoordinator}", MunicipalityCoordinator.class);
        muniCoordinator = (MunicipalityCoordinator) ve.getValue(context.getELContext());
        return muniCoordinator;
    }

    /**
     * @param muniCoordinator the muniCoordinator to set
     */
    public void setMuniCoordinator(MunicipalityCoordinator muniCoordinator) {
        this.muniCoordinator = muniCoordinator;
    }

    /**
     * @return the occChecklistIntegrator
     */
    public OccChecklistIntegrator getOccChecklistIntegrator() {
           FacesContext context = getFacesContext();
        ValueExpression ve = context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{occChecklistIntegrator}", OccChecklistIntegrator.class);
        occChecklistIntegrator = (OccChecklistIntegrator) ve.getValue(context.getELContext());
        
        
        return occChecklistIntegrator;
    }

    /**
     * @param occChecklistIntegrator the occChecklistIntegrator to set
     */
    public void setOccChecklistIntegrator(OccChecklistIntegrator occChecklistIntegrator) {
        this.occChecklistIntegrator = occChecklistIntegrator;
    }

       


}