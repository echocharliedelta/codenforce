/*
 * Copyright (C) 2018 Turtle Creek Valley
 * Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.*;
import com.tcvcog.tcvce.domain.*;
import com.tcvcog.tcvce.entities.*;
import com.tcvcog.tcvce.util.*;
import com.tcvcog.tcvce.util.viewoptions.ViewOptionsActiveListsEnum;
import java.io.Serializable;
import java.util.*;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Premier backing bean for Notices of Violation, which are now called 
 * generically "Letters"
 * @author ellen bascomb of apt 31y
 */
public class NoticeOfViolationBB extends BackingBeanUtils implements Serializable {

    
    static final String VIOLATION_INJECTION_MARKER = "***VIOLATIONS***";
    
    
    private CECaseDataHeavy currentCaseForNOVs;

    private NoticeOfViolation currentNotice;
    private List<CodeViolation> activeVList;
    private CodeViolation currentViolation;

    private boolean permissionAllowNOVFinalize;
    private boolean permissionAllowNOVDraftEdit;
    
    private Person noticePerson;
    private String formNoteText;

    final static String NOV_TYPE_EVCAT_PARAM = "novtype-eventcat-field";
    private String currentNovTypeEventCatField;
    
    private boolean editModeTemplate;
    private NoticeOfViolationTemplate currentTemplate;
    private NoticeOfViolationTemplate formTemplateForUpdate;
    private List<NoticeOfViolationTemplate> templateList;
    private boolean showTemplatesAllMuni;

    // NOV type manager
    // as of OCTOBER 2023, types are no longer muni-specific
    private boolean editModeNOVType;
    private NoticeOfViolationType currentNOVType;
    private List<NoticeOfViolationType> novTypeList;
    private List<PrintStyle> printStyleList;
    
    private List<EventType> eventTypeCandidateList;
    private List<EventCategory> eventCatCandidateList;
    private EventType eventTypeSelected;
    
    // NOV MAIN
    private boolean editModeNOV;
    
    private List<BlobLight> novHeaderImageCandidateList;
    
    private List<HumanLink> personCandidateList;
    private List<Person> recipientPersonCandidateList;

    
    private Person selectedRecipientPerson;
    private MailingAddress selectedRecipAddr;
    
    private User notifyingOfficerCandidateChosen;

    private boolean personLookupUseID;
    private Person retrievedManualLookupPerson;
    private int recipientPersonID;

    private List<ViewOptionsActiveListsEnum> viewOptionList;
    private ViewOptionsActiveListsEnum selectedViewOption;
    
    private boolean nov_createNoticeFollowupEvent;
    private LocalDateTime followUpDate;
    
    /**
     * Creates a new instance of NoticeOfViolationBB
     */
    public NoticeOfViolationBB() {

    }

    /**
     * Sets up bean members based on the session's current CECase
     */
    @PostConstruct
    public void initBean() {
        CaseCoordinator cc = getCaseCoordinator();
        PropertyCoordinator pc = getPropertyCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        EventCoordinator evc = getEventCoordinator();
        

        try {
            if(getSessionBean().getSessNotice() != null){
                registerCurrentNotice(getSessionBean().getSessNotice());
            }
            

            // Turned off 23FEB 2023 to reduce load time
            // 2OCT 2023: we need this to work
            currentCaseForNOVs = getSessionBean().getSessCECase();

            recipientPersonCandidateList = new ArrayList<>();
            showTemplatesAllMuni = false;
            refreshNOVTypeList();
            refreshTemplateList();
            eventCatCandidateList = evc.getEventCategeryList(EventType.Notice);
            printStyleList = sc.getPrintStyleList();
        } catch (IntegrationException  | BObStatusException | BlobException  ex) {
            System.out.println(ex);
        } 
        nov_createNoticeFollowupEvent = true;
        configureNOVPermissions();
        
    } // close initbean
    
    /**
     * Sets boolean flags for UI based on permissions business logic
     * outcomes from the CaseCoordinator
     */
    private void configureNOVPermissions(){
        CaseCoordinator cc = getCaseCoordinator();
        permissionAllowNOVFinalize = cc.permissionsCheckpointNOVFinalize(getSessionBean().getSessUser());
        permissionAllowNOVDraftEdit = cc.permissionsCheckpointNOVCreateEdit(getSessionBean().getSessUser());
        
    }
    
    /**************************************************************************
     * **************       NOV MAIN        ***************************
     *************************************************************************/
    
    private void registerCurrentNotice(NoticeOfViolation nov) throws BObStatusException{
        if(nov == null){
            System.out.println("NoticeOfViolationBB.registerCurrentNotice | Incoming Null NOV for registration");
            throw new BObStatusException("Cannot registre null NOV");
        }
        currentNotice = nov;
        getSessionBean().setSessNotice(currentNotice);
        System.out.println("");
        System.out.println("NoticeOfViolationBB.registerCurrentNotice | NoticeID: " + currentNotice.getNoticeID());
        
    }
    
    /**
     * Asks coordinator for new case data
     * BUT note that this does not refresh the CECaseBB's current case
     */
    private void refreshCurrentCase(){
        CaseCoordinator cc = getCaseCoordinator();
         try {
            currentCaseForNOVs = cc.cecase_assembleCECaseDataHeavy(
                    cc.cecase_getCECase(getSessionBean().getSessCECase().getCaseID(), 
                            getSessionBean().getSessUser()), 
                    getSessionBean().getSessUser());
            getSessionEventConductor().setSessEventListForRefreshUptake(currentCaseForNOVs.getEventList());
            
        } catch (BObStatusException | IntegrationException | SearchException ex) {
            System.out.println(ex);
        }
        
    }
    
    /**
     * fetches a fresh NOV from the DB
     */
    private void refreshCurrentNotice(){
        CaseCoordinator cc = getCaseCoordinator();
        if(currentNotice != null){
            try {
                registerCurrentNotice(cc.nov_getNoticeOfViolation(currentNotice.getNoticeID()));
            } catch (IntegrationException | BObStatusException | BlobException ex) {
                System.out.println(ex);
            } 
        }
    }
    
    
    /**
     * Listener to start the event cat choice process for NOV type events
     * @param ev 
     */
    public void onChooseEventCatInit(ActionEvent ev){
        EventCoordinator ec = getEventCoordinator();
        currentNovTypeEventCatField = getFacesContext().getExternalContext().getRequestParameterMap().get(NOV_TYPE_EVCAT_PARAM);
        
        eventTypeCandidateList = ec.getEventTypesAll();
         onEventTypeListChange();
    }
    
    /**
     * listener for user changes of the event type drop down
     * @param et 
     */
    public void onEventTypeListChange(){
        EventCoordinator ec = getEventCoordinator();
        if(eventTypeCandidateList != null && !eventTypeCandidateList.isEmpty()){
            try {
                if(eventTypeSelected == null){
                    eventTypeSelected = eventTypeCandidateList.get(0);
                } 
                eventCatCandidateList = ec.getEventCategeryList(eventTypeSelected);
                
                if(eventCatCandidateList != null && !eventCatCandidateList.isEmpty()){
                    eventCatCandidateList = ec.assembleEventCategoryListActiveOnly(eventCatCandidateList);
                    System.out.println("NoticeOfViolationBB.onEventTypeListChange | Candidate cat list size: " + eventCatCandidateList.size());
                }
            } catch (IntegrationException ex) {
                System.out.println(ex);
            }
        }
    }
    
    /**
     * listener to finalize teh event cat choice process for NOV type events
     * @param evcat 
     */
    public void onChooseEventCatCommit(EventCategory evcat){
        System.out.println("NoticeOfViolationBB.chooseEventCatCommit | passed in cat id: " + evcat.getCategoryID() );
        if(currentNovTypeEventCatField != null && currentNOVType != null){
            switch(currentNovTypeEventCatField){
                
                case "sent":
                    currentNOVType.setEventCatSent(evcat);
                    break;
                case "followup":
                    currentNOVType.setEventCatFollowUp(evcat);
                    break;
                case "returned":
                    currentNOVType.setEventCatReturned(evcat);
                    break;
                default:
                    System.out.println("Unrecognized event cat field on NOV type");
            }
        } else {
            System.out.println("Error on NOV event cat type setup");
        }
    }
    
    /**
     * Primary listener method which copies a reference to the selected user
     * from the list and sets it on the selected user perch
     *
     * @param nov
     */
    public void onObjectViewButtonChange(NoticeOfViolation nov) {
        CaseCoordinator cc = getCaseCoordinator();
        if (nov != null) {
            try {
                nov = cc.nov_getNoticeOfViolation(nov.getNoticeID());
                registerCurrentNotice(nov);
            } catch (IntegrationException | BObStatusException | BlobException ex) {
                System.out.println(ex);
            } 
        }
    }

    /**
     * Listener for user requests to start the sending dialog 
     * @param nov 
     */
    public void markNoticeOfViolationAsSentInit(NoticeOfViolation nov){
        try {
            registerCurrentNotice(nov);
            currentNotice.setFollowupEventDaysRequest(currentNotice.getTemplate().getTemplateType().getFollowUpWindowDays());
            getSessionBean().setSessNotice(currentNotice);
            System.out.println("NoticeOfViolationBB.markNoticeOfViolationAsSentInit: NOV " + currentNotice.getNoticeID());
            onChangeFollowUpWindowValue();
        } catch (BObStatusException ex) {
            System.out.println(ex);
        }
    }
    
    /**
     * Listener for user changes to default follow-up window
     */
    public void onChangeFollowUpWindowValue(){
        if(currentNotice != null){
            followUpDate = LocalDateTime.now().plusDays(currentNotice.getFollowupEventDaysRequest());
        }
    }
    
    /**
     * Listener for finalization of NOV sending
     * @param ev
     */
    public void markNoticeOfViolationAsSent(ActionEvent ev) {
        CaseCoordinator caseCoord = getCaseCoordinator();
        try {
            registerCurrentNotice(getSessionBean().getSessNotice());
            caseCoord.nov_markAsSentAndLogSentAndFollowupEvents(currentCaseForNOVs, currentNotice, getSessionBean().getSessUser());
            refreshCurrentCase();
        } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        } catch (EventException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to generate case event to log phase change",
                            "Note that because this message is being displayed, the phase change"
                            + "has probably succeeded"));
        }
    }
    
    /***
     * Listener for user requests to start follow-up event creation
     * @param ev 
     */
    public void onNOVFollowupEventCreateButtonPush(ActionEvent ev){
        if(currentNotice != null){
            currentNotice.setFollowupEventDaysRequest(recipientPersonID);
        }
    }
    
   
    /**
     * Listener to look at a single NOV
     * @param nov 
     */
    public void onNoticeDetailsButtonChange(NoticeOfViolation nov){
        refreshCurrentCase();
        try {
            registerCurrentNotice(nov);
        } catch (BObStatusException ex) {
            System.out.println(ex);
        }
        
    }

     public String resetNotice() {
        CaseCoordinator cc = getCaseCoordinator();
        try {
            cc.nov_ResetMailing(getCurrentNotice(), currentCaseForNOVs, getSessionBean().getSessUser());
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Notice mailing status has been reset", ""));
            refreshCurrentCase();
            return "ceCaseProfile";
            
        } catch (IntegrationException | AuthorizationException  | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        return "";
    }
    
     /**
      * Listener to mark notice as returned in the mail
      * @param ev 
      */
    public void markNoticeOfViolationAsReturned(ActionEvent ev) {
        CaseCoordinator caseCoord = getCaseCoordinator();
        try {
            caseCoord.nov_markAsReturned(currentCaseForNOVs, currentNotice, getSessionBean().getSessUser());
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Notice no. " + getCurrentNotice().getNoticeID()
                            + " has been marked as returned on today's date", ""));
            refreshCurrentCase();
        } catch (IntegrationException | BObStatusException  ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            
        }
        
    }
    
    /**
     * Listener for NOV deac
     * @return 
     */
    public String deactivateNoticeOfViolation() {
        CaseCoordinator caseCoord = getCaseCoordinator();
        try {
            caseCoord.nov_deactivate(currentNotice, currentCaseForNOVs, getSessionBean().getSessUser());
            refreshCurrentCase();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Notice no. " + getCurrentNotice().getNoticeID() + " has been nuked forever", ""));
            return "ceCaseProfile";
        } catch (BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        } 
        return "";
    }
    
    /**
     * Official pathway for locking a notice and thereby readying it for printing
     * @param nov 
     */
    public void lockNoticeAndQueueForMailing(NoticeOfViolation nov) {
        CaseCoordinator caseCoord = getCaseCoordinator();
        PersonCoordinator pc = getPersonCoordinator();
        try {
            caseCoord.nov_LockAndQueue(currentCaseForNOVs, nov, getSessionBean().getSessUser());
            pc.linkNOVHumanToParentCECase(currentCaseForNOVs, currentNotice, getSessionBean().getSessUser());
            
        } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        } catch (EventException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "The automatic event generation associated with this action has thrown an error. "
                            + "Please create an event manually which logs this letter being queued for mailing", ""));

        } catch (ViolationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Unable to queue notice of violatio. "
                            + "Please create an event manually which logs this letter being queued for mailing", ""));
        }
    }
    
    /**
     * Listener for user requests to update the NOV's notifying officer
     * @param usr 
     */
    public void changeNotifyingOfficer(){
        currentNotice.setNotifyingOfficer(notifyingOfficerCandidateChosen);
        
    }

    /**
     * Internal logic container for beginning the user creation change process
     * Delegated from the mode button router
     */
    public void onModeInsertInit() {
        CaseCoordinator cc = getCaseCoordinator();
        NoticeOfViolation nov;
        refreshCurrentCase();
        try {
            nov = cc.nov_GetNewNOVSkeleton(currentCaseForNOVs, getSessionBean().getSessUser());
            nov.setCreationBy(getSessionBean().getSessUser());
            registerCurrentNotice(nov);
            getSessionBean().setSessNotice(currentNotice);
        } catch (AuthorizationException | BObStatusException  ex) {
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Database error", ""));
        }
        if (!currentCaseForNOVs.getViolationListUnresolved().isEmpty()) {
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Skeleton notice created", ""));

        } else {
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "No unresolved violations exist for building a letter.", ""));
        }
    }

    /**
     * Listener for users to edit an existing but unsent letter
     * 
     * @param nov
     */
    public void onNOVEditTextInitButtonChange(NoticeOfViolation nov) {
        System.out.println("NoticeOfViolationBB.onNOVEditInitButtonChange");
        try {
            registerCurrentNotice(nov);
        } catch (BObStatusException ex) {
            System.out.println(ex);
        }
        editModeNOV  = true;
    }
   
    
    /**
    * Listener for user requests to update an existing notice
    * @param ev
    */
    public void onUpdateNoticeButtonChange() {
        System.out.println("NoticeOfViolationBB.onUpdateNoticeButtonChange");
        CaseCoordinator cc = getCaseCoordinator();
        try {
            if(currentNotice != null){
                cc.nov_update(currentNotice);
                registerCurrentNotice(cc.nov_getNoticeOfViolation(currentNotice.getNoticeID()));
                refreshCurrentNotice();
                editModeNOV = false;
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Notice udated", ""));
            }

        } catch (IntegrationException | BObStatusException | BlobException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
    } // close method
    
    
    /**
     * Listener to end the NOV editing session
     * @param ev 
     */
    public void onNOVEditAbortButtonChange(ActionEvent ev){
        System.out.println("NoticeOfViolationBB.onNOVEditAbortButtonChange");
        editModeNOV = false;
    }

    /**
     * Listener to begin the notice process
     * @param ev 
     */
    public void onStartNewNoticeButtonChange(ActionEvent ev){
        onModeInsertInit();
        try {
            editModeNOV = false;
            refreshTemplateList();
            getFacesContext().addMessage(null,
                   new FacesMessage(FacesMessage.SEVERITY_INFO,
                           "Building a new notice; Next: Apply template", ""));
        } catch (IntegrationException | BlobException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                   new FacesMessage(FacesMessage.SEVERITY_ERROR,
                           "Unable to build new notice", ""));
        }
    }
    
    /**
     * Listener for user requests to bring up the choose person dialog
     *
     * @param ev
     */
    public void onChoosePersonInitButtonChange(ActionEvent ev) {
        prepareRecipientPersonList();
    }

    /**
     * Converts the session property's human link list to a person list with addresses
     */
    private void prepareRecipientPersonList(){
        PersonCoordinator pc = getPersonCoordinator();
        try {
            recipientPersonCandidateList = pc.getPersonListFromHumanLinkList(pc.getHumanLinkList(getSessionBean().getSessProperty()));
            recipientPersonCandidateList.addAll(pc.getPersonListFromHumanLinkList(pc.getHumanLinkList(getSessionBean().getSessCECase())));
            System.out.println("NoticeOfViolationBB.prepareRecipientPersonList | recpient cadidate list size: " + recipientPersonCandidateList.size());
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        } 
    }

    /**
     * Listener for user requests to build NOV with template
     * @param plate 
     */
    public void onBuildNOVUsingTemplateBlock(NoticeOfViolationTemplate plate){
        CaseCoordinator cc = getCaseCoordinator();
        currentTemplate = plate;
        try {
            if(currentNotice != null){
                registerCurrentNotice(cc.nov_assembleNOVFromTemplate(currentNotice, currentTemplate, currentCaseForNOVs));
            }
            prepareRecipientPersonList();
            getFacesContext().addMessage(null,
               new FacesMessage(FacesMessage.SEVERITY_INFO,
                       "Assembled block using template", ""));
        } catch (BObStatusException ex) {
            getFacesContext().addMessage(null,
               new FacesMessage(FacesMessage.SEVERITY_ERROR,
                       "Unable to build notice from template.", ""));
            System.out.println(ex);
        }
    }
    
    /**
     * Listener for user requests to view template
     * @param plate 
     */
    public void onTemplateViewButtonChange(NoticeOfViolationTemplate plate){
        currentTemplate = plate;
        System.out.println("NoticeOfViolationBB.onTemplateViewButtonChange | block ID: " + currentTemplate.getTemplateID());

        
    }

    /**
     * Listener for user requests to remove a violation from the NOV during
     * the creation and config sequence
     * @param viol 
     */
    public void removeViolationFromList(CodeViolationDisplayable viol) {
        if(currentNotice != null){
            
            currentNotice.getViolationList().remove(viol);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Done: violation ID " + viol.getViolationID() + "will not be included in letter.", ""));
        } else {
            
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "ERROR removing violation from NOV; please file a trouble ticket", ""));
        }
    }

  
    
    /**
     * listener for user requests to complete the selection process of address and addressee
     * @param ev 
     */
    public void onCompleteRecipientAndAddressSelection(ActionEvent ev){
        if(currentNotice != null){
            if(selectedRecipientPerson != null){
                currentNotice.setRecipient(selectedRecipientPerson);
            } else {
                getFacesContext().addMessage(null,
                      new FacesMessage(FacesMessage.SEVERITY_ERROR,
                      "Recipient missing! Please select a notice recipient", ""));
            }   
            if(getSelectedRecipAddr() != null){
                currentNotice.setRecipientMailingAddress(selectedRecipAddr);
            } else {
                      getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Mailing Address missing! Please select an address to continue", ""));
            }
            
            getFacesContext().addMessage(null,
                  new FacesMessage(FacesMessage.SEVERITY_INFO,
                  "Recipient and Address chosen!", ""));
        } else {
            getFacesContext().addMessage(null,
                  new FacesMessage(FacesMessage.SEVERITY_ERROR,
                  "Fatal NOV setup fault. sorry!", ""));
        }
    }
      /**
     * Listener for user choice of a recipient person
     * @param pers 
     */
    public void storeRecipient(Person pers) {
        System.out.println("Store Recipient: " + pers);
        selectedRecipientPerson = pers;
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Notice recipient is now Person: " + pers.getLastName(), ""));

    }
    
    
    /**
     * Listener for user requests to store a recipient's mailing address
     * @param ma
     */
    public void storeRecipientAddress(MailingAddress ma){
        if(ma != null){
            System.out.println("NoticeOfViolationBB.storeRecipientAddress | address ID " + ma.getAddressID());
            selectedRecipAddr = ma;
            currentNotice.setRecipientMailingAddress(selectedRecipAddr);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Notice will be sent to " + ma.getAddressPretty1Line(), ""));
        }
        
        
    }

    public void checkNOVRecipient(ActionEvent ev) {
        PersonCoordinator pc = getPersonCoordinator();
        if (recipientPersonID != 0) {
            try {
                recipientPersonCandidateList.add(pc.getPerson(pc.getHuman(recipientPersonID)));
                System.out.println("NoticeOfViolationBB.checkNOVRecipient | looked up person: " + getRetrievedManualLookupPerson());
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Search complete", ""));
            } catch (IntegrationException | BObStatusException  ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_WARN,
                                getRecipientPersonID() + "not mapped to a known person", "Please try again or visit our person search page."));
            }
        }
    }

  
    /**
     * Listener for user requests to create a new notice
     * @param ev
     */
    public void onInsertNewNoticeButtonChange() {
        CaseCoordinator cc = getCaseCoordinator();
        PersonCoordinator pc = getPersonCoordinator();
        try {
            System.out.println("NoticeOfViolationBB.onInsertNewNoticeButtonChange");    
            int frshID = cc.nov_InsertNotice(currentNotice, currentCaseForNOVs, getSessionBean().getSessUser());
            registerCurrentNotice(cc.nov_getNoticeOfViolation(frshID)); 
            
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Notice saved and recipient linked to CECase!", ""));
//            refreshCurrentCase();
        } catch (IntegrationException | BObStatusException  | BlobException   ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            
        }
        
    } // close method
    
    public String printNotice() {
        getSessionBean().setSessNotice(currentNotice);
//        positionCurrentCaseAtHeadOfQueue();
        return "noticeOfViolationPrint";
    }

    /**
     * Listener to print an NOV, called both from the property profile page and
     * the one click case facility
     * @param nov
     * @return 
     */
    public String printNotice(NoticeOfViolation nov) {
        try {
            registerCurrentNotice(nov);
        } catch (BObStatusException ex) {
            System.out.println(ex);
        }
//        positionCurrentCaseAtHeadOfQueue();
        return "noticeOfViolationPrint";
    }
 
    /**
     * Listener for printing an NOV directly from the property profile via the one-click case options
     * @param nov
     * @param cse
     * @return 
     */
    public String printNoticesFromOneClick(NoticeOfViolation nov, CECase cse){
        
        if(nov == null || cse == null){
             getFacesContext().addMessage(null,
                  new FacesMessage(FacesMessage.SEVERITY_ERROR,
                          "Could not print NOV due to call configuration error! Must be fixed by a dev", ""));
        } else {
            // first mark our notice as sent
            markNoticeOfViolationAsSent(null);
            CaseCoordinator cc = getCaseCoordinator();
            try {
                currentCaseForNOVs = cc.cecase_assembleCECaseDataHeavy(cc.cecase_getCECase(cse.getCaseID(), getSessionBean().getSessUser()), getSessionBean().getSessUser());
            } catch (BObStatusException | IntegrationException | SearchException  ex) {
                System.out.println(ex);
            } 
            return printNotice(nov);
        }
        return "";
    }
   
  
    /**
     * Listener for commencement of note writing process
     *
     * @param ev
     */
    public void onNoteInitButtonChange(ActionEvent ev) {
        setFormNoteText(null);
    }

   
     /**
      * Special getter for person links--I check the session bean's
      * PersonLinkList to see if there is a new list to send to the UI
      * 
     * @return the recipientPersonCandidateList
     */
    public List<Person> getRecipientPersonCandidateList() {
        if(getSessionBean().getSessHumanListRefreshedList() != null){
            PersonCoordinator pc = getPersonCoordinator();
            try {
                recipientPersonCandidateList = pc.getPersonListFromHumanLinkList(getSessionBean().getSessHumanListRefreshedList());
                getSessionBean().setSessHumanListRefreshedList(null);
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
            } 
        }
        return recipientPersonCandidateList;
    }
    
    
    
    
    /**************************************************************************
     * **************       NOV/LETTER TYPES        ***************************
     *************************************************************************/
    
    
    /**
     * JSF Dynamic accessor of the injection marker string for use in Injectable templates
     * @return 
     */
    public String getViolationInjectionMarker(){
        return VIOLATION_INJECTION_MARKER;
    }
    /**
     * sets up and refreshes template list based on muni control
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.BlobException
     */
    public void refreshTemplateList() throws IntegrationException, BObStatusException, BlobException{
        CaseCoordinator cc = getCaseCoordinator();
        if(showTemplatesAllMuni){
            templateList = cc.nov_getNoticeOfViolationTemplateList(null);
        } else {
            templateList = cc.nov_getNoticeOfViolationTemplateList(getSessionBean().getSessMuni());
        }
        
    }
    
    /**
     * Gets us a nice new list
     */
    public void refreshNOVTypeList(){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            novTypeList = cc.nov_getNOVTypeList();
        } catch (IntegrationException | BlobException ex) {
            System.out.println(ex);
        } 
    }
    /**
     * listener for type change
     * @param ev 
     */
    public void onNOVTypeChangeInit(ActionEvent ev){
        System.out.println("NoticeOfViolationBB.onNOVTypeChangeInit");
        try {
            refreshTemplateList();
        } catch (IntegrationException | BlobException | BObStatusException ex) {
            System.out.println(ex);
        } 
    }
    
    /**
     * Listener for type change commit requests
     * @param ev 
     */
    public void onNOVTypeChangeCommit(ActionEvent ev){
        CaseCoordinator cc = getCaseCoordinator();
        if(currentNotice != null){
            currentNotice.setTemplate(formTemplateForUpdate);
            try {
                cc.nov_update(currentNotice);
                registerCurrentNotice(cc.nov_getNoticeOfViolation(currentNotice.getNoticeID()));
                 getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Updated letter type: Success!", ""));
            } catch (IntegrationException | BObStatusException | BlobException ex) {
                System.out.println(ex);
                 getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Could not update this letter's type due to a fatal error.", ""));
            }
        }
    }
  
    
    /**
     * Listener to start the NOV type manage process
     * @param ev 
     */
    public void onNOVTypeManageButtonClick(ActionEvent ev){
        System.out.println("NoticeOfViolationBB.onNOVTypeManageButtonClick");
        refreshCurrentNOVTypeAndList();
        PropertyCoordinator pc = getPropertyCoordinator();
        BlobCoordinator bc = getBlobCoordinator();
        try {
            novHeaderImageCandidateList = bc.getBlobLightList(getSessionBean().getSessMuni());
        } catch (BObStatusException | BlobException | IntegrationException  ex) {
            System.out.println(ex);
        } 
        
    }
    
    /**
     * Gets our total NOV type list showing all munis
    */
    private void refreshCurrentNOVTypeAndList(){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            if(currentNOVType != null && currentNOVType.getTypeID() != 0){
                currentNOVType = cc.nov_getNOVType(currentNOVType.getTypeID());
            }
            refreshTemplateList();
            
            System.out.println("NoticeOfViolationBB.refreshCurrentNOVTypeAndList");
        } catch (IntegrationException | BlobException | BObStatusException ex) {
            System.out.println(ex);
        }
        
    }
    
    /**
     * Listener for user requests to view an NOVType
     * @param novt 
     */
    public void onNOVTypeView(NoticeOfViolationType novt){
        MunicipalityCoordinator mc = getMuniCoordinator();
        currentNOVType = novt;
        editModeNOVType = false;
    }
    
    
    /**
     * Toggles the NOV record edit mode
     * @param ev 
     */
    public void onToggleEditModeNOVType(ActionEvent ev){
        System.out.println("NoticeOfViolationBB.onToggleEditModeNOVType | incoming edit mode: " + editModeNOVType);
        if(editModeNOVType){
            if(currentNOVType != null){
                if(currentNOVType.getTypeID() == 0){
                    insertCommitNOVType();
                } else {
                    updateCurrentNOVType();
                }
            }
            refreshNOVTypeList();
            refreshCurrentNOVTypeAndList();
        } else {
            // do nothing
        }
        editModeNOVType = !editModeNOVType;
    }
    
    /**
     * Starts the NOV type creation process
     * @param ev 
     */
    public void onNOVTypeAddInitButtonChange(ActionEvent ev){
        CaseCoordinator cc = getCaseCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        currentNOVType = cc.nov_getNoticeOfViolationTypeSkeleton();
        editModeNOVType = true; 
        
    }
    
    /**
     * Internal update method for NOV types
     */
    private void updateCurrentNOVType(){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            cc.nov_updateNOVType(currentNOVType);
            refreshCurrentNOVTypeAndList();
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Could not update a letter type with ID: " + currentNOVType.getTypeID(), ""));
        } 
    }
    
    
    /**
     * Internal insertion method for NOV types
     */
    private void insertCommitNOVType(){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            int freshid = cc.nov_insertNOVType(currentNOVType);
            currentNOVType.setTypeID(freshid);
            getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Successfully added a new letter type with ID: " + currentNOVType.getTypeID(), ""));
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Could not insert a new letter type!", ""));
        } 
    }
    
    /**
     * Listener for user requests to start the NOV type deac process
     * @param novt 
     */
    public void onDeactivateNOVTypeInit(NoticeOfViolationType novt){
        currentNOVType = novt;
    }
    
    /**
     * Listener for user requests to complete the nov type deac process
     * @param ev 
     */
    public void onDeactivateNOVTypeCommit(ActionEvent ev){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            cc.nov_deactivateNOVType(currentNOVType);
            refreshCurrentNOVTypeAndList();
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Successfully deactivated a letter type with ID: " + currentNOVType.getTypeID(), ""));
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Could not deactivated letter type with ID: " + currentNOVType.getTypeID(), ""));
        } 
    }
    
    /**
     * Listener for user requests to stop editing an NOV type
     * @param ev 
     */
    public void onNOVTypeEditAbort(ActionEvent ev){
        editModeNOVType = false;
    }
    
    /**
     * Listener for use requests to start the header image select process
     * @param ev 
     */
    public void onNOVTypeHeaderImageSelectInit(ActionEvent ev){
        System.out.println("Header Image select start");
    }
    
    /**
     * Listener for user requests to select a blob for use as a header image
     * on an NOV
     * @param bl 
     */
    public void onNOVTypeHeaderImageSelectBlobLinkClick(BlobLight bl){
        if(currentNOVType != null){
            currentNOVType.setNovHeaderBlob(bl);
            
        }
    }
  
    
    /**
     * ************************  TEMPLATES  ************************
     */
    
    
      
    /**
     * Listener for user requests to start the NOV template creation process
     * @param ev 
     */
    public void onTemplateCreateInit(ActionEvent ev){
        CaseCoordinator cc = getCaseCoordinator();
        currentTemplate = cc.nov_getNoticeOfViolationSkeleton();
        editModeTemplate = true;
    }
    
    /**
     * Listener to start the template editor dialog
     * @param ev 
     */
    public void onTemplateManageInit(ActionEvent ev){
        try {
            refreshTemplateList();
        } catch (IntegrationException | BObStatusException | BlobException ex) {
            System.out.println(ex);
        } 
        
    }
    
    /**
     * Listener for user requests to update template
     * @param plate
     */
    public void onTemplateEditButtonChange(NoticeOfViolationTemplate plate){
        currentTemplate = plate;
        editModeTemplate = true;
    }
    
    
    /**
     * Listener to edit the raw HTML of a template
     * @param ev 
     */
    public void onTemplateEditRawTextInit(ActionEvent ev){
        editModeTemplate = true;
    }
    
    
    /**
     * listener for toggles of the template editor
     * @param ev 
     */
    public void toggleTemplateEditMode(ActionEvent ev){
        CaseCoordinator cc = getCaseCoordinator();
        if(editModeTemplate){
            try {
                if(currentTemplate != null){
                    if(currentTemplate.getTemplateID() == 0){
                        int freshID = cc.nov_insertNoticeOfViolationTemplate(currentTemplate, getSessionBean().getSessUser());
                        currentTemplate.setTemplateID(freshID);
                        getFacesContext().addMessage(null,
                           new FacesMessage(FacesMessage.SEVERITY_INFO,
                                   "Success! New template created.", ""));
                    } else {
                        cc.nov_updateNoticeOfViolationTemplate(currentTemplate, getSessionBean().getSessUser());
                        getFacesContext().addMessage(null,
                           new FacesMessage(FacesMessage.SEVERITY_INFO,
                                   "Success! Template updated.", ""));
                    }
                    refreshCurrentTemplate();
                    refreshTemplateList();
                }
            } catch (BObStatusException | IntegrationException | BlobException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                   new FacesMessage(FacesMessage.SEVERITY_ERROR,
                           ex.getMessage(), ""));
            } 
        }
        editModeTemplate = !editModeTemplate;
        
    }
    
    /**
     * Gets a new copy of our current template
     */
    private void refreshCurrentTemplate() throws BObStatusException, IntegrationException, BlobException{
        CaseCoordinator cc = getCaseCoordinator();
        if(currentTemplate != null && currentTemplate.getTemplateID() != 0){
            currentTemplate = cc.nov_getNoticeOfViolationTemplate(currentTemplate.getTemplateID());
        }
    }
    
    /**
     * Listener for user requests to abort template edit
     * @param ev 
     */
    public void onTemplateEditAbort(ActionEvent ev){
        System.out.println("NoticeOfViolationBB.onTemplateEditAbort");
        editModeTemplate = false;
    }
    
    /**
     * Listener to start the deac process
     * @param plate 
     */
    public void onTemplateDeacInit(NoticeOfViolationTemplate plate){
        System.out.println("NoticeOfViolationBB.onTemplateDeacInit");
        currentTemplate = plate;
    }
    
    /**
     * Finalizes the template deac process
     * @param ev 
     */
    public void onTemplateDeacCommit(ActionEvent ev){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            cc.nov_onTemplateDeac(currentTemplate, getSessionBean().getSessUser());
            refreshTemplateList();
            refreshCurrentTemplate();
            getFacesContext().addMessage(null,
                   new FacesMessage(FacesMessage.SEVERITY_INFO,
                           "Successfully deactivated template", ""));
        } catch (BObStatusException | IntegrationException | BlobException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                   new FacesMessage(FacesMessage.SEVERITY_ERROR,
                           ex.getMessage(), ""));
        } 
    }
    
    /**
     * ------------------------ END TEMPLATES -----------------------
     */
    
    // GETTERS AND SETTERS
    
    /**
     * @return the currentNotice
     */
    public NoticeOfViolation getCurrentNotice() {
        return currentNotice;
    }

    
    /**
     * @return the activeVList
     */
    public List<CodeViolation> getActiveVList() {
        if (activeVList == null) {
            activeVList = getSessionBean().getSessViolationList();
        }
        return activeVList;
    }

    /**
     * @param activeVList the activeVList to set
     */
    public void setActiveVList(ArrayList<CodeViolation> activeVList) {
        this.activeVList = activeVList;
    }

    /**
     * @return the personCandidateList
     */
    public List<HumanLink> getPersonCandidateList() {
        return personCandidateList;
    }

    /**
     * @param personCandidateAL the personCandidateList to set
     */
    public void setPersonCandidateAL(List<HumanLink> personCandidateAL) {
        this.personCandidateList = personCandidateAL;
    }

   
    /**
     * @return the currentCaseForNOVs
     */
    public CECaseDataHeavy getCurrentCaseForNOVs() {
        return currentCaseForNOVs;
    }

    /**
     * @param currentCaseForNOVs the currentCaseForNOVs to set
     */
    public void setCurrentCaseForNOVs(CECaseDataHeavy currentCaseForNOVs) {
        this.currentCaseForNOVs = currentCaseForNOVs;
    }

    /**
     * @return the retrievedManualLookupPerson
     */
    public Person getRetrievedManualLookupPerson() {
        return retrievedManualLookupPerson;
    }

    /**
     * @return the recipientPersonID
     */
    public int getRecipientPersonID() {
        return recipientPersonID;
    }

    /**
     * @param retrievedManualLookupPerson the retrievedManualLookupPerson to set
     */
    public void setRetrievedManualLookupPerson(Person retrievedManualLookupPerson) {
        this.retrievedManualLookupPerson = retrievedManualLookupPerson;
    }

    /**
     * @param recipientPersonID the recipientPersonID to set
     */
    public void setRecipientPersonID(int recipientPersonID) {
        this.recipientPersonID = recipientPersonID;
    }

   

    /**
     * @param recipientPersonCandidateList the recipientPersonCandidateList to set
     */
    public void setRecipientPersonCandidateList(List<Person> recipientPersonCandidateList) {
        this.recipientPersonCandidateList = recipientPersonCandidateList;
    }

    /**
     * @return the showTemplatesAllMuni
     */
    public boolean isShowTemplatesAllMuni() {
        return showTemplatesAllMuni;
    }

    /**
     * @param showTemplatesAllMuni the showTemplatesAllMuni to set
     */
    public void setShowTemplatesAllMuni(boolean showTemplatesAllMuni) {
        this.showTemplatesAllMuni = showTemplatesAllMuni;
    }

    /**
     * @return the noticePerson
     */
    public Person getNoticePerson() {
        return noticePerson;
    }

    /**
     * @param noticePerson the noticePerson to set
     */
    public void setNoticePerson(Person noticePerson) {
        this.noticePerson = noticePerson;
    }

    /**
     * @return the viewOptionList
     */
    public List<ViewOptionsActiveListsEnum> getViewOptionList() {
        return viewOptionList;
    }

    /**
     * @return the selectedViewOption
     */
    public ViewOptionsActiveListsEnum getSelectedViewOption() {
        return selectedViewOption;
    }

    /**
     * @param viewOptionList the viewOptionList to set
     */
    public void setViewOptionList(List<ViewOptionsActiveListsEnum> viewOptionList) {
        this.viewOptionList = viewOptionList;
    }

    /**
     * @param selectedViewOption the selectedViewOption to set
     */
    public void setSelectedViewOption(ViewOptionsActiveListsEnum selectedViewOption) {
        this.selectedViewOption = selectedViewOption;
    }

    /**
     * @return the formNoteText
     */
    public String getFormNoteText() {
        return formNoteText;
    }

    /**
     * @param formNoteText the formNoteText to set
     */
    public void setFormNoteText(String formNoteText) {
        this.formNoteText = formNoteText;
    }


    /**
     * @return the currentViolation
     */
    public CodeViolation getCurrentViolation() {
        return currentViolation;
    }

    /**
     * @param currentViolation the currentViolation to set
     */
    public void setCurrentViolation(CodeViolation currentViolation) {
        this.currentViolation = currentViolation;
    }

    /**
     * @return the personLookupUseID
     */
    public boolean isPersonLookupUseID() {
        return personLookupUseID;
    }

    /**
     * @param personLookupUseID the personLookupUseID to set
     */
    public void setPersonLookupUseID(boolean personLookupUseID) {
        this.personLookupUseID = personLookupUseID;
    }


    /**
     * @return the draftNoticeLoaded
     */
    public boolean isDraftNoticeLoaded() {
        boolean d = false;
        if(currentNotice != null && currentNotice.getNoticeID() == 0){
            d = true;
        }
        return d;
    }

   

    /**
     * @return the nov_createNoticeFollowupEvent
     */
    public boolean isNov_createNoticeFollowupEvent() {
        return nov_createNoticeFollowupEvent;
    }

    /**
     * @param nov_createNoticeFollowupEvent the nov_createNoticeFollowupEvent to set
     */
    public void setNov_createNoticeFollowupEvent(boolean nov_createNoticeFollowupEvent) {
        this.nov_createNoticeFollowupEvent = nov_createNoticeFollowupEvent;
    }

    /**
     * @return the notifyingOfficerCandidateChosen
     */
    public User getNotifyingOfficerCandidateChosen() {
        return notifyingOfficerCandidateChosen;
    }

    /**
     * @param notifyingOfficerCandidateChosen the notifyingOfficerCandidateChosen to set
     */
    public void setNotifyingOfficerCandidateChosen(User notifyingOfficerCandidateChosen) {
        this.notifyingOfficerCandidateChosen = notifyingOfficerCandidateChosen;
    }

   

   

    /**
     * @param sr
     */
    public void setSelectedRecipAddr(MailingAddressLink sr) {
        this.selectedRecipAddr = sr;
    }

    /**
     * @return the selectedRecipientPerson
     */
    public Person getSelectedRecipientPerson() {
        return selectedRecipientPerson;
    }

    /**
     * @param selectedRecipientPerson the selectedRecipientPerson to set
     */
    public void setSelectedRecipientPerson(Person selectedRecipientPerson) {
        this.selectedRecipientPerson = selectedRecipientPerson;
    }

    /**
     * @return the selectedRecipAddr
     */
    public MailingAddress getSelectedRecipAddr() {
        return selectedRecipAddr;
    }

    /**
     * @param selectedRecipAddr the selectedRecipAddr to set
     */
    public void setSelectedRecipAddr(MailingAddress selectedRecipAddr) {
        this.selectedRecipAddr = selectedRecipAddr;
    }

    /**
     * @return the novTypeList
     */
    public List<NoticeOfViolationType> getNovTypeList() {
        return novTypeList;
    }

    /**
     * @param novTypeList the novTypeList to set
     */
    public void setNovTypeList(List<NoticeOfViolationType> novTypeList) {
        this.novTypeList = novTypeList;
    }

    /**
     * @return the currentNOVType
     */
    public NoticeOfViolationType getCurrentNOVType() {
        return currentNOVType;
    }

    /**
     * @param currentNOVType the currentNOVType to set
     */
    public void setCurrentNOVType(NoticeOfViolationType currentNOVType) {
        this.currentNOVType = currentNOVType;
    }

    /**
     * @return the editModeNOVType
     */
    public boolean isEditModeNOVType() {
        return editModeNOVType;
    }

    /**
     * @param editModeNOVType the editModeNOVType to set
     */
    public void setEditModeNOVType(boolean editModeNOVType) {
        this.editModeNOVType = editModeNOVType;
    }

    /**
     * @return the printStyleList
     */
    public List<PrintStyle> getPrintStyleList() {
        return printStyleList;
    }

    /**
     * @param printStyleList the printStyleList to set
     */
    public void setPrintStyleList(List<PrintStyle> printStyleList) {
        this.printStyleList = printStyleList;
    }

    /**
     * @return the eventCatCandidateList
     */
    public List<EventCategory> getEventCatCandidateList() {
        return eventCatCandidateList;
    }

    /**
     * @param eventCatCandidateList the eventCatCandidateList to set
     */
    public void setEventCatCandidateList(List<EventCategory> eventCatCandidateList) {
        this.eventCatCandidateList = eventCatCandidateList;
    }

    /**
     * @return the currentNovTypeEventCatField
     */
    public String getCurrentNovTypeEventCatField() {
        return currentNovTypeEventCatField;
    }

    /**
     * @param currentNovTypeEventCatField the currentNovTypeEventCatField to set
     */
    public void setCurrentNovTypeEventCatField(String currentNovTypeEventCatField) {
        this.currentNovTypeEventCatField = currentNovTypeEventCatField;
    }

    /**
     * @return the eventTypeCandidateList
     */
    public List<EventType> getEventTypeCandidateList() {
        return eventTypeCandidateList;
    }

    /**
     * @param eventTypeCandidateList the eventTypeCandidateList to set
     */
    public void setEventTypeCandidateList(List<EventType> eventTypeCandidateList) {
        this.eventTypeCandidateList = eventTypeCandidateList;
    }

    /**
     * @return the eventTypeSelected
     */
    public EventType getEventTypeSelected() {
        return eventTypeSelected;
    }

    /**
     * @param eventTypeSelected the eventTypeSelected to set
     */
    public void setEventTypeSelected(EventType eventTypeSelected) {
        this.eventTypeSelected = eventTypeSelected;
    }

    /**
     * @return the novHeaderImageCandidateList
     */
    public List<BlobLight> getNovHeaderImageCandidateList() {
        return novHeaderImageCandidateList;
    }

    /**
     * @param novHeaderImageCandidateList the novHeaderImageCandidateList to set
     */
    public void setNovHeaderImageCandidateList(List<BlobLight> novHeaderImageCandidateList) {
        this.novHeaderImageCandidateList = novHeaderImageCandidateList;
    }

   

    /**
     * @return the permissionAllowNOVFinalize
     */
    public boolean isPermissionAllowNOVFinalize() {
        return permissionAllowNOVFinalize;
    }

    /**
     * @return the permissionAllowNOVDraftEdit
     */
    public boolean isPermissionAllowNOVDraftEdit() {
        return permissionAllowNOVDraftEdit;
    }

    

    /**
     * @return the templateList
     */
    public List<NoticeOfViolationTemplate> getTemplateList() {
        return templateList;
    }

    /**
     * @param templateList the templateList to set
     */
    public void setTemplateList(List<NoticeOfViolationTemplate> templateList) {
        this.templateList = templateList;
    }

    /**
     * @return the currentTemplate
     */
    public NoticeOfViolationTemplate getCurrentTemplate() {
        return currentTemplate;
    }

    /**
     * @param currentTemplate the currentTemplate to set
     */
    public void setCurrentTemplate(NoticeOfViolationTemplate currentTemplate) {
        this.currentTemplate = currentTemplate;
    }

    /**
     * @return the formTemplateForUpdate
     */
    public NoticeOfViolationTemplate getFormTemplateForUpdate() {
        return formTemplateForUpdate;
    }

    /**
     * @param formTemplateForUpdate the formTemplateForUpdate to set
     */
    public void setFormTemplateForUpdate(NoticeOfViolationTemplate formTemplateForUpdate) {
        this.formTemplateForUpdate = formTemplateForUpdate;
    }

    /**
     * @return the editModeTemplate
     */
    public boolean isEditModeTemplate() {
        return editModeTemplate;
    }

    /**
     * @param editModeTemplate the editModeTemplate to set
     */
    public void setEditModeTemplate(boolean editModeTemplate) {
        this.editModeTemplate = editModeTemplate;
    }

    /**
     * @return the followUpDate
     */
    public LocalDateTime getFollowUpDate() {
        return followUpDate;
    }

    /**
     * @param followUpDate the followUpDate to set
     */
    public void setFollowUpDate(LocalDateTime followUpDate) {
        this.followUpDate = followUpDate;
    }

    /**
     * @return the editModeNOV
     */
    public boolean isEditModeNOV() {
        return editModeNOV;
    }

    /**
     * @param editModeNOV the editModeNOV to set
     */
    public void setEditModeNOV(boolean editModeNOV) {
        this.editModeNOV = editModeNOV;
    }

}
