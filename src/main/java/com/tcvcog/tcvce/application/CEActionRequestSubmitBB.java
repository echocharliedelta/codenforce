/*
 * Copyright (C) 2017 Technology Rediscovery LLC.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.*;
import com.tcvcog.tcvce.domain.*;
import com.tcvcog.tcvce.entities.*;
import com.tcvcog.tcvce.entities.search.*;
import java.io.Serializable;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.faces.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Premier backing bean for the public facing CEAR submission page flow
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */
public class CEActionRequestSubmitBB 
        extends BackingBeanUtils implements Serializable {

    static final String EMPTY_STRING = "";
    private CEActionRequest currentRequest; //the CEAR the user is filling out
    
    private boolean allowSubmissionAsInternalUser;
  
    //If the user is an employee with restricted access, they can select some 
    //persons using the restricted tools. They will appear in this field.
    private List<Person> personCandidateList;
    private List<PersonType> submittingPersonTypes; //A list of person types that can fill out a CEAR;
    private List<ContactPhoneType> phoneTypeList;
    private List<Property> propList; //TODO: Turn into PublicInfoBundles!

    //Two fields used for searching properties
    private String houseNum;
    private String streetName;
    private List<Municipality> muniList;

    private Municipality selectedMuni;

    private Property selectedProperty; //TODO: Turn into PublicInfoBundle!

    private List<CEActionRequestIssueType> issueTypeList;

    //Used by the interface to prevent users from selecting dates in the future.
    private java.util.Date currentDate;
    
    //Fields used when adding descriptions to blobs.
    private String blobDescription;
    private int descriptionBlobID;

    /**
     * Creates a new instance of ActionRequestBean
     */
    public CEActionRequestSubmitBB() {
    }

    @PostConstruct
    public void initBean() {
        MunicipalityCoordinator mc = getMuniCoordinator();
        PersonCoordinator pc = getPersonCoordinator();
        PropertyCoordinator propc = getPropertyCoordinator();
        UserCoordinator uc = getUserCoordinator();
        System.out.println("CEActionRequestSubmitBB.initBean()");
        if (currentRequest == null) {
            initializeReqAndMuni();
        } 
        try {
            if(getSessionBean().getSessUser() == null){
                getSessionBean().setSessUser(uc.auth_getPublicUserAuthorized());
            }
            // Ask the user coordinator if our current user is the sentinel public user
            if(!uc.determineIfUserIsPublicUser(getSessionBean().getSessUser())){
                allowSubmissionAsInternalUser = true;
            }
            muniList = mc.getMuniListAllowCEAR();
            phoneTypeList = pc.getContactPhoneTypeList();
            submittingPersonTypes = new ArrayList<>();
            selectedMuni = getSessionBean().getSessMuniLight();
            if(currentRequest != null && currentRequest.getParcelKey() != 0){
                selectedProperty = propc.getProperty(currentRequest.getParcelKey());
                System.out.println("CEActionRequestSubmitBB.initBean() | Selected property: " + selectedProperty.getAddress().getAddressPretty1Line());
            }

        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
        }
        

        //Manually add what person types users should choose from
        submittingPersonTypes.add(PersonType.MuniStaff);
        submittingPersonTypes.add(PersonType.Owner);
        submittingPersonTypes.add(PersonType.Tenant);
        submittingPersonTypes.add(PersonType.Manager);
        submittingPersonTypes.add(PersonType.Public);
        submittingPersonTypes.add(PersonType.LawEnforcement);
        submittingPersonTypes.add(PersonType.ElectedOfficial);
    }

    /**
     * Starts new request with the user's chosen muni
     */
    public void initializeReqAndMuni() {
        currentRequest = getSessionBean().getSessCEAR();
        CaseCoordinator cc = getCaseCoordinator();
        User usr = getSessionBean().getSessUser();
        if (usr != null) {
            selectedMuni = getSessionBean().getSessMuni();
        }
        if (currentRequest == null) {
            currentRequest = cc.cear_getInititalizedCEActionRequest();
        }
        //Load the issue types so the user can categorize their problem
            try {
                issueTypeList = cc.cear_getIssueTypes(null, false);
            } catch (IntegrationException ex) {
                System.out.println("Error occured while fetching issue typelist: " + ex);
            }
    }

    /**
     * Return to the previous step
     * @return 
     */
    public String goBack() {
        System.out.println("CEActionRequestSubmitBB.goBack");
        try {
            return getSessionBean().getNavStack().popLastPage();
        } catch (NavigationException ex) {
            System.out.println("CEActionRequestSubmitBB.goBack() | ERROR: " + ex);
            //We must do things a little bit different here to make sure messages are kept after the redirect.
            FacesContext context = getFacesContext();
            context.addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Something went wrong when we tried to direct you back a page!", ""));
            
            context.getExternalContext().getFlash().setKeepMessages(true);
            
            return "";
        }
    }

    /**
     * A user with restricted access can use their account information
     * instead of filling out the request manually.
     * @return 
     */
    public String requestActionAsFacesUser() {
        CaseCoordinator cc = getCaseCoordinator();
        if(currentRequest != null){
            currentRequest.setCreatedBy(getSessionBean().getSessUser());
            currentRequest.setRequestorName(getSessionBean().getSessUser().getHuman().getName());
            currentRequest.setInternalUserSubmitter(true);
            currentRequest.setRequestorPersonType(PersonType.User);
            try {
                getSessionBean().setSessCEAR(cc.cear_getCEARPropertyHeavy(currentRequest));
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
            } 
        }
        return "reviewAndSubmit";
    }
    
    /**
     * Listener for user AJAX requests to toggle not an known address
     */
    public void toggleRequestNotAtKnownAddress(){
        System.out.println("CEActionRequestSubmitBB.toggleRequestNotAtKnownAddress");
    }
    
  
    /**
     * Entry mechanism to the Code Enforcement Action Request creation process:
     * We grab the muni the user selected, set it in the new Action Request and
     * store it in the session bean which we'll access and manipulate over the
     * next few pages and finally submit on the last page. This is a poor
     * person's flow system
     *
     * @return String pointer to the next step in the process: choose property
     */
    public String storeSelectedMuni() {
        CaseCoordinator cc = getCaseCoordinator();
        currentRequest = cc.cear_getInititalizedCEActionRequest();
        if(selectedMuni != null){
            currentRequest.setMuni(selectedMuni);
            getSessionBean().setSessMuniLight(selectedMuni);
            try {
                getSessionBean().setSessCEAR(cc.cear_getCEARPropertyHeavy(currentRequest));
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
            } 
            getSessionBean().getNavStack().pushCurrentPage();
        }
        return "chooseCEARProperty";
    }

    /**
     * Stores the property that requires code enforcement before moving on to
     * the description step.
     * @return 
     */
    public String storePropertyInfo() {
        CaseCoordinator cc = getCaseCoordinator();
        if(currentRequest != null){
            if (selectedProperty == null && !currentRequest.isNotAtKnownAddress()) {
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Please select a property from the list of search results to continue or desribe the alternate location.", ""));
                return "";
            } 
            if(selectedProperty != null && !currentRequest.isNotAtKnownAddress()){
                currentRequest.setParcelKey(selectedProperty.getParcelKey());
            }
            try {
                getSessionBean().setSessCEAR(cc.cear_getCEARPropertyHeavy(currentRequest));
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
            } 
            getSessionBean().getNavStack().pushCurrentPage();
            return "describeConcern";
        } else {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal system error: please call TCVCOG at 412.858.5115.", ""));
            return "";
        }
    }

    /**
     * Save the descriptive fields of the request on the sessionBean and move
     * to the next page.
     * Also validates to make sure the user has filled out all the required fields.
     * @return 
     */
    public String saveConcernDescriptions() {
        if(currentRequest != null){
            if(currentRequest.getIssue() == null){
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Please specify what kind of issue you are reporting", ""));
                return "";
            }
            if(currentRequest.getRequestDescription()== null || currentRequest.getRequestDescription().trim().length() == 0){
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Please give a short description of the issue", ""));
                return "";
            }
            try{
                storeCEARPublicFields();
                
            } catch (IntegrationException |  BObStatusException ex) {
                System.out.println(ex.toString());
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "INTEGRATION ERROR: Unable write request into the database, our apologies!",
                                "Please call your municipal office and report your concern by phone."));
                return "";
        }
            getSessionBean().getNavStack().pushCurrentPage();
            getSessionBean().setSessBlobHolder(currentRequest);
            return "photoUpload";
        } else {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal system error, please call TCVCOG at 412.858.5115", ""));
            return "";
        }
    }

    /**
     * Not only goes to the next page without connecting photos to the CEAR,
     * deletes all blobs the user has uploaded.
     *
     * @return
     */
    public String skipPhotoUpload() {
        getSessionBean().getNavStack().pushCurrentPage();
        return "requestorDetails";
    }
    
    /**
     * Gets a new copy of the current request
     */
    private void refreshCurrentRequest(){
        CaseCoordinator cc = getCaseCoordinator();
        if(currentRequest != null && currentRequest.getRequestID() != 0){
            try {
                currentRequest = cc.cear_getCEActionRequest(currentRequest.getRequestID());
                getSessionBean().setSessCEAR(cc.cear_getCEARPropertyHeavy(currentRequest)); 
            } catch (IntegrationException | BObStatusException | BlobException ex) {
                System.out.println(ex);
            } 
        }
    }

    /**
     * The photos the user uploaded are already saved in the database, so let's 
     * just save the descriptions the user entered in and move on.
     * @return 
     */
    public String savePhotosAndContinue() {     
        getSessionBean().getNavStack().pushCurrentPage();
        refreshCurrentRequest();
        return "requestorDetails";
    }

    /**
     * Going back after uploading photos can cause photos to remain unlinked to
     * an object and slip through the cracks, so we'll save their connections.
     * The user can always delete them later.
     *
     * @return
     */
    public String goBackFromPhotos() {
        try {
            //Just use the uploading method
            savePhotosAndContinue();

            //savePhotosAndContinue() puts another page on the stack, so let's 
            //toss that into the void
            getSessionBean().getNavStack().popLastPage();
            
            return getSessionBean().getNavStack().popLastPage();
        } catch (NavigationException ex) {
            System.out.println("CEActionRequestSubmitBB.goBackFromPhotos() | ERROR: " + ex);
            //We must do things a little bit different here to make sure messages are kept after the redirect.
            FacesContext context = getFacesContext();
            context.addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Something went wrong when we tried to direct you back a page!", ""));
            context.getExternalContext().getFlash().setKeepMessages(true);
            return "";
        }
    }
    
        
    /**
     * Special getter wrapper around the CECase blob list
     * that checks the session for a new blob list
     * that may have been injected by the BlobUtilitiesBB
     * 
     * @return the CECases's updated blob list
     */
    public List<BlobLight> getManagedBlobLightListFromCEAR(){
        System.out.println("CEActionRequestSubmitBB.getManagedBlobLightListFromCEAR ");
        List<BlobLight> sessBlobListForUpdate = getSessionBean().getSessBlobLightListForRefreshUptake();
        if(sessBlobListForUpdate != null 
                && currentRequest != null
                && getSessionBean().getSessBlobLightListHolderForRefreshUptake() != null
                && getSessionBean().getSessBlobLightListHolderForRefreshUptake().getBlobLinkEnum() == BlobLinkEnum.CEACTION_REQUEST
                && getSessionBean().getSessBlobLightListHolderForRefreshUptake().getParentObjectID() == currentRequest.getRequestID()){
            System.out.println("CEActionRequestBB.getManagedBlobLightListFromCEAR| found non-null session blob list for uptake: " + sessBlobListForUpdate.size());
            currentRequest.setBlobList(sessBlobListForUpdate);
            // clear session since we have the new list
            getSessionBean().resetBlobRefreshUptakeFields();
            return currentRequest.getBlobList();
        } else {
            if(currentRequest.getBlobList() != null){
                return currentRequest.getBlobList();
            } else {
                return new ArrayList<>();
            }
        }
    }

  

    /**
     * This method finalizes the CEAR submission process by saving the request
     * in the database as well as all the objects attached to it.
     *
     * @return
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    private void storeCEARPublicFields() throws IntegrationException, BObStatusException  {

        CaseCoordinator cc = getCaseCoordinator();

        int submittedActionRequestID;
      
            System.out.println("CEActionRequestSubmitBB.storeCEARPublicFields");
            if(currentRequest != null){
                if(currentRequest.getRequestID() == 0){
                    submittedActionRequestID = cc.cear_insertCEARFirstStage(currentRequest, getSessionBean().getSessUser());
                    currentRequest.setRequestID(submittedActionRequestID);
                } else {
                    cc.cear_updatePublicFields(currentRequest);
                }
                refreshCurrentRequest();
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Data storage success!", ""));
            }
    }

    /**
     * Destroys all data the user has filled out so far (also deletes files they
     * have uploaded) and returns them to the beginning of the request process
     * so they can start over.
     * Currently not used by the interface, as it could cause confusion.
     * Still, this method is here in case it would like to be used at a later date.
     * @return 
     */
    public String restartRequest() {

        currentRequest = null;
        selectedMuni = null;
        selectedProperty = null;
        getSessionBean().setSessCEAR(null);
        clearNavStack();
        return "systemHome";
    }

    /**
     * A method that clears the navStack of all entries to ensure the user does 
     * not retrace steps that are no longer needed.
     */
    private void clearNavStack() {
        while (getSessionBean().getNavStack().peekLastPage() != null) {
            try {
                getSessionBean().getNavStack().popLastPage();
            } catch (NavigationException ex) {
                //nothing, we just want to clear the stack anyway.
            }
        }
    }

    /**
     * Uses the terms entered by the user to search properties within the muni
     * the user selected.
     * @param ev 
     */
    public void searchForPropertiesSingleMuni(ActionEvent ev) throws BObStatusException {
        SearchCoordinator sc = getSearchCoordinator();
        UserCoordinator uc = getUserCoordinator();

        propList = new ArrayList<>();

        QueryProperty qp = null;

        try {
            qp = sc.initQuery(QueryPropertyEnum.ADDRESS_BLDG_NUM_ONLY, getSessionBean().getSessUser().getKeyCard());

            if (qp != null && !qp.getParamsList().isEmpty()) {
                SearchParamsProperty spp = qp.getPrimaryParams();
                spp.setAddress_bldgNum_ctl(true);
                spp.setAddress_bldgNum_val(houseNum);
                spp.setAddressStreetName_ctl(true);
                spp.setAddressStreetName_val(streetName);
                spp.setMuni_ctl(true);
                spp.setMuni_val(currentRequest.getMuni());
                spp.setLimitResultCount_ctl(true);
                spp.setLimitResultCount_val(20);

                sc.runQuery(qp);

            } else {
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "A fatal error occured during the property search.", ""));
            }

        } catch (SearchException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "A fatal error occured during the property search.", ""));
        }

        if (qp != null) {
            if(!qp.getBOBResultList().isEmpty()){

                propList = qp.getBOBResultList();
                if(propList != null){
                    System.out.println("CEActionRequestSubmitBB.Search | proplist: " + propList.size());
                } else {
                    System.out.println("CEActionRequestSubmitBB.Search | proplist NULL");
                }
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Your search completed with " + getPropList().size() + " results", ""));
            } else {
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "No results! Try using less specific search terms, such as using only the property number and leaving Street Name blank.", ""));
            }
        }

    }
    
    /**
     * listener for user requests to save their requestor details
     * and move on to the review and submit screen
     * @return 
     */
    public String saveRequestorDetails(){
        try {
            if(currentRequest != null){
                CaseCoordinator cc = getCaseCoordinator();
                getSessionBean().setSessCEAR(cc.cear_getCEARPropertyHeavy(currentRequest));
                if(currentRequest.getRequestorName() == null || currentRequest.getRequestorName().equals(EMPTY_STRING)){
                     getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                    "Your name is required to complete this request, even if you prefer your name withheld during investigation.", ""));
                     return "";
                }
                // Must have either a phone or an email to submit
                if((currentRequest.getRequestorEmail() == null || currentRequest.getRequestorEmail().equals(EMPTY_STRING)) 
                        && (currentRequest.getRequestorPhone() == null || currentRequest.getRequestorPhone().equals(EMPTY_STRING))){
                     getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                    "Either a phone number or email address is required to complete your request.", ""));
                }
            }
            storeCEARPublicFields();
        } catch (IntegrationException | BObStatusException ex) {
            getFacesContext().addMessage(null,
                   new FacesMessage(FacesMessage.SEVERITY_ERROR,
                           "Fatal system error: unable to store action request. Please call TCVCOG at 412.858.5115", ""));
            
        } 
        return "reviewAndSubmit";
    }

    /**
     * Listener for user requests to end the CEAR submission process.
     * @return 
     */
    public String onEndCEARFlow(){
        return "cearConfirmationCode";
        
    }
    
    /**
     * Page jump
     * @return to CEAR rquest flow page
     */
    public String editMuni(){
        return "requestCEActionFlow";
       
    }
    
    /**
     * Page jump
     * @return to CEAR rquest flow page
     */
    public String editProperty(){
        return "chooseCEARProperty";
    }
    
    /**
     * Page jump
     * @return to CEAR rquest flow page
     */
    public String editRequestDetails(){
        return "describeConcern";
    }
    
    /**
     * Page jump
     * @return to CEAR rquest flow page
     */
    public String editPhotos(){
        return "photoUpload";
        
    }
    
    /**
     * Page jump
     * @return to CEAR rquest flow page
     */
    public String editRequestorDetails(){
        return "requestorDetails";
        
    }
    
    
    // ********************
    // GETTERS AND SETTERS
    // ********************
    
    /**
     * @return the submittingPersonTypes
     */
    public List<PersonType> getSubmittingPersonTypes() {

        return submittingPersonTypes;
    }

    /**
     * @return the selectedProperty
     */
    public Property getSelectedProperty() {
        return selectedProperty;
    }

    /**
     * @param selectedProperty the selectedProperty to set
     */
    public void setSelectedProperty(Property selectedProperty) {
        this.selectedProperty = selectedProperty;
    }

    /**
     * @return the propList
     */
    public List<Property> getPropList() {
        return propList;
    }

    /**
     * @return the houseNum
     */
    public String getHouseNum() {
        return houseNum;
    }

    /**
     * @param propList the propList to set
     */
    public void setPropList(List<Property> propList) {
        this.propList = propList;
    }

    /**
     * @param houseNum the houseNum to set
     */
    public void setHouseNum(String houseNum) {
        this.houseNum = houseNum;
    }

    /**
     * @return the streetName
     */
    public String getStreetName() {
        return streetName;
    }

    /**
     * @param streetName the streetName to set
     */
    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    /**
     * @return the selectedMuni
     */
    public Municipality getSelectedMuni() {
        return selectedMuni;
    }

    /**
     * @param selectedMuni the selectedMuni to set
     */
    public void setSelectedMuni(Municipality selectedMuni) {
        this.selectedMuni = selectedMuni;
    }

    /**
     * @return the currentDate
     */
    public java.util.Date getCurrentDate() {
        currentDate = java.util.Date.from(java.time.LocalDateTime.now()
                .atZone(ZoneId.systemDefault()).toInstant());
        return currentDate;
    }

    /**
     * @param currentDate the currentDate to set
     */
    public void setCurrentDate(java.util.Date currentDate) {
        this.currentDate = currentDate;
    }

    /**
     * @return the currentRequest
     */
    public CEActionRequest getCurrentRequest() {
        return currentRequest;
    }

    /**
     * @param currentRequest the currentRequest to set
     */
    public void setCurrentRequest(CEActionRequest currentRequest) {
        this.currentRequest = currentRequest;
    }
    
    /**
     * @return the personCandidateList
     */
    public List<Person> getPersonCandidateList() {
        return personCandidateList;
    }

    /**
     * @param personCandidateList the personCandidateList to set
     */
    public void setPersonCandidateList(List<Person> personCandidateList) {
        this.personCandidateList = personCandidateList;
    }

    public List<CEActionRequestIssueType> getIssueTypeList() {
        return issueTypeList;
    }

    public void setIssueTypeList(List<CEActionRequestIssueType> issueTypeList) {
        this.issueTypeList = issueTypeList;
    }

    public String getBlobDescription() {
        return blobDescription;
    }

    public void setBlobDescription(String blobDescription) {
        this.blobDescription = blobDescription;
    }

    public int getDescriptionBlobID() {
        return descriptionBlobID;
    }

    public void setDescriptionBlobID(int descriptionBlobID) {
        this.descriptionBlobID = descriptionBlobID;
    }

    /**
     * @return the muniList
     */
    public List<Municipality> getMuniList() {
        return muniList;
    }

    /**
     * @param muniList the muniList to set
     */
    public void setMuniList(List<Municipality> muniList) {
        this.muniList = muniList;
    }

    /**
     * @return the phoneTypeList
     */
    public List<ContactPhoneType> getPhoneTypeList() {
        return phoneTypeList;
    }

    /**
     * @param phoneTypeList the phoneTypeList to set
     */
    public void setPhoneTypeList(List<ContactPhoneType> phoneTypeList) {
        this.phoneTypeList = phoneTypeList;
    }

    /**
     * @return the allowSubmissionAsInternalUser
     */
    public boolean isAllowSubmissionAsInternalUser() {
        return allowSubmissionAsInternalUser;
    }

    /**
     * @param allowSubmissionAsInternalUser the allowSubmissionAsInternalUser to set
     */
    public void setAllowSubmissionAsInternalUser(boolean allowSubmissionAsInternalUser) {
        this.allowSubmissionAsInternalUser = allowSubmissionAsInternalUser;
    }

}