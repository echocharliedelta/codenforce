/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.coordinators.EventCoordinator;
import com.tcvcog.tcvce.coordinators.PersonCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.coordinators.UserCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.EventException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.BlobLight;
import com.tcvcog.tcvce.entities.BlobLinkEnum;
import com.tcvcog.tcvce.entities.CECaseDataHeavy;
import com.tcvcog.tcvce.entities.Citation;
import com.tcvcog.tcvce.entities.CitationCodeViolationLink;
import com.tcvcog.tcvce.entities.CitationCodeViolationLinkUpdateBundle;
import com.tcvcog.tcvce.entities.CitationCodeViolationLinkUpdateStatus;
import com.tcvcog.tcvce.entities.CitationDocketRecord;
import com.tcvcog.tcvce.entities.CitationFilingType;
import com.tcvcog.tcvce.entities.CitationPenalty;
import com.tcvcog.tcvce.entities.CitationPenaltyTypeEnum;
import com.tcvcog.tcvce.entities.CitationStatus;
import com.tcvcog.tcvce.entities.CitationStatusLogEntry;
import com.tcvcog.tcvce.entities.CitationViolationStatusEnum;
import com.tcvcog.tcvce.entities.CodeViolation;
import com.tcvcog.tcvce.entities.CourtEntity;
import com.tcvcog.tcvce.entities.EventType;
import com.tcvcog.tcvce.entities.HumanLink;
import com.tcvcog.tcvce.entities.IfaceCitationPenaltyHolder;
import com.tcvcog.tcvce.entities.User;
import com.tcvcog.tcvce.integration.CourtEntityIntegrator;
import com.tcvcog.tcvce.util.MessageBuilderParams;
import org.primefaces.PrimeFaces;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.faces.event.ActionEvent;
import org.primefaces.PrimeFaces;

/**
 * Manages citation operations that are attached to CECases
 * @author sylvia
 */
public class CitationBB extends BackingBeanUtils {

    private Citation currentCitation;
    
    private List<CitationFilingType> citationFilingTypeList;

    private boolean issueCitationDisabled;
    private boolean updateCitationDisabled;
    private String formNoteCitationText;
    private User citationIssuingOfficer;
    
    private boolean showCitationAddWarning;
    private String citationAddWarningText;
    
    // CITATION permissions
    private boolean permissionAllowCitationOpenClose;
    private boolean permissionAllowCitationUpdate;
    
    
    // CITATION - VIOLATION LINKS
    private List<CitationCodeViolationLink> removedViolationList;
    private String citationEditEventDescription;
    private CitationCodeViolationLink currentCitationViolationLink;
    private String formCitationViolationLinkNotes;
    private List<CitationViolationStatusEnum> citationViolationStatusEnumList;
    private List<User> citingOfficerUserCandidateList;
    private CitationCodeViolationLinkUpdateBundle citationViolationUpdateBundle;
    private CitationViolationStatusEnum selectedCitationViolationBatchStatusEnum;
    
    //    DOCKETS 
    private boolean citationDocketEditMode;
    private CitationDocketRecord currentCitationDocket;
    private List<CourtEntity> courtEntityList;
    private String docketNotesFormText;
    
    //    STATUS LOGS
    private boolean citationStatusEditMode;
    private CitationStatusLogEntry currentCitationStatusLogEntry;
    private List<CitationStatus> citationStatusList;
    private String statusNotesFormText;
    
    private boolean citationInfoEditMode;
    
    //      PENALTIES
    private CitationPenalty currentPenalty;
    private IfaceCitationPenaltyHolder currentPenaltyHolder;
    private boolean currentPenaltyEditMode;
    private List<CitationPenaltyTypeEnum> penaltyTypeCandidateList;
    
    
    
    /**
     * Creates a new instance of CitationBB
     */
    public CitationBB() {
    }
    
    
    @PostConstruct
    public void initBean()  {
        System.out.println("CitationBB.initBean()");
        CaseCoordinator cc = getCaseCoordinator();
        UserCoordinator uc = getUserCoordinator();
        // Citation stuff
        
        
        try {
            citationStatusList = cc.citation_getCitationStatusList(false);
            courtEntityList = cc.getCourtEntityListComplete(true);
            citationFilingTypeList = cc.citation_getCitationFilingTypeList();
            citingOfficerUserCandidateList = uc.user_assembleUserListOfficerRequired(getSessionBean().getSessMuni(), true);
            penaltyTypeCandidateList = Arrays.asList(CitationPenaltyTypeEnum.values());
            
        } catch (IntegrationException | BObStatusException | AuthorizationException  ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "ERROR setting up citation subsystem: " + ex.getMessage(), ""));
        }
        
        setRemovedViolationList(new ArrayList<>());
        
        // set edit defaults
        citationStatusEditMode = false;
        citationDocketEditMode = false;
        citationInfoEditMode = false;
        citationViolationStatusEnumList = Arrays.asList(CitationViolationStatusEnum.values());
        
        configureCitationPermissions();
        
        setShowCitationAddWarning(false);
    }
    
    public void onCitationViewButtonChange(Citation cit){
        currentCitation = cit;
    }
    
    /**
     * Generic cancellation event listener
     * @param ev 
     */
    public void onOperationCancelButtonChange(ActionEvent ev){
        System.out.println("CitationBB.onOperationCancelButtonChange");
    }
    
    
    /*******************************************************/
    /*******************************************************/
    /**              Citations PERMISSIONS                **/
    /*******************************************************/
    /*******************************************************/
    
    /**
     * Sets boolean flags for the view based on permissions business logic
     * living in the CaseCoordinator
     */
    private void configureCitationPermissions(){
        CaseCoordinator cc = getCaseCoordinator();
        permissionAllowCitationOpenClose = cc.permissionsCheckpointCitationOpenClose(getSessionBean().getSessUser());
        permissionAllowCitationUpdate = cc.permissionsCheckpointCitationUpdate(getSessionBean().getSessUser());
    }
    
    /*******************************************************
    /*******************************************************
     **              Citations GENERAL                    **
    /*******************************************************/
    /*******************************************************/
    
    /**
     * Special getter wrapper for citation blobs that responds to
     * the bl ob tools update field
     * @return 
     */
    public List<BlobLight> getCitationBlobsAutoUpdated(){
       List<BlobLight> sessBlobListForUpdate = getSessionBean().getSessBlobLightListForRefreshUptake();
        if(sessBlobListForUpdate != null 
                && currentCitation != null 
                && getSessionBean().getSessBlobLightListHolderForRefreshUptake() != null
                && getSessionBean().getSessBlobLightListHolderForRefreshUptake().getBlobLinkEnum() == BlobLinkEnum.CITATION
                && getSessionBean().getSessBlobLightListHolderForRefreshUptake().getParentObjectID() == currentCitation.getCitationID()){
            System.out.println("CitationBB.getBlobLightListFromCECase | found non-null session blob list for uptake: " + sessBlobListForUpdate.size());
            getCurrentCitation().setBlobList(sessBlobListForUpdate);
            // clear session since we have the new list
            getSessionBean().resetBlobRefreshUptakeFields();
            return sessBlobListForUpdate;
        } else {
            if(currentCitation.getBlobList() != null){
                return currentCitation.getBlobList();
            } else {
                return new ArrayList<>();
            }
        }
    }
    
    /**
     * Listener for requests to refresh the current citation
     */
    public void refreshCurrentCitation(){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            currentCitation = cc.citation_getCitation(currentCitation.getCitationID());
            System.out.println("CitationBB.refreshCurrentCitation | ID: " + currentCitation.getCecaseID());
        } catch (IntegrationException | BObStatusException | BlobException ex) {
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
    }
    
   
    
    /**
     * Listener for user requests to edit a citation's info
     */
   public void onCitationEditModeToggle(){
        if(citationInfoEditMode){
            onCitationUpdateCommitButtonChange(null);
            refreshCurrentCitation();
        }
        citationInfoEditMode = !citationInfoEditMode;
       System.out.println("CitationBB.onCitationEditModeToggle: End of method citationInfoEditMode is: " + citationInfoEditMode);
   } 
   
   /**
    * listener for user requests to abort edits of citation info
    * @param ev 
    */
   public void onCitationEditAbortButtonChange(ActionEvent ev){
       citationInfoEditMode = false;
       
   }
    
   /**
    * listener for user requests to start a new citation
    * @param ev 
    */
    public void onCitationAddInitButtonChange(ActionEvent ev){
          CaseCoordinator cc = getCaseCoordinator();
        
        try {
            citationIssuingOfficer = getSessionBean().getSessUser();
            currentCitation = cc.citation_getCitationSkeleton(getSessionBean().getSessCECase(), getSessionBean().getSessUser(), citationIssuingOfficer );
            configureCitationAddWarning();
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
    }
    
    /**
     * Sets up a a boolean flag alert and message during citation add for a 
     * potential flow non compliance during work
     */
    private void configureCitationAddWarning(){
        CECaseDataHeavy csedh = getSessionBean().getSessCECase();
        StringBuilder sb = new StringBuilder();
        if(csedh != null){
            System.out.println("CitationBB.configureCitationAddWarning | Checking case for citation warning ID:" + csedh.getCaseID());
            if(csedh.getNoticeList() == null || csedh.getNoticeList().isEmpty()){
                sb.append("No Notices of violation have been issued. Consider sending notice prior to issuing a citation");
                showCitationAddWarning = true;
            }
            if(csedh.getViolationList() == null || csedh.getViolationList().isEmpty()){
                sb.append("No violations have been attached to this case and you won't succeed in adding this citation");
                showCitationAddWarning = true;
            }
        }
        citationAddWarningText = sb.toString();
    }
    
    /**
     * Turns off citation add warning.
     * @param ev 
     */
    public void onDismissCitationAddWarning(ActionEvent ev){
        showCitationAddWarning = false;
    }
    
      /**
     * Listener for user requests to issue a citation
     *
     * @param ev
     */
    public void onCitationAddCommitButtonChange() {
        System.out.println("CitationBB.onCitationAddCommitButtonChange");
        CaseCoordinator cc = getCaseCoordinator();
        if(currentCitation != null){
            try {
                cc.citation_insertCitation(currentCitation, getSessionBean().getSessUser());
                refreshCurrentCitation();
                onStatusLogEntryAddInitButtonChange(null);
                getSessionBean().setSessCECaseRefreshTriggerCitations(LocalDateTime.now());
                getSessionBean().setSessCECaseRefreshTriggerViolations(LocalDateTime.now());
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "New citation added to database!", ""));
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Please make a citation status log entry now.", ""));
            } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                ex.getMessage(), ""));
                System.out.println(ex);
            }
        } else {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to issue citation due to page object error", ""));
        }
    }
    
    /**
     * listener for users to start the citing officer change list
     * @param ev 
     */
    public void onCitationChangeOfficerInit(ActionEvent ev){
        if(currentCitation != null && currentCitation.getFilingOfficer() != null){
            System.out.println("CitationBB.onCitationChangeOfficerInit | current officer: " +  currentCitation.getFilingOfficer().getUsername());
        }
        
    }
    
    /**
     * Listener for users to commit the citing officer change operation
     * @param ev 
     */
    public void onCitationChangeOfficerCommit(ActionEvent ev){
        if(currentCitation != null && currentCitation.getFilingOfficer() != null){
            System.out.println("CitationBB.onCitationChangeOfficerCommit | new officer: " +  currentCitation.getFilingOfficer().getUsername());
        }
    }
    
    /**
     * Listener for user requests to move to the final step of the citation add process
     * which is adding an initial status log
     * @param ev 
     */
    public void onCitationAddPersonlinksComplete(ActionEvent ev){
        System.out.println("CitationBB.onCitationAddPersonlinksComplete");
        
        
        
    }
    /**
     * Listener for user requests to move to the final step of the citation add process
     * which is adding an initial status log
     * @param ev 
     */
    public void onCitationDocketAddPersonlinksComplete(ActionEvent ev){
        System.out.println("CitationBB.onCitationDocketAddPersonlinksComplete");
        
        
        
    }
    
     /**
     * Listener for user requests to update the current citation.
     * Only accessible from a citation profile, so no need for
     * the Citation to come as an input param
     */
    public void onCitationUpdateInitButtonChange(){
        
        // TODO: Check logic on citation to see for allowable updates
        
    }
    
      /**
     * Listener for user requests to commit
     * @param ev citation updates
     *
     */
    public void onCitationUpdateCommitButtonChange(ActionEvent ev) {
        System.out.println("CitationBB.updateCitation");
        CaseCoordinator cc = getCaseCoordinator();

        try {
            cc.citation_updateCitation(currentCitation, getSessionBean().getSessUser()); 
            refreshCurrentCitation();
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
            System.out.println(ex);
            
        }
    }
    
    /**
     * Listener for user requests to start the citation removal process
     * @param ev
     */
    public void onCitationRemoveInitButtonChange(ActionEvent ev){
        System.out.println("CitationBB.onCitationRemoveInitButtonChange");
    }
    
     /**
     * Listener for user requests to remove a citation
     *
     * @param ev
     */
    public void onCitationRemoveCommitButtonChange(ActionEvent ev) {
        CaseCoordinator cc = getCaseCoordinator();
        try {
            cc.citation_deactivateCitation(currentCitation, getSessionBean().getSessCECase(), getSessionBean().getSessUser());
            refreshCurrentCitation();
                    
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }

    }
    
    /**
     * Listener for user requests to start the note process
     * for a specific citation
     */
    public void onCitationNoteInitButtonChange(){
        formNoteCitationText = "";
        
    }
    
    /**
     * Listener for user requests to complete the citation
     * note add operation
     * @param ev 
     */
    public void onCitationNoteCommitButtonChange(ActionEvent ev){
        
         CaseCoordinator cc = getCaseCoordinator();

        MessageBuilderParams mbp = new MessageBuilderParams();
        mbp.setCred(getSessionBean().getSessUser().getKeyCard());
        mbp.setExistingContent(getCurrentCitation().getNotes());
        mbp.setNewMessageContent(formNoteCitationText);
        mbp.setHeader("Citation Note");
        mbp.setUser(getSessionBean().getSessUser());

        try {

            cc.citation_updateNotes(mbp, getCurrentCitation());
            refreshCurrentCitation();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Succesfully appended note!", ""));
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error appending note; apologies!", ""));
        }
        
    }
    
    /* **********************************************/
    /* ********** CITATION VIOLATION TOODL **********/
    /* **********************************************/
    
    /**
     * Listener for user requests to remove a violation from a citation
     * @param v 
     */
    public void onCitationViolationRemoveButtonChange(CitationCodeViolationLink v) {
        System.out.println("CitationBB.onCitationViolationRemoveButtonChange | CITV ID: " + v.getCitationViolationID());
        getCurrentCitation().getViolationList().remove(v);
        getRemovedViolationList().add(v);
    }

    /**
     * Listener for user requests to add a violation to a citation
     * @param v 
     */
    public void onCitationViolationRestoreButtonChange(CitationCodeViolationLink v) {
        System.out.println("CitationBB.onCitationViolationRestoreButtonChange | CITV ID: " + v.getCitationViolationID());
        getCurrentCitation().getViolationList().add(v);
        getRemovedViolationList().remove(v);
    }
    
    /**
     * Listener for user requests to start the status update for a citation
     * violation 
     * @param ccvl 
     */
    public void onCitationViolationStatusUpdateInit(CitationCodeViolationLink ccvl){
        currentCitationViolationLink = ccvl;
        formCitationViolationLinkNotes = "";
    }
    
    /**
     * Listener for users who are done with updating a citation violation status
     * and I'll automatically append the update and notes to the notes column
     * @param ev 
     */
    public void onCitationViolationStatusUpdateCommit(ActionEvent ev){
        SystemCoordinator sc = getSystemCoordinator();
        CaseCoordinator cc = getCaseCoordinator();
        if(currentCitationViolationLink != null){
            try {
                cc.citation_updateCitationCodeViolationLink(currentCitationViolationLink, formCitationViolationLinkNotes, getSessionBean().getSessUser());
                refreshCitationRelated();
                 getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Updated citation violation status!", ""));
            } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
                System.out.println(ex);
                 getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
            } 
        }
    }
    
   /**
    * Listener to view the history of the citation violation link
    * @param ccvl 
    */
    public void onCitationViolationView(CitationCodeViolationLink ccvl){
        currentCitationViolationLink = ccvl;
        
    }
    
    /* **********************************************/
    /* ********** CITATION STATUS LOGS **************/
    /* **********************************************/
    
    /**
     * Listener for user requests to view the citation status log entry
     * @param csle 
     */
    public void onCitationStatusLogEntryViewButtonChange(CitationStatusLogEntry csle){
        System.out.println("CitationBB.onCitationStatusLogEntryViewButtonChange | Viewing CSLE ID " + csle.getLogEntryID());
        currentCitationStatusLogEntry = csle;
    }
    
    /**
     * Listener for user requests to view the citation status log entry
     * @param csle 
     */
    public void onCitationStatusLogEntryEditButtonChange(CitationStatusLogEntry csle){
        System.out.println("CitationBB.onCitationStatusLogEntryEditButtonChange | Viewing CSLE ID " + csle.getLogEntryID());
        citationStatusEditMode = true;
        currentCitationStatusLogEntry = csle;
    }
    
    
    /**
     * Listener for user requests to edit a citation's status record 
     */
   public void onCitationStatusLogEditModeToggle(){
       EventCoordinator ec = getEventCoordinator();
        // clicking done means save edits
        if(citationStatusEditMode){
            if(currentCitationStatusLogEntry != null){
                // form validation
               
                if(currentCitationStatusLogEntry.getLogEntryID() == 0){
                    onStatusLogEntryAddCommitButtonChange(null);
                    System.out.println("citationBB.onStatusLogEntryAddCommitButtonChange | called on new status edit mode toggle");
                } else {
                    onStatusLogEntryEditCommitButtonChange(null);
                    System.out.println("citationBB.onStatusLogEntryEditCommitButtonChange | called on existing status edit mode toggle");
                }
                try {
                    // implicit current log entry update in edits and add methods
                    refreshCitationRelated();
                    // refresh event subsystem
                    getSessionBean().setSessCECaseRefreshTriggerCitations(LocalDateTime.now());
                    getSessionEventConductor().refreshCalendarAndFollowupBacklog(null);
                    getSessionEventConductor().setSessEventListForRefreshUptake(ec.getEventList(getSessionBean().getSessCECase()));
                    
                    // setup for citation-violation management
                    boolean showCitVUpdateDialog = false;
                    if(currentCitationStatusLogEntry.getStatus().getBatchApplyCitationViolationStatus() != null){
                        showCitVUpdateDialog = true;
                        selectedCitationViolationBatchStatusEnum = currentCitationStatusLogEntry.getStatus().getBatchApplyCitationViolationStatus();
                        configureCitationViolationBatchUpdateBundle();
                    }
                    PrimeFaces.current().ajax().addCallbackParam("showcitvupdatedialog", showCitVUpdateDialog);
                    
                    
                } catch (IntegrationException  | BObStatusException ex) {
                    System.out.println(ex);
                    FacesContext.getCurrentInstance().validationFailed();
                    getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Error adjusting related events: " + ex.getMessage(), ""));
                }
            
            }
        }
        citationStatusEditMode = !citationStatusEditMode;
        System.out.println("CitationBB.onCitationStatusLogEditModeToggle | citationStatusEditMode val is " + citationStatusEditMode);
   } 
   
   /**
    * listener for user requests to start a batch update of all their citation-violations.
    * This is the dedicated pathway. This process can also be triggered
    * by adding a citation status log entry with an batch status update
    * mapping.
    * 
    * @param ev 
    */
   public void batchCitationViolationUpdateInit(ActionEvent ev){
        try {
            // choose a placeholder status
            selectedCitationViolationBatchStatusEnum = CitationViolationStatusEnum.UNKNOWN;
            configureCitationViolationBatchUpdateBundle();
        } catch (BObStatusException ex) {
            System.out.println(ex);
        }
       
       
   }
   
   /**
    * Listener for any batch status request changes; Iterates overall 
    * all our bundle's violations to update their status to user batch requested
    */
   public void onChangeSelectedCitationViolationStatusDropDown(){
       System.out.println("CitationBB.onChangeSelectedCitationViolationStatusDropDown");
       if(selectedCitationViolationBatchStatusEnum != null && citationViolationUpdateBundle != null && citationViolationUpdateBundle.getCvLinksToUpdate() != null){
            for(CitationCodeViolationLinkUpdateStatus status: citationViolationUpdateBundle.getCvLinksToUpdate()){
                status.setUpdatedStatus(selectedCitationViolationBatchStatusEnum);
            }
       }
       
   }
   
   
   /**
    * Sets up a bundle for the UI of citation-violation links that are terminal
    * and those whose status is in process. 
    */
   private void configureCitationViolationBatchUpdateBundle() throws BObStatusException{
       CaseCoordinator cc = getCaseCoordinator();
       citationViolationUpdateBundle = cc.citation_buildCitationViolationBatchUpdateBundle(currentCitation, selectedCitationViolationBatchStatusEnum, getSessionBean().getSessUser());
       
       
   }
   
   /**
    * Listener for user requests to proceed with the batch cit-v update process
    * @param ev 
    */
   public void onCitationViolationBatchUpdateCommit(ActionEvent ev){
       CaseCoordinator cc = getCaseCoordinator();
       
        try {
            citationViolationUpdateBundle = cc.citation_processCitationViolationBatchUpdateBundle(citationViolationUpdateBundle);
            refreshCitationRelated();
            // reset our bundle processing facility
            citationViolationUpdateBundle  = null;
            System.out.println("CitationBB.onCitationViolationBatchUpdateCommit");
            getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Success! Updated code violation links", ""));
            
        } catch (BObStatusException | AuthorizationException | BlobException | IntegrationException  ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Error batch processing citation violations: " + ex.getMessage(), ""));
        } 
   }
   
   /**
    * Dequeues a CV link
    * @param link 
    */
    public void onCitationViolationRemoveFromBatchQueue(CitationCodeViolationLinkUpdateStatus link){
        if(citationViolationUpdateBundle != null && citationViolationUpdateBundle.getCvLinksToSkip() != null){
            citationViolationUpdateBundle.getCvLinksToUpdate().remove(link);
            link.setUpdatedStatus(null);
            citationViolationUpdateBundle.getCvLinksToSkip().add(link);
            System.out.println("CitationBB.onCitationViolationRemoveFromBatchQueue | linkID: " + link.getCcvLink().getCitationViolationID());
        }
    }
    
    /**
     * Queues a CV link 
     * @param link 
     */
    public void onCitationViolationAddToBatchQueue(CitationCodeViolationLinkUpdateStatus link){
        if(citationViolationUpdateBundle != null && citationViolationUpdateBundle.getCvLinksToSkip() != null){
            link.setUpdatedStatus(selectedCitationViolationBatchStatusEnum);
            citationViolationUpdateBundle.getCvLinksToUpdate().add(link);
            citationViolationUpdateBundle.getCvLinksToSkip().remove(link);
            System.out.println("CitationBB.onCitationViolationAddToBatchQueue | linkID: " + link.getCcvLink().getCitationViolationID());
        }
    }
   
   
   /**
    * Because we changed the underlying statuses of our citation-violation links
    * in preparation for our batch update, we'll need to get a fresh DB copy to inject
    * during this abort procss.
    * @param ev 
    */
   public void onCitationViolationBatchUpdateAbort(ActionEvent ev){
       refreshCurrentCitation();
       // throw out our update bundle entirely
       citationViolationUpdateBundle = null;
       
       
   }
   
   /**
    * Listener for methods to abort the edit process
    */
   public void onCitationStatusLogEditAbortListener(){
       citationStatusEditMode = false;
   }
   
   /**
    * Listener to create a new status log directly from the citation table itself.
    * I just call the view citation and the new status log listeners
    * @param cit 
    */
   public void onStatusLogEntryInitFromTable(Citation cit){
       onCitationViewButtonChange(cit);
       onStatusLogEntryAddInitButtonChange(null);
   }
   
  
   /**
    * Listener for user requests to start the status log entry process
    * @param ev 
    */
   public void onStatusLogEntryAddInitButtonChange(ActionEvent ev){
       CaseCoordinator cc = getCaseCoordinator();
       currentCitationStatusLogEntry = cc.citation_getStatusLogEntrySkeleton(currentCitation);
       citationStatusEditMode = true;
   }
   
   
   
   /**
    * Listener for user requests to finalize their new log entry
    * @param ev 
    */
   public void onStatusLogEntryAddCommitButtonChange(ActionEvent ev){
       CaseCoordinator cc = getCaseCoordinator();
       
        try {
            int freshID = cc.citation_insertCitationStatusLogEntry(currentCitation, currentCitationStatusLogEntry, getSessionBean().getSessCECase(), getSessionBean().getSessUser());
            currentCitationStatusLogEntry =cc.citation_getCitationStatusLogEntry(freshID);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Success! Created log entry with ID " + freshID, ""));
        } catch (BObStatusException | IntegrationException | AuthorizationException | EventException ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        } 
       
   }
   
       
    /**
     * Listener for user requests to commit edits to the current log entry
     * @param ev 
     */
    public void onStatusLogEntryEditCommitButtonChange(ActionEvent ev){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            cc.citation_updateCitationStatusLogEntry(currentCitation, currentCitationStatusLogEntry, getSessionBean().getSessCECase(), getSessionBean().getSessUser());
            currentCitationStatusLogEntry = cc.citation_getCitationStatusLogEntry(currentCitationStatusLogEntry.getLogEntryID());
            
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Success! Updated log entry ID: " + currentCitationStatusLogEntry.getLogEntryID(), ""));
        } catch (BObStatusException | IntegrationException | AuthorizationException | EventException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
            
        }
    }
    
    /**
     * Listener for user requests to start the log entry removal process
     * @param ev 
     */
    public void onStatusLogEntryRemoveInitButtonChange(ActionEvent ev){
        // Nothing to do here yet
        
        
    }
    
    /**
     * Listener for user requests to finalize the log entry removal process
     * @param ev 
     */
    public void onStatuslogEntryRemoveCommitButtonChange(ActionEvent ev){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            cc.citation_deactivateCitationStatusLogEntry(currentCitationStatusLogEntry, getSessionBean().getSessCECase(), getSessionBean().getSessUser()); 
            refreshCurrentCitation();
            
            getSessionBean().setSessCECaseRefreshTriggerCitations(LocalDateTime.now());
            getSessionEventConductor().refreshCalendarAndFollowupBacklog(null);
            getSessionEventConductor().setSessEventListForRefreshUptake(getSessionBean().getSessCECase().getEventList());
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                           "Success! Removed citation status log entry ID " + currentCitationStatusLogEntry.getLogEntryID(), ""));
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
        
    }
    
    /**
     * listener for user requests to start to add note to a status log entry
     */
    public void onStatusLogNoteInitButtonChange(){
        statusNotesFormText = "";
        
        
    }
    
    /**
     * Listener for user requests to complete note process for status logs
     */
    public void onStatusLogNoteCommitButtonChange(){
        SystemCoordinator sc = getSystemCoordinator();
        
        MessageBuilderParams mbp = new MessageBuilderParams();
        
        mbp.setCred(getSessionBean().getSessUser().getKeyCard());
        mbp.setUser(getSessionBean().getSessUser());
        mbp.setNewMessageContent(statusNotesFormText);
        mbp.setExistingContent(currentCitationStatusLogEntry.getNotes());
        
        currentCitationStatusLogEntry.setNotes(sc.appendNoteBlock(mbp));
        try {
            sc.writeNotes(currentCitationStatusLogEntry, getSessionBean().getSessUser());
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Success! Added note to Status Log ID " + currentCitationStatusLogEntry.getLogEntryID(),""));
        } catch (IntegrationException | BObStatusException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        } 
    }
    
    
    
    /* **********************************************/
    /* **************** DOCKETS  ********************/
    /* **********************************************/
    
    /**
     * Listener to view a docket
     * @param cdr 
     */
    public void onDocketViewButtonChange(CitationDocketRecord cdr){
        currentCitationDocket = cdr;
        citationDocketEditMode = false;
    }
    
    
    /**
     * Listener to view a docket
     * @param cdr 
     */
    public void onDocketEditButtonChange(CitationDocketRecord cdr){
        currentCitationDocket = cdr;
        citationDocketEditMode = true;
    }
    
    
    /**
     * Listener for user requests to edit a citation docket record info
     */
   public void onCitationDocketEditModeToggle(){
       if(citationDocketEditMode){
           if(currentCitationDocket != null && currentCitationDocket.getDocketID()==0){
               onDocketAddCommitButtonChange();
           } else {
               onDocketEditCommitButtonChange(null);
               
           }
           refreshCurrentCitation();
           getSessionBean().setSessCECaseRefreshTriggerCitations(LocalDateTime.now());
       }
        citationDocketEditMode = !citationDocketEditMode;
        
        System.out.println("CitationBB.onCitationDocketEditModeToggle | citationDocketEditMode val is " + citationDocketEditMode);
   } 
   
   /**
    * Internal refresher of dockets!
    * @throws BObStatusException
    * @throws IntegrationException 
    */
   private void refreshCurrentDocket() throws BObStatusException, IntegrationException{
       CaseCoordinator cc = getCaseCoordinator();
       currentCitationDocket = cc.citation_getCitationDocketRecord(currentCitationDocket.getHostPK());
   }
    
   
   /**
    * Listener for commencement of docket creation process
     * @param ev
    */
   public void onDocketAddInitButtonChange(ActionEvent ev){
       CaseCoordinator cc = getCaseCoordinator();
       currentCitationDocket = cc.citation_getCitationDocketRecordSkeleton(currentCitation);
       System.out.println("CitationBB.onDocketAddInitButtonChange");
       citationDocketEditMode = true;
       
   }
   
   /**
    * Initiates the writing of a new docket to a citation
    */
   public void onDocketAddCommitButtonChange(){
       CaseCoordinator cc = getCaseCoordinator();
        try {
            int freshID = cc.citation_insertDocketEntry(currentCitationDocket, getSessionBean().getSessUser());
            currentCitationDocket = cc.citation_getCitationDocketRecord(freshID);
            
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Success! Docket added to citation.", ""));
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
   }
   
    /**
     * Listener for user requests to commit edits to the current docket
     * @param ev 
     */
    public void onDocketEditCommitButtonChange(ActionEvent ev){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            cc.citation_updateDocketEntry(currentCitationDocket, getSessionBean().getSessUser());
            refreshCurrentDocket();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Success! Updated docket Number: " + currentCitationDocket.getDocketNumber(), ""));
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
        
        System.out.println("CitationBB.onDocketEditCommitButtonChange");
    }
    
    /**
     * Commences the user's docket removal process
     */
    public void onDocketRemoveInitButtonChange(){
        // nothing to do here yet
    }
    
    /**
     * Listener for user requests to confirm their removal of a docket
     */
    public void onDocketRemoveCommitButtonChange(){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            cc.citation_deactivateDocketEntry(currentCitationDocket, getSessionBean().getSessUser());
            refreshCurrentCitation();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Success! Removed Docket ID: " + currentCitationDocket.getDocketID(), ""));
        } catch (IntegrationException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to remove the Docket ID: " + currentCitationDocket.getDocketID(), ""));
        }
    }
    
      /**
     * listener for user requests to start to add note to a status log entry
     */
    public void onDocketNoteInitButtonChange(){
        docketNotesFormText = "";
        
        
    }
    
    /**
     * Listener for user requests to complete note process for status logs
     */
    public void onDocketNoteCommitButtonChange(){
        SystemCoordinator sc = getSystemCoordinator();
        
        MessageBuilderParams mbp = new MessageBuilderParams();
        
        mbp.setCred(getSessionBean().getSessUser().getKeyCard());
        mbp.setUser(getSessionBean().getSessUser());
        mbp.setNewMessageContent(docketNotesFormText);
        mbp.setExistingContent(currentCitationDocket.getNotes());
        
        currentCitationDocket.setNotes(sc.appendNoteBlock(mbp));
        try {
            sc.writeNotes(currentCitationDocket, getSessionBean().getSessUser());
            refreshCurrentCitation();
            refreshCurrentDocket();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Success! Added note to Docket ID " + currentCitationDocket.getDocketID(),""));
        } catch (IntegrationException | BObStatusException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        } 
    }
    
    /**
     * Listener for users to stop the docket add or update operation
     * @param ev 
     */
    public void onDocketAddEditOperationAbortButtonChange(ActionEvent ev){
        citationDocketEditMode = false;
        System.out.println("citationBB.onDocketAddEditOperationAbortButtonChange");
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Operation aborted!" + currentCitationDocket.getDocketID(),""));
    }
    
    
  
    
     /**
     * Special wrapper getter around the current citation's human
     * link list that asks the session for a new link list
     * on table load that might occur during a link edit operation
     * @return the new human link list
     */
    public List<HumanLink> getManagedDocketHumanLinkList(){
        List<HumanLink> hll = getSessionBean().getSessHumanListRefreshedList();
        if(hll != null){
            currentCitationDocket.sethumanLinkList(hll);
            // clear our refreshed list
            getSessionBean().setSessHumanListRefreshedList(null);
        } else {
            System.out.println("CitationBB.getManagedDocketHumanLinkList | emtpy session human links list" );
        }
        return currentCitationDocket.gethumanLinkList();
    }
    
    
    
    
    /* ******************************************** */
    /* ************* PENALTIES!!!       *********** */
    /* ******************************************** */
    
    /**
     * Listener to start the citation penalty creation process
     * @param polder 
     */
    public void onPenaltyCreateInit(IfaceCitationPenaltyHolder polder){
        CaseCoordinator cc = getCaseCoordinator();
        if(polder == null){
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to create new penalty",""));
            return;
        }
        try {
            currentPenaltyHolder = polder;
            currentPenalty = cc.citation_getCitationPenaltySkeleton(polder);
            currentPenaltyEditMode = true;
        } catch (BObStatusException ex) {
            System.out.println();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to create new penalty",""));
        }
        
    }
    
    /**
     * View pathway for Penalties
     * @param penalty 
     */
    public void onPenalityViewEdit(CitationPenalty penalty){
        if(penalty != null){
            System.out.println("CitationBB.onPenalityViewEdit | penaltyID: " + penalty.getPenaltyID());
            currentPenalty = penalty;
            currentPenaltyEditMode = true;
        }
    }
    
    /**
     * Listener for user requests to insert or update a citation penalty
     * @param ev 
     */
    public void onTogglePenaltyAddEditMode(ActionEvent ev){
        CaseCoordinator cc = getCaseCoordinator();
        System.out.println("CitationBB.onTogglePenaltyAddEditMode | edit mode: " + currentPenaltyEditMode);
        if(currentPenaltyEditMode){
            if(currentPenalty != null){
                try{
                    if(currentPenalty.getPenaltyID() == 0){
                        int freshID = cc.citation_insertCitationPenalty(currentPenalty, currentPenaltyHolder, getSessionBean().getSessCECase(), getSessionBean().getSessUser());
                        currentPenalty = cc.citation_getCitationPenalty(freshID);
                    } else {
                        cc.citation_updateCitationPenalty(currentPenalty, currentPenaltyHolder, getSessionBean().getSessCECase(), getSessionBean().getSessUser());
                        currentPenalty = cc.citation_getCitationPenalty(currentPenalty.getPenaltyID());
                    }
                  refreshCitationRelated();
                    
                } catch(BObStatusException | IntegrationException ex){
                    System.out.println(ex);
                    getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                    "Unable to insert or update current penalty",""));
                    
                }
            }
        }
        currentPenaltyEditMode = !currentPenaltyEditMode;
    }
    
    /**
     * Calls refreshCurrentCitation() AND requires that the cecase be refreshed as well
     */
    private void refreshCitationRelated(){
        refreshCurrentCitation();
        CECaseBB cseBB = (CECaseBB) getFacesContext().getApplication().evaluateExpressionGet(getFacesContext(), "#{ceCaseBB}", CECaseBB.class);
        if(cseBB != null){
            cseBB.reloadCurrentCase();
        }
    }
    
    /**
     * Abort listener
     * @param ev 
     */
    public void onPenaltyAddEditAbort(ActionEvent ev){
        currentPenaltyEditMode = false;
    }
    
    /**
     * listener to deac the citation penalty
     * @param ev
     */
    public void onDeactivateCitationPenalty(ActionEvent ev){
        CaseCoordinator cc = getCaseCoordinator();
        if(currentPenalty != null){
            try {
                cc.citation_deactivateCitationPenalty(currentPenalty, currentCitation, getSessionBean().getSessCECase(), getSessionBean().getSessUser());
                System.out.println("CitationBB.onDeactivateCitationPenalty | penaltyID: " + currentPenalty.getPenaltyID());
                refreshCitationRelated();
            } catch (BObStatusException | IntegrationException  ex) {
                System.out.println(ex);
            } 
        }
    }
    
    
    
    
    /* ******************************************** */
    /* *************misc citation stuff *********** */
    /* ******************************************** */
    
    /**
     * Special wrapper getter around the current citation's human
     * link list that asks the session for a new link list
     * on table load that might occur during a link edit operation
     * @return the new human link list
     */
    public List<HumanLink> getManagedCitationHumanLinkList(){
        List<HumanLink> hll = getSessionBean().getSessHumanListRefreshedList();
        if(hll != null){
            currentCitation.sethumanLinkList(hll);
            // clear our refreshed list
            getSessionBean().setSessHumanListRefreshedList(null);
        } else {
            System.out.println("CitationBB.getManagedCitationHumanLinkList | emtpy session human links list" );
        }
        return currentCitation.gethumanLinkList();
    }
  
   
    /**
     * @return the currentCitation
     */
    public Citation getCurrentCitation() {
        return currentCitation;
    }

    /**
     * @return the citationStatusList
     */
    public List<CitationStatus> getCitationStatusList() {
        return citationStatusList;
    }

    /**
     * @return the courtEntityList
     */
    public List<CourtEntity> getCourtEntityList() {
        return courtEntityList;
    }

    /**
     * @return the citationFilingTypeList
     */
    public List<CitationFilingType> getCitationFilingTypeList() {
        return citationFilingTypeList;
    }

    /**
     * @return the issueCitationDisabled
     */
    public boolean isIssueCitationDisabled() {
        return issueCitationDisabled;
    }

    /**
     * @return the updateCitationDisabled
     */
    public boolean isUpdateCitationDisabled() {
        return updateCitationDisabled;
    }

    /**
     * @return the formNoteCitationText
     */
    public String getFormNoteCitationText() {
        return formNoteCitationText;
    }

    /**
     * @return the removedViolationList
     */
    public List<CitationCodeViolationLink> getRemovedViolationList() {
        return removedViolationList;
    }

    /**
     * @return the citationEditEventDescription
     */
    public String getCitationEditEventDescription() {
        return citationEditEventDescription;
    }

    /**
     * @return the citationIssuingOfficer
     */
    public User getCitationIssuingOfficer() {
        return citationIssuingOfficer;
    }

    /**
     * @return the currentCitationStatusLogEntry
     */
    public CitationStatusLogEntry getCurrentCitationStatusLogEntry() {
        return currentCitationStatusLogEntry;
    }

    /**
     * @return the citationInfoEditMode
     */
    public boolean isCitationInfoEditMode() {
        return citationInfoEditMode;
    }

    /**
     * @return the citationDocketEditMode
     */
    public boolean isCitationDocketEditMode() {
        return citationDocketEditMode;
    }

    /**
     * @return the citationStatusEditMode
     */
    public boolean isCitationStatusEditMode() {
        return citationStatusEditMode;
    }

   
    /**
     * @param currentCitation the currentCitation to set
     */
    public void setCurrentCitation(Citation currentCitation) {
        this.currentCitation = currentCitation;
    }

    /**
     * @param citationStatusList the citationStatusList to set
     */
    public void setCitationStatusList(List<CitationStatus> citationStatusList) {
        this.citationStatusList = citationStatusList;
    }

    /**
     * @param courtEntityList the courtEntityList to set
     */
    public void setCourtEntityList(List<CourtEntity> courtEntityList) {
        this.courtEntityList = courtEntityList;
    }

    /**
     * @param citationFilingTypeList the citationFilingTypeList to set
     */
    public void setCitationFilingTypeList(List<CitationFilingType> citationFilingTypeList) {
        this.citationFilingTypeList = citationFilingTypeList;
    }

    /**
     * @param issueCitationDisabled the issueCitationDisabled to set
     */
    public void setIssueCitationDisabled(boolean issueCitationDisabled) {
        this.issueCitationDisabled = issueCitationDisabled;
    }

    /**
     * @param updateCitationDisabled the updateCitationDisabled to set
     */
    public void setUpdateCitationDisabled(boolean updateCitationDisabled) {
        this.updateCitationDisabled = updateCitationDisabled;
    }

    /**
     * @param formNoteCitationText the formNoteCitationText to set
     */
    public void setFormNoteCitationText(String formNoteCitationText) {
        this.formNoteCitationText = formNoteCitationText;
    }

    /**
     * @param removedViolationList the removedViolationList to set
     */
    public void setRemovedViolationList(List<CitationCodeViolationLink> removedViolationList) {
        this.removedViolationList = removedViolationList;
    }

    /**
     * @param citationEditEventDescription the citationEditEventDescription to set
     */
    public void setCitationEditEventDescription(String citationEditEventDescription) {
        this.citationEditEventDescription = citationEditEventDescription;
    }

    /**
     * @param citationIssuingOfficer the citationIssuingOfficer to set
     */
    public void setCitationIssuingOfficer(User citationIssuingOfficer) {
        this.citationIssuingOfficer = citationIssuingOfficer;
    }

    /**
     * @param currentCitationStatusLogEntry the currentCitationStatusLogEntry to set
     */
    public void setCurrentCitationStatusLogEntry(CitationStatusLogEntry currentCitationStatusLogEntry) {
        this.currentCitationStatusLogEntry = currentCitationStatusLogEntry;
    }

    /**
     * @param citationInfoEditMode the citationInfoEditMode to set
     */
    public void setCitationInfoEditMode(boolean citationInfoEditMode) {
        this.citationInfoEditMode = citationInfoEditMode;
    }

    /**
     * @param citationDocketEditMode the citationDocketEditMode to set
     */
    public void setCitationDocketEditMode(boolean citationDocketEditMode) {
        this.citationDocketEditMode = citationDocketEditMode;
    }

    /**
     * @param citationStatusEditMode the citationStatusEditMode to set
     */
    public void setCitationStatusEditMode(boolean citationStatusEditMode) {
        this.citationStatusEditMode = citationStatusEditMode;
    }

    /**
     * @return the currentCitationDocket
     */
    public CitationDocketRecord getCurrentCitationDocket() {
        return currentCitationDocket;
    }

    /**
     * @param currentCitationDocket the currentCitationDocket to set
     */
    public void setCurrentCitationDocket(CitationDocketRecord currentCitationDocket) {
        this.currentCitationDocket = currentCitationDocket;
    }

    /**
     * @return the docketNotesFormText
     */
    public String getDocketNotesFormText() {
        return docketNotesFormText;
    }

    /**
     * @return the statusNotesFormText
     */
    public String getStatusNotesFormText() {
        return statusNotesFormText;
    }

    /**
     * @param docketNotesFormText the docketNotesFormText to set
     */
    public void setDocketNotesFormText(String docketNotesFormText) {
        this.docketNotesFormText = docketNotesFormText;
    }

    /**
     * @param statusNotesFormText the statusNotesFormText to set
     */
    public void setStatusNotesFormText(String statusNotesFormText) {
        this.statusNotesFormText = statusNotesFormText;
    }

    /**
     * @return the currentCitationViolationLink
     */
    public CitationCodeViolationLink getCurrentCitationViolationLink() {
        return currentCitationViolationLink;
    }

    /**
     * @param currentCitationViolationLink the currentCitationViolationLink to set
     */
    public void setCurrentCitationViolationLink(CitationCodeViolationLink currentCitationViolationLink) {
        this.currentCitationViolationLink = currentCitationViolationLink;
    }

    /**
     * @return the formCitationViolationLinkNotes
     */
    public String getFormCitationViolationLinkNotes() {
        return formCitationViolationLinkNotes;
    }

    /**
     * @param formCitationViolationLinkNotes the formCitationViolationLinkNotes to set
     */
    public void setFormCitationViolationLinkNotes(String formCitationViolationLinkNotes) {
        this.formCitationViolationLinkNotes = formCitationViolationLinkNotes;
    }

    /**
     * @return the citationViolationStatusEnumList
     */
    public List<CitationViolationStatusEnum> getCitationViolationStatusEnumList() {
        return citationViolationStatusEnumList;
    }

    /**
     * @param citationViolationStatusEnumList the citationViolationStatusEnumList to set
     */
    public void setCitationViolationStatusEnumList(List<CitationViolationStatusEnum> citationViolationStatusEnumList) {
        this.citationViolationStatusEnumList = citationViolationStatusEnumList;
    }

    /**
     * @return the permissionAllowCitationOpenClose
     */
    public boolean isPermissionAllowCitationOpenClose() {
        return permissionAllowCitationOpenClose;
    }

    /**
     * @return the permissionAllowCitationUpdate
     */
    public boolean isPermissionAllowCitationUpdate() {
        return permissionAllowCitationUpdate;
    }

    /**
     * @return the citationAddWarningText
     */
    public String getCitationAddWarningText() {
        return citationAddWarningText;
    }

    /**
     * @return the showCitationAddWarning
     */
    public boolean isShowCitationAddWarning() {
        return showCitationAddWarning;
    }

    /**
     * @param showCitationAddWarning the showCitationAddWarning to set
     */
    public void setShowCitationAddWarning(boolean showCitationAddWarning) {
        this.showCitationAddWarning = showCitationAddWarning;
    }

    /**
     * @return the citingOfficerUserCandidateList
     */
    public List<User> getCitingOfficerUserCandidateList() {
        return citingOfficerUserCandidateList;
    }

    /**
     * @param citingOfficerUserCandidateList the citingOfficerUserCandidateList to set
     */
    public void setCitingOfficerUserCandidateList(List<User> citingOfficerUserCandidateList) {
        this.citingOfficerUserCandidateList = citingOfficerUserCandidateList;
    }

    /**
     * @return the currentPenalty
     */
    public CitationPenalty getCurrentPenalty() {
        return currentPenalty;
    }

    /**
     * @param currentPenalty the currentPenalty to set
     */
    public void setCurrentPenalty(CitationPenalty currentPenalty) {
        this.currentPenalty = currentPenalty;
    }

    /**
     * @return the currentPenaltyEditMode
     */
    public boolean isCurrentPenaltyEditMode() {
        return currentPenaltyEditMode;
    }

    /**
     * @param currentPenaltyEditMode the currentPenaltyEditMode to set
     */
    public void setCurrentPenaltyEditMode(boolean currentPenaltyEditMode) {
        this.currentPenaltyEditMode = currentPenaltyEditMode;
    }

    /**
     * @return the penaltyTypeCandidateList
     */
    public List<CitationPenaltyTypeEnum> getPenaltyTypeCandidateList() {
        return penaltyTypeCandidateList;
    }

    /**
     * @param penaltyTypeCandidateList the penaltyTypeCandidateList to set
     */
    public void setPenaltyTypeCandidateList(List<CitationPenaltyTypeEnum> penaltyTypeCandidateList) {
        this.penaltyTypeCandidateList = penaltyTypeCandidateList;
    }

    /**
     * @return the currentPenaltyHolder
     */
    public IfaceCitationPenaltyHolder getCurrentPenaltyHolder() {
        return currentPenaltyHolder;
    }

    /**
     * @param currentPenaltyHolder the currentPenaltyHolder to set
     */
    public void setCurrentPenaltyHolder(IfaceCitationPenaltyHolder currentPenaltyHolder) {
        this.currentPenaltyHolder = currentPenaltyHolder;
    }

    /**
     * @return the citationViolationUpdateBundle
     */
    public CitationCodeViolationLinkUpdateBundle getCitationViolationUpdateBundle() {
        return citationViolationUpdateBundle;
    }

    /**
     * @param citationViolationUpdateBundle the citationViolationUpdateBundle to set
     */
    public void setCitationViolationUpdateBundle(CitationCodeViolationLinkUpdateBundle citationViolationUpdateBundle) {
        this.citationViolationUpdateBundle = citationViolationUpdateBundle;
    }

    /**
     * @return the selectedCitationViolationBatchStatusEnum
     */
    public CitationViolationStatusEnum getSelectedCitationViolationBatchStatusEnum() {
        return selectedCitationViolationBatchStatusEnum;
    }

    /**
     * @param selectedCitationViolationBatchStatusEnum the selectedCitationViolationBatchStatusEnum to set
     */
    public void setSelectedCitationViolationBatchStatusEnum(CitationViolationStatusEnum selectedCitationViolationBatchStatusEnum) {
        this.selectedCitationViolationBatchStatusEnum = selectedCitationViolationBatchStatusEnum;
    }

    
    
   
}
