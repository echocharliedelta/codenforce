/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.coordinators.EventCoordinator;
import com.tcvcog.tcvce.coordinators.PropertyCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.coordinators.UserCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.EventException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.domain.SearchException;
import com.tcvcog.tcvce.domain.ViolationException;
import com.tcvcog.tcvce.entities.CEARProcessingRouteEnum;
import com.tcvcog.tcvce.entities.CEActionRequest;
import com.tcvcog.tcvce.entities.CECase;
import com.tcvcog.tcvce.entities.CECaseDataHeavy;
import com.tcvcog.tcvce.entities.CECaseOneClickTemplate;
import com.tcvcog.tcvce.entities.CodeSet;
import com.tcvcog.tcvce.entities.EventCategory;
import com.tcvcog.tcvce.entities.EventType;
import com.tcvcog.tcvce.entities.NoticeOfViolationTemplate;
import com.tcvcog.tcvce.entities.Property;
import com.tcvcog.tcvce.entities.PropertyUnit;
import com.tcvcog.tcvce.entities.User;
import com.tcvcog.tcvce.util.Constants;
import com.tcvcog.tcvce.util.MessageBuilderParams;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.faces.event.ActionEvent;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Backs the single operation of adding a CECase to the session's property
 * which is used in the property profile and the CEAR subsystem for request routing
 *  
 * @author Ellen Bascom of Apartment 31Y
 */
public class CECaseAddBB extends BackingBeanUtils {
    
    static final String CECASE_FORMRELOAD_PARAM = "cecase-add-containing-form";
    
    private CECase currentCECase;
    
    private List<EventCategory> ceCaseOriginiationEventCandidateList;
    
    private boolean ceCaseUnitAssociated;
    private PropertyUnit ceCaseUnitAssociation;
    
    private User selectedManagerUser;
    private List<User> managerUserCandidateList;
    private String ceCaseAddFormPathForUpdate;
    
    private List<CECaseOneClickTemplate> oneClickCaseList;
    
    private List<CECaseOneClickTemplate> oneClickCaseListForConfig;
    private boolean editModeOneClickTemplate;
    private boolean oneClickShowAllMunis;
    private CECaseOneClickTemplate currentOneClickTemplate;
    private List<EventCategory> eventCategoryCandidates;
    private CodeSet currentCodeSet;
    
    
    private List<NoticeOfViolationTemplate> templateListForConfig;
    
    private boolean permissionAllowCECaseCreate;
    
    @PostConstruct
    public void initBean(){
        System.out.println("CECaseAddBB.initBean()");
        CaseCoordinator cc = getCaseCoordinator();
        EventCoordinator ec = getEventCoordinator();
        UserCoordinator uc = getUserCoordinator();
        // for some reason we need a skeleton case here to not null out a view
        // but we really don't want this to be called before we want to make a new case
//        onCECaseAddInitButtonChange(null);

        configureCECaseAddPermissions();
        try {
            oneClickCaseList = cc.cecase_getCECaseOneClickTemplatesByMuni(getSessionBean().getSessMuni(), false);
            oneClickShowAllMunis = false;
            templateListForConfig = cc.nov_getNoticeOfViolationTemplateList(null);
            currentCodeSet = getSessionBean().getSessCodeSet();
            eventCategoryCandidates = ec.determinePermittedEventCategories(EventType.Origination, getSessionBean().getSessUser());
            managerUserCandidateList = uc.user_assembleUserListOfficerRequired(getSessionBean().getSessMuni(), false);
            refreshOneClickCaseLists();
        } catch (IntegrationException | BObStatusException | BlobException | AuthorizationException ex) {
            System.out.println(ex);
        } 
    }
    
    /**
     * Asks the case coordinator to check on user permissions for cecase add
     */
    private void configureCECaseAddPermissions(){
        CaseCoordinator cc = getCaseCoordinator();
         permissionAllowCECaseCreate = cc.permissionsCheckpointOpenCECase(getSessionBean().getSessProperty(), getSessionBean().getSessUser());
    }
    
    /**
     * Listener for user requests to start manager change process on case add
     * @param ev 
     */
    public void onChangeManagerInit(ActionEvent ev){
        System.out.println("CECaseAddBB.onChangeManagerInit");
        extractCaseInitFormComponentForReloadFromRequest();
    }
    
    /**
     * Listener for user requests to start manager change process on case add
     * @param ev 
     */
    public void onChangeManagerCommit(ActionEvent ev){
        if(currentCECase != null && selectedManagerUser != null){
            currentCECase.setCaseManager(selectedManagerUser);
            System.out.println("CECaseAddBB.onChangeManagerCommit | selectd new manager: " + selectedManagerUser.getUsername());
        }
    }
    
    
    /**
     * Extracts the component to update after creation or edit of an inspection
     * by the UI
     *
     */
    private void extractCaseInitFormComponentForReloadFromRequest() {
        ceCaseAddFormPathForUpdate = 
                  FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getRequestParameterMap()
                        .get(CECASE_FORMRELOAD_PARAM);
        System.out.println("CECaseBB.extractCaseInitFormComponentForReloadFromRequest | Component = " + ceCaseAddFormPathForUpdate);
    }
    
    
    
    /**
     * Listener for user requests to create a one-touch case from a template;
     * This method calls the much-too-fancy case creation method on the CaseCoordinator
     * 
     * @param template 
     */
    public void onCreateCaseFromOneClickButton(CECaseOneClickTemplate template){
        CaseCoordinator cc = getCaseCoordinator();
        PropertyCoordinator pc = getPropertyCoordinator();
        System.out.println("CECaseAddBB.onCreateCaseFromOneClickButton: " + template.getTitle());
        try {
            currentCECase = cc.cecase_createCECaseFromOneClickTemplate(getSessionBean().getSessProperty(), template, getSessionBean().getSessUser());
            // rebuild our property dh case list for refresh
            getFacesContext().addMessage(null,
                  new FacesMessage(FacesMessage.SEVERITY_INFO,
                          "Succesfully created one click case!", ""));
        } catch (BObStatusException | AuthorizationException | EventException | IntegrationException | SearchException | ViolationException | BlobException ex) {
            System.out.println(ex);
              getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
        // still refresh our case list even on an exception from above since we might have a partial case opened
        try {
            if(currentCECase != null){
                getSessionBean().setSessCECaseRefreshedList(pc.getPropertyDataHeavy(currentCECase.getParcelKey(), getSessionBean().getSessUser()).getCeCaseList());
                getSessionBean().setSessCECase(currentCECase);
            } else {
              getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Could not refresh case list due to error during one-click case creation process.", ""));
                
            }
        } catch (IntegrationException | BObStatusException | SearchException | AuthorizationException | BlobException | EventException ex) {
            System.out.println(ex);
              getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        } 
    }
    
    /**
     * Listener to start config process
     * @param ev 
     */
    public void onConfigureOneClickCases(ActionEvent ev){
        System.out.println("CECaseAddBB.onConfigureOneClickCases");
        if(oneClickCaseListForConfig != null && !oneClickCaseListForConfig.isEmpty()){
            currentOneClickTemplate = oneClickCaseListForConfig.get(0);
        }
    }
    
    /**
     * Listener for the start of the one click management interface
     * @param ev 
     */
    public void onManageOneClickCaseButtions(ActionEvent ev){
        refreshOneClickCaseLists();
    }
    
    /**
     * Gets a fresh copy of the one-touch cases
     */
    public void refreshOneClickCaseLists(){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            if(oneClickShowAllMunis){
                oneClickCaseListForConfig = cc.cecase_getCECaseOneClickTemplatesByMuni(null, false);
            } else {
                oneClickCaseListForConfig = cc.cecase_getCECaseOneClickTemplatesByMuni(getSessionBean().getSessMuni(), false);
            }
            oneClickCaseList = cc.cecase_getCECaseOneClickTemplatesByMuni(getSessionBean().getSessMuni(), false);
        } catch (IntegrationException | BObStatusException  | BlobException ex) {
            System.out.println(ex);
            
        } 
    }
    
    /**
     * Listener for user requests to cancel one click add edit operation
     * @param ev 
     */
    public void onOneClickEditAbort(ActionEvent ev){
        editModeOneClickTemplate = false;
    }
    
    /**
     * Starts the user viewing the passed in template
     * @param template 
     */
    public void onViewOneClickTemplate(CECaseOneClickTemplate template){
        currentOneClickTemplate = template;
    }
    
    /**
     * Listener to start the one-click process
     * @param ev 
     */
    public void onOneClickTemplateAddInit(ActionEvent ev){
        CaseCoordinator cc = getCaseCoordinator();
        currentOneClickTemplate = cc.cecase_getOneClickTemplateSkeleton(getSessionBean().getSessMuni());
        editModeOneClickTemplate = true;
    }
    
    /**
     * Listener to toggle the edit mode for one click cases
     * @param ev 
     */
    public void onToggleOneClickTemplateAddEditButtonChange(ActionEvent ev){
        System.out.println("CECaseAddBB.onToggleOneClickTemplateAddEditButtonChange");
        if(editModeOneClickTemplate){
            if(currentOneClickTemplate != null && currentOneClickTemplate.getOneClickID() == 0){
                onOneClickTemplateInsertCommit();
            } else {
                onOneClickTemplateUpdateCommit();
            }
            refreshOneClickCaseLists();
        }
        editModeOneClickTemplate = !editModeOneClickTemplate;
    }
    
    /**
     * Listener for one click template insertion commit
     */
    private void onOneClickTemplateInsertCommit() {  
        CaseCoordinator cc = getCaseCoordinator();
        try {
            int freshID = cc.cecase_insertCECaseOneClickTemplate(currentOneClickTemplate, getSessionBean().getSessUser());
            currentOneClickTemplate = cc.cecase_getCECaseOneClickTemplate(freshID);
            
            getFacesContext().addMessage(null,
                  new FacesMessage(FacesMessage.SEVERITY_INFO,
                          "Succesfully created one click case!", ""));
        } catch (BObStatusException | AuthorizationException | IntegrationException | BlobException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                  new FacesMessage(FacesMessage.SEVERITY_ERROR,
                          ex.getMessage(), ""));
        } 
    }
    
    /**
     * Listener to finalize the one click templates
     */
    private void onOneClickTemplateUpdateCommit(){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            cc.cecase_updateCECaseOneClickTemplate(currentOneClickTemplate, getSessionBean().getSessUser());
            getFacesContext().addMessage(null,
                  new FacesMessage(FacesMessage.SEVERITY_INFO,
                          "Succesfully update one click case!", ""));
        } catch (BObStatusException | AuthorizationException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                  new FacesMessage(FacesMessage.SEVERITY_ERROR,
                          ex.getMessage(), ""));
        } 
    
    }
    
    
    /**
     * Listener for the user to start the deac operation
     * @param template 
     */
    public void onOneClickTemplateDeactivateInit(CECaseOneClickTemplate template){
        System.out.println("CECaseAddBB.oneClickDeacInit");
        currentOneClickTemplate = template;
        
        
    }
    
    /**
     * Listener for user to complete deac process
     */
    public void onOneClickTemplateDeactivateCommit(){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            cc.cecase_deactivateCECaseOneClickTemplate(currentOneClickTemplate, getSessionBean().getSessUser());
            refreshOneClickCaseLists();
            if(oneClickCaseListForConfig != null && !oneClickCaseListForConfig.isEmpty()){
                currentOneClickTemplate = oneClickCaseListForConfig.get(0);
            }
        } catch (BObStatusException | AuthorizationException | IntegrationException ex) {
            System.out.println(ex);
        } 
    
    }
    
    /**
     * Listener for user requests to inject the chosen template into one-click
     * @param template 
     */
    public void onSelectTemplateForCurrentOneClick(NoticeOfViolationTemplate template){
        if(currentOneClickTemplate != null){
            currentOneClickTemplate.setNovTemplate(template);
        }
    }
    
    
   
    
     /**
     * Listener for user requests to start the ce case creation process;
     * I also build a list of eligible event categories for origination
     * @param ev
     */
    public void onCECaseAddInitButtonChange(ActionEvent ev){
        CaseCoordinator cc = getCaseCoordinator();
        EventCoordinator ec = getEventCoordinator();
        setCeCaseOriginiationEventCandidateList(ec.determinePermittedEventCategories(EventType.Origination, getSessionBean().getSessUser()));
        try {
            Property prop = getSessionBean().getSessProperty();
            if(prop != null){
                System.out.println("CECaseAddBB.onCECaseAddInitButtonChange | Prop Key" + prop.getParcelKey());
                currentCECase = cc.cecase_initCECase(getSessionBean().getSessProperty(), getSessionBean().getSessUser());
                
                // PROCESS CEAR opening if we have one
                if(getSessionBean().getActiveCEARProcessingRoute() != null && getSessionBean().getActiveCEARProcessingRoute() == CEARProcessingRouteEnum.ATTACH_TO_NEW_CASE){
                    // auto select CEAR if we're opening from that pathway
                    EventCategory cearOriginCat = ec.initEventCategory(
                    Integer.parseInt(getResourceBundle(
                            Constants.EVENT_CATEGORY_BUNDLE).getString("originiationByActionRequest")));
                    currentCECase.setOriginationEventCategory(cearOriginCat);
                    currentCECase.setCaseName("Created from action request");
                    // reset our flag
                    getSessionBean().setActiveCEARProcessingRoute(null);
                }
            } else {
                System.out.println("CECaseAddBB.onCECaseAddInitButtonChange | no session property on which to create case");
            }
        } catch (IntegrationException ex) {
            System.out.println(ex);
        }
    }
    
     /**
     * Listener method for creation of a new case
     * @return routing string: case profile
     */
    public String onAddNewCaseCommitButtonChange(){
        CaseCoordinator cc = getCaseCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        // check to see if we have an action request that needs to be connected
        // to this new case
        CEActionRequest cear = getSessionBean().getSessCEAR();
        // Check to see if the session's action request is in fact attached to our property
        if(cear != null && currentCECase != null){
            if(cear.getParcelKey() != currentCECase.getParcelKey()){
                // clear out the session CEAR on this operation
                cear = null;
            }
        }
        
        try {
            if(getCurrentCECase() == null){
                throw new BObStatusException("Cannot init new case with null case");
            }
            if(getCurrentCECase().getNotes() != null){
                MessageBuilderParams mbp = new MessageBuilderParams(
                        null, 
                        "Case Origination Note", 
                        null, 
                        getCurrentCECase().getNotes(), 
                        getSessionBean().getSessUser(), 
                        null);
                getCurrentCECase().setNotes(sc.appendNoteBlock(mbp));
            }
            
            if(isCeCaseUnitAssociated() && getCeCaseUnitAssociation() != null){
                getCurrentCECase().setPropertyUnitID(getCeCaseUnitAssociation().getUnitID());
            }
            Property prop = getSessionBean().getSessProperty();
            System.out.println("CECaseAddBB.onAddNewCaseCommitButtonChange | Prop " + prop.getAddress().getAddressPretty1Line());
            // DO THE INSERT
            int freshID = cc.cecase_insertNewCECase(getCurrentCECase(),
                    prop,
                    getSessionBean().getSessUser(), 
                    cear);  
            CECaseDataHeavy csedh = cc.cecase_assembleCECaseDataHeavy(cc.cecase_getCECase(freshID, getSessionBean().getSessUser()), 
                                            getSessionBean().getSessUser());
            getSessionBean().setSessCECase(csedh);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Successfully added case to property! Access the case from the list below.", ""));
            
            return getSessionBean().navigateToPageCorrespondingToObject(csedh);
        } catch (IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Integration Module error: Unable to add case to current property.",
                            "Best try again or note the error and complain to Eric."));
        } catch (BObStatusException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "A code enf action request was found in queue to be attached to this case. "
                            + "I couldn't do that, though, sorry..",
                            "Best try again or note the error and complain to Eric."));
            System.out.println(ex);

        } catch (ViolationException | EventException | SearchException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }

        //reload page on error
        return "";
    }

    /**
     * @return the currentCECase
     */
    public CECase getCurrentCECase() {
        return currentCECase;
    }

    /**
     * @param currentCECase the currentCECase to set
     */
    public void setCurrentCECase(CECase currentCECase) {
        this.currentCECase = currentCECase;
    }

    

    /**
     * @return the ceCaseOriginiationEventCandidateList
     */
    public List<EventCategory> getCeCaseOriginiationEventCandidateList() {
        return ceCaseOriginiationEventCandidateList;
    }

    /**
     * @param ceCaseOriginiationEventCandidateList the ceCaseOriginiationEventCandidateList to set
     */
    public void setCeCaseOriginiationEventCandidateList(List<EventCategory> ceCaseOriginiationEventCandidateList) {
        this.ceCaseOriginiationEventCandidateList = ceCaseOriginiationEventCandidateList;
    }

    /**
     * @return the ceCaseUnitAssociated
     */
    public boolean isCeCaseUnitAssociated() {
        return ceCaseUnitAssociated;
    }

    /**
     * @param ceCaseUnitAssociated the ceCaseUnitAssociated to set
     */
    public void setCeCaseUnitAssociated(boolean ceCaseUnitAssociated) {
        this.ceCaseUnitAssociated = ceCaseUnitAssociated;
    }

    /**
     * @return the ceCaseUnitAssociation
     */
    public PropertyUnit getCeCaseUnitAssociation() {
        return ceCaseUnitAssociation;
    }

    /**
     * @param ceCaseUnitAssociation the ceCaseUnitAssociation to set
     */
    public void setCeCaseUnitAssociation(PropertyUnit ceCaseUnitAssociation) {
        this.ceCaseUnitAssociation = ceCaseUnitAssociation;
    }

    /**
     * @return the permissionAllowCECaseCreate
     */
    public boolean isPermissionAllowCECaseCreate() {
        return permissionAllowCECaseCreate;
    }

    /**
     * @return the oneClickCaseList
     */
    public List<CECaseOneClickTemplate> getOneClickCaseList() {
        return oneClickCaseList;
    }

    /**
     * @param oneClickCaseList the oneClickCaseList to set
     */
    public void setOneClickCaseList(List<CECaseOneClickTemplate> oneClickCaseList) {
        this.oneClickCaseList = oneClickCaseList;
    }

    /**
     * @return the oneClickCaseListForConfig
     */
    public List<CECaseOneClickTemplate> getOneClickCaseListForConfig() {
        return oneClickCaseListForConfig;
    }

    /**
     * @param oneClickCaseListForConfig the oneClickCaseListForConfig to set
     */
    public void setOneClickCaseListForConfig(List<CECaseOneClickTemplate> oneClickCaseListForConfig) {
        this.oneClickCaseListForConfig = oneClickCaseListForConfig;
    }

    /**
     * @return the editModeOneClickTemplate
     */
    public boolean isEditModeOneClickTemplate() {
        return editModeOneClickTemplate;
    }

    /**
     * @param editModeOneClickTemplate the editModeOneClickTemplate to set
     */
    public void setEditModeOneClickTemplate(boolean editModeOneClickTemplate) {
        this.editModeOneClickTemplate = editModeOneClickTemplate;
    }

    /**
     * @return the currentOneClickTemplate
     */
    public CECaseOneClickTemplate getCurrentOneClickTemplate() {
        return currentOneClickTemplate;
    }

    /**
     * @param currentOneClickTemplate the currentOneClickTemplate to set
     */
    public void setCurrentOneClickTemplate(CECaseOneClickTemplate currentOneClickTemplate) {
        this.currentOneClickTemplate = currentOneClickTemplate;
    }

    /**
     * @return the oneClickShowAllMunis
     */
    public boolean isOneClickShowAllMunis() {
        return oneClickShowAllMunis;
    }

    /**
     * @param oneClickShowAllMunis the oneClickShowAllMunis to set
     */
    public void setOneClickShowAllMunis(boolean oneClickShowAllMunis) {
        this.oneClickShowAllMunis = oneClickShowAllMunis;
    }

    /**
     * @return the templateListForConfig
     */
    public List<NoticeOfViolationTemplate> getTemplateListForConfig() {
        return templateListForConfig;
    }

    /**
     * @param templateListForConfig the templateListForConfig to set
     */
    public void setTemplateListForConfig(List<NoticeOfViolationTemplate> templateListForConfig) {
        this.templateListForConfig = templateListForConfig;
    }

    /**
     * @return the eventCategoryCandidates
     */
    public List<EventCategory> getEventCategoryCandidates() {
        return eventCategoryCandidates;
    }

    /**
     * @param eventCategoryCandidates the eventCategoryCandidates to set
     */
    public void setEventCategoryCandidates(List<EventCategory> eventCategoryCandidates) {
        this.eventCategoryCandidates = eventCategoryCandidates;
    }

    /**
     * @return the currentCodeSet
     */
    public CodeSet getCurrentCodeSet() {
        return currentCodeSet;
    }

    /**
     * @param currentCodeSet the currentCodeSet to set
     */
    public void setCurrentCodeSet(CodeSet currentCodeSet) {
        this.currentCodeSet = currentCodeSet;
    }

    /**
     * @return the selectedManagerUser
     */
    public User getSelectedManagerUser() {
        return selectedManagerUser;
    }

    /**
     * @param selectedManagerUser the selectedManagerUser to set
     */
    public void setSelectedManagerUser(User selectedManagerUser) {
        this.selectedManagerUser = selectedManagerUser;
    }

    /**
     * @return the managerUserCandidateList
     */
    public List<User> getManagerUserCandidateList() {
        return managerUserCandidateList;
    }

    /**
     * @param managerUserCandidateList the managerUserCandidateList to set
     */
    public void setManagerUserCandidateList(List<User> managerUserCandidateList) {
        this.managerUserCandidateList = managerUserCandidateList;
    }

    /**
     * @return the ceCaseAddFormPathForUpdate
     */
    public String getCeCaseAddFormPathForUpdate() {
        return ceCaseAddFormPathForUpdate;
    }

    /**
     * @param ceCaseAddFormPathForUpdate the ceCaseAddFormPathForUpdate to set
     */
    public void setCeCaseAddFormPathForUpdate(String ceCaseAddFormPathForUpdate) {
        this.ceCaseAddFormPathForUpdate = ceCaseAddFormPathForUpdate;
    }

  
}
