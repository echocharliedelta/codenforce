/*
 * Copyright (C) 2019 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.*;
import com.tcvcog.tcvce.domain.*;
import com.tcvcog.tcvce.entities.*;
import com.tcvcog.tcvce.entities.occupancy.FieldInspectionLight;
import com.tcvcog.tcvce.entities.reports.ReportConfigCECase;
import com.tcvcog.tcvce.session.SessionBean;
import com.tcvcog.tcvce.util.*;
import com.tcvcog.tcvce.util.viewoptions.ViewOptionsActiveListsEnum;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;
import jakarta.faces.validator.ValidatorException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.jsoup.select.Collector;

/**
 * Primary backing bean for the Code Enforcement case 
 * @author sylvia
 */
public class CECaseBB
        extends BackingBeanUtils
        implements Serializable {
    

    private CECaseDataHeavy currentCase;
    private boolean editModeCurrentCase;
    private boolean editModeCurrentCaseManager;
    private boolean editModeCurrentCloseCase;
    private boolean editModeCurrentCaseRecord;
    
    private boolean permissionAllowCaseOpenEdit;
    private boolean permissionAllowCaseClosure;
    private boolean permissionAllowCaseDeactivate;
    
    private int formNOVFollowupDays;
    private boolean formCurrentCaseUnitAssociated;
    private PropertyUnit formSelectedUnit;
    
    
    private List<User> userManagerOptionList;
    private User userManagerSeletedForManagerChange;
    private List<BOBSource> bobSourceOptionList;
    
    private String formNoteText;
    
    private ReportConfigCECase reportCECase;
    
    /*******************************************************
     *              EVENTS
    /*******************************************************/
    private List<EventCnF> managedEventList;
    
    private List<EventCategory> closingEventCategoryList;
    private List<EventCategory> originationEventCategoryList;
    private EventCategory formCECaseOriginationEventCat;
    
    private EventCategory closingEventCategorySelected;
    protected int eventPersonIDForLookup;
    
    // REOPENING
    private OperationConfigurationCECaseReopen operationConfigBundleCaseReopen;
   
    
    /*******************************************************
     *              Violation collapse fields
     *              FROM ViolationBB
    /*******************************************************/
    private final static int MIN_FINDINGS_LENGTH_CHAR = 5;
    
    private CodeViolation currentViolation;
    private boolean editModeCurrentViolation;
    
    private boolean permissionAllowViolationAddUpdate;
    private boolean permissionAllowViolationExtendStipCompDate;
    
    private List<ViewOptionsActiveListsEnum> viewOptionList;
    private ViewOptionsActiveListsEnum selectedViewOption;
    
    private List<IntensityClass> severityList;
    private String formNoteTextViolation;
    
    private CodeSet currentCodeSet;
    
    private List<EnforceableCodeElement> filteredElementList;
    private List<EnforceableCodeElement> enfCodeElementList;
    private List<EnforceableCodeElement> selectedElementList;
    private Set<EnforceableCodeElement> enfCodeElementListPreviousSelected;
    private List<EnforceableCodeElement> enfCodeElementSelectedFilteredList;
    private EnforceableCodeElement currentEnforcableCodeElement;
    
    private List<CodeViolation> selectedViolationList;
    
    private boolean requireFindingsOnViolationAdd;
    private String ordinanceFilterText;
    private boolean ordinanceFilterSearchOrdFullText;
    private boolean ordinanceEntryActivateTree;
    private boolean ordinanceEntryActivateTable;
    
    /*******************************************************
     *              Violation batch stuff
    /*******************************************************/
    
    /**
     * Holds all the configuration params for a violation batch operation
     * which in SEPT 2024 are compliance, extension of dates, or nullification
     */
    private CodeViolationBatchOperationConfiguration violationOperationConfig;
    private String violationBatchOperationModeString;
    
    
    /*******************************************************
     *              BLOB Jazz
    /*******************************************************/
    
    private BlobLight currentBlob;
    private List<BlobLight> blobList;
    
    /**
     * Creates a new instance of CECaseSearchBB
     */
    public CECaseBB() {
    }

    /**
     * Sets up primary case list and aux lists for editing, and searching
     * This is a hybrid bean that coordinates both CECase search and 
     * list viewing functions
     */
    @PostConstruct
    public void initBean()  {
        EventCoordinator ec = getEventCoordinator();
        UserCoordinator uc = getUserCoordinator();
        SystemCoordinator sysCor = getSystemCoordinator();
        SessionBean sessionBean = getSessionBean();
        // signal event domain
        System.out.println("CECaseBB.initBean: Signaling CE event domain");
        getSessionEventConductor().setSessEventsPageEventDomainRequest(EventRealm.CODE_ENFORCEMENT);
        
        try {
            currentCase = getSessionBean().getSessCECase();
            if(currentCase != null){
                reloadCurrentCase();

                userManagerOptionList = uc.user_assembleUserListOfficerRequired(getSessionBean().getSessMuni(), false); 
                bobSourceOptionList = sysCor.getBobSourceListComplete();
                severityList = sysCor.getIntensitySchemaWithClasses(
                        getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("intensityschema_violationseverity"), getSessionBean().getSessMuni())
                        .getClassList();
                originationEventCategoryList = ec.getEventCategeryList(EventType.Origination);
                closingEventCategoryList = ec.getEventCategeryList(EventType.Closing);
                getSessionBean().setSessBlobHolder(currentCase);
                enfCodeElementListPreviousSelected = new HashSet<>();
                enfCodeElementSelectedFilteredList = new ArrayList<>();
            }
            
        } catch (IntegrationException | BObStatusException | AuthorizationException  ex) {
            System.out.println(ex);
        }
     
        formNOVFollowupDays = getSessionBean().getSessMuni().getProfile().getNovDefaultDaysForFollowup();
        if(formNOVFollowupDays == 0){
            formNOVFollowupDays = 20;
        }
        
        // VIOLATION STUFF FROM VIOLATIONBB
        currentViolation = getSessionBean().getSessCodeViolation();
        
        if (currentViolation == null) {
            if (currentCase != null && currentCase.getViolationList() != null && !currentCase.getViolationList().isEmpty()) {
                currentViolation = currentCase.getViolationList().get(0);
            }
        }
        getSessionBean().setSessHumanListRefreshedList(null);
        
        
        enfCodeElementList = sessionBean.getSessCodeSet().getEnfCodeElementList();
        
            // ui defaults
        toggleAllEditModesToFalse();
        ordinanceFilterSearchOrdFullText = false;
        configureCECaseOperationPermissions();
        
    }
    
    /**
     * Sets boolean flags for UI elements based on CaseCoordinator's permissions logic
     * rules
     */
    private void configureCECaseOperationPermissions(){
        CaseCoordinator cc = getCaseCoordinator();
        PropertyCoordinator pc = getPropertyCoordinator();
        if(currentCase != null){
            try {
                permissionAllowCaseOpenEdit = cc.permissionsCheckpointOpenCECase(pc.getProperty(currentCase.getParcelKey()), getSessionBean().getSessUser());
            } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
                System.out.println(ex);
            } 
            permissionAllowCaseClosure = cc.permissionsCheckpointCloseReopenCECase(getSessionBean().getSessUser());
            permissionAllowViolationAddUpdate = cc.permissionsCheckpointViolationAdd(getSessionBean().getSessUser());
            permissionAllowViolationExtendStipCompDate = cc.permissionsCheckpointViolationExtendStipCompDate(getSessionBean().getSessUser());
            permissionAllowCaseDeactivate = cc.permissionCheckpointDeactivateCECase(currentCase, getSessionBean().getSessUser());
        }
    }
    
    /**
     * Turns all page edit modes to false
     */
    private void toggleAllEditModesToFalse(){
        editModeCurrentCase = false;
        editModeCurrentCaseManager = false;
        editModeCurrentCaseRecord = false;
        editModeCurrentViolation = false;
        editModeCurrentCloseCase = false;
    }
    
    /**
     * Sets the session event
     * @param ev 
     */
    public void injectSessionEvent(EventCnF ev){
        if(ev != null){
            getSessionEventConductor().setSessEvent(ev);
        }
        
    }

    /*******************************************************
     *              Case Details form listeners
    /*******************************************************/
    
    /**
     * Listener for user requests to start case updates or end them
     * @param ev 
     */
    public void onToggleCECaseEditButtonChange(ActionEvent ev){
        CaseCoordinator cc = getCaseCoordinator();
        PropertyCoordinator pc = getPropertyCoordinator();
        
        System.out.println("CECaseBB.onToggleCECaseEditButtonChange | top of method case edit mode: " + editModeCurrentCase);
        
        // if we don't have a case, then don't do anything.
        if(currentCase == null){
            return;
        }
        try {
            if(editModeCurrentCase){
                // We're writing edits
                if(formCurrentCaseUnitAssociated && formSelectedUnit != null){
                    currentCase.setPropertyUnitID(formSelectedUnit.getUnitID());
                }
                    System.out.println("CECaseBB.onToggleCECaseEditButtonChange | sending meta data " + currentCase.getCaseName());
                    cc.cecase_updateCECaseMetadata(currentCase, getSessionBean().getSessUser());
                    cc.cecase_checkForAndUpdateCaseOriginationEventCategory(currentCase, formCECaseOriginationEventCat, getSessionBean().getSessUser());
                    reloadCurrentCase();
                    getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                            "Case update success!", ""));

            // prepare the form for EDITS
            } else {
                if(currentCase.getUnitID() != 0){
                    formSelectedUnit = pc.getPropertyUnit(currentCase.getUnitID());
                    formCurrentCaseUnitAssociated = true;
                } else {
                    formCurrentCaseUnitAssociated = false;
                }

                if(currentCase.getOriginationEvent() != null){
                    formCECaseOriginationEventCat = currentCase.getOriginationEvent().getCategory();
                }
            }
        } catch (BObStatusException | IntegrationException | EventException ex) {
            System.out.println(ex);
              getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    "Fatal error on case update!", ""));
        } 
        editModeCurrentCase = !editModeCurrentCase;
    }

    /**
     * Listener for user requests to abort case record update
     * @param ev 
     */
    public void onEditCECaseAbort(ActionEvent ev){
        editModeCurrentCase = false;
        System.out.println("CECaseBB.onEditCECaseAbort | edit mode edit case: " + editModeCurrentCase);
    }
    
    /**
     * Listener for toggling edit mode on and then saving manager edits
     * @param ev 
     */
    public void onToggleCECaseManagerEdit(ActionEvent ev){
        EventCoordinator ec = getEventCoordinator();
        CaseCoordinator cc = getCaseCoordinator();
        if(editModeCurrentCaseManager && userManagerSeletedForManagerChange != null){
            try {
                System.out.println("CECaseBB.onToggleCECaseManagerEdit | new manager: " + userManagerSeletedForManagerChange.getUsername());
                cc.cecase_updateCECaseManagerAndLogEvent(currentCase, userManagerSeletedForManagerChange, getSessionBean().getSessUser(), getSessionBean().getSessMuni().getProfile());
                reloadCurrentCase();
                getSessionEventConductor().setSessEventListForRefreshUptake(ec.getEventList(currentCase));
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                        "Manager edit complete", ""));
            } catch (BObStatusException | IntegrationException | EventException | AuthorizationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                        "Fatal error updating case manager. Please log a ticket.", ""));
            } 
        }
        editModeCurrentCaseManager = !editModeCurrentCaseManager;
    }
    
    /**
     * Listener for cancellation of manager edit
     * @param ev 
     */
    public void onEditManagerAbort(ActionEvent ev){
        
        editModeCurrentCaseManager = false;
        System.out.println("CECaseBB.onEditManagerAbort | edit mode edit manager: " + editModeCurrentCaseManager);
         getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                        "Aborted: Manager edit operation.", ""));
    }
    
    /**
     * Listener for toggling edit mode on and then saving manager edits
     * @param ev 
     */
    public void onToggleEditCECaseRecordStatus(ActionEvent ev){
        
        CaseCoordinator cc = getCaseCoordinator();
        if(editModeCurrentCaseRecord){
            try {
                cc.cecase_updateCECaseMetadata(currentCase, getSessionBean().getSessUser());
                reloadCurrentCase();
                  getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                        "Record edit complete", ""));
            } catch (BObStatusException | IntegrationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                        "Fatal error udating case manager. Please log a ticket.", ""));
            } 
        }
        editModeCurrentCaseRecord = !editModeCurrentCaseRecord;
    }
    
    /**
     * Listener for cancellation of manager edit
     * @param ev 
     */
    public void onEditRecordAbort(ActionEvent ev){
        
        editModeCurrentCaseRecord = false;
        getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                        "Aborted: Case record edit operation.", ""));
    }
    
    /**
     * provides the ID of the component for accessory UIs to refresh so the CE case is
     * refreshed. Updating this component will call getReloadCECaseTrigger which, if not null
     * triggers a reload.
     * 
     * As of May 2022, I think this is defunct. Use managed fields instead
     * @return 
     */
    public String getReloadCECaseComponentIDToUpdate(){
        return "case-refresh-trigger-form";
    }
    
    /**
     * Gets a new cecase data heavy
     */
    public void reloadCurrentCase(){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            if(currentCase != null){
                cc.cecase_forceCaseCacheDump(currentCase);
                currentCase = cc.cecase_assembleCECaseDataHeavy(cc.cecase_getCECase(currentCase.getCaseID(), getSessionBean().getSessUser()), getSessionBean().getSessUser());
                System.out.println("CECaseBB.reloadCurrentCase | notice size:" + currentCase.getNoticeList().size());
                getSessionBean().setSessCECase(currentCase);
                getSessionBean().setNoteholderRefreshTimestampTrigger(LocalDateTime.now());
            } else {
                System.out.println("CECaseBB.reloadCurrentCase | null currentCase");
            }
           
        } catch (BObStatusException  | IntegrationException | SearchException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                "Could not refresh current case", ""));
        }
    }
    
    /**
     * Fetches a fresh copy of the current violation
     */
    public void reloadCurrentViolation(){
        CaseCoordinator cc = getCaseCoordinator();
        if(currentViolation != null){
            try {
                currentViolation = cc.violation_getCodeViolation(currentViolation.getViolationID());
            } catch (IntegrationException | BObStatusException  ex) {
                System.out.println(ex);
            } 
        }
    }
    
    /**
     * Listener for UI requests to directly reload case
     * @param ev 
     */
    public void reloadCaseListener(ActionEvent ev){
        reloadCurrentCase();
    }
    
   
    /**
     * Listener to start case deac process
     * @param ev 
     */
     public void onCaseDeactivatetInitButtonChange(ActionEvent ev){
         System.out.println("CECaseBB.onCaseDeactivatetInitButtonChange");
     }
     
     /**
      * listeners for users who want to stop the process of case deac immediately
      * @param ev 
      */
     public void onCaseDeactivateAbortButtonChange(ActionEvent ev){
         System.out.println("CECaseBB.onCaseDeactivateAbortButtonChange");
     }
     
    /**
     * Listener for user requests to deactivate a cecase
     * @param ev
     * @return  go to dash
     */
    public String onCaseDeactivateCommitButtonChange(){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            cc.cecase_deactivateCase(currentCase, getSessionBean().getSessUser());
            
            
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                ex.getMessage(), ""));
        }
        return "missionControl";
    }
    
    /**
     * Listener for user requests to start the case force close operation
     * @param ev 
     */
    public void onCaseCloseInitButtonChange(ActionEvent ev){
        editModeCurrentCloseCase = true;
        if(currentCase != null){
            currentCase.setClosingDate(LocalDateTime.now());
        }
        System.out.println("CECaseBB.onCaseCloseInitButtonChange | edit mode case close: " + editModeCurrentCloseCase);
    }
    
    /**
     * Listener for user requests to 
     * @return 
     */
    public String onCaseForceCloseCommitButtonChnage(){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            cc.cecase_forceclose(currentCase, closingEventCategorySelected, getSessionBean().getSessUser());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                "Case closed and unresolved violations nullified.", ""));
        } catch (IntegrationException | BObStatusException | EventException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                ex.getMessage(), ""));
        }
        return "ceCaseProfile";
    }
    
    /**
     * Listener for user requests to abort the case closure operation
     * @param ev 
     */
    public void onCaseCloseAbortLinkClick(ActionEvent ev){
        System.out.println("CECaseBB.onCaseCloseAbortLinkClick");
        editModeCurrentCloseCase = false;
        if(currentCase != null){
            currentCase.setClosingDate(null);
        }
    }
    
    /**
     * Listener for user requests to start the re-opening process.
     * This could occur because a citation is appealed by the defendant or during
     * or because a case review reveals more work  must be done on the case.
     * @param ev 
     */
    public void onCaseReopenInit(ActionEvent ev){
        try {
            CaseCoordinator cc = getCaseCoordinator();
            System.out.println("CECaseBB.onCaseReopenInit");
            operationConfigBundleCaseReopen = cc.cecase_caseReopenInit(currentCase, getSessionBean().getSessUser());
        } catch (IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                ex.getMessage(), ""));
        }
        
    }
    
    
    /**
     * Commits and refreshes the current cecase
     * @param ev 
     */
    public void onCaseReopenCommit(ActionEvent ev){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            System.out.println("CECaseBB.onCaseReopenCommit");
            operationConfigBundleCaseReopen = cc.cecase_CaseReopenCommit(operationConfigBundleCaseReopen);
        } catch (BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                ex.getMessage(), ""));
        }
        
        
    }
    
    /**
     * When users are done with the reopen, they'll activate this and reload the
     * case and page.
     * @return 
     */
    public String onCaseReopenDoneCommenceReload(){
        try {
            System.out.println("CECaseBB.onCaseReopenDoneCommenceReload");
            operationConfigBundleCaseReopen = null;
            reloadCurrentCase();
            return getSessionBean().navigateToPageCorrespondingToObject(currentCase);
        } catch (BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            
        } 
        return "";
    }
    
    /**
     * Listener for abort requests during reopen config
     * @param ev 
     */
    public void onCaseReopenAbort(ActionEvent ev){
        System.out.println("CECaseBB.onCaseReopenDoneCommenceAbort");
        operationConfigBundleCaseReopen = null;
    }
    
    
    public void onHowToNextStepButtonChange(ActionEvent ev){
        System.out.println("ceCaseBB.onHowToNextStepButtonChange");
        
    }
    
    
    /**
     * Special listener for field inspection lists from the CECase
     * I check the session bean for an updated list each call
     * @return a list, perhaps with some freshly written inspections
     */
    public List<FieldInspectionLight> getManagedCECaseFieldInspectionList(){
        List<FieldInspectionLight> finlist = getSessionInspectionConductor().getSessFieldInspectionListForRefresh();
        if(finlist != null){
            currentCase.setInspectionList(finlist);
            getSessionInspectionConductor().setSessFieldInspectionListForRefresh(null);
            reloadCurrentCase();
        } else {
            return currentCase.getInspectionList();
        }
        return finlist;
        
    }
    
    /*******************************************************
    /*******************************************************
     **              BLOBS                               **
    /*******************************************************/
    /*******************************************************/
    
    /**
     * Special getter wrapper around the CECase blob list
     * that checks the session for a new blob list
     * that may have been injected by the BlobUtilitiesBB
     * 
     * @return the CECases's updated blob list
     */
    public List<BlobLight> getManagedBlobLightListFromCECase(){
        System.out.println("ceCaseBB.getBlobLightListFromCECase ");
        List<BlobLight> sessBlobListForUpdate = getSessionBean().getSessBlobLightListForRefreshUptake();
        // Make sure this list is, in fact, for our current case
        if(sessBlobListForUpdate != null 
                && currentCase != null 
                && getSessionBean().getSessBlobLightListHolderForRefreshUptake() != null
                && getSessionBean().getSessBlobLightListHolderForRefreshUptake().getBlobLinkEnum() == BlobLinkEnum.CE_CASE
                && getSessionBean().getSessBlobLightListHolderForRefreshUptake().getParentObjectID() == currentCase.getCaseID()){
            System.out.println("ceCaseBB.getBlobLightListFromCECase | found non-null session blob list for uptake: " + sessBlobListForUpdate.size());
            currentCase.setBlobList(sessBlobListForUpdate);
            // clear session since we have the new list
            getSessionBean().setSessBlobLightListForRefreshUptake(null);
            return sessBlobListForUpdate;
        } else {
            if(currentCase.getBlobList() != null){
                return currentCase.getBlobList();
            } else {
                return new ArrayList<>();
            }
        }
    }

    /**
     * Listener for requests to go view the property profile of a property associated
     * with the given case
     * @return 
     */
    public String exploreProperty(){
        try {
            getSessionBean().setSessProperty(currentCase.getParcelKey());
            return getSessionBean().navigateToPageCorrespondingToObject(getSessionBean().getSessProperty());
        } catch (IntegrationException | BObStatusException | BlobException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Could not load property data heavy; reloaded page", ""));
        }
        return "";
        
    }
    
   
    
  
  
    
    /**
     * Catch all for cancellation requests
     * @param ev 
     */
    public void onOperationCancelButtonChange(ActionEvent ev){
        //  nothing to do here yet
    }
    
    /*******************************************************
    /*******************************************************
     **              REPORTING                            **
    /*******************************************************/
      /**
     * Listener for user requests to build a code enforcement case report
     * 
     * @return 
     */
    public String generateReportCECase() {
        System.out.println("ceCaseBB.generateReportCECase");
        CaseCoordinator cc = getCaseCoordinator();

        reportCECase.setCreator(getSessionBean().getSessUser());
        reportCECase.setMuni(getSessionBean().getSessMuni());
        reportCECase.setGenerationTimestamp(LocalDateTime.now());
        reportCECase.setCse(currentCase);
        
        try {
            setReportCECase(cc.report_prepareCECaseReport(reportCECase, getSessionBean().getSessUser()));
        } catch (IntegrationException | BObStatusException | SearchException | BlobException ex) {
            System.out.println(ex);
                getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Could not generate report, sorry!", ""));
        }

        getSessionBean().setReportConfigCECase(getReportCECase());
        // this is for use by the report header to have a super class with only
        // the basic info. reportingBB exposes it to the faces page
        getSessionBean().setSessReport(getReportCECase());
        // force our reportingBB to choose the right bundle
        getSessionBean().setReportConfigCECaseList(null);

        return "reportCECase";
    }

  /**
     * Listener for the report initiation process
     * @param ev 
     */
    public void prepareReportCECase(ActionEvent ev) {
        CaseCoordinator cc = getCaseCoordinator();
        setReportCECase(cc.report_getDefaultReportConfigCECase(currentCase, getSessionBean().getSessUser()));
        System.out.println("CaseProfileBB.prepareReportCECase | reportConfigOb: " + getReportCECase());

    }
    
    /*******************************************************
    /*******************************************************
     **              Citation crossover                     **
    /*******************************************************/
   
     /**
     * Special getter that refreshes violation list if the session bean signals to do so
     * @return the immediately updated list of violations on the current case
     */
    public List<Citation> getManagedCitationList(){
        if(currentCase != null){
            if(getSessionBean().getSessCECaseRefreshTriggerCitations()!= null){
                reloadCurrentCase();
                System.out.println("CECaseBB.getManagedCitationList | refresh trigger found; refreshed case");
                getSessionBean().setSessCECaseRefreshTriggerCitations(null);
            } 
            return currentCase.getCitationList();
            
        } else {
            return new ArrayList<>();
        }
        
    }
    
    /*******************************************************
    /*******************************************************
     **              Violation processing                 **
    /*******************************************************/
    
    /**
     * Listener for adding an ordinance from a tree to the master list
     * @param ece 
     */
    public void addEnforceableCodeElementToSelectedElementList(EnforceableCodeElement ece){
        if(selectedElementList != null){
            selectedElementList.add(ece);
        }
    }
   
    /**
     * Listener to remove a violation from the list of violated ords
     * @param ece 
     */
    public void removeEnforcableCodeElementFromList(EnforceableCodeElement ece){
        if(selectedElementList != null){
            selectedElementList.remove(ece);
        }
    }
    
    /**
     * Special getter that refreshes violation list if the session bean signals to do so
     * @return the immediately updated list of violations on the current case
     */
    public List<CodeViolationStatusHeavy> getManagedViolationList(){
        if(currentCase != null){
            if(getSessionBean().getSessCECaseRefreshTriggerViolations()!= null){
                reloadCurrentCase();
                System.out.println("CECaseBB.getManagedViolationList | refresh trigger found; refreshed cecase");
                getSessionBean().setSessCECaseRefreshTriggerViolations(null);
            } 
            return currentCase.getViolationList();
            
        } else {
            return new ArrayList<>();
        }
    }
    
    /**
     * Listener for users to view a violation
     * @param cv 
     */
    public void onViolationView(CodeViolation cv){
        currentViolation = cv;
    }
    
    /**
     * User toggling violation edit mode
     * @param ev 
     */
    public void onToggleViolationUpdateButtonPress(ActionEvent ev){
        System.out.println("CECaseBB.onToggleViolationUpdateButtonPress | mode in " + editModeCurrentViolation);
        if(editModeCurrentViolation){
            try {
                onViolationUpdateCommitButtonChange();
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
            } 
        } else {
            
        }
        editModeCurrentViolation = !editModeCurrentViolation;
    }
    
    /**
     * Listener for user abort requests
     * @param ev 
     */
    public void onViolationUpdateAbort(ActionEvent ev){
        editModeCurrentViolation = false;   
        System.out.println("CECaseBB.onViolationUpdateAbort | edit mode is now: " + editModeCurrentViolation);
    }
    
    /**
     * Listener for user requests to view the full text of a given ECE before violation creation
     * @param ece 
     */
    public void onViewOrdinanceText(EnforceableCodeElement ece){
        currentEnforcableCodeElement = ece;
    }
    
     /**
     * Listener for user reqeusts to commit updates to a codeViolation
     *
     * @throws IntegrationException
     * @throws BObStatusException
     */
    public void onViolationUpdateCommitButtonChange() throws IntegrationException, BObStatusException {
        CaseCoordinator cc = getCaseCoordinator();
        EventCoordinator eventCoordinator = getEventCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        CodeCoordinator codec = getCodeCoordinator();
        EventCategory ec = eventCoordinator.initEventCategory(
                Integer.parseInt(getResourceBundle(Constants.EVENT_CATEGORY_BUNDLE).getString("updateViolationEventCategoryID")));

        try {
            if(currentCase != null && currentViolation != null){
                cc.violation_updateCodeViolation(currentCase, currentViolation, getSessionBean().getSessUser());
              
                // if update succeeds without throwing an error, then generate an
                // update violation event
                // TODO: Rewire this to work with new event processing cycle
    //             eventCoordinator.generateAndInsertCodeViolationUpdateEvent(getCurrentCase(), currentViolation, event);
                reloadCurrentCase();
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Success! Violation updated, ID: " + currentViolation.getViolationID(), ""));
            }
        } catch (IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to edit violation in the database",
                            "This is a system-level error that msut be corrected by an administrator, Sorry!"));

        } catch (ViolationException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            ex.getMessage(), "Please revise the stipulated compliance date"));
        }
    }

    
    
    /**
     * Listener for user requests to start nuke operation
     * @param ev
     */
    public void onViolationNukeInitButtonChange(ActionEvent ev){
        
    }
    
       /**
     * Listener for user requests to remove a violation from a case
     *
     * @param ev
     */
    public void onViolationRemoveCommitButtonChange(ActionEvent ev) {
        CaseCoordinator cc = getCaseCoordinator();
        try {
            cc.violation_deactivateCodeViolation(getCurrentViolation(), getSessionBean().getSessUser());
            reloadCurrentCase();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Violation deactivated with ID: " + currentViolation.getViolationID(), null));
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), null));
        }
    }
    
    
   
     /**
     * Listener for user selection of a violation from the code set violation
     * table
     *
     * @param ece
     */
     private CodeViolation injectOrdinanceIntoViolation(EnforceableCodeElement ece) throws BObStatusException {
        CaseCoordinator cc = getCaseCoordinator();
        CodeViolation cv = null;
        try {
            cv = cc.violation_injectOrdinance(cc.violation_getCodeViolationSkeleton(currentCase), ece);
        } catch (IntegrationException ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                    ex.getMessage(), ""));
        }
        return cv;
    }
     
     
     
     
     /**
      * Listener to change UI to view in tree mode for violation select
      * @param ev 
      */
     public void onOrdinanceSelectSetForTreeView(ActionEvent ev){
         ordinanceEntryActivateTree = true;
         ordinanceEntryActivateTable = false;
     }
     
     // Listener to change ui view to table mode for violation select
     public void onOrdinanceSelectSetForTableView(ActionEvent ev){
         ordinanceEntryActivateTree = false;
         ordinanceEntryActivateTable = true;
         
     }
     
    /**
     * Listener for user requests to start the violation add process
     * 
     * @param ev
     */
    public void onViolationAddInit(ActionEvent ev) {
        // setup our lists
        selectedElementList = new ArrayList<>();
        selectedViolationList = new ArrayList<>();
        ordinanceFilterText = "";
        if (!enfCodeElementList.isEmpty())
        {
            filteredElementList = enfCodeElementList;
        }
        enfCodeElementListPreviousSelected.clear();
        enfCodeElementSelectedFilteredList.clear();
        //setup our UI
        onOrdinanceSelectSetForTreeView(ev);
        System.out.println("OnViolationAddInit: Selected Element list Size: " + selectedElementList.size());
    }
    
    
    /**
    * Listener for user indication that ordinances have been selected
     * and we're ready to configure the violation of those ordinances
     * Iterates over each of the selected elements and injects the ordinance
     * and builds a violation list for configuration.
     * 
     * As of JAN 2025 we also will write these violations to the 
     * db so the details screen can be refreshed without losing data.
     * 
    */
    public void onOrdinanceSelectionCompleteButtonChange(){
        compareSelectedList();
        setSelectedFilteredList();
        if(!selectedElementList.isEmpty()){
            for(EnforceableCodeElement ece: selectedElementList){
                try{
                    CodeViolation cv = injectOrdinanceIntoViolation(ece);
                    selectedViolationList.add(cv);
                } catch (BObStatusException ex) {
                    getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                    ex.getMessage(), ""));
                }
            }
            // Added initial save pathway Jan 2025 during canned findings
            // so the details screen can be saved and refreshed during 
            // canned finding management
            onViolationConnectToCECase(null);
            requireFindingsOnViolationAdd = false;
       } else {
            getFacesContext().validationFailed();
             getFacesContext().addMessage(null,
                     new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "At least one ordinance is required!", ""));
        }
    }
    
    /**
     * Listener to abort violation selection
     * @param ev 
     */
    public void onOrdinanceSelectionAbort(ActionEvent ev){
        if(selectedElementList != null && !selectedElementList.isEmpty()){
            selectedElementList.clear();
        }
    }
    
    /**
     * Second step after user has completed selection of code violations to
     * attach to the current CECase: saving into the database and
     *
     * @param ev
    */
    public void onViolationConnectToCECase(ActionEvent ev) {

        CaseCoordinator cc = getCaseCoordinator();
        UserAuthorized ua = getSessionBean().getSessUser();
        List<Integer> freshViolationIDList = new ArrayList<>();
        
        // Iterate over all the violations and add them to our case
        if(!selectedViolationList.isEmpty()){
            for(CodeViolation cv: selectedViolationList){
                try {
                    freshViolationIDList.add(cc.violation_attachViolationToCase(cv, currentCase, ua));
                    getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO,
                                    "Success! Violation attached to case.", ""));
                    System.out.println("ceCaseBB.onViolationAddCommmitButtonChange | attached violation to case");
                    // rebuild selected violation list with violationIDs intact
                    selectedViolationList = cc.violation_getCodeViolations(freshViolationIDList);
                    Collections.sort(selectedViolationList);
                } catch (IntegrationException | SearchException | BObStatusException | EventException | ViolationException ex) {
                    System.out.println(ex);
                    getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                    ex.getMessage(), ""));
                }
            }
        }
    }
    
    /**
     * Canned findings require that we can save and reload the violations being added 
     * to a cecase to reflect changes made to the ece's underlying canned ords.
     */
    public void onSaveAndRefreshSelectedViolationList(){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            if(selectedViolationList != null && !selectedViolationList.isEmpty()){
                // if we're on final submit, then enforce findings required
                if(requireFindingsOnViolationAdd){
                    if(selectedViolationList != null && !selectedViolationList.isEmpty()){
                        for(CodeViolation cv: selectedViolationList){
                            if(cv.getFindings() == null || cv.getFindings().length() < MIN_FINDINGS_LENGTH_CHAR){
                                String minFindingAlert = "At leat 5 characters of findings are required for each violation";
                                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                       minFindingAlert, "");
                                getFacesContext().addMessage(null,msg);
                                throw new ValidatorException(msg);
                                
                            }
                        }
                    }
                }
               cc.violation_updateCodeViolationList(currentCase, selectedViolationList, getSessionBean().getSessUser());
               selectedViolationList = cc.violation_refreshCodeViolationList(selectedViolationList);
            }
        } catch (BObStatusException | IntegrationException | ViolationException  ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                    ex.getMessage(), ""));
        } 
    }
    
    /**
     * Last step in adding a set of selected violations to a CECase which is
     * just an update of these violation's members and a case reload.
     * @param ev 
     */
    public void onCommitAttachmentOfSelectedViolationsToCECase(ActionEvent ev){
        CaseCoordinator cc = getCaseCoordinator();
        onSaveAndRefreshSelectedViolationList();
        reloadCurrentCase();
    }
    
    
    
    
    
        /**
     * Listener for user requests to abort violation add process
     * @param ev
     */
    public void onViolationAddAbortButtonChange(ActionEvent ev){
        CaseCoordinator cc = getCaseCoordinator();
        System.out.println("CECaseBB.onViolationAddAbortButtonChange");  //reload our current page with dialogs closed
        try {
            cc.violation_deactivateCodeViolations(selectedViolationList, getSessionBean().getSessUser());
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            ex.getMessage(), "Unable to gracefully abort violation operation"));
        } 
        
    }

    
     /**
     * Listener for user reqeusts to commit updates to a codeViolation
     *
     * @return
     * @throws IntegrationException
     * @throws BObStatusException
     */
    public String onViolationUpdateDORCommitButtonChange() throws IntegrationException, BObStatusException {
        CaseCoordinator cc = getCaseCoordinator();
        EventCoordinator eventCoordinator = getEventCoordinator();
        SystemCoordinator sc = getSystemCoordinator();

        EventCategory ec = eventCoordinator.initEventCategory(
                Integer.parseInt(getResourceBundle(Constants.EVENT_CATEGORY_BUNDLE).getString("updateViolationEventCategoryID")));

        try {

            cc.violation_updateCodeViolation(currentCase, getCurrentViolation(), getSessionBean().getSessUser());

            // if update succeeds without throwing an error, then generate an
            // update violation event
            // TODO: Rewire this to work with new event processing cycle
//             eventCoordinator.generateAndInsertCodeViolationUpdateEvent(getCurrentCase(), currentViolation, event);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Success! Violation updated and notice event generated", ""));
        } catch (IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to edit violation in the database",
                            "This is a system-level error that msut be corrected by an administrator, Sorry!"));

        } catch (ViolationException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            ex.getMessage(), "Please revise the stipulated compliance date"));

        }
        return "ceCaseProfile";
    }
    
    /**
     * Internal tool for updating default findings on a violation
     * 
     * @param cv 
     */
    private void makeViolationFindingsDefault(CodeViolation cv){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            cc.violation_makeFindingsDefaultInCodebook(cv, getSessionBean().getSessUser() , false);
            getSessionBean().refreshSessionCodeBook();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Updated default violation findings",
                            ""));
        } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to make violation findings default " + ex.getMessage(),
                            ""));
            System.out.println(ex);
        } 
    }
    
    
    
    /**
     * Listener for commencement of note writing process
     *
     * @param ev
     */
    public void onViolationNotesInitButtonChange(ActionEvent ev) {
        formNoteTextViolation = "";

    }

    /**
     * Special wrapper getter method for blobs on a code violation that checks session
     * for updates on each getter call
     * 
     * @return 
     */
    public List<BlobLight> getManagedViolationBlobList(){
        List<BlobLight> blist = getSessionBean().getSessBlobLightListForRefreshUptake();
        if(currentViolation != null){
            if(blist != null
                    && currentViolation != null 
                && getSessionBean().getSessBlobLightListHolderForRefreshUptake() != null
                && getSessionBean().getSessBlobLightListHolderForRefreshUptake().getBlobLinkEnum() == BlobLinkEnum.CODE_VIOLATION
                && getSessionBean().getSessBlobLightListHolderForRefreshUptake().getParentObjectID() == currentViolation.getViolationID()){
                    System.out.println("CECaseBB.getManagedViolationBlobList | found refreshed BLOB list for violation ID " + currentViolation.getViolationID());
                    currentViolation.setBlobList(blist);
                    getSessionBean().resetBlobRefreshUptakeFields();
            }
            return currentViolation.getBlobList();
        }
        return new ArrayList<>();
    }

    /**
     * Listener for user requests to commit new note content to the current
     * violation
     *
     * @param ev
     */
    public void onViolationNoteCommitButtonChange(ActionEvent ev) {
        CaseCoordinator cc = getCaseCoordinator();
        if(currentViolation == null){
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error appending note: no violation on cecaseBB!", ""));
            return;
        }
        MessageBuilderParams mbp = new MessageBuilderParams();
        mbp.setCred(getSessionBean().getSessUser().getKeyCard());
        mbp.setExistingContent(currentViolation.getNotes());
        mbp.setNewMessageContent(getFormNoteText());
        mbp.setHeader("Violation Note");
        mbp.setUser(getSessionBean().getSessUser());

        try {

            cc.violation_updateNotes(mbp, getCurrentViolation(), getSessionBean().getSessUser());
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Succesfully appended note!", ""));
            currentViolation = cc.violation_getCodeViolation(currentViolation.getViolationID());
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error appending note; apologies!", ""));
        }
    }


 
    
    /**
     * Special getter wrapper around the cease's human link list that can be managed
     * by a shared UI for person link management and search
     * @return the current Case's human link list
     */
    public List<HumanLink> getManagedCECaseHumanLinkList(){
        List<HumanLink> hll = getSessionBean().getSessHumanListRefreshedList();
        if(hll != null){
            currentCase.sethumanLinkList(hll);
            getSessionBean().setSessHumanListRefreshedList(null);
        }
        return currentCase.gethumanLinkList();
    }
    
    
    /**
     * To build enforcableCodeElementListFiltered with respect to ordinance
     * filter text
     */
    private void applyOrdinanceFilter() {
        compareSelectedList();
        CaseCoordinator cc = getCaseCoordinator();
        filteredElementList = cc.violationAction_configureEnforcableCodeElementListFiltered(
                enfCodeElementList,
                ordinanceFilterText,
                ordinanceFilterSearchOrdFullText);
        setSelectedFilteredList();
    }

    /**
     * To clear applied ordinace filter
     */
    private void clearOrdinanceFilter() {
        compareSelectedList();
        setSelectedFilteredList();
        ordinanceFilterText = "";
        filteredElementList = enfCodeElementList;
    }
    
    /**
     * BITS uncommented method
     */
    private void setSelectedFilteredList() {
        CaseCoordinator cc = getCaseCoordinator();
        enfCodeElementSelectedFilteredList.clear();
        enfCodeElementSelectedFilteredList = cc.getFilterSelectedList(filteredElementList, enfCodeElementListPreviousSelected);
        selectedElementList.clear();
        selectedElementList.addAll(enfCodeElementListPreviousSelected);
    }

    /**
     * BITS uncommented method
     */
    private void compareSelectedList() {
        CaseCoordinator cc = getCaseCoordinator();
        enfCodeElementListPreviousSelected.addAll(selectedElementList);
        if (!selectedElementList.containsAll(enfCodeElementSelectedFilteredList))
        {
            Set<EnforceableCodeElement> removedElements = new HashSet<>(cc.updatePreviousSelectedList(
                    enfCodeElementListPreviousSelected, selectedElementList, filteredElementList));

            enfCodeElementListPreviousSelected.removeAll(removedElements);
        }
        if (!enfCodeElementSelectedFilteredList.containsAll(selectedElementList))
        {
            Set<EnforceableCodeElement> removedElements = new HashSet<>(cc.updatePreviousSelectedList(
                    enfCodeElementListPreviousSelected, selectedElementList, filteredElementList));

            enfCodeElementListPreviousSelected.removeAll(removedElements);
        }
    }

    /**
     * Listener for user requests to apply ordinance filter
     */
    public void onApplyOrdinanceFilter() {
        applyOrdinanceFilter();
    }

    /**
     * Listener for user requests to clear ordinance filter
     */
    public void onClearOrdinanceFilter() {
        clearOrdinanceFilter();
    }
    
    
    
    /*******************************************************
    /*******************************************************
     **              Violation BATCH operations                 **
    /*******************************************************/
    
    
    /**
     * Listener to start the batch operation on code violations
     * @param ev 
     */
    public void onViolationBatchOperationInit(ActionEvent ev){
        System.out.println("CECaseBB.onViolationBatchOperationInit");
        configureViolationOperationFacility(true);
        onViolationBatchModeChange();
    }
    
    /**
     * Internal organ for setting up violation facility
     * @param autobatch 
     */
    private void configureViolationOperationFacility(boolean autoBatch){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            violationOperationConfig = cc.violation_getCodeViolationBatchOperationConfigurationSkeleton(getSessionBean().getSessUser(), currentCase);
            if(autoBatch){
                violationOperationConfig.setSelectedCodeViolations(cc.violation_assembleEligibleViolationListForBatchOperation(currentCase));        
                // default mode
                violationBatchOperationModeString = CodeViolationBatchOperationEnum.STIPCOMP_EXTENSION.name();
            }
        } catch (BObStatusException ex) {
            System.out.println(ex);
        }
    }
    
    
    /**
     * Listener for changes to batch mode
     * @param ev 
     */
    public void onViolationBatchModeChange(){
        if(violationOperationConfig != null){
            violationOperationConfig.setOperation(CodeViolationBatchOperationEnum.valueOf(violationBatchOperationModeString));
            // logging
            if(violationOperationConfig.getOperation() != null){
                System.out.println("CECaseBB.onViolationBatchModeChange | operation: " + violationOperationConfig.getOperation().getLabel() );
            } else {
                System.out.println("CECaseBB.onViolationBatchModeChange | Operation activation error" );
                
            }
        }
    }
    
    /**
     * Listener for user requests to remove a violation from the list
     * of ECEs turned into violations, prior to their attachment to the case
     * @param cv 
     */
    public void onViolationRemoveFromBatchButtonChange(CodeViolation cv){
        CaseCoordinator cc = getCaseCoordinator();
        if(cv != null && violationOperationConfig != null && !violationOperationConfig.getSelectedCodeViolations().isEmpty()){
            try {
                cc.violation_deactivateCodeViolation(cv, getSessionBean().getSessUser());
                violationOperationConfig.getSelectedCodeViolations().remove(cv);
            } catch (BObStatusException | IntegrationException ex) {
                System.out.println(ex); 
                getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal ERROR removing codeviolation", ""));
            } 
            System.out.println("Removed Violation ECE ID from batch: " + cv.getViolatedEnfElement().getCodeSetElementID());
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Removed Ordiance from Batch-Code Set Element ID: " + cv.getViolatedEnfElement().getCodeSetElementID(), ""));
        }
    }
    
    /**
     * Internal shared organ for operation and violation specific UI click listeners
     * to setup the batch processing system. As of Sept 2024, compliance, extension, and 
     * nullification all work through a single batch processing pathway. This central
     * pathway can be accessed through violation and UI specific buttons and links
     * for user convenience.
     * 
     * @param cv on which the batch should be applied. Batch size = 1
     * @param op the operation to be undertaken
     */
    private void onSetupViolationBatchModeForSingleViolation(CodeViolation cv, CodeViolationBatchOperationEnum op){
        configureViolationOperationFacility(false);
        if(cv != null && op != null && violationOperationConfig != null){
            violationOperationConfig.getSelectedCodeViolations().add(cv);
            violationOperationConfig.setOperation(op);
            violationBatchOperationModeString = op.name();
        } else {
            System.out.println("CECaseBB.onSetupViolationBatchModeForSingleViolation");
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal ERROR setting up violation batch operation", ""));
            
        }
        
    }
    
    // *********** COMPLIANCE *********
    
    /**
     * Listener for user requests to start the compliance recording operation
     * for a single violation which we'll plug into our batch processing facility
     * @param viol 
     */
    public void onViolationRecordComplianceInitButtonChange(CodeViolation viol){
        System.out.println("CeCaseSearchProfileBB.onViolationRecordComplianceInitButtonChange | from table" + viol.getViolationID());
        onSetupViolationBatchModeForSingleViolation(viol, CodeViolationBatchOperationEnum.COMPLIANCE);
    }
    
    /**
     * Listener for user requests to record compliance from the violation
     * details dialog 
     * @param ev 
     */
    public void onViolationRecordComplianceInitButtonChange(ActionEvent ev){
        onViolationRecordComplianceInitButtonChange(currentViolation);
        System.out.println("CeCaseSearchProfileBB.onViolationRecordComplianceInitButtonChange | from dialog");
    }
    
    // *********** UPDATE CITATION VIOLATION STATUS *********
    
    /**
     * listener for user requests to update the given violation's citation link
     * status since this violation has entered the court system.
     * 
     * @param viol 
     */
    public void onViolationUpdateCitationViolationStatus(CodeViolation viol){
        CaseCoordinator cc = getCaseCoordinator();
        if(viol != null && viol.getCitationIDList() != null && !viol.getCitationIDList().isEmpty()){
            System.out.println("CECaseBB.onViolationUpdateCitationViolationStatus | violationID: " + viol.getViolationID());
            CitationBB citationBB = (CitationBB) getFacesContext().getApplication().evaluateExpressionGet(getFacesContext(), "#{citationBB}", CitationBB.class);
            if(citationBB != null){
                try {
                    citationBB.onCitationViewButtonChange(cc.citation_getCitation(viol.getCitationIDList().get(0)));
                    getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Loaded the first citation which cited the violation you clicked. To see the others, scroll down to the citation panel.", ""));
                } catch (IntegrationException | BObStatusException | BlobException  ex) {
                    getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error: Unable to load citation from violation, sorry.", ""));
                } 
            } else {
                    getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error: Unable to connect to citation system, sorry.", ""));
                
            }
        }
    }
    
    
    // *********** NULLIFY *********
  
    /**
     * Listener for user requests to start nullify operation
     * @param viol 
     */
    public void onViolationNullifyInitButtonChange(CodeViolation viol){
        System.out.println("CeCaseSearchProfileBB.onViolationNullifyInitButtonChange | from table" + viol.getViolationID());
        onSetupViolationBatchModeForSingleViolation(viol, CodeViolationBatchOperationEnum.NULLIFY);
    }
    
    /**
     * Listener for user requests to start nullify operation
     * @param ev
     */
    public void onViolationNullifyInitButtonChange(ActionEvent ev){
        System.out.println("CeCaseSearchProfileBB.onViolationNullifyInitButtonChange | from dialog");
        onSetupViolationBatchModeForSingleViolation(currentViolation, CodeViolationBatchOperationEnum.NULLIFY);
    }
    
    // *********** EXTEND STIP COMP ***********
    
    /**
     * Listener for commencement of extending stip comp date
     *
     * @param ev
     */
    public void onViolationExtendStipCompDateInitButtonChange(ActionEvent ev) {
        System.out.println("CeCaseSearchProfileBB.onViolationExtendStipCompDateInitButtonChange | from dialog");
        onSetupViolationBatchModeForSingleViolation(currentViolation, CodeViolationBatchOperationEnum.STIPCOMP_EXTENSION);
    }
    
    /**
     * Init of stip comp extension for a single violation
     * @param cv 
     */
    public void onViolationExtendStipCompDateInitButtonChange(CodeViolation cv){
        System.out.println("CeCaseSearchProfileBB.onViolationExtendStipCompDateInitButtonChange | from table | CVid:" + cv.getViolationID());
        onSetupViolationBatchModeForSingleViolation(cv, CodeViolationBatchOperationEnum.STIPCOMP_EXTENSION);
    }
    

    // *********** Shared pathways for vbatch ***********
   
    /**
     * listener to commit batch update
     * @param ev 
     */
    public void onViolationBatchOperationCommit(ActionEvent ev){
        System.out.println("CECaseBB.onViolationBatchOperationCommit");
        CaseCoordinator cc = getCaseCoordinator();
        try{
            if(violationOperationConfig.getSelectedCodeViolations() != null && !violationOperationConfig.getSelectedCodeViolations().isEmpty()){
                violationOperationConfig = cc.violation_batchOperationExecute(violationOperationConfig);
                reloadCurrentCase();
                reloadCurrentViolation();
                getSessionEventConductor().triggerEventReload(getCurrentCase().getEventList());
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "violation operation a success!", ""));
            } else {
                violationBatchOperationCleanup();
                System.out.println("CECaseBB.onViolationBatchOperationCommit | Empty violation list on batch operation: aborting");
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "NO violations selected; batch operation aborted", ""));
            }
        } catch(AuthorizationException | BObStatusException | EventException | IntegrationException  ex){
            System.out.println(ex);
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to complete batch operation | " + ex.getMessage(),
                            null));
        }
    }
    
    /**
     * Resets the violation batch system
     */
    private void violationBatchOperationCleanup(){
        violationOperationConfig = null;
    }
    
    /**
     * Listener to abort batch update
     * @param ev 
     */
    public void onViolationBatchOperationCancel(){
        System.out.println("CECaseBB.onViolationBatchOperationCancel");
        violationBatchOperationCleanup();
    }
    
    /**
     * Listener to end log view on batch operation
     * @param ev 
     */
    public void onViolationBatchOperationLogViewClose(ActionEvent ev){
        System.out.println("CECaseBB.onViolationBatchOperationLogViewClose");
        violationBatchOperationCleanup();
        
    }

    
    /*******************************************************
    /*******************************************************
     **                       STATUS XML                  **
    /*******************************************************/
    /*******************************************************/
    
    public void writeStatusXML(ActionEvent ev){
        CaseCoordinator cc = getCaseCoordinator();
        
        try {
            cc.cecase_updateCaseStatusXML(currentCase, getSessionBean().getSessUser());
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
        } 
        
    }
    
    
    
    
    
    
    /*******************************************************
    /*******************************************************
     **              GETTERS AND SETTERS                  **
    /*******************************************************/
    /*******************************************************/

    /**
     * @return the currentCase
     */
    public CECaseDataHeavy getCurrentCase() {
        return currentCase;
    }

   
    
    /**
     * @param currentCase the currentCase to set
     */
    public void setCurrentCase(CECaseDataHeavy currentCase) {
        this.currentCase = currentCase;
    }

   
    

   
    /**
     * @return the userManagerOptionList
     */
    public List<User> getUserManagerOptionList() {
        return userManagerOptionList;
    }

    /**
     * @param userManagerOptionList the userManagerOptionList to set
     */
    public void setUserManagerOptionList(List<User> userManagerOptionList) {
        this.userManagerOptionList = userManagerOptionList;
    }

    /**
     * @return the bobSourceOptionList
     */
    public List<BOBSource> getBobSourceOptionList() {
        return bobSourceOptionList;
    }

    /**
     * @param bobSourceOptionList the bobSourceOptionList to set
     */
    public void setBobSourceOptionList(List<BOBSource> bobSourceOptionList) {
        this.bobSourceOptionList = bobSourceOptionList;
    }

    /**
     * @return the formNoteText
     */
    public String getFormNoteText() {
        return formNoteText;
    }

    /**
     * @param formNoteText the formNoteText to set
     */
    public void setFormNoteText(String formNoteText) {
        this.formNoteText = formNoteText;
    }

    /**
     * @return the closingEventList
     */
    public List<EventCategory> getClosingEventList() {
        return getClosingEventCategoryList();
    }

    /**
     * @param closingEventList the closingEventList to set
     */
    public void setClosingEventList(List<EventCategory> closingEventList) {
        this.setClosingEventCategoryList(closingEventList);
    }

    /**
     * @return the closingEventCategorySelected
     */
    public EventCategory getClosingEventCategorySelected() {
        return closingEventCategorySelected;
    }

    /**
     * @param closingEventCategorySelected the closingEventCategorySelected to set
     */
    public void setClosingEventCategorySelected(EventCategory closingEventCategorySelected) {
        this.closingEventCategorySelected = closingEventCategorySelected;
    }

   
    /**
     * @return the formNOVFollowupDays
     */
    public int getFormNOVFollowupDays() {
        return formNOVFollowupDays;
    }

    /**
     * @param formNOVFollowupDays the formNOVFollowupDays to set
     */
    public void setFormNOVFollowupDays(int formNOVFollowupDays) {
        this.formNOVFollowupDays = formNOVFollowupDays;
    }

    /**
     * @return the currentViolation
     */
    public CodeViolation getCurrentViolation() {
        return currentViolation;
    }

    /**
     * @return the viewOptionList
     */
    public List<ViewOptionsActiveListsEnum> getViewOptionList() {
        return viewOptionList;
    }

    /**
     * @return the selectedViewOption
     */
    public ViewOptionsActiveListsEnum getSelectedViewOption() {
        return selectedViewOption;
    }

    /**
     * @return the severityList
     */
    public List<IntensityClass> getSeverityList() {
        return severityList;
    }

    /**
     * @return the formNoteTextViolation
     */
    public String getFormNoteTextViolation() {
        return formNoteTextViolation;
    }

    /**
     * @return the filteredElementList
     */
    public List<EnforceableCodeElement> getFilteredElementList() {
        return filteredElementList;
    }

    /**
     * @return the currentCodeSet
     */
    public CodeSet getCurrentCodeSet() {
        return currentCodeSet;
    }

  
    /**
     * @param currentViolation the currentViolation to set
     */
    public void setCurrentViolation(CodeViolation currentViolation) {
        this.currentViolation = currentViolation;
    }

    /**
     * @param viewOptionList the viewOptionList to set
     */
    public void setViewOptionList(List<ViewOptionsActiveListsEnum> viewOptionList) {
        this.viewOptionList = viewOptionList;
    }

    /**
     * @param selectedViewOption the selectedViewOption to set
     */
    public void setSelectedViewOption(ViewOptionsActiveListsEnum selectedViewOption) {
        this.selectedViewOption = selectedViewOption;
    }

    /**
     * @param severityList the severityList to set
     */
    public void setSeverityList(List<IntensityClass> severityList) {
        this.severityList = severityList;
    }

    /**
     * @param formNoteTextViolation the formNoteTextViolation to set
     */
    public void setFormNoteTextViolation(String formNoteTextViolation) {
        this.formNoteTextViolation = formNoteTextViolation;
    }

    /**
     * @param filteredElementList the filteredElementList to set
     */
    public void setFilteredElementList(List<EnforceableCodeElement> filteredElementList) {
        this.filteredElementList = filteredElementList;
    }

    /**
     * @param currentCodeSet the currentCodeSet to set
     */
    public void setCurrentCodeSet(CodeSet currentCodeSet) {
        this.currentCodeSet = currentCodeSet;
    }

    /**
     * @return the closingEventCategoryList
     */
    public List<EventCategory> getClosingEventCategoryList() {
        return closingEventCategoryList;
    }

    
    /**
     * @param closingEventCategoryList the closingEventCategoryList to set
     */
    public void setClosingEventCategoryList(List<EventCategory> closingEventCategoryList) {
        this.closingEventCategoryList = closingEventCategoryList;
    }

 
    
    /**
     * @return the blobList
     */
    public List<BlobLight> getBlobList() {
        return blobList;
    }

    /**
     * @param blobList the blobList to set
     */
    public void setBlobList(List<BlobLight> blobList) {
        this.blobList = blobList;
    }

    /**
     * @return the currentBlob
     */
    public BlobLight getCurrentBlob() {
        return currentBlob;
    }

    /**
     * @param currentBlob the currentBlob to set
     */
    public void setCurrentBlob(BlobLight currentBlob) {
        this.currentBlob = currentBlob;
    }

   
    /**
     * @return the eventPersonIDForLookup
     */
    public int getEventPersonIDForLookup() {
        return eventPersonIDForLookup;
    }

    /**
     * @param eventPersonIDForLookup the eventPersonIDForLookup to set
     */
    public void setEventPersonIDForLookup(int eventPersonIDForLookup) {
        this.eventPersonIDForLookup = eventPersonIDForLookup;
    }

    /**
     * @return the selectedElementList
     */
    public List<EnforceableCodeElement> getSelectedElementList() {
        return selectedElementList;
    }

    /**
     * @return the selectedViolationList
     */
    public List<CodeViolation> getSelectedViolationList() {
        return selectedViolationList;
    }

    /**
     * @param selectedElementList the selectedElementList to set
     */
    public void setSelectedElementList(List<EnforceableCodeElement> selectedElementList) {
        this.selectedElementList = selectedElementList;
    }

    /**
     * @param selectedViolationList the selectedViolationList to set
     */
    public void setSelectedViolationList(List<CodeViolation> selectedViolationList) {
        this.selectedViolationList = selectedViolationList;
    }

    
   

    /**
     * @return the editModeCurrentViolation
     */
    public boolean isEditModeCurrentViolation() {
        return editModeCurrentViolation;
    }

    /**
     * @param editModeCurrentViolation the editModeCurrentViolation to set
     */
    public void setEditModeCurrentViolation(boolean editModeCurrentViolation) {
        this.editModeCurrentViolation = editModeCurrentViolation;
    }

    /**
     * @return the reportCECase
     */
    public ReportConfigCECase getReportCECase() {
        return reportCECase;
    }

    /**
     * @param reportCECase the reportCECase to set
     */
    public void setReportCECase(ReportConfigCECase reportCECase) {
        this.reportCECase = reportCECase;
    }

    /**
     * @return the editModeCurrentCase
     */
    public boolean isEditModeCurrentCase() {
        return editModeCurrentCase;
    }

    /**
     * @param editModeCurrentCase the editModeCurrentCase to set
     */
    public void setEditModeCurrentCase(boolean editModeCurrentCase) {
        this.editModeCurrentCase = editModeCurrentCase;
    }


    /**
     * @return the editModeCurrentCaseManager
     */
    public boolean isEditModeCurrentCaseManager() {
        return editModeCurrentCaseManager;
    }



    /**
     * @return the editModeCurrentCloseCase
     */
    public boolean isEditModeCurrentCloseCase() {
        return editModeCurrentCloseCase;
    }

    /**
     * @return the editModeCurrentCaseRecord
     */
    public boolean isEditModeCurrentCaseRecord() {
        return editModeCurrentCaseRecord;
    }

    /**
     * @param editModeCurrentCaseManager the editModeCurrentCaseManager to set
     */
    public void setEditModeCurrentCaseManager(boolean editModeCurrentCaseManager) {
        this.editModeCurrentCaseManager = editModeCurrentCaseManager;
    }

   
    /**
     * @param editModeCurrentCloseCase the editModeCurrentCloseCase to set
     */
    public void setEditModeCurrentCloseCase(boolean editModeCurrentCloseCase) {
        this.editModeCurrentCloseCase = editModeCurrentCloseCase;
    }

    /**
     * @param editModeCurrentCaseRecord the editModeCurrentCaseRecord to set
     */
    public void setEditModeCurrentCaseRecord(boolean editModeCurrentCaseRecord) {
        this.editModeCurrentCaseRecord = editModeCurrentCaseRecord;
    }

    /**
     * @return the formCurrentCaseUnitAssociated
     */
    public boolean getFormCurrentCaseUnitAssociated() {
        return formCurrentCaseUnitAssociated;
    }

    /**
     * @param formCurrentCaseUnitAssociated the formCurrentCaseUnitAssociated to set
     */
    public void setFormCurrentCaseUnitAssociated(boolean formCurrentCaseUnitAssociated) {
        this.formCurrentCaseUnitAssociated = formCurrentCaseUnitAssociated;
    }

    /**
     * @return the formSelectedUnit
     */
    public PropertyUnit getFormSelectedUnit() {
        return formSelectedUnit;
    }

    /**
     * @param formSelectedUnit the formSelectedUnit to set
     */
    public void setFormSelectedUnit(PropertyUnit formSelectedUnit) {
        this.formSelectedUnit = formSelectedUnit;
    }

    /**
     * @return the originationEventCategoryList
     */
    public List<EventCategory> getOriginationEventCategoryList() {
        return originationEventCategoryList;
    }

    /**
     * @param originationEventCategoryList the originationEventCategoryList to set
     */
    public void setOriginationEventCategoryList(List<EventCategory> originationEventCategoryList) {
        this.originationEventCategoryList = originationEventCategoryList;
    }

    /**
     * @return the formCECaseOriginationEventCat
     */
    public EventCategory getFormCECaseOriginationEventCat() {
        return formCECaseOriginationEventCat;
    }

    /**
     * @param formCECaseOriginationEventCat the formCECaseOriginationEventCat to set
     */
    public void setFormCECaseOriginationEventCat(EventCategory formCECaseOriginationEventCat) {
        this.formCECaseOriginationEventCat = formCECaseOriginationEventCat;
    }

    /**
     * @return the userManagerSeletedForManagerChange
     */
    public User getUserManagerSeletedForManagerChange() {
        return userManagerSeletedForManagerChange;
    }

    /**
     * @param userManagerSeletedForManagerChange the userManagerSeletedForManagerChange to set
     */
    public void setUserManagerSeletedForManagerChange(User userManagerSeletedForManagerChange) {
        this.userManagerSeletedForManagerChange = userManagerSeletedForManagerChange;
    }

    /**
     * @return the permissionAllowViolationAddUpdate
     */
    public boolean isPermissionAllowViolationAddUpdate() {
        return permissionAllowViolationAddUpdate;
    }

    /**
     * @return the permissionAllowViolationExtendStipCompDate
     */
    public boolean isPermissionAllowViolationExtendStipCompDate() {
        return permissionAllowViolationExtendStipCompDate;
    }

    /**
     * @return the permissionAllowCaseClosure
     */
    public boolean isPermissionAllowCaseClosure() {
        return permissionAllowCaseClosure;
    }

    /**
     * @return the permissionAllowCaseOpenEdit
     */
    public boolean isPermissionAllowCaseOpenEdit() {
        return permissionAllowCaseOpenEdit;
    }

    /**
     * @return the permissionAllowCaseDeactivate
     */
    public boolean isPermissionAllowCaseDeactivate() {
        return permissionAllowCaseDeactivate;
    }

    public List<EnforceableCodeElement> getEnfCodeElementList() {
        return enfCodeElementList;
    }

    public void setEnfCodeElementList(List<EnforceableCodeElement> enfCodeElementList) {
        this.enfCodeElementList = enfCodeElementList;
    }

    public String getOrdinanceFilterText() {
        return ordinanceFilterText;
    }

    public void setOrdinanceFilterText(String ordinanceFilterText) {
        this.ordinanceFilterText = ordinanceFilterText;
    }

    /**
     * @return the currentEnforcableCodeElement
     */
    public EnforceableCodeElement getCurrentEnforcableCodeElement() {
        return currentEnforcableCodeElement;
    }

    /**
     * @param currentEnforcableCodeElement the currentEnforcableCodeElement to set
     */
    public void setCurrentEnforcableCodeElement(EnforceableCodeElement currentEnforcableCodeElement) {
        this.currentEnforcableCodeElement = currentEnforcableCodeElement;
    }

    /**
     * @return the ordinanceEntryActivateTable
     */
    public boolean isOrdinanceEntryActivateTable() {
        return ordinanceEntryActivateTable;
    }

    /**
     * @param ordinanceEntryActivateTable the ordinanceEntryActivateTable to set
     */
    public void setOrdinanceEntryActivateTable(boolean ordinanceEntryActivateTable) {
        this.ordinanceEntryActivateTable = ordinanceEntryActivateTable;
    }

    /**
     * @return the ordinanceEntryActivateTree
     */
    public boolean isOrdinanceEntryActivateTree() {
        return ordinanceEntryActivateTree;
    }

    /**
     * @param ordinanceEntryActivateTree the ordinanceEntryActivateTree to set
     */
    public void setOrdinanceEntryActivateTree(boolean ordinanceEntryActivateTree) {
        this.ordinanceEntryActivateTree = ordinanceEntryActivateTree;
    }

    /**
     * @return the ordinanceFilterSearchOrdFullText
     */
    public boolean isOrdinanceFilterSearchOrdFullText() {
        return ordinanceFilterSearchOrdFullText;
    }

    /**
     * @param ordinanceFilterSearchOrdFullText the ordinanceFilterSearchOrdFullText to set
     */
    public void setOrdinanceFilterSearchOrdFullText(boolean ordinanceFilterSearchOrdFullText) {
        this.ordinanceFilterSearchOrdFullText = ordinanceFilterSearchOrdFullText;
    }


    /**
     * @return the violationOperationConfig
     */
    public CodeViolationBatchOperationConfiguration getViolationOperationConfig() {
        return violationOperationConfig;
    }

    /**
     * @param violationOperationConfig the violationOperationConfig to set
     */
    public void setViolationOperationConfig(CodeViolationBatchOperationConfiguration violationOperationConfig) {
        this.violationOperationConfig = violationOperationConfig;
    }

    /**
     * @return the violationBatchOperationModeString
     */
    public String getViolationBatchOperationModeString() {
        return violationBatchOperationModeString;
    }

    /**
     * @param violationBatchOperationModeString the violationBatchOperationModeString to set
     */
    public void setViolationBatchOperationModeString(String violationBatchOperationModeString) {
        this.violationBatchOperationModeString = violationBatchOperationModeString;
    }

    /**
     * @return the operationConfigBundleCaseReopen
     */
    public OperationConfigurationCECaseReopen getOperationConfigBundleCaseReopen() {
        return operationConfigBundleCaseReopen;
    }

    /**
     * @param operationConfigBundleCaseReopen the operationConfigBundleCaseReopen to set
     */
    public void setOperationConfigBundleCaseReopen(OperationConfigurationCECaseReopen operationConfigBundleCaseReopen) {
        this.operationConfigBundleCaseReopen = operationConfigBundleCaseReopen;
    }

    /**
     * @return the requireFindingsOnViolationAdd
     */
    public boolean isRequireFindingsOnViolationAdd() {
        return requireFindingsOnViolationAdd;
    }

    /**
     * @param requireFindingsOnViolationAdd the requireFindingsOnViolationAdd to set
     */
    public void setRequireFindingsOnViolationAdd(boolean requireFindingsOnViolationAdd) {
        this.requireFindingsOnViolationAdd = requireFindingsOnViolationAdd;
    }

  
   

}
