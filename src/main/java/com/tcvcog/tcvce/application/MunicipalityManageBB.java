/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.coordinators.CodeCoordinator;
import com.tcvcog.tcvce.coordinators.EventCoordinator;
import com.tcvcog.tcvce.coordinators.MunicipalityCoordinator;
import com.tcvcog.tcvce.coordinators.OccInspectionCoordinator;
import com.tcvcog.tcvce.coordinators.OccupancyCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.coordinators.UserCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.EventException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.BlobLight;
import com.tcvcog.tcvce.entities.BlobLinkEnum;
import com.tcvcog.tcvce.entities.CodeSet;
import com.tcvcog.tcvce.entities.CodeSource;
import com.tcvcog.tcvce.entities.County;
import com.tcvcog.tcvce.entities.CourtEntity;
import com.tcvcog.tcvce.entities.EventCategory;
import com.tcvcog.tcvce.entities.LinkedObjectRole;
import com.tcvcog.tcvce.entities.LinkedObjectSchemaEnum;
import com.tcvcog.tcvce.entities.MailingAddressLink;
import com.tcvcog.tcvce.entities.MailingCityStateZip;
import com.tcvcog.tcvce.entities.MuniProfile;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.MunicipalityDataHeavy;
import com.tcvcog.tcvce.entities.PrintStyle;
import com.tcvcog.tcvce.entities.Property;
import com.tcvcog.tcvce.entities.User;
import com.tcvcog.tcvce.entities.occupancy.OccChecklistTemplate;
import com.tcvcog.tcvce.entities.occupancy.OccPeriod;
import com.tcvcog.tcvce.entities.occupancy.OccPermitType;
import com.tcvcog.tcvce.util.MessageBuilderParams;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * SECURITY CRITICAL: The premier tool for managing municipalities system wide!
 * 
 * @author CHEN&CHEN and sylvia and Ellen Bascomb of 31Y Revisiting for mcandless (sic) JUNE 2022
 * and finalizing in Jan 2023 for beginning of Hillman
 */
public class MunicipalityManageBB extends BackingBeanUtils implements Serializable {

    private MunicipalityDataHeavy currentMuniDataheavy;
    private List<Municipality> muniList;
    private boolean includeInactiveMunis;
    
    
    
    private Map<String, Integer> styleMap;
    
    /**
     * Permit types are now configured based on permit file types
     * @Deprecated
     */
    private List<OccPermitType> occPermitTypeCandidateList;
    
    
    
    private List<EventCategory> eventCategoryCandidateList;

  
 
    
    
    /***************************************************************************
     ************ BEAN LOADING AND MUNI SELECTION AND REFRESH ****************** 
     **************************************************************************/
       /**
     * Creates a new instance of MunicipalityManageBB
     */
    public MunicipalityManageBB() {
    }

    @PostConstruct
    public void initBean() {    
        EventCoordinator ec = getEventCoordinator();
        
        includeInactiveMunis = false;
        includeInactiveProfiles = false;
        
        currentMuniDataheavy = getSessionBean().getSessMuni();
        refreshMuniList();
        //initialize default setting 
        setupCandidateLists();
        setupDynamicCandidateLists();
        
        editModeMuniInfo = false;
        
        try {
            eventCategoryCandidateList = ec.getEventCategoryList();
        } catch (IntegrationException ex) {
            System.out.println(ex);
        }
    }
    
   /**
     * Initialize the whole page into default setting
     */
    public void setupCandidateLists() {

        MunicipalityCoordinator mc = getMuniCoordinator();
        CodeCoordinator cc = getCodeCoordinator();
        UserCoordinator uc = getUserCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        OccupancyCoordinator oc = getOccupancyCoordinator();
        CaseCoordinator casec = getCaseCoordinator();
        
        try {
            courtEntityCandidateList = casec.getCourtEntityListComplete(true);
            setOccPermitTypeCandidateList(oc.getOccPermitTypeList(true));
            codeSetCandidateList = cc.getCodeSetListComplete();
            codeSourceCandidateList = cc.getCodeSourceList(false);
            muniProfileCandidateList = mc.getMuniProfilesList(false);
            userCandidateListe = uc.user_assembleUserListForSearch(getSessionBean().getSessUser());
            printStyleCandidateList = sc.getPrintStyleList();
            countyCandidateList = mc.getCountyListComplete(false);
            
            countyListSelected = new ArrayList<>();
            courtEntitiesSelected = new ArrayList<>();
            
            propertyLinkRoleCandidateList = sc.assembleLinkedObjectRolesBySchema(LinkedObjectSchemaEnum.ParcelMailingaddress);


        } catch (IntegrationException | BObStatusException  ex) {
            System.out.println(ex);
            //Message Noticefication
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Municipality Page Unsuccessfully Initialized", ""));
        }
    }
    
    
    /**
     * Updates backing bean lists that are specific to the current MuniDataHeavy
     */
    private void setupDynamicCandidateLists(){
        OccupancyCoordinator oc = getOccupancyCoordinator();
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        if(currentMuniDataheavy != null){
            try {
                // NOTE:
                //initialize default current occperiod list
                if(currentMuniDataheavy.getMuniPropertyDH() != null){
                    occPeriodCandidateList = oc.upcastOccPeriodDataHeavyToOccPeriod(currentMuniDataheavy.getMuniPropertyDH().getCompletePeriodList());
                }
                mailingAddressCandidateList = getSessionBean().getSessProperty().getMailingAddressLinkList();
                inspectionChecklistCandidateList = oic.getOccChecklistTemplateList(currentMuniDataheavy, false);
            } catch (IntegrationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fatal error: Municipality object candidate lists unsuccessfully initialized", ""));
            }
        }
    }
  
    /**
     * Listener for user requests to view a muni
     * @param m 
     */
    public void viewMuni(Municipality m){
        MunicipalityCoordinator mc = getMuniCoordinator();
        try {
            currentMuniDataheavy = mc.assembleMuniDataHeavy(m, getSessionBean().getSessUser());
            System.out.println("View Muni: " + m.getMuniCode());
            getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Viewing muni: " + m.getMuniName(), ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException | BlobException | EventException  ex) {
            System.out.println(ex);
        } 
    }
    
    /**
     * Listener for user clicks on a muni to view its guts
     *
     * @param mdh
     * @throws IntegrationException
     * @throws AuthorizationException
     */
    public void onMuniSelectedButtonChange(MunicipalityDataHeavy mdh) throws IntegrationException, AuthorizationException {
        currentMuniDataheavy = mdh;
        refreshCurrentMuniDataHeavy();
        getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Current Selected Municipality: " + currentMuniDataheavy.getMuniName(), ""));
    }
    
    /**
     * Gets a fresh list of munis from the DB
     */
    public void refreshMuniList(){
        MunicipalityCoordinator mc = getMuniCoordinator();
        try {
            // flip include inactive to match with requireActive flags 
            // down through coordinator and integrator
            muniList = mc.getMuniList(includeInactiveMunis);
        } catch (IntegrationException | BObStatusException   ex) {
            System.out.println("MunicipalityManageBB.refreshMuniList" + ex.toString());
            getFacesContext().addMessage(null, 
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unable to load muni list", ""));
        } 
    }
    
    /**
     * Extracts a fresh copy of the current muni data heavy from the db
     */
    public void refreshCurrentMuniDataHeavy(){
        MunicipalityCoordinator mc = getMuniCoordinator();
        
        if(currentMuniDataheavy != null){
            try {
                currentMuniDataheavy = mc.assembleMuniDataHeavy(currentMuniDataheavy, getSessionBean().getSessUser());
                getFacesContext().addMessage(null, 
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Refreshed Municode: " + currentMuniDataheavy.getMuniCode(), ""));
            } catch (IntegrationException | AuthorizationException | BObStatusException | BlobException | EventException ex) {
                System.out.println("refreshCurrentMuniDataHeavy | exception: " + ex.toString());
                getFacesContext().addMessage(null, 
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unable to extract muni data heavy from DB", ""));
            }
        }
    }
    
    
    /***************************************************************************
     ********************** MUNI STATUS AUDIT ********************************** 
     **************************************************************************/
     
    
    /***************************************************************************
     ********************** CORE MUNI INIT AND MUNI INFO CRAP ****************** 
     **************************************************************************/
    
     // but share an update method on the coordinator and the integrator
    private boolean editModeMuniInfo;
    private boolean muniCreateMode;
    
    /**
     * Listener for user requests to start the muni add process
     * 
     */
    public void onMuniAddInitButtonChange(){
       MunicipalityCoordinator mc = getMuniCoordinator();
        currentMuniDataheavy = mc.createMunicipalityDataHeavySkeleton();
        editModeMuniInfo = false;
        muniCreateMode = true;
        toggleMuniAddEditInfoButtonChange(null);
        System.out.println("MunicpaltiyManageBB.onMuniAddInitButtonChange: Skeleton ready to roll");
       
    }
    
    
    /**
     * Listener for users to cancel their muni add/edit operation
     * @param ev 
     */
    public void onOperationMuniAddEditAbort(ActionEvent ev){
        System.out.println("MunicpaltiyManageBB.toggleMuniAddEditButtonChange: ABORT");
        if(currentMuniDataheavy.getMuniCode() == 0){
            // just get our first muni in the list
            if(muniList != null && !muniList.isEmpty()){
                viewMuni(muniList.get(0));
            }
        }
        editModeMuniInfo = false;
        muniCreateMode = false;
        
    }
    
    
     /**
     * Listener for users to click the add or the edit button
     * @param ev
     */
    public void toggleMuniAddEditInfoButtonChange(ActionEvent ev){
        System.out.println("MunicpaltiyManageBB.toggleMuniAddEditButtonChange: incoming: " + editModeMuniInfo);
       
        if(editModeMuniInfo){
            if(currentMuniDataheavy != null){
               
                if(muniCreateMode){
                    onInsertMuniCommit();
                    muniCreateMode = false;
                } else {
                    onUpdateMuniCommit();
                    refreshCurrentMuniDataHeavy();
                }
            }
        }
        resetMuniInfoFormControls();
        editModeMuniInfo = !editModeMuniInfo;
    }
    
    /**
     * Listener for user requests to stop the editing process of muni info
     * @param ev 
     */
    public void onMuniInfoEditOperationAbortButtonChange(ActionEvent ev){
        if(currentMuniDataheavy.getMuniCode() == 0){
            // pick random muni
            viewMuni(muniList.get(0));
        }
        editModeMuniInfo = false;
    }
      
    /**
     * Finalizes the muni update process
     */
    public void onUpdateMuniCommit() {
        MunicipalityCoordinator mc = getMuniCoordinator();
        try {
            mc.updateMuniInfo(currentMuniDataheavy, getSessionBean().getSessUser());
            refreshCurrentMuniDataHeavy();
            refreshMuniList();
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Muni update successful!", ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fail To Update Municipality", ""));
        }
    }

    /**
     * Finalizes the muni insert process
     */
    public void onInsertMuniCommit() {
        MunicipalityCoordinator mc = getMuniCoordinator();
        try {
            System.out.println("MunicipalityManageBB.onInsertMuniCommit");
            mc.insertMuni(currentMuniDataheavy, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
            currentMuniDataheavy = mc.assembleMuniDataHeavy(mc.getMuni(currentMuniDataheavy.getMuniCode()), getSessionBean().getSessUser());
            refreshMuniList();
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Successful Insert New Municipality", ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException | BlobException | EventException  ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        } 
    }

    /**
     * Initthe muni deac process
     */
    public void onDeactivateMuniInit() {
        System.out.println("MunicipalityManageBB.onDeactivateMuniInit");
    }
    

    /**
     * Finalizes the muni deac process
     */
    public void onDeactivateMuniCommit() {
        currentMuniDataheavy.setActiveInProgram(false);
        MunicipalityCoordinator mc = getMuniCoordinator();
        try {
            mc.deactivateMuni(currentMuniDataheavy, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
            refreshCurrentMuniDataHeavy();
            refreshMuniList();
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fail To Remove Municipality", ""));
        }
    }
    

    /**
     * Cancel the muni deac process
     */
    public void onDeactivateMuniAbort() {
        System.out.println("MunicipalityManageBB.onDeactivateMuniAbort");
    }
    
    
    
    
    /***************************************************************************
     ********************** OBJECT LINKS JAZZ ********************************** 
     **************************************************************************/
     
    private boolean editModeMuniObjectLinks;
    
    private boolean updateToSelectedCourts;
    private List<CourtEntity> courtEntityCandidateList;
    private List<CourtEntity> courtEntitiesSelected;
    
    
    private List<CodeSet> codeSetCandidateList;
    
    private boolean updateCountyListToSelected;
    private List<County> countyCandidateList;
    private List<County> countyListSelected;
    
    private List<CodeSource> codeSourceCandidateList;
    private List<User> userCandidateListe;
    
    
    // DYNAMIC LISTS and tools that vary with current muni
    private boolean useSessionParcel;
    
    private boolean updateToSelectedOccPeriod;
    private List<OccPeriod> occPeriodCandidateList;
    private OccPeriod selectedOccPeriod;
    
    private boolean updateToSelectedMailingAddress;
    private List<MailingAddressLink> mailingAddressCandidateList;
    private MailingAddressLink selectedMailingAddress;
    
    private List<OccChecklistTemplate> inspectionChecklistCandidateList;
    
    /**
     * internal organ for setting selected options to off 
     * and selected lists to empty new ones
     */
    private void resetMuniInfoFormControls(){
            useSessionParcel = false;
            updateToSelectedMailingAddress = false;
            updateToSelectedOccPeriod = false;
            updateToSelectedHeaderImage = false;
            updateToSelectedCourts = false;
            updateCountyListToSelected = false;
              
            countyListSelected = new ArrayList<>();
            courtEntitiesSelected = new ArrayList<>();
    }
    
    
    /**
     * Listener for user requests to toggle muni object links edit mode
     * @param ev 
     */
    public void toggleEditMuniObjectLinks(ActionEvent ev){
        MunicipalityCoordinator mc = getMuniCoordinator();
        if(!editModeMuniObjectLinks){
            setupDynamicCandidateLists();
            resetMuniInfoFormControls();
        }
        if(editModeMuniObjectLinks){
            System.out.println("MunicipalityManageBB.toggleEditMuniObjectLinks | entering write updates");
             
            try{
                
                // check about injecting new address or occ period
                if(updateToSelectedMailingAddress && selectedMailingAddress != null){
                    currentMuniDataheavy.setContactAddress(selectedMailingAddress);
                }
                if(updateToSelectedOccPeriod && selectedOccPeriod != null){
                    currentMuniDataheavy.setDefaultOccPeriodID(selectedOccPeriod.getPeriodID());
                }
                if(useSessionParcel && getSessionBean().getSessProperty() != null){
                    currentMuniDataheavy.setMuniPropertyDH(getSessionBean().getSessProperty());
                }
                if(updateCountyListToSelected && countyListSelected != null){
                    currentMuniDataheavy.setCountyList(countyListSelected);
                }
                if(updateToSelectedCourts && courtEntitiesSelected != null){
                    currentMuniDataheavy.setCourtEntities(courtEntitiesSelected);
                }
                mc.updateMuniObjectLinks(currentMuniDataheavy, getSessionBean().getSessUser());
                refreshCurrentMuniDataHeavy();
                getFacesContext().addMessage(null, 
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Muni info update success!", ""));
            } catch (AuthorizationException | BObStatusException | IntegrationException ex ){
                System.out.println(ex);
                getFacesContext().addMessage(null, 
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            }
        } else {
            System.out.println("MunicipalityManageBB.toggleEditMuniObjectLinks | entering edit mode");
            
        }
        editModeMuniObjectLinks = !editModeMuniObjectLinks;
    }
    
    /**
     * Listener for user requests to abort module edit mode
     * @param ev 
     */
    public void onEditMuniObjectLinksAbort(ActionEvent ev){
        System.out.println("MunicipalityManageBB.onEditMuniModulesAbort");
        editModeMuniObjectLinks = false;
        
    }
    
    
    
    
    /***************************************************************************
     ********************** MODULES SHIT *************************************** 
     **************************************************************************/
     
    private boolean editModeMuniModules;
    
    /**
     * Listener for user requests to toggle muni modules edit mode
     * @param ev 
     */
    public void toggleEditMuniModules(ActionEvent ev){
        MunicipalityCoordinator mc = getMuniCoordinator();
        if(editModeMuniModules){
            System.out.println("MunicipalityManageBB.toggleEditMuniModules | entering write updates");
            try {
                mc.updateMuniModules(currentMuniDataheavy, getSessionBean().getSessUser());
                refreshCurrentMuniDataHeavy();
                getFacesContext().addMessage(null, 
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Muni modules update success!", ""));
            } catch (AuthorizationException | IntegrationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null, 
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            }
        } else {
            System.out.println("MunicipalityManageBB.toggleEditMuniModules | entering edit mode");
            
        }
        
        editModeMuniModules = !editModeMuniModules;
    }
    
    /**
     * Listener for user requests to abort module edit mode 
     * @param ev 
     */
    public void onEditMuniModulesAbort(ActionEvent ev){
        System.out.println("MunicipalityManageBB.onEditMuniModulesAbort");
        editModeMuniModules = false;
        
    }
    
    
    /***************************************************************************
     *************** MUNI SPECIFIC SELECTED PERMISSIONS PROFILE **************** 
     **************************************************************************/
    private boolean editModeMuniPermissionsProfile;
    
    private List<MuniProfile> muniProfileCandidateList;
    
    
    /**
     * Listener for user requests to toggle muni object links edit mode
     * @param ev 
     */
    public void toggleEditMuniPermissions(ActionEvent ev){
        MunicipalityCoordinator mc = getMuniCoordinator();
        if(editModeMuniPermissionsProfile){
            System.out.println("MunicipalityManageBB.toggleEditMuniPermissionsLinks | entering write updates");
            try {
                mc.updateMuniPermissions(currentMuniDataheavy, getSessionBean().getSessUser());
                refreshCurrentMuniDataHeavy();
                getFacesContext().addMessage(null, 
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Muni permissions update success!", ""));
            } catch (AuthorizationException | IntegrationException | AbstractMethodError  ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null, 
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            } 
        } else {
            System.out.println("MunicipalityManageBB.toggleEditMuniPermissionsLinks | entering edit mode");
            
        }
        
        editModeMuniPermissionsProfile = !editModeMuniPermissionsProfile;
    }
    
    /**
     * Listener for user requests to abort module edit mode
     * @param ev 
     */
    public void onEditMuniPermissionsAbort(ActionEvent ev){
        System.out.println("MunicipalityManageBB.onEditMuniModulesAbort");
        editModeMuniPermissionsProfile = false;
        
    }
    
    /***************************************************************************
     ************ MUNI SPECIFIC SELECTED PRINTING AND HEADER *******************
     **************************************************************************/
    
    private boolean editModeMuniPrintStyleAndHeader;
    
    
    private List<PrintStyle> printStyleCandidateList;
    
    private boolean updateToSelectedHeaderImage;
    private BlobLight selectedHeaderBlob;
  
    
    
    /**
     * Listener for user requests to toggle muni object links edit mode
     * @param ev 
     */
    public void toggleEditMuniPrintStyles(ActionEvent ev){
        MunicipalityCoordinator mc = getMuniCoordinator();
        if(editModeMuniPrintStyleAndHeader){
            System.out.println("MunicipalityManageBB.toggleEditMuniPrintingLinks | entering write updates");
            try {
               if(updateToSelectedHeaderImage && selectedHeaderBlob != null){
                    currentMuniDataheavy.setDefaultMuniHeaderImage(selectedHeaderBlob);
                }
                mc.updateMuniPrinting(currentMuniDataheavy, getSessionBean().getSessUser());
                getFacesContext().addMessage(null, 
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Muni print info update success!", ""));
                refreshCurrentMuniDataHeavy();
                updateToSelectedHeaderImage = false;
            } catch (AuthorizationException | IntegrationException  ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null, 
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            }
        } else {
            System.out.println("MunicipalityManageBB.toggleEditMuniPrintingLinks | entering edit mode");
            
        }
        
        editModeMuniPrintStyleAndHeader = !editModeMuniPrintStyleAndHeader;
        
    }
    
    /**
     * Listener for user requests to abort module edit mode
     * @param ev 
     */
    public void onEditMuniPrintingAbort(ActionEvent ev){
        System.out.println("MunicipalityManageBB.onEditMuniModulesAbort");
        editModeMuniPrintStyleAndHeader = false;
        
    }
    
    
    /**
     * Listener for user requests to install a new header blob
     * @param headerBlob 
     */
    
    public void onSetHeaderImage(BlobLight headerBlob){
        if(currentMuniDataheavy != null){
            currentMuniDataheavy.setDefaultMuniHeaderImage(headerBlob);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Default header photo chosen with ID:" + headerBlob.getPhotoDocID(), ""));
        }
    }
    
    /***************************************************************************
     ********************** ZIP CODES ****************************************** 
     **************************************************************************/
     
    
    
   
  
    /**
     * Listener for user requests to read in the session CSZ into the local  UI
     * @param ev 
     */
    public void onMuniZipLookupSessionZip(ActionEvent ev){
        if(getSessionBean().getSessMailingCityStateZip() != null){
            System.out.println("MunicipalityManageBB.onMuniZipLookupSessionZip: " + getSessionBean().getSessMailingCityStateZip().getZipCode());
        } else {
            System.out.println("MunicipalityManageBB.onMuniZipLookupSessionZip: NO SESSION ZIP");
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No ZIP Code has been loaded; please follow steps in panel!", ""));
        }
    }
     
    
    
    /**
     * Listener for users to add a selected zip to the current muni
     * @param ev
     */
    public void onMuniZipAddSessionZIPToMuni(ActionEvent ev){
        MunicipalityCoordinator mc = getMuniCoordinator();
        if(currentMuniDataheavy != null && currentMuniDataheavy.getZipList() != null){
            if(getSessionBean().getSessMailingCityStateZip() != null){
                currentMuniDataheavy.getZipList().add(getSessionBean().getSessMailingCityStateZip());
                try {
                    mc.updateMuniZIPLinks(currentMuniDataheavy);
                    getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Added ZIP Code from muni", ""));
                } catch (IntegrationException | BObStatusException ex) {
                    System.out.println(ex);
                    getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unable to link ZIP Code to muni; Check that this is not a duplicate ZIP Code.", ""));
                     currentMuniDataheavy.getZipList().remove(getSessionBean().getSessMailingCityStateZip());
                } 
            } else {
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No Session ZIP Code to link, sorry; Try selecting one again, then report the issue if error persists", ""));
            }
        }
    }
    
    
    
    
    
    /**
     * Listener for user requests to remove a zip from a given muni data heavy
     * @param csz 
     */
    public void onRemoveZIPCodeFromCurrentMDH(MailingCityStateZip csz){
        MunicipalityCoordinator mc = getMuniCoordinator();
        if(currentMuniDataheavy != null && currentMuniDataheavy.getZipList() != null && csz != null){
            currentMuniDataheavy.getZipList().remove(csz);
            try {
                mc.updateMuniZIPLinks(currentMuniDataheavy);
                getFacesContext().addMessage(null, 
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Removed ZIP Code from muni", ""));
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unable to remove ZIP Code muni links", ""));
                
            } 
        }
    }
    
    
    
    /***************************************************************************
     ********************** BLOBS  ********************************************** 
     **************************************************************************/
     
      
    /**
     * Special getter for muni blobs
     * @return 
     */
    public List<BlobLight> getManagedMuniBlobList(){
        List<BlobLight> blist = getSessionBean().getSessBlobLightListForRefreshUptake();
        if(currentMuniDataheavy != null){
            if(blist != null 
                && currentMuniDataheavy != null 
                && getSessionBean().getSessBlobLightListHolderForRefreshUptake() != null
                && getSessionBean().getSessBlobLightListHolderForRefreshUptake().getBlobLinkEnum() == BlobLinkEnum.MUNICIPALITY
                && getSessionBean().getSessBlobLightListHolderForRefreshUptake().getParentObjectID() == currentMuniDataheavy.getMuniCode()){
                    currentMuniDataheavy.setBlobList(blist);
                    getSessionBean().resetBlobRefreshUptakeFields();
            }
            return currentMuniDataheavy.getBlobList();
        }
        return new ArrayList<>();
    }
    
    
    /***************************************************************************
     *********************** MUNI-SPECIFIC NOTES ******************************* 
     **************************************************************************/
     
    private String formNotesMuni;
    private String formNotesMuniProfile;
    /**
     * listener for user requests to start the note process
     * @param ev 
     */
    public void onMuniNoteAddInitButtonChange(ActionEvent ev){
        System.out.println("MuniManageBB.onMuniNoteAddInitButtonChange");
        formNotesMuni = "";
    }
    
    
    
    /**
     * Completes the note writing process on munis
     * @param ev 
     */
    public void onMuniNoteCommitButtonChange(ActionEvent ev){
        System.out.println("MunicipalityManageBB.onMuniNoteCommitButtonChange");
        SystemCoordinator sc = getSystemCoordinator();
        if(currentMuniDataheavy != null){
            MessageBuilderParams mbp = new MessageBuilderParams(currentMuniDataheavy.getNotes(), "MUNI NOTE", null, formNotesMuni, getSessionBean().getSessUser(), null);
            currentMuniDataheavy.setNotes(sc.appendNoteBlock(mbp));
            try {
                sc.writeNotes(currentMuniDataheavy, getSessionBean().getSessUser());
                refreshCurrentMuniDataHeavy();
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Muni Note write: success!", ""));
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Note write error: " + ex.getMessage(), ""));
            } 
            formNotesMuni = "";
        }
    }
    
    
  
    
    /**
     * Listener for user requests to abort the note proces on either
     * the muni or muni profile
     * @param ev 
     */
    public void onNoteOperationCancelButtonChange(ActionEvent ev){
        formNotesMuni = "";
        formNotesMuniProfile = "";
    }
    
    
    /***************************************************************************
     *************** SYSTEM-WIDE MUNI PERMISSIONS PROFILE MANAGEMENT *********** 
     **************************************************************************/
    
    
    private MuniProfile currentMuniProfile;
    private boolean includeInactiveProfiles;
    private boolean editModeMuniProfile;
    
      /**
     * For selecting the linked object role to use when filtering property search results.
     * Towns with well-structured scrapes will want to filter out property address only, 
     * but other towns might want no filter applied.
     */
    private List<LinkedObjectRole> propertyLinkRoleCandidateList;
    
    /**
     * Listener for user requests to view permissions profiles
     * @param ev 
     */
    public void onViewProfilesLinkClick(ActionEvent ev){
        
        System.out.println("MunicipalityManageBB.onViewProfilesLinkClick");
        
    }
    
    
    
  
    
    /**
     * Listener for user request to view a profile
     * @param profile 
     */
    public void viewMuniProfile(MuniProfile profile){
        currentMuniProfile = profile;
    }
    
    
     
    /**
     * Extracts a fresh copy of the current muni data heavy from the db
     */
    public void refreshCurrentMuniProfile(){
        MunicipalityCoordinator mc = getMuniCoordinator();
        
        if(currentMuniProfile != null){
            try {
                currentMuniProfile = mc.getMuniProfile(currentMuniProfile.getProfileID());
                getFacesContext().addMessage(null, 
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Refreshed Muni profile: " + currentMuniProfile.getProfileID(), ""));
            } catch (IntegrationException | BObStatusException  ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null, 
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unable to extract muni data heavy from DB", ""));
            }
        }
    }
    
    /**
     * Gets a fresh list of munis from the DB
     */
    public void refreshMuniProfileList(){
        MunicipalityCoordinator mc = getMuniCoordinator();
        try {
            muniProfileCandidateList = mc.getMuniProfilesList(includeInactiveProfiles);
        } catch (IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, 
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unable to load muni profile heavy list", ""));
        } 
    }
    
    
    

    /**
     * Begins the muni profile creation process
     */
    public void onMuniProfileAddInit(){
        MunicipalityCoordinator mp = getMuniCoordinator();
        System.out.println("MunicipalityManageBB.onMuniProfileAddInit");
        currentMuniProfile = mp.getMuniProfileSkeleton(getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
        editModeMuniProfile = true;
    }
    
      /**
     * Listener for users to click the add or the edit button
     * @param ev
     */
    public void toggleMuniProfileAddEditButtonChange(ActionEvent ev){
        System.out.println("MunicpaltiyManageBB.toggleMuniProfileAddEditButtonChange: incoming: " + editModeMuniProfile);
        if(editModeMuniProfile){
            if(currentMuniProfile != null && currentMuniProfile.getProfileID() == 0){
                onInsertMuniProfileCommit();
            } else {
                onUpdateMuniProfileCommit();
                refreshCurrentMuniProfile();
            }
            refreshMuniProfileList();
        }
        editModeMuniProfile = !editModeMuniProfile;
    }
      
    
    /**
     * Finalizes the muni update process
     */
    public void onUpdateMuniProfileCommit() {
        MunicipalityCoordinator mc = getMuniCoordinator();
        try {
            mc.updateMuniProfile(currentMuniProfile, 
                                getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                    "Profile Updated with ID: " + currentMuniProfile.getProfileID(), ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    "Fail To Update Municipality", ""));
        }
    }

    /**
     * Finalizes the muni insert process
     */
    public void onInsertMuniProfileCommit() {
        MunicipalityCoordinator mc = getMuniCoordinator();
        try {
            currentMuniProfile = mc.insertMuniProfile(currentMuniProfile, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                    "Successful Insert New Permissions Profile!", ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        } 
        
    }

    /**
     * Finalizes the muni deac process
     */
    public void onDeactivateMuniProfileCommit() {
        MunicipalityCoordinator mc = getMuniCoordinator();
        try {
            mc.deactivateMuniProfile(currentMuniProfile, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Muni profile deactivated with ID: " + currentMuniProfile.getProfileID(), ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fatal error deactivating muni profile; Please report", ""));
        }
    }
    
    
    
    /**
     * Listener for user requests to stop the editing process of muni info
     * @param ev 
     */
    public void onMuniProfileAddEditOperationAbort(ActionEvent ev){
        editModeMuniProfile = false;
    }
      
    
    /**
     * Listener for user requests to start the note process on
     * municipality permissions profiles
     * @param ev 
     */
    public void onMuniProfileNoteAddInitButtonChange(ActionEvent ev){
        formNotesMuniProfile = "";
        
        
    }
    
    public void onMuniProfileNoteAbortButtonChange(ActionEvent ev){
        formNotesMuniProfile = "";
    }
    
    
    
    /**
     * Completes the note writing process on munis
     * @param ev 
     */
    public void onMuniProfileNoteCommitButtonChange(ActionEvent ev){
        SystemCoordinator sc = getSystemCoordinator();
        if(currentMuniProfile != null){
            MessageBuilderParams mbp = new MessageBuilderParams(currentMuniProfile.getNotes(), "MUNI PROFILE NOTE", null, formNotesMuniProfile, getSessionBean().getSessUser(), null);
            currentMuniProfile.setNotes(sc.appendNoteBlock(mbp));
            try {
                sc.writeNotes(currentMuniProfile, getSessionBean().getSessUser());
                refreshCurrentMuniProfile();
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Muni Profile Note write: success!", ""));
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Note write error: " + ex.getMessage(), ""));
            } 
            formNotesMuniProfile = "";
        }
    }
    
    
      
    /**
     * Listener for user requests to stop the editing process of muni profile
     * @param ev 
     */
    public void onMuniProfileSelectEditOperationAbortButtonChange(ActionEvent ev){
        editModeMuniProfile = false;
    }

   
    
    /***************************************************************************
     *************** SYSTEM-WIDE PRINT STYLE MANAGEMENT ************************
     **************************************************************************/
      private PrintStyle currentPrintStyle;
    private boolean editModePrintStyle;
     
    
    public void onViewPrintStylesLinkClick(ActionEvent ev){
        
        System.out.println("MunicipalityManageBB.onViewPrintStylesLinkClick");
        
    }
    
    
    
    
    
    /**
     * Listener for user request to view a profile
     * @param ps
     */
    public void viewPrintStyle(PrintStyle ps){
        currentPrintStyle = ps;
    }
    
    /**
     * Initiates a new print style object
     * 
     * @param ev 
     */
    public void onPrintStyleAddInit(ActionEvent ev){
        SystemCoordinator sc = getSystemCoordinator();
        currentPrintStyle = sc.getPrintStyleSkeleton();
        editModePrintStyle = true;
    }
    
    
    /**
     * Listener for users to click the add or the edit button
     * @param ev
     */
    public void togglePrintStyleAddEditButtonChange(ActionEvent ev){
        SystemCoordinator sc = getSystemCoordinator();
        System.out.println("MunicpaltiyManageBB.togglePrintStyleAddEditButtonChange: incoming: " + editModePrintStyle);
        if(editModePrintStyle){
            try {
                if(currentPrintStyle != null && currentPrintStyle.getStyleID() == 0){
                    int freshStyleID = sc.insertPrintStyle(currentPrintStyle);
                    currentPrintStyle = sc.getPrintStyle(freshStyleID);
                    getFacesContext().addMessage(null, 
                          new FacesMessage(FacesMessage.SEVERITY_INFO, "Print style insert success with ID: " + currentPrintStyle.getStyleID(), ""));
                } else {
                    sc.updatePrintStyle(currentPrintStyle, getSessionBean().getSessUser());
                    currentPrintStyle = sc.getPrintStyle(currentPrintStyle.getStyleID());
                    getFacesContext().addMessage(null, 
                          new FacesMessage(FacesMessage.SEVERITY_INFO, "Print style update success with ID: " + currentPrintStyle.getStyleID(), ""));
                }
                refreshPrintStyleList();
            } catch (BObStatusException | IntegrationException ex) {
                System.out.println(ex);
                  getFacesContext().addMessage(null, 
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unable to update or insert print style", ""));
            }
        }
        editModePrintStyle = !editModePrintStyle;
    }
    
    
    
    /**
     * Listener for user requests to stop the editing process of a PrintStyle
     * @param ev 
     */
    public void onPrintStyleAddEditOperationAbort(ActionEvent ev){
        editModePrintStyle = false;
    }
      
    
    
    /**
     * Gets a fresh list of munis from the DB
     */
    public void refreshPrintStyleList(){
        SystemCoordinator sc = getSystemCoordinator();
        try {
            printStyleCandidateList = sc.getPrintStyleList();
        } catch (IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, 
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unable to refresh print style list", ""));
        } 
    }
    
     /**************************************************************************
     *********************** GETTERS AND SETTERS *******************************
     **************************************************************************/
     
    
    /**
     * @return the styleMap
     */
    public Map<String, Integer> getStyleMap() {
        return styleMap;
    }

    /**
     * @param styleMap the styleMap to set
     */
    public void setStyleMap(Map<String, Integer> styleMap) {
        this.styleMap = styleMap;
    }
    
    public MunicipalityDataHeavy getCurrentMuniDataheavy() {
        return currentMuniDataheavy;
    }

    public void setCurrentMuniDataheavy(MunicipalityDataHeavy currentMuniDataheavy) {
        this.currentMuniDataheavy = currentMuniDataheavy;
    }


    public List<CodeSet> getCodeSetCandidateList() {
        return codeSetCandidateList;
    }

    public void setCodeSetCandidateList(List<CodeSet> codeSetCandidateList) {
        this.codeSetCandidateList = codeSetCandidateList;
    }

    public List<CodeSource> getCodeSourceCandidateList() {
        return codeSourceCandidateList;
    }

    public void setCodeSourceCandidateList(List<CodeSource> codeSourceCandidateList) {
        this.codeSourceCandidateList = codeSourceCandidateList;
    }

    public List<MuniProfile> getMuniProfileCandidateList() {
        return muniProfileCandidateList;
    }

    public void setMuniProfileCandidateList(List<MuniProfile> muniProfileCandidateList) {
        this.muniProfileCandidateList = muniProfileCandidateList;
    }

    public List<User> getUserCandidateListe() {
        return userCandidateListe;
    }

    public void setUserCandidateListe(List<User> userCandidateListe) {
        this.userCandidateListe = userCandidateListe;
    }

    public List<PrintStyle> getPrintStyleCandidateList() {
        return printStyleCandidateList;
    }

    public void setPrintStyleCandidateList(List<PrintStyle> printStyleCandidateList) {
        this.printStyleCandidateList = printStyleCandidateList;
    }

    public List<OccPeriod> getOccPeriodCandidateList() {
        return occPeriodCandidateList;
    }

    public void setOccPeriodCandidateList(List<OccPeriod> occPeriodCandidateList) {
        this.occPeriodCandidateList = occPeriodCandidateList;
    }

   
    /**
     * @return the muniList
     */
    public List<Municipality> getMuniList() {
        return muniList;
    }

    /**
     * @param muniList the muniList to set
     */
    public void setMuniList(List<Municipality> muniList) {
        this.muniList = muniList;
    }

    /**
     * @return the includeInactiveMunis
     */
    public boolean isIncludeInactiveMunis() {
        return includeInactiveMunis;
    }

    /**
     * @return the editModeMuniInfo
     */
    public boolean isEditModeMuniInfo() {
        return editModeMuniInfo;
    }

    /**
     * @return the currentMuniProfile
     */
    public MuniProfile getCurrentMuniProfile() {
        return currentMuniProfile;
    }

    /**
     * @return the includeInactiveProfiles
     */
    public boolean isIncludeInactiveProfiles() {
        return includeInactiveProfiles;
    }

    /**
     * @return the editModeMuniProfile
     */
    public boolean isEditModeMuniProfile() {
        return editModeMuniProfile;
    }

    /**
     * @param includeInactiveMunis the includeInactiveMunis to set
     */
    public void setIncludeInactiveMunis(boolean includeInactiveMunis) {
        this.includeInactiveMunis = includeInactiveMunis;
    }

    /**
     * @param editModeMuniInfo the editModeMuniInfo to set
     */
    public void setEditModeMuniInfo(boolean editModeMuniInfo) {
        this.editModeMuniInfo = editModeMuniInfo;
    }

    /**
     * @param currentMuniProfile the currentMuniProfile to set
     */
    public void setCurrentMuniProfile(MuniProfile currentMuniProfile) {
        this.currentMuniProfile = currentMuniProfile;
    }

    /**
     * @param includeInactiveProfiles the includeInactiveProfiles to set
     */
    public void setIncludeInactiveProfiles(boolean includeInactiveProfiles) {
        this.includeInactiveProfiles = includeInactiveProfiles;
    }

    /**
     * @param editModeMuniProfile the editModeMuniProfile to set
     */
    public void setEditModeMuniProfile(boolean editModeMuniProfile) {
        this.editModeMuniProfile = editModeMuniProfile;
    }

   
    /**
     * @return the formNotesMuni
     */
    public String getFormNotesMuni() {
        return formNotesMuni;
    }

    /**
     * @return the formNotesMuniProfile
     */
    public String getFormNotesMuniProfile() {
        return formNotesMuniProfile;
    }

    /**
     * @param formNotesMuni the formNotesMuni to set
     */
    public void setFormNotesMuni(String formNotesMuni) {
        this.formNotesMuni = formNotesMuni;
    }

    /**
     * @param formNotesMuniProfile the formNotesMuniProfile to set
     */
    public void setFormNotesMuniProfile(String formNotesMuniProfile) {
        this.formNotesMuniProfile = formNotesMuniProfile;
    }

    /**
     * @return the editModePrintStyle
     */
    public boolean isEditModePrintStyle() {
        return editModePrintStyle;
    }

    /**
     * @param editModePrintStyle the editModePrintStyle to set
     */
    public void setEditModePrintStyle(boolean editModePrintStyle) {
        this.editModePrintStyle = editModePrintStyle;
    }

    /**
     * @return the currentPrintStyle
     */
    public PrintStyle getCurrentPrintStyle() {
        return currentPrintStyle;
    }

    /**
     * @param currentPrintStyle the currentPrintStyle to set
     */
    public void setCurrentPrintStyle(PrintStyle currentPrintStyle) {
        this.currentPrintStyle = currentPrintStyle;
    }

    /**
     * @return the occPermitTypeCandidateList
     */
    public List<OccPermitType> getOccPermitTypeCandidateList() {
        return occPermitTypeCandidateList;
    }

    /**
     * @param occPermitTypeCandidateList the occPermitTypeCandidateList to set
     */
    public void setOccPermitTypeCandidateList(List<OccPermitType> occPermitTypeCandidateList) {
        this.occPermitTypeCandidateList = occPermitTypeCandidateList;
    }

    /**
     * @return the courtEntityCandidateList
     */
    public List<CourtEntity> getCourtEntityCandidateList() {
        return courtEntityCandidateList;
    }

    /**
     * @param courtEntityCandidateList the courtEntityCandidateList to set
     */
    public void setCourtEntityCandidateList(List<CourtEntity> courtEntityCandidateList) {
        this.courtEntityCandidateList = courtEntityCandidateList;
    }

    /**
     * @return the countyCandidateList
     */
    public List<County> getCountyCandidateList() {
        return countyCandidateList;
    }

    /**
     * @param countyCandidateList the countyCandidateList to set
     */
    public void setCountyCandidateList(List<County> countyCandidateList) {
        this.countyCandidateList = countyCandidateList;
    }

    /**
     * @return the useSessionParcel
     */
    public boolean isUseSessionParcel() {
        return useSessionParcel;
    }

    /**
     * @param useSessionParcel the useSessionParcel to set
     */
    public void setUseSessionParcel(boolean useSessionParcel) {
        this.useSessionParcel = useSessionParcel;
    }

    /**
     * @return the updateToSelectedMailingAddress
     */
    public boolean isUpdateToSelectedMailingAddress() {
        return updateToSelectedMailingAddress;
    }

    /**
     * @param updateToSelectedMailingAddress the updateToSelectedMailingAddress to set
     */
    public void setUpdateToSelectedMailingAddress(boolean updateToSelectedMailingAddress) {
        this.updateToSelectedMailingAddress = updateToSelectedMailingAddress;
    }

    /**
     * @return the updateToSelectedOccPeriod
     */
    public boolean isUpdateToSelectedOccPeriod() {
        return updateToSelectedOccPeriod;
    }

    /**
     * @param updateToSelectedOccPeriod the updateToSelectedOccPeriod to set
     */
    public void setUpdateToSelectedOccPeriod(boolean updateToSelectedOccPeriod) {
        this.updateToSelectedOccPeriod = updateToSelectedOccPeriod;
    }

    /**
     * @return the mailingAddressCandidateList
     */
    public List<MailingAddressLink> getMailingAddressCandidateList() {
        return mailingAddressCandidateList;
    }

    /**
     * @param mailingAddressCandidateList the mailingAddressCandidateList to set
     */
    public void setMailingAddressCandidateList(List<MailingAddressLink> mailingAddressCandidateList) {
        this.mailingAddressCandidateList = mailingAddressCandidateList;
    }

    /**
     * @return the selectedMailingAddress
     */
    public MailingAddressLink getSelectedMailingAddress() {
        return selectedMailingAddress;
    }

    /**
     * @param selectedMailingAddress the selectedMailingAddress to set
     */
    public void setSelectedMailingAddress(MailingAddressLink selectedMailingAddress) {
        this.selectedMailingAddress = selectedMailingAddress;
    }

    /**
     * @return the selectedOccPeriod
     */
    public OccPeriod getSelectedOccPeriod() {
        return selectedOccPeriod;
    }

    /**
     * @param selectedOccPeriod the selectedOccPeriod to set
     */
    public void setSelectedOccPeriod(OccPeriod selectedOccPeriod) {
        this.selectedOccPeriod = selectedOccPeriod;
    }

    /**
     * @return the updateToSelectedHeaderImage
     */
    public boolean isUpdateToSelectedHeaderImage() {
        return updateToSelectedHeaderImage;
    }

    /**
     * @param updateToSelectedHeaderImage the updateToSelectedHeaderImage to set
     */
    public void setUpdateToSelectedHeaderImage(boolean updateToSelectedHeaderImage) {
        this.updateToSelectedHeaderImage = updateToSelectedHeaderImage;
    }

    /**
     * @return the selectedHeaderBlob
     */
    public BlobLight getSelectedHeaderBlob() {
        return selectedHeaderBlob;
    }

    /**
     * @param selectedHeaderBlob the selectedHeaderBlob to set
     */
    public void setSelectedHeaderBlob(BlobLight selectedHeaderBlob) {
        this.selectedHeaderBlob = selectedHeaderBlob;
    }

    /**
     * @return the updateCountyListToSelected
     */
    public boolean isUpdateCountyListToSelected() {
        return updateCountyListToSelected;
    }

    /**
     * @param updateCountyListToSelected the updateCountyListToSelected to set
     */
    public void setUpdateCountyListToSelected(boolean updateCountyListToSelected) {
        this.updateCountyListToSelected = updateCountyListToSelected;
    }

    /**
     * @return the countyListSelected
     */
    public List<County> getCountyListSelected() {
        return countyListSelected;
    }

    /**
     * @param countyListSelected the countyListSelected to set
     */
    public void setCountyListSelected(List<County> countyListSelected) {
        this.countyListSelected = countyListSelected;
    }

    /**
     * @return the updateToSelectedCourts
     */
    public boolean isUpdateToSelectedCourts() {
        return updateToSelectedCourts;
    }

    /**
     * @return the courtEntitiesSelected
     */
    public List<CourtEntity> getCourtEntitiesSelected() {
        return courtEntitiesSelected;
    }

    /**
     * @param updateToSelectedCourts the updateToSelectedCourts to set
     */
    public void setUpdateToSelectedCourts(boolean updateToSelectedCourts) {
        this.updateToSelectedCourts = updateToSelectedCourts;
    }

    /**
     * @param courtEntitiesSelected the courtEntitiesSelected to set
     */
    public void setCourtEntitiesSelected(List<CourtEntity> courtEntitiesSelected) {
        this.courtEntitiesSelected = courtEntitiesSelected;
    }

    /**
     * @return the muniCreateMode
     */
    public boolean isMuniCreateMode() {
        return muniCreateMode;
    }

    /**
     * @param muniCreateMode the muniCreateMode to set
     */
    public void setMuniCreateMode(boolean muniCreateMode) {
        this.muniCreateMode = muniCreateMode;
    }

    /**
     * @return the propertyLinkRoleCandidateList
     */
    public List<LinkedObjectRole> getPropertyLinkRoleCandidateList() {
        return propertyLinkRoleCandidateList;
    }

    /**
     * @param propertyLinkRoleCandidateList the propertyLinkRoleCandidateList to set
     */
    public void setPropertyLinkRoleCandidateList(List<LinkedObjectRole> propertyLinkRoleCandidateList) {
        this.propertyLinkRoleCandidateList = propertyLinkRoleCandidateList;
    }

    /**
     * @return the eventCategoryCandidateList
     */
    public List<EventCategory> getEventCategoryCandidateList() {
        return eventCategoryCandidateList;
    }

    /**
     * @param eventCategoryCandidateList the eventCategoryCandidateList to set
     */
    public void setEventCategoryCandidateList(List<EventCategory> eventCategoryCandidateList) {
        this.eventCategoryCandidateList = eventCategoryCandidateList;
    }

    /**
     * @return the editModeMuniModules
     */
    public boolean isEditModeMuniModules() {
        return editModeMuniModules;
    }

    /**
     * @param editModeMuniModules the editModeMuniModules to set
     */
    public void setEditModeMuniModules(boolean editModeMuniModules) {
        this.editModeMuniModules = editModeMuniModules;
    }

    /**
     * @return the editModeMuniPermissionsProfile
     */
    public boolean isEditModeMuniPermissionsProfile() {
        return editModeMuniPermissionsProfile;
    }

    /**
     * @param editModeMuniPermissionsProfile the editModeMuniPermissionsProfile to set
     */
    public void setEditModeMuniPermissionsProfile(boolean editModeMuniPermissionsProfile) {
        this.editModeMuniPermissionsProfile = editModeMuniPermissionsProfile;
    }

    /**
     * @return the editModeMuniPrintStyleAndHeader
     */
    public boolean isEditModeMuniPrintStyleAndHeader() {
        return editModeMuniPrintStyleAndHeader;
    }

    /**
     * @param editModeMuniPrintStyleAndHeader the editModeMuniPrintStyleAndHeader to set
     */
    public void setEditModeMuniPrintStyleAndHeader(boolean editModeMuniPrintStyleAndHeader) {
        this.editModeMuniPrintStyleAndHeader = editModeMuniPrintStyleAndHeader;
    }

    /**
     * @return the editModeMuniObjectLinks
     */
    public boolean isEditModeMuniObjectLinks() {
        return editModeMuniObjectLinks;
    }

    /**
     * @param editModeMuniObjectLinks the editModeMuniObjectLinks to set
     */
    public void setEditModeMuniObjectLinks(boolean editModeMuniObjectLinks) {
        this.editModeMuniObjectLinks = editModeMuniObjectLinks;
    }

    /**
     * @return the inspectionChecklistCandidateList
     */
    public List<OccChecklistTemplate> getInspectionChecklistCandidateList() {
        return inspectionChecklistCandidateList;
    }

    /**
     * @param inspectionChecklistCandidateList the inspectionChecklistCandidateList to set
     */
    public void setInspectionChecklistCandidateList(List<OccChecklistTemplate> inspectionChecklistCandidateList) {
        this.inspectionChecklistCandidateList = inspectionChecklistCandidateList;
    }

   


}
