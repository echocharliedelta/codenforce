/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.session.SessionBean;
import com.tcvcog.tcvce.coordinators.EventCoordinator;
import com.tcvcog.tcvce.coordinators.PermissionsCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.EventException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.domain.SearchException;
import com.tcvcog.tcvce.entities.*;
import com.tcvcog.tcvce.util.MessageBuilderParams;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.List;
import jakarta.faces.context.FacesContext;
import jakarta.faces.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.tcvcog.tcvce.application.interfaces.IFaceActivatableBOB;
/**
 * The premier backing bean for universal events panel workflow.
 *
 * @author jurplel and Ellen Bascomb of Apartment 31Y
 */
public class EventBB extends BackingBeanUtils implements Serializable {
    
    static final String EVENT_HOLDER_TO_REFRESH_PARAM_KEY = "event-list-component-to-update";
    
    private EventRealm pageDomain;
    private boolean eventEditMode;
    private EventCnF currentEvent;
    private EventCategory currentEventCategory;
    
    /**
     * When this field is not null, the current event has the chance to become
     * the follow up event to this member
     */
    private EventCnF eventFollowUpTarget;
    
    private EventCnF alertEventForStoppage;
    /**
     * Set to true during the alert stop event process so users 
     * cannot choose some random category and therefore NOT STOP their alert
     */
    private boolean alertStopEventPathwayActive;
    private boolean generalEventPathwayActive;
    
    private boolean nonEventHolderSpecificPageAdd;
    
    private boolean permissionAllowEventEditDeac;

    private IFace_EventHolder currentEventHolder;
    private String eventListComponentForRefreshTrigger;

    private Map<EventType, List<EventCategory>> typeCategoryMap;

    // form logic stuff
    private String formNoteText;
    private long formEventDuration;

    // skeleton values used in the the new event form (subset of form logic stuff I guess)
    private EventCnF skeletonEvent;

    private boolean updateFieldsOnCategoryChange;

    private long skeletonDuration;
    private EventType skeletonType;

    private boolean includeDeactivatedEvent;

    private List<EventCnF> selectedEvents;
    private boolean selectionMode;

    public EventBB() {
    }

    @PostConstruct
    public void initBean() {
        loadSessionEventHolder();
        configureTypeCategoryMapUsingCurrentEventHolder();
        // Find event holder and setup event list
        configurePermissionsOnCurrentEvent();
    }
    
    /**
     * Determines which types and hence which categories to display
     * on event add dialog
     */
    private void configureTypeCategoryMapUsingCurrentEventHolder(){
        EventCoordinator ec = getEventCoordinator();
        try {
            if(pageDomain != null && currentEventHolder != null){
                typeCategoryMap = ec.assembleEventTypeCatMap_toEnact(pageDomain, currentEventHolder, getSessionBean().getSessUser());
            }
        } catch (BObStatusException ex) {
            System.out.println(ex.getMessage());
        }
        configurePermissionsOnCurrentEvent();
        selectionMode = false;
    }

    /**
     * Asks the permissions coordinator for a fresh determination concerning the
     * current event's edit and deac rights based on the current user's rank and
     * creator status
     */
    private void configurePermissionsOnCurrentEvent() {
        EventCoordinator ec = getEventCoordinator();
        permissionAllowEventEditDeac = ec.permissionsCheckpointEventEditDeac(currentEventHolder, currentEvent, getSessionBean().getSessUser());
    }

    /**
     * Grabs the current page event type from the session bean and uses that to
     * populate the currentEventHolder object with something we can grab events
     * from!
     */
    public void loadSessionEventHolder() {
        SessionBean sb = getSessionBean();

//        pageDomain = getSessionEventConductor().getSessEventsPageEventDomainRequest();
        pageDomain = getSessionBean().getSessionDomain().getEventRealmMapping();
        switch (pageDomain) {
            case CODE_ENFORCEMENT:
                currentEventHolder = sb.getSessCECase();
                break;
            case OCCUPANCY:
                currentEventHolder = sb.getSessOccPeriod();
                break;
            case PARCEL:
                currentEventHolder = sb.getSessProperty();
                break;
            case UNIVERSAL:
                System.out.println("eventBB reached universal case in loadSessionEventHolder; no object-specific event list to display");
                break;
        }
        refreshEventHolderListAndTriggerSessionReload();
        System.out.println("EventBB.loadSessionEventHolder page domain | " + pageDomain.getTitle());
    }

    /**
     * Grabs a new set of events from the DB and sends it to the session bean
     * and the UI will trigger the event holder's management BB to get a new
     * copy of the session event list and display that to the user
     */
    public void refreshEventHolderListAndTriggerSessionReload() {
        EventCoordinator ec = getEventCoordinator();
        if (currentEventHolder != null) {
            try {
                System.out.println("EventBB.refreshEventHolderListAndTriggerSessionReload | eventHolder ID: " + currentEventHolder.getBObID());
                getSessionEventConductor().setSessEventListForRefreshUptake(ec.getEventList(currentEventHolder));
               
            } catch (IntegrationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        ex.getMessage(), ""));

            }
            if(currentEventHolder instanceof IFaceActivatableBOB abob){
                getSessionBean().refreshActivatableObject(abob);
            }
        }
    }

    /**
     * Retrieves a new copy of our current event
     */
    private void refreshCurrentEvent() {
        EventCoordinator ec = getEventCoordinator();
        if (currentEvent != null) {
            try {
                currentEvent = ec.getEvent(currentEvent.getEventID());
                configurePermissionsOnCurrentEvent();
                System.out.println("eventBB.refreshCurrentEvent: Refreshed Event ID " + currentEvent.getEventID());
            } catch (IntegrationException ex) {
                System.out.println(ex);
            }
        }
    }

    /**
     * Special getter for event lists on the host pages of objects that have
     * events, which in FEB 2023 were CECases, Occ Periods (permit files), and
     * Parcels
     *
     * @return the events
     */
    public List<EventCnF> getManagedEventList() {
        EventCoordinator ec = getEventCoordinator();
        if (currentEventHolder != null) {
            List<EventCnF> evlist = getSessionEventConductor().getSessEventListForRefreshUptake();
            if (evlist != null) {
                currentEventHolder.setEventList(evlist);
                getSessionEventConductor().setSessEventListForRefreshUptake(null);
            }
            return ec.filterEventListForViewFloor(currentEventHolder.getEventList(), getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod().getRole());
        } else {
            return new ArrayList<>();
        }
    }
    
    
    
    /**
     * Listener to view current event
     *
     * @param ev
     */
    public void onViewEvent(EventCnF ev) {
        System.out.println("EventBB.onViewEvnt: " + ev.getEventID());
        currentEvent = ev;
        extractEventListComponentForRefresh();
        refreshCurrentEvent();
        configurePermissionsOnCurrentEvent();
    }

    /**
     * Listener for user requests to see a dialog with event cat details
     * @param ev 
     */
    public void onViewCurrentEventCategoryDetails(ActionEvent ev){
        // nothing to do, we are viewing our current event
        System.out.println("EventBB.onViewEventCategoryDetails");
        if(currentEvent != null){
            currentEventCategory = currentEvent.getCategory();
        }
    }
    
    /**
     * Listener to view a category's details during the event add process
     * @param ev
     */
    public void onViewEventCategoryDetails(ActionEvent ev){
        System.out.println("EventBB.onViewEventCategoryDetails " );
        if(skeletonEvent != null){
            currentEventCategory = skeletonEvent.getCategory();
            
        }
    }
    
    /**
     * Reaches into the request object to pull out a K:V pair for component
     * update
     */
    private void extractEventListComponentForRefresh() {

        eventListComponentForRefreshTrigger
                = FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getRequestParameterMap()
                        .get(EVENT_HOLDER_TO_REFRESH_PARAM_KEY);
           System.out.println("EventBB.extractEventListComponentForRefresh | " + eventListComponentForRefreshTrigger);
        
        
    }

    /**
     * Listener for when the user starts editing the person links to this
     * particular event
     */
    public void onManageEventPersonButtonChange() {
        System.out.println("eventBB.onManageEventPersonButtonChange");

    }

//      /**
//     * Checks boolean flag on cecase profile page's event list for including deactivated events
//     */
//    public void configureManagedEventList(){
//        List<EventCnF> weededEvList = new ArrayList<>();
//        if(managedEventList != null && !managedEventList.isEmpty()){
//            for(EventCnF ev: managedEventList){
//                boolean include = false;
//                if(ev.getDeactivatedTS() == null) include = true;
//                if(isIncludeDeactivatedEvents() && ev.getDeactivatedTS() != null) include = true;
//                if(include) weededEvList.add(ev);
//            }
//            managedEventList = weededEvList;
//        }
//    }
    /**
     * Listener for user requests to start or end the event editing process
     *
     * @param ev
     */
    public void onEventEditModeToggleButtonPress(ActionEvent ev) {
        System.out.println("eventBB.onEventEditModeToggleButtonPress | edit mode: " + eventEditMode);
        if (eventEditMode) {
            if (currentEvent != null && currentEvent.getEventID() != 0) {

                eventUpdateCommit();
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Success: Event update ID:" + getCurrentEvent().getEventID(), ""));
            } else {
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
                        "Mostly fatal error: null current event or evid=0: error code EV007", ""));
            }

        }
        eventEditMode = !eventEditMode;
    }

    /**
     * listener for user requests to cancel an even edit oepration
     *
     * @param ev
     */
    public void onEventEditCancelButtonChange(ActionEvent ev) {
        System.out.println("EventBB.onEventEditCancelButtonChange");
        eventEditMode = !eventEditMode;
        getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                "Edit aborted--changes discarded", ""));

    }

    /**
     * This method attempts to update the database entry for the selectedEvent.
     * It will fail in certain conditions, in which case the selectedEvent is
     * returned to a backup made before any current unsaved changes.
     */
    public void eventUpdateCommit() {
        EventCoordinator ec = getEventCoordinator();

        try {
            ec.updateEvent(currentEvent, currentEventHolder, getSessionBean().getSessUser());
            System.out.println("eventBB.saveEventChanges successful");
            refreshCurrentEvent();
            refreshEventHolderListAndTriggerSessionReload();
            getSessionEventConductor().refreshCalendarAndFollowupBacklog(null);
            // Set backup copy in case of failure if saving to database succeeds
//            lastSavedSelectedEvent = new EventCnF(currentEvent);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Event update successful with ID: " + currentEvent.getEventID(), ""));
        } catch (IntegrationException | BObStatusException | EventException | AuthorizationException ex) {
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
            System.out.println("eventBB.saveEventChanges failure");
            // don't turn off editing yet!
//            discardEventChanges();
        }
    }

    /**
     * This method will discard the changes to the current working event and set
     * its value to that of the event present in the last successful save.
     */
    public void discardEventChanges() {
        System.out.println("eventBB.discardEventChanges");
        eventEditMode = false;
    }

    /**
     * listener for user requests to start the vent remove process that applies
     * only to the bean's current event. I won't just take in any old random
     * event.
     *
     * @param ev
     */
    public void onEventDeactivateInit(ActionEvent ev) {
        if (currentEvent != null) {
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Beginning deactivation of event ID: " + currentEvent.getEventID(), ""));
            System.out.println("eventBB.onEventDeactivate ID: " + currentEvent.getEventID());

        } else {
            System.out.println("eventBB.onEventDeactivate | current event null");

        }
    }

    /**
     * Listener for user request to remove an event
     *
     * @param ev
     */
    public void onEventDeactivateCommit(ActionEvent ev) {
        EventCoordinator ec = getEventCoordinator();
        try {
            ec.deactivateEvent(currentEvent, currentEventHolder, getSessionBean().getSessUser());
            refreshCurrentEvent();
            refreshEventHolderListAndTriggerSessionReload();
            configurePermissionsOnCurrentEvent();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Removed event ID " + getCurrentEvent().getEventID(), ""));
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
    }

    /**
     * Listener for user cancels to event deac
     *
     * @param ev
     */
    public void onEventDeactivateAbort(ActionEvent ev) {
        System.out.println("EventBB.onEventDeactivateAbort");
    }

    /**
     * Listener for user requests to resurrect an event
     *
     * @param ev
     */
    public void onEventReactivateCommitButtonChange(ActionEvent ev) {
        EventCoordinator ec = getEventCoordinator();
        try {
            ec.reactivateEvent(currentEvent, getSessionBean().getSessUser());
            refreshCurrentEvent();
            refreshEventHolderListAndTriggerSessionReload();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Reactivated event ID " + getCurrentEvent().getEventID(), ""));
        } catch (IntegrationException | BObStatusException | EventException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }

    }

    /**
     * Updates event's end time using a given duration from the start time
     */
    public void updateTimeEndFromDuration() {
        if (getCurrentEvent().getTimeStart() != null) {
            getCurrentEvent().setTimeEnd(getCurrentEvent().getTimeStart().plusMinutes(formEventDuration));
        }
    }

    /**
     * Special listener for dashboard event adds that sets a flag to true to skipping
     * the session event holder list refresh trigger.
     * @param h 
     */
    public void onEventAddFromNonHolderSpecificpageInit(IFace_EventHolder h){
        nonEventHolderSpecificPageAdd = true;
        currentEventHolder = h;
        onEventAddInit(null);
    }

    //
    // New event stuff
    //
    /**
     * Listener to start the whole event creati
     * @param ev
     *
     */
    public void onEventAddInit(ActionEvent ev) {
        // Set potentialEvent to an empty event
        EventCoordinator ec = getEventCoordinator();
        if (currentEventHolder == null) {
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "EventBB.onEventAddInit: null event holder", ""));
        } else {
            pageDomain = currentEventHolder.getEventDomain();
        }
        configureTypeCategoryMapUsingCurrentEventHolder();

        try {
            skeletonEvent = ec.initEvent(currentEventHolder, null);
        } catch (BObStatusException | EventException ex) {
            System.out.println("Failed to initialize new event:" + ex);
            return;
        }

        // Set fields not included in potentialEvent to default values
        extractEventListComponentForRefresh();
        setUpdateFieldsOnCategoryChange(true);
        setSkeletonType(null);
        setSkeletonDuration(10);
    }
    /**
     * Primary listener for creating a new event
     */
    public void eventAddCommit() {
        if (pageDomain == null || currentEventHolder == null
                || skeletonEvent == null || skeletonEvent.getCategory() == null) {
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Event must have a category or I found a null page domain, event holder, or skeleton", ""));
            return;
        }

        EventCoordinator ec = getEventCoordinator();
        SessionBean sb = getSessionBean();
        // Add new event to database and to the event holder
        try {
            List<EventCnF> evlist = ec.addEventAsFollowUpEvent(skeletonEvent, currentEventHolder, sb.getSessUser(), eventFollowUpTarget);

            if (evlist != null && !evlist.isEmpty()) {
                for (EventCnF ev : evlist) {
                    currentEvent = ev;
                    getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Successfully logged event with an ID of  " + ev.getEventID(), ""));
                }
            }
        refreshEventHolderListAndTriggerSessionReload();
        refreshCurrentEvent();
        resetSpecialEventPathwaysAlertFollowupGeneral();
        getSessionEventConductor().refreshCalendarAndFollowupBacklog(null);
        } catch (BObStatusException | EventException | IntegrationException | AuthorizationException ex) {
            System.out.println("Failed to update new event with entered details:" + ex);
        }
    }
    
    /**
     * Listener for users stopping the event add process
     * @param ev 
     */
    public void onEventAddAbort(ActionEvent ev){
        System.out.println("EventBB.onEventAddAbort");
        resetSpecialEventPathwaysAlertFollowupGeneral();
    }
    
    /**
     * Listener for general events that are attached to the muni property
     * @param ev 
     */
    public void onEventAddGeneralInit(ActionEvent ev){
        System.out.println("EventBB.onEventAddGeneralInit");
        generalEventPathwayActive = true;
        currentEventHolder = getSessionBean().getSessMuni().getMuniPropertyDH();
        // follow normal procedure
        onEventAddInit(null);
        
    }
    
    /**
     * Stops the general event pathways
     */
    public void onEventAddGeneralAbort(){
        generalEventPathwayActive = false;
    }
    
    /**
     * Clears alert situation
     */
    private void resetAlertEventStoppage(){
        alertEventForStoppage = null;
        alertStopEventPathwayActive = false;
    }
    
    /**
     * writes null to our follow up member
     */
    private void clearFollowUpEventTarget(){
        eventFollowUpTarget = null;
    }
    
    /**
     * Turns off special pathways
     */
    private void resetSpecialEventPathwaysAlertFollowupGeneral(){
         clearFollowUpEventTarget();
        resetAlertEventStoppage();
        onEventAddGeneralAbort();
    }

    /**
     * Special getter for event reconfiguration and add
     *
     * @return
     */
    public List<EventType> getListOfPotentialTypes() {
        List<EventType> keys = null;
        if(typeCategoryMap != null){
            keys = new ArrayList(typeCategoryMap.keySet());
            // Sort the keys so they are always in the same order
            keys.sort((EventType et1, EventType et2) -> et1.getLabel().compareTo(et2.getLabel()));

            // Set a default potentialType if there isn't already one
            if (skeletonType == null) {
                skeletonType = keys.get(0);
            }
        } else {
            System.out.println("EventBB.getListOfPotentialTypes | no typeCategoryMap");
        }
        return keys;
    }

    /**
     * Special getter for event config
     *
     * @return
     */
    public List<EventCategory> getListOfPotentialCategories() {
        if (skeletonEvent != null && skeletonType != null && typeCategoryMap.containsKey(skeletonType)) {
            List<EventCategory> eventCategories = typeCategoryMap.get(skeletonType);
            // Set a default potentialCategory if there isn't already one in this list
            if (!eventCategories.contains(skeletonEvent.getCategory())) {
                if(!eventCategories.isEmpty()){
                    skeletonEvent.setCategory(eventCategories.get(0));
                    onCategoryChangeUpdateSkeletonEventMembers();
                }
            }
            return eventCategories;
        } else {
            return new ArrayList();
        }
    }

    /**
     * Sets fields of the skeleton event based on the default fields of the
     * skeleton event's category (if updateFieldsOnCategoryChange is set)
     */
    public void onCategoryChangeUpdateSkeletonEventMembers() {
        if (isUpdateFieldsOnCategoryChange() && skeletonEvent != null && getSkeletonEvent().getCategory() != null) {
            EventCategory category = skeletonEvent.getCategory();

            skeletonEvent.setTimeStart(LocalDateTime.now());
            skeletonDuration = category.getDefaultDurationMins();
            skeletonEvent.setDescription("");
            skeletonEvent.appendToDescription(category.getHostEventDescriptionSuggestedText());

            recalculateEndTime();
        }
    }

    /**
     * Sets end time of skeleton event based on the duration held in this class,
     * and the start time held in the skeleton event.
     */
    public void recalculateEndTime() {
        // Set end time of potential event to its start time + potential duration
        if (skeletonEvent == null || skeletonEvent.getTimeStart() == null) {
            return;
        }

        skeletonEvent.setTimeEnd(skeletonEvent.getTimeStart().plusMinutes(skeletonDuration));
    }

    /**
     * Checks the session bean. if the session bean has queued an EventCnF for
     * person list refresh, get a new copy of CurrentEvent with updated persons.
     * Then clear the session trigger.
     */
    private void checkForPersonListReloadTrigger() {
        if (currentEvent != null
                && getSessionBean().getSessHumanListRefreshedList() != null) {

            // clear refresh trigger
            getSessionBean().setSessHumanListRefreshedList(null);
        }
    }
    
    /**
     * Looks into the passed in event to set this bean's currentEventHolder
     * 
     * @param evForHolderExtraction 
     * @param holder the parent of the given event, and if null, I'll ask the coordinator to figure it out
     * and inject it in this bean's currentEventHolder member
     */
    private void configureEventHolderFromEvent(EventCnF evForHolderExtraction, IFace_EventHolder holder) throws IntegrationException, BObStatusException, SearchException, BlobException{
        if(evForHolderExtraction != null){
            if(holder != null){
                currentEventHolder = holder;
            } else {
                EventCoordinator ec = getEventCoordinator();
                currentEventHolder = ec.extractParentEventHolderFromEvent(currentEvent, getSessionBean().getSessUser());
            }
        }
    }
    
    /**
     * Listener for user requests to start the follow-up process for a given 
     * event; I will spawn the event add dialog with a properly configured skeleton
     * 
     * @param evToFollowUpOn cannot be null
     */
    public void onFollowUpToEventInit(EventCnF evToFollowUpOn){
        eventFollowUpTarget = evToFollowUpOn;
        currentEvent = evToFollowUpOn;
        try {
            configureEventHolderFromEvent(evToFollowUpOn, null);
            onEventAddInit(null);
        } catch (IntegrationException | BObStatusException | BlobException | SearchException  ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        } 
    }
    
    
    
    /**
     * Listener for user requests to start the follow-up process for a given 
     * event; I will spawn the event add dialog with a properly configured skeleton
     * 
     * @param alertToStop cannot be null
     */
    public void onStopALertEventInit(EventCnF alertToStop){
        System.out.println("EventBB.onSTopAlertEventInit | ID: " + alertToStop.getEventID());
        alertEventForStoppage = alertToStop;
        currentEvent = alertToStop;
        alertStopEventPathwayActive = true;
        // clear any followup settings
        eventFollowUpTarget = null;
        try {
            configureEventHolderFromEvent(alertToStop, null);
            onEventAddInit(null);
            configureSkeletonForAlertStoppage();
        } catch (IntegrationException | BObStatusException | BlobException | SearchException  ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        } 
    }
    
    /**
     * Extracts the stop alert category and makes our skeleton that cat
     */
    private void configureSkeletonForAlertStoppage(){
        if(alertEventForStoppage != null && skeletonEvent != null) {
            skeletonEvent.setCategory(alertEventForStoppage.getCategory().getAlertStopCategory());
            skeletonType = alertEventForStoppage.getCategory().getAlertStopCategory().getEventType();
        }
        onCategoryChangeUpdateSkeletonEventMembers();
        
    }
    
    

    /**
     * Listener for commencement of note writing process
     *
     * @param ev
     */
    public void onNoteInitButtonChange(ActionEvent ev) {
        formNoteText = new String();

    }

    /**
     * Listener for user requests to commit new note content to the current
     * Event
     *
     * @param ev
     */
    public void onNoteCommitButtonChange(ActionEvent ev) {
        SystemCoordinator sc = getSystemCoordinator();

        MessageBuilderParams mbp = new MessageBuilderParams();
        try {
            if (currentEvent != null) {

                mbp.setCred(getSessionBean().getSessUser().getKeyCard());
                mbp.setExistingContent(currentEvent.getNotes());
                mbp.setNewMessageContent(getFormNoteText());
                mbp.setHeader("Event Note");
                mbp.setUser(getSessionBean().getSessUser());
                currentEvent.setNotes(sc.appendNoteBlock(mbp));
            } else {
                throw new BObStatusException("No current event on which to write note");
            }

            sc.writeNotes(currentEvent, getSessionBean().getSessUser());
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Succesfully appended note to event!", ""));
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error appending note; apologies!", ""));

        }
    }

    //
    // Mostly boring getters and setters start here.
    //
    public IFace_EventHolder getCurrentEventHolder() {
        return currentEventHolder;
    }

    /**
     * "Wrapper" member to avoid lots of calls to session bean to check for the
     * trigger event presence
     *
     * @return the eventHumanLinkList
     */
    public List<HumanLink> getManagedEventHumanLinkList() {
        List<HumanLink> hll = getSessionBean().getSessHumanListRefreshedList();
        if (currentEvent != null) {
            if (hll != null) {
                currentEvent.sethumanLinkList(hll);
                // clear our refreshed list
                getSessionBean().setSessHumanListRefreshedList(null);
            }
            return currentEvent.gethumanLinkList();
        } else {
            return new ArrayList<>();
        }

    }

    /**
     * The primary getter
     *
     * @return
     */
    public EventCnF getCurrentEvent() {
        return currentEvent;
    }

    // Not boring
    public void setCurrentEvent(EventCnF currentEvent) {
        this.currentEvent = currentEvent;
        // Calculate duration for forms
        if (currentEvent.getTimeStart() != null && currentEvent.getTimeEnd() != null) {
            formEventDuration = ChronoUnit.MINUTES.between(currentEvent.getTimeStart(), currentEvent.getTimeEnd());
        }

//        this.lastSavedSelectedEvent = new EventCnF(this.currentEvent);
    }

    /**
     * This function exports selected events as an .ics file for download.
     */
    public void exportCalendar() {
        List<EventCnF> selEventCnFs = getSelectedEvents();
        EventCoordinator ec = getEventCoordinator();
        if (!selEventCnFs.isEmpty())
        {
            ec.exportCalendar(selEventCnFs);
            System.out.println("Successfully generated events.ics file");
        } else
        {
            selectionMode = false;
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "No Events Selected To Export!", ""));
        }
    }

    
    public void changeSelectionMode() {
        selectionMode = true;
        selectedEvents = null;
    }
    
    public void abortExportOperation() {
        selectionMode = false;
        selectedEvents = null;
    }
    
    public void setUpdateFieldsOnCategoryChange(boolean updateFieldsOnCategoryChange) {
        this.updateFieldsOnCategoryChange = updateFieldsOnCategoryChange;
        onCategoryChangeUpdateSkeletonEventMembers();
    }
    
    
    
    
    /* ************************************************************************
    ********************** GETTERS AND SETTERS  *******************************
    ***************************************************************************
    */ 


    public String getFormNoteText() {
        return formNoteText;
    }

    public void setFormNoteText(String formNoteText) {
        this.formNoteText = formNoteText;
    }

    public long getFormEventDuration() {
        return formEventDuration;
    }

    public void setFormEventDuration(long formEventDuration) {
        this.formEventDuration = formEventDuration;
    }

    public EventCnF getSkeletonEvent() {
        return skeletonEvent;
    }

    public void setSkeletonEvent(EventCnF skeletonEvent) {
        this.skeletonEvent = skeletonEvent;
    }

    public long getSkeletonDuration() {
        return skeletonDuration;
    }

    public void setSkeletonDuration(long skeletonDuration) {
        this.skeletonDuration = skeletonDuration;
    }

    public EventType getSkeletonType() {
        return skeletonType;
    }

    public void setSkeletonType(EventType skeletonType) {
        this.skeletonType = skeletonType;
    }

    public boolean isUpdateFieldsOnCategoryChange() {
        return updateFieldsOnCategoryChange;
    }

  
    /**
     * @return the eventEditMode
     */
    public boolean isEventEditMode() {
        return eventEditMode;
    }

    /**
     * @param eventEditMode the eventEditMode to set
     */
    public void setEventEditMode(boolean eventEditMode) {
        this.eventEditMode = eventEditMode;
    }

    /**
     * @return the eventListComponentForRefreshTrigger
     */
    public String getEventListComponentForRefreshTrigger() {
        return eventListComponentForRefreshTrigger;
    }

    /**
     * @param eventListComponentForRefreshTrigger the
     * eventListComponentForRefreshTrigger to set
     */
    public void setEventListComponentForRefreshTrigger(String eventListComponentForRefreshTrigger) {
        this.eventListComponentForRefreshTrigger = eventListComponentForRefreshTrigger;
    }

    /**
     * @return the includeDeactivatedEvent
     */
    public boolean isIncludeDeactivatedEvent() {
        return includeDeactivatedEvent;
    }

    /**
     * @param includeDeactivatedEvent the includeDeactivatedEvent to set
     */
    public void setIncludeDeactivatedEvent(boolean includeDeactivatedEvent) {
        this.includeDeactivatedEvent = includeDeactivatedEvent;
    }

    /**
     * @return the permissionAllowEventEditDeac
     */
    public boolean isPermissionAllowEventEditDeac() {
        return permissionAllowEventEditDeac;
    }

    /**
     * @param permissionAllowEventEditDeac the permissionAllowEventEditDeac to
     * set
     */
    public void setPermissionAllowEventEditDeac(boolean permissionAllowEventEditDeac) {
        this.permissionAllowEventEditDeac = permissionAllowEventEditDeac;
    }

    /**
     * @return the nonEventHolderSpecificPageAdd
     */
    public boolean isNonEventHolderSpecificPageAdd() {
        return nonEventHolderSpecificPageAdd;
    }

    /**
     * @param nonEventHolderSpecificPageAdd the nonEventHolderSpecificPageAdd to set
     */
    public void setNonEventHolderSpecificPageAdd(boolean nonEventHolderSpecificPageAdd) {
        this.nonEventHolderSpecificPageAdd = nonEventHolderSpecificPageAdd;
    }
    public List<EventCnF> getSelectedEvents() {
        return selectedEvents;
    }

    public void setSelectedEvents(List<EventCnF> selectedEvents) {
        this.selectedEvents = selectedEvents;
    }

    public boolean isSelectionMode() {
        return selectionMode;
    }

    public void setSelectionMode(boolean selectionMode) {
        this.selectionMode = selectionMode;
    }

    /**
     * @return the eventFollowUpTarget
     */
    public EventCnF getEventFollowUpTarget() {
        return eventFollowUpTarget;
    }

    /**
     * @param eventFollowUpTarget the eventFollowUpTarget to set
     */
    public void setEventFollowUpTarget(EventCnF eventFollowUpTarget) {
        this.eventFollowUpTarget = eventFollowUpTarget;
    }

    /**
     * @return the alertEventForStoppage
     */
    public EventCnF getAlertEventForStoppage() {
        return alertEventForStoppage;
    }

    /**
     * @param alertEventForStoppage the alertEventForStoppage to set
     */
    public void setAlertEventForStoppage(EventCnF alertEventForStoppage) {
        this.alertEventForStoppage = alertEventForStoppage;
    }

    /**
     * @return the alertStopEventPathwayActive
     */
    public boolean isAlertStopEventPathwayActive() {
        return alertStopEventPathwayActive;
    }

    /**
     * @param alertStopEventPathwayActive the alertStopEventPathwayActive to set
     */
    public void setAlertStopEventPathwayActive(boolean alertStopEventPathwayActive) {
        this.alertStopEventPathwayActive = alertStopEventPathwayActive;
    }

    /**
     * @return the currentEventCategory
     */
    public EventCategory getCurrentEventCategory() {
        return currentEventCategory;
    }

    /**
     * @param currentEventCategory the currentEventCategory to set
     */
    public void setCurrentEventCategory(EventCategory currentEventCategory) {
        this.currentEventCategory = currentEventCategory;
    }

    /**
     * @return the generalEventPathwayActive
     */
    public boolean isGeneralEventPathwayActive() {
        return generalEventPathwayActive;
    }

    /**
     * @param generalEventPathwayActive the generalEventPathwayActive to set
     */
    public void setGeneralEventPathwayActive(boolean generalEventPathwayActive) {
        this.generalEventPathwayActive = generalEventPathwayActive;
    }

}
