/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.coordinators.CodeCoordinator;
import com.tcvcog.tcvce.coordinators.OccInspectionCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.CodeViolation;
import com.tcvcog.tcvce.entities.IFaceFindingsHolder;
import com.tcvcog.tcvce.entities.Icon;
import com.tcvcog.tcvce.entities.TextBlock;
import com.tcvcog.tcvce.entities.TextBlockCategory;
import com.tcvcog.tcvce.entities.occupancy.OccInspectedSpaceElement;
import com.tcvcog.tcvce.integration.CaseIntegrator;
import com.tcvcog.tcvce.occupancy.application.FieldInspectionBB;
import com.tcvcog.tcvce.util.Constants;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.faces.event.ActionEvent;
import java.util.ArrayList;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Backs UI elements for adding, editing, and deactivating text blocks which
 * form the basis of Letters (NOVs, etc) and canned remarks on permits and
 * inspections.
 *
 * Upgraded in December 2024 to include facilities for managing multiple canned
 * findings on any given ordinance holder
 *
 * @author Ellen Bascomb of Apartment 31Y
 */
public class TextBlockBB extends BackingBeanUtils {

    private TextBlock currentTextBlock;
    private List<TextBlock> blockList;
    private boolean editModeTextBlock;
    private boolean loadAllMunisTextBlock;
    private boolean currentTextBlockActive;

    private TextBlockCategory currentTextBlockCategory;
    private List<TextBlockCategory> textBlockCategoryList;
    private boolean currentTextBlockCategoryActive;
    private boolean editModeTextBlockCategory;
    private boolean loadAllMunisTextBlockCategory;

    private List<Icon> iconList;

    // CANNED FINDINGS STUFF
    private IFaceFindingsHolder currentFindingsHolder;
    private TextBlock currentFindingsTextBlock;
    private TextBlock testTextBlock;
    private String cannedFindingComponentForRefresh;
    private String fullPathToOutputPanel;
    private int refreshCount = 0;
    
    /**
     * Creates a new instance of TextBlockBB
     */
    public TextBlockBB() {
    }

    @PostConstruct
    public void initBean() {
        SystemCoordinator sc = getSystemCoordinator();
        System.out.println("TextBlockBB.initBean()");
        try {
            iconList = sc.getIconList();
            editModeTextBlock = false;
            editModeTextBlockCategory = false;
            loadAllMunisTextBlock = false;
            textBlockCategoryList = sc.getTextBlockCategoryListForCertificates();
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }
    }
    
    /**
     * Listener for start of manage process
     *
     * @param ev
     */
    public void onManageBlocksInit(ActionEvent ev) {
        System.out.println("TextBlockBB.onManageBlocksInit");
        buildMasterTextBlockList(null);
        refreshTextBlockAndCatAndLists();
        if (blockList != null && !blockList.isEmpty()) {
            currentTextBlock = blockList.get(0);
        }

    }

    /**
     * Extracts all the text blocks from the DB that match any of the three
     * categories used in certificates
     *
     * @param ev
     */
    public void buildMasterTextBlockList(ActionEvent ev) {
        SystemCoordinator sc = getSystemCoordinator();
        try {
            blockList = new ArrayList<>();
            for (TextBlockCategory cat : textBlockCategoryList) {
                if (loadAllMunisTextBlock) {
                    blockList.addAll(sc.getTextBlockList(cat, null));
                } else {
                    blockList.addAll(sc.getTextBlockList(cat, getSessionBean().getSessMuni()));
                }
            }
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }

    }

    /**
     * Internal organ for setting up our master lists
     */
    public void refreshTextBlockAndCatAndLists() {
        SystemCoordinator sc = getSystemCoordinator();
        try {
            if (currentTextBlock != null) {
                sc.getTextBlock(currentTextBlock.getBlockID());
            }
            buildMasterTextBlockList(null);
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }
    }

    /**
     * Listener to start the cat manage process
     *
     * @param ev
     */
    public void onTextBlockCategoryManageInit(ActionEvent ev) {
        System.out.println("TextBlockBB.onTextBlockCategoryManageInit");
    }

    /**
     * Listener for block select clicking
     *
     * @param tb
     */
    public void onBlockSelect(TextBlock tb) {
        currentTextBlock = tb;

    }

    /**
     * Listener for block category select clicking
     *
     * @param tbc
     */
    public void onBlockCategorySelect(TextBlockCategory tbc) {
        setCurrentTextBlockCategory(tbc);
    }

    // MIGRATED FROM TEXT BLOCK BB // There used to be one!
    // and now back to a TextBlockBB
    /**
     * lISTENER FOR Toggling of text block categories
     *
     * @param ev
     */
    public void onEditModeTextBlockToggle(ActionEvent ev) {
        System.out.println("TextBlockConfigBB.onToggleTextBlockEditMode | incoming value: " + editModeTextBlock);
        if (editModeTextBlock) {
            try {
                if (currentTextBlock != null) {
                    if (currentTextBlock.getBlockID() == 0) {
                        onTextBlockAddCommitButtonChange();
                    } else {
                        onTextBlockUpdateCommit();
                    }
                } else {
                    getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                    "Event Category page configuration error EC1: currentTextBlockegory null",
                                    ""));
                }
            } catch (BObStatusException ex) {
                System.out.println();
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Text block page configuration error EC1",
                                ex.getMessage()));
            }
            refreshTextBlockAndCatAndLists();
        } else {
            // nothing to do--toggle on edit mode below
        }
        editModeTextBlock = !editModeTextBlock;
    }

    /**
     * Listener for abort requests
     *
     * @param ev
     */
    public void onTextBlockOperationAbort(ActionEvent ev) {
        editModeTextBlock = false;
    }

    /**
     * Listener for abort requests
     *
     * @param ev
     */
    public void onTextBlockCategoryOperationAbort(ActionEvent ev) {
        editModeTextBlockCategory = false;
    }

    /**
     * Listener to start the creation process of a text block
     *
     * @param ev
     */
    public void onTextBlockAddInit(ActionEvent ev) {
        SystemCoordinator sc = getSystemCoordinator();
        currentTextBlock = sc.getTextBlockSkeleton(getSessionBean().getSessMuni());
        editModeTextBlock = true;
    }

    /**
     * Listener for user requests to finalize the text block creation process
     *
     * @param ev
     */
    private void onTextBlockAddCommitButtonChange() throws BObStatusException {
        SystemCoordinator sc = getSystemCoordinator();
        int freshID = 0;

        try {
            freshID = sc.insertTextBlock(currentTextBlock, getSessionBean().getSessUser());
            currentTextBlock = sc.getTextBlock(freshID);
            refreshTextBlockAndCatAndLists();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Success! Added a new text block to the db!", ""));
        } catch (IntegrationException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));

        }
    }

    /**
     * Listener for updates to text blocks
     *
     * @throws BObStatusException
     */
    private void onTextBlockUpdateCommit() throws BObStatusException {
        SystemCoordinator sc = getSystemCoordinator();
        if (currentTextBlock != null) {
            try {

                sc.updateTextBlock(currentTextBlock, getSessionBean().getSessUser());
                refreshTextBlockAndCatAndLists();
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Success! Updated text block id " + currentTextBlock.getBlockID(), ""));
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Please select a text block and try again", ""));
            }
        } else {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Please select a text block and try again", ""));

        }
    }

    /**
     * listener to start deac process
     *
     * @param block
     */
    public void onTextBlockDeactivateInit(TextBlock block) {
        currentTextBlock = block;
        System.out.println("TextBlockDeacInit");

    }

    /**
     * Deac abort
     *
     * @param ev
     */
    public void onTextBlockDeactivateAbort(ActionEvent ev) {

        System.out.println("TextBlockDeacAbort");

    }

    /**
     * Listener for user requests to cmmit deac
     *
     * @param ev
     */
    public void onTextBlockDeactivateCommit(ActionEvent ev) {
        SystemCoordinator sc = getSystemCoordinator();
        if (currentTextBlock != null) {
            try {
                sc.deactivateTextBlock(currentTextBlock, getSessionBean().getSessUser());
                refreshTextBlockAndCatAndLists();
                if (blockList != null && !blockList.isEmpty()) {
                    currentTextBlock = blockList.get(0);
                }
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Success! Deactivated block id " + currentTextBlock.getBlockID(), ""));
                System.out.println("TextBlockDeac commit");
            } catch (IntegrationException | BObStatusException ex) {
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            }
        } else {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Please select a text block and try again", ""));
        }
    }

    /**
     * lISTENER FOR Toggling of text block categories
     *
     * @param ev
     */
    public void onEditModeTextBlockCategoryToggle(ActionEvent ev) {
        System.out.println("TextBlockConfigBB.onToggleTextBlockEditMode | incoming value: " + editModeTextBlock);
        if (editModeTextBlockCategory) {
            try {
                if (currentTextBlockCategory != null) {

                    if (currentTextBlockCategory.getCategoryID() == 0) {
                        onTextBlockCategoryAddCommitButtonChange();
                    } else {
                        onTextBlockCategoryUpdateCommit();
                    }
                } else {
                    getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                    "Text block Category page configuration error EC1: currentTextBlockegory null",
                                    ""));
                }
            } catch (BObStatusException ex) {
                System.out.println();
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Text block Category page configuration error EC1",
                                ex.getMessage()));
            }
        } else {
            // nothing to do--toggle on edit mode below
        }
        editModeTextBlockCategory = !editModeTextBlockCategory;
    }

    /**
     * Listener to start the creation process of a text block
     *
     * @param ev
     */
    public void onTextBlockCategoryAddInit(ActionEvent ev) {
        SystemCoordinator sc = getSystemCoordinator();
        currentTextBlockCategory = sc.getTextBlockCategorySkeleton(getSessionBean().getSessMuni());
        editModeTextBlockCategory = true;
    }

    /**
     * Listener for user requests to finalize the text block creation process
     *
     * @param ev
     */
    private void onTextBlockCategoryAddCommitButtonChange() throws BObStatusException {
        SystemCoordinator sc = getSystemCoordinator();
        int freshID = 0;

        try {
            freshID = sc.insertTextBlockCategory(currentTextBlockCategory);
            currentTextBlockCategory = sc.getTextBlockCategory(freshID);
            refreshTextBlockAndCatAndLists();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Success! Added a new text block Category to the db!", ""));
        } catch (IntegrationException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));

        }
    }

    /**
     * Listener for updates to text blocks
     *
     * @throws BObStatusException
     */
    private void onTextBlockCategoryUpdateCommit() throws BObStatusException {
        SystemCoordinator sc = getSystemCoordinator();
        if (currentTextBlockCategory != null) {
            try {
                // deal with deactivation TS adaptor to boolean box on UI
                if (currentTextBlockCategory.getDeactivatedTS() == null && !currentTextBlockCategoryActive) {
                    System.out.println("TextBlockBB.onTextBlockCategoryUpdateCommit | deac trigger! text blockCat ID: " + currentTextBlockCategory.getCategoryID());
                    onTextBlockCategoryDeactivateCommit();
                }
                if (currentTextBlockCategory.getDeactivatedTS() != null && currentTextBlockCategoryActive) {
                    currentTextBlockCategory.setDeactivatedTS(null);
                    System.out.println("TextBlockBB.onTextBlockCategoryUpdateCommit | reactivating text blockCat ID: " + currentTextBlockCategory.getCategoryID());
                }
                sc.updateTextBlockCategory(currentTextBlockCategory);
                refreshTextBlockAndCatAndLists();
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Success! Updated text block Category id " + currentTextBlockCategory.getCategoryID(), ""));
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Please select a text block  Category and try again", ""));
            }
        } else {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Please select a text block  Category and try again", ""));

        }
    }

    public void onTextBlockCategoryDeactivateInit(ActionEvent ev) {
        System.out.println("TextBlockCategoryDeacInit");

    }

    private void onTextBlockCategoryDeactivateCommit() {
        SystemCoordinator sc = getSystemCoordinator();
        if (currentTextBlockCategory != null) {
            try {
                sc.deactivateTextBlockCategory(currentTextBlockCategory);
                refreshTextBlockAndCatAndLists();
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Success! Nuked block cat id " + currentTextBlockCategory.getCategoryID(), ""));
            } catch (IntegrationException | BObStatusException ex) {
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            }
        } else {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Please select a text block category and try again", ""));
        }
    }

    /**
     * Special getter that looks at the text block deac TS field
     *
     * @return the currentTextBlockActive
     */
    public boolean isCurrentTextBlockActive() {
        currentTextBlockActive = false;
        if (currentTextBlock != null) {
            if (currentTextBlock.getDeactivatedTS() == null) {
                currentTextBlockActive = true;
            }
        }
        return currentTextBlockActive;
    }

    /**
     * Special adaptor getter that examines the current text block category's
     * deac field
     *
     * @return the currentTextBlockCategoryActive
     */
    public boolean isCurrentTextBlockCategoryActive() {
        currentTextBlockCategoryActive = false;
        if (currentTextBlockCategory != null) {
            if (currentTextBlockCategory.getDeactivatedTS() == null) {
                currentTextBlockCategoryActive = true;
            }
        }
        return currentTextBlockCategoryActive;
    }

    /**
     * *************************************************************
     *  **************** CANNED FINDINGS FACILITY ******************
     * *************************************************************
     *
     */
    /**
     * Inject the selected text block into the findings holder as holder
     *
     * @param block
     * @param holder
     */
    public void onApplyCannedFinding(TextBlock block, IFaceFindingsHolder holder) {
        if (block != null && holder != null && holder.getFindings() != null) {
            StringBuilder sb = new StringBuilder(holder.getFindings());
            sb.append(Constants.FMT_HTML_BREAK);
            sb.append(block.getTextBlockText());
            holder.setFindings(sb.toString());
            System.out.println("TextBlockBB.onApplyCannedFinding | appended text block ID: " + block.getBlockID());
        }
    }

    /**
     * Pulls the sending component ID for refresh on canned findings
     */
    private void extractCannedFindingComponentForRefresh() {
        String fullID = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("cannedFindingComponentForRefresh");
        fullPathToOutputPanel = fullID;
        if(fullID != null){
            // help from GPT 40
            String regex = "^[^:]+";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(fullID);

            if (matcher.find()) {
                cannedFindingComponentForRefresh = matcher.group();
                System.out.println("Root Element: " + cannedFindingComponentForRefresh);
            } else {
                System.out.println("No root element found.");
            }

            System.out.println("TextBlockBB.extractCannedFindingComponentForRefresh | ID: " + cannedFindingComponentForRefresh);
        }
    }

    /**
     * Starts process of saving the current findings as a default finding
     *
     * @param holder
     */
    public void onSaveCurrentFindingsAsCannedFindingInit(IFaceFindingsHolder holder) {
        extractCannedFindingComponentForRefresh();
        try {
            if (holder == null) {
                throw new BObStatusException("cannot set default findings on null holder");
            }
            configureNewCannedFinding(holder);
            currentFindingsTextBlock.setTextBlockText(holder.getFindings());

        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
    }

    /**
     * Shared organ for init of a new canned finding; used by both the save
     * these finding as a can or fresh canned finding with no guts
     */
    private void configureNewCannedFinding(IFaceFindingsHolder holder) throws IntegrationException {
        if (holder == null) {
            System.out.println("TextBlockBB.configureNewCannedFinding | null holder! aborting");
            return;
        }
        // test non-null checking
        testTextBlock = new TextBlock();

        CodeCoordinator cc = getCodeCoordinator();
        currentFindingsHolder = holder;
        currentFindingsTextBlock = cc.getCannedFindingTextBlockSkeleton();
        System.out.println("TextBlockBB.configureNewCannedFinding | holder: " + holder.getFindings());
        // build a default title
        StringBuilder sb = new StringBuilder();
        sb.append("canned finding ");
        if (holder.getCannedFindingsCandidates() == null || holder.getCannedFindingsCandidates().isEmpty()) {
            sb.append(1);
        } else {
            sb.append(holder.getCannedFindingsCandidates().size() + 1);
        }
        currentFindingsTextBlock.setTextBlockName(sb.toString());

    }

    /**
     * Starts management process
     *
     * @param holder
     */
    public void onManageCannedFindings(IFaceFindingsHolder holder) {
        currentFindingsHolder = holder;
        extractCannedFindingComponentForRefresh();
        configureCurrentFindingsTextBlock();
    }
    
    /**
     * Queues up the first if any text block
     */
    private void configureCurrentFindingsTextBlock(){
        if(currentFindingsHolder != null && currentFindingsHolder.getCannedFindingsCandidates() != null && !currentFindingsHolder.getCannedFindingsCandidates().isEmpty()){
            currentFindingsTextBlock = currentFindingsHolder.getCannedFindingsCandidates().get(0);
        } else {
            currentFindingsTextBlock = null;
        }
        
    }

    /**
     * Creates a new canned finding from scratch
     *
     * @param ev
     */
    public void onAddNewCannedFinding(ActionEvent ev) {
        try {
            configureNewCannedFinding(currentFindingsHolder);
        } catch (IntegrationException ex) {
            System.out.println(ex);
        }
    }

    /**
     * Listener to view/edit a canned finding
     *
     * @param block
     */
    public void onViewEditCannedFinding(TextBlock block) {
        
        currentFindingsTextBlock = block;
    }

    /**
     * Listener for user requests to save the current canned finding which works
     * for both new and updates
     *
     * @param ev
     */
    public void onSaveUpdatesToCannedFinding(ActionEvent ev) {
        CodeCoordinator cc = getCodeCoordinator();
        System.out.println("TextBlockBB.onSaveUpdatesToCannedFinding");
        try {
            if (testTextBlock != null) {
                System.out.println("TextBlockBB.onSaveUpdatesToCannedFinding | test block persists!");
            } else {
                System.out.println("TextBlockBB.onSaveUpdatesToCannedFinding | test block disappears");
            }
            if (currentFindingsTextBlock != null) {
                if (currentFindingsTextBlock.getBlockID() == 0) {
                    int freshID = cc.linkEnforceableCodeElementToTextBlock(currentFindingsHolder.getEnforceableCodeElement(), currentFindingsTextBlock, getSessionBean().getSessUser());
                    currentFindingsTextBlock.setBlockID(freshID);
                } else {
                    cc.updateEnforcableCodeElementTextBlock(currentFindingsHolder.getEnforceableCodeElement(), currentFindingsTextBlock, getSessionBean().getSessUser());
                }
                refreshCurrentFindingsHolder();
                flushCannedFindingsComponents();
            }
        } catch (AuthorizationException | BObStatusException | IntegrationException ex) {
            System.out.println(ex);
        }
    }
    
    /**
     * Hacky path for form refresh
     * @param ev 
     */
    public void refreshCurrentForm(ActionEvent ev){
        System.out.println("TextBlockBB.refreshCurrentForm");
        refreshCurrentFindingsHolder();
        flushCannedFindingsComponents();
        refreshCount++;
    }

    /**
     * Internal organ for FindingsHolder refresher. I also figure out which
     * backing bean's finding holder's parent needs to be refreshed. The caller
     * of this method will have called the CodeCoordinator which will have
     * dumped the relevant caches so we just need to tell the backing bean to
     * get the fresh copy since its ECE now have some new canned findings that
     * need to be displayed.
     */
    private void refreshCurrentFindingsHolder() {
        CodeCoordinator cc = getCodeCoordinator();
        try {
            currentFindingsHolder = cc.refreshFindingsHolder(currentFindingsHolder);
            if(currentFindingsHolder != null){
                if (currentFindingsHolder instanceof OccInspectedSpaceElement oise) {
                    FieldInspectionBB fieldInspectionBB = (FieldInspectionBB) getFacesContext().getApplication().evaluateExpressionGet(getFacesContext(), "#{fieldInspectionBB}", FieldInspectionBB.class);
                    if(Objects.nonNull(fieldInspectionBB)){
                        fieldInspectionBB.refreshCurrentInspectedSpace();
                        refreshCount = refreshCount + 2;
                    }
                } else if (currentFindingsHolder instanceof CodeViolation cv) {
                    CECaseBB ceCaseBB = (CECaseBB) getFacesContext().getApplication().evaluateExpressionGet(getFacesContext(), "#{ceCaseBB}", CECaseBB.class);
                    if(ceCaseBB != null){
                        ceCaseBB.onSaveAndRefreshSelectedViolationList();
                    }
                }
            }

        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
        }
    }

    /**
     * Done with findings management
     *
     * @param ev
     */
    public void onManageCannedFindingsDone(ActionEvent ev) {
        System.out.println("TextBlockBB.onManageCannedFindingsDone");
    }
    
    /**
     * Internal organ for flushing components during re-rendering
     */
    private void flushCannedFindingsComponents(){
        getFacesContext().getCurrentInstance().getViewRoot().markInitialState();
        
        
    }

    /**
     * Removes a canned finding permanently
     *
     * @param block
     * @param holder
     */
    public void onDeleteCannedFinding(TextBlock block) {
        CodeCoordinator cc = getCodeCoordinator();
        try {
           
            currentFindingsTextBlock = block;
            cc.deleteEnforcableCodeElementToTextBlockLink(currentFindingsHolder.getEnforceableCodeElement(), currentFindingsTextBlock, getSessionBean().getSessUser());
            refreshCurrentFindingsHolder();
            configureCurrentFindingsTextBlock();
            flushCannedFindingsComponents();
            
            // write in a skeleton to keep page components happy
//            onAddNewCannedFinding(null);
        } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
    }

    /**
     * *************************************************************
     *  **************** GETTERS AND SETTERS ***********************
     * *************************************************************
     */
    /**
     * @return the currentTextBlock
     */
    public TextBlock getCurrentTextBlock() {
        return currentTextBlock;
    }

    /**
     * @param currentTextBlock the currentTextBlock to set
     */
    public void setCurrentTextBlock(TextBlock currentTextBlock) {
        this.currentTextBlock = currentTextBlock;
    }

    /**
     * @return the blockList
     */
    public List<TextBlock> getBlockList() {
        return blockList;
    }

    /**
     * @return the currentTextBlockCategory
     */
    public TextBlockCategory getCurrentTextBlockCategory() {
        return currentTextBlockCategory;
    }

    /**
     * @return the textBlockCategoryList
     */
    public List<TextBlockCategory> getTextBlockCategoryList() {
        return textBlockCategoryList;
    }

    /**
     * @param blockList the blockList to set
     */
    public void setBlockList(List<TextBlock> blockList) {
        this.blockList = blockList;
    }

    /**
     * @param currentTextBlockCategory the currentTextBlockCategory to set
     */
    public void setCurrentTextBlockCategory(TextBlockCategory currentTextBlockCategory) {
        this.currentTextBlockCategory = currentTextBlockCategory;
    }

    /**
     * @param textBlockCategoryList the textBlockCategoryList to set
     */
    public void setTextBlockCategoryList(List<TextBlockCategory> textBlockCategoryList) {
        this.textBlockCategoryList = textBlockCategoryList;
    }

    /**
     * @return the editModeTextBlockCategory
     */
    public boolean isEditModeTextBlockCategory() {
        return editModeTextBlockCategory;
    }

    /**
     * @param editModeTextBlockCategory the editModeTextBlockCategory to set
     */
    public void setEditModeTextBlockCategory(boolean editModeTextBlockCategory) {
        this.editModeTextBlockCategory = editModeTextBlockCategory;
    }

    /**
     * @return the editModeTextBlock
     */
    public boolean isEditModeTextBlock() {
        return editModeTextBlock;
    }

    /**
     * @param editModeTextBlock the editModeTextBlock to set
     */
    public void setEditModeTextBlock(boolean editModeTextBlock) {
        this.editModeTextBlock = editModeTextBlock;
    }

    /**
     * @return the loadAllMunisTextBlockCategory
     */
    public boolean isLoadAllMunisTextBlockCategory() {
        return loadAllMunisTextBlockCategory;
    }

    /**
     * @param loadAllMunisTextBlockCategory the loadAllMunisTextBlockCategory to
     * set
     */
    public void setLoadAllMunisTextBlockCategory(boolean loadAllMunisTextBlockCategory) {
        this.loadAllMunisTextBlockCategory = loadAllMunisTextBlockCategory;
    }

    /**
     * @return the loadAllMunisTextBlock
     */
    public boolean isLoadAllMunisTextBlock() {
        return loadAllMunisTextBlock;
    }

    /**
     * @param loadAllMunisTextBlock the loadAllMunisTextBlock to set
     */
    public void setLoadAllMunisTextBlock(boolean loadAllMunisTextBlock) {
        this.loadAllMunisTextBlock = loadAllMunisTextBlock;
    }

    /**
     * @return the iconList
     */
    public List<Icon> getIconList() {
        return iconList;
    }

    /**
     * @param iconList the iconList to set
     */
    public void setIconList(List<Icon> iconList) {
        this.iconList = iconList;
    }

    /**
     * @param currentTextBlockActive the currentTextBlockActive to set
     */
    public void setCurrentTextBlockActive(boolean currentTextBlockActive) {
        this.currentTextBlockActive = currentTextBlockActive;
    }

    /**
     * @param currentTextBlockCategoryActive the currentTextBlockCategoryActive
     * to set
     */
    public void setCurrentTextBlockCategoryActive(boolean currentTextBlockCategoryActive) {
        this.currentTextBlockCategoryActive = currentTextBlockCategoryActive;
    }

    /**
     * @return the currentFindingsHolder
     */
    public IFaceFindingsHolder getCurrentFindingsHolder() {
        return currentFindingsHolder;
    }

    /**
     * @param currentFindingsHolder the currentFindingsHolder to set
     */
    public void setCurrentFindingsHolder(IFaceFindingsHolder currentFindingsHolder) {
        this.currentFindingsHolder = currentFindingsHolder;
    }

    /**
     * @return the currentFindingsTextBlock
     */
    public TextBlock getCurrentFindingsTextBlock() {
        return currentFindingsTextBlock;
    }

    /**
     * @param currentFindingsTextBlock the currentFindingsTextBlock to set
     */
    public void setCurrentFindingsTextBlock(TextBlock currentFindingsTextBlock) {
        this.currentFindingsTextBlock = currentFindingsTextBlock;
    }

    /**
     * @return the testTextBlock
     */
    public TextBlock getTestTextBlock() {
        return testTextBlock;
    }

    /**
     * @param testTextBlock the testTextBlock to set
     */
    public void setTestTextBlock(TextBlock testTextBlock) {
        this.testTextBlock = testTextBlock;
    }

    /**
     * @return the cannedFindingComponentForRefresh
     */
    public String getCannedFindingComponentForRefresh() {
        return cannedFindingComponentForRefresh;
    }

    /**
     * @param cannedFindingComponentForRefresh the
     * cannedFindingComponentForRefresh to set
     */
    public void setCannedFindingComponentForRefresh(String cannedFindingComponentForRefresh) {
        this.cannedFindingComponentForRefresh = cannedFindingComponentForRefresh;
    }

    /**
     * @return the fullPathToOutputPanel
     */
    public String getFullPathToOutputPanel() {
        return fullPathToOutputPanel;
    }

    /**
     * @param fullPathToOutputPanel the fullPathToOutputPanel to set
     */
    public void setFullPathToOutputPanel(String fullPathToOutputPanel) {
        this.fullPathToOutputPanel = fullPathToOutputPanel;
    }

    /**
     * @return the refreshCount
     */
    public int getRefreshCount() {
        return refreshCount;
    }

    /**
     * @param refreshCount the refreshCount to set
     */
    public void setRefreshCount(int refreshCount) {
        this.refreshCount = refreshCount;
    }
}
