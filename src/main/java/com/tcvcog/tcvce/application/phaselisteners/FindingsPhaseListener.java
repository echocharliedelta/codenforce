/*
 * Copyright (C) 2025 echocdelta
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application.phaselisteners;

import com.tcvcog.tcvce.application.CECaseBB;
import jakarta.faces.context.FacesContext;
import jakarta.faces.event.PhaseEvent;
import jakarta.faces.event.PhaseId;
import java.util.Map;

/**
 * Phase listener for UI validation hacking
 * @author echocdelta
 */
public class FindingsPhaseListener implements jakarta.faces.event.PhaseListener{

    @Override
    public void afterPhase(PhaseEvent pe) {
        // do nothing
    }

    /**
     * Ridiculous work around for a UI peculiarity related to only validating
     * user's findings in violation add when a the user click's the attach violations
     * to CECase button and NOT during intermediate saves triggered by clicks of the
     * canned findings.
     * @param pe 
     */
    @Override
    public void beforePhase(PhaseEvent pe) {
        FacesContext ctx = pe.getFacesContext();
        if(ctx != null && ctx.getViewRoot() != null){
            String currentViewID = ctx.getViewRoot().getViewId();
            if("/restricted/cogstaff/ce/ceCaseProfile.xhtml".equals(currentViewID)){
                Map<String,String> params = ctx.getExternalContext().getRequestParameterMap();
                if(params.containsKey("violation-add-form:violation-insert-button0")){
                    System.out.println("FindingsPhaseListener.beforePhase | found violation-insert-button0 | init cecCaseBB");
                    CECaseBB ceCaseBB = ctx.getApplication()
                                       .evaluateExpressionGet(ctx, "#{ceCaseBB}", CECaseBB.class);

                    if(ceCaseBB != null){
                        ceCaseBB.setRequireFindingsOnViolationAdd(true);
                    }
                }
            }
        }
        
        
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.APPLY_REQUEST_VALUES;
    }
    
}
