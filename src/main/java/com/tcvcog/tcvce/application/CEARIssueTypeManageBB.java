/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.CEActionRequestIssueType;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;
import java.util.List;
import java.util.Objects;

/**
 * Premier backing bean for managing CEAR issue types
 * @author pierre15
 */
public class CEARIssueTypeManageBB extends BackingBeanUtils{
    
    
    private List<CEActionRequestIssueType> issueTypeList;
    private CEActionRequestIssueType currentIssueType;
    private List<CEActionRequestIssueType> intensityCandidateList;
    
    private boolean editModeIssueType;
    private boolean showInactiveIssueTypes;
    private boolean showAllMunis;
    
    @PostConstruct
    public void initBean(){
        System.out.println("CEARIssueTypeManageBB.initBean()");
            showInactiveIssueTypes = false;
            showAllMunis = false;
            refreshIssueTypes();
            if(issueTypeList != null && !issueTypeList.isEmpty()){
                currentIssueType = issueTypeList.get(0);
            }
        
    }
    
    /**
     * Fetches a new copy of the master list
     */
    public void refreshIssueTypes() {
        try {
            CaseCoordinator cc = getCaseCoordinator();
            if(showAllMunis){
                issueTypeList = cc.cear_getIssueTypes(null, showInactiveIssueTypes);
            } else {
                issueTypeList = cc.cear_getIssueTypes(getSessionBean().getSessMuni(), showInactiveIssueTypes);
            }
        } catch (IntegrationException  ex) {
            System.out.println(ex);
        }
        
    }
    
    /**
     * Listener for user toggles the show inactive checkbox
     * @param ev 
     */
    public void onToggleShowInactive(){
            refreshIssueTypes();
    }
    
    /**
     * Listener for user toggling of show all munis check box
     */
    public void onToggleShowAllMunis(){
        refreshIssueTypes();
    }
    
    /**
     * Listener for user requests to view a citations status
     * @param cearit
     */
    public void onViewCEARIssueType(CEActionRequestIssueType cearit){
        System.out.println("CEARIssueTypeManageBB.onViewCEARIssueType | StatusID: " + cearit.getIssueTypeID());
        currentIssueType = cearit;
    }
    
    
     public void toggleIssueTypeAddEditButtonChange(ActionEvent ev) throws BObStatusException, AuthorizationException {
        System.out.println("CEARIssueTypeManageBB.toggleStatusAddEditButtonChange");
        if (editModeIssueType) {
            if (Objects.nonNull(currentIssueType) && currentIssueType.getIssueTypeID()== 0) {
                onIssueTypeInsertCommit();
            } else {
                onUpdateIssueTypeCommit();
            }
            refreshIssueTypes();
        }
        editModeIssueType = !editModeIssueType;
    }

    /**
     * Begins the OccInspection Cause creation process
     *
     * @param ev
     */
    public void onIssueTypeAddInit(ActionEvent ev) {
        System.out.println("CEARIssueTypeManageBB.onIssueTypeAddInit");
        CaseCoordinator cc = getCaseCoordinator();
        currentIssueType = cc.cear_getIssueTypeSkeleton(getSessionBean().getSessMuni());
        editModeIssueType = true;
    }

    /**
     * inserter for citation status objects
     * @throws AuthorizationException 
     */
    public void onIssueTypeInsertCommit() throws AuthorizationException {
        CaseCoordinator cc = getCaseCoordinator();
        try {
            int freshid = cc.cear_insertIssueType(currentIssueType, getSessionBean().getSessUser());
            currentIssueType = cc.cear_getCEARIssueType(freshid);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Successful Insert new issue type!", ""));
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
        }
    }

    /**
     * Update citation status objects
     */
    public void onUpdateIssueTypeCommit() {
        CaseCoordinator cc = getCaseCoordinator();
        try {
            cc.cear_UpdateIssueType(currentIssueType, getSessionBean().getSessUser());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "CEAR issue type udpated with Id " + currentIssueType.getIssueTypeID(), ""));
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Failed to Update OccInspection Cause", ""));
        }
    }


    /**
     * Listener for users to cancel their OccInspection Cause add/edit operation
     *
     * @param ev
     */
    public void onOperationAddEditIssueTypeAbort(ActionEvent ev) {
        System.out.println("CEARIssueTypeManageBB.onOperationAddEditIssueTypeAbort");
        editModeIssueType = false;
    }

    /**
     * Listener for user request to start the nuking process of a OccInspectionCause
     *
     * @param cearit
     */
    public void onOccInspectionCauseNukeInitButtonChange(CEActionRequestIssueType cearit) {
        currentIssueType = cearit;
    }

    /**
     * Listener for user requests to commit a 
     *
     * @param cearit
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public void onCEARIssueTypeDeacCommitButtonChange(CEActionRequestIssueType cearit) throws AuthorizationException, BObStatusException {
        currentIssueType = cearit;
        
        CaseCoordinator cc = getCaseCoordinator();
        try {
            cc.cear_deactivateIssueType(currentIssueType, getSessionBean().getSessUser());
            refreshIssueTypes();
            
            if (Objects.nonNull(issueTypeList) && !issueTypeList.isEmpty()) {
                currentIssueType = issueTypeList.get(0);
            } else {
                currentIssueType = null;
            }
        } catch (IntegrationException | BObStatusException  ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
        }
    }
    
    // ****************** GETTERS AND SETTERS **********************************

    /**
     * @return the issueTypeList
     */
    public List<CEActionRequestIssueType> getIssueTypeList() {
        return issueTypeList;
    }

    /**
     * @param issueTypeList the issueTypeList to set
     */
    public void setIssueTypeList(List<CEActionRequestIssueType> issueTypeList) {
        this.issueTypeList = issueTypeList;
    }

    /**
     * @return the currentIssueType
     */
    public CEActionRequestIssueType getCurrentIssueType() {
        return currentIssueType;
    }

    /**
     * @param currentIssueType the currentIssueType to set
     */
    public void setCurrentIssueType(CEActionRequestIssueType currentIssueType) {
        this.currentIssueType = currentIssueType;
    }

    /**
     * @return the intensityCandidateList
     */
    public List<CEActionRequestIssueType> getIntensityCandidateList() {
        return intensityCandidateList;
    }

    /**
     * @param intensityCandidateList the intensityCandidateList to set
     */
    public void setIntensityCandidateList(List<CEActionRequestIssueType> intensityCandidateList) {
        this.intensityCandidateList = intensityCandidateList;
    }

    /**
     * @return the editModeIssueType
     */
    public boolean isEditModeIssueType() {
        return editModeIssueType;
    }

    /**
     * @param editModeIssueType the editModeIssueType to set
     */
    public void setEditModeIssueType(boolean editModeIssueType) {
        this.editModeIssueType = editModeIssueType;
    }

    /**
     * @return the showInactiveIssueTypes
     */
    public boolean isShowInactiveIssueTypes() {
        return showInactiveIssueTypes;
    }

    /**
     * @param showInactiveIssueTypes the showInactiveIssueTypes to set
     */
    public void setShowInactiveIssueTypes(boolean showInactiveIssueTypes) {
        this.showInactiveIssueTypes = showInactiveIssueTypes;
    }

    /**
     * @return the showAllMunis
     */
    public boolean isShowAllMunis() {
        return showAllMunis;
    }

    /**
     * @param showAllMunis the showAllMunis to set
     */
    public void setShowAllMunis(boolean showAllMunis) {
        this.showAllMunis = showAllMunis;
    }

    
    
}
