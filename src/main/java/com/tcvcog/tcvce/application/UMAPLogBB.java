/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.UserCoordinator;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.UserMuniAuthPeriodLogEntry;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;
import java.util.List;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ellen Bascomb of Apartment 31Y
 */
public class UMAPLogBB extends BackingBeanUtils{

    private List<UserMuniAuthPeriodLogEntry> currentUMAPLog;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    
    /**
     * Creates a new instance of UMAPLogBB
     */
    public UMAPLogBB() {
    }
    
       @PostConstruct
    public void initBean() {
        endDate = LocalDateTime.now();
        startDate = endDate.minusMonths(1);
        queryUMAPLogs(null);
        
    }
    
    /**
     * Listener for user requests to query the log entries
     * @param ev 
     */
    public void queryUMAPLogs(ActionEvent ev){
        UserCoordinator uc = getUserCoordinator();
        try {
            System.out.println("UMAPLogBB.queryUMAPLogs");
            currentUMAPLog = uc.getUMAPLog(startDate, endDate);
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                ex.getMessage(), ""));
        }
        
    }

    /**
     * @return the currentUMAPLog
     */
    public List<UserMuniAuthPeriodLogEntry> getCurrentUMAPLog() {
        return currentUMAPLog;
    }

    /**
     * @return the startDate
     */
    public LocalDateTime getStartDate() {
        return startDate;
    }

    /**
     * @return the endDate
     */
    public LocalDateTime getEndDate() {
        return endDate;
    }

    /**
     * @param currentUMAPLog the currentUMAPLog to set
     */
    public void setCurrentUMAPLog(List<UserMuniAuthPeriodLogEntry> currentUMAPLog) {
        this.currentUMAPLog = currentUMAPLog;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    
}
