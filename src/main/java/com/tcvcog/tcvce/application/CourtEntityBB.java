/*
 * Copyright (C) 2018 Adam Gutonski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.CourtEntity;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.bean.ViewScoped;
import jakarta.faces.event.ActionEvent;

/**
 * @author Adam Gutonski
 */

@ViewScoped
public class CourtEntityBB extends BackingBeanUtils implements Serializable {

    private List<CourtEntity> courtEntityList;
    private CourtEntity currentCourtEntity;

    private boolean editModeCourtEntityInfo;
    private boolean courtEntityCreateMode;

    private boolean includeInactiveCourtEntities;

    public CourtEntityBB() {
    }

    @PostConstruct
    public void initBean() {
        System.out.println("Court Entity BB: Init");
        includeInactiveCourtEntities = false;
        courtEntityCreateMode = false;
        editModeCourtEntityInfo = false;
        refreshCourtEntityList();
        if (Objects.nonNull(courtEntityList) && !courtEntityList.isEmpty()) {
            currentCourtEntity = courtEntityList.get(0);
        }
    }

    /**
     * Loads a fresh lists of court entities from the db
     */
    public void refreshCourtEntityList() {
        CaseCoordinator cc = getCaseCoordinator();
        try {
            courtEntityList = cc.getCourtEntityListComplete(!includeInactiveCourtEntities);
        } catch (IntegrationException | BObStatusException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to load CourtEntityList",
                            "This must be corrected by the system administrator"));
        }
    }

    /**
     * Extracts a fresh copy of the court entity from the db
     */
    public void refreshCurrentCourtEntity() {
        CaseCoordinator caseCoordinator = getCaseCoordinator();

        if (Objects.nonNull(currentCourtEntity)) {
            try {
                currentCourtEntity = caseCoordinator.getCourtEntity(currentCourtEntity.getCourtEntityID());
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Refreshed Court entity: " + currentCourtEntity.getCourtEntityID(), ""));
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Unable to extract court entity from DB", ""));
            }
        }
    }

    public void onInsertCourtEntityCommit() {
        CaseCoordinator caseCoordinator = getCaseCoordinator();
        try {
            currentCourtEntity = caseCoordinator.insertCourtEntity(currentCourtEntity, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Successful Insert New Court entity!", ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
        }
    }

    public void onUpdateCourtEntityCommit() {
        CaseCoordinator caseCoordinator = getCaseCoordinator();
        try {
            caseCoordinator.updateCourtEntity(currentCourtEntity, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Court Entity Updated with ID " + currentCourtEntity.getCourtEntityID(), ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Failed to Update Court Entity", ""));
        }
    }

    /**
     * Listener for users to click the add or the edit button
     *
     * @param ev
     */
    public void toggleCourtEntityAddEditButtonChange(ActionEvent ev) {
        System.out.println("toggleCourtEntityAddEditButtonChange");
        if (editModeCourtEntityInfo) {
            if (Objects.nonNull(currentCourtEntity) && currentCourtEntity.getCourtEntityID() == 0) {
                onInsertCourtEntityCommit();
            } else {
                onUpdateCourtEntityCommit();
                System.out.println(currentCourtEntity.getCourtEntityID());
            }
            refreshCourtEntityList();
        }
        editModeCourtEntityInfo = !editModeCourtEntityInfo;
    }

    /**
     * Listener for user requests to view a court entity
     *
     * @param courtEntity
     */
    public void viewCourtEntity(CourtEntity courtEntity) {
        currentCourtEntity = courtEntity;
        System.out.println("View Court Entity: " + courtEntity.getCourtEntityName());
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Viewing Court Entity: " + courtEntity.getCourtEntityName(), ""));
    }

    /**
     * Begins the court entity creation process
     *
     * @param ev
     */
    public void onCourtEntityAddInit(ActionEvent ev) {
        System.out.println("CourtEntityManageBB.onCourtEntityAddInit");
        CaseCoordinator cc = getCaseCoordinator();
        currentCourtEntity = cc.getCourtEntitySkeleton(getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
        editModeCourtEntityInfo = true;
    }

    /**
     * Listener for users to cancel their court entity add/edit operation
     *
     * @param ev
     */
    public void onOperationCourtEntityAddEditAbort(ActionEvent ev) {
        System.out.println("CourtEntityManageBB.toggleCourtEntityAddEditButtonChange: ABORT");
        editModeCourtEntityInfo = false;
        courtEntityCreateMode = false;
    }

    /**
     * Listener for user request to start the nuking process of a Court Entity
     *
     * @param courtEntity
     */
    public void onCourtEntityNukeInitButtonChange(CourtEntity courtEntity) {
        currentCourtEntity = courtEntity;
    }

    /**
     * Listener for user requests to commit a court entity nuke operation
     */
    public void onCourtEntityNukeCommitButtonChange() {
        CaseCoordinator caseCoordinator = getCaseCoordinator();
        try {
            caseCoordinator.deactivateCourtEntity(currentCourtEntity, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
            refreshCourtEntityList();
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Court Entity deactivated with ID: " + currentCourtEntity.getCourtEntityID(), ""));
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
        }
    }

    public List<CourtEntity> getCourtEntityList() {
        return courtEntityList;
    }

    public void setCourtEntityList(List<CourtEntity> courtEntityList) {
        this.courtEntityList = courtEntityList;
    }

    public CourtEntity getCurrentCourtEntity() {
        return currentCourtEntity;
    }

    public void setCurrentCourtEntity(CourtEntity currentCourtEntity) {
        this.currentCourtEntity = currentCourtEntity;
    }

    public boolean isEditModeCourtEntityInfo() {
        return editModeCourtEntityInfo;
    }

    public void setEditModeCourtEntityInfo(boolean editModeCourtEntityInfo) {
        this.editModeCourtEntityInfo = editModeCourtEntityInfo;
    }

    public boolean isCourtEntityCreateMode() {
        return courtEntityCreateMode;
    }

    public void setCourtEntityCreateMode(boolean courtEntityCreateMode) {
        this.courtEntityCreateMode = courtEntityCreateMode;
    }

    public boolean isIncludeInactiveCourtEntities() {
        return includeInactiveCourtEntities;
    }

    public void setIncludeInactiveCourtEntities(boolean includeInactiveCourtEntities) {
        this.includeInactiveCourtEntities = includeInactiveCourtEntities;
    }
}
