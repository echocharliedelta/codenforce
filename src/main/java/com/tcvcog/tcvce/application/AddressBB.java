/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.PropertyCoordinator;
import com.tcvcog.tcvce.coordinators.SearchCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.EventException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.domain.SearchException;
import com.tcvcog.tcvce.entities.BOBSource;
import com.tcvcog.tcvce.entities.BobMergeRequest;
import com.tcvcog.tcvce.entities.Human;
import com.tcvcog.tcvce.entities.IFace_Mergable;
import com.tcvcog.tcvce.entities.IFace_addressListHolder;
import com.tcvcog.tcvce.entities.LinkedObjectRole;
import com.tcvcog.tcvce.entities.LinkedObjectSchemaEnum;
import com.tcvcog.tcvce.entities.MailingAddress;
import com.tcvcog.tcvce.entities.MailingAddressLink;
import com.tcvcog.tcvce.entities.MailingAddressLinkParentObjectInfoHeavy;
import com.tcvcog.tcvce.entities.MailingAddressQuickAddBundle;
import com.tcvcog.tcvce.entities.MailingCityStateZip;
import com.tcvcog.tcvce.entities.MailingStreet;
import com.tcvcog.tcvce.entities.MergeDomainEnum;
import com.tcvcog.tcvce.entities.Parcel;
import com.tcvcog.tcvce.entities.Person;
import com.tcvcog.tcvce.entities.PropertyDataHeavy;
import com.tcvcog.tcvce.entities.PropertyUnit;
import com.tcvcog.tcvce.entities.PropertyUnitDataHeavy;
import com.tcvcog.tcvce.entities.search.QueryMailingCityStateZip;
import com.tcvcog.tcvce.util.MessageBuilderParams;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.faces.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * The official backing bean for address management
 *
 * @author Ellen Bascomb of Apartment 31Y
 */
public class AddressBB
        extends BackingBeanUtils {

    // mailing street stuff
    private MailingStreet currentStreet;
    private List<MailingStreet> streetList;
    private List<MailingStreet> streetListFiltered;
    private boolean editModeCurrentStreet;
    private boolean permissionAllowCurrentStreetDeactivate;

    // mailing address stuff
    private MailingAddress currentMailingAddress;
    private List<MailingAddressLinkParentObjectInfoHeavy> currentMailingAddressRecursiveLinkList;
    private List<MailingAddress> mailingAddressList;
    private List<MailingAddress> mailingAddressListFiltered;
    private boolean permissionAllowCurrentMailingAddressDeactivation;

    private boolean editModeCurrentAddress;
    private MailingCityStateZip currentCityStateZip;
    private List<MailingCityStateZip> cityStateZipListFiltered;

    // address linking stuff
    private boolean editModeCurrentAddressLink;
    private MailingAddressLink currentMailingAddressLink;
    private IFace_addressListHolder currentAddressListHolder;
    private List<LinkedObjectRole> mailingAddressLinkRoleCandidateList;
    private String addressListHolderComponentForUpdatePostMADLinkOperation;

    // merging facility
    private MergeDomainEnum currentMergeDomain;
    private BobMergeRequest currentMergeRequest;
    private List<IFace_Mergable> selectedObjectsToMerge;

    private boolean mailingStreetMergeSelectMode;
    private List<MailingStreet> selectedStreetList;
    private BobMergeRequest<MailingStreet> mailingStreetMergeRequest;

    private boolean mailingAddressMergeSelectMode;
    private List<MailingAddress> selectedAddressList;
    private BobMergeRequest<MailingAddress> mailingAddressMergeRequest;

    // first gen form fields 
    private String formBuildingNo;
    private String formStreet;
    private boolean formPOBox;
    private boolean formAddressVerified;
    private String formCity;
    private String formZip;
    private String formNotesAddress;
    private String formNotesStreet;
    private String formNotesAddressLink;

    // quick add form fields
    private MailingAddressQuickAddBundle addressQuickAddBundle;
    private boolean addressQuickAddDisableFields;

    // menu selection support
    private List<BOBSource> addressSourceList;

    // SEARCH
    private List<QueryMailingCityStateZip> qcszEnumList;
    private QueryMailingCityStateZip selectedCSZQuery;

    // 2nd gen search
    private List<MailingAddress> mailingAddressListFilterSearchResults;

    private String formZIPFilter;
    private String formStreetFilter;
    private String formBuildingFilter;

    /**
     * Creates a new instance of AddressBB
     */
    public AddressBB() {

    }

    @PostConstruct
    public void initBean() {
        SystemCoordinator sc = getSystemCoordinator();
        addressSourceList = sc.getBobSourceListComplete();

//        currentAddressListHolder = getSessionBean().getSessAddressListHolder();
        // LOAD UP OUR FILTERED LISTS 
        cityStateZipListFiltered = new ArrayList<>();
        streetListFiltered = new ArrayList<>();
        mailingAddressListFiltered = new ArrayList<>();
        setupCSZQuery();

    }

    /**
     * Internal tool for resetting the query and selecting the first one
     */
    private void setupCSZQuery() {
        SearchCoordinator srchc = getSearchCoordinator();
        if (srchc != null && getSessionBean() != null && getSessionBean().getSessUser() != null && getSessionBean().getSessUser().getMyCredential() != null) {
            qcszEnumList = srchc.buildQueryMailingCityStateZipList(getSessionBean().getSessUser().getMyCredential());
            if (qcszEnumList != null && !qcszEnumList.isEmpty()) {
                selectedCSZQuery = qcszEnumList.get(0);
            }
        } else {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error: Null session bean or session user or session credential", ""));
        }
    }

    /**
     * Listener for user requests to execute the selected search
     *
     * @param ev
     */
    public void onQueryCSZExecuteButtonChange(ActionEvent ev) {
        SearchCoordinator sc = getSearchCoordinator();
        try {
            if (selectedCSZQuery != null) {
                sc.runQuery(selectedCSZQuery);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Search Returned " + selectedCSZQuery.getResults().size() + " records!", ""));
            } else {
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Fatal error: Null selected query" + " records!", ""));

            }
        } catch (SearchException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Search error!", ""));
        }
    }

    /**
     * listener for changes in the query drop down dialog
     *
     * @param ev
     */
    public void onQueryChange() {
        System.out.println("AddressBB.onQueryChange | Selected Zip Query: " + selectedCSZQuery.getQueryTitle());
    }

    /**
     * Flushes out old query
     *
     * @param ev
     */
    public void onQueryResetButtonChange(ActionEvent ev) {
        selectedCSZQuery = null;
        setupCSZQuery();

        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Search system reset", ""));
    }

    /**
     * Listener for user requests to close the search dialog
     *
     * @param ev
     */
    public void onQueryOperationCancelButtonChange(ActionEvent ev) {
        //nothing to do here

    }

    /**
     * User requests to make their chosen CSZ result record the session active
     * one
     *
     * @param mcsz
     */
    public void selectCityStateZip(MailingCityStateZip mcsz) {
        getSessionBean().setSessMailingCityStateZip(mcsz);
        currentCityStateZip = mcsz;
        if (currentMailingAddress != null && currentMailingAddress.getStreet() != null) {
            currentMailingAddress.getStreet().setCityStateZip(currentCityStateZip);
        }

        searchForMailingStreet();

    }

    /**
     * Listener to start the 2nd generation address search tool by loading into
     * memory all the addresses
     *
     * @param ev
     */
    public void onAddressFilterSearchInit(ActionEvent ev) {
        try {
            PropertyCoordinator pc = getPropertyCoordinator();
            mailingAddressListFilterSearchResults = pc.getMailingAddressListComplete();
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }

    }

    /**
     * Listener for key change in ZIP filter
     *
     * @param ev
     */
    public void onZIPCodeFilterChange(ActionEvent ev) {
        if (mailingAddressListFilterSearchResults != null && !mailingAddressListFilterSearchResults.isEmpty()) {
            mailingAddressListFilterSearchResults = mailingAddressListFilterSearchResults.stream()
                    .filter(mad
                            -> mad.getStreet() != null
                    && mad.getStreet().getCityStateZip() != null
                    && mad.getStreet().getCityStateZip().getZipCode().contains(formZIPFilter))
                    .collect(Collectors.toList());
        }
    }

    /**
     * Listener for key change on street filter
     *
     * @param ev
     */
    public void onStreetFilterChange(ActionEvent ev) {

    }

    /**
     * Listener for change on building no
     *
     * @param ev
     */
    public void onBuildingFilterChange(ActionEvent ev) {

    }

    /**
     * ****************************************************************
     */
    /**
     * ************ Street and address Merging Facility ***************
     */
    /**
     * ****************************************************************
     */
    /**
     * Catch all listener for both street and address merging
     *
     * @param ev
     */
    public void onMergeInit(ActionEvent ev) {
        String mergeDomainStr = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("merge-domain");

        if (mergeDomainStr != null) {
            switch (mergeDomainStr) {
                case "ADDRESS" -> {
                    currentMergeDomain = MergeDomainEnum.MAILINGADDRESS;
                }
                case "STREET" -> {
                    currentMergeDomain = MergeDomainEnum.STREET;
                }
            }
            selectedObjectsToMerge = new ArrayList<>();

        } else {
            System.out.println("AddressBB.onMergeInit | null domain KV");
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal merging back-end configuration error", ""));
        }
    }

    /**
     * Listener for user requests to start the merge street operation
     *
     * @Deprecated
     * @param ev
     */
    public void onMergeStreetsInit(ActionEvent ev) {
        System.out.println("AddressBB.onMergeStreetsInit");
        currentMergeDomain = MergeDomainEnum.STREET;
        mailingStreetMergeSelectMode = true;
        selectedStreetList = new ArrayList<>();
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Use the check boxes to select two or more streets to merge", ""));
    }

    /**
     * Listener for user requests to start the merge address operation
     *
     * @Deprecated
     * @param ev
     */
    public void onMergeMailingAdddresssInit(ActionEvent ev) {
        System.out.println("AddressBB.onMergeMailingAdddresssInit");
        currentMergeDomain = MergeDomainEnum.MAILINGADDRESS;
        mailingAddressMergeSelectMode = true;
        selectedAddressList = new ArrayList<>();
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Use the check boxes to select two or more addressses to merge", ""));
    }

    /**
     * Listener for user finishing the selection of streets to merge
     *
     * @param ev
     */
    public void onMergeObjectSelectionComplete(ActionEvent ev) {
        PropertyCoordinator pc = getPropertyCoordinator();
        System.out.println("AddressBB.onMergeObjectSelectionComplete");
        switch (currentMergeDomain) {
            case MAILINGADDRESS -> {
                currentMergeRequest = pc.getMailingAddressMergeRequestSkeleton(getSessionBean().getSessUser());
            }
            case STREET -> {
                currentMergeRequest = pc.getMailingStreetMergeRequestSkeleton(getSessionBean().getSessUser());
            }
        } // close switch
        currentMergeRequest.setBobsToMerge(selectedObjectsToMerge);
        // verify sizing of the list
        if (currentMergeRequest != null) {
            if (currentMergeRequest.getBobsToMerge().size() > 1) {
                // inject the first street in the list as primary which user can change if desired
                currentMergeRequest.setMergeTarget(selectedObjectsToMerge.get(0));
                // take the primary out of the sub list
                onMergeRemoveObjectFromQueue(currentMergeRequest, selectedObjectsToMerge.get(0));
            } else {
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "You must select 2 or more streets for merging", ""));
            }
        } else {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal merging back-end configuration error", ""));
        }
    }

    /**
     * Listener for users to say go ahead and do the merge
     *
     * @param ev
     */
    public void onMergeCommit(ActionEvent ev) {
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            switch (currentMergeDomain) {
                case STREET -> {
                    System.out.println("AddressBB.onMergeCommit | street domain");
                    currentMergeRequest = pc.mergeStreets(currentMergeRequest);
                    // refresh after merge by getting new streets in our current zip
                    if(currentMergeRequest.getMergeTarget() instanceof MailingStreet strt){
                        selectCityStateZip(strt.getCityStateZip());
                        onStreetSelectLinkClick(strt);
                    }
                }
                case MAILINGADDRESS -> {
                    System.out.println("AddressBB.onMergeCommit | address domain");
                    currentMergeRequest = pc.mergeAddresses(currentMergeRequest);
                    // refresh after address collapse by reloading street's addresses
                    if(currentMergeRequest.getMergeTarget() instanceof MailingAddress mad){
                        onStreetSelectLinkClick(mad.getStreet());
                    }
                }
            }
            // reset our current zip's streets
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Merge success!", ""));
        } catch (BObStatusException | AuthorizationException | EventException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal merging back-end configuration error", ""));
        }
    }

    /**
     * Listener for user to confirm completion of the merge
     *
     * @param ev
     */
    public void onMergeDone(ActionEvent ev) {
        System.out.println("AddressBB.onMergeDone");
        onMergeReset();
    }

    /**
     * Turns off and nulls out street merging mode
     */
    private void onMergeReset() {
        currentMergeDomain = null;
        currentMergeRequest = null;
        selectedObjectsToMerge = new ArrayList<>();

    }

    /**
     * cancels street merge
     *
     * @param ev
     */
    public void onMergeAbort(ActionEvent ev) {
        System.out.println("AddressBB.onMergeAbort");
        onMergeReset();
    }

    // **********************
    // SAHRED Street and address merging crap
    // **********************
    /**
     * Listener for primary mergable selection
     *
     * @param req
     * @param mergable
     */
    public void onMergeSelectPrimaryObject(BobMergeRequest req, IFace_Mergable mergable) {
        if (req != null) {
            req.setMergeTarget(mergable);
            // take our primary out of the subordinate list
            onMergeRemoveObjectFromQueue(req, mergable);
        }
    }

    /**
     * Puts the target back in the queue
     *
     * @param req
     */
    public void onMergeClearPrimaryObject(BobMergeRequest req) {
        if (req != null && req.getMergeTarget() != null) {
            if (req.getBobsToMerge() != null) {
                req.getBobsToMerge().add(req.getMergeTarget());
            }
            req.setMergeTarget(null);
        }
    }

    /**
     * Removes mergable from queue
     *
     * @param req
     * @param mergable
     */
    public void onMergeRemoveObjectFromQueue(BobMergeRequest req, IFace_Mergable mergable) {
        if (req != null && !req.getBobsToMerge().isEmpty()) {
            boolean res = req.getBobsToMerge().remove(mergable);
            if (res) {
                System.out.println("AddressBB.onMergeRemoveObjectFromQueue | mergable found in list");
            } else {
                System.out.println("AddressBB.onMergeRemoveObjectFromQueue | mergable not found in list");
            }
        }
    }

    /**
     * ****************************************************************
     */
    /**
     * ************ Mailing address INFRASTRUCTURE ********************
     */
    /**
     * ****************************************************************
     */
    /**
     * Listener for user requests to view street info
     *
     * @param ms the MailingStreet to query
     */
    public void onStreetViewEditLinkClick(MailingStreet ms) {
        System.out.println("AddressBB.onStreetViewEditLinkClick");
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            currentStreet = pc.getMailingStreet(ms.getStreetID());
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex.toString());
        }
        editModeCurrentStreet = false;
    }

    /**
     * Adaptor method for reusing mergable object table
     *
     * @param mergableStreet
     */
    public void onStreetViewEditLinkClick(IFace_Mergable mergableStreet) {
        if (mergableStreet != null && mergableStreet instanceof MailingStreet) {
            onStreetSelectLinkClick((MailingStreet) mergableStreet);
        }
    }

    /**
     * Listener for user requests to create a new street
     */
    public void onMailingStreetCreateInitButtonChange() {
        PropertyCoordinator pc = getPropertyCoordinator();
        currentStreet = pc.getMailingStreetSkeleton(currentCityStateZip);
        editModeCurrentStreet = true;
    }

    /**
     * Listener for user requests to see building Numbers connected to this
     * street. Initiates DB search of this Street in this ZIP Code
     *
     * @param ms the MailingStreet to query
     */
    public void onStreetSelectLinkClick(MailingStreet ms) {
        currentStreet = ms;
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            mailingAddressList = pc.getMailingAddressListByStreet(currentStreet);
            if (mailingAddressList != null) {
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Search for addresses returned " + mailingAddressList.size(), ""));
            }
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Could not find Addresses by Street, Sorry", ""));
        }
    }

    public void onTestStreetSelect(MailingStreet ms) {
        System.out.println("AddressBB.onTestStreetSelect: ms=" + ms.getStreetID());
        onStreetSelectLinkClick(ms);
    }

    /**
     * Listener for user requests to search for a street
     *
     * @param ev
     */
    public void onSearchForMailingStreet(ActionEvent ev) {

        searchForMailingStreet();
    }

    /**
     * Undertakes a street search
     */
    public void searchForMailingStreet() {
        PropertyCoordinator pc = getPropertyCoordinator();
        System.out.println("AddressBB.searchForMailingStreet");
        try {
            streetList = pc.searchForMailingStreet(formStreet, currentCityStateZip);
            if (streetList != null) {
                Collections.sort(streetList);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Search for Streets returned " + streetList.size(), ""));
            }
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal: Could not search for streets, sorry! ", ""));

        }
    }

    /**
     * Listener for user requests to change to or from edit mode on mailing
     * address
     *
     * @param ev
     */
    public void onToggleStreetEditModeButtonChange(ActionEvent ev) {
        try {
            toggleStreetEditMode();
        } catch (InvocationTargetException ex) {
            System.out.println(ex.toString());
        }
    }

    /**
     * Toggles the mailing address edit mode
     *
     * @throws java.lang.reflect.InvocationTargetException
     */
    public void toggleStreetEditMode() throws InvocationTargetException {
        PropertyCoordinator pc = getPropertyCoordinator();
        System.out.println("AddressBB.toggleStreetEditMode");
        if (editModeCurrentStreet) {

            try {
                if (currentStreet.getStreetID() == 0) {
                    int freshStreetID = pc.insertMailingStreet(currentStreet, getSessionBean().getSessUser());
                    currentStreet = pc.getMailingStreet(freshStreetID);
                    getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO,
                                    "Street Insert successful", ""));
                } else {
                    pc.updateMailingStreet(currentStreet, getSessionBean().getSessUser());
                    onMailingStreetcommitNotes(null);
                    currentStreet = pc.getMailingStreet(currentStreet.getStreetID());
                    getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO,
                                    "Street update successful", ""));
                }
                searchForMailingStreet();

            } catch (BObStatusException | IntegrationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Could not update or add  this street", ""));
            }
        } else {
            onMailingAddressNoteInitButtonChange(null);
            onMailingStreetPrepareNotes();
        }
        editModeCurrentStreet = !editModeCurrentStreet;
    }

    /**
     * Listener for user requests to start the process of adding a mailing
     * address
     *
     * @param ev
     */
    public void onMailingStreetInitButtonChange(ActionEvent ev) {
        PropertyCoordinator pc = getPropertyCoordinator();
        currentStreet = pc.getMailingStreetSkeleton(currentCityStateZip);
        editModeCurrentStreet = true;

    }

    /**
     * Listener for user requests to abort street edit
     *
     * @param ev
     */
    public void onMailingStreetEditAbort(ActionEvent ev) {
        System.out.println("AddressBB.onMailingStreetEditAbort");
        editModeCurrentStreet = false;
    }

    /**
     * Listener to start the street deac process
     *
     * @param ev
     */
    public void onMailingStreetDeactivateInitLinkClick(ActionEvent ev) {
        System.out.println("AddressBB.onMailingStreetDeactivateInit");
        try {
            permissionAllowCurrentStreetDeactivate = getPropertyCoordinator().permissionCheckpointDeacStreet(currentStreet, getSessionBean().getSessUser());
        } catch (IntegrationException | AuthorizationException | BObStatusException | EventException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
    }

    /**
     * Listener for user requests to confirm delete of street
     */
    public void onMailingStreetDeactivateCommit() {
        PropertyCoordinator pc = getPropertyCoordinator();
        System.out.println("AddressBB.onMailingStreetDeactivateCommit");
        try {
            pc.deactivateMailingStreet(currentStreet, getSessionBean().getSessUser());
            searchForMailingStreet();
            // clear the current street
            currentStreet = null;
            mailingAddressList = new ArrayList<>();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Street deactivated", ""));
        } catch (BObStatusException | IntegrationException | AuthorizationException | EventException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
    }

    /**
     * Listener for user requests to view the current address
     *
     * @param ev
     */
    public void onMailingAddressViewCurrentAddress(ActionEvent ev) {
        getSessionBean().setSessMailingAddress(currentMailingAddress);

    }

    /**
     * Listener to view current Maling addresss
     *
     * @param ma
     */
    public void onMailingAddressViewEditLinkClick(MailingAddress ma) {
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            currentMailingAddress = pc.getMailingAddress(ma.getAddressID());
            currentMailingAddressRecursiveLinkList = pc.getMailingAddressLinkList(ma, getSessionBean().getSessUser());

            System.out.println("AddressBB.onMailingAddressViewEditLinkClick | reverse searched on address ID: " + ma.getAddressID());
        } catch (IntegrationException | BObStatusException | AuthorizationException | EventException ex) {
            System.out.println(ex.toString());
        }
        editModeCurrentAddress = false;
        formAddressVerified = currentMailingAddress.getVerifiedTS() != null;
        getSessionBean().setSessMailingAddress(currentMailingAddress);
    }

    /**
     * Adaptor method for reusing data table for merging objects
     *
     * @param mergableMad
     */
    public void onMailingAddressViewEditLinkClick(IFace_Mergable mergableMad) {
        if (mergableMad != null && mergableMad instanceof MailingAddress) {
            onMailingAddressViewEditLinkClick((MailingAddress) mergableMad);
        }
    }

    /**
     * Toggles the mailing address edit mode
     *
     * @param ev
     */
    public void toggleMailingAddressEditMode(ActionEvent ev) {
        PropertyCoordinator pc = getPropertyCoordinator();
        System.out.println("AddressBB.toggleMailingAddressEditMode");
        if (isEditModeCurrentAddress()) {
            int freshID;
            try {
                if (formAddressVerified && currentMailingAddress != null) {
                    currentMailingAddress.setVerifiedBy(getSessionBean().getSessUser());
                    currentMailingAddress.setVerifiedTS(LocalDateTime.now());
                }
                if (currentMailingAddress.getAddressID() == 0) {
                    freshID = pc.insertMailingAddress(currentMailingAddress, getSessionBean().getSessUser());
                    getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO,
                                    "Mailing Address Insert successful", ""));
                    currentMailingAddress = pc.getMailingAddress(freshID);
                } else {
                    pc.updateMailingAddress(currentMailingAddress, getSessionBean().getSessUser());
                    onMailingAddressCommitNotes(null);
                    getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO,
                                    "Mailing Address update successful", ""));
                    currentMailingAddress = pc.getMailingAddress(currentMailingAddress.getAddressID());
                }
                if (currentStreet != null) {
                    onStreetSelectLinkClick(currentStreet);
                }
            } catch (BObStatusException | IntegrationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                ex.getMessage(), ""));
            }
        } else {
            onMailingAddressNoteInitButtonChange(null);
            try {
                onMailingAddressPrepareNotes();
            } catch (InvocationTargetException ex) {
                System.out.println(ex.toString());
            }
        }
        setEditModeCurrentAddress(!isEditModeCurrentAddress());
    }

    /**
     * Listener for user requests to start the process of adding a mailing
     * address
     *
     * @param ev
     */
    public void onMailingAddressAddInitButtonChange(ActionEvent ev) {
        PropertyCoordinator pc = getPropertyCoordinator();
        currentMailingAddress = pc.getMailingAddressSkeleton();
        if (currentStreet != null) {
            currentStreet.setCityStateZip(currentCityStateZip);
            currentMailingAddress.setStreet(currentStreet);
        }
        editModeCurrentAddress = true;

    }

    /**
     * Listener for user requests to abort any mailing address operation
     *
     * @param ev
     */
    public void onMailingAddressAbortOperationButtonChange(ActionEvent ev) {
        editModeCurrentAddress = false;
    }

    /**
     * Listener for user requests to start the address deactivation process
     *
     * @param ev
     */
    public void onMailingAddressDeactivateInitLinkClick(ActionEvent ev) {
        System.out.println("AddressBB.onMailingAddressDeactivateInitLinkClick");
        // we should have a current addresss to deac
        try {
            permissionAllowCurrentMailingAddressDeactivation = getPropertyCoordinator().permissionCheckpointDeacMailingAddress(currentMailingAddress, getSessionBean().getSessUser());
        } catch (IntegrationException | AuthorizationException | BObStatusException | EventException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }

    }

    /**
     *
     * Listener for user requests to commit the deactivation operation
     *
     * @param ev
     */
    public void onMailingAddressDeactivateConfirmButtonChange(ActionEvent ev) {
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            pc.deactivateMailingAddress(currentMailingAddress, getSessionBean().getSessUser());
            // refresh our address list if we have a current street
            if (currentStreet != null) {
                onStreetSelectLinkClick(currentStreet);
            }
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Mailing Address deactivation successful! ", ""));
        } catch (BObStatusException | IntegrationException | AuthorizationException | EventException ex) {
            System.out.println("ex");
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Could deactivate address: " + ex.getMessage(), ""));
        }
    }

    /**
     * Listener for user requests to start the note process on a mailing address
     *
     * @param ev
     */
    public void onMailingAddressNoteInitButtonChange(ActionEvent ev) {
        formNotesAddress = "";
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            if (Objects.nonNull(currentMailingAddress)) {
                currentMailingAddress = pc.getMailingAddress(currentMailingAddress.getAddressID());
            }

        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }
    }

    /**
     * user requests to commit a note to a mailin address
     *
     * @param ev
     */
    public void onMailingAddressNoteCommitButtonChage(ActionEvent ev) {
        SystemCoordinator sc = getSystemCoordinator();
        PropertyCoordinator pc = getPropertyCoordinator();
        if (currentMailingAddress != null) {
            MessageBuilderParams mbp = new MessageBuilderParams(currentMailingAddress.getNotes(), null, null, formNotesAddress, getSessionBean().getSessUser(), null);
            currentMailingAddress.setNotes(sc.appendNoteBlock(mbp));
            try {
                sc.writeNotes(currentMailingAddress, getSessionBean().getSessUser());
                currentMailingAddress = pc.getMailingAddress(currentMailingAddress.getAddressID());
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Note write successful! Woot woot!", ""));

            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Fatal note error: " + ex.getMessage(), ""));
            }
            formNotesAddress = "";
        }
    }

    /**
     * Listener for Preparing notes for Mailing Address
     */
    public void onMailingAddressPrepareNotes() throws InvocationTargetException {
        SystemCoordinator sc = getSystemCoordinator();
        String fieldDump = "";
        fieldDump = sc.getFieldDump(currentMailingAddress);
        System.out.println(fieldDump);
        setFormNotesAddress(fieldDump);
    }

    /**
     * user requests to commit a note to a mailing address
     *
     * @param ev
     */
    public void onMailingAddressCommitNotes(ActionEvent ev) {
        SystemCoordinator sc = getSystemCoordinator();
        PropertyCoordinator pc = getPropertyCoordinator();
        if (Objects.nonNull(currentMailingAddress)) {
            MessageBuilderParams mbp = new MessageBuilderParams(currentMailingAddress.getNotes(), null, null, formNotesAddress, getSessionBean().getSessUser(), null);
            currentMailingAddress.setNotes(sc.universalAppendNoteBlock(mbp));
            try {
                sc.writeNotes(currentMailingAddress, getSessionBean().getSessUser());
                currentMailingAddress = pc.getMailingAddress(currentMailingAddress.getAddressID());
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Note write successful! Woot woot!", ""));

            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Fatal note error: " + ex.getMessage(), ""));
            }
            formNotesAddress = "";
        }
    }

    /**
     * Listener for user requests to start the note process on a street
     *
     * @param ev
     */
    public void onMailingStreetNoteInitButtonChange(ActionEvent ev) {
        formNotesStreet = "";
    }

    /**
     * Listener for user Requests to commit notes to the currentStreet
     *
     * @param ev
     */
    public void onMailingStreetNoteCommitButtonChage(ActionEvent ev) {
        SystemCoordinator sc = getSystemCoordinator();
        PropertyCoordinator pc = getPropertyCoordinator();
        if (currentStreet != null) {
            MessageBuilderParams mbp = new MessageBuilderParams(currentStreet.getNotes(), null, null, formNotesStreet, getSessionBean().getSessUser(), null);
            currentStreet.setNotes(sc.appendNoteBlock(mbp));
            try {
                sc.writeNotes(currentStreet, getSessionBean().getSessUser());
                currentStreet = pc.getMailingStreet(currentStreet.getStreetID());
            } catch (BObStatusException | IntegrationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "FATAL Error on mailing street note commit", ""));
            }
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Successfully added note to mailing", ""));
            if (Objects.nonNull(currentMailingAddress)) {
                currentMailingAddress.setStreet(currentStreet);
            }
        }
        // I don't think we want this here
//        editModeCurrentAddress = true;

    }

    /**
     * Listener for Preparing notes for MailingStreet
     */
    public void onMailingStreetPrepareNotes() throws InvocationTargetException {
        SystemCoordinator sc = getSystemCoordinator();
        String fieldDump = "";
        fieldDump = sc.getFieldDump(currentStreet);
        System.out.println(fieldDump);
        setFormNotesStreet(fieldDump);
    }

    /**
     * Listener for user Requests to commit notes to the currentStreet
     *
     * @param ev
     */
    public void onMailingStreetcommitNotes(ActionEvent ev) {
        SystemCoordinator sc = getSystemCoordinator();
        PropertyCoordinator pc = getPropertyCoordinator();
        if (Objects.nonNull(currentStreet)) {
            MessageBuilderParams mbp = new MessageBuilderParams(currentStreet.getNotes(), null, null, formNotesStreet, getSessionBean().getSessUser(), null);
            currentStreet.setNotes(sc.universalAppendNoteBlock(mbp));
            try {
                sc.writeNotes(currentStreet, getSessionBean().getSessUser());
                currentStreet = pc.getMailingStreet(currentStreet.getStreetID());
            } catch (BObStatusException | IntegrationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "FATAL Error on mailing street note commit", ""));
            }
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Successfully added note to mailing", ""));
            if (Objects.nonNull(currentMailingAddress)) {
                currentMailingAddress.setStreet(currentStreet);
            }
        }
        editModeCurrentAddress = true;
    }

    private void refreshCurrentMailingAddress() throws IntegrationException, BObStatusException {
        PropertyCoordinator pc = getPropertyCoordinator();
        currentMailingAddress = pc.getMailingAddress(currentMailingAddress.getAddressID());
    }

    /**
     * *********************************************************
     */
    /**
     * ************ ADDRESS QUICK ADD **************************
     */
    /**
     * *********************************************************
     */
    /**
     * Listener to start the quick add process
     *
     * @param ev
     */
    public void onAddressQuickAddInit(ActionEvent ev) {
        PropertyCoordinator pc = getPropertyCoordinator();
        System.out.println("AddressBB.onAddressQuickAddInit");
        if (addressQuickAddBundle == null) {
            addressQuickAddBundle = pc.getMailingAddressQuickAddBundleSkeleton();
            addressQuickAddDisableFields = false;
        }

    }

    /**
     * Listener for the user's completion of the initial proposal form which is
     * a building number, street, and ZIP Code
     *
     * @param ev
     */
    public void onAddressQuickAddFieldSubmit(ActionEvent ev) {
        try {
            PropertyCoordinator pc = getPropertyCoordinator();
            System.out.println("AddressBB.onAddressQuickAddFieldSubmit");
            addressQuickAddBundle = pc.searchForMatchingAddressCandidates(addressQuickAddBundle);
            addressQuickAddDisableFields = true;
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
            addressQuickAddDisableFields = false;
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            ex.getMessage(), ""));
        }

    }

    /**
     * Listener for the user selecting that none of the close matches are the
     * best candidate and instead a fresh building and street should be written
     *
     * @param ev
     */
    public void onAddressQuickAddConfirmFreshInsert(ActionEvent ev) {
        System.out.println("AddressBB.onAddressQuickAddConfirmFreshInsert");
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            int freshAddr = pc.mailingAddressQuickAddCommitNewAddress(addressQuickAddBundle, getSessionBean().getSessUser());
            onMailingAddressViewEditLinkClick(pc.getMailingAddress(freshAddr));
            onAddressQuickAddResetQuickAdd(null);
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));

        }
    }

    /**
     * Listener for user to select a match closer to their entries rather than a
     * new insert
     *
     * @param matchMad the alternative to then use as our link target
     */
    public void onAddressQuickAddChooseCloseMatch(MailingAddress matchMad) {
        System.out.println("AddressBB.onAddressQuickAddChooseCloseMatch | chosen match addressID: " + matchMad.getAddressID());
        getSessionBean().setSessMailingAddress(matchMad);
        currentMailingAddress = matchMad;

    }

    /**
     * Reactivation of initial user address fields without wiping them
     *
     * @param ev
     */
    public void onAddressQuickAddReviseFieldValues(ActionEvent ev) {
        System.out.println("AddressBB.onAddressQuickAddReviseFieldValues");
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            addressQuickAddDisableFields = false;
            addressQuickAddBundle = pc.mailingAddressQuickAddClearSearch(addressQuickAddBundle);
        } catch (BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            ex.getMessage(), ""));
        }

    }

    /**
     * Abort the current quick add sequence and start over with fresh fields
     *
     * @param ev
     */
    public void onAddressQuickAddResetQuickAdd(ActionEvent ev) {
        addressQuickAddDisableFields = false;
        addressQuickAddBundle = getPropertyCoordinator().getMailingAddressQuickAddBundleSkeleton();
    }

    /**
     * *********************************************************
     */
    /**
     * ************ LINK MANAGEMENT TOOLS  *********************
     */
    /**
     * *********************************************************
     */
    /**
     * Listener to start linking process
     *
     * @param holder
     */
    public void onLinkPersonPropToAddressInitButtonChange(IFace_addressListHolder holder) {
        currentAddressListHolder = holder;
        configureSessionObjectsFromAddressListHolder(holder);
        configureAddressListHolderUpdateComponent();
        System.out.println("AddressBB.onLinkPersonPropToAddressInitButtonChange: target PK: " + holder.getTargetObjectPK());
    }

    /**
     * If we are initiating linking to a given property unit, we'll set that as
     * the session one here for use by the addressing system
     *
     * @param holder
     */
    private void configureSessionObjectsFromAddressListHolder(IFace_addressListHolder holder) {
        if (holder != null) {
            if (holder instanceof PropertyUnitDataHeavy pudh) {
                getSessionBean().setSessPropertyUnit(pudh);
                System.out.println("AddressBB.configureSessionObjectsFromAddressListHolder | setting unit to session unit: " + pudh.getUnitID());
            }
        }
    }

    /**
     * Listener to start the linking process to this bean's current address
     *
     * @param ev
     */
    public void onLinkToCurrentMailingAddressInitButtonChange(ActionEvent ev) {
        System.out.println("AddressBB.onLinkToCurrentMailingAddressInitButtonChange");
    }

    /**
     * Listener for user requests to link this bean's current address to session
     * property
     *
     * @param ev
     */
    public void onLinkCurrentAddressToSessionProperty(ActionEvent ev) {
        onLinkToSelectedMailingAddressInitButtonChange(getSessionBean().getSessProperty());

    }

    /**
     * Listener to close linking dialog without linking
     *
     * @param ev
     */
    public void onCloseLinkingDialog(ActionEvent ev) {
        System.out.println("AddressBB.onCloseLinkingDialog");
        // nothing to do 
    }

    /**
     * Listener for user requests to link this bean's current address to session
     * person
     *
     * @param ev
     */
    public void onLinkCurrentAddressToSessionPerson(ActionEvent ev) {
        onLinkToSelectedMailingAddressInitButtonChange(getSessionBean().getSessPerson());
    }

    /**
     * Listener for user requests to link this bean's current address to session
     * person
     *
     * @param ev
     */
    public void onLinkCurrentAddressToSessionUnit(ActionEvent ev) {
        onLinkToSelectedMailingAddressInitButtonChange(getSessionBean().getSessPropertyUnit());
    }

    /**
     * Listener for user requests to start the address linking process
     *
     * @param alholder
     */
    public void onLinkToSelectedMailingAddressInitButtonChange(IFace_addressListHolder alholder) {
        PropertyCoordinator pc = getPropertyCoordinator();
        currentAddressListHolder = alholder;

        // Try not updating this since the first operation should store the component id
//        configureAddressListHolderUpdateComponent();
        if (currentAddressListHolder != null && getSessionBean().getSessMailingAddress() != null) {
            getSessionBean().setSessAddressListHolder(currentAddressListHolder);
            currentMailingAddress = getSessionBean().getSessMailingAddress();
            currentMailingAddressLink = pc.getMailingAddressLinkSkeleton(getSessionBean().getSessMailingAddress());
            if (currentAddressListHolder.getMailingAddressLinkList() != null) {
                currentMailingAddressLink.setPriority(currentAddressListHolder.getMailingAddressLinkList().size() + 1);
            }
            configureLinkedObjectRoleList(null);
            editModeCurrentAddressLink = true;
        } else {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error! Could not initiate the address linking process", ""));
        }
    }

    /**
     * Special method that extracts from the HTTP request the value of param
     * initiating-address-list-component-id which allows multiple pages to use
     * the same infrastructure for address link management, specifically allows
     * the shared address components to tell their parent page component to
     * refresh itself after a change operation occurs on a list element
     */
    private void configureAddressListHolderUpdateComponent() {
        addressListHolderComponentForUpdatePostMADLinkOperation
                = FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getRequestParameterMap()
                        .get("initiating-address-list-component-id");
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Viewing address link", ""));
        System.out.println("AddressBB.configureAddressListHolderUpdateComponent : " + addressListHolderComponentForUpdatePostMADLinkOperation);

    }

    /**
     * Asks this bean's currentAddressList holder what type it is, and sets the
     * association candidate list appropriately for the dialog that's about to
     * appear
     *
     * @param madLink if null, I'll make the list reflect the
     * currentAddressListHolder otherwise, I'll make the list reflect our
     * current link
     */
    public void configureLinkedObjectRoleList(MailingAddressLink madLink) {
        SystemCoordinator sc = getSystemCoordinator();
        try {
            if (madLink != null) {
                mailingAddressLinkRoleCandidateList = sc.assembleLinkedObjectRolesBySchema(madLink.getLinkedObjectRoleSchemaEnum());
                return;
            }
            if (currentAddressListHolder != null) {
                if (currentAddressListHolder instanceof Human) {
                    mailingAddressLinkRoleCandidateList = sc.assembleLinkedObjectRolesBySchema(LinkedObjectSchemaEnum.MailingaddressHuman);
                } else if (currentAddressListHolder instanceof Parcel) {
                    mailingAddressLinkRoleCandidateList = sc.assembleLinkedObjectRolesBySchema(LinkedObjectSchemaEnum.ParcelMailingaddress);
                } else if (currentAddressListHolder instanceof PropertyUnitDataHeavy) {
                    mailingAddressLinkRoleCandidateList = sc.assembleLinkedObjectRolesBySchema(LinkedObjectSchemaEnum.ParcelUnitMailingAddress);
                }
            }
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatak error! Could not setup linked object role list" + ex.getMessage(), ""));
        }
    }

    /**
     * Finalizes the MailingAddres linking process
     *
     * @param ev
     */
    public void onLinkToSelectedMailingAddressCommit(ActionEvent ev) {
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            int linkid = pc.linkToMailingAddress(currentAddressListHolder, currentMailingAddressLink, getSessionBean().getSessUser());
            System.out.println("AddressBB.onLinkToSelectedMailingAddressCommit | new link ID: " + linkid);
            currentMailingAddressLink = pc.getMailingAddressLink(currentAddressListHolder, linkid);
            refreshCurrentAddressListHolderLinks();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Successfully wrote address link", ""));
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal Address linking error: " + ex.getMessage(), ""));
        }
    }

    /**
     * Page specific activator for viewing address links on property units
     *
     * @param madLink
     * @param pudh
     */
    public void onMailingAddressPropertyUnitLinkView(MailingAddressLink madLink, PropertyUnitDataHeavy pudh) {
        getSessionBean().setSessPropertyUnit(pudh);
        onMailingAddressLinkViewEditInit(madLink);

    }

    /**
     * Entry point for viewing/editing this mailing address link stuff
     *
     * @param madLink
     */
    public void onMailingAddressLinkViewEditInit(MailingAddressLink madLink) {
        PropertyCoordinator pc = getPropertyCoordinator();
        currentMailingAddressLink = madLink;

        configureAddressLinkHolderUsingCurrentMailingAddressLink();
        configureAddressListHolderUpdateComponent();
        configureLinkedObjectRoleList(madLink);
        try {
            if (Objects.nonNull(currentAddressListHolder)) {
                currentMailingAddressLink = pc.getMailingAddressLink(currentAddressListHolder, currentMailingAddressLink.getLinkID());
            }
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex.toString());
        }
        editModeCurrentAddressLink = false;
        System.out.println("AddressBB.onMalingAddressLinkViewEditInit | madLink: linkid " + madLink.getLinkID());
    }

    /**
     * Examines the schema enum of the currentMailingAddressLink and loads the
     * proper main bob for refreshing
     */
    private void configureAddressLinkHolderUsingCurrentMailingAddressLink() {
        if (currentMailingAddressLink != null && currentMailingAddressLink.getLinkedObjectRoleSchemaEnum() != null) {
            switch (currentMailingAddressLink.getLinkedObjectRoleSchemaEnum()) {
                case MailingaddressHuman:
                    currentAddressListHolder = getSessionBean().getSessPerson();
                    break;
                case ParcelMailingaddress:
                    currentAddressListHolder = getSessionBean().getSessProperty();
                    break;
                case ParcelUnitMailingAddress:
                    currentAddressListHolder = getSessionBean().getSessPropertyUnit();

                default:
                    System.out.println("AddressBB.configureAddressLinkHolderUsingCurrentMailingAddressLink | no address list holder case setup");
            }

        }

    }

    /**
     * Listener for user toggling of the edit/save mailing address record
     *
     * @param ev
     */
    public void onMailingAddressLinkEditToggleButtonChange(ActionEvent ev) {
        System.out.println("AddressBB.onMailingAddressLinkEditToggleButtonChange");
        if (editModeCurrentAddressLink) {
            if (currentMailingAddressLink != null) {
                if (currentMailingAddressLink.getLinkID() == 0) {
                    onLinkToSelectedMailingAddressCommit(ev);
                } else {
                    updateCurrentMailingAddressLink();
                }
                refreshCurrentAddressListHolderLinks();
            }
        } else {
            formNotesAddressLink = "";
            try {
                onMailingAddressLinkPrepareNotes();
            } catch (InvocationTargetException ex) {
                System.out.println(ex.toString());
            }
        }
        editModeCurrentAddressLink = !editModeCurrentAddressLink;
    }

    /**
     * Listener for user requests to stop editing an address link
     *
     * @param ev
     */
    public void onMailingAddressLinkEditAbort(ActionEvent ev) {
        System.out.println("AddressBB.madLinkAbort");
        editModeCurrentAddressLink = false;
    }

    /**
     * Extracts and installs a fresh copy of the current madlink
     */
    private void refreshCurrentMailingAddressLink() throws IntegrationException, BObStatusException {
        PropertyCoordinator pc = getPropertyCoordinator();
        if (currentAddressListHolder != null && currentMailingAddressLink != null) {
            currentMailingAddressLink = pc.getMailingAddressLink(currentAddressListHolder, currentMailingAddressLink.getLinkID());
        }
    }

    /**
     * Internal caller to update a mad link
     */
    private void updateCurrentMailingAddressLink() {
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            onMailingAddressLinkCommitNotes(null);
            pc.updateMailingAddressLink(currentMailingAddressLink, getSessionBean().getSessUser());
            refreshCurrentMailingAddressLink();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Succssfully updated address link!", ""));
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal mailing address link update error: " + ex.getMessage(), ""));
        }
    }

    /**
     * Hides the given madlink
     *
     * @param madLink
     */
    public void onToggleMailingAddressLinkHideUnhide(MailingAddressLink madLink) {
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            if (madLink.isHidden()) {
                // unhide hidden MAD links
                pc.hideUnhideMailingAddressLink(madLink, false, getSessionBean().getSessUser());
                System.out.println("AddressBB.onToggleMailingAddressLinkHideUnhide | unhiding | madlinkID: " + madLink.getLinkID());
            } else {
                // hide unhidden MAD links
                pc.hideUnhideMailingAddressLink(madLink, true, getSessionBean().getSessUser());
                System.out.println("AddressBB.onToggleMailingAddressLinkHideUnhide | hiding | madlinkID: " + madLink.getLinkID());
            }
            configureAddressLinkHolderUsingCurrentMailingAddressLink();
            refreshCurrentMailingAddressLink();
            triggerRefreshOfCurrentPerson();
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
        }
    }

    /**
     * Potentially completely awful and horrible design which is used to trigger
     * the refresh of our session person after, say, the edit of a mailing
     * address link's hidden/not hidden status
     */
    private void triggerRefreshOfCurrentPerson() {
        PersonBB personBB = (PersonBB) getFacesContext().getApplication().evaluateExpressionGet(getFacesContext(), "#{personBB}", PersonBB.class);
        if (personBB != null) {
            System.out.println("Got handle on PersonBB | refreshing current person");
            personBB.refreshCurrentPersonAndUpdateSessionPerson(null);
        }
    }

    /**
     * Starts deac process
     *
     * @param ev
     */
    public void onMailingAddressLinkDeactivateInitLinkClick(ActionEvent ev) {
        System.out.println("AddressBB.onMailingAddressLinkDeactivateInitLinkClick | linkid " + currentMailingAddressLink.getLinkID());
    }

    /**
     * listener for user finalization of the address link removal process
     *
     * @param ev
     */
    public void onMailingAddressLinkDeactivateCommitButtonChange(ActionEvent ev) {
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            pc.deactivateLinkToMailingAddress(currentMailingAddressLink, getSessionBean().getSessUser());
            currentMailingAddressLink = pc.getMailingAddressLink(currentAddressListHolder, currentMailingAddressLink.getLinkID());
            refreshCurrentAddressListHolderLinks();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Tada! And it's gone! ", ""));
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal mailing address link deactivation error: " + ex.getMessage(), ""));
        }
    }

    /**
     * Internal method for getting a new copy of our AddressListHolder after
     * updates
     */
    private void refreshCurrentAddressListHolderLinks() {
        PropertyCoordinator pc = getPropertyCoordinator();
        if (currentAddressListHolder != null) {
            try {
                List<MailingAddressLink> madLinkList = pc.getMailingAddressLinkList(currentAddressListHolder);
                currentAddressListHolder.setMailingAddressLinkList(madLinkList);
                if (currentAddressListHolder instanceof PropertyDataHeavy) {
                    if (getSessionBean().getSessProperty() != null) {
                        getSessionBean().getSessProperty().setMailingAddressLinkList(madLinkList);
                    }
                } else if (currentAddressListHolder instanceof Person) {
                    if (getSessionBean().getSessPerson() != null) {
                        getSessionBean().getSessPerson().setMailingAddressLinkList(madLinkList);
                    }
                }
                getSessionBean().setSessMailingAddressLinkRefreshedList(madLinkList);
            } catch (BObStatusException | IntegrationException ex) {
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Fatal object reload error: " + ex.getMessage(), ""));
            }
        }
    }

    /**
     * Listener for user requests to start the note process on a street
     *
     * @param ev
     */
    public void onMailingAddressLinkNoteInitButtonChange(ActionEvent ev) {
        formNotesAddressLink = "";
    }

    public void onMailingAddressLinkNoteAbort(ActionEvent ev) {
        formNotesAddressLink = "";
    }

    /**
     * Listener for user Requests to commit notes to the currentStreet
     *
     * @param ev
     */
    public void onMailingAddressLinkNoteCommitButtonChage(ActionEvent ev) {
        SystemCoordinator sc = getSystemCoordinator();
        PropertyCoordinator pc = getPropertyCoordinator();
        if (currentMailingAddressLink != null) {
            MessageBuilderParams mbp = new MessageBuilderParams(currentMailingAddressLink.getLinkNotes(), null, null, formNotesAddressLink, getSessionBean().getSessUser(), null);
            currentMailingAddressLink.setLinkNotes(sc.appendNoteBlock(mbp));
            try {
                pc.updateMailingAddressLink(currentMailingAddressLink, getSessionBean().getSessUser());
                currentMailingAddressLink = pc.getMailingAddressLink(currentAddressListHolder, currentMailingAddressLink.getLinkID());
            } catch (BObStatusException | IntegrationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "FATAL Error on mailing address note commit", ""));
            }
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Successfully added note to mailing address link", ""));

        }

    }

    /**
     * Listener for user Requests to commit notes to the MailingAddressLink
     *
     * @param ev
     */
    public void onMailingAddressLinkCommitNotes(ActionEvent ev) {
        SystemCoordinator sc = getSystemCoordinator();
        PropertyCoordinator pc = getPropertyCoordinator();
        if (Objects.nonNull(currentMailingAddressLink)) {
            MessageBuilderParams mbp = new MessageBuilderParams(currentMailingAddressLink.getLinkNotes(), null, null, formNotesAddressLink, getSessionBean().getSessUser(), null);
            currentMailingAddressLink.setLinkNotes(sc.universalAppendNoteBlock(mbp));
        }

        formNotesAddressLink = "";
    }

    /**
     * Listener for Preparing notes for MailingAddressLink
     */
    public void onMailingAddressLinkPrepareNotes() throws InvocationTargetException {
        SystemCoordinator sc = getSystemCoordinator();
        if (currentMailingAddressLink != null) {
            String fieldDump = sc.getFieldDump(currentMailingAddressLink);
            System.out.println(fieldDump);
            setFormNotesAddressLink(fieldDump);
        }
    }

    /**
     * *********************************************************
     */
    /**
     * ************ GETTERS AND SETTERS    *********************
     */
    /**
     * *********************************************************
     */
    /**
     * @return the currentMailingAddress
     */
    public MailingAddress getCurrentMailingAddress() {
        return currentMailingAddress;
    }

    /**
     * @return the editModeCurrentAddress
     */
    public boolean isAddressEditMode() {
        return isEditModeCurrentAddress();
    }

    /**
     * @return the currentCityStateZip
     */
    public MailingCityStateZip getCurrentCityStateZip() {
        return currentCityStateZip;
    }

    /**
     * @return the formBuildingNo
     */
    public String getFormBuildingNo() {
        return formBuildingNo;
    }

    /**
     * @return the formStreet
     */
    public String getFormStreet() {
        return formStreet;
    }

    /**
     * @return the formCity
     */
    public String getFormCity() {
        return formCity;
    }

    /**
     * @return the formZip
     */
    public String getFormZip() {
        return formZip;
    }

    /**
     * @param currentMailingAddress the currentMailingAddress to set
     */
    public void setCurrentMailingAddress(MailingAddress currentMailingAddress) {
        this.currentMailingAddress = currentMailingAddress;
    }

    /**
     * @param addressEditMode the editModeCurrentAddress to set
     */
    public void setAddressEditMode(boolean addressEditMode) {
        this.setEditModeCurrentAddress(addressEditMode);
    }

    /**
     * @param currentCityStateZip the currentCityStateZip to set
     */
    public void setCurrentCityStateZip(MailingCityStateZip currentCityStateZip) {
        this.currentCityStateZip = currentCityStateZip;
    }

    /**
     * @param formBuildingNo the formBuildingNo to set
     */
    public void setFormBuildingNo(String formBuildingNo) {
        this.formBuildingNo = formBuildingNo;
    }

    /**
     * @param formStreet the formStreet to set
     */
    public void setFormStreet(String formStreet) {
        this.formStreet = formStreet;
    }

    /**
     * @param formCity the formCity to set
     */
    public void setFormCity(String formCity) {
        this.formCity = formCity;
    }

    /**
     * @param formZip the formZip to set
     */
    public void setFormZip(String formZip) {
        this.formZip = formZip;
    }

    /**
     * @return the addressSourceList
     */
    public List<BOBSource> getAddressSourceList() {
        return addressSourceList;
    }

    /**
     * @param addressSourceList the addressSourceList to set
     */
    public void setAddressSourceList(List<BOBSource> addressSourceList) {
        this.addressSourceList = addressSourceList;
    }

    /**
     * @return the qcszEnumList
     */
    public List<QueryMailingCityStateZip> getQcszEnumList() {
        return qcszEnumList;
    }

    /**
     * @param qcszEnumList the qcszEnumList to set
     */
    public void setQcszEnumList(List<QueryMailingCityStateZip> qcszEnumList) {
        this.qcszEnumList = qcszEnumList;
    }

    /**
     * @return the selectedCSZQuery
     */
    public QueryMailingCityStateZip getSelectedCSZQuery() {
        return selectedCSZQuery;
    }

    /**
     * @param selectedCSZQuery the selectedCSZQuery to set
     */
    public void setSelectedCSZQuery(QueryMailingCityStateZip selectedCSZQuery) {
        this.selectedCSZQuery = selectedCSZQuery;
    }

    /**
     * @return the streetList
     */
    public List<MailingStreet> getStreetList() {
        return streetList;
    }

    /**
     * @return the editModeCurrentStreet
     */
    public boolean isEditModeCurrentStreet() {
        return editModeCurrentStreet;
    }

    /**
     * @return the editModeCurrentAddress
     */
    public boolean isEditModeCurrentAddress() {
        return editModeCurrentAddress;
    }

    /**
     * @param streetList the streetList to set
     */
    public void setStreetList(List<MailingStreet> streetList) {
        this.streetList = streetList;
    }

    /**
     * @param editModeCurrentStreet the editModeCurrentStreet to set
     */
    public void setEditModeCurrentStreet(boolean editModeCurrentStreet) {
        this.editModeCurrentStreet = editModeCurrentStreet;
    }

    /**
     * @param editModeCurrentAddress the editModeCurrentAddress to set
     */
    public void setEditModeCurrentAddress(boolean editModeCurrentAddress) {
        this.editModeCurrentAddress = editModeCurrentAddress;
    }

    /**
     * @return the currentStreet
     */
    public MailingStreet getCurrentStreet() {
        return currentStreet;
    }

    /**
     * @param currentStreet the currentStreet to set
     */
    public void setCurrentStreet(MailingStreet currentStreet) {
        this.currentStreet = currentStreet;
    }

    /**
     * @return the formPOBox
     */
    public boolean isFormPOBox() {
        return formPOBox;
    }

    /**
     * @param formPOBox the formPOBox to set
     */
    public void setFormPOBox(boolean formPOBox) {
        this.formPOBox = formPOBox;
    }

    /**
     * @return the mailingAddressList
     */
    public List<MailingAddress> getMailingAddressList() {
        return mailingAddressList;
    }

    /**
     * @param mailingAddressList the mailingAddressList to set
     */
    public void setMailingAddressList(List<MailingAddress> mailingAddressList) {
        this.mailingAddressList = mailingAddressList;
    }

    /**
     * @return the streetListFiltered
     */
    public List<MailingStreet> getStreetListFiltered() {
        return streetListFiltered;
    }

    /**
     * @return the mailingAddressListFiltered
     */
    public List<MailingAddress> getMailingAddressListFiltered() {
        return mailingAddressListFiltered;
    }

    /**
     * @return the cityStateZipListFiltered
     */
    public List<MailingCityStateZip> getCityStateZipListFiltered() {
        return cityStateZipListFiltered;
    }

    /**
     * @param streetListFiltered the streetListFiltered to set
     */
    public void setStreetListFiltered(List<MailingStreet> streetListFiltered) {
        this.streetListFiltered = streetListFiltered;
    }

    /**
     * @param mailingAddressListFiltered the mailingAddressListFiltered to set
     */
    public void setMailingAddressListFiltered(List<MailingAddress> mailingAddressListFiltered) {
        this.mailingAddressListFiltered = mailingAddressListFiltered;
    }

    /**
     * @param cityStateZipListFiltered the cityStateZipListFiltered to set
     */
    public void setCityStateZipListFiltered(List<MailingCityStateZip> cityStateZipListFiltered) {
        this.cityStateZipListFiltered = cityStateZipListFiltered;
    }

    /**
     * @return the formAddressVerified
     */
    public boolean isFormAddressVerified() {
        return formAddressVerified;
    }

    /**
     * @param formAddressVerified the formAddressVerified to set
     */
    public void setFormAddressVerified(boolean formAddressVerified) {
        this.formAddressVerified = formAddressVerified;
    }

    /**
     * @return the formNotesAddress
     */
    public String getFormNotesAddress() {
        return formNotesAddress;
    }

    /**
     * @return the formNotesStreet
     */
    public String getFormNotesStreet() {
        return formNotesStreet;
    }

    /**
     * @param formNotesAddress the formNotesAddress to set
     */
    public void setFormNotesAddress(String formNotesAddress) {
        this.formNotesAddress = formNotesAddress;
    }

    /**
     * @param formNotesStreet the formNotesStreet to set
     */
    public void setFormNotesStreet(String formNotesStreet) {
        this.formNotesStreet = formNotesStreet;
    }

    /**
     * @return the mailingAddressLinkRoleCandidateList
     */
    public List<LinkedObjectRole> getMailingAddressLinkRoleCandidateList() {
        return mailingAddressLinkRoleCandidateList;
    }

    /**
     * @param mailingAddressLinkRoleCandidateList the
     * mailingAddressLinkRoleCandidateList to set
     */
    public void setMailingAddressLinkRoleCandidateList(List<LinkedObjectRole> mailingAddressLinkRoleCandidateList) {
        this.mailingAddressLinkRoleCandidateList = mailingAddressLinkRoleCandidateList;
    }

    /**
     * @return the currentMailingAddressLink
     */
    public MailingAddressLink getCurrentMailingAddressLink() {
        return currentMailingAddressLink;
    }

    /**
     * @return the currentAddressListHolder
     */
    public IFace_addressListHolder getCurrentAddressListHolder() {
        return currentAddressListHolder;
    }

    /**
     * @param currentMailingAddressLink the currentMailingAddressLink to set
     */
    public void setCurrentMailingAddressLink(MailingAddressLink currentMailingAddressLink) {
        this.currentMailingAddressLink = currentMailingAddressLink;
    }

    /**
     * @param currentAddressListHolder the currentAddressListHolder to set
     */
    public void setCurrentAddressListHolder(IFace_addressListHolder currentAddressListHolder) {
        this.currentAddressListHolder = currentAddressListHolder;
    }

    /**
     * @return the editModeCurrentAddressLink
     */
    public boolean isEditModeCurrentAddressLink() {
        return editModeCurrentAddressLink;
    }

    /**
     * @param editModeCurrentAddressLink the editModeCurrentAddressLink to set
     */
    public void setEditModeCurrentAddressLink(boolean editModeCurrentAddressLink) {
        this.editModeCurrentAddressLink = editModeCurrentAddressLink;
    }

    /**
     * @return the addressListHolderComponentForUpdatePostMADLinkOperation
     */
    public String getAddressListHolderComponentForUpdatePostMADLinkOperation() {
        return addressListHolderComponentForUpdatePostMADLinkOperation;
    }

    /**
     * @param addressListHolderComponentForUpdatePostMADLinkOperation the
     * addressListHolderComponentForUpdatePostMADLinkOperation to set
     */
    public void setAddressListHolderComponentForUpdatePostMADLinkOperation(String addressListHolderComponentForUpdatePostMADLinkOperation) {
        this.addressListHolderComponentForUpdatePostMADLinkOperation = addressListHolderComponentForUpdatePostMADLinkOperation;
    }

    /**
     * @return the formNotesAddressLink
     */
    public String getFormNotesAddressLink() {
        return formNotesAddressLink;
    }

    /**
     * @param formNotesAddressLink the formNotesAddressLink to set
     */
    public void setFormNotesAddressLink(String formNotesAddressLink) {
        this.formNotesAddressLink = formNotesAddressLink;
    }

    /**
     * @return the formBuildingFilter
     */
    public String getFormBuildingFilter() {
        return formBuildingFilter;
    }

    /**
     * @param formBuildingFilter the formBuildingFilter to set
     */
    public void setFormBuildingFilter(String formBuildingFilter) {
        this.formBuildingFilter = formBuildingFilter;
    }

    /**
     * @return the formStreetFilter
     */
    public String getFormStreetFilter() {
        return formStreetFilter;
    }

    /**
     * @param formStreetFilter the formStreetFilter to set
     */
    public void setFormStreetFilter(String formStreetFilter) {
        this.formStreetFilter = formStreetFilter;
    }

    /**
     * @return the formZIPFilter
     */
    public String getFormZIPFilter() {
        return formZIPFilter;
    }

    /**
     * @param formZIPFilter the formZIPFilter to set
     */
    public void setFormZIPFilter(String formZIPFilter) {
        this.formZIPFilter = formZIPFilter;
    }

    /**
     * @return the mailingAddressListFilterSearchResults
     */
    public List<MailingAddress> getMailingAddressListFilterSearchResults() {
        return mailingAddressListFilterSearchResults;
    }

    /**
     * @param mailingAddressListFilterSearchResults the
     * mailingAddressListFilterSearchResults to set
     */
    public void setMailingAddressListFilterSearchResults(List<MailingAddress> mailingAddressListFilterSearchResults) {
        this.mailingAddressListFilterSearchResults = mailingAddressListFilterSearchResults;
    }

    /**
     * @return the currentMailingAddressRecursiveLinkList
     */
    public List<MailingAddressLinkParentObjectInfoHeavy> getCurrentMailingAddressRecursiveLinkList() {
        return currentMailingAddressRecursiveLinkList;
    }

    /**
     * @param currentMailingAddressRecursiveLinkList the
     * currentMailingAddressRecursiveLinkList to set
     */
    public void setCurrentMailingAddressRecursiveLinkList(List<MailingAddressLinkParentObjectInfoHeavy> currentMailingAddressRecursiveLinkList) {
        this.currentMailingAddressRecursiveLinkList = currentMailingAddressRecursiveLinkList;
    }

    /**
     * @return the addressQuickAddDisableFields
     */
    public boolean isAddressQuickAddDisableFields() {
        return addressQuickAddDisableFields;
    }

    /**
     * @param addressQuickAddDisableFields the addressQuickAddDisableFields to
     * set
     */
    public void setAddressQuickAddDisableFields(boolean addressQuickAddDisableFields) {
        this.addressQuickAddDisableFields = addressQuickAddDisableFields;
    }

    /**
     * @return the addressQuickAddBundle
     */
    public MailingAddressQuickAddBundle getAddressQuickAddBundle() {
        return addressQuickAddBundle;
    }

    /**
     * @param addressQuickAddBundle the addressQuickAddBundle to set
     */
    public void setAddressQuickAddBundle(MailingAddressQuickAddBundle addressQuickAddBundle) {
        this.addressQuickAddBundle = addressQuickAddBundle;
    }

    /**
     * @return the permissionAllowCurrentStreetDeactivate
     */
    public boolean isPermissionAllowCurrentStreetDeactivate() {
        return permissionAllowCurrentStreetDeactivate;
    }

    /**
     * @param permissionAllowCurrentStreetDeactivate the
     * permissionAllowCurrentStreetDeactivate to set
     */
    public void setPermissionAllowCurrentStreetDeactivate(boolean permissionAllowCurrentStreetDeactivate) {
        this.permissionAllowCurrentStreetDeactivate = permissionAllowCurrentStreetDeactivate;
    }

    /**
     * @return the permissionAllowCurrentMailingAddressDeactivation
     */
    public boolean isPermissionAllowCurrentMailingAddressDeactivation() {
        return permissionAllowCurrentMailingAddressDeactivation;
    }

    /**
     * @param permissionAllowCurrentMailingAddressDeactivation the
     * permissionAllowCurrentMailingAddressDeactivation to set
     */
    public void setPermissionAllowCurrentMailingAddressDeactivation(boolean permissionAllowCurrentMailingAddressDeactivation) {
        this.permissionAllowCurrentMailingAddressDeactivation = permissionAllowCurrentMailingAddressDeactivation;
    }

    /**
     * @return the mailingStreetMergeRequest
     */
    public BobMergeRequest<MailingStreet> getMailingStreetMergeRequest() {
        return mailingStreetMergeRequest;
    }

    /**
     * @param mailingStreetMergeRequest the mailingStreetMergeRequest to set
     */
    public void setMailingStreetMergeRequest(BobMergeRequest<MailingStreet> mailingStreetMergeRequest) {
        this.mailingStreetMergeRequest = mailingStreetMergeRequest;
    }

    /**
     * @return the mailingAddressMergeRequest
     */
    public BobMergeRequest<MailingAddress> getMailingAddressMergeRequest() {
        return mailingAddressMergeRequest;
    }

    /**
     * @param mailingAddressMergeRequest the mailingAddressMergeRequest to set
     */
    public void setMailingAddressMergeRequest(BobMergeRequest<MailingAddress> mailingAddressMergeRequest) {
        this.mailingAddressMergeRequest = mailingAddressMergeRequest;
    }

    /**
     * @return the selectedStreetList
     */
    public List<MailingStreet> getSelectedStreetList() {
        return selectedStreetList;
    }

    /**
     * @param selectedStreetList the selectedStreetList to set
     */
    public void setSelectedStreetList(List<MailingStreet> selectedStreetList) {
        this.selectedStreetList = selectedStreetList;
    }

    /**
     * @return the selectedAddressList
     */
    public List<MailingAddress> getSelectedAddressList() {
        return selectedAddressList;
    }

    /**
     * @param selectedAddressList the selectedAddressList to set
     */
    public void setSelectedAddressList(List<MailingAddress> selectedAddressList) {
        this.selectedAddressList = selectedAddressList;
    }

    /**
     * @return the mailingAddressMergeSelectMode
     */
    public boolean isMailingAddressMergeSelectMode() {
        return mailingAddressMergeSelectMode;
    }

    /**
     * @param mailingAddressMergeSelectMode the mailingAddressMergeSelectMode to
     * set
     */
    public void setMailingAddressMergeSelectMode(boolean mailingAddressMergeSelectMode) {
        this.mailingAddressMergeSelectMode = mailingAddressMergeSelectMode;
    }

    /**
     * @return the mailingStreetMergeSelectMode
     */
    public boolean isMailingStreetMergeSelectMode() {
        return mailingStreetMergeSelectMode;
    }

    /**
     * @param mailingStreetMergeSelectMode the mailingStreetMergeSelectMode to
     * set
     */
    public void setMailingStreetMergeSelectMode(boolean mailingStreetMergeSelectMode) {
        this.mailingStreetMergeSelectMode = mailingStreetMergeSelectMode;
    }

    /**
     * @return the currentMergeDomain
     */
    public MergeDomainEnum getCurrentMergeDomain() {
        return currentMergeDomain;
    }

    /**
     * @param currentMergeDomain the currentMergeDomain to set
     */
    public void setCurrentMergeDomain(MergeDomainEnum currentMergeDomain) {
        this.currentMergeDomain = currentMergeDomain;
    }

    /**
     * @return the currentMergeRequest
     */
    public BobMergeRequest getCurrentMergeRequest() {
        return currentMergeRequest;
    }

    /**
     * @param currentMergeRequest the currentMergeRequest to set
     */
    public void setCurrentMergeRequest(BobMergeRequest currentMergeRequest) {
        this.currentMergeRequest = currentMergeRequest;
    }

    /**
     * @return the selectedObjectsToMerge
     */
    public List<IFace_Mergable> getSelectedObjectsToMerge() {
        return selectedObjectsToMerge;
    }

    /**
     * @param selectedObjectsToMerge the selectedObjectsToMerge to set
     */
    public void setSelectedObjectsToMerge(List<IFace_Mergable> selectedObjectsToMerge) {
        this.selectedObjectsToMerge = selectedObjectsToMerge;
    }
}
