/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.coordinators.PersonCoordinator;
import com.tcvcog.tcvce.coordinators.PropertyCoordinator;
import com.tcvcog.tcvce.coordinators.SearchCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.DatabaseFetchRuntimeException;
import com.tcvcog.tcvce.domain.EventException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.domain.SearchException;
import com.tcvcog.tcvce.entities.BOBSource;
import com.tcvcog.tcvce.entities.ContactEmail;
import com.tcvcog.tcvce.entities.ContactPhone;
import com.tcvcog.tcvce.entities.ContactPhoneType;
import com.tcvcog.tcvce.entities.Human;
import com.tcvcog.tcvce.entities.HumanAlias;
import com.tcvcog.tcvce.entities.HumanAliasRoleEnum;
import com.tcvcog.tcvce.entities.HumanLink;
import com.tcvcog.tcvce.entities.HumanLinkOutoingThisObjectsContainerIsSource;
import com.tcvcog.tcvce.entities.HumanLinkParentObjectInfoHeavy;
import com.tcvcog.tcvce.entities.IFace_humanListHolder;
import com.tcvcog.tcvce.entities.IFace_noteHolder;
import com.tcvcog.tcvce.entities.LinkedObjectRole;
import com.tcvcog.tcvce.entities.LinkedObjectSchemaEnum;
import com.tcvcog.tcvce.entities.MailingAddress;
import com.tcvcog.tcvce.entities.MailingAddressLink;
import com.tcvcog.tcvce.entities.MergeMatchLevelEnum;
import com.tcvcog.tcvce.entities.Person;
import com.tcvcog.tcvce.entities.PersonLinkHeavy;
import com.tcvcog.tcvce.entities.PersonMergeLog;
import com.tcvcog.tcvce.entities.PersonMergeRequest;
import com.tcvcog.tcvce.entities.PropertyUnit;
import com.tcvcog.tcvce.entities.PropertyUnitDataHeavy;
import com.tcvcog.tcvce.entities.search.QueryPerson;
import com.tcvcog.tcvce.entities.search.QueryPersonEnum;
import com.tcvcog.tcvce.entities.search.SearchParamsPerson;
import com.tcvcog.tcvce.occupancy.application.OccPeriodBB;
import com.tcvcog.tcvce.util.Constants;
import com.tcvcog.tcvce.util.MessageBuilderParams;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.faces.event.ActionEvent;
import java.util.Arrays;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The inaugural humanized PersonBB for all basic person stuff Backing the
 * personTools.xhtml which gets embedded in the nav container
 *
 * @author sylvia
 */
public class PersonBB extends BackingBeanUtils {

    private PersonLinkHeavy currentPerson;
    private boolean currentPersonHumanFieldsEditMode;
    private boolean currentHumanLinkEditMode;

    // Alias stuff
    private HumanAlias currentAlias;
    private boolean editModeAlias;
    private List<HumanAliasRoleEnum> aliasRoleCandidateList; 
    
    private IFace_noteHolder currentNoteHolder;
    private String formNewNoteUniversal;
    private String pageComponentToUpdateAfterNoteCommit;

    private IFace_humanListHolder currentHumanListHolder;
    private IFace_humanListHolder currentHumanLinkPoolFeeder;
    private List<HumanLinkParentObjectInfoHeavy> humanLinkParentInfoHeavyList;
    private List<IFace_humanListHolder> upstreamPersonPoolList;
    private List<LinkedObjectRole> linkRoleCandidateList;
    private LinkedObjectRole selecetedLinkedObjetRole;
    private PropertyUnit selectedUnitForHumanLinkTarget;
    private PropertyUnitDataHeavy selectedUnitForHumanLinkTargetDataHeavy;

    private PersonLinkHeavy currentPersonLinkHeavy;
    private HumanLink currentHumanLink;
    private String formHumanLinkNotes;

    private MailingAddress currentMailingAddress;
    private boolean currentMailingAddressEditMode;

    // PHONE STUFF
    private ContactPhone currentContactPhone;
    private boolean currentContactPhoneEditMode;
    private List<ContactPhoneType> phoneTypeList;
    private boolean currentContactPhoneDisconnected;

    // EMAIL STUFF
    private ContactEmail currentContactEmail;
    private boolean currentContactEmailEditMode;

    private List<BOBSource> sourceList;

    // ********************************************************
    // *************** MIGRATED SEARCH STUFF ******************
    // ********************************************************
    private QueryPerson querySelected;
    private List<QueryPerson> queryList;
    private SearchParamsPerson paramsSelected;
    private String queryLog;

    private List<Person> personList;
    private List<Person> filteredPersonList;
    private boolean appendResultsToList;

    private List<HumanLink> humanLinkListQueued;
    private String personListComponentIDToUpdatePostLinkingOperation;

    // ********************************************************
    // *************** QUICK SEARCH STUFF *********************
    // ********************************************************
    private QueryPerson queryQuickSearch;
    private List<Person> personQuickSearchResultList;
    
    // ********************************************************
    // *************** Merging shit *********************
    // ********************************************************

    private PersonMergeRequest mergeRequest;
    private PersonMergeLog currentMergeLog;
    private PersonMergeLog currentMergeLogForViewingOnly;
    
    private List<MergeMatchLevelEnum> matchLevelCandidates;
    
    // ********************************************************
    // *********** Occ Certificate Quick Add Junk *************
    // ********************************************************
    private String certPersonParentStringSelected;
    private LinkedObjectRole certPersonParentSelectedLORSelected;
    private boolean personQuickAddIncludePhone;
    private boolean personQuickAddIncludeEmail;

    /**
     * Creates a new instance of PersonBB
     */
    public PersonBB() {
    }

    @PostConstruct
    public void initBean() {
        SystemCoordinator sc = getSystemCoordinator();
        PersonCoordinator pc = getPersonCoordinator();

        injectPersonAsCurrentPersonLinkHeavy(getSessionBean().getSessPerson());
        System.out.println("PersonBB.initBean()");
        currentPersonHumanFieldsEditMode = false;
        currentContactPhoneEditMode = false;
        currentContactEmailEditMode = false;

        try {
            loadLinkedObjectRoleList();
            phoneTypeList = pc.getContactPhoneTypeList();
            sourceList = sc.getBobSourceListComplete();
            currentHumanListHolder = getSessionBean().getSessProperty();
            loadLinkedObjectRoleList();
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }
        setupPersonQuery();
        initResetPersonQueryQuick();
        humanLinkListQueued = new ArrayList<>();
        aliasRoleCandidateList = Arrays.asList(HumanAliasRoleEnum.values());

    }

    /**
     * Utility method for loading link roles based on session list holder
     *
     * @throws IntegrationException
     * @throws BObStatusException
     */
    private void loadLinkedObjectRoleList() throws IntegrationException, BObStatusException {
        SystemCoordinator sc = getSystemCoordinator();
        if (currentHumanListHolder == null) {
            currentHumanListHolder = getSessionBean().getSessHumanListHolder();
        }
        if (currentHumanListHolder != null) {
            linkRoleCandidateList = sc.assembleLinkedObjectRolesBySchema(currentHumanListHolder.getHUMAN_LINK_SCHEMA_ENUM());
            if (linkRoleCandidateList != null) {
                System.out.println("PersonBB.loadLinkedObjectRoleListUsingSessionHLH | roleListSize = " + linkRoleCandidateList.size());
            }
        }

    }
    
    /**
     * Our current person is a subclass PersonLInkHeavy so this method
     * will take the base class and get the link heavy version
     * and make it our CurrentPerson.
     * 
     * I'll refresh this person too
     * 
     * I can also cope with a person skeleton meaning one without a DB ID
     * that is in creation process
     * 
     * @param p 
     */
    private void injectPersonAsCurrentPersonLinkHeavy(Person p){
        PersonCoordinator pc = getPersonCoordinator();
        if(p != null){
            try {
                currentPerson = pc.assemblePersonLinkHeavy(pc.getPersonByHumanID(p.getHumanID()));
            } catch (IntegrationException | BObStatusException  ex) {
                System.out.println(ex);
            } 
            
        }
    }

    /**
     * *******************************************************
     */
    /**
     * ************ SEARCH ORGANS - MIGRATED!! ***************
     */
    /**
     * *******************************************************
     */
    private void setupPersonQuery() {
        SearchCoordinator sc = getSearchCoordinator();
        personList = new ArrayList<>();
        appendResultsToList = false;
        filteredPersonList = new ArrayList<>();
        try {
            queryList = sc.buildQueryPersonList(getSessionBean().getSessUser().getMyCredential());
        } catch (IntegrationException ex) {
            System.out.println(ex);
        }

        if (queryList != null && !queryList.isEmpty()) {
            querySelected = getQueryList().get(0);
            if (querySelected != null) {
                System.out.println("PersonBB.setupPersonQuery | query selected: " + querySelected.getQueryTitle());
            }
        }

        // a hacky test to see if we can get a name automatically populated in the query param
        if (querySelected != null && querySelected.getPrimaryParams() != null) {
            paramsSelected = getQuerySelected().getPrimaryParams();
            String sessionPersonName = getSessionBean().getPersonNameForSearch();
            if (sessionPersonName != null && !sessionPersonName.equals(Constants.EMPTY_STRING)) {
                paramsSelected.setName_val(sessionPersonName);
                getSessionBean().setPersonNameForSearch(null);
            }
        }
    }

    /**
     * Hacky attempt at a trigger for dialog update from somewhere in the system
     *
     * @return
     */
    public String getSpecialTriggerElement() {
        System.out.println("PersonBB.getSpecialTriggerElement");
        setupPersonQuery();
        return "";
    }

    /**
     * Listener for user clicks of the "search" link for persons
     *
     * @param ev
     */
    public void onPersonSearchInitLinkClick(ActionEvent ev) {
        getSessionBean().setSessHumanListHolder(getSessionBean().getSessProperty());
        System.out.println("Search for Persons INIT");
    }

    /**
     * Listener for user requests to clear the person list
     *
     * @param ev
     */
    public void clearResultList(ActionEvent ev) {
        getPersonList().clear();

    }

    /**
     * Listener method for changes to the query drop down box
     */
    public void changeQuerySelected() {

        if (getQuerySelected() != null && getQuerySelected().getPrimaryParams() != null) {
            setParamsSelected(getQuerySelected().getPrimaryParams());
        }

        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "New query loaded!", ""));

    }

    /**
     * Listener for user request to execute the query!
     *
     * @param event
     */
    public void executeQuery(ActionEvent event) {
        PersonCoordinator pc = getPersonCoordinator();
        SearchCoordinator sc = getSearchCoordinator();
        try {
            List<Person> pl = pc.assemblePersonListFromHumanList(sc.runQuery(getQuerySelected()).getBOBResultList());

            if (!appendResultsToList && personList != null) {
                personList.clear();
            }
            if (personList != null) {
                personList.addAll(pl);
            }

            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Your search completed with " + pl.size() + " results", ""));
            System.out.println("PersonBB.executeQuery: results " + pl.size());

        } catch (SearchException | IntegrationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to complete search! ", ""));
        }
    }

    /**
     * Sets up / refreshes quick query
     */
    public void initResetPersonQueryQuick() {
        SearchCoordinator sc = getSearchCoordinator();
        queryQuickSearch = sc.initQuery(QueryPersonEnum.PERSON_NAME, getSessionBean().getSessUser().getKeyCard());
    }

    /**
     * Listener for user request to execute the query!
     *
     * @param event
     */
    public void executeQueryQuick(ActionEvent event) {
        PersonCoordinator pc = getPersonCoordinator();
        SearchCoordinator sc = getSearchCoordinator();
        try {
            System.out.println("PersonBB.executeQueryQuick");
            auditPersonQuickQuery();
            List<Person> pl = pc.assemblePersonListFromHumanList(sc.runQuery(queryQuickSearch).getBOBResultList());
            personQuickSearchResultList = new ArrayList<>();
            personQuickSearchResultList.addAll(pl);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Your search completed with " + pl.size() + " results", ""));
            System.out.println("PersonBB.executeQuickQuery: results " + pl.size());
            initResetPersonQueryQuick();
        } catch (SearchException | IntegrationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to complete search! ", ""));
        }
    }

    /**
     * Throws an error for empty name queries
     *
     * @throws BObStatusException
     */
    private void auditPersonQuickQuery() throws BObStatusException {
        if (queryQuickSearch != null && queryQuickSearch.getPrimaryParams() != null) {
            if (queryQuickSearch.getPrimaryParams().getName_val() == null || queryQuickSearch.getPrimaryParams().getName_val().length() < 1) {
                throw new BObStatusException("Your person search must contain at least one character!");
            }
        }
    }

    /**
     * Clears quick result list by setting a new list
     *
     * @param ev
     */
    public void onClearPersonQueryQuickResultList(ActionEvent ev) {
        System.out.println("PersonBB.onClearPersonQueryQuickResultList");
        personQuickSearchResultList = new ArrayList<>();
    }

    /**
     * listener for user requests to clear the current query
     *
     * @param ev
     */
    public void resetCurrentQuery(ActionEvent ev) {
        setupPersonQuery();
        System.out.println("PersonSearchBB.resetCurrentQuery ");
//        querySelected = sc.initQuery(QueryPersonEnum.valueOf(querySelected.getQueryName().toString()),
//                getSessionBean().getSessUser().getMyCredential());

        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Query list rebuilt; filters reset", ""));

        setQueryLog(null);

    }

    /**
     * Responds to the user wanting to view a person from the search result list
     *
     * @param p
     */
    public void explorePerson(Person p) {
        PersonCoordinator pc = getPersonCoordinator();
        Person per;
        try {
            if (Objects.nonNull(p.getHumanID())) {
                per = pc.getPerson(pc.getHuman(p.getHumanID()));
                getSessionBean().setSessPerson(per);
                injectPersonAsCurrentPersonLinkHeavy(p);
            }
            onLoadHumanLinksToCurrentPerson(null);
            currentPersonHumanFieldsEditMode = false;
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Person exploring is broken", ""));
        }
    }

    /**
     * Adaptor method for calling explorePerson with a human
     *
     * @param h
     */
    public void exploreHuman(Human h) {
        PersonCoordinator pc = getPersonCoordinator();
        Person p = null;
        try {
            p = pc.getPerson(h);
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }
        if (p != null) {
            explorePerson(p);
        }

    }

    /**
     * *******************************************************
     */
    /**
     * ************ REFRESHING  ******************************
     */
    /**
     * *******************************************************
     */
    /**
     * Listener for user wanting to view the current person
     *
     * @param ev
     */
    public void onViewCurrentPersonLinkClick(ActionEvent ev) {
        refreshCurrentPersonAndUpdateSessionPerson();

    }
    
    /**
     * Listener for user requests to view the person to which the current person
     * is linking (i.e. view from link target)
     * 
     * @param outgoing 
     */
    public void onViewPersonThroughOutgoingLink(HumanLinkOutoingThisObjectsContainerIsSource outgoing){
        PersonCoordinator pc = getPersonCoordinator();
        if(outgoing != null){
            try {
                injectPersonAsCurrentPersonLinkHeavy(pc.getPersonByHumanID(outgoing.getTargetHumanID()));
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
            }
        }
    }

    /**
     * Adaptor method for local calls to refresh without a null ActionEvent
     */
    private void refreshCurrentPersonAndUpdateSessionPerson(){
        refreshCurrentPersonAndUpdateSessionPerson(null);
    }
    
    
    /**
     * Refreshes the current person
     * @param ev
     */
    public void refreshCurrentPersonAndUpdateSessionPerson(ActionEvent ev) {
        PersonCoordinator pc = getPersonCoordinator();
        try {
            if (currentPerson != null) {
                Person p = pc.getPersonByHumanID(currentPerson.getHumanID());
                injectPersonAsCurrentPersonLinkHeavy(p);
                onLoadHumanLinksToCurrentPerson(null);
                getSessionBean().setSessPerson(currentPerson);
            } else {
                throw new BObStatusException("PersonBB.refreshCurrentPersonAndUpdateSessionPerson | null currentPerson");
            }
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Could not refresh current Person", ""));
        } 
    }

    /**
     * *******************************************************
     */
    /**
     * ************ HUMANS INFRASTRUCTURE ********************
     */
    /**
     * *******************************************************
     */
    /**
     * Listener for user requests to view a human from within a list of links
     *
     * @param h
     */
    public void onHumanViewLinkClick(Human h) {
        System.out.println("PersonBB.onHumanViewLinkClick : " + h.getName());
        PersonCoordinator pc = getPersonCoordinator();
        try {
            injectPersonAsCurrentPersonLinkHeavy(pc.getPerson(h));
            extractHumanLinkListComponentIDFromHTTPRequest();
            refreshCurrentPersonAndUpdateSessionPerson();
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }
    }

    /**
     * Listener for user requests to view a human from within a list of links
     * but also takes in the human list holder for refreshing after a plain old
     * update of the underlying human, not the link itself
     *
     * @param h
     * @param hlh
     */
    public void onHumanViewLinkClick(Human h, IFace_humanListHolder hlh) {
        if (hlh != null) {
            System.out.println("PersonBB.onHumanViewLinkClick : " + h.getName() + " human link holder id " + hlh.getHostPK());
            currentHumanListHolder = hlh;
        }
        PersonCoordinator pc = getPersonCoordinator();
        try {
            injectPersonAsCurrentPersonLinkHeavy(pc.getPerson(h));
            extractHumanLinkListComponentIDFromHTTPRequest();
            refreshCurrentPersonAndUpdateSessionPerson();
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }
    }

    /**
     * Listener for user clicks of the human edit button
     *
     * @param ev
     */
    public void toggleHumanEditMode(ActionEvent ev) {
        System.out.println("PersonBB.toggleHumanEditMode: toggle val: " + currentPersonHumanFieldsEditMode);
        if (currentPersonHumanFieldsEditMode) {
            if (currentPerson != null && currentPerson.getHumanID() == 0) {
                // we've got a new record, so commit our add to DB
                onPersonAddCommit();
                System.out.println("PersonBB.toggleHumanEditMode: new person; committed");
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Successfully added new person record: " + currentPerson.getHumanID(), ""));
            } else {
                // we've got an existing human, so commit updates
                onHumanEditCommitButtonChange(null);
                System.out.println("PersonBB.toggleHumanEditMode: person edit done");
            }
            refreshCurrentPersonAndUpdateSessionPerson();
        } else {
            onNoteAppendUniversalInitLinkClick(currentPerson);
            prepareNotes(currentNoteHolder);
        }
        currentPersonHumanFieldsEditMode = !currentPersonHumanFieldsEditMode;
    }

    /**
     * Listener for user clicks of the human link edit button
     *
     * @param ev
     */
    public void toggleHumanLinkEditMode(ActionEvent ev) {
        System.out.println("PersonBB.toggleHumanLinkEditMode: toggle val: " + currentHumanLinkEditMode);
        if (currentHumanLinkEditMode) {

            onHumanLinkEditCommit();
            System.out.println("PersonBB.toggleHumanLinkEditMode: person link edit done");
            refreshCurrentPersonAndUpdateSessionPerson();
        }
        currentHumanLinkEditMode = !currentHumanLinkEditMode;
    }

    /**
     *
     * @param ev
     */
    private void onHumanLinkEditCommit() {
        PersonCoordinator pc = getPersonCoordinator();
        try {
            pc.updateHumanLink(currentHumanListHolder, currentHumanLink, getSessionBean().getSessUser());
            refreshCurrentHumanLink();
            injectSessionHumanLinkListForComponentRefresh();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Successfully updated human link ID: " + currentHumanLink.getLinkID(), ""));
        } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error updating human link", ""));
        }
    }

    /**
     * Refreshes current human link (how could you have guessed?)
     */
    private void refreshCurrentHumanLink() {
        PersonCoordinator pc = getPersonCoordinator();
        if (currentHumanLink != null) {
            try {
                currentHumanLink = pc.getHumanLink(currentHumanLink.getLinkID(), currentHumanListHolder.getHUMAN_LINK_SCHEMA_ENUM());
                if (currentHumanLink != null) {
                    System.out.println("PersonBB.refreshCurrentHumanLink | Refreshed human link: " + currentHumanLink.getLinkID());
                } else {
                    throw new IntegrationException("PersonBB.refreshCurrentHumanLink: NULL human linka fter refresh of human link;");
                }
            } catch (BObStatusException | IntegrationException ex) {
                System.out.println(ex);
            }
        }
    }

    /**
     * Listener for user requests to start the humanlink view process
     *
     * @param hl
     * @param hlh
     */
    public void onHumanLinkEditInitButtonChange(HumanLink hl, IFace_humanListHolder hlh) {
        currentHumanLink = hl;
        if (hlh != null) {
            currentHumanListHolder = hlh;
            currentNoteHolder = hl;

        }
        extractHumanLinkListComponentIDFromHTTPRequest();
        try {
            loadLinkedObjectRoleList();

        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error preparing human link view edit operation", ""));
        }

    }

    /**
     * Listener for user requests to cease editing a human link
     *
     * @param ev
     */
    public void onHumanLinkEditOpertationAbortButtonChange(ActionEvent ev) {
        currentHumanLinkEditMode = false;
    }

    /**
     * Listener for user requests to add a human
     *
     * @param ev
     */
    public void onPersonCreateInitButtonChange(ActionEvent ev) {
        PersonCoordinator pc = getPersonCoordinator();
        currentPerson = pc.createPersonSkeleton(getSessionBean().getSessMuni());
        currentPersonHumanFieldsEditMode = true;
        System.out.println("PersonBB.onHumanAddInitButtonChange");
    }

    /**
     * Listener for user requests to finalize human changes
     *
     * @param ev
     */
    private void onPersonAddCommit() {
        PersonCoordinator pc = getPersonCoordinator();
        try {
            injectPersonAsCurrentPersonLinkHeavy(pc.insertHuman(currentPerson, getSessionBean().getSessUser()));
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Succesfully added person to database!", ""));
        } catch (IntegrationException | BObStatusException | AuthorizationException  ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "FATAL ERROR CODE P001: Could not add new Person to the database!", ""));
            getFacesContext().validationFailed();
        }
    }

    /**
     * Listener for user requests to start the human edit process
     *
     * @param h
     */
    public void onHumanEditInitButtonChange(Human h) {
        PersonCoordinator pc = getPersonCoordinator();
        try {
            injectPersonAsCurrentPersonLinkHeavy(pc.getPerson(h));
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }
    }

    /**
     * Listener for user requests to commit the human edit process
     *
     * @param ev
     */
    public void onHumanEditCommitButtonChange(ActionEvent ev) {

        PersonCoordinator pc = getPersonCoordinator();
        try {
            pc.updateHuman(currentPerson, getSessionBean().getSessUser());
            commitNotes();
            refreshCurrentPersonAndUpdateSessionPerson();
            injectSessionHumanLinkListForComponentRefresh();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Succesfully updated " + currentPerson.getName() + ", id " + currentPerson.getHumanID(), ""));
        } catch (IntegrationException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "ERROR updating " + currentPerson.getName() + ", id " + currentPerson.getHumanID(), ""));

        }

    }

    /**
     * Listener for user requests to abort a human add or edit operation
     *
     * @param ev
     */
    public void onHumanOperationAbortButtonChange(ActionEvent ev) {
        System.out.println("PersonBB.onHumanOperationAbortButtonChange");
        currentPersonHumanFieldsEditMode = false;
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Abort successful; no changes made", ""));
    }
    
    /**
     * listener to start the deac process and check permissions
     * @param ev 
     */
    public void onHumanDeacInit(ActionEvent ev){
        System.out.println("PersonBB.onHumanDeacInit");
        
        
    }
    
    /**
     * Listener to complete the massive deac process
     * @param ev 
     */
    public void onHumanDeacCommit(ActionEvent ev){
        PersonCoordinator pc = getPersonCoordinator();
        try {
            pc.deativateHuman(currentPerson, getSessionBean().getSessUser());
            refreshCurrentPersonAndUpdateSessionPerson();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Person sucessfully deactivatd", ""));
            
        } catch (BObStatusException | AuthorizationException | IntegrationException  ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        } 
        
        System.out.println("PersonBB.onHumanDeacCommit");
    }
    
    /**
     * Aborts the deac process
     * @param ev 
     */
    public void onHumanDeacAbort(ActionEvent ev){
        System.out.println("PersonBB.onHumanDeacAbort");
    }
    
    
    /**
     * Listener to start the quick link process for a certificate 
     * from an existing human from a search result 
     * @param p
     */
    public void onPersonQuickLinkInit(Person p){
        System.out.println("PersonBB.onPersonQuickLinkInit");
        injectPersonAsCurrentPersonLinkHeavy(p);
        getSessionBean().setSessPerson(currentPerson);
        getSessionBean().setCertPersonQuickAddNewPersonPathway(false);
        onPersonQuickAddDefaultParentConfig();
    }

    /**
     * Prepares person quick add form with default values
     */
    private void onPersonQuickAddDefaultParentConfig(){
        // choose a default parent
        certPersonParentStringSelected = "PERS_PARENT_PROP";
        onPersonQuickAddParentSelectChange();
    }
    
    /**
     * Listener for user requests to start the process of adding a person in
     * quick add mode which allows for adding a name, phone, and email in one
     * single form, with a single click.This feature was added in early Feb 2024 for use in the upgraded
     * dynamically saved certificate flow and involves quite a few back flips to
     * integrate into our atomized, single object flow used during traditional
     * person adds which involves writing the person, then separately the phone
     * and email
     *
     * @param ev
     */
    public void onPersonQuickAddInit(ActionEvent ev) {
        getSessionBean().setCertPersonQuickAddNewPersonPathway(true);
        // call our three inits so we have skeletons
        onPersonCreateInitButtonChange(null);
        onPhoneAddInitButtonChange(null);
        onEmailAddInitButtonChange(null);
        onPersonQuickAddDefaultParentConfig();
        personQuickAddIncludePhone = true;
        personQuickAddIncludeEmail = true;
        
    }
    
    /***
     * listener to stop person quick add process; nothing to do here
     * @param ev 
     */
    public void onPersonQuickAddCancel(ActionEvent ev){
        System.out.println("PersonBB.onPersonQuickAddCancel");
    }
    
    /**
     * Listener for users to switch between new person or new person link
     * candidates; I set the linked object role candidates
     */
    public void onPersonQuickAddParentSelectChange() {
        if (certPersonParentStringSelected != null) {
            System.out.println("PersonBB.onPersonQuickAddParentSelectChange | parent string: " + certPersonParentStringSelected);
            switch (certPersonParentStringSelected) {
                case "PERS_PARENT_PROP" -> {
                    currentHumanListHolder = getSessionBean().getSessProperty();
                }
                case "PERS_PARENT_UNIT" -> {
                    currentHumanListHolder = getSessionBean().getSessPropertyUnit();
                }
                case "PERS_PARENT_OCCPERIOD" -> {
                    currentHumanListHolder = getSessionBean().getSessOccPeriod();
                }
            } // close switch
            // register our chosen human list holder with the session
            if(currentHumanListHolder != null){
                getSessionBean().setSessHumanListHolder(currentHumanListHolder);
            }
            try {
                loadLinkedObjectRoleList();
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex.getMessage());
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                ex.getMessage(), ""));
            }
        } // close iff we have a parent string
    }
    
    /**
     * First listener called at the completion of the person quick add 
     * feature first pioneered on the certificate saved dynamic flow.I'm going to write the new person to the db then connect the phone 
     *   and/or email, then do the parent linking, then turn it over to 
     *   the occ side for permit linking using the session bean as our 
     *   common data relay pool
     * @param ev
     */
    public void onPersonQuickAddCommitButtonChange(ActionEvent ev){
        System.out.println("PersonBB.onPersonQuickAddCommitButtonChange");
        
        if(getSessionBean().isCertPersonQuickAddNewPersonPathway() && currentPerson != null && currentPerson.getName() != null){
            onPersonAddCommit();
            System.out.println("PersonBB.onPersonQuickAddCommitButtonChange | wrote human with fresh ID: " + currentPerson.getHumanID());
        }
        // make sure our human insert worked before any more linking
        if(currentPerson != null && currentPerson.getPersonID() != 0){
            if(getSessionBean().isCertPersonQuickAddNewPersonPathway() && personQuickAddIncludePhone && currentContactPhone != null && currentContactPhone.getPhoneNumber() != null){
                onPhoneAddCommitButtonChange();
                System.out.println("PersonBB.onPersonQuickAddCommitButtonChange | wrote phone with fresh ID: " + currentContactPhone.getPhoneID());
            }
            if(getSessionBean().isCertPersonQuickAddNewPersonPathway() && personQuickAddIncludeEmail && currentContactEmail != null && currentContactEmail.getEmailaddress() != null){
                onEmailAddCommitButtonChange();
                System.out.println("PersonBB.onPersonQuickAddCommitButtonChange | wrote email with fresh ID: " + currentContactEmail.getEmailID());
            }
            if(currentHumanListHolder != null){
                onHumanLinkAddInitButtonChange(null);
                onHumanLinkCreateCommitButtonChange(null);
            }
        }
        
        // now call my listener on occperiod bb
        FacesContext context = FacesContext.getCurrentInstance();
        OccPeriodBB opbb = (OccPeriodBB) context.getApplication().evaluateExpressionGet(context, "#{occPeriodBB}", OccPeriodBB.class);
        opbb.onPersonQuickAddCommitButtonChange();
    }
    
    /**
     * Listener to toggle the edit mode for the current alias
     * @param ev 
     */
    public void onToggleAliasAddEditMode(ActionEvent ev){
        if(currentAlias != null){
            if(currentAlias.getAliasID() == 0){
                onAliasCreateCommit(null);
            } else {
                onAliasUpdateCommit(null);
            }
            editModeAlias = !editModeAlias;
        }
        
    }
    
    /**
     * listener to start the alias process
     * @param ev 
     */
    public void onAliasCreateInit(ActionEvent ev){
        PersonCoordinator pc = getPersonCoordinator();
        try {
            System.out.println("PersonBB.onAliasCreateInit");
            currentAlias = pc.getHumanAliasSkeleton(currentPerson, getSessionBean().getSessUser());
            editModeAlias = true;
        } catch (BObStatusException | IntegrationException  ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                ex.getMessage(), ""));
        }
    }
    
    /**
     * listener for user requests to complete the alias creation process
     * @param ev 
     */
    public void onAliasCreateCommit(ActionEvent ev){
        PersonCoordinator pc = getPersonCoordinator();
        try {
            System.out.println("PersonBB.onAliasCreateCommit");
            int freshID = pc.insertHumanAlias(currentAlias, currentPerson, getSessionBean().getSessUser());
            currentAlias = pc.getHumanAlias(freshID);
            refreshCurrentPersonAndUpdateSessionPerson();
             getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Alias creation: success!", ""));
        } catch (AuthorizationException | BObStatusException | IntegrationException ex) {
            System.out.println(ex); 
             getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                ex.getMessage(), ""));
        }
        
    }
    
    /**
     * listener for user requests to complete the alias creation process
     * @param ev 
     */
    public void onAliasUpdateCommit(ActionEvent ev){
        PersonCoordinator pc = getPersonCoordinator();
        try {
            System.out.println("PersonBB.onAliasCreateCommit");
            pc.updateHumanAlias(currentAlias, getSessionBean().getSessUser());
            currentAlias = pc.getHumanAlias(currentAlias.getAliasID());
            refreshCurrentPersonAndUpdateSessionPerson();
            getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Alias update: success!", ""));
        } catch (AuthorizationException | BObStatusException | IntegrationException ex) {
            System.out.println(ex); 
             getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                ex.getMessage(), ""));
        }
        
    }
    
    /**
     * listener to stop the alias create process
     * @param ev
     */
    public void onAliasAddEditAbort(ActionEvent ev){
        System.out.println("PersonBB.onAliasCreateAbortButton");
        editModeAlias = false;
    }

    /**
     * listener for user requests to view the given Alias
     * @param ha 
     */
    public void onAliasView(HumanAlias ha){
        System.out.println("PersonBB.onAliasView | aliasID: " + ha.getAliasID());
        currentAlias = ha;
    }
    
    /**
     * Listener for user requests to start the deac process of a given alias
     * @param ev
     */
    public void onAliasDeacInit(ActionEvent ev) {
        System.out.println("PersonBB.onAliasDeacInit");
        
    }
    
    /**
     * listener for users to commit the finalization of deac
     * @param ev 
     */
    public void onAliasDeacCommit(ActionEvent ev){
        PersonCoordinator pc = getPersonCoordinator();
        try {
            System.out.println("PersonBB.onAliasDeacCommit");
            pc.deactivateHumanAlias(currentAlias, getSessionBean().getSessUser());
            refreshCurrentPersonAndUpdateSessionPerson();
             getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Alias deactivation: Success!", ""));
        } catch (AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                ex.getMessage(), ""));
        } 
        
        
    }
    

    /**
     * ********************************************************
     * ************ MERGING -> <- INFRASTRUCTURE ***************
     * *******************************************************
     */

    /**
     * Listener for user to select a person for the primary merge person
     * @param primary 
     */
    public void onSelectPersonAsMergePrimary(Person primary){
        System.out.println("PersonBB.onSelectPersonAsMergePrimary | humanID: " + primary.getHumanID());
        PersonCoordinator pc = getPersonCoordinator();
        checkForAndInitPersonMergeRequest();
        try {
            mergeRequest.setPersonPrimary(pc.assemblePersonLinkHeavy(primary));
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        } 
    }
    
    /**
     * Listener for user to select a subordinate merge person
     * @param sub 
     */
    public void onSelectPersonAsMergeSubordinate(Person sub){
        System.out.println("PersonBB.onSelectPersonAsMergeSubordinate | humanID: " + sub.getHumanID());
        PersonCoordinator pc = getPersonCoordinator();
        checkForAndInitPersonMergeRequest();
        try {
            mergeRequest.setPersonSubordinate(pc.assemblePersonLinkHeavy(sub));
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        } 
        
    }
    
    private void checkForAndInitPersonMergeRequest(){
        PersonCoordinator pc = getPersonCoordinator();
        if(mergeRequest == null){
            mergeRequest = pc.getPersonMergeRequestSkeleton();
            mergeRequest.setRequestingUser(getSessionBean().getSessUser());
        }
        matchLevelCandidates = Arrays.asList(MergeMatchLevelEnum.values());
    }
    
    /**
     * Clears the primary person
     * @param ev 
     */
    public void onClearPersonPrimary(ActionEvent ev){
        System.out.println("PersonBB.onClearPersonPrimary");
        if(mergeRequest != null){
            mergeRequest.setPersonPrimary(null);
        }
        
    }
    
    /**
     * Clears the subordinate person
     * @param ev 
     */
    public void onClearPersonSubordinate(ActionEvent ev){
        System.out.println("PersonBB.onClearPersonSubordinate");
        if(mergeRequest != null){
            mergeRequest.setPersonSubordinate(null);
        }
        
        
    }
    
    /**
     * Starts the merge process; requires primary and secondary persons
     * @param ev 
     */
    public void onMergePersonsInit(ActionEvent ev){
        System.out.println("PersonBB.onMergePersonsInit");
       
        PersonCoordinator pc = getPersonCoordinator();
        pc.marshallCheckPersonMergeAuth(mergeRequest);
        currentMergeLog = null;
        
    }
    
    /**
     * Listener to bring up confirm merge dialog
     * @param ev 
     */
    public void onMergePersonCommitInit(ActionEvent ev){
        System.out.println("PersonBB.onMergePersonCommitInit");
    }
    
    /**
     * Sends the MergeRequest to the coordinator
     * @param ev
     */
    public void onMergePersonsCommit(ActionEvent ev){
        System.out.println("PersonBB.onMergePersonsCommit");
        PersonCoordinator pc = getPersonCoordinator();
        try {
            currentMergeLog = pc.mergePersons(mergeRequest, getSessionBean().getSessUser());
            currentPerson = mergeRequest.getPersonPrimary();
            refreshCurrentPersonAndUpdateSessionPerson();
            // clear our former request
//            mergeRequest = null;
        } catch (BObStatusException | AuthorizationException | DatabaseFetchRuntimeException | IntegrationException ex) {
            System.out.println(ex);
        } 
        
    }
    
    /**
     * Aborts the current person merge operation.
     * Wipes current request
     * @param ev 
     */
    public void onMergePersonsAbort(ActionEvent ev){
        System.out.println("PersonBB.onMergePersonsAbort");
        PersonCoordinator pc = getPersonCoordinator();
        mergeRequest = null;
        
    }
    
    /**
     * ends the merging process
     * @param ev 
     */
    public void onMergePersonsDone(ActionEvent ev){
        System.out.println("PersonBB.onMergePersonDone");
        mergeRequest = null;
        currentMergeLog = null;
    }
    
    
    /**
     * Listener to view a single merge log from a completed merge only
     * @param log 
     */
    public void onViewMergeLogCompletedViewOnly(PersonMergeLog log){
        System.out.println("PersonBB.onViewMergeLogCompletedViewOnly | logID: " + log.getMergeID());
        currentMergeLogForViewingOnly = log;
    }
    
    /**
     * ********************************************************
     * ************ Phone number INFRASTRUCTURE ***************
     * *******************************************************
     */
    /**
     * Listener for user clicks of the phone edit button
     */
    public void togglePhoneEditMode() {
        System.out.println("PersonBB.togglePhoneEditMode: toggle mode " + currentContactPhoneEditMode);
        if (currentContactPhoneEditMode) {
            if (currentContactPhone != null && currentContactPhoneDisconnected) {
                currentContactPhone.setDisconnectRecordedBy(getSessionBean().getSessUser());
                currentContactPhone.setDisconnectTS(LocalDateTime.now());
            } else {
                currentContactPhone.setDisconnectRecordedBy(null);
                currentContactPhone.setDisconnectTS(null);
            }
            if (currentContactPhone != null && currentContactPhone.getPhoneID() == 0) {
                onPhoneAddCommitButtonChange();
                System.out.println("PersonBB.togglePhoneEditMode: added new phone");
            } else {
                onPhoneEditCommitButtonChange();
                System.out.println("PersonBB.togglePhoneEditMode: edited phone");
            }
        } else {
            onNoteAppendUniversalInitLinkClick(currentContactPhone);
            prepareNotes(currentNoteHolder);
        }

        currentContactPhoneEditMode = !currentContactPhoneEditMode;

    }

    /**
     * Listener for user requests to start adding a new phone number
     *
     * @param ev
     */
    public void onPhoneAddInitButtonChange(ActionEvent ev) {
        PersonCoordinator pc = getPersonCoordinator();
        currentContactPhone = pc.createContactPhoneSkeleton();
        currentContactPhoneEditMode = true;
        System.out.println("PersonBB.onPhoneAddInitButtonChange");

    }

    /**
     * Listener for user requests to finalize a new phone number
     */
    public void onPhoneAddCommitButtonChange() {
        PersonCoordinator pc = getPersonCoordinator();
        try {
            currentContactPhone = pc.contactPhoneAdd(currentContactPhone, currentPerson, getSessionBean().getSessUser());
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Succesfully inserted new phone number with ID " + currentContactPhone.getPhoneID(), ""));
            refreshCurrentPersonAndUpdateSessionPerson();
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to insert new ContactPhone, sorry. This problem must be fixed by an dev.", ""));
            getFacesContext().validationFailed();
        }
    }

    /**
     * Listener for user requests to start the phone editing process
     *
     * @param ph
     */
    public void onPhoneEditInitButtonChange(ContactPhone ph) {
        PersonCoordinator pc = getPersonCoordinator();
        if (ph != null) {
            try {
                currentContactPhone = pc.getContactPhone(ph.getPhoneID());
            } catch (IntegrationException ex) {
                System.out.println(ex.toString());
            }
            currentContactPhoneDisconnected = ph.getDisconnectTS() != null;
        }
        currentContactPhoneEditMode = false;
    }

    /**
     * Listener for user requests to finalize the phone edit operation
     */
    public void onPhoneEditCommitButtonChange() {
        PersonCoordinator pc = getPersonCoordinator();
        try {
            if (currentContactPhoneDisconnected) {
                currentContactPhone.setDisconnectTS(LocalDateTime.now());
                currentContactPhone.setDisconnectRecordedBy(getSessionBean().getSessUser());
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Marked phone number ID " + currentContactPhone.getPhoneID() + " as disconnected!", ""));
            }
            pc.contactPhoneUpdate(currentContactPhone, getSessionBean().getSessUser());
            commitNotes();
            currentContactPhone = pc.getContactPhone(currentContactPhone);
            // reset our form
            currentContactPhoneDisconnected = false;
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Succesfully Updated new phone number with ID " + currentContactPhone.getPhoneID(), ""));
            refreshCurrentPersonAndUpdateSessionPerson();
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to Update phone number, sorry. This problem must be fixed by an dev.", ""));
        }
    }

    /**
     * Listener for user requests to abort any phone operation
     *
     * @param ev
     */
    public void onPhoneOperationAbortButtonChange(ActionEvent ev) {
        currentContactPhoneEditMode = false;
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Aborted; no changes made!", ""));
    }

    /**
     * Listener for user requests to start the phone removal process
     *
     * @param ev
     */
    public void onPhoneRemoveInitLinkClick(ActionEvent ev) {
        System.out.println("PersonBB.onPhoneRemoveInitLinkClick");
    }
    
    /**
     * Hides a given phone from pretty view
     * @param phone 
     */
    public void onTogglePhoneHideUnhide(ContactPhone phone){
        PersonCoordinator pc = getPersonCoordinator();
        try {
            if(phone.isHidden()){
                // unhide hidden phones
                pc.contactPhoneHideUnhide(phone, false, getSessionBean().getSessUser());
                System.out.println("PersonBB.onTogglePhoneHideUnhide | Unhide | phoneID: " + phone.getPhoneID());
            } else {
                //  hide unhidden phones
                pc.contactPhoneHideUnhide(phone, true, getSessionBean().getSessUser());
                System.out.println("PersonBB.onTogglePhoneHideUnhide | hide | phoneID: " + phone.getPhoneID());
            }
            refreshCurrentPersonAndUpdateSessionPerson();
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
        } 
    }

    /**
     * Listener for user requests to confirm phone removal
     *
     * @param ev
     */
    public void onPhoneRemoveCommitButtonPress(ActionEvent ev) {
        PersonCoordinator pc = getPersonCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        try {
            pc.contactPhoneDeactivate(currentContactPhone, getSessionBean().getSessUser());
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Succesfully removed phone ID: " + currentContactPhone.getPhoneID(), ""));
            StringBuilder sb = new StringBuilder();
            sb.append("Phone ID: ");
            sb.append(currentContactPhone.getPhoneID());
            sb.append(" | Phone number: ");
            sb.append(currentContactPhone.getPhoneNumber());

            MessageBuilderParams mbp = new MessageBuilderParams(
                    currentPerson.getNotes(),
                    "Phone Removal",
                    "The following phone number was removed from this person record: ",
                    sb.toString(),
                    getSessionBean().getSessUser(), null);
            currentPerson.setNotes(sc.appendNoteBlock(mbp));
            sc.writeNotes(currentPerson, getSessionBean().getSessUser());
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Appended note to Person ID: " + currentPerson.getHumanID(), ""));
            refreshCurrentPersonAndUpdateSessionPerson();
        } catch (BObStatusException | IntegrationException | AuthorizationException  ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Phone removal fatal error: " + ex.getMessage(), ""));
        }

    }

    /**
     * ********************************************************
     * ******************* Email INFRASTRUCTURE ***************
     * *******************************************************
     */
    /**
     * Listener for user clicks of the edit button for emails
     *
     * @param ev
     */
    public void toggleEmailEditMode(ActionEvent ev) {
        System.out.println("PersonBB.toggleEmailEditMode | toggle mode: " + currentContactEmailEditMode);
        if (currentContactEmailEditMode) {
            if (currentContactEmail != null && currentContactEmail.getEmailID() == 0) {
                onEmailAddCommitButtonChange();
                System.out.println("PersonBB.toggleEmailEditMode | commited new email");
            } else {
                onEmailEditCommitButtonChange();
                System.out.println("PersonBB.toggleEmailEditMode | edited new email");
            }
        } else {
            onNoteAppendUniversalInitLinkClick(currentContactEmail);
            prepareNotes(currentNoteHolder);
        }
        currentContactEmailEditMode = !currentContactEmailEditMode;
    }

    /**
     * Listener for user requests to start adding a new email
     *
     * @param ev
     */
    public void onEmailAddInitButtonChange(ActionEvent ev) {
        System.out.println("PersonPP.onEmailAddInitButtonChange");
        PersonCoordinator pc = getPersonCoordinator();
        currentContactEmail = pc.createContactEmailSkeleton();
        currentContactEmailEditMode = true;

    }

    /**
     * Listener for user requests to finalize a new email
     */
    public void onEmailAddCommitButtonChange() {
        PersonCoordinator pc = getPersonCoordinator();
        try {
            currentContactEmail = pc.contactEmailAdd(currentContactEmail, currentPerson, getSessionBean().getSessUser());
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Succesfully updated " + currentContactEmail.getEmailaddress() + ", id " + currentPerson.getHumanID(), ""));
            refreshCurrentPersonAndUpdateSessionPerson();
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "ERROR updating email address with id " + currentContactEmail.getEmailID(), ""));
            getFacesContext().validationFailed();
        }
    }

    /**
     * Listener for user requests to start the email editing process
     *
     * @param ce
     */
    public void onEmailEditInitButtonChange(ContactEmail ce) {
        PersonCoordinator pc = getPersonCoordinator();
        try {
            currentContactEmail = pc.getContactEmail(ce.getEmailID());
        } catch (IntegrationException ex) {
            System.out.println(ex.toString());
        }
        currentContactEmailEditMode = false;

    }

    /**
     * Listener for user requests to start the removal process
     *
     * @param ev
     */
    public void onEmailRemoveInitButtonChange(ActionEvent ev) {
        System.out.println("PersonBB.onEmailRemoveInitButtonChange");
    }

    /**
     * Listener for user confirmation to remove an email
     *
     * @param ev
     */
    public void onEmailRemoveCommitButtonChange(ActionEvent ev) {
        PersonCoordinator pc = getPersonCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        try {
            pc.contactEmailDeactivate(currentContactEmail, getSessionBean().getSessUser());
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Successfully removed email ID: " + currentContactEmail.getEmailID(), ""));

            StringBuilder sb = new StringBuilder();
            sb.append("Email ID: ");
            sb.append(currentContactEmail.getEmailID());
            sb.append(" | Email address: ");
            sb.append(currentContactEmail.getEmailaddress());

            MessageBuilderParams mbp = new MessageBuilderParams(
                    currentPerson.getNotes(),
                    "Email Removal",
                    "The following email address was removed from this person record: ",
                    sb.toString(),
                    getSessionBean().getSessUser(), null);
            currentPerson.setNotes(sc.appendNoteBlock(mbp));
            sc.writeNotes(currentPerson, getSessionBean().getSessUser());
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Appended note to Person ID: " + currentPerson.getHumanID(), ""));
            refreshCurrentPersonAndUpdateSessionPerson();
        } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Email removal fatal error: " + ex.getMessage(), ""));

        }
    }

    /**
     * Listener for user requests to finalize the email edit operation
     */
    public void onEmailEditCommitButtonChange() {
        PersonCoordinator pc = getPersonCoordinator();
        try {
            pc.contactEmailUpdate(currentContactEmail, getSessionBean().getSessUser());
            commitNotes();
            currentContactEmail = pc.getContactEmail(currentContactEmail);
            refreshCurrentPersonAndUpdateSessionPerson();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Succesfully updated " + currentPerson.getName() + ", id " + currentPerson.getHumanID(), ""));
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "ERROR updating " + currentPerson.getName() + ", id " + currentPerson.getHumanID(), ""));
        }
    }
    
    /**
     * Hides the given email address
     * @param em 
     */
    public void onToggleEmailHideUnhide(ContactEmail em){
        PersonCoordinator pc = getPersonCoordinator();
        try {
            if(em.isHidden()){
                // unhide hidden ones
                pc.contactEmailHideUnhide(em, false, getSessionBean().getSessUser());
                System.out.println("PersonBB.onToggleEmailHideUnhide | unhidden | emailID: " + em.getEmailID());
            } else {
                // hide unhidden emails
                pc.contactEmailHideUnhide(em, true, getSessionBean().getSessUser());
                System.out.println("PersonBB.onToggleEmailHideUnhide | hidden | emailID: " + em.getEmailID());
            }
            refreshCurrentPersonAndUpdateSessionPerson();
        } catch (BObStatusException | AuthorizationException | IntegrationException  ex) {
            System.out.println(ex);
        } 
    }
    
    

    /**
     * Listener for user requests to abort any email operation
     *
     * @param ev
     */
    public void onEmailOperationAbortButtonChange(ActionEvent ev) {
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Aborted; No changes made!", ""));
        currentContactEmailEditMode = false;

    }

    /**
     * ********************************************************
     */
    /**
     * ******************* MISC STUFF           ***************
     */
    /**
     * ********************************************************
     */
    /**
     * Starts the note adding process on a general note holding object
     *
     * @param holder
     */
    public void onNoteAppendUniversalInitLinkClick(IFace_noteHolder holder) {
        currentNoteHolder = holder;
        formNewNoteUniversal = "";
        pageComponentToUpdateAfterNoteCommit = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("component-to-update");
        System.out.println("PersonBB.onNoteAppendUniversalInitLinkClick | page comp: " + pageComponentToUpdateAfterNoteCommit);
    }

    /**
     * Listener for user aborts of the noting process
     *
     * @param ev
     */
    public void onNoteAppendOperationAbort(ActionEvent ev) {
        formNewNoteUniversal = "";

    }

    /**
     * Listener for user to prepare notes
     *
     * @param holder
     */
    public void prepareNotes(IFace_noteHolder holder) {
        SystemCoordinator sc = getSystemCoordinator();
        String fieldDump = "";
        switch (holder.getNoteHolderFriendlyName()) {
            case "Phone number":
                fieldDump = sc.getFieldDump(currentContactPhone);
                break;
            case "Email Address":
                fieldDump = sc.getFieldDump(currentContactEmail);
                break;
            case "Person":
                fieldDump = sc.getFieldDump(currentPerson);
                break;
            default:
                fieldDump = "";
                break;
        }
        System.out.println(fieldDump);
        setFormNewNoteUniversal(fieldDump);
    }

    /**
     * Listener for user to commit notes
     *
     */
    public void commitNotes() {
        SystemCoordinator sc = getSystemCoordinator();
        if (currentNoteHolder != null) {
            MessageBuilderParams mbp = new MessageBuilderParams(currentNoteHolder.getNotes(),
                    null, null, formNewNoteUniversal, getSessionBean().getSessUser(), null);
            currentNoteHolder.setNotes(sc.universalAppendNoteBlock(mbp));
            try {
                sc.writeNotes(currentNoteHolder, getSessionBean().getSessUser());
                refreshCurrentNoteHolder();
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Note write success on :  " + currentNoteHolder.getNoteHolderFriendlyName(), ""));
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Fatal note error:  " + ex.getMessage(), ""));

            }
            formNewNoteUniversal = "";
        }
    }

    /**
     * Listener for user to commit notes to a universal note holder
     *
     * @param ev
     */
    public void onNoteCommitOnNoteHolder(ActionEvent ev) {
        SystemCoordinator sc = getSystemCoordinator();
        if (currentNoteHolder != null) {
            MessageBuilderParams mbp = new MessageBuilderParams(currentNoteHolder.getNotes(),
                    null, null, formNewNoteUniversal, getSessionBean().getSessUser(), null);
            currentNoteHolder.setNotes(sc.appendNoteBlock(mbp));
            try {
                sc.writeNotes(currentNoteHolder, getSessionBean().getSessUser());
                refreshCurrentNoteHolder();
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Note write success on :  " + currentNoteHolder.getNoteHolderFriendlyName(), ""));
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Fatal note error:  " + ex.getMessage(), ""));

            }
            formNewNoteUniversal = "";
        }
    }

    /**
     * Internal method for asking what type the NoteHolder is and fetching a new
     * copy from the DB. The UI will update itself
     */
    private void refreshCurrentNoteHolder() throws IntegrationException {
        PersonCoordinator pc = getPersonCoordinator();
        if (currentNoteHolder != null) {
            if (currentNoteHolder instanceof ContactEmail) {
                currentContactEmail = pc.getContactEmail(currentContactEmail);
                System.out.println("PersonBB.refreshCurrentNoteHolder | refreshing email");
            } else if (currentNoteHolder instanceof ContactPhone) {
                currentContactPhone = pc.getContactPhone(currentContactPhone);
                System.out.println("PersonBB.refreshCurrentNoteHolder | refreshing phone");
            } else if (currentNoteHolder instanceof Person) {
                refreshCurrentPersonAndUpdateSessionPerson();
                System.out.println("PersonBB.refreshCurrentNoteHolder | refreshing person");
            } else if (currentNoteHolder instanceof HumanLink) {
                refreshCurrentHumanLink();
                System.out.println("PersonBB.refreshCurrentNoteHolder | refreshing humanlink");
            }
        }
    }

    /**
     * ********************************************************
     */
    /**
     * ******************* FANCY LINK MANAGEMENT STUFF ********
     */
    /**
     * ********************************************************
     */
    /**
     * ********************************************************
     */
    /**
     * ************ LINK MANAGE INFRASTRUCTURE ****************
     */
    /**
     * *******************************************************
     */
    /**
     * Listener for user requests to load person links
     *
     * @param ev
     */
    public void onLoadHumanLinksToCurrentPerson(ActionEvent ev) {
        if (currentPerson != null) {
            PersonCoordinator pc = getPersonCoordinator();
            try {
                currentPersonLinkHeavy = pc.assemblePersonLinkHeavy(currentPerson);
                humanLinkParentInfoHeavyList = pc.getHumanLinkParentObjectInfoHeavyList(currentPersonLinkHeavy.getHumanNonHumanLinkList(), getSessionBean().getSessUser());
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Loaded person " + currentPersonLinkHeavy.getHumanNonHumanLinkList().size() + " links!", ""));
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Fatal: Could not load person links, sorry!", ""));

            }
        }
    }

    /**
     * Listener for user requests to load information about the human link
     *
     * @param hl
     */
    public void onHumanLinkSelectLinkClick(HumanLink hl) {
        currentHumanLink = hl;

    }

    /**
     * Listener for user requests to start the change target operation
     *
     * @param ev
     */
    public void onChangeHumanLinkTargetLinkClick(ActionEvent ev) {
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Choosing new person link target", ""));
    }

    /**
     * Listener for user clicks of a human link holder
     *
     * @param hlh
     */
    public void onActivateNewHumanLinkTarget(IFace_humanListHolder hlh) {
        if (hlh != null) {
            try {
                currentHumanListHolder = hlh;
                loadLinkedObjectRoleList();
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "You are now ready to link to the session " + hlh.getHUMAN_LINK_SCHEMA_ENUM().getTARGET_OBJECT_FRIENDLY_NAME(), ""));
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "FATAL ERROR: Could not setup link target, sorry!", ""));
            }
            getSessionBean().setSessHumanListHolder(hlh);
        } else {
            System.out.println("PersonBB.onActivateNewHumanLinkTarget | null IFace_humanListHolder");
        }

    }

    /**
     * Listener for user clicks of the link representing the target of a human
     * link, such as a case or an event
     *
     *
     * @param hlpoih the human link to explore
     * @return page nav route
     */
    public String onHumanLinkTargetIDLinkClick(HumanLinkParentObjectInfoHeavy hlpoih) {
        if (hlpoih != null && hlpoih.getLinkedObjectRole() != null && hlpoih.getLinkedObjectRole().getSchema() != null) {
            try {
                return getSessionBean().evaluateHumanLinkParentForCrossMuniViewing(hlpoih, getSessionBean().getSessUser());
            } catch (BObStatusException ex) {
                System.out.println("ERROR: PersonBB.onHumanLinkTargetIDLinkClick");
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "FATAL ERROR: Could navigate to linked object due to a system error!", ""));
            }
        }
        return "";
    }

    /**
     * Listener to start the human link note process
     *
     * @param ev
     */
    public void onHumanLinkNoteInit(ActionEvent ev) {
        formHumanLinkNotes = "";
    }

    /**
     * Listener for user requests to complete the note appending process on a
     * human link
     *
     * @param ev
     */
    public void onHumanLinkNoteAppendCommitButtonChange(ActionEvent ev) {
        PersonCoordinator pc = getPersonCoordinator();
        try {
            pc.appendNoteToHumanLink(currentHumanLink, formHumanLinkNotes, getSessionBean().getSessUser());
            refreshCurrentHumanLink();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Humanlink Note write success!", ""));
        } catch (BObStatusException | IntegrationException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal: Could not append note: " + ex.getMessage(), ""));
        }
    }

    /**
     * Listener for user requests to start the deac process for human links
     *
     * @param ev
     */
    public void onHumanLinkDeactivatInitLinkClick(ActionEvent ev) {
        System.out.println("PersonBB.onHumanLinkDeactivatInitLinkClick");
    }

    /**
     * Listener for user requests to complete the note appending process on a
     * human link
     *
     * @param ev
     */
    public void onHumanLinkDeactivateButtonChange(ActionEvent ev) {
        PersonCoordinator pc = getPersonCoordinator();
        try {
            pc.deactivateHumanLink(currentHumanLink, getSessionBean().getSessUser());
            // this call is needed to refresh the cross-link domain link list on the person
            // profile that doesn't follow the same pattern as an oject that stores a list of humans
            onLoadHumanLinksToCurrentPerson(null);
            // this call refreshes the human list holder
            injectSessionHumanLinkListForComponentRefresh();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Link deactivate success!", ""));
        } catch (BObStatusException | IntegrationException | AuthorizationException  ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal: Could not deactivate human link: " + ex.getMessage(), ""));
        }

    }

    /**
     * Listener to user requests to start the creation process for a new human
     * link. Checks to make sure the session has a HumanLinkHolder. If it
     * doesn't, inject the current CECase, if that's null, inject the current
     * session OccPeriod
     *
     * @param ev
     */
    public void onHumanLinkAddInitButtonChange(ActionEvent ev) {
        PersonCoordinator pc = getPersonCoordinator();
        currentHumanLink = pc.createHumanLinkSkeleton(currentPerson);
        // setup our human list holder if we need one
        if (getSessionBean().getSessHumanListHolder() == null) {
            if (getSessionBean().getSessCECase() != null) {
                getSessionBean().setSessHumanListHolder(getSessionBean().getSessCECase());
            } else if (getSessionBean().getSessOccPeriod() != null) {
                getSessionBean().setSessHumanListHolder(getSessionBean().getSessOccPeriod());
            }
        }
        // configure our candidate roles
        try {
            loadLinkedObjectRoleList();
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }
    }

    /**
     * Listener for user requests to link the current IFace_HumanListHolder to
     * the current person with the currently selected role
     *
     * @param ev
     */
    public void onHumanLinkCreateCommitButtonChange(ActionEvent ev) {
        PersonCoordinator pc = getPersonCoordinator();
        try {
            currentHumanLink.setLinkRole(selecetedLinkedObjetRole);

            // write and refresh our link
            int freshLinkID = pc.insertHumanLink(getSessionBean().getSessHumanListHolder(), currentHumanLink, getSessionBean().getSessUser());
            currentHumanLink = pc.getHumanLink(freshLinkID, getSessionBean().getSessHumanListHolder().getHUMAN_LINK_SCHEMA_ENUM());
            getSessionBean().setSessHumanLink(currentHumanLink);
            
            // refresh our current person and its links
            refreshCurrentPersonAndUpdateSessionPerson();
            onLoadHumanLinksToCurrentPerson(null);
            injectSessionHumanLinkListForComponentRefresh();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Successfully linked human!", ""));
        } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal: Could not create human link: " + ex.getMessage(), ""));
            getFacesContext().validationFailed();
            
        }
    }

    /**
     * Gets a new set of human links from the coordinator and injects it into
     * the session's master list of human links. It's the job of the refreshed
     * component to ask the session for this list and inject it into its
     * HumanListHolder for immediate viewing by the user in the browser. Note
     * that this means the only call to the DB is made here via the person
     * coordinator and the calling object family need not rebuild its entire
     * data heaby object!
     */
    private void injectSessionHumanLinkListForComponentRefresh() {
        PersonCoordinator pc = getPersonCoordinator();
        List<HumanLink> hllist = null;
        if (currentHumanListHolder != null) {
            try {
                hllist = pc.getHumanLinkList(currentHumanListHolder);
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
            }
            getSessionBean().setSessHumanListRefreshedList(hllist);
        } else {
            System.out.println("PersonBB.injectSessionHumanLinkListForComponentRefresh | no currentHumanListHolder for session injection");
        }
    }

    /**
     *
     * @return the linkRoleCandidateList
     */
    public List<LinkedObjectRole> getLinkRoleCandidateList() {

        return linkRoleCandidateList;
    }

    /**
     * Big cheese listener for user requests to start the process of linking
     * humans to a human list holder. I basically inject the HLH into the
     * session and keep a record of the name of the component I should update
     * when the linking is done.
     *
     * I also configure the upstream pool.
     *
     * @param hlh to which we should start linking persons
     */
    public void onSelectAndLinkPersonsInit(IFace_humanListHolder hlh) {

        currentHumanListHolder = hlh;
        getSessionBean().setSessHumanListHolder(currentHumanListHolder);

        extractHumanLinkListComponentIDFromHTTPRequest();
        try {
            loadLinkedObjectRoleList();
            configureUpstreamPersonLinkPool();
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }

        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Commencing linking of Persons to " + hlh.getHUMAN_LINK_SCHEMA_ENUM().getTARGET_OBJECT_FRIENDLY_NAME() + " ID: " + hlh.getHostPK(), ""));
        System.out.println("PersonBB.onSelectAndLinkPersonsInit | " + hlh.getHUMAN_LINK_SCHEMA_ENUM().getTARGET_OBJECT_FRIENDLY_NAME() + " |  PK: " + hlh.getHostPK());

    }

    /**
     * Interrogates the current humanListHolder and builds an upstream pool if
     * possible. So, if my HLH is a cecase, my pool of human link candidates are
     * this case's parent property's human links
     */
    private void configureUpstreamPersonLinkPool() {
        if (currentHumanListHolder != null
                && currentHumanListHolder.getUpstreamHumanLinkPoolEnum() != null
                && currentHumanListHolder.getUpstreamHumanLinkPoolFeederID() != 0) {

            try {
                PropertyCoordinator pc = getPropertyCoordinator();
                CaseCoordinator cc = getCaseCoordinator();

                switch (currentHumanListHolder.getUpstreamHumanLinkPoolEnum()) {
                    case CECaseHuman ->
                        currentHumanLinkPoolFeeder = (IFace_humanListHolder) cc.cecase_assembleCECaseDataHeavy(cc.cecase_getCECase(currentHumanListHolder.getUpstreamHumanLinkPoolFeederID(), getSessionBean().getSessUser()), getSessionBean().getSessUser());
                    case CitationHuman ->
                        currentHumanLinkPoolFeeder = (IFace_humanListHolder) cc.citation_getCitation(currentHumanListHolder.getUpstreamHumanLinkPoolFeederID());
                    case ParcelHuman ->
                        currentHumanLinkPoolFeeder = (IFace_humanListHolder) pc.getPropertyDataHeavy(currentHumanListHolder.getUpstreamHumanLinkPoolFeederID(), getSessionBean().getSessUser());
                    case ParcelUnitHuman ->
                        currentHumanLinkPoolFeeder = (IFace_humanListHolder) pc.getPropertyUnitDataHeavy(pc.getPropertyUnit(currentHumanListHolder.getUpstreamHumanLinkPoolFeederID()), getSessionBean().getSessUser());
                    default ->
                        System.out.println("PersonBB.configureUpstreamPersonLinkPool | unsupported upstream pool enum");
                }
            } catch (IntegrationException | AuthorizationException | BObStatusException | BlobException | EventException | SearchException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Fatal: Could not create human link pool: " + ex.getMessage(), ""));
            }

        } else {
            System.out.println("PersonBB.configureUpstreamPersonLinkPool | cannot build pool due to null HLH, pool enum, or ID == 0");
        }

    }

    /**
     * Extracts the value of key person-list-component-to-update from the HTTP
     * request for later updating
     */
    private void extractHumanLinkListComponentIDFromHTTPRequest() {
        personListComponentIDToUpdatePostLinkingOperation
                = FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getRequestParameterMap()
                        .get("person-list-component-to-update");
        System.out.println("PersonBB.extractHumanLinkListComponentIDFromHTTPRequest | got component ID: " + personListComponentIDToUpdatePostLinkingOperation);
    }

    /**
     * Special getter that checks the session for updates on just this person.
     *
     * @return
     */
    public List<MailingAddressLink> getCurrentPersonMADLinkList() {
        if (currentPerson != null) {
            List<MailingAddressLink> sessLinkList = getSessionBean().getSessMailingAddressLinkRefreshedList();
            if (sessLinkList != null) {
                currentPerson.setMailingAddressLinkList(sessLinkList);
                getSessionBean().setSessMailingAddressLinkRefreshedList(null);
            }
            return currentPerson.getMailingAddressLinkList();
        }
        return new ArrayList<>();
    }

    /**
     * Listener for user requests to load person on the current property, which
     * is a human list holder
     *
     * @param ev
     */
    public void onLoadPersonListOnSessionPropertyLinkClick(ActionEvent ev) {
        System.out.println("PersonBB.loadPersonListOnSessionProperty");
        PersonCoordinator pc = getPersonCoordinator();
        if (personList != null && !personList.isEmpty()) {
            if (getSessionBean().getSessProperty() != null && getSessionBean().getSessProperty().gethumanLinkList() != null) {
                if (!getSessionBean().getSessProperty().gethumanLinkList().isEmpty()) {
                    personList.clear();
                    try {
                        personList.addAll(pc.getPersonListFromHumanLinkList(getSessionBean().getSessProperty().gethumanLinkList()));
                        getFacesContext().addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_INFO,
                                        "Loaded " + personList.size() + " persons from the current property!", ""));
                    } catch (IntegrationException | BObStatusException ex) {
                        System.out.println(ex);
                        getFacesContext().addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                        "Error converting human links into persons for display", ""));
                    }
                } else {
                    getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                    "Session property has a person list but that list is sans persons", ""));
                }
            } else {
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Session property does not contain a list of persons", ""));
            }
        } else {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error: Person search list improperly setup", ""));
        }
    }

    /**
     * Listener for user requests to add a person from the person search to the
     * list of selected humans ready to be linked to the session humanListHolder
     *
     * @param per
     */
    public void onAddPersonToSelectedList(Person per) {
        PersonCoordinator pc = getPersonCoordinator();
        humanLinkListQueued.add(pc.createHumanLinkSkeleton(per));
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Added " + per.getName() + " to selected list.", ""));
    }

    /**
     * listener for user requests to add all of the persons in the working
     * person list to the queued for linking list
     *
     * @param ev
     */
    public void onQueueAllPersonsForLinking(ActionEvent ev) {
        if (personList != null && !personList.isEmpty() && humanLinkListQueued != null) {
            for (Person p : personList) {
                onAddPersonToSelectedList(p);
            }
        }
    }

    /**
     * Listener for user requests to remove a person from the session list of
     * selected persons
     *
     * @param hl
     */
    public void onRemovePersonFromSelectedList(HumanLink hl) {
        humanLinkListQueued.remove(hl);
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Removed" + hl.getName() + " to selected list.", ""));
    }

    /**
     * Listener for user requests to extract the humans from a list of human
     * links and make them part of our list queued for linking
     *
     * @param ev
     */
    public void onTransferLinkedPersonsToWorkingPersonList(ActionEvent ev) {
        PersonCoordinator pc = getPersonCoordinator();
        if (currentHumanListHolder != null && currentHumanListHolder.gethumanLinkList() != null && personList != null) {
            if (!appendResultsToList) {
                personList.clear();
            }
            try {
                System.out.println("PersonBB.onTransferLinkedPersonsToWorkingPersonList | Transferring " + currentHumanListHolder.gethumanLinkList().size() + " persons!");
                personList.addAll(pc.getPersonListFromHumanLinkList(currentHumanListHolder.gethumanLinkList()));
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Transferred " + currentHumanListHolder.gethumanLinkList().size() + " persons to working list for queuing.", ""));
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Fatal error: transferring persons to working list!", ""));
            }
        }
    }

    /**
     * Listener for user requests to start the linking process
     *
     * @Deprecated users must select a link role first
     * @param ev
     */
    public void onLinkPersonToCurrentParcelInit(ActionEvent ev) {
        PersonCoordinator pc = getPersonCoordinator();
        currentHumanLink = pc.createHumanLinkSkeleton(currentPerson);
        getSessionBean().setSessHumanListHolder(getSessionBean().getSessProperty());
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Choose your link role", ""));
    }

    /**
     * Quick workaround to link a person to the session property
     *
     * @param ev
     */
    public void onLinkPersonToCurrentParcel(ActionEvent ev) {
        PersonCoordinator pc = getPersonCoordinator();
        if (currentPerson != null && getSessionBean().getSessProperty() != null) {
            try {
                pc.insertHumanLink(getSessionBean().getSessProperty(), pc.createHumanLinkSkeleton(currentPerson), getSessionBean().getSessUser());
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Linked " + currentPerson.getName() + " to Property ID " + getSessionBean().getSessProperty().getCountyParcelID(), ""));
                getSessionBean().setSessHumanListRefreshedList(pc.getHumanLinkList(getSessionBean().getSessProperty()));
            } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Error linking person: " + ex.getMessage(), ""));
            }
        }
    }

    /**
     * listener for user requests to undertake the linking operation of the
     * selected humans to the session's humanlisthold
     *
     * @param ev
     */
    public void onLinkSelectedPersonsToPersonHolder(ActionEvent ev) {
        PersonCoordinator pc = getPersonCoordinator();
        if (humanLinkListQueued != null && !humanLinkListQueued.isEmpty()) {
            for (HumanLink hl : humanLinkListQueued) {
                try {
                    pc.insertHumanLink(currentHumanListHolder,
                            hl,
                            getSessionBean().getSessUser());
                    System.out.println("PersonBB.onLinkSelectedPersonsToPersonHolder | linked p " + hl.getName());

                    getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO,
                                    "Linked " + hl.getName() + " to Object ID " + currentHumanListHolder.getHostPK(), ""));
                } catch (BObStatusException | IntegrationException | AuthorizationException  ex) {
                    System.out.println(ex);
                    getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                    "Error linking person: " + ex.getMessage(), ""));
                }
            }
            refreshHumanListHolder();
            humanLinkListQueued.clear();
        }
    }

    /**
     * Refreshes our human link list
     */
    public void refreshHumanListHolder() {
        if (currentHumanListHolder != null) {
            PersonCoordinator pc = getPersonCoordinator();
            try {
                currentHumanListHolder.sethumanLinkList(pc.getHumanLinkList(currentHumanListHolder));
                getSessionBean().setSessHumanListHolder(currentHumanListHolder);
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
            }
        }
    }

    /**
     * Listener for user requests to remove the given human from the session's
     * human list holder permanently
     *
     * @param hl
     */
    public void onRemoveHumanFromSessionHumanListHolder(HumanLink hl) {
        PersonCoordinator pc = getPersonCoordinator();
        try {
            pc.deactivateHumanLink(hl, getSessionBean().getSessUser());
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Successfully deactivated person link " + hl.getLinkID(), ""));
        } catch (BObStatusException | IntegrationException | AuthorizationException  ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Error removing person link: " + ex.getMessage(), ""));
        }

    }

    private void convertSelectedUnitToDataHeavyVersion() {
        PropertyCoordinator pc = getPropertyCoordinator();
        if (selectedUnitForHumanLinkTarget != null) {
            try {
                selectedUnitForHumanLinkTargetDataHeavy = pc.getPropertyUnitDataHeavy(
                        selectedUnitForHumanLinkTarget,
                        getSessionBean().getSessUser());
            } catch (IntegrationException | AuthorizationException | BObStatusException | EventException ex) {
                System.out.println(ex);
            }
        }
    }

    /**
     * ********************************************************
     */
    /**
     * ******************* GETTERS AND SETTERS  ***************
     */
    /**
     * *******************************************************
     */
    /**
     * @return the currentPersonHumanFieldsEditMode
     */
    public boolean isCurrentPersonHumanFieldsEditMode() {
        return currentPersonHumanFieldsEditMode;
    }

    /**
     * @return the currentMailingAddress
     */
    public MailingAddress getCurrentMailingAddress() {
        return currentMailingAddress;
    }

    /**
     * @return the currentMailingAddressEditMode
     */
    public boolean isCurrentMailingAddressEditMode() {
        return currentMailingAddressEditMode;
    }

    /**
     * @return the currentContactPhone
     */
    public ContactPhone getCurrentContactPhone() {
        return currentContactPhone;
    }

    /**
     * @return the currentContactPhoneEditMode
     */
    public boolean isCurrentContactPhoneEditMode() {
        return currentContactPhoneEditMode;
    }

    /**
     * @return the currentContactEmail
     */
    public ContactEmail getCurrentContactEmail() {
        return currentContactEmail;
    }

    /**
     * @return the currentContactEmailEditMode
     */
    public boolean isCurrentContactEmailEditMode() {
        return currentContactEmailEditMode;
    }

    /**
     * @param currentPersonHumanFieldsEditMode the
     * currentPersonHumanFieldsEditMode to set
     */
    public void setCurrentPersonHumanFieldsEditMode(boolean currentPersonHumanFieldsEditMode) {
        this.currentPersonHumanFieldsEditMode = currentPersonHumanFieldsEditMode;
    }

    /**
     * @param currentMailingAddress the currentMailingAddress to set
     */
    public void setCurrentMailingAddress(MailingAddress currentMailingAddress) {
        this.currentMailingAddress = currentMailingAddress;
    }

    /**
     * @param currentMailingAddressEditMode the currentMailingAddressEditMode to
     * set
     */
    public void setCurrentMailingAddressEditMode(boolean currentMailingAddressEditMode) {
        this.currentMailingAddressEditMode = currentMailingAddressEditMode;
    }

    /**
     * @param currentContactPhone the currentContactPhone to set
     */
    public void setCurrentContactPhone(ContactPhone currentContactPhone) {
        this.currentContactPhone = currentContactPhone;
    }

    /**
     * @param currentContactPhoneEditMode the currentContactPhoneEditMode to set
     */
    public void setCurrentContactPhoneEditMode(boolean currentContactPhoneEditMode) {
        this.currentContactPhoneEditMode = currentContactPhoneEditMode;
    }

    /**
     * @param currentContactEmail the currentContactEmail to set
     */
    public void setCurrentContactEmail(ContactEmail currentContactEmail) {
        this.currentContactEmail = currentContactEmail;
    }

    /**
     * @param currentContactEmailEditMode the currentContactEmailEditMode to set
     */
    public void setCurrentContactEmailEditMode(boolean currentContactEmailEditMode) {
        this.currentContactEmailEditMode = currentContactEmailEditMode;
    }

    /**
     * @return the currentPerson
     */
    public PersonLinkHeavy getCurrentPerson() {
        return currentPerson;
    }

    /**
     * @param currentPerson the currentPerson to set
     */
    public void setCurrentPerson(PersonLinkHeavy currentPerson) {
        this.currentPerson = currentPerson;
    }

    /**
     * @return the phoneTypeList
     */
    public List<ContactPhoneType> getPhoneTypeList() {
        return phoneTypeList;
    }

    /**
     * @param phoneTypeList the phoneTypeList to set
     */
    public void setPhoneTypeList(List<ContactPhoneType> phoneTypeList) {
        this.phoneTypeList = phoneTypeList;
    }

    /**
     * @return the currentContactPhoneDisconnected
     */
    public boolean isCurrentContactPhoneDisconnected() {
        return currentContactPhoneDisconnected;
    }

    /**
     * @param currentContactPhoneDisconnected the
     * currentContactPhoneDisconnected to set
     */
    public void setCurrentContactPhoneDisconnected(boolean currentContactPhoneDisconnected) {
        this.currentContactPhoneDisconnected = currentContactPhoneDisconnected;
    }

    /**
     * @return the sourceList
     */
    public List<BOBSource> getSourceList() {
        return sourceList;
    }

    /**
     * @param sourceList the sourceList to set
     */
    public void setSourceList(List<BOBSource> sourceList) {
        this.sourceList = sourceList;
    }

    /**
     * @return the currentPersonLinkHeavy
     */
    public PersonLinkHeavy getCurrentPersonLinkHeavy() {
        return currentPersonLinkHeavy;
    }

    /**
     * @param currentPersonLinkHeavy the currentPersonLinkHeavy to set
     */
    public void setCurrentPersonLinkHeavy(PersonLinkHeavy currentPersonLinkHeavy) {
        this.currentPersonLinkHeavy = currentPersonLinkHeavy;
    }

    /**
     * @return the currentHumanLink
     */
    public HumanLink getCurrentHumanLink() {
        return currentHumanLink;
    }

    /**
     * @param currentHumanLink the currentHumanLink to set
     */
    public void setCurrentHumanLink(HumanLink currentHumanLink) {
        this.currentHumanLink = currentHumanLink;
    }

    /**
     * @return the formHumanLinkNotes
     */
    public String getFormHumanLinkNotes() {
        return formHumanLinkNotes;
    }

    /**
     * @param formHumanLinkNotes the formHumanLinkNotes to set
     */
    public void setFormHumanLinkNotes(String formHumanLinkNotes) {
        this.formHumanLinkNotes = formHumanLinkNotes;
    }

    /**
     * @return the selecetedLinkedObjetRole
     */
    public LinkedObjectRole getSelecetedLinkedObjetRole() {
        return selecetedLinkedObjetRole;
    }

    /**
     * @param selecetedLinkedObjetRole the selecetedLinkedObjetRole to set
     */
    public void setSelecetedLinkedObjetRole(LinkedObjectRole selecetedLinkedObjetRole) {
        this.selecetedLinkedObjetRole = selecetedLinkedObjetRole;
    }

    /**
     * @param linkRoleCandidateList the linkRoleCandidateList to set
     */
    public void setLinkRoleCandidateList(List<LinkedObjectRole> linkRoleCandidateList) {
        this.linkRoleCandidateList = linkRoleCandidateList;
    }

    /**
     * @return the querySelected
     */
    public QueryPerson getQuerySelected() {
        return querySelected;
    }

    /**
     * @return the queryList
     */
    public List<QueryPerson> getQueryList() {
        return queryList;
    }

    /**
     * @return the paramsSelected
     */
    public SearchParamsPerson getParamsSelected() {
        return paramsSelected;
    }

    /**
     * @return the queryLog
     */
    public String getQueryLog() {
        return queryLog;
    }

    /**
     * @return the filteredPersonList
     */
    public List<Person> getFilteredPersonList() {
        return filteredPersonList;
    }

    /**
     * @return the appendResultsToList
     */
    public boolean isAppendResultsToList() {
        return appendResultsToList;
    }

    /**
     * @param querySelected the querySelected to set
     */
    public void setQuerySelected(QueryPerson querySelected) {
        this.querySelected = querySelected;
    }

    /**
     * @param queryList the queryList to set
     */
    public void setQueryList(List<QueryPerson> queryList) {
        this.queryList = queryList;
    }

    /**
     * @param paramsSelected the paramsSelected to set
     */
    public void setParamsSelected(SearchParamsPerson paramsSelected) {
        this.paramsSelected = paramsSelected;
    }

    /**
     * @param queryLog the queryLog to set
     */
    public void setQueryLog(String queryLog) {
        this.queryLog = queryLog;
    }

    /**
     * @param filteredPersonList the filteredPersonList to set
     */
    public void setFilteredPersonList(List<Person> filteredPersonList) {
        this.filteredPersonList = filteredPersonList;
    }

    /**
     * @param appendResultsToList the appendResultsToList to set
     */
    public void setAppendResultsToList(boolean appendResultsToList) {
        this.appendResultsToList = appendResultsToList;
    }

    /**
     * @return the personList
     */
    public List<Person> getPersonList() {
        return personList;
    }

    /**
     * @param personList the personList to set
     */
    public void setPersonList(List<Person> personList) {
        this.personList = personList;
    }

    /**
     * @return the currentNoteHolder
     */
    public IFace_noteHolder getCurrentNoteHolder() {
        return currentNoteHolder;
    }

    /**
     * @param currentNoteHolder the currentNoteHolder to set
     */
    public void setCurrentNoteHolder(IFace_noteHolder currentNoteHolder) {
        this.currentNoteHolder = currentNoteHolder;
    }

    /**
     * @return the formNewNoteUniversal
     */
    public String getFormNewNoteUniversal() {
        return formNewNoteUniversal;
    }

    /**
     * @param formNewNoteUniversal the formNewNoteUniversal to set
     */
    public void setFormNewNoteUniversal(String formNewNoteUniversal) {
        this.formNewNoteUniversal = formNewNoteUniversal;
    }

    /**
     * @return the pageComponentToUpdateAfterNoteCommit
     */
    public String getPageComponentToUpdateAfterNoteCommit() {
        return pageComponentToUpdateAfterNoteCommit;
    }

    /**
     * @param pageComponentToUpdateAfterNoteCommit the
     * pageComponentToUpdateAfterNoteCommit to set
     */
    public void setPageComponentToUpdateAfterNoteCommit(String pageComponentToUpdateAfterNoteCommit) {
        this.pageComponentToUpdateAfterNoteCommit = pageComponentToUpdateAfterNoteCommit;
    }

    /**
     * @return the humanLinkListQueued
     */
    public List<HumanLink> getHumanLinkListQueued() {
        return humanLinkListQueued;
    }

    /**
     * @param humanLinkListQueued the humanLinkListQueued to set
     */
    public void setHumanLinkListQueued(List<HumanLink> humanLinkListQueued) {
        this.humanLinkListQueued = humanLinkListQueued;
    }

    /**
     * @return the personListComponentIDToUpdatePostLinkingOperation
     */
    public String getPersonListComponentIDToUpdatePostLinkingOperation() {
        return personListComponentIDToUpdatePostLinkingOperation;
    }

    /**
     * @param personListComponentIDToUpdatePostLinkingOperation the
     * personListComponentIDToUpdatePostLinkingOperation to set
     */
    public void setPersonListComponentIDToUpdatePostLinkingOperation(String personListComponentIDToUpdatePostLinkingOperation) {
        this.personListComponentIDToUpdatePostLinkingOperation = personListComponentIDToUpdatePostLinkingOperation;
    }

    /**
     * @return the currentHumanLinkEditMode
     */
    public boolean isCurrentHumanLinkEditMode() {
        return currentHumanLinkEditMode;
    }

    /**
     * @param currentHumanLinkEditMode the currentHumanLinkEditMode to set
     */
    public void setCurrentHumanLinkEditMode(boolean currentHumanLinkEditMode) {
        this.currentHumanLinkEditMode = currentHumanLinkEditMode;
    }

    /**
     * @return the currentHumanListHolder
     */
    public IFace_humanListHolder getCurrentHumanListHolder() {
        return currentHumanListHolder;
    }

    /**
     * @param currentHumanListHolder the currentHumanListHolder to set
     */
    public void setCurrentHumanListHolder(IFace_humanListHolder currentHumanListHolder) {
        this.currentHumanListHolder = currentHumanListHolder;
    }

    /**
     * @return the selectedUnitForHumanLinkTarget
     */
    public PropertyUnit getSelectedUnitForHumanLinkTarget() {
        return selectedUnitForHumanLinkTarget;
    }

    /**
     * @param selectedUnitForHumanLinkTarget the selectedUnitForHumanLinkTarget
     * to set
     */
    public void setSelectedUnitForHumanLinkTarget(PropertyUnit selectedUnitForHumanLinkTarget) {
        convertSelectedUnitToDataHeavyVersion();
        this.selectedUnitForHumanLinkTarget = selectedUnitForHumanLinkTarget;
    }

    /**
     * @return the selectedUnitForHumanLinkTargetDataHeavy
     */
    public PropertyUnitDataHeavy getSelectedUnitForHumanLinkTargetDataHeavy() {
        return selectedUnitForHumanLinkTargetDataHeavy;
    }

    /**
     * @param selectedUnitForHumanLinkTargetDataHeavy the
     * selectedUnitForHumanLinkTargetDataHeavy to set
     */
    public void setSelectedUnitForHumanLinkTargetDataHeavy(PropertyUnitDataHeavy selectedUnitForHumanLinkTargetDataHeavy) {
        this.selectedUnitForHumanLinkTargetDataHeavy = selectedUnitForHumanLinkTargetDataHeavy;
    }

    /**
     * @return the upstreamPersonPoolList
     */
    public List<IFace_humanListHolder> getUpstreamPersonPoolList() {
        return upstreamPersonPoolList;
    }

    /**
     * @param upstreamPersonPoolList the upstreamPersonPoolList to set
     */
    public void setUpstreamPersonPoolList(List<IFace_humanListHolder> upstreamPersonPoolList) {
        this.upstreamPersonPoolList = upstreamPersonPoolList;
    }

    /**
     * @return the humanLinkParentInfoHeavyList
     */
    public List<HumanLinkParentObjectInfoHeavy> getHumanLinkParentInfoHeavyList() {
        return humanLinkParentInfoHeavyList;
    }

    /**
     * @param humanLinkParentInfoHeavyList the humanLinkParentInfoHeavyList to
     * set
     */
    public void setHumanLinkParentInfoHeavyList(List<HumanLinkParentObjectInfoHeavy> humanLinkParentInfoHeavyList) {
        this.humanLinkParentInfoHeavyList = humanLinkParentInfoHeavyList;
    }

    /**
     * @return the currentHumanLinkPoolFeeder
     */
    public IFace_humanListHolder getCurrentHumanLinkPoolFeeder() {
        return currentHumanLinkPoolFeeder;
    }

    /**
     * @param currentHumanLinkPoolFeeder the currentHumanLinkPoolFeeder to set
     */
    public void setCurrentHumanLinkPoolFeeder(IFace_humanListHolder currentHumanLinkPoolFeeder) {
        this.currentHumanLinkPoolFeeder = currentHumanLinkPoolFeeder;
    }

    /**
     * @return the personQuickSearchResultList
     */
    public List<Person> getPersonQuickSearchResultList() {
        return personQuickSearchResultList;
    }

    /**
     * @param personQuickSearchResultList the personQuickSearchResultList to set
     */
    public void setPersonQuickSearchResultList(List<Person> personQuickSearchResultList) {
        this.personQuickSearchResultList = personQuickSearchResultList;
    }

    /**
     * @return the queryQuickSearch
     */
    public QueryPerson getQueryQuickSearch() {
        return queryQuickSearch;
    }

    /**
     * @param queryQuickSearch the queryQuickSearch to set
     */
    public void setQueryQuickSearch(QueryPerson queryQuickSearch) {
        this.queryQuickSearch = queryQuickSearch;
    }

    /**
     * @return the certPersonParentStringSelected
     */
    public String getCertPersonParentStringSelected() {
        return certPersonParentStringSelected;
    }

    /**
     * @param certPersonParentStringSelected the certPersonParentStringSelected
     * to set
     */
    public void setCertPersonParentStringSelected(String certPersonParentStringSelected) {
        this.certPersonParentStringSelected = certPersonParentStringSelected;
    }

    /**
     * @return the certPersonParentSelectedLORSelected
     */
    public LinkedObjectRole getCertPersonParentSelectedLORSelected() {
        return certPersonParentSelectedLORSelected;
    }

    /**
     * @param certPersonParentSelectedLORSelected the
     * certPersonParentSelectedLORSelected to set
     */
    public void setCertPersonParentSelectedLORSelected(LinkedObjectRole certPersonParentSelectedLORSelected) {
        this.certPersonParentSelectedLORSelected = certPersonParentSelectedLORSelected;
    }

    /**
     * @return the currentAlias
     */
    public HumanAlias getCurrentAlias() {
        return currentAlias;
    }

    /**
     * @param currentAlias the currentAlias to set
     */
    public void setCurrentAlias(HumanAlias currentAlias) {
        this.currentAlias = currentAlias;
    }

    /**
     * @return the aliasRoleCandidateList
     */
    public List<HumanAliasRoleEnum> getAliasRoleCandidateList() {
        return aliasRoleCandidateList;
    }

    /**
     * @param aliasRoleCandidateList the aliasRoleCandidateList to set
     */
    public void setAliasRoleCandidateList(List<HumanAliasRoleEnum> aliasRoleCandidateList) {
        this.aliasRoleCandidateList = aliasRoleCandidateList;
    }

    /**
     * @return the editModeAlias
     */
    public boolean isEditModeAlias() {
        return editModeAlias;
    }

    /**
     * @param editModeAlias the editModeAlias to set
     */
    public void setEditModeAlias(boolean editModeAlias) {
        this.editModeAlias = editModeAlias;
    }

    /**
     * @return the mergeRequest
     */
    public PersonMergeRequest getMergeRequest() {
        return mergeRequest;
    }

    /**
     * @param mergeRequest the mergeRequest to set
     */
    public void setMergeRequest(PersonMergeRequest mergeRequest) {
        this.mergeRequest = mergeRequest;
    }

    /**
     * @return the currentMergeLog
     */
    public PersonMergeLog getCurrentMergeLog() {
        return currentMergeLog;
    }

    /**
     * @param currentMergeLog the currentMergeLog to set
     */
    public void setCurrentMergeLog(PersonMergeLog currentMergeLog) {
        this.currentMergeLog = currentMergeLog;
    }

    /**
     * @return the matchLevelCandidates
     */
    public List<MergeMatchLevelEnum> getMatchLevelCandidates() {
        return matchLevelCandidates;
    }

    /**
     * @param matchLevelCandidates the matchLevelCandidates to set
     */
    public void setMatchLevelCandidates(List<MergeMatchLevelEnum> matchLevelCandidates) {
        this.matchLevelCandidates = matchLevelCandidates;
    }

    /**
     * @return the currentMergeLogForViewingOnly
     */
    public PersonMergeLog getCurrentMergeLogForViewingOnly() {
        return currentMergeLogForViewingOnly;
    }

    /**
     * @param currentMergeLogForViewingOnly the currentMergeLogForViewingOnly to set
     */
    public void setCurrentMergeLogForViewingOnly(PersonMergeLog currentMergeLogForViewingOnly) {
        this.currentMergeLogForViewingOnly = currentMergeLogForViewingOnly;
    }

    /**
     * @return the personQuickAddIncludePhone
     */
    public boolean isPersonQuickAddIncludePhone() {
        return personQuickAddIncludePhone;
    }

    /**
     * @param personQuickAddIncludePhone the personQuickAddIncludePhone to set
     */
    public void setPersonQuickAddIncludePhone(boolean personQuickAddIncludePhone) {
        this.personQuickAddIncludePhone = personQuickAddIncludePhone;
    }

    /**
     * @return the personQuickAddIncludeEmail
     */
    public boolean isPersonQuickAddIncludeEmail() {
        return personQuickAddIncludeEmail;
    }

    /**
     * @param personQuickAddIncludeEmail the personQuickAddIncludeEmail to set
     */
    public void setPersonQuickAddIncludeEmail(boolean personQuickAddIncludeEmail) {
        this.personQuickAddIncludeEmail = personQuickAddIncludeEmail;
    }

}
