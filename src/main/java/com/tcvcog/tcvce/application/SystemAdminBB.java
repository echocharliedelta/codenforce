/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.integration.OccupancyCacheManager;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Backing bean for system configuration and monitoring.
 * Created in July 2024 to facilitate caching controls
 * @author pierre15
 */
public class SystemAdminBB extends BackingBeanUtils{
     
    @PostConstruct
    public void initBean() {
        System.out.println("SystemAdminBB.initBean()");
        
    }
    
    
    /**
     * Listener for toggling of a cache manager control on or off
     */
    public void toggleCacheManagerActive(){
        IFaceCacheManager mngr = extractCacheManagerStringAndAcquireManager();
        if(mngr != null){
            System.out.println("SystemAdminBB.toggleCacheManagerActive | class: " + mngr.getClass());
            if(mngr.isCachingEnabled()){
                mngr.disableClientCaching();
                getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Caching enabled: " + mngr.getClass().getSimpleName(), ""));
            } else {
                mngr.enableClientCaching();
                getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Caching DISABLED: " + mngr.getClass().getSimpleName(), ""));
            }
        }
    }
    
    /**
     * listener for user requests to flush a cache
     */
    public void flushCache(){
        IFaceCacheManager mngr = extractCacheManagerStringAndAcquireManager();
        if(mngr != null){
            System.out.println("SystemAdminBB.flushCache | class: " + mngr.getClass() );
            mngr.flushAllCaches();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Cache flushed! | " + mngr.getClass().getSimpleName(), ""));
        }
    }
    
    /**
     * Listener for disable all clicks
     * @param ev 
     */
    public void disableAllCaching(ActionEvent ev){
        getSystemCoordinator().disableAllCaching();
    }
    
    /**
     * Listener for enable all clicks
     * @param ev 
     */
    public void enableAllCaching(ActionEvent ev){
        getSystemCoordinator().enableAllCaching();
        
    }

    /**
     * Tells all caches that are able to dump their estimated size
     * @param ev
     */
    public void writeCacheEstimatedSize(ActionEvent ev){
        getSystemCoordinator().writeAllCacheStats();
        getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Wrote cache sizes!", ""));
    }

    /**
     * Internal organ for pulling out the Cache manager String ID from the UI and returning
     * the actual cache manager
     * @return 
     */
    private IFaceCacheManager extractCacheManagerStringAndAcquireManager(){
         String mngrString = getFacesContext().getExternalContext().getRequestParameterMap().get("cache-manager-string");
         System.out.println("SystemAdminBB.extractCacheManagerStringAndAcquireManager | extracted: "+  mngrString);
         if(mngrString != null){
            switch (mngrString){
                case "SYSMUNIUSER" -> {
                    return getSystemMuniCacheManager();
                }
                case "CODE" -> {
                    return getCodeCacheManager();
                }
                case "PROPERTY" -> {
                    return getPropertyCacheManager();
                }
                case "PERSON" -> {
                    return getPersonCacheManager();
                }
                case "CECASE" -> {
                    return getCECaseCacheManager();
                }
                case "OCCUPANCY" -> {
                    return getOccupancyCacheManager();
                }
                case "INSPECTION" -> {
                    return getOccInspectionCacheManager();
                }
                case "EVENT" -> {
                    return getEventCacheManager();
                }
                case "BLOB" -> {
                    return getBlobCacheManager();
                }
            }
         }
         return null;
    }
    
    /**
     * System wide cache flush
     * @param ev 
     */
    public void flushAllSystemCaches(ActionEvent ev){
        SystemCoordinator sc = getSystemCoordinator();
        System.out.println("SytemAdminBB.flushAllSystemCaches ");
        sc.flushAllSystemCaches();
        getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "All caches flushed! " , ""));
    }
}
