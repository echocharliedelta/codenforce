/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.entities.HumanHeadless;
import com.tcvcog.tcvce.entities.HumanHeadlessEnum;

/**
 * Specifies that implemeneters can  emit a special object containing candidate fields
 * for searching and creating new humans
 * @author sylvia
 */
public interface IFace_humanHeadlessEmitter {
    public HumanHeadless getHeadlessHumanBundle(); 
    public HumanHeadlessEnum getHeadlessEunum(); 
    public int getHeadlessHostID();
     
}
