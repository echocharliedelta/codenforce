/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.Icon;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import java.io.Serializable;
import java.util.List;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;
import java.util.Objects;

public class IconBB extends BackingBeanUtils implements Serializable {

    private List<Icon> iconList;
    private Icon currentIcon;
    private boolean editModeIconInfo;
    private boolean iconCreateMode;
    
    /**
     * Creates a new instance of IconBB
     */
    public IconBB() {
    }
    
    @PostConstruct
    public void initBean() {
        System.out.println("Icon BB: Init");
        iconCreateMode = false;
        editModeIconInfo = false;
        refreshIconList();
        if (Objects.nonNull(iconList) && !iconList.isEmpty()) {
            currentIcon = iconList.get(0);
        }
    }

    /**
     * Loads a fresh lists of icons from the db
     */
    public void refreshIconList() {
        SystemCoordinator sc = getSystemCoordinator();
        try {
            iconList = sc.getIconList();
        } catch (IntegrationException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to load IconList",
                            "This must be corrected by the system administrator"));
        }
    }

    /**
     * Listener for user requests to view icon
     *
     * @param icon
     */
    public void viewIcon(Icon icon) {
        currentIcon = icon;
        System.out.println("View Icon: " + icon.getID());
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Viewing Icon: " + icon.getName(), ""));
    }

    public void onInsertIconCommit() {
        SystemCoordinator sc = getSystemCoordinator();
        try {
            currentIcon = sc.insertIcon(currentIcon, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Successful Insert New Icon!", ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
        }
    }

    public void onUpdateIconCommit() {
        SystemCoordinator sc = getSystemCoordinator();
        try {
            sc.updateIcon(currentIcon, getSessionBean().getSessUser());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Icon Updated with ID " + currentIcon.getID(), ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Failed to Update Icon", ""));
        }
    }

    /**
     * Listener for users to click the add or the edit button
     *
     * @param ev
     */
    public void toggleIconAddEditButtonChange(ActionEvent ev) {
        System.out.println("toggleIconAddEditButtonChange");
        if (editModeIconInfo) {
            if (Objects.nonNull(currentIcon) && currentIcon.getID() == 0) {
                onInsertIconCommit();
            } else {
                onUpdateIconCommit();
                System.out.println(currentIcon.getID());
            }
            refreshIconList();
        }
        editModeIconInfo = !editModeIconInfo;
    }

    /**
     * Begins the icon creation process
     *
     * @param ev
     */
    public void onIconAddInit(ActionEvent ev) {
        System.out.println("IconBB.onIconAddInit");
        SystemCoordinator sc = getSystemCoordinator();
        currentIcon = sc.getIconSkeleton();
        editModeIconInfo = true;
    }

    /**
     * Listener for users to cancel their icon add/edit operation
     *
     * @param ev
     */
    public void onOperationIconAddEditAbort(ActionEvent ev) {
        System.out.println("IconBB.toggleIconAddEditButtonChange: ABORT");
        editModeIconInfo = false;
        iconCreateMode = false;
    }

    /**
     * Listener for user request to start the nuking process of icon
     *
     * @param i
     */
    public void onIconNukeInitButtonChange(Icon i) {
        currentIcon = i;
    }

    public void commitRemove() {
        SystemCoordinator sc = getSystemCoordinator();
        if (currentIcon.getID() > 0) {
            try {
                int uses = sc.checkForUse(currentIcon);
                if (uses == 0) {
                    sc.deactivateIcon(currentIcon, getSessionBean().getSessUser());
                    getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Icon deactivated with ID: " + currentIcon.getID(), ""));
                } else {
                    getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO,
                                    "Icon is in use " + uses + " times. Could not remove", ""));
                }
            } catch (IntegrationException | BObStatusException ex) {
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Could not remove Icon, sorry", ""));
            }
            refreshIconList();
        } else {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Invalid IconID: " + currentIcon.getID(), ""));
        }
    }

    /**
     * @return the iconList
     */
    public List<Icon> getIconList() {
        return iconList;
    }

    /**
     * @param iconList the iconList to set
     */
    public void setIconList(List<Icon> iconList) {
        this.iconList = iconList;
    }

    /**
     * @return the currentIcon
     */
    public Icon getCurrentIcon() {
        return currentIcon;
    }

    /**
     * @param currentIcon the currentIcon to set
     */
    public void setCurrentIcon(Icon currentIcon) {
        this.currentIcon = currentIcon;
    }

    public boolean isEditModeIconInfo() {
        return editModeIconInfo;
    }

    public void setEditModeIconInfo(boolean editModeIconInfo) {
        this.editModeIconInfo = editModeIconInfo;
    }

    public boolean isIconCreateMode() {
        return iconCreateMode;
    }

    public void setIconCreateMode(boolean iconCreateMode) {
        this.iconCreateMode = iconCreateMode;
    }

}
