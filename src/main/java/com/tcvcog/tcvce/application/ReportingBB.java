/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.coordinators.UserCoordinator;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.BlobLight;
import com.tcvcog.tcvce.entities.CECase;
import com.tcvcog.tcvce.entities.CECaseDataHeavy;
import com.tcvcog.tcvce.entities.CECase;
import com.tcvcog.tcvce.entities.CasePhaseEnum;
import com.tcvcog.tcvce.entities.CaseStageEnum;
import com.tcvcog.tcvce.entities.ReportTextSizeEnum;
import com.tcvcog.tcvce.entities.RoleType;
import com.tcvcog.tcvce.entities.reports.Report;
import com.tcvcog.tcvce.entities.reports.ReportConfigCECase;
import com.tcvcog.tcvce.entities.reports.ReportConfigCECaseList;
import com.tcvcog.tcvce.entities.reports.ReportConfigCEEventList;
import com.tcvcog.tcvce.entities.reports.ReportConfigOccInspection;
import com.tcvcog.tcvce.entities.reports.ReportConfigOccPermit;
import com.tcvcog.tcvce.entities.reports.ReportConfigOccActivity;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import jakarta.annotation.PostConstruct;
import jakarta.faces.event.ActionEvent;
import java.util.Arrays;

/**
 * Container for report configuration objects for all of the reports in CNF
 * which includes notices of violations (letters), field inspection reports, 
 * occ permits, case list reports, case docket reports
 * @author sylvia
 */
public class ReportingBB extends BackingBeanUtils implements Serializable{
    
    private ReportConfigCECase reportCECase;
    private ReportConfigCECaseList reportCECaseList;
    private ReportConfigCECaseList reportCECaseListCustom;
    
    private Report currentReport;
   
    private ReportConfigCEEventList reportCEEvent;
    
    private ReportConfigOccInspection reportConfigFieldInspection;
    private ReportConfigOccPermit reportConfigOccPermit;
    private ReportConfigOccActivity reportConfigOccActivity;
    
    
    private List<ReportTextSizeEnum> textSizeCandidateList;
    private BlobLight headerImageBlob;
    private int headerImageWidthPx;
    
    private List<RoleType> roleTypeCandidateListForReportViewerFiltering;

    private Map<CasePhaseEnum, Integer> cPhaseMap;
     

    /**
     * Creates a new instance of ReportingBB
     */
    public ReportingBB() {
   
    }
    
    @PostConstruct
    public void initBean(){
        
        UserCoordinator uc = getUserCoordinator();
        roleTypeCandidateListForReportViewerFiltering = uc.auth_getPermittedRoleTypesToGrant(getSessionBean().getSessUser());
        
        headerImageBlob = getSessionBean().getSessMuni().getDefaultMuniHeaderImage();
        headerImageWidthPx = getSessionBean().getSessMuni().getDefaultMuniHeaderImageWidthPX();
        reportCECase = getSessionBean().getReportConfigCECase();
        reportCECaseList = getSessionBean().getReportConfigCECaseList();
        
        reportConfigOccPermit = getSessionBean().getReportConfigOccPermit();
        reportConfigFieldInspection = getSessionBean().getReportConfigFieldInspection();
        setReportConfigOccActivity(getSessionBean().getReportConfigOccActivity());
        
        textSizeCandidateList = Arrays.asList(ReportTextSizeEnum.values());
        
        if(reportCECase != null){
            currentReport = reportCECase;
        } else {
            currentReport = reportCECaseList;
        }
        System.out.println("ReportingBB.initBean");
        
        if(reportConfigFieldInspection != null && reportConfigFieldInspection.getInspection() != null && reportConfigFieldInspection.getInspection().getBlobList() != null && !reportConfigFieldInspection.getInspection().getBlobList().isEmpty()){
            System.out.println("ReportingBB.initBean | fin photos size: " + reportConfigFieldInspection.getInspection().getBlobList().size());
        }
        
    }
    
    /**
     * Listener for municipality report start--brings up 
     * dialog
     * @param ev 
     */
    public void muniActivityReportInit(ActionEvent ev){
        
        
    }
    
    /**
     * Listener to requests to build a monthly activity report
     * 
     * @param ev 
     */
    public void muniActivityReportGenerate(ActionEvent ev){
        
        
    }
 
    /**
     * @return the reportCEEvent
     */
    public ReportConfigCEEventList getReportCEEvent() {
        return reportCEEvent;
    }

    /**
     * @return the reportCECase
     */
    public ReportConfigCECase getReportCECase() {
        return reportCECase;
    }

    /**
     * @param reportCEEvent the reportCEEvent to set
     */
    public void setReportCEEvent(ReportConfigCEEventList reportCEEvent) {
        this.reportCEEvent = reportCEEvent;
    }

    /**
     * @param reportCECase the reportCECase to set
     */
    public void setReportCECase(ReportConfigCECase reportCECase) {
        this.reportCECase = reportCECase;
    }

    /**
     * @return the currentReport
     */
    public Report getCurrentReport() {
        currentReport = getSessionBean().getSessReport();
        return currentReport;
    }

    /**
     * @param currentReport the currentReport to set
     */
    public void setCurrentReport(Report currentReport) {
        
        this.currentReport = currentReport;
    }

    /**
     * @return the reportCECaseList
     */
    public ReportConfigCECaseList getReportCECaseList() {
        return reportCECaseList;
    }

    /**
     * @param reportCECaseList the reportCECaseList to set
     */
    public void setReportCECaseList(ReportConfigCECaseList reportCECaseList) {
        this.reportCECaseList = reportCECaseList;
    }

 
   

    /**
     * @return the cPhaseMap
     */
    public Map<CasePhaseEnum, Integer> getcPhaseMap() {
        return cPhaseMap;
    }

    /**
     * @param cPhaseMap the cPhaseMap to set
     */
    public void setcPhaseMap(Map<CasePhaseEnum, Integer> cPhaseMap) {
        this.cPhaseMap = cPhaseMap;
    }

   
   

    /**
     * @return the reportConfigFieldInspection
     */
    public ReportConfigOccInspection getReportConfigFieldInspection() {
        return reportConfigFieldInspection;
    }

    /**
     * @param reportConfigFieldInspection the reportConfigFieldInspection to set
     */
    public void setReportConfigFieldInspection(ReportConfigOccInspection reportConfigFieldInspection) {
        this.reportConfigFieldInspection = reportConfigFieldInspection;
    }

    /**
     * @return the reportConfigOccPermit
     */
    public ReportConfigOccPermit getReportConfigOccPermit() {
        return reportConfigOccPermit;
    }

    /**
     * @param reportConfigOccPermit the reportConfigOccPermit to set
     */
    public void setReportConfigOccPermit(ReportConfigOccPermit reportConfigOccPermit) {
        this.reportConfigOccPermit = reportConfigOccPermit;
    }

    /**
     * @return the reportCECaseListCustom
     */
    public ReportConfigCECaseList getReportCECaseListCustom() {
        return reportCECaseListCustom;
    }

    /**
     * @param reportCECaseListCustom the reportCECaseListCustom to set
     */
    public void setReportCECaseListCustom(ReportConfigCECaseList reportCECaseListCustom) {
        this.reportCECaseListCustom = reportCECaseListCustom;
    }

    /**
     * @return the headerImageBlob
     */
    public BlobLight getHeaderImageBlob() {
        return headerImageBlob;
    }

    /**
     * @param headerImageBlob the headerImageBlob to set
     */
    public void setHeaderImageBlob(BlobLight headerImageBlob) {
        this.headerImageBlob = headerImageBlob;
    }

    /**
     * @return the headerImageWidthPx
     */
    public int getHeaderImageWidthPx() {
        return headerImageWidthPx;
    }

    /**
     * @param headerImageWidthPx the headerImageWidthPx to set
     */
    public void setHeaderImageWidthPx(int headerImageWidthPx) {
        this.headerImageWidthPx = headerImageWidthPx;
    }

    /**
     * @return the reportConfigOccActivity
     */
    public ReportConfigOccActivity getReportConfigOccActivity() {
        return reportConfigOccActivity;
    }

    /**
     * @param reportConfigOccActivity the reportConfigOccActivity to set
     */
    public void setReportConfigOccActivity(ReportConfigOccActivity reportConfigOccActivity) {
        this.reportConfigOccActivity = reportConfigOccActivity;
    }

    /**
     * @return the roleTypeCandidateListForReportViewerFiltering
     */
    public List<RoleType> getRoleTypeCandidateListForReportViewerFiltering() {
        return roleTypeCandidateListForReportViewerFiltering;
    }

    /**
     * @param roleTypeCandidateListForReportViewerFiltering the roleTypeCandidateListForReportViewerFiltering to set
     */
    public void setRoleTypeCandidateListForReportViewerFiltering(List<RoleType> roleTypeCandidateListForReportViewerFiltering) {
        this.roleTypeCandidateListForReportViewerFiltering = roleTypeCandidateListForReportViewerFiltering;
    }

    /**
     * @return the textSizeCandidateList
     */
    public List<ReportTextSizeEnum> getTextSizeCandidateList() {
        return textSizeCandidateList;
    }

    /**
     * @param textSizeCandidateList the textSizeCandidateList to set
     */
    public void setTextSizeCandidateList(List<ReportTextSizeEnum> textSizeCandidateList) {
        this.textSizeCandidateList = textSizeCandidateList;
    }

   
    
}
