/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.CitationStatus;
import com.tcvcog.tcvce.entities.CitationViolationStatusEnum;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Backing bean for citations status management
 * 
 * @author pierre15
 */
public class CitationStatusManageBB extends BackingBeanUtils {
    
    private List<CitationStatus> statusList;
    private CitationStatus currentCitationStatus;
    private List<CitationViolationStatusEnum> citationViolationStatusList;
    
    
    private boolean editModeCitationStatus;
    private boolean showInactiveCitationStatuses;
    
    @PostConstruct
    public void initBean(){
        System.out.println("CitationStatusManageBB.initBean()");
            showInactiveCitationStatuses = false;
            citationViolationStatusList = Arrays.asList(CitationViolationStatusEnum.values());
            refreshStatusList();
            if(statusList != null && !statusList.isEmpty()){
                currentCitationStatus = statusList.get(0);
            }
        
    }
    
    /**
     * Fetches a new copy of the master list
     */
    public void refreshStatusList() {
        try {
            CaseCoordinator cc = getCaseCoordinator();
            statusList = cc.citation_getCitationStatusList(showInactiveCitationStatuses);
        } catch (IntegrationException | BObStatusException  ex) {
            System.out.println(ex);
        }
        
    }
    
    /**
     * Listener for user toggles the show inactive checkbox
     * @param ev 
     */
    public void onToggleShowInactive(){
            refreshStatusList();
    }
    
    /**
     * Listener for user requests to view a citations status
     * @param status 
     */
    public void onViewCitationStatus(CitationStatus status){
        System.out.println("CitationStatusManageBB.onViewCitationStatus | StatusID: " + status.getStatusID());
        currentCitationStatus = status;
    }
    
    
     public void toggleStatusAddEditButtonChange(ActionEvent ev) throws BObStatusException, AuthorizationException {
        System.out.println("CitationStatusManageBB.toggleStatusAddEditButtonChange");
        if (editModeCitationStatus) {
            if (Objects.nonNull(currentCitationStatus) && currentCitationStatus.getStatusID() == 0) {
                onInsertCitationStatusCommit();
            } else {
                onUpdateCitationStatusCommit();
            }
            refreshStatusList();
        }
        editModeCitationStatus = !editModeCitationStatus;
    }

    /**
     * Begins the OccInspection Cause creation process
     *
     * @param ev
     */
    public void onCitationStatusAddInit(ActionEvent ev) {
        System.out.println("CitationStatusManageBB.onCitationStatusAddInit");
        CaseCoordinator cc = getCaseCoordinator();
        currentCitationStatus = cc.citation_getCitationStatusSkeleton();
        editModeCitationStatus = true;
    }

    /**
     * inserter for citation status objects
     * @throws AuthorizationException 
     */
    public void onInsertCitationStatusCommit() throws AuthorizationException {
        CaseCoordinator cc = getCaseCoordinator();
        try {
            int freshid = cc.citation_insertCitationStatus(currentCitationStatus, getSessionBean().getSessUser());
            currentCitationStatus = cc.citation_getCitationStatus(freshid);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Successful Insert new citation status!", ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
        }
    }

    /**
     * Update citation status objects
     */
    public void onUpdateCitationStatusCommit() {
        CaseCoordinator cc = getCaseCoordinator();
        try {
            cc.citation_updateCitationStatus(currentCitationStatus, getSessionBean().getSessUser());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "OccInspection Cause updated with Id " + currentCitationStatus.getStatusID(), ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Failed to Update OccInspection Cause", ""));
        }
    }


    /**
     * Listener for users to cancel their OccInspection Cause add/edit operation
     *
     * @param ev
     */
    public void onOperationOccCauseAddEditAbort(ActionEvent ev) {
        System.out.println("CitationStatusManageBB.onOperationOccCauseAddEditAbort");
        editModeCitationStatus = false;
    }

    /**
     * Listener for user request to start the nuking process of a OccInspectionCause
     *
     * @param status
     */
    public void onOccInspectionCauseNukeInitButtonChange(CitationStatus status) {
        currentCitationStatus = status;
    }

    /**
     * Listener for user requests to commit a OccInspection Cause nuke operation
     *
     * @param status
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public void onStatusDeacCommitButtonChange(CitationStatus status) throws AuthorizationException, BObStatusException {
        currentCitationStatus = status;
        
        CaseCoordinator cc = getCaseCoordinator();
        try {
            cc.citation_deactivateCitationStatus(currentCitationStatus, getSessionBean().getSessUser());
            refreshStatusList();
            
            if (Objects.nonNull(statusList) && !statusList.isEmpty()) {
                currentCitationStatus = statusList.get(0);
            } else {
                currentCitationStatus = null;
            }
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
        }
    }
    
    // ****************** GETTERS AND SETTERS **********************************

    /**
     * @return the statusList
     */
    public List<CitationStatus> getStatusList() {
        return statusList;
    }

    /**
     * @param statusList the statusList to set
     */
    public void setStatusList(List<CitationStatus> statusList) {
        this.statusList = statusList;
    }

    /**
     * @return the currentCitationStatus
     */
    public CitationStatus getCurrentCitationStatus() {
        return currentCitationStatus;
    }

    /**
     * @param currentCitationStatus the currentCitationStatus to set
     */
    public void setCurrentCitationStatus(CitationStatus currentCitationStatus) {
        this.currentCitationStatus = currentCitationStatus;
    }

    /**
     * @return the editModeCitationStatus
     */
    public boolean isEditModeCitationStatus() {
        return editModeCitationStatus;
    }

    /**
     * @param editModeCitationStatus the editModeCitationStatus to set
     */
    public void setEditModeCitationStatus(boolean editModeCitationStatus) {
        this.editModeCitationStatus = editModeCitationStatus;
    }

    /**
     * @return the showInactiveCitationStatuses
     */
    public boolean isShowInactiveCitationStatuses() {
        return showInactiveCitationStatuses;
    }

    /**
     * @param showInactiveCitationStatuses the showInactiveCitationStatuses to set
     */
    public void setShowInactiveCitationStatuses(boolean showInactiveCitationStatuses) {
        this.showInactiveCitationStatuses = showInactiveCitationStatuses;
    }

    /**
     * @return the citationViolationStatusList
     */
    public List<CitationViolationStatusEnum> getCitationViolationStatusList() {
        return citationViolationStatusList;
    }

    /**
     * @param citationViolationStatusList the citationViolationStatusList to set
     */
    public void setCitationViolationStatusList(List<CitationViolationStatusEnum> citationViolationStatusList) {
        this.citationViolationStatusList = citationViolationStatusList;
    }
    
    
    
}
