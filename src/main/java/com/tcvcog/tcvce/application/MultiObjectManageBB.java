package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.entities.MultiObjectTypeEnum;
import jakarta.annotation.PostConstruct;
import jakarta.faces.event.ValueChangeEvent;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class MultiObjectManageBB implements Serializable {

    private MultiObjectTypeEnum[] objectOptions;
    private MultiObjectTypeEnum selectedObject;

    @PostConstruct
    public void initBean() {
        objectOptions = MultiObjectTypeEnum.values();
        selectedObject = objectOptions[0];
    }

    public void handleObjectChange(ValueChangeEvent event) {
        selectedObject = (MultiObjectTypeEnum) event.getNewValue();
        System.out.println("Selected Object: " + selectedObject.getValue());
    }

    public MultiObjectTypeEnum getSelectedObject() {
        return selectedObject;
    }

    public void setSelectedObject(MultiObjectTypeEnum selectedObject) {
        this.selectedObject = selectedObject;
    }

    public MultiObjectTypeEnum[] getObjectOptions() {
        return objectOptions;
    }

    public List<MultiObjectTypeEnum> getObjectOptionValues() {
        return Arrays.asList(objectOptions);
    }

    public String getIncludePath() {
        return selectedObject.getIncludePath();
    }
}
    