/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.SearchCoordinator;
import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.coordinators.PermissionsCoordinator;
import com.tcvcog.tcvce.coordinators.PropertyCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.domain.SearchException;
import com.tcvcog.tcvce.entities.CEARProcessingRouteEnum;
import com.tcvcog.tcvce.entities.CEActionRequest;
import com.tcvcog.tcvce.entities.CEActionRequestIssueType;
import com.tcvcog.tcvce.entities.CEActionRequestPropertyHeavy;
import com.tcvcog.tcvce.entities.CEActionRequestStatus;
import com.tcvcog.tcvce.entities.CECase;
import com.tcvcog.tcvce.entities.FocusedObjectEnum;
import com.tcvcog.tcvce.entities.HumanLink;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.NoteScopeEnum;
import com.tcvcog.tcvce.entities.Person;
import com.tcvcog.tcvce.entities.Property;
import com.tcvcog.tcvce.entities.PropertyDataHeavy;
import com.tcvcog.tcvce.entities.reports.ReportCEARList;
import com.tcvcog.tcvce.entities.search.QueryCEAR;
import com.tcvcog.tcvce.entities.search.QueryCEAREnum;
import com.tcvcog.tcvce.entities.search.SearchParamsCEActionRequests;
import com.tcvcog.tcvce.integration.CEActionRequestIntegrator;
import com.tcvcog.tcvce.occupancy.application.OccPeriodBB;
import com.tcvcog.tcvce.util.Constants;
import com.tcvcog.tcvce.util.MessageBuilderParams;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.faces.event.ActionEvent;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * SUPPORTS PUBLIC FACING FACES PAGES!
 * 
 * Objects on this bean are wired up to the CE action request flow which
 * does not require authentication.
 * 
 * Primary backing bean for managing all code enforcement action requests.
 * Contains four methods, each of which routes a selected request down a
 * workflow pathway and adjusts the request object's status accordingly.
 *
 * Also contains utility methods for manipulating and editing requests.
 *
 * @author Ellen Bascomb of Apartment 31Y
 */
public class CEActionRequestsBB extends BackingBeanUtils implements Serializable {

    final static int CEAR_REFRESH_WINDOW_MINS = 5;
    
    private CEActionRequestPropertyHeavy selectedRequest;
    private List<CEActionRequestPropertyHeavy> requestListSearch;
    private List<CEActionRequestPropertyHeavy> requestListUnprocessed;

    private boolean editModeCEARProperty;
    private boolean editModeCEARPersonLink;
    private boolean renderCEAREditingComponents;
    
    private ReportCEARList reportConfig;
    
    private List<CEARProcessingRouteEnum> processingRouteList;
    private boolean selectedRequestAwaitingInitialRouting;
    private CEARProcessingRouteEnum selectedRoute;
    private String routeSpecificDialogVar;
    private String routeSpecificComponentsForUpdate;
    
    private PropertyDataHeavy selectedRequestPropertyDH;
    
    private List<QueryCEAR> queryList;
    private QueryCEAR selectedQueryCEAR;
    private SearchParamsCEActionRequests searchParamsSelected;
    private List<CEActionRequestIssueType> issueTypeList;
    private List<CEActionRequestStatus> statusList;
    

    private CEActionRequestStatus selectedChangeToStatus;
    private String invalidMessage;
    private String noViolationFoundMessage;
    
    private String formNoteText;
    private NoteScopeEnum noteScope;

    private Person selectedPersonForAttachment;
    private List<CECase> caseListForSelectedProperty;
    private CECase selectedCaseForAttachment;

    private int ceCaseIDForConnection;
    private boolean disablePACCControl;
    private boolean disabledDueToRoutingNotAllowed;
    
    private boolean permissionAllowCEARRoutingAndEditing;
    private boolean permissionAllowCEARDeactivation;

    /**
     * Creates a new instance of ActionRequestManageBB
     */
    public CEActionRequestsBB() {

    }

    @PostConstruct
    public void initBean() {
        System.out.println("CEACtionRequestBB.initBean");
        PermissionsCoordinator permCoor = getPermissionsCoordinator();
        SearchCoordinator sc = getSearchCoordinator();
          //First search for CEARs using a standard query
        queryList = sc.buildQueryCEARList(getSessionBean().getSessUser().getMyCredential());
        selectedQueryCEAR = sc.initQuery(QueryCEAREnum.UNPROCESSED, getSessionBean().getSessUser().getMyCredential());
        searchParamsSelected = selectedQueryCEAR.getPrimaryParams();

        // upon dumping on dashboard this should be the unprocessed CEAR query results
        // from the session init query run
        requestListSearch = getSessionBean().getSessCEARListUnprocessed();
        selectedRequest = getSessionBean().getSessCEAR();
        
        refreshSelectedRequest();
        
        permissionAllowCEARRoutingAndEditing = permCoor.verifyGeneralEditFloor(getSessionBean().getSessUser());
        permissionAllowCEARDeactivation = permCoor.permissionsCheckpointSystemAdminRank(getSessionBean().getSessUser());
        
        evaulateSelectedRequestRoutingStatusAndUpdateRouteList();
        setupQueryLists();
        configureCEAREditFunctionRendering();
        updateCEARUnprocessedList(false);
    }
    
    /**
     * Asks session bean for its CEAR unprocssed query and checks if is more than 5 mins old or null,
     * in which cases the query is re-run. This method also resets this view-scoped bean's
     * query to unprocssed
     */
    private void updateCEARUnprocessedList(boolean requireQuery){
        SearchCoordinator sc = getSearchCoordinator();
        CaseCoordinator cc = getCaseCoordinator();
        if(sc.determineQueryExpiryRefresh(getSessionBean().getQueryCEARUnprocessed(), CEAR_REFRESH_WINDOW_MINS) || requireQuery){
           getSessionBean().refreshCEARUnprocessedList();
        }
    }
    
    /**
     * Asks the session bean if the current page is the dash, which
     * means we'll allow CEAR rendering, otherwise, turn off the components
     * that would adjust CEAR ojects. 
     * 
     * This is the first example of a method that uses the sessionFocusObject
     * enum trick
     */
    private void configureCEAREditFunctionRendering(){
        if(getSessionBean().getSessionDomain() != null){
            if(getSessionBean().getSessionDomain() == FocusedObjectEnum.MUNI_DASHBOARD){
                renderCEAREditingComponents = true;
            }
        } else {
            renderCEAREditingComponents = false;
        }
        System.out.println("CEActionRequestsBB.configureCEAREditFunctionRendering | " + renderCEAREditingComponents);
    }
    
   
    /**
     * Loads up our candidate list for query params
     */
    private void setupQueryLists(){
        CaseCoordinator cc = getCaseCoordinator();
        try {
            issueTypeList = cc.cear_getIssueTypes(null, false);
            statusList = cc.getCEARStatusList();
        } catch (IntegrationException ex) {
            System.out.println(ex);
        }
    }
    
    /**
     * Checks current request against the sentinel status of unrouted and sets the
     * backing bean flag accordingly
     */
    private void evaulateSelectedRequestRoutingStatusAndUpdateRouteList(){
        // Populate our routing options
        processingRouteList = new ArrayList<>();
        processingRouteList.add(CEARProcessingRouteEnum.ATTACH_TO_EXISTING_CASE);
        processingRouteList.add(CEARProcessingRouteEnum.ATTACH_TO_NEW_CASE);
        processingRouteList.add(CEARProcessingRouteEnum.INVALID_REQUEST);
        processingRouteList.add(CEARProcessingRouteEnum.NO_VIOLATION_FOUND);
        
        // Determine if our current request has been routed or not, and adjust the route list
        // accordingly
        if(selectedRequest != null && selectedRequest.getRequestStatus() != null){
            if(selectedRequest.getRequestStatus().getStatusID() == Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                                .getString(CEARProcessingRouteEnum.UNPROCESSED.getCEAR_STATUS_STRING_FOR_DB_KEY_LOOKUP()))){
                selectedRequestAwaitingInitialRouting = true;
                // build list that only contains non-initial state 
            } else {
                // if we're re-routing, add back the initial state if users
                // want to kick the CEAR back to the start
                processingRouteList.add(CEARProcessingRouteEnum.UNPROCESSED);
                selectedRequestAwaitingInitialRouting = false;
            }
        }
    }
    
    /**
     * Listener for user requests to start or stop the property edit mode proecss
     * @param ev 
     */
    public void onToggleCEARPropertyEditMode(ActionEvent ev){
        CaseCoordinator cc = getCaseCoordinator();
        System.out.println("CEActionRequestsBB.toggleCEARPropertyEditMode");
        if(editModeCEARProperty){
            if(selectedRequest != null && getSessionBean().getSessProperty() != null){
                try {
                    cc.cear_updateCEARProperty(selectedRequest, getSessionBean().getSessProperty(), getSessionBean().getSessUser());
                    refreshSelectedRequest();
                    updateCEARUnprocessedList(true);
                    getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_INFO,
                               "Success! Action request is now linked to property: " + getSessionBean().getSessProperty().getAddress().getAddressPretty1Line(), ""));
                } catch (IntegrationException | BObStatusException ex) { 
                    System.out.println("ex");
                    getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_ERROR,
                               ex.getMessage(), ""));
                } 
            } else {
                 getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error encountered updating CEAR property", ""));
            }
        }
        editModeCEARProperty = !editModeCEARProperty;
    }
    
    /**
     * Special getter wrapper around the cease's human link list that can be managed
     * by a shared UI for person link management and search
     * @return the current Case's human link list
     */
    public List<HumanLink> getManagedCEARHumanLinkList(){
        List<HumanLink> hll = getSessionBean().getSessHumanListRefreshedList();
        if(hll != null){
            selectedRequest.sethumanLinkList(hll);
            getSessionBean().setSessHumanListRefreshedList(null);
        }
        return selectedRequest.gethumanLinkList();
    }
    
    /**
     * listener for user requests to transfer the person named in the selected CEAR
     * to the person search form and run that search
     * @param ev 
     */
    public void onTransferNameToPersonSearch(ActionEvent ev){
        System.out.println("CEActionRequestsBB.onTransferNameToPersonSearch ");
        if(selectedRequest != null && selectedRequest.getRequestorName() != null && !selectedRequest.getRequestorName().equals(Constants.EMPTY_STRING)){
            System.out.println("CEActionRequestsBB.onTransferNameToPersonSearch | found: " + selectedRequest.getRequestorName());
            getSessionBean().setPersonNameForSearch(selectedRequest.getRequestorName());
        }
    }
    
    /**
     * Listener for user requests to abort the property edit mode
     * @param ev 
     */
    public void onCEARPropertyEditModeAbort(ActionEvent ev){
        System.out.println("CEActionRequestsBB.onCEARPropertyEditModeAbort");
        editModeCEARProperty = false;
        getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_INFO,
                               "CEAR property link abort", ""));
    }
    
    
    /**
     * Listener for user requests to start or stop the person linking process
     * @Deprecated by EGW: no use case for linking a CEAR to a person
     * @param ev 
     */
    public void onToggleCEARPersonLinkMode(ActionEvent ev){
        System.out.println("CEActionRequestsBB.toggleCEARPersonLinkMode");
        CaseCoordinator cc = getCaseCoordinator();
        if(editModeCEARPersonLink){
            if(selectedRequest != null && getSessionBean().getSessPerson()!= null){
                selectedRequest.setParcelKey(getSessionBean().getSessProperty().getParcelKey());    

                try {
                    cc.cear_updateCEAR(selectedRequest, getSessionBean().getSessUser(), null);
                    refreshSelectedRequest();
                    getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_INFO,
                               "Success! Action request is now linked to property: " + getSessionBean().getSessProperty().getAddress().getAddressPretty1Line(), ""));
                } catch (IntegrationException | BObStatusException ex) {
                    System.out.println("ex");
                    getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_ERROR,
                               ex.getMessage(), ""));
                } 
            } else {
                 getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error encountered updating CEAR property", ""));
            }
        }
        editModeCEARPersonLink = !editModeCEARPersonLink;
    }
    
    /**
     * Listener for user requests to abort the person linking process
     * @param ev 
     */
    public void onCEARPersonLinkEditModeAbort(ActionEvent ev){
        System.out.println("CEActionRequestsBB.onCEARPersonLinkEditModeAbort");
        editModeCEARPersonLink = false;
         getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_INFO,
                               "CEAR person link abort", ""));
    }


  

    /**
     * Coordinates searches against the ceactionrequest table
     * @param ev 
     */
    public void executeQuery(ActionEvent ev) {
        SearchCoordinator searchC = getSearchCoordinator();
        CaseCoordinator cc = getCaseCoordinator();
//        selectedQueryCEAR = searchC.initQuery(selectedQueryCEAR.getQueryName(), getSessionBean().getSessUser().getMyCredential());
        requestListSearch = new ArrayList<>();

        try {
            if (selectedQueryCEAR != null && !selectedQueryCEAR.getParamsList().isEmpty()) {
                searchC.runQuery(selectedQueryCEAR);
            } 

            if (selectedQueryCEAR != null && !selectedQueryCEAR.getBOBResultList().isEmpty()) {
                requestListSearch.addAll(cc.cear_getCEARPropertyHeavyList(selectedQueryCEAR.getBOBResultList()));
            }
            if(requestListSearch != null){
                System.out.println("CEActionRequestsBB.executeQuery | result list: " + requestListSearch.size());
            } else {
                System.out.println("CEActionRequestsBB.executeQuery | NULL result list: ");
            }
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Your query completed with " + requestListSearch.size() + " results!", ""));
        } catch (SearchException ex) {
            System.out.println("CEActionRequestsBB.executeQuery() | ERROR: " + ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to query action requests, sorry", ""));
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }
    }
    
    /**
     * Listener for user requests to view property associated with CEAR
     * @param cearph
     * @return 
     */
    public String onViewCEARProperty(CEActionRequestPropertyHeavy cearph){
        if(cearph != null && cearph.getRequestProperty() != null){
            try {
                return getSessionBean().navigateToPageCorrespondingToObject(cearph.getRequestProperty());
            } catch (BObStatusException | AuthorizationException ex) {
                System.out.println(ex);
                 getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                            ex.getMessage(), ""));
                return "";
            }
        }
        return "";
    }
    
    
    /**
     * listener for user reset of selected query
     * @param ev 
     */
     public void resetQuery(ActionEvent ev) {
        System.out.println("CEActionRequestsBB.resetQuery");
        SearchCoordinator sc = getSearchCoordinator();
        selectedQueryCEAR = sc.initQuery(selectedQueryCEAR.getQueryName(), getSessionBean().getSessUser().getKeyCard());
        requestListSearch = new ArrayList<>();
        configureSearchParameters();
        
    }
     
     /**
      * injects the current search params from the selected query
      */
     private void configureSearchParameters(){
         if(selectedQueryCEAR != null && selectedQueryCEAR.getParamsList() != null && !selectedQueryCEAR.getParamsList().isEmpty()){
             searchParamsSelected = selectedQueryCEAR.getPrimaryParams();
         }
         
     }
     
     /**
      * Listener on user change of the selected query drop down box
      */
     public void changeQuerySelected(){
         System.out.println("CEActionRequestsBB.changeQuerySelected");
         configureSearchParameters();
     }
  
    /**
     * Asks the request list for its size. For some reason, calling size() on 
     * a faces page either doesn't work or doesn't work consistently, even with
     * a form update
     * @return 
     */
    public int getRequestListSize(){
        if(requestListSearch != null){
            return requestListSearch.size();
        }
        else{
            return -9;
        }
    }
    
   
     /**
     * Listener for user requests to start viewing or updating the chosen CEAR
     * @param req 
     */
    public void manageActionRequest(CEActionRequest req) {
        CaseCoordinator cc = getCaseCoordinator();
        if(req != null){
            System.out.println("CEActionRequestsBB.manageActionRequest req: " + req.getRequestID());
            try {
                selectedRequest = cc.cear_getCEARPropertyHeavy(req);
                
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        ex.getMessage(),""));
            } 
            refreshSelectedRequest();
            evaulateSelectedRequestRoutingStatusAndUpdateRouteList();
            getSessionBean().setSessCEAR(selectedRequest);
        }
    }
    

    /**
     * Asks db for a fresh copy of the selected request
     */
    private void refreshSelectedRequest(){
        CaseCoordinator cc  = getCaseCoordinator();
        PropertyCoordinator pc = getPropertyCoordinator();
        if(selectedRequest != null && selectedRequest.getRequestID() != 0){
            try {
                selectedRequest = cc.cear_getCEARPropertyHeavy(cc.cear_getCEActionRequest(selectedRequest.getRequestID()));
                // now make sure our propertyDH matches the request
                selectedRequestPropertyDH = pc.assemblePropertyDataHeavy(selectedRequest.getRequestProperty(), getSessionBean().getSessUser());
            } catch (IntegrationException | BObStatusException | BlobException | SearchException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        ex.getMessage(),""));
            } 
        }
    }
    
    /**
     * Listener for user requests to view the property associated with this CEAR, if any
     * @return 
     */
    public String onViewRequestproperty(){
        if(selectedRequest != null && selectedRequest.getRequestProperty() != null){
            getSessionBean().setSessProperty(selectedRequest.getRequestProperty());
            try {
                return getSessionBean().navigateToPageCorrespondingToObject(getSessionBean().getSessProperty());
            } catch (BObStatusException | AuthorizationException ex) {
                System.out.println(ex);
            } 
         }
        return "";
    }
    
    /**
     * Listener for user requests to start the deac process on a CEAR
     * @param ev 
     */
    public void onDeactivateInit(ActionEvent ev){
        if(selectedRequest != null){
            System.out.println("CEActionRequestsBB.onDeactivateInit | requestID " + selectedRequest.getRequestID());
        }
    }
    
    
    /**
     * Listener for user requests to start the deac process on a CEAR
     * @param ev 
     */
    public void onDeactivateAbort(ActionEvent ev){
        System.out.println("CEActionRequestsBB.onDeactivateAbort");
    }
    
    /**
     * Listener for user requests to finalize the deac process of the selected request
     * @param ev 
     */
    public void onDeactivateCommit(ActionEvent ev){
        System.out.println("CEActionRequestsBB.onDeactivateCommit");
        CaseCoordinator cc = getCaseCoordinator();
        try {
            if(selectedRequest != null){
                cc.cear_deactivateCEAR(selectedRequest, getSessionBean().getSessUser());
                refreshSelectedRequest();
                updateCEARUnprocessedList(true);
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Action request deactivated with ID: " + selectedRequest.getRequestID(),""));
            } else {
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                           "Cannot deac a null CEAR!",""));
                
            }
        } catch (BObStatusException | AuthorizationException | IntegrationException ex) {
             getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        ex.getMessage(),""));
        } 
        
    }
    
    /**
     * Listener for user requests to start the CEAR routing process
     * @param ev 
     */
    public void onCEARRouteInit(ActionEvent ev){
        if(selectedRoute != null){
            System.out.println("CEActionRequestsBB.onCEARRouteInit | selected route: " + selectedRoute.getTITLE());
            routeSpecificDialogVar = selectedRoute.getPAGE_DIALOG_ID();
            routeSpecificComponentsForUpdate = selectedRoute.getCOMPONENTS_TO_UPDATE();
        } 
        
    }
    
     /**
     * Listener for user completion of the CEAR route selection process;
     * Based on the selection, I'll release the next dialog to show the user
     * @param ev 
     */
    public void onCEARRouteSelectButtonChange(ActionEvent ev){
        System.out.println("CEActionRequestsBB.onCEARRouteSelectButtonChange");
        if(selectedRoute != null && selectedRequest != null){
            switch(selectedRoute){
                case ATTACH_TO_EXISTING_CASE -> {
                    System.out.println("Attaching to existing case");
                    if(selectedRequest != null & selectedRequest.getRequestProperty() != null){
                        getSessionBean().setSessProperty(selectedRequest.getRequestProperty());
                    } else {
                         getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "This action request is not linked to a property, so a case on that property cannot be created."
                                + "Cancel this operation and connect the request to a single property of conern",""));
                    }
                }
                case ATTACH_TO_NEW_CASE -> {
                    System.out.println("Attaching to new case");
                    if(selectedRequest.getRequestProperty() != null){
                        getSessionBean().setSessProperty(selectedRequest.getRequestProperty());
                        // setup our sessionBean to know that once the new case
                        // is created, we want to link this CEAR to that case
                        // which will then be visible from that case
                        getSessionBean().setSessCEAR(selectedRequest);
                        getSessionBean().setActiveCEARProcessingRoute(CEARProcessingRouteEnum.ATTACH_TO_NEW_CASE);
                        
                        // trigger case add init on our cecase add bb
                        FacesContext context = FacesContext.getCurrentInstance();
                        CECaseAddBB cecaseAddBB = (CECaseAddBB) context.getApplication().evaluateExpressionGet(context, "#{ceCaseAddBB}", CECaseAddBB.class);
                        cecaseAddBB.onCECaseAddInitButtonChange(null);
                        
                        
                        System.out.println("CEActionRequestsBB.onCEARRouteSelectButtonChange | session property now: " + getSessionBean().getSessProperty().getAddress().getAddressPretty1Line());
                    } else {
                         getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "This action request is not linke to a property, so a case on that property cannot be created."
                                + "Cancel this operation and connect the request to a single property of conern",""));
                    }
                }
                case INVALID_REQUEST -> System.out.println("CEActionRequestsBB.onCEARRouteSelectButtonChange | invalid request | no operation");
                case NO_VIOLATION_FOUND -> System.out.println("CEActionRequestsBB.onCEARRouteSelectButtonChange | no violation request | no operation");
                case UNPROCESSED -> System.out.println("CEActionRequestsBB.onCEARRouteSelectButtonChange | reset cear | no operation");
                default -> {
                    System.out.println("CEAR default route ");
                }
            }
        }
    }

    /**
     * Listener for user requests to navigate to the case to which a given CEAR is attached
     * @return 
     */
    public String goToCase() {

        CaseCoordinator cc = getCaseCoordinator();
        try {
            CECase cse = cc.cecase_getCECase(selectedRequest.getCaseID(), getSessionBean().getSessUser());

            getSessionBean().setSessCECase(cc.cecase_assembleCECaseDataHeavy(cse, getSessionBean().getSessUser()));

            return getSessionBean().navigateToPageCorrespondingToObject(getSessionBean().getSessCECase());
        } catch (BObStatusException | IntegrationException | SearchException | AuthorizationException ex) {
            System.out.println("CEActionRequestsBB.goToCase() | ERROR: " + ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "An error occured while trying to redirect you to the CE Case Workflow.", ""));
            return "";
        }
    }

    /**
     * NO reporting implemented in May 2023 Incarnation
     * @Deprecated
     * @param ev 
     */
    public void prepareReportMultiCEAR(ActionEvent ev) {
        CaseCoordinator cc = getCaseCoordinator();
        SearchCoordinator searchCoord = getSearchCoordinator();

        reportConfig = cc.report_getInitializedReportConficCEARs(
                getSessionBean().getSessUser(), getSessionBean().getSessMuni());

        reportConfig.setPrintFullCEARQueue(true);
        if (selectedQueryCEAR != null) {
            //go run the Query if it hasn't been yet
            if (selectedQueryCEAR.getExecutionTimestamp() == null) {
                try {
                    selectedQueryCEAR = searchCoord.runQuery(selectedQueryCEAR);
                } catch (SearchException ex) {
                    System.out.println(ex);
                }
            }
            reportConfig.setTitle("Code enforcement requests: " + selectedQueryCEAR.getQueryName().getTitle());
            reportConfig.setNotes(selectedQueryCEAR.getQueryName().getDesc());
            reportConfig.setBOBQuery(selectedQueryCEAR);
        }
    }
  /**
     * NO reporting implemented in May 2023 Incarnation
     * @Deprecated
     * @return nav String
     */
    public String generateReportSingleCEAR() {
        getSessionBean().setSessCEAR(selectedRequest);
        getSessionBean().setSessReport(reportConfig);
        getSessionBean().setQueryCEARUnprocessed(selectedQueryCEAR);
        return "reportCEARList";
    }
    
  /**
     * NO reporting implemented in May 2023 Incarnation
     * @return nav string 
     * @Deprecated
     */
    public String generateReportMultiCEAR() {
        getSessionBean().setSessCEAR(selectedRequest);

        // Not working
//        Collections.sort(reportConfig.getBOBQuery().getBOBResultList());
        // tell the first request in the list to not print a page break before itself
        reportConfig.getBOBQuery().getBOBResultList().get(0).setInsertPageBreakBefore(false);

        getSessionBean().setSessReport(reportConfig);
        getSessionBean().setQueryCEARUnprocessed(selectedQueryCEAR);
        return "reportCEARList";
    }
    
    /**
     * Sets the request back to its initial state
     */
    public void path0ResetRequestToNotRouted(ActionEvent ev){
         CaseCoordinator cc = getCaseCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        PropertyCoordinator pc = getPropertyCoordinator();

        if (selectedRequest != null) {
            try {

                MessageBuilderParams mbp = new MessageBuilderParams(selectedRequest.getPublicExternalNotes());
                mbp.setUser(getSessionBean().getSessUser());
                mbp.setHeader(getResourceBundle(Constants.MESSAGE_TEXT).getString("cear_reset_header"));
                mbp.setNewMessageContent("");

                selectedRequest.setPublicExternalNotes(sc.appendNoteBlock(mbp));

                // force the bean to go to the integrator and fetch a fresh, updated
                // list of action requests
                cc.cear_updateCEAR(selectedRequest, getSessionBean().getSessUser(), CEARProcessingRouteEnum.UNPROCESSED);
                updateCEARUnprocessedList(true);
            } catch (IntegrationException  | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Unable to update action request with case attachment notes", ""));
            } 
        } else {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Please select an action request from the table to open a new case", ""));
        }

        getSessionBean().setSessCEAR(selectedRequest);
        //Here's a navstack to guide the user back after they add a case:
    }
    

    /**
     * Listener for CEAR routing pathway, which will turn over
     * control to the new case dialog, which will dump the user on the 
     * new case page not on cear standard
     * @param ev
     */
    public void path1CreateNewCaseAtProperty(ActionEvent ev) {
        CaseCoordinator cc = getCaseCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        PropertyCoordinator pc = getPropertyCoordinator();

        if (selectedRequest != null) {
            try {
                if (selectedRequest.getParcelKey() != 0) {
                    try {
                        getSessionBean().setSessProperty(pc.assemblePropertyDataHeavy(pc.getProperty(selectedRequest.getParcelKey()), getSessionBean().getSessUser()));
                    } catch (AuthorizationException ex) {
                        throw new BObStatusException(ex.getMessage());
                    }
                }

                MessageBuilderParams mbp = new MessageBuilderParams();
                mbp.setUser(getSessionBean().getSessUser());
                mbp.setExistingContent(selectedRequest.getPublicExternalNotes());
                mbp.setHeader(getResourceBundle(Constants.MESSAGE_TEXT).getString("attachedToCaseHeader"));
                mbp.setExplanation(getResourceBundle(Constants.MESSAGE_TEXT).getString("attachedToCaseExplanation"));
                mbp.setNewMessageContent("");

                selectedRequest.setPublicExternalNotes(sc.appendNoteBlock(mbp));

                // force the bean to go to the integrator and fetch a fresh, updated
                // list of action requests
                cc.cear_updateCEAR(selectedRequest, getSessionBean().getSessUser(), null);
                updateCEARUnprocessedList(true);
            } catch (IntegrationException  | BlobException | BObStatusException | SearchException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Unable to update action request with case attachment notes", ""));
            } 
        } else {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Please select an action request from the table to open a new case", ""));
        }

        getSessionBean().setSessCEAR(selectedRequest);
        //Here's a navstack to guide the user back after they add a case:
    }

    /**
     * Listener for user requests to stop their current routing operation
     * @param ev 
     */
    public void onAbortCEARRoutingOperation(ActionEvent ev){
        System.out.println("CEActionRequestsBB.onAbortCEARRoutingOperation");
        
    }
    
    /**
     * Listener for CEAR routing pathway confirm
     * @param c 
     */
    public void path2UseSelectedCaseForAttachment(CECase c) {
        CaseCoordinator cc = getCaseCoordinator();
        selectedCaseForAttachment = c;
        try {
            cc.cear_connectCEARToCECase(selectedCaseForAttachment, selectedRequest, CEARProcessingRouteEnum.ATTACH_TO_EXISTING_CASE, getSessionBean().getSessUser());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Successfully connected action request ID " + selectedRequest.getRequestID()
                    + " to code enforcement case ID " + selectedCaseForAttachment.getCaseID(), ""));
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Unable to connect request to case.",
                    getResourceBundle(Constants.MESSAGE_TEXT).getString("systemLevelError")));
        }
        refreshSelectedRequest();
        updateCEARUnprocessedList(true);
    }

    /**
     * listener for CEAR routing pathway
     * @param ev 
     */
    public void path3AttachInvalidMessage(ActionEvent ev) {
        SystemCoordinator sc = getSystemCoordinator();
        CaseCoordinator cc = getCaseCoordinator();

        if (selectedRequest != null) {

            // build message to document change
            MessageBuilderParams mcc = new MessageBuilderParams();
            mcc.setUser(getSessionBean().getSessUser());
            mcc.setExistingContent(selectedRequest.getPublicExternalNotes());
            mcc.setHeader(getResourceBundle(Constants.MESSAGE_TEXT).getString("invalidActionRequestHeader"));
            mcc.setExplanation(getResourceBundle(Constants.MESSAGE_TEXT).getString("invalidActionRequestExplanation"));
            mcc.setNewMessageContent(invalidMessage);
            selectedRequest.setPublicExternalNotes(sc.appendNoteBlock(mcc));
            // reset message
            invalidMessage = "";
            try {
                cc.cear_updateCEAR(selectedRequest, getSessionBean().getSessUser(), CEARProcessingRouteEnum.INVALID_REQUEST);
                refreshSelectedRequest();
                updateCEARUnprocessedList(true);
                evaulateSelectedRequestRoutingStatusAndUpdateRouteList();
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Public case note added to action request ID " + selectedRequest.getRequestID() + ".", ""));
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Unable to write message to The Database",
                                "This is a system level error that must be corrected by a sys admin--sorries!."));
            }
        } else {
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "You just tried to attach a message to a nonexistent request!",
                    "Choose the request to manage on the left, then click manage"));
        }
        
    }

    /**
     * Listener for CEAR routing pathway 4
     * @param ev 
     */
    public void path4AttachNoViolationFoundMessage(ActionEvent ev) {
        SystemCoordinator sc = getSystemCoordinator();
        if (selectedRequest != null) {

            CaseCoordinator cc = getCaseCoordinator();

            // build message to document change
            MessageBuilderParams mbp = new MessageBuilderParams();
            mbp.setUser(getSessionBean().getSessUser());
            mbp.setExistingContent(selectedRequest.getPublicExternalNotes());
            mbp.setHeader(getResourceBundle(Constants.MESSAGE_TEXT).getString("noViolationFoundHeader"));
            mbp.setExplanation(getResourceBundle(Constants.MESSAGE_TEXT).getString("noViolationFoundExplanation"));
            mbp.setNewMessageContent(noViolationFoundMessage);

            selectedRequest.setPublicExternalNotes(sc.appendNoteBlock(mbp));
            // reset noviolation found message
            noViolationFoundMessage = "";
            // force the bean to go to the integrator and fetch a fresh, updated
            // list of action requests
            requestListSearch = null;
            try {
                cc.cear_updateCEAR(selectedRequest, getSessionBean().getSessUser(), CEARProcessingRouteEnum.NO_VIOLATION_FOUND);
                evaulateSelectedRequestRoutingStatusAndUpdateRouteList();
                refreshSelectedRequest();
                updateCEARUnprocessedList(true);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Public case note added to action request ID " + selectedRequest.getRequestID() + ".", ""));
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Unable to write message to The Database",
                                "This is a system level error that must be corrected by a sys admin--sorries!."));
            }
        } else {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "No request selected!",
                            "Choose the request to manage on the left, then click manage"));
        }
    }
    
    /**
     * general listener for user requests to cancel any CEAR routing operation
     */
    public void onCEARRoutOperationAbort(){
        System.out.println("CEACtionRequestsBB.onCEARRoutOperationAbort");
    }

    /**
     * Updates internal fields only
     */
    private void updateCurrentCEAR(){
        CaseCoordinator cc = getCaseCoordinator();
        if(selectedRequest != null){
            try {
                cc.cear_updateCEAR(selectedRequest, getSessionBean().getSessUser(), null);
                refreshSelectedRequest();
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                  getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
            } 
        }
    }

  
    /**
     * Listener for user requests to start note add process on the 
     * selected request
     * @param ev 
     */
    public void noteAddInternalInit(ActionEvent ev){
        noteScope = NoteScopeEnum.INTERNAL;
        formNoteText = "";
    }
    
    
    /**
     * Listener for user requests to start note add process on the 
     * selected request
     * @param ev 
     */
    public void noteAddPublicInit(ActionEvent ev){
        noteScope = NoteScopeEnum.EXTERNAL;
        formNoteText = "";
    }
    

    /**
     * Listener for user requests to update the current CEAR's public-facing message
     * @param ev 
     */
    public void noteAddCommit(ActionEvent ev) {
        CaseCoordinator cc = getCaseCoordinator();
        try {
            cc.cear_updateNotes(selectedRequest, formNoteText, noteScope, getSessionBean().getSessUser());
            refreshSelectedRequest();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Note success!", ""));
        } catch (BObStatusException | IntegrationException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        } 
       
    }
   
    /**
     * @return the ceCaseIDForConnection
     */
    public int getCeCaseIDForConnection() {
        return ceCaseIDForConnection;
    }

    /**
     * @param ceCaseIDForConnection the ceCaseIDForConnection to set
     */
    public void setCeCaseIDForConnection(int ceCaseIDForConnection) {
        this.ceCaseIDForConnection = ceCaseIDForConnection;
    }
    
    /**
     * @return the selectedRequest
     */
    public CEActionRequestPropertyHeavy getSelectedRequest() {

        return selectedRequest;
    }

    /**
     * @param selectedRequest the selectedRequest to set
     */
    public void setSelectedRequest(CEActionRequestPropertyHeavy selectedRequest) {
        this.selectedRequest = selectedRequest;
    }

    /**
     * @return the searchParamsSelected
     */
    public SearchParamsCEActionRequests getSearchParamsSelected() {
        return searchParamsSelected;
    }

    /**
     * @param searchParamsSelected the searchParamsSelected to set
     */
    public void setSearchParamsSelected(SearchParamsCEActionRequests searchParamsSelected) {
        this.searchParamsSelected = searchParamsSelected;
    }

    /**
     * @return the selectedChangeToStatus
     */
    public CEActionRequestStatus getSelectedChangeToStatus() {
        return selectedChangeToStatus;
    }

    /**
     * @param selectedChangeToStatus the selectedChangeToStatus to set
     */
    public void setSelectedChangeToStatus(CEActionRequestStatus selectedChangeToStatus) {
        this.selectedChangeToStatus = selectedChangeToStatus;
    }

    /**
     * @return the invalidMessage
     */
    public String getInvalidMessage() {
        return invalidMessage;
    }

    /**
     * @param invalidMessage the invalidMessage to set
     */
    public void setInvalidMessage(String invalidMessage) {
        this.invalidMessage = invalidMessage;
    }

    /**
     * @return the caseListForSelectedProperty
     */
    public List<CECase> getCaseListForSelectedProperty() {
        if (selectedRequest != null) {

//            try {
                // TODO: use system-wide query mechanisms 
                // caseListForSelectedProperty = ci.getCECasesByProp(selectedRequest.getRequestProperty().getParcelKey()); 
//                System.out.println("CEActionRequestsBB.getCaseListForSelectedProperty | case list size: " + caseListForSelectedProperty.size());
//            } catch (IntegrationException | BObStatusException ex) {
//                System.out.println(ex);
//            }
        }
        return caseListForSelectedProperty;
    }

    /**
     * @param caseListForSelectedProperty the caseListForSelectedProperty to set
     */
    public void setCaseListForSelectedProperty(List<CECase> caseListForSelectedProperty) {
        this.caseListForSelectedProperty = caseListForSelectedProperty;
    }

    /**
     * @return the selectedCaseForAttachment
     */
    public CECase getSelectedCaseForAttachment() {
        return selectedCaseForAttachment;
    }

    /**
     * @param selectedCaseForAttachment the selectedCaseForAttachment to set
     */
    public void setSelectedCaseForAttachment(CECase selectedCaseForAttachment) {
        this.selectedCaseForAttachment = selectedCaseForAttachment;
    }

    /**
     * @return the noViolationFoundMessage
     */
    public String getNoViolationFoundMessage() {
        return noViolationFoundMessage;
    }

    /**
     * @param noViolationFoundMessage the noViolationFoundMessage to set
     */
    public void setNoViolationFoundMessage(String noViolationFoundMessage) {
        this.noViolationFoundMessage = noViolationFoundMessage;
    }

    /**
     * @return the disabledDueToRoutingNotAllowed
     */
    public boolean getIsDisabledDueToRoutingNotAllowed() {
        CaseCoordinator cc = getCaseCoordinator();

        disabledDueToRoutingNotAllowed
                = !(cc.cear_determineCEActionRequestRoutingActionEnabledStatus(
                        selectedRequest,
                        getSessionBean().getSessUser()));

        return disabledDueToRoutingNotAllowed;
    }

    /**
     * @param disabledDueToRoutingNotAllowed the disabledDueToRoutingNotAllowed
     * to set
     */
    public void setDisabledDueToRoutingNotAllowed(boolean disabledDueToRoutingNotAllowed) {
        this.disabledDueToRoutingNotAllowed = disabledDueToRoutingNotAllowed;
    }

    /**
     * @return the formNoteText
     */
    public String getFormNoteText() {
        return formNoteText;
    }

   

    /**
     * @param formNoteText the formNoteText to set
     */
    public void setFormNoteText(String formNoteText) {
        this.formNoteText = formNoteText;
    }

   

    /**
     * @return if the user should not be able to change public access on an object
     */
    public boolean isDisablePACCControl() {
        disablePACCControl = false;
        if (getSessionBean().getSessUser().getMyCredential().isHasMuniStaffPermissions() == false) {
            disablePACCControl = true;
        }
        return disablePACCControl;
    }

    /**
     * @param disablePACCControl the disablePACCControl to set
     */
    public void setDisablePACCControl(boolean disablePACCControl) {
        this.disablePACCControl = disablePACCControl;
    }

    /**
     * @return the selectedPersonForAttachment
     */
    public Person getSelectedPersonForAttachment() {
        return selectedPersonForAttachment;
    }

    /**
     * @param selectedPersonForAttachment the selectedPersonForAttachment to set
     */
    public void setSelectedPersonForAttachment(Person selectedPersonForAttachment) {
        this.selectedPersonForAttachment = selectedPersonForAttachment;
    }

    /**
     * @return the selectedQueryCEAR
     */
    public QueryCEAR getSelectedQueryCEAR() {
        return selectedQueryCEAR;
    }

    /**
     * @param selectedQueryCEAR the selectedQueryCEAR to set
     */
    public void setSelectedQueryCEAR(QueryCEAR selectedQueryCEAR) {
        this.selectedQueryCEAR = selectedQueryCEAR;
    }

    /**
     * @return the queryList
     */
    public List<QueryCEAR> getQueryList() {
        return queryList;
    }

    /**
     * @param queryList the queryList to set
     */
    public void setQueryList(List<QueryCEAR> queryList) {
        this.queryList = queryList;
    }

    /**
     * @return the requestListSearch
     */
    public List<CEActionRequestPropertyHeavy> getRequestListSearch() {
        return requestListSearch;
    }

    /**
     * @param requestListSearch the requestListSearch to set
     */
    public void setRequestListSearch(List<CEActionRequestPropertyHeavy> requestListSearch) {
        this.requestListSearch = requestListSearch;
    }

    /**
     * @return the reportConfig
     */
    public ReportCEARList getReportConfig() {
        return reportConfig;
    }

    /**
     * @param reportConfig the reportConfig to set
     */
    public void setReportConfig(ReportCEARList reportConfig) {
        this.reportConfig = reportConfig;
    }

 

    /**
     * @return the permissionAllowCEARRoutingAndEditing
     */
    public boolean isPermissionAllowCEARRoutingAndEditing() {
        return permissionAllowCEARRoutingAndEditing;
    }

    /**
     * @param permissionAllowCEARRoutingAndEditing the permissionAllowCEARRoutingAndEditing to set
     */
    public void setPermissionAllowCEARRoutingAndEditing(boolean permissionAllowCEARRoutingAndEditing) {
        this.permissionAllowCEARRoutingAndEditing = permissionAllowCEARRoutingAndEditing;
    }

    /**
     * @return the processingRouteList
     */
    public List<CEARProcessingRouteEnum> getProcessingRouteList() {
        return processingRouteList;
    }

    /**
     * @param processingRouteList the processingRouteList to set
     */
    public void setProcessingRouteList(List<CEARProcessingRouteEnum> processingRouteList) {
        this.processingRouteList = processingRouteList;
    }

    /**
     * @return the selectedRoute
     */
    public CEARProcessingRouteEnum getSelectedRoute() {
        return selectedRoute;
    }

    /**
     * @param selectedRoute the selectedRoute to set
     */
    public void setSelectedRoute(CEARProcessingRouteEnum selectedRoute) {
        this.selectedRoute = selectedRoute;
    }

    /**
     * @return the routeSpecificComponentsForUpdate
     */
    public String getRouteSpecificComponentsForUpdate() {
        return routeSpecificComponentsForUpdate;
    }

    /**
     * @param routeSpecificComponentsForUpdate the routeSpecificComponentsForUpdate to set
     */
    public void setRouteSpecificComponentsForUpdate(String routeSpecificComponentsForUpdate) {
        this.routeSpecificComponentsForUpdate = routeSpecificComponentsForUpdate;
    }

    /**
     * @return the routeSpecificDialogVar
     */
    public String getRouteSpecificDialogVar() {
        return routeSpecificDialogVar;
    }

    /**
     * @param routeSpecificDialogVar the routeSpecificDialogVar to set
     */
    public void setRouteSpecificDialogVar(String routeSpecificDialogVar) {
        this.routeSpecificDialogVar = routeSpecificDialogVar;
    }

    /**
     * @return the selectedRequestAwaitingInitialRouting
     */
    public boolean isSelectedRequestAwaitingInitialRouting() {
        return selectedRequestAwaitingInitialRouting;
    }

    /**
     * @param selectedRequestAwaitingInitialRouting the selectedRequestAwaitingInitialRouting to set
     */
    public void setSelectedRequestAwaitingInitialRouting(boolean selectedRequestAwaitingInitialRouting) {
        this.selectedRequestAwaitingInitialRouting = selectedRequestAwaitingInitialRouting;
    }

    /**
     * @return the editModeCEARProperty
     */
    public boolean isEditModeCEARProperty() {
        return editModeCEARProperty;
    }

    /**
     * @return the editModeCEARPersonLink
     */
    public boolean isEditModeCEARPersonLink() {
        return editModeCEARPersonLink;
    }

    /**
     * @param editModeCEARProperty the editModeCEARProperty to set
     */
    public void setEditModeCEARProperty(boolean editModeCEARProperty) {
        this.editModeCEARProperty = editModeCEARProperty;
    }

    /**
     * @param editModeCEARPersonLink the editModeCEARPersonLink to set
     */
    public void setEditModeCEARPersonLink(boolean editModeCEARPersonLink) {
        this.editModeCEARPersonLink = editModeCEARPersonLink;
    }

    /**
     * @return the selectedRequestPropertyDH
     */
    public PropertyDataHeavy getSelectedRequestPropertyDH() {
        return selectedRequestPropertyDH;
    }

    /**
     * @param selectedRequestPropertyDH the selectedRequestPropertyDH to set
     */
    public void setSelectedRequestPropertyDH(PropertyDataHeavy selectedRequestPropertyDH) {
        this.selectedRequestPropertyDH = selectedRequestPropertyDH;
    }

    /**
     * @return the permissionAllowCEARDeactivation
     */
    public boolean isPermissionAllowCEARDeactivation() {
        return permissionAllowCEARDeactivation;
    }

    /**
     * @param permissionAllowCEARDeactivation the permissionAllowCEARDeactivation to set
     */
    public void setPermissionAllowCEARDeactivation(boolean permissionAllowCEARDeactivation) {
        this.permissionAllowCEARDeactivation = permissionAllowCEARDeactivation;
    }

    /**
     * @return the issueTypeList
     */
    public List<CEActionRequestIssueType> getIssueTypeList() {
        return issueTypeList;
    }

    /**
     * @param issueTypeList the issueTypeList to set
     */
    public void setIssueTypeList(List<CEActionRequestIssueType> issueTypeList) {
        this.issueTypeList = issueTypeList;
    }

    /**
     * @return the statusList
     */
    public List<CEActionRequestStatus> getStatusList() {
        return statusList;
    }

    /**
     * @param statusList the statusList to set
     */
    public void setStatusList(List<CEActionRequestStatus> statusList) {
        this.statusList = statusList;
    }

    /**
     * @return the renderCEAREditingComponents
     */
    public boolean isRenderCEAREditingComponents() {
        return renderCEAREditingComponents;
    }

    /**
     * @param renderCEAREditingComponents the renderCEAREditingComponents to set
     */
    public void setRenderCEAREditingComponents(boolean renderCEAREditingComponents) {
        this.renderCEAREditingComponents = renderCEAREditingComponents;
    }

    /**
     * @return the requestListUnprocessed
     */
    public List<CEActionRequestPropertyHeavy> getRequestListUnprocessed() {
        return requestListUnprocessed;
    }

    /**
     * @param requestListUnprocessed the requestListUnprocessed to set
     */
    public void setRequestListUnprocessed(List<CEActionRequestPropertyHeavy> requestListUnprocessed) {
        this.requestListUnprocessed = requestListUnprocessed;
    }

    /**
     * @return the noteScope
     */
    public NoteScopeEnum getNoteScope() {
        return noteScope;
    }

    /**
     * @param noteScope the noteScope to set
     */
    public void setNoteScope(NoteScopeEnum noteScope) {
        this.noteScope = noteScope;
    }

  

}
