/*
 * Copyright (C) 2020 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.*;
import com.tcvcog.tcvce.domain.*;
import com.tcvcog.tcvce.entities.*;
import com.tcvcog.tcvce.entities.occupancy.OccPeriod;
import com.tcvcog.tcvce.entities.occupancy.OccPeriodType;
import com.tcvcog.tcvce.integration.PropertyIntegrator;
import com.tcvcog.tcvce.util.*;
import com.tcvcog.tcvce.util.viewoptions.ViewOptionsActiveHiddenListsEnum;
import com.tcvcog.tcvce.util.viewoptions.ViewOptionsProposalsEnum;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The resurrected version of the Property Profile BB
 * At one point, this was collaspsed into the PropertySearchBB
 * when it was though these would happen at the same time;
 * Upon the dialogification of search based on the 
 * main container, this Bean was brought back to life and once
 * again occupies its hallowed role as the premier backing 
 * bean for individual parcel/property management
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */
public class PropertyProfileBB 
        extends BackingBeanUtils{

    private PropertyDataHeavy currentProperty;
    
    private boolean currentPropertyEditMode;
    private String parcelIDBeforeEdit;
    
    private boolean currentParcelInfoEditMode;
    private boolean permissionAllowCECaseCreate;
    private boolean permissionAllowParcelEdit;
    private boolean permissionAllowOccPeriodCreate;
    
    private List<PropertyUseType> putList;
    
    // UNIT STUFF
    protected boolean unitEditMode;
    private PropertyUnitDataHeavy currentPropertyUnit;
    
    private CECase currentCECase;
  
    private String formUnitNoteText;
    
    // occ period creation stuff
    private OccPeriod currentOccPeriod;
    private List<OccPeriodType> occPeriodTypeList;
    private List<EventCategory> occPeriodOriginiationEventCandidateList;
    private PropertyUnit occPeriodInitSelectedUnit;
    private boolean occPeriodInitCreateNewUnitWithNumber;
    private String occPeriodInitNewUnitNumberForm;
    private User occPeriodInitDifferentManagerSelected;
    private String formEventNotes;    
    
    
    private Municipality muniSelected;
    
    private ViewOptionsActiveHiddenListsEnum eventViewOptionSelected;
    private List<ViewOptionsActiveHiddenListsEnum> eventViewOptions;

    private PropertyUseType selectedPropertyUseType;

    private List<IntensityClass> conditionIntensityList;
    private List<IntensityClass> landBankProspectIntensityList;
    private List<BOBSource> sourceList;
    
    private List<BlobLight> broadViewPhotoCandidates;
    private BlobLight selectedBoadviewPhoto;

    private String formNoteText;

    private Person personSelected;
    private List<Person> personToAddList;
    private boolean personLinkUseID;
    private int humanIDToLink;

    private ViewOptionsProposalsEnum selectedPropViewOption;
    
    // BLOBS
    private BlobLight currentBlob;
    /**
     * Creates a new instance of PropertyCreateBB
     */
    public PropertyProfileBB() {
        
    }
    
     
    @PostConstruct
    public void initBean(){
        PropertyIntegrator pi = getPropertyIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        OccupancyCoordinator oc = getOccupancyCoordinator();
        
        currentProperty = getSessionBean().getSessProperty();
        
        if(currentProperty != null){
            System.out.println("PropertyProfileBB.initBean() | Current Property: " + currentProperty.getParcelKey());
        } else {
            System.out.println("PropertyProfileBB.initBean() | Current Property null ");
        }
        
        // setup event domain
        getSessionEventConductor().setSessEventsPageEventDomainRequest(EventRealm.PARCEL);
        reloadCurrentPropertyDataHeavy();
         try {
             setPutList(pi.getPropertyUseTypeList());
         } catch (IntegrationException ex) {
             System.out.println(ex);
         }
         
        currentParcelInfoEditMode = false;
        currentPropertyEditMode = false;
        try {
            setConditionIntensityList(sc.getIntensitySchemaWithClasses(
                    getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("intensityschema_propertycondition"), getSessionBean().getSessMuni())
                    .getClassList());
            setLandBankProspectIntensityList(sc.getIntensitySchemaWithClasses(
                    getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("intensityschema_landbankprospect"), getSessionBean().getSessMuni())
                    .getClassList());
         
            occPeriodTypeList = oc.getOccPeriodTypeList(getSessionBean().getSessMuni(), false);
            sourceList = sc.getBobSourceListComplete();
            personToAddList = new ArrayList<>();
            eventViewOptions = Arrays.asList(ViewOptionsActiveHiddenListsEnum.values());
            eventViewOptionSelected = ViewOptionsActiveHiddenListsEnum.VIEW_ACTIVE_NOTHIDDEN;
            getSessionBean().setSessHumanListRefreshedList(null);
            configurePermissions();
        } catch (IntegrationException  ex) {
            System.out.println(ex);
        }
    }
    
   
    
    /**
     * Asks respective subsystem coordinators for permissions bits for various tasks to pass on to the UI
     */
    private void configurePermissions(){
        // Set permissions bit for parcel add/edit info add/edit
        PropertyCoordinator pc = getPropertyCoordinator();
        CaseCoordinator cc = getCaseCoordinator();
        OccupancyCoordinator oc = getOccupancyCoordinator();
        permissionAllowParcelEdit = pc.permissionCheckpointEditparcelAndPropertyInfo(getSessionBean().getSessUser());
        permissionAllowCECaseCreate = cc.permissionsCheckpointOpenCECase(currentProperty, getSessionBean().getSessUser());
        permissionAllowOccPeriodCreate = oc.permissionsCheckpointOpenOccPeriod(getSessionBean().getSessUser());
        
    }
    
      /**
     * Called when the user initiates new occ period creation.
     * As of April 2024 this pathway will now also initiate a skeleton
     * field inspection with pre-filled form fields, hopefully increasing
     * use of the dispatch function
     * 
     * @param pu may be null for a general non-unit specific add pathway in which case 
     * a drop down will appear and a new unit form component in the permit file add dialog
     * 
     */
    public void onOccperiodCreateInitButtonChange(PropertyUnit pu){
        OccupancyCoordinator oc = getOccupancyCoordinator();
        EventCoordinator ec = getEventCoordinator();
        try {
            // check if we are in our unitless creation pathway
            if(pu == null){
                currentPropertyUnit = null;
            } else {
                configureCurrentPropertyUnitFromUnitBase(pu);
            }
            currentOccPeriod = oc.getOccPeriodSkeleton(pu, getSessionBean().getSessUser());
            occPeriodOriginiationEventCandidateList = ec.determinePermittedEventCategories(EventType.OccupancyOrigination, getSessionBean().getSessUser());
            
            
        } catch (BObStatusException | AuthorizationException | EventException | IntegrationException ex) {
            System.out.println(ex);
              getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                        "Unable to start occ period creation process, sorry. ", 
                        ""));
        } 
    }
    
    /**
     * Sets the data heavy property unit member on this class from base class
     * @param pu 
     */
    private void configureCurrentPropertyUnitFromUnitBase(PropertyUnit pu) throws IntegrationException, EventException, AuthorizationException, BObStatusException{
        PropertyCoordinator pc = getPropertyCoordinator();
        currentPropertyUnit = pc.getPropertyUnitDataHeavy(pu, getSessionBean().getSessUser());
        
    }
    
      /**
     * Final step in creating a new occ period
     * @return 
     */
    public String onOccPeriodCreateCommitButtonChange(){
        
        OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            // three pathways, the first is if our current property unit is set
            // meaning we're making a new occ period on this bean's currentPropertyUnit
            // path 2 and 3 are inside this if
            if(currentPropertyUnit == null){
                System.out.println("PropertyProfileBB.onOccPeriodCreateCommitButtonChange | no selected unit");
                // path two is the new unit number input text is empty, so we need a unit from drop down
                if(!occPeriodInitCreateNewUnitWithNumber){
                    if(occPeriodInitSelectedUnit != null){
                        System.out.println("PropertyProfileBB.onOccPeriodCreateCommitButtonChange | selected existing unit " + occPeriodInitSelectedUnit.getUnitNumber());
                        configureCurrentPropertyUnitFromUnitBase(occPeriodInitSelectedUnit);
                    } else {
                        // use the first unit on the property if they mucked up the form
                        System.out.println("PropertyProfileBB.onOccPeriodCreateCommitButtonChange |  empty form and no new unit");
                        configureCurrentPropertyUnitFromUnitBase(currentProperty.getUnitList().get(0));
                    }
                    // pathway 3, make a new unit from the form
                } else {
                        System.out.println("PropertyProfileBB.onOccPeriodCreateCommitButtonChange |  creating from form field " + occPeriodInitNewUnitNumberForm);
                        onUnitAddInitButtonChange(null);
                        currentPropertyUnit.setUnitNumber(occPeriodInitNewUnitNumberForm);
                        onUnitAddCommit();
                        unitEditMode = false;
                        // we now have a new fresh unit
                        System.out.println("PropertyProfileBB.onOccPeriodCreateCommitButtonChange |  fresh unit with ID: " + currentPropertyUnit.getUnitID());
                }
            }
            // regardless of path, set our currentOccPeriodSkeleton with our currentPropertyUnit id, either new or existing
            currentOccPeriod.setPropertyUnitID(currentPropertyUnit.getUnitID());
            int newID = oc.insertOccPeriod(currentPropertyUnit, currentOccPeriod, getSessionBean().getSessUser(), formEventNotes);
            formEventNotes = null;
            return getSessionBean().navigateToPageCorrespondingToObject(oc.assembleOccPeriodDataHeavy(oc.getOccPeriod(newID, getSessionBean().getSessUser()), getSessionBean().getSessUser()));
        } catch (IntegrationException | BObStatusException | EventException | AuthorizationException | BlobException  ex) {
            System.out.println("PropertyProfileBB.onAddOccperiodCommitButtonChange | " + ex.getClass().getCanonicalName());
            System.out.println(ex);
            getFacesContext().addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                        ex.getMessage() , ""));
            getFacesContext().validationFailed();
        }
        return "";
    }
    
    
    /**
     * Listener for user selecting or changing the new occ period type drop down
     * which will update the default init event cat
     */
    public void onOccPeriodInitTypeChange(){
        if(currentOccPeriod != null && currentOccPeriod.getPeriodType() != null && currentOccPeriod.getPeriodType().getDefaultOriginationEventCategory() != null){
            EventCategory defaultOrigCat = currentOccPeriod.getPeriodType().getDefaultOriginationEventCategory();
            if(!occPeriodOriginiationEventCandidateList.contains(defaultOrigCat)){
                occPeriodOriginiationEventCandidateList.add(defaultOrigCat);
            }
            currentOccPeriod.setOriginationEventCategory(defaultOrigCat);
            formEventNotes = defaultOrigCat.getHostEventDescriptionSuggestedText();
            System.out.println("PropertyProfileBB.onOccPeriodInitTypeChange | injected " + defaultOrigCat.getEventCategoryTitle());
        } else {
            System.out.println("PropertyProfileBB.onOccPeriodInitTypeChange | nothing to inject");
        }
            
    }
    
    
    /**
     * Listener for user reqeusts to start the note process on the parcel
     * @param ev 
     */
    public void onParcelNoteAppendInit(ActionEvent ev){
        formNoteText = "";
        System.out.println("PropertyProfileBB.onParcelNoteAppendInit");
        
    }
    
    /**
     * Finishes note process.
     * @param ev 
     */
    public void onParcelNoteAppendCommit(ActionEvent ev){
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            pc.updateParcelNotes(currentProperty, formNoteText, getSessionBean().getSessUser());
            reloadCurrentPropertyDataHeavy();
            System.out.println("PropertyProfileBB.onParcelNoteAppendCommit");
            getFacesContext().addMessage(null,
             new FacesMessage(FacesMessage.SEVERITY_INFO, 
                      "Successfully appended note to parcel", 
                      ""));
            formNoteText = "";
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
              getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                        "Database error writing parcel note", 
                        ""));
            
        } 
        
    }
    
    
    /**
     * Listener for user requests to start property creation 
     * process
     * @param ev 
     */
    public void initiatePropertyCreation(ActionEvent ev){
        
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            currentProperty = pc.assemblePropertyDataHeavy(pc.generatePropertySkeleton(getSessionBean().getSessMuni()),getSessionBean().getSessUser());
        } catch (IntegrationException | BObStatusException | SearchException | BlobException ex) {
            System.out.println(ex);
        } 
    }
    
    /**
     * Listener for user requests to start the broadview photo update process
     * @param ev 
     */
    public void onChangeBroadviewPhotoInitLinkClick(ActionEvent ev){
        BlobCoordinator bc = getBlobCoordinator();
        
        broadViewPhotoCandidates = bc.assembleBrowserViewableBlobs(currentProperty.getBlobList());
        try {
            broadViewPhotoCandidates.add(bc.getDefaultBroadviewPhoto());
        } catch (IntegrationException | BlobException ex) {
            System.out.println(ex);
        } 
    
    }
    
    
    /**
     * Listener for user requests to commit the new broadview photo
     * @param blight    
     */
    public void onChangeBroadviewPhotoCommitLinkClick(BlobLight blight){
        PropertyCoordinator pc = getPropertyCoordinator();
        selectedBoadviewPhoto = blight;
        if(selectedBoadviewPhoto != null){
            currentProperty.setBroadviewPhoto(selectedBoadviewPhoto);
            try {
                pc.updatePropertyDataHeavyBroadviewPhoto(currentProperty, getSessionBean().getSessUser());
                reloadCurrentPropertyDataHeavy();
            } catch (BObStatusException | IntegrationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                   new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                           "ERROR updating photo", ""));
            } 
        } else {
             getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                        "Cannot update broadview photo to null", ""));
        }
        
        
    }
    
    /**
     * Listener for user requests to insert a new property
     * @param ev 
     */
    public void insertProp(ActionEvent ev){
        PropertyCoordinator pc = getPropertyCoordinator();
        UserAuthorized ua = getSessionBean().getSessUser();
        SystemCoordinator sc = getSystemCoordinator();
        int freshID = 0;
        try {
            freshID = pc.addParcel(currentProperty, ua);
            currentProperty = pc.getPropertyDataHeavy(freshID, getSessionBean().getSessUser()); 
            getSessionBean().setSessProperty(pc.assemblePropertyDataHeavy(currentProperty, ua));
            sc.logObjectView(ua, currentProperty);
            getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, 
                        "Successfully inserted property with ID " + freshID
                                + ", which is now your 'active property'", ""));
            
            
        } catch (IntegrationException | BObStatusException | SearchException | AuthorizationException | EventException | BlobException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                        "Unable to insert property in database, sorry. ", 
                        "Please make sure all required fields are completed. "));
        }
    }
    
       /**
     * ********************************************************
     * ************* PROPERTY PROFILE/INFO METHODS ************
     * ********************************************************
     */
    
    /**
     * Convenience method for manual reload on property unit list
     * @param ev 
     */
    public void onReloadCurrentPropertyDataHeavy(ActionEvent ev){
        reloadCurrentPropertyDataHeavy();
    }
    
    /**
     * Utilty method for refreshing property
     */
    public void reloadCurrentPropertyDataHeavy(){
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            currentProperty = pc.assemblePropertyDataHeavy(currentProperty, getSessionBean().getSessUser());
        } catch (IntegrationException | BObStatusException | SearchException | BlobException ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Fatal error reloading property; sorries!", ""));
        }
    }
    
    /**
     * Listener for user requests to toggle edit mode of parcel level 
     * data
     * @param ev 
     */
    public void onPropertyEditModeToggleButtonChange(ActionEvent ev){
        System.out.println("PropertyProfileBB.onPropertyEditModeToggleButtonChange | edit mode start:" + currentPropertyEditMode);
        if(currentProperty != null){
            if(currentPropertyEditMode){

                if(currentProperty.getParcelKey() == 0){
                    // we have a new property 
                    // this page will never have a new property. If we do, throw error
                    System.out.println("PropertyProfileBB.onPropertyEditModeToggleButtonChange | found property without ID on property profile page!! ERROR!");

                } else {
                    // we have an existing property
                    onPropertyUpdateCommit();
                    writeParcelIDChangesToNotes();
                }
            } else {
                // if we're going into EDIT mode, meaning on this click we're not in edit mode, save the parcel ID
                parcelIDBeforeEdit = currentProperty.getCountyParcelID();
            }
        } // close null prop
        currentPropertyEditMode = !currentPropertyEditMode;
    }
    
    /**
     * Appends a note to the current property archiving the parcel ID deltas
     */
    private void writeParcelIDChangesToNotes() {
        SystemCoordinator sc = getSystemCoordinator();
        PropertyCoordinator pc = getPropertyCoordinator();
        // write old parcelID to notes
        StringBuilder sb = new StringBuilder();
        sb.append("Previous parcel ID: ");
        sb.append(parcelIDBeforeEdit);
        sb.append(Constants.FMT_HTML_BREAK);
        sb.append("Updated parcel ID: ");
        sb.append(currentProperty.getCountyParcelID());

        MessageBuilderParams mbp = new MessageBuilderParams(
                currentProperty.getNotes(), 
                "Parcel ID change", 
                "This parcel's county Parcel ID was edited", 
                sb.toString(), 
                getSessionBean().getSessUser(), 
                null);
        currentProperty.setNotes(sc.appendNoteBlock(mbp));
        try {
            pc.updateParcelNotes(currentProperty, getSessionBean().getSessUser());
            getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_INFO,
                               "Archived previous parcel ID to property notes", ""));
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Fatal error writing old parcel ID to notes", ""));
        } 
        
    }
    
    /**
     * Listener for user requests to abandon a parcel info edit operation
     * 
     * @param ev 
     */
    public void onPropertyEditModeOperationAbortButtonChange(ActionEvent ev){
        System.out.println("PropertyProfileBB.abortPropertyEditmode");
        currentPropertyEditMode = false;
    }
    
    /**
     * Listener for user requests to abandon a parcel info edit operation
     * 
     * @param ev 
     */
    public void onParcelInfoEditModeOperationAbortButtonChange(ActionEvent ev){
        System.out.println("PropertyProfileBB.abortParcelInfoEditmode");
        currentParcelInfoEditMode = false;
    }
    
    /**
     * listener for user requests to start the parcel info edit process
     * @param ev 
     */
    public void onParcelInfoViewEditLinkClick(ActionEvent ev){
        // nothign to do yet
        System.out.println("propertyProfileBB.onParcelInfoViewEditLinkClick");
    }
    
    /**
     * Listener for user requests to toggle edit mode of a parcel info record
     * @param ev 
     */
    public void onParcelInfoEditModeToggleButtonChange(ActionEvent ev){
        System.out.println("PropertyProfileBB.onParcelInfoEditModeToggleButtonChange | edit mode start:" + currentParcelInfoEditMode);
        
        if(currentParcelInfoEditMode && currentProperty != null && currentProperty.getParcelInfo() != null){
            if(currentProperty.getParcelInfo().getParcelInfoID() == 0){
                 onParcelInfoAddCommit();
                  
            } else {
                onParcelInfoUpdateCommit();
            }
        }
        currentParcelInfoEditMode = !currentParcelInfoEditMode;
    }
   
    /**
     * Listener for user requests to commit property updates
     */
    public void onPropertyUpdateCommit() {
        PropertyCoordinator pc = getPropertyCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        try {
//            currentProperty.setAbandonedDateStart(pc.configureDateTime(currentProperty.getAbandonedDateStart().to));
            pc.updateParcel(currentProperty, getSessionBean().getSessUser());
            reloadCurrentPropertyDataHeavy();
            sc.logObjectView(getSessionBean().getSessUser(), currentProperty);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Successfully updated property with ID " + getCurrentProperty().getParcelKey()
                            + ", which is now your 'active property'", ""));
        } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Could not update property, sorries! " + ex.toString(), ""));
        }
    }
    
    /**
     * Listener to start the viewing of property alerts
     * @param ev 
     */
    public void onPropertyAlertViewInit(ActionEvent ev){
        System.out.println("PropertyProfileBB.onPropertyAlertViewInit");
    }
    
    /**
     * listener for user requests to start the deac process
     * @param ev 
     */
    public void onPropertyDeacInit(ActionEvent ev){
        try {
            System.out.println("PropertyProfileBB.onPropertyDeacInit");
            PropertyCoordinator pc = getPropertyCoordinator();
            currentProperty = pc.deactivateParcelAudit(currentProperty);
        } catch (BObStatusException ex) {
            System.out.println(ex);
            currentProperty.setAllowDeactivation(false);
        }
    }
    
    /**
     * Listener for user requests to commit a deac request
     
     * @return  back to dash with muni prop activation
     */
    public String onPropertyDeacCommit(){
        System.out.println("PropertyProfileBB.onPropertyDeacCommit");
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            pc.deactivateParcel(currentProperty, getSessionBean().getSessUser());
            
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Parcel deactivated with key: " + currentProperty.getParcelKey(), ""));
            return getSessionBean().navigateToPageCorrespondingToObject(getSessionBean().getSessMuni().getMuniPropertyDH());
        } catch (BObStatusException | AuthorizationException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Could not deac property, sorries! " + ex.toString(), ""));
        } 
        return "";
    }
    
    /**
     * Listener for user requests to abort the deactivation operation
     * @param ev 
     */
    public void onPropertyDeacAbort(ActionEvent ev){
        System.out.println("PropertyProfileBB.onPropertyDeacAbort");
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Parcel deactivated aborted!: ", ""));
    }
    
    /**
     * Writes new parcel info record to the DB
     */
    public void onParcelInfoAddCommit(){
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            pc.insertParcelInfoRecord(currentProperty.getParcelInfo(), getSessionBean().getSessUser());
            reloadCurrentPropertyDataHeavy();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Successfully inserted a new property info record", ""));
        } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "FATAL: Could not insert parcel info! " + ex.toString(), ""));
        }
    }
    
    /**
     * Writes changes to current parcel info record to the DB
     */
    public void onParcelInfoUpdateCommit(){
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            pc.updateParcelInfoRecord(currentProperty.getParcelInfo(), getSessionBean().getSessUser());
            reloadCurrentPropertyDataHeavy();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Successfully updated parcel info record ID: " + currentProperty.getParcelInfo().getParcelInfoID(), ""));
        } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "FATAL: Could not update parcel info! " + ex.toString(), ""));
        }
    }
    
      /**
     * Special getter that checks the session for updates on just this person.
     * @return 
     */
    public List<MailingAddressLink> getCurrentPropertyMADLinkList(){
        if(currentProperty != null){
            List<MailingAddressLink> sessLinkList = getSessionBean().getSessMailingAddressLinkRefreshedList();
            if(sessLinkList != null){
                System.out.println("PropertyProfileBB.currentPropertyMADLinkList | sessLinkListSize: " + sessLinkList.size());
                currentProperty.setMailingAddressLinkList(sessLinkList);
                getSessionBean().setSessMailingAddressLinkRefreshedList(null);
            }
            return currentProperty.getMailingAddressLinkList();
        }
        return new ArrayList<>();
    }
    
    /**
     * Listener for when the user aborts a property add operation;
     *
     * @param ev
     */
    public void onDiscardNewPropertyDataButtonChange(ActionEvent ev) {

    }

   
    
    
    /**
     * Special wrapper getter around the current property Unit's human
     * link list that asks the session for a new link list
     * on table load that might occur during a link edit operation
     * @return the new human link list
     */
    public List<HumanLink> getManagedUnitHumanLinkList(){
        List<HumanLink> hll = getSessionBean().getSessHumanListRefreshedList();
        if(hll != null){
            currentPropertyUnit.sethumanLinkList(hll);
            // clear our refreshed list
            getSessionBean().setSessHumanListRefreshedList(null);
        }
        return currentPropertyUnit.gethumanLinkList();
    }
   
    /**
     * Special wrapper getter around the current property's human
     * link list that asks the session for a new link list
     * on table load that might occur during a link edit operation
     * @return the new human link list
     */
    public List<HumanLink> getManagedHumanLinkList(){
        List<HumanLink> hll = getSessionBean().getSessHumanListRefreshedList();
        if(hll != null){
            currentProperty.sethumanLinkList(hll);
            // clear our refreshed list
            getSessionBean().setSessHumanListRefreshedList(null);
        } else {
        }
        return currentProperty.gethumanLinkList();
    }

    /**
     * Special getter that responds to updates to property links in a unit
     * @return 
     */
    public List<MailingAddressLink> getManagedPropertyUnitAddressLinkList(){
        List<MailingAddressLink> madList = getSessionBean().getSessMailingAddressLinkRefreshedList();
        if(madList != null){
            currentPropertyUnit.setMailingAddressLinkList(madList);
            // clear our refreshed list
            getSessionBean().setSessMailingAddressLinkRefreshedList(null);
        } else {
        }
        return currentPropertyUnit.getMailingAddressLinkList();
        
    }
    
    
    /**
     * Listener for user requests to remove the currently selected ERA;
     * Delegates all work to internal, non-listener method.
     *
     * @return Empty string which prompts page reload without wiping bean memory
     */
    public String onRemoveButtonChange() {
        getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Successfully Remove Municipality", ""));
        return "";
    }

    /**
     * Listener for user requests to manage units on a property
     * @param ev 
     */
    public void onPropUnitExploreButtonChange(ActionEvent ev) {
        System.out.println("PropertyProfileBB.onPropUnitExploreButtonChange");
    }

    /**
     * Listener for user requests to view advanced search dialog
     *
     * @param ev
     */
    public void onAdvancedSearchButtonChange(ActionEvent ev) {

    }
   
    /**
     * Listener for user requests to start the update process
     */
    public void onPropertyUpdateInit() {
        // nothing to do here yet
    }

    public String onCreateNewCaseButtonChange() {
        getSessionBean().setSessProperty(currentProperty);
        getSessionBean().getNavStack().pushPage("ceCaseWorkflow");
        return "addNewCase";

    }

    /**
     * Listener for requests to view an event
     *
     * @param ev
     * @return
     */
    public String onViewEventButtonChange(EventCnF ev) {
        if (ev != null) {
            getSessionEventConductor().setSessEvent(ev);
            return "eventAddEdit";
        }
        return "";
    }
    
    /**
     * Listener for user requests to view an occ period
     * @param op
     * @return 
     */
    public String onViewOccPeriodButtonChange(OccPeriod op){
        if(op != null){
            try {
                return getSessionBean().navigateToPageCorrespondingToObject(op);
            } catch (BObStatusException | AuthorizationException ex) {
                System.out.println(ex);
            } 
        }
        return "";
    }

    /**
     * Listener for requests to view a CECase
     *
     * @param cse
     * @return
     */
    public String onViewCaseButtonChange(CECase cse) {
        CaseCoordinator cc = getCaseCoordinator();
        if (cse != null) {
            System.out.println("PropertyProfile.onViewCaseButtonChange: setting in session case ID " + cse.getCaseID());

            getSessionBean().setSessCECase(cse);
        }
        try {
            return getSessionBean().navigateToPageCorrespondingToObject(getSessionBean().getSessCECase());
        } catch (BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
        } 
        return "";
    }
    
    
  
    /**
     * Listener for user requests to link a new address to the 
     * current parcel
     * 
     * @param ev 
     */
    public void onLinkNewAddressToParcelButtonChange(ActionEvent ev){
        System.out.println("PropertyProfileBB.linkNewAddressToParcel");
    }
    
    // ********************************************************
    // ********************* CECASE ***************************
    // ********************************************************
    
    
    /**
     * Special getter that will ask session bean for a fresh case list if 
     * this property's cases have been augmented while the profile is being viewed
     * @return 
     */
    public List<CECaseDataHeavy> getManagedCECasePDHList(){
        List<CECaseDataHeavy> sessCECaseList = getSessionBean().getSessCECaseRefreshedList();
        if(currentProperty != null){
            if(sessCECaseList != null){
                currentProperty.setCeCaseList(sessCECaseList);
                getSessionBean().setSessCECaseRefreshedList(null);
            }
            return currentProperty.getCeCaseList();
        } else {
            return new ArrayList<>();
        }
        
    }
   
    
    // ********************************************************
    // *******************   UNIT TOOLS ***********************
    // ********************************************************
    
     
    /**
     * Listener for when the user starts editing the person links
     * to this particular event
     */
    public void onManageUnitPersonButtonChange(){
        System.out.println("PropertyProfileBB.onManageUnitPersonButtonChange");
        
    }
    
      /**
       * Listener for user requests to view or edit a property unit
       * 
       * @param pu 
       */
      public void onUnitViewEditLinkClick(PropertyUnit pu){
          PropertyCoordinator pc = getPropertyCoordinator();
        try {
            currentPropertyUnit = pc.getPropertyUnitDataHeavy(pu, getSessionBean().getSessUser());
        } catch (IntegrationException | AuthorizationException | BObStatusException | EventException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Could not load property unit with lists" + ex.getMessage(), ""));
        } 
      }

      /**
       * listener for user requests to start the unit add process
       * @param ev 
       */
      public void onUnitAddInitButtonChange(ActionEvent ev){
          PropertyCoordinator pc = getPropertyCoordinator();
        try {
            currentPropertyUnit = pc.getPropertyUnitDataHeavy(pc.getPropertyUnitSkeleton(currentProperty), getSessionBean().getSessUser());
            unitEditMode = true;
        } catch (IntegrationException | AuthorizationException | BObStatusException | EventException ex) {
            System.out.println(ex);
              getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error starting unit creation: " + ex.getMessage(), ""));
        } 
      }
      
      /**
       * Listener for the press of the unit edit on/off mode
       * @param ev 
       */
      public void onUnitEditToggleButtonChange(ActionEvent ev){
          if(unitEditMode){
              if(currentPropertyUnit != null){
                  if(currentPropertyUnit.getUnitID() == 0){
                      onUnitAddCommit();
                  } else {
                      onUnitUpdateCommit();
                  }
                  reloadCurrentPropertyDataHeavy();
              }
          }
          unitEditMode = !unitEditMode;
      }
      
      /**
       * Listener for user request to abort the unit edit process
       * @param ev 
       */
      public void onUnitEditModeAbortButtonChange(ActionEvent ev){
          System.out.println("PropertyProfileBB.onUnitEditModeAbortButtonChange");
          unitEditMode = false;
      }
      
      /**
       * Internal caller to the coordinator for unit adds
       */
      private void onUnitAddCommit(){
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            int freshID = pc.insertPropertyUnit(currentPropertyUnit, currentProperty, getSessionBean().getSessUser());
            if(freshID != 0){

                currentPropertyUnit = pc.getPropertyUnitDataHeavy(pc.getPropertyUnit(freshID), getSessionBean().getSessUser());

                getFacesContext().addMessage(null,
                      new FacesMessage(FacesMessage.SEVERITY_INFO,
                              "Unit add success!", ""));
            } else {
                throw new IntegrationException("could not get a non-zero New Unit ID");
                
            }
        } catch (BObStatusException | AuthorizationException | EventException | IntegrationException ex) {
            System.out.println(ex);
              getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error on unit insert: " + ex.getMessage(), ""));
        }
      }
      
      /**
       * Listener to start the human linking process
       * @param ev 
       */
      public void onUnitPersonManageInit(ActionEvent ev){
          System.out.println("PropertyProfileBB.onUnitPersonManageInit");
      }
      
      /**
       * Listener for user requests to start the note on unit process
       * @param ev 
       */
      public void onUnitNoteAddInitButtonChange(ActionEvent ev){
          formUnitNoteText = "";
      }
      
      /**
       * Listener for user requests to commit a note to a unit
       * @param ev 
       */
      public void onUnitNoteAddCommitButtonChange(ActionEvent ev){
          SystemCoordinator sc = getSystemCoordinator();
          PropertyCoordinator pc = getPropertyCoordinator();
          MessageBuilderParams mbp = new MessageBuilderParams(currentPropertyUnit.getNotes(), null, null , formUnitNoteText, getSessionBean().getSessUser(), null);
          currentPropertyUnit.setNotes(sc.appendNoteBlock(mbp));
        try {
            pc.updatePropertyUnit(currentPropertyUnit, getSessionBean().getSessUser());
            currentPropertyUnit = pc.getPropertyUnitDataHeavy(currentPropertyUnit, getSessionBean().getSessUser());
            formUnitNoteText = "";
            getFacesContext().addMessage(null,
                  new FacesMessage(FacesMessage.SEVERITY_INFO,
                          "Unit note update succses! Great work!", ""));
        } catch (BObStatusException | AuthorizationException  | EventException | IntegrationException  ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                  new FacesMessage(FacesMessage.SEVERITY_ERROR,
                          "Unit  note write failiure.", ""));
        }
          
      }
      
      /**
       * Internal caller to the coordinator for unit updates
       */
      private void onUnitUpdateCommit(){
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            pc.updatePropertyUnit(currentPropertyUnit,getSessionBean().getSessUser());
            refreshCurrentPropertyUnit(null);
            getFacesContext().addMessage(null,
                  new FacesMessage(FacesMessage.SEVERITY_INFO,
                          "Unit update success!", ""));
        } catch (IntegrationException | BObStatusException   ex) {
            System.out.println(ex);
              getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error on unit update: " + ex.getMessage(), ""));
        }
      }
      
      /**
       * Installs a new copy of the current property unit
     * @param ev
       */
      public void refreshCurrentPropertyUnit(ActionEvent ev) {
          PropertyCoordinator pc = getPropertyCoordinator();
          if(currentPropertyUnit != null){  
              try {
                  currentPropertyUnit = pc.getPropertyUnitDataHeavy(pc.getPropertyUnit(currentPropertyUnit.getUnitID()), getSessionBean().getSessUser());
              } catch (EventException | AuthorizationException | BObStatusException | IntegrationException ex) {
                   System.out.println(ex);
              getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error on unit refresh: " + ex.getMessage(), ""));
              } 
          }
      }
      
      /**
       * Listener for user requests to start the unit deac process
       * 
       * @param ev 
       */
      public void onUnitDeacInit(ActionEvent ev){
          if(currentPropertyUnit != null){
                System.out.println("PropertyProfileBB.onUnitDeacInit + unitID: " + currentPropertyUnit.getUnitID());
          } else {
                System.out.println("PropertyProfileBB.onUnitDeacInit | NULL current unit");
          }
      }
      
      
      /**
       * Listener for user requests to start the unit deac process
       * 
       * @param ev 
       */
    public void onUnitDeacCommit(ActionEvent ev){
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            pc.deactivatePropertyUnit(currentPropertyUnit, getSessionBean().getSessUser());
            reloadCurrentPropertyDataHeavy();
            refreshCurrentPropertyUnit(null);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Success! Deactivated unit num: " + currentPropertyUnit.getUnitNumber(), ""));
        } catch (BObStatusException | IntegrationException | AuthorizationException | EventException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error on unit deactivate: " + ex.getMessage(), ""));

        } 
          
    }
    
    /**
     * Listener for user requests to abort the unit deac process
     * @param ev 
     */
    public void onUnitDeacAbort(ActionEvent ev){
        System.out.println("PropertyProfileBB.Unit deac abort!");
    }
      
    
    // ********************************************************
    // **********   GETTERS AND SETTERS ***********************
    // ********************************************************

    /**
     * The exception to the "no logic in getters and setters" rule: 
     * this getter monitors the object
     * reload trigger so we can see changes made by
     * multi-use components such as the person linker or blob linker
     * 
     * @return the currentProperty
     */
    public PropertyDataHeavy getCurrentProperty() {
       
        return currentProperty;
    }

    /**
     * @param currentProperty the currentProperty to set
     */
    public void setCurrentProperty(PropertyDataHeavy currentProperty) {
        this.currentProperty = currentProperty;
    }

    /**
     * @return the putList
     */
    public List<PropertyUseType> getPutList() {
        return putList;
    }

    /**
     * @param putList the putList to set
     */
    public void setPutList(List<PropertyUseType> putList) {
        this.putList = putList;
    }

  
 
    /**
     * @return the occPeriodTypeList
     */
    public List<OccPeriodType> getOccPeriodTypeList() {
        return occPeriodTypeList;
    }

   

    /**
     * @param occPeriodTypeList the occPeriodTypeList to set
     */
    public void setOccPeriodTypeList(List<OccPeriodType> occPeriodTypeList) {
        this.occPeriodTypeList = occPeriodTypeList;
    }

    /**
     * @return the currentOccPeriod
     */
    public OccPeriod getCurrentOccPeriod() {
        return currentOccPeriod;
    }

    /**
     * @param currentOccPeriod the currentOccPeriod to set
     */
    public void setCurrentOccPeriod(OccPeriod currentOccPeriod) {
        this.currentOccPeriod = currentOccPeriod;
    }

    /**
     * @return the muniSelected
     */
    public Municipality getMuniSelected() {
        return muniSelected;
    }

    /**
     * @return the eventViewOptionSelected
     */
    public ViewOptionsActiveHiddenListsEnum getEventViewOptionSelected() {
        return eventViewOptionSelected;
    }

    /**
     * @return the eventViewOptions
     */
    public List<ViewOptionsActiveHiddenListsEnum> getEventViewOptions() {
        return eventViewOptions;
    }

    /**
     * @return the selectedPropertyUseType
     */
    public PropertyUseType getSelectedPropertyUseType() {
        return selectedPropertyUseType;
    }

    /**
     * @return the conditionIntensityList
     */
    public List<IntensityClass> getConditionIntensityList() {
        return conditionIntensityList;
    }

    /**
     * @return the landBankProspectIntensityList
     */
    public List<IntensityClass> getLandBankProspectIntensityList() {
        return landBankProspectIntensityList;
    }

    /**
     * @return the sourceList
     */
    public List<BOBSource> getSourceList() {
        return sourceList;
    }

  

    /**
     * @return the formNoteText
     */
    public String getFormNoteText() {
        return formNoteText;
    }

    /**
     * @return the personSelected
     */
    public Person getPersonSelected() {
        return personSelected;
    }

    /**
     * @return the personToAddList
     */
    public List<Person> getPersonToAddList() {
        return personToAddList;
    }

    /**
     * @return the personLinkUseID
     */
    public boolean isPersonLinkUseID() {
        return personLinkUseID;
    }

    /**
     * @return the humanIDToLink
     */
    public int getHumanIDToLink() {
        return humanIDToLink;
    }

    /**
     * @return the selectedPropViewOption
     */
    public ViewOptionsProposalsEnum getSelectedPropViewOption() {
        return selectedPropViewOption;
    }

    /**
     * @return the currentBlob
     */
    public BlobLight getCurrentBlob() {
        return currentBlob;
    }

    /**
     * @param muniSelected the muniSelected to set
     */
    public void setMuniSelected(Municipality muniSelected) {
        this.muniSelected = muniSelected;
    }

    /**
     * @param eventViewOptionSelected the eventViewOptionSelected to set
     */
    public void setEventViewOptionSelected(ViewOptionsActiveHiddenListsEnum eventViewOptionSelected) {
        this.eventViewOptionSelected = eventViewOptionSelected;
    }

    /**
     * @param eventViewOptions the eventViewOptions to set
     */
    public void setEventViewOptions(List<ViewOptionsActiveHiddenListsEnum> eventViewOptions) {
        this.eventViewOptions = eventViewOptions;
    }

    /**
     * @param selectedPropertyUseType the selectedPropertyUseType to set
     */
    public void setSelectedPropertyUseType(PropertyUseType selectedPropertyUseType) {
        this.selectedPropertyUseType = selectedPropertyUseType;
    }

    /**
     * @param conditionIntensityList the conditionIntensityList to set
     */
    public void setConditionIntensityList(List<IntensityClass> conditionIntensityList) {
        this.conditionIntensityList = conditionIntensityList;
    }

    /**
     * @param landBankProspectIntensityList the landBankProspectIntensityList to set
     */
    public void setLandBankProspectIntensityList(List<IntensityClass> landBankProspectIntensityList) {
        this.landBankProspectIntensityList = landBankProspectIntensityList;
    }

    /**
     * @param sourceList the sourceList to set
     */
    public void setSourceList(List<BOBSource> sourceList) {
        this.sourceList = sourceList;
    }

   

    /**
     * @param formNoteText the formNoteText to set
     */
    public void setFormNoteText(String formNoteText) {
        this.formNoteText = formNoteText;
    }

    /**
     * @param personSelected the personSelected to set
     */
    public void setPersonSelected(Person personSelected) {
        this.personSelected = personSelected;
    }

    /**
     * @param personToAddList the personToAddList to set
     */
    public void setPersonToAddList(List<Person> personToAddList) {
        this.personToAddList = personToAddList;
    }

    /**
     * @param personLinkUseID the personLinkUseID to set
     */
    public void setPersonLinkUseID(boolean personLinkUseID) {
        this.personLinkUseID = personLinkUseID;
    }

    /**
     * @param humanIDToLink the humanIDToLink to set
     */
    public void setHumanIDToLink(int humanIDToLink) {
        this.humanIDToLink = humanIDToLink;
    }

    /**
     * @param selectedPropViewOption the selectedPropViewOption to set
     */
    public void setSelectedPropViewOption(ViewOptionsProposalsEnum selectedPropViewOption) {
        this.selectedPropViewOption = selectedPropViewOption;
    }

    /**
     * @param currentBlob the currentBlob to set
     */
    public void setCurrentBlob(BlobLight currentBlob) {
        this.currentBlob = currentBlob;
    }

    /**
     * @return the currentPropertyUnit
     */
    public PropertyUnitDataHeavy getCurrentPropertyUnit() {
        return currentPropertyUnit;
    }

    /**
     * @param currentPropertyUnit the currentPropertyUnit to set
     */
    public void setCurrentPropertyUnit(PropertyUnitDataHeavy currentPropertyUnit) {
        this.currentPropertyUnit = currentPropertyUnit;
    }

    /**
     * @return the currentPropertyEditMode
     */
    public boolean isCurrentPropertyEditMode() {
        return currentPropertyEditMode;
    }

    /**
     * @return the currentParcelInfoEditMode
     */
    public boolean isCurrentParcelInfoEditMode() {
        return currentParcelInfoEditMode;
    }

    /**
     * @param currentPropertyEditMode the currentPropertyEditMode to set
     */
    public void setCurrentPropertyEditMode(boolean currentPropertyEditMode) {
        this.currentPropertyEditMode = currentPropertyEditMode;
    }

    /**
     * @param currentParcelInfoEditMode the currentParcelInfoEditMode to set
     */
    public void setCurrentParcelInfoEditMode(boolean currentParcelInfoEditMode) {
        this.currentParcelInfoEditMode = currentParcelInfoEditMode;
    }

    /**
     * @return the currentCECase
     */
    public CECase getCurrentCECase() {
        return currentCECase;
    }

    

    /**
     * @param currentCECase the currentCECase to set
     */
    public void setCurrentCECase(CECase currentCECase) {
        this.currentCECase = currentCECase;
    }

    /**
     * @return the occPeriodOriginiationEventCandidateList
     */
    public List<EventCategory> getOccPeriodOriginiationEventCandidateList() {
        return occPeriodOriginiationEventCandidateList;
    }

    /**
     * @param occPeriodOriginiationEventCandidateList the occPeriodOriginiationEventCandidateList to set
     */
    public void setOccPeriodOriginiationEventCandidateList(List<EventCategory> occPeriodOriginiationEventCandidateList) {
        this.occPeriodOriginiationEventCandidateList = occPeriodOriginiationEventCandidateList;
    }

    /**
     * @return the unitEditMode
     */
    public boolean isUnitEditMode() {
        return unitEditMode;
    }

    /**
     * @param unitEditMode the unitEditMode to set
     */
    public void setUnitEditMode(boolean unitEditMode) {
        this.unitEditMode = unitEditMode;
    }

    /**
     * @return the formUnitNoteText
     */
    public String getFormUnitNoteText() {
        return formUnitNoteText;
    }

    /**
     * @param formUnitNoteText the formUnitNoteText to set
     */
    public void setFormUnitNoteText(String formUnitNoteText) {
        this.formUnitNoteText = formUnitNoteText;
    }

    /**
     * @return the selectedBoadviewPhoto
     */
    public BlobLight getSelectedBoadviewPhoto() {
        return selectedBoadviewPhoto;
    }

    /**
     * @param selectedBoadviewPhoto the selectedBoadviewPhoto to set
     */
    public void setSelectedBoadviewPhoto(BlobLight selectedBoadviewPhoto) {
        this.selectedBoadviewPhoto = selectedBoadviewPhoto;
    }

    /**
     * @return the broadViewPhotoCandidates
     */
    public List<BlobLight> getBroadViewPhotoCandidates() {
        return broadViewPhotoCandidates;
    }

    /**
     * @param broadViewPhotoCandidates the broadViewPhotoCandidates to set
     */
    public void setBroadViewPhotoCandidates(List<BlobLight> broadViewPhotoCandidates) {
        this.broadViewPhotoCandidates = broadViewPhotoCandidates;
    }

    /**
     * @return the permissionAllowParcelEdit
     */
    public boolean isPermissionAllowParcelEdit() {
        return permissionAllowParcelEdit;
    }

    /**
     * @param permissionAllowParcelEdit the permissionAllowParcelEdit to set
     */
    public void setPermissionAllowParcelEdit(boolean permissionAllowParcelEdit) {
        this.permissionAllowParcelEdit = permissionAllowParcelEdit;
    }

  

    /**
     * @return the permissionAllowOccPeriodCreate
     */
    public boolean isPermissionAllowOccPeriodCreate() {
        return permissionAllowOccPeriodCreate;
    }

    /**
     * @return the permissionAllowCECaseCreate
     */
    public boolean isPermissionAllowCECaseCreate() {
        return permissionAllowCECaseCreate;
    }

    /**
     * @param permissionAllowCECaseCreate the permissionAllowCECaseCreate to set
     */
    public void setPermissionAllowCECaseCreate(boolean permissionAllowCECaseCreate) {
        this.permissionAllowCECaseCreate = permissionAllowCECaseCreate;
    }

    /**
     * @return the parcelIDBeforeEdit
     */
    public String getParcelIDBeforeEdit() {
        return parcelIDBeforeEdit;
    }

    /**
     * @param parcelIDBeforeEdit the parcelIDBeforeEdit to set
     */
    public void setParcelIDBeforeEdit(String parcelIDBeforeEdit) {
        this.parcelIDBeforeEdit = parcelIDBeforeEdit;
    }

    /**
     * @return the formEventNotes
     */
    public String getFormEventNotes() {
        return formEventNotes;
    }

    /**
     * @param formEventNotes the formEventNotes to set
     */
    public void setFormEventNotes(String formEventNotes) {
        this.formEventNotes = formEventNotes;
    }

    /**
     * @return the occPeriodInitSelectedUnit
     */
    public PropertyUnit getOccPeriodInitSelectedUnit() {
        return occPeriodInitSelectedUnit;
    }

    /**
     * @param occPeriodInitSelectedUnit the occPeriodInitSelectedUnit to set
     */
    public void setOccPeriodInitSelectedUnit(PropertyUnit occPeriodInitSelectedUnit) {
        this.occPeriodInitSelectedUnit = occPeriodInitSelectedUnit;
    }

    /**
     * @return the occPeriodInitCreateNewUnitWithNumber
     */
    public boolean isOccPeriodInitCreateNewUnitWithNumber() {
        return occPeriodInitCreateNewUnitWithNumber;
    }

    /**
     * @param occPeriodInitCreateNewUnitWithNumber the occPeriodInitCreateNewUnitWithNumber to set
     */
    public void setOccPeriodInitCreateNewUnitWithNumber(boolean occPeriodInitCreateNewUnitWithNumber) {
        this.occPeriodInitCreateNewUnitWithNumber = occPeriodInitCreateNewUnitWithNumber;
    }

    /**
     * @return the occPeriodInitNewUnitNumberForm
     */
    public String getOccPeriodInitNewUnitNumberForm() {
        return occPeriodInitNewUnitNumberForm;
    }

    /**
     * @param occPeriodInitNewUnitNumberForm the occPeriodInitNewUnitNumberForm to set
     */
    public void setOccPeriodInitNewUnitNumberForm(String occPeriodInitNewUnitNumberForm) {
        this.occPeriodInitNewUnitNumberForm = occPeriodInitNewUnitNumberForm;
    }

    /**
     * @return the occPeriodInitDifferentManagerSelected
     */
    public User getOccPeriodInitDifferentManagerSelected() {
        return occPeriodInitDifferentManagerSelected;
    }

    /**
     * @param occPeriodInitDifferentManagerSelected the occPeriodInitDifferentManagerSelected to set
     */
    public void setOccPeriodInitDifferentManagerSelected(User occPeriodInitDifferentManagerSelected) {
        this.occPeriodInitDifferentManagerSelected = occPeriodInitDifferentManagerSelected;
    }

  
    
}
