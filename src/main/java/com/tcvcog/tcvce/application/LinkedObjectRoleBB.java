/*
 * Copyright (C) 2018 Adam Gutonski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.LinkedObjectRole;
import com.tcvcog.tcvce.entities.LinkedObjectSchemaEnum;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.bean.ViewScoped;
import jakarta.faces.event.ActionEvent;

/**
 * @author atulr
 */
@ViewScoped
public class LinkedObjectRoleBB extends BackingBeanUtils implements Serializable {
    
    private List<LinkedObjectRole> linkedObjectRoleList;
    private LinkedObjectRole currentLinkedObjectRole;

    private boolean editModeLinkedObjectRole;
    private boolean createModeLinkedObjectRole;

    private boolean includeInactiveLinkedObjectRole;
    private List<LinkedObjectSchemaEnum> schemaEnumList;

    @PostConstruct
    public void initBean() {
        System.out.println("LinkedObjectRoleBB: Init");
        includeInactiveLinkedObjectRole = false;
        createModeLinkedObjectRole = false;
        editModeLinkedObjectRole = false;
        refreshSchemaList();
        refreshLinkedObjectRoleList();
        
        if (Objects.nonNull(linkedObjectRoleList) && !linkedObjectRoleList.isEmpty()) {
            currentLinkedObjectRole = linkedObjectRoleList.get(0);
        }
    }

    /**
     * Loads a fresh lists of linked object role entities from the db
     */
    public void refreshLinkedObjectRoleList() {
        SystemCoordinator sc = getSystemCoordinator();
        try {
            linkedObjectRoleList = sc.getLinkedObjectRolesList(!includeInactiveLinkedObjectRole);
        } catch (IntegrationException | BObStatusException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to load LinkedObjectRolesList",
                            "This must be corrected by the system administrator"));
        }
    }

    public void refreshSchemaList() {
        SystemCoordinator sc = getSystemCoordinator();
        try {
            schemaEnumList = sc.getLinkedObjectSchemaEnumList();
        } catch (BObStatusException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to load schemaEnumList",
                            "This must be corrected by the system administrator"));
        }
    }
    /**
     * Extracts a fresh copy of the LinkedObjectRole from the db
     */
    public void refreshCurrentLinkedObjectRole() {
        SystemCoordinator sc = getSystemCoordinator();

        if (Objects.nonNull(currentLinkedObjectRole)) {
            try {
                currentLinkedObjectRole = sc.getLinkedObjectRole(currentLinkedObjectRole.getRoleID());
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Refreshed LinkedObjectRole: " + currentLinkedObjectRole.getRoleID(), ""));
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Unable to extract LinkedObjectRole from DB", ""));
            }
        }
    }

    public void onInsertLinkedObjectRole() {
        SystemCoordinator sc = getSystemCoordinator();
        try {
            currentLinkedObjectRole = sc.insertLinkedObjectRole(currentLinkedObjectRole, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Successful Insert New LinkedObjectRole!", ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
        }
    }

    public void onUpdateLinkedObjectRole() {
        SystemCoordinator sc = getSystemCoordinator();
        try {
            sc.updateLinkedObjectRole(currentLinkedObjectRole, getSessionBean().getSessUser());
            refreshCurrentLinkedObjectRole();
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "LinkedObjectRole Updated with ID " + currentLinkedObjectRole.getRoleID(), ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Failed to Update LinkedObjectRole", ""));
        }
    }

    /**
     * Listener for users to click the add or the edit button
     *
     * @param ev
     */
    public void toggleLinkedObjectRoleAddEditButtonChange(ActionEvent ev) {
        System.out.println("toggleLinkedObjectRoleAddEditButtonChange");
        if (editModeLinkedObjectRole) {
            if (Objects.nonNull(currentLinkedObjectRole) && currentLinkedObjectRole.getRoleID() == 0) {
                onInsertLinkedObjectRole();
            } else {
                onUpdateLinkedObjectRole();
                System.out.println(currentLinkedObjectRole.getRoleID());
            }
            refreshLinkedObjectRoleList();
        }
        editModeLinkedObjectRole = !editModeLinkedObjectRole;
    }

    /**
     * Listener for user requests to view a linkedObjectRole
     *
     * @param loRole
     */
    public void viewLinkedObjectRole(LinkedObjectRole loRole) {
        currentLinkedObjectRole = loRole;
        System.out.println("View LinkedObjectRole: " + loRole.getRoleID());
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Viewing LinkedObjectRole: " + loRole.getRoleID(), ""));
    }

    /**
     * Begins the LinkedObjectRole creation process
     *
     * @param ev
     */
    public void onLinkedObjectRoleAddInit(ActionEvent ev) {
        System.out.println("LinkedObjectRoleBB.onLinkedObjectRoleAddInit");
        SystemCoordinator sc = getSystemCoordinator();
        currentLinkedObjectRole = sc.getLinkedObjectRoleSkeleton(getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
        editModeLinkedObjectRole = true;
    }

    /**
     * Listener for users to cancel their linkedObjectRole add/edit operation
     *
     * @param ev
     */
    public void onOperationLinkedObjectRoleAddEditAbort(ActionEvent ev) {
        System.out.println("LinkedObjectRoleBB.onOperationLinkedObjectRoleAddEditAbort: ABORT");
        editModeLinkedObjectRole = false;
        createModeLinkedObjectRole = false;
    }

    /**
     * Listener for user request to start the nuking process of a LinkedObjectRole
     *
     * @param loRole
     */
    public void onLinkedObjectRoleNukeInitButtonChange(LinkedObjectRole loRole) {
        currentLinkedObjectRole = loRole;
    }

    /**
     * Listener for user requests to commit a linkedObjectRole nuke operation
     */
    public void onLinkedObjectRoleNukeCommitButtonChange() {
        SystemCoordinator sc = getSystemCoordinator();
        try {
            sc.deactivateLinkedObjectRole(currentLinkedObjectRole, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
            refreshLinkedObjectRoleList();
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "LinkedObjectRole deactivated with ID: " + currentLinkedObjectRole.getRoleID(), ""));
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
        }
    }

    public List<LinkedObjectRole> getLinkedObjectRoleList() {
        return linkedObjectRoleList;
    }

    public void setLinkedObjectRoleList(List<LinkedObjectRole> linkedObjectRoleList) {
        this.linkedObjectRoleList = linkedObjectRoleList;
    }

    public LinkedObjectRole getCurrentLinkedObjectRole() {
        return currentLinkedObjectRole;
    }

    public void setCurrentLinkedObjectRole(LinkedObjectRole currentLinkedObjectRole) {
        this.currentLinkedObjectRole = currentLinkedObjectRole;
    }

    public boolean isEditModeLinkedObjectRole() {
        return editModeLinkedObjectRole;
    }

    public void setEditModeLinkedObjectRole(boolean editModeLinkedObjectRole) {
        this.editModeLinkedObjectRole = editModeLinkedObjectRole;
    }

    public boolean isCreateModeLinkedObjectRole() {
        return createModeLinkedObjectRole;
    }

    public void setCreateModeLinkedObjectRole(boolean createModeLinkedObjectRole) {
        this.createModeLinkedObjectRole = createModeLinkedObjectRole;
    }

    public boolean isIncludeInactiveLinkedObjectRole() {
        return includeInactiveLinkedObjectRole;
    }

    public void setIncludeInactiveLinkedObjectRole(boolean includeInactiveLinkedObjectRole) {
        this.includeInactiveLinkedObjectRole = includeInactiveLinkedObjectRole;
    }

    public List<LinkedObjectSchemaEnum> getSchemaEnumList() {
        return schemaEnumList;
    }

    public void setSchemaEnumList(List<LinkedObjectSchemaEnum> schemaEnumList) {
        this.schemaEnumList = schemaEnumList;
    }

    
}
