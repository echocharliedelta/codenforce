/*
 * Copyright (C) 2018 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.tcvcog.tcvce.application;
import com.tcvcog.tcvce.session.SessionBean;
import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.coordinators.SearchCoordinator;
import com.tcvcog.tcvce.coordinators.UserCoordinator;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.User;
import com.tcvcog.tcvce.integration.CodeIntegrator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.entities.CECase;
import com.tcvcog.tcvce.entities.CECase;
import com.tcvcog.tcvce.entities.EventCnF;
import com.tcvcog.tcvce.entities.HumanLink;
import com.tcvcog.tcvce.entities.MunicipalityDataHeavy;
import com.tcvcog.tcvce.entities.Property;
import com.tcvcog.tcvce.entities.ProposalCECase;
import com.tcvcog.tcvce.entities.ProposalOccPeriod;
import com.tcvcog.tcvce.entities.occupancy.OccPeriod;
import com.tcvcog.tcvce.integration.MunicipalityIntegrator;
import java.sql.SQLException;
import jakarta.annotation.PostConstruct;
import jakarta.faces.event.ActionEvent;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.pie.PieChartDataSet;
import org.primefaces.model.charts.pie.PieChartModel;

/**
 *
 * @author ellen bascomb of apt 31y
 */
public class MissionControlBB extends BackingBeanUtils implements Serializable {
    
    private User currentUser;
    private MunicipalityDataHeavy currentMuni;
    private Municipality selectedMuni;
    
    private List<User> userList;
    private User selectedUser;
    
    
    private List<EventCnF> filteredEventWithCasePropList;
    private int timelineEventViewDateRange;
    
    private List<ProposalCECase> ceProposalList;
    private List<ProposalOccPeriod> occProposalList;
    
    private List<CECase> filteredCaseList;
    
    private PieChartModel pieProperty;
    private PieChartModel pieCasePhase;
    private PieChartModel pieEvents;
    
    
    private String mapThumbFilePath; //added by wwalk 12.30.22
    /*
    static final String MAP_THUMB_DEFAULT_PATH = "/images/map_thumbnails/default.png"; //added by wwalk 12.30.22
    */

    /**
     * Creates a new instance of InitiateSessionBB
     */
    public MissionControlBB() {
    }
    
    @PostConstruct
    public void initBean() {
        UserCoordinator uc = getUserCoordinator();
        currentUser = getSessionBean().getSessUser();
        mapThumbFilePath = mapThumbFilePath + ".png"; //added by wwalk 3.28.23, LAST TRIED TO ASSIGN VARIABLE UPON DECLARATION
        System.out.println("THIS IS THE MAP THUMBNAIL FILEPATH: " + mapThumbFilePath); //THIS IS FOR TESTING, REMOVE BEFORE PUSH
        //get the municipality from the session bean
        //build private method that returns the map of muni codes to image names
        //use muni code to look up image file path in mapping
        //inject into member variable mapThumbFilePath
        //make sure your have a file name, if not, inject the default
        filteredCaseList = null;
    }
    
      private void initPieProperty(){
        setPieProperty(new PieChartModel());
        ChartData pieData = new ChartData();
        
        PieChartDataSet dataSet = new PieChartDataSet();
        List<Number> propValues = new ArrayList<>();
        
        propValues.add(344);
        propValues.add(23);
        propValues.add(103);
        
        dataSet.setData(propValues);
        
        List<String> pieColors = new ArrayList<>();
        pieColors.add("rgb(200,100,33)");
        pieColors.add("rgb(100,0,33)");
        pieColors.add("rgb(20,40,233)");
        dataSet.setBackgroundColor(pieColors);
        
        pieData.addChartDataSet(dataSet);
        List<String> labels = new ArrayList<>();
        labels.add("Rentals");
        labels.add("Commercial");
        labels.add("Owner-occupied");
        
        pieData.setLabels(labels);
        getPieProperty().setData(pieData);
    }
    
      private void initPieCasePhase(){
         pieCasePhase = new PieChartModel();
        
        ChartData pieData = new ChartData();
        
        PieChartDataSet dataSet = new PieChartDataSet();
        List<Number> propValues = new ArrayList<>();
        
        propValues.add(8);
        propValues.add(56);
        propValues.add(21);
        propValues.add(30);
        
        dataSet.setData(propValues);
        
        List<String> pieColors = new ArrayList<>();
        pieColors.add("rgb(200,100,33)");
        pieColors.add("rgb(100,0,33)");
        pieColors.add("rgb(20,40,233)");
        pieColors.add("rgb(80,50,133)");
        dataSet.setBackgroundColor(pieColors);
        
        pieData.addChartDataSet(dataSet);
        List<String> labels = new ArrayList<>();
        labels.add("Investigation");
        labels.add("Enforcement");
        labels.add("Citation");
        labels.add("Closed");
        
        pieData.setLabels(labels);
        pieCasePhase.setData(pieData);
    }
    
      private void initPieEvents(){
        pieEvents = new PieChartModel();
        
        ChartData pieData = new ChartData();
        
        PieChartDataSet dataSet = new PieChartDataSet();
        List<Number> propValues = new ArrayList<>();
        
        propValues.add(8);
        propValues.add(12);
        propValues.add(15);
        propValues.add(18);
        propValues.add(30);
        propValues.add(50);
        propValues.add(55);
        
        dataSet.setData(propValues);
        
        List<String> pieColors = new ArrayList<>();
        pieColors.add("rgb(200,100,33)");
        pieColors.add("rgb(100,0,33)");
        pieColors.add("rgb(20,40,233)");
        pieColors.add("rgb(80,50,133)");
        pieColors.add("rgb(10,60,123)");
        pieColors.add("rgb(20,50,153)");
        pieColors.add("rgb(30,40,163)");
        dataSet.setBackgroundColor(pieColors);
        
        pieData.addChartDataSet(dataSet);
        List<String> labels = new ArrayList<>();
        labels.add("Field inspection");
        labels.add("Case management");
        labels.add("Violation Compliance");
        labels.add("Citation");
        labels.add("Communication");
        labels.add("Meeting");
        labels.add("Citation");
        
        pieData.setLabels(labels);
        pieEvents.setData(pieData);
    }
      
    
 
    
    
    public String jumpToPublicPortal(){
        return "publicPortal";
    }
    
  
    
    /**
     * Listener for user requests to view a CECase
     * @param cse
     * @return 
     */
    public String onViewCECaseButtonChange(CECase cse){
        if(cse != null){
            System.out.println("view case " + cse.getCaseID());
            getSessionBean().setSessCECase(cse);
        }
        try {
            return getSessionBean().navigateToPageCorrespondingToObject(getSessionBean().getSessCECase());
        } catch (BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
        } 
        return "";
    }
    
    /**
     * Dummy listener for users viewing their permissions profile
     * @param ev 
     */
    public void onViewPermissionsLinkClick(ActionEvent ev){
        System.out.println("MissionControlBB.onViewPermissionsLinkClick");
        
    }
    /**
     * Listener for user request to explore a property
     * @param prop
     * @return 
     */
    public String onViewPropertyButtonChange(Property prop){
        getSessionBean().setSessProperty(prop);
        try {
            return getSessionBean().navigateToPageCorrespondingToObject(getSessionBean().getSessProperty());
        } catch (BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
        } 
        return "";
    }
    
    
    /**
     * Listener for clicks of an occ period from the dashboard searches
     * @param per
     * @return 
     */
    public String onViewOccPeriodButtonChange(OccPeriod per){
        
        if(per != null){
            try {
                return getSessionBean().navigateToPageCorrespondingToObject(per);
            } catch (BObStatusException | AuthorizationException ex) {
                System.out.println(ex);
                FacesContext facesContext = getFacesContext();
                        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                            ex.getMessage(), ""));
            
            }
        }
        return "";
    }
    
    /**
     * Listener for user requests to view an event from the dashboard 
     * on the events page
     * @param ev
     * @return 
     */
    public String onViewEventButtonChange(EventCnF ev){
        getSessionEventConductor().setSessEvent(ev);
        return "events";
        
    }
    
    /**
     * No longe implement such magical powers
     * @Deprecated
     * @return 
     */
    public String switchToUser(){
        System.out.println("MissionControlBB.switchToUser");
        if(selectedUser != null){
            getSessionBean().setUserForReInit(selectedUser);
        } else {
            getSessionBean().setUserForReInit(getSessionBean().getSessUser());
        }
        return "sessionReinit";
    }
    
    /**
     * Called to route user back to the UMAP selection screen;
     * I also clear all the relevant session objects.
     * @return 
     */
    public String reauthenticate(){
        System.out.println("MissionControlBB.reauthenticate");
        getSessionConductor().clearSessionObjectsForSessionReInit();
        return "sessionReinit";
    }
    
     /**
     * Special wrapper getter around the current property's human
     * link list that asks the session for a new link list
     * on table load that might occur during a link edit operation
     * @return the new human link list
     */
    public List<HumanLink> getManagedHumanLinkList(){
        List<HumanLink> hll = getSessionBean().getSessHumanListRefreshedList();
        if(hll != null){
            currentMuni.sethumanLinkList(hll);
            // clear our refreshed list
            getSessionBean().setSessHumanListRefreshedList(null);
        } else {
            System.out.println("MissionControlBB.getManagedHumanLinkList | emtpy session human links list" );
        }
        return currentMuni.gethumanLinkList();
    }
   
    
    /**
     * @return the user
     */
    public User getCurrentUser() {
        
        return currentUser;
    }

    /**
     * @param currentUser the user to set
     */
    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    /**
     * @return the currentMuni
     */
    public MunicipalityDataHeavy getCurrentMuni() {
        return currentMuni;
    }

    /**
     * @param currentMuni the currentMuni to set
     */
    public void setCurrentMuni(MunicipalityDataHeavy currentMuni) {
        this.currentMuni = currentMuni;
    }

    

    

    /**
     * @return the selectedMuni
     */
    public Municipality getSelectedMuni() {
        return selectedMuni;
    }

    /**
     * @param selectedMuni the selectedMuni to set
     */
    public void setSelectedMuni(Municipality selectedMuni) {
        this.selectedMuni = selectedMuni;
    }

    
    /**
     * @return the filteredEventWithCasePropList
     */
    public List<EventCnF> getFilteredEventWithCasePropList() {
        return filteredEventWithCasePropList;
    }

    /**
     * @param filteredEventWithCasePropList the filteredEventWithCasePropList to set
     */
    public void setFilteredEventWithCasePropList(List<EventCnF> filteredEventWithCasePropList) {
        this.filteredEventWithCasePropList = filteredEventWithCasePropList;
    }

    /**
     * @return the timelineEventViewDateRange
     */
    public int getTimelineEventViewDateRange() {
        return timelineEventViewDateRange;
    }

    /**
     * @param timelineEventViewDateRange the timelineEventViewDateRange to set
     */
    public void setTimelineEventViewDateRange(int timelineEventViewDateRange) {
        this.timelineEventViewDateRange = timelineEventViewDateRange;
    }

  


    /**
     * @return the occProposalList
     */
    public List<ProposalOccPeriod> getOccProposalList() {
        return occProposalList;
    }

    /**
     * @param occProposalList the occProposalList to set
     */
    public void setOccProposalList(List<ProposalOccPeriod> occProposalList) {
        this.occProposalList = occProposalList;
    }

    /**
     * @return the ceProposalList
     */
    public List<ProposalCECase> getCeProposalList() {
        return ceProposalList;
    }

    /**
     * @param ceProposalList the ceProposalList to set
     */
    public void setCeProposalList(List<ProposalCECase> ceProposalList) {
        this.ceProposalList = ceProposalList;
    }

    /**
     * @return the selectedUser
     */
    public User getSelectedUser() {
        return selectedUser;
    }

    /**
     * @param selectedUser the selectedUser to set
     */
    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }

    /**
     * @return the filteredCaseList
     */
    public List<CECase> getFilteredCaseList() {
        return filteredCaseList;
    }

    /**
     * @param filteredCaseList the filteredCaseList to set
     */
    public void setFilteredCaseList(List<CECase> filteredCaseList) {
        this.filteredCaseList = filteredCaseList;
    }

    /**
     * @return the pieProperty
     */
    public PieChartModel getPieProperty() {
        return pieProperty;
    }

    /**
     * @param pieProperty the pieProperty to set
     */
    public void setPieProperty(PieChartModel pieProperty) {
        this.pieProperty = pieProperty;
    }

    /**
     * @return the pieCasePhase
     */
    public PieChartModel getPieCasePhase() {
        return pieCasePhase;
    }

    /**
     * @return the pieEvents
     */
    public PieChartModel getPieEvents() {
        return pieEvents;
    }

    /**
     * @return the mapThumbFilePath
     */
    public String getMapThumbFilePath() {
        return mapThumbFilePath;
    }

    /**
     * @param mapThumbFilePath the mapThumbFilePath to set
     */
    public void setMapThumbFilePath(String mapThumbFilePath) {
        this.mapThumbFilePath = mapThumbFilePath;
    }


    

   
}
