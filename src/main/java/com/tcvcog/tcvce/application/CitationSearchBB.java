package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.coordinators.SearchCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.domain.SearchException;
import com.tcvcog.tcvce.entities.CitationCECasePropertyHeavy;
import com.tcvcog.tcvce.entities.CitationStatus;
import com.tcvcog.tcvce.entities.CitationViolationStatusEnum;
import com.tcvcog.tcvce.entities.EnforceableCodeElement;
import com.tcvcog.tcvce.entities.search.QueryCitation;
import com.tcvcog.tcvce.entities.search.SearchParamsCitation;
import com.tcvcog.tcvce.session.SessionBean;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class CitationSearchBB extends BackingBeanUtils {
    
    private QueryCitation querySelected;
    private List<QueryCitation> queryList;
    private SearchParamsCitation searchParamsSelected;
    private boolean appendResultsToList;
    
    private List<CitationViolationStatusEnum> citationViolationStatusEnumList;
    private List<CitationStatus> citationStatusList;
    private List<EnforceableCodeElement> enfCodeElementList;
    
    private List<CitationCECasePropertyHeavy> citationList;
    private EnforceableCodeElement selectedCodeElement;
    
    @PostConstruct
    public void initBean() {
        SearchCoordinator sc = getSearchCoordinator();
        CaseCoordinator cc = getCaseCoordinator();
        queryList = sc.buildQueryCitationList(getSessionBean().getSessUser().getMyCredential());
        enfCodeElementList = getSessionBean().getSessCodeSet().getEnfCodeElementList();
        appendResultsToList = false;
        
        if (Objects.isNull(querySelected) && !queryList.isEmpty())
        {
            querySelected = queryList.stream().findFirst().orElse(null);
            setSearchParamsSelected(querySelected.getParamsList().get(0));
        }
        
        try
        {
            citationStatusList = cc.citation_getCitationStatusList(false);
            if (Objects.nonNull(citationStatusList))
            {
                // Sort the list based on statusTitle
                Collections.sort(citationStatusList, (CitationStatus o1, CitationStatus o2) -> o1.getStatusTitle().compareTo(o2.getStatusTitle()));
            }
            
        } catch (IntegrationException | BObStatusException ex)
        {
            System.out.println(ex.toString());
        }
        citationList = new ArrayList<>();
        citationViolationStatusEnumList = Arrays.asList(CitationViolationStatusEnum.values());
        if (Objects.nonNull(citationViolationStatusEnumList) && !citationViolationStatusEnumList.isEmpty())
        {
            // Sort the enum values based on labels
            Collections.sort(citationViolationStatusEnumList, (CitationViolationStatusEnum o1, CitationViolationStatusEnum o2) -> o1.getLabel().compareTo(o2.getLabel()));
        }
        
    }

    /**
     * Action listener for the user's request to run the query
     *
     * @param event
     */
    public void executeQuery(ActionEvent event) {
        SearchCoordinator sc = getSearchCoordinator();
        searchParamsSelected.setCodeElement_val(selectedCodeElement);
        if (Objects.nonNull(querySelected))
        {
            System.out.println("CitationSearchBB.executeQuery | querySelected: " + querySelected.getQueryTitle());
        }
        List<CitationCECasePropertyHeavy> cList;
        try
        {
            cList = sc.runQuery(querySelected, getSessionBean().getSessUser()).getBOBResultList();
            if (!appendResultsToList && Objects.nonNull(citationList))
            {
                citationList.clear();
            }
            if (Objects.nonNull(cList) && !cList.isEmpty())
            {
                citationList.addAll(cList);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Your search completed with " + cList.size() + " results", ""));
            } else
            {
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Your search had no results", ""));
            }
        } catch (SearchException | IntegrationException | BObStatusException ex)
        {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to complete search! ", ""));
        }
    }

    /**
     * Listener method for changes in the selected query; Updates search params
     * and UI updates based on this changed value
     */
    public void changeQuerySelected() {
        System.out.println("CitationSearchBB.changeQuerySelected | querySelected: " + querySelected.getQueryTitle());
        configureParameters();
        
    }

    /**
     * Sets up search parameters for properties
     */
    private void configureParameters() {
        if (Objects.nonNull(querySelected)
                && Objects.nonNull(querySelected.getParamsList())
                && !querySelected.getParamsList().isEmpty())
        {
            setSearchParamsSelected(querySelected.getParamsList().get(0));
        }
        
    }

    /**
     * Event listener for resetting a query after it's run
     *
     * @param event
     */
    public void resetQuery(ActionEvent event) {
        SearchCoordinator sc = getSearchCoordinator();
        queryList = sc.buildQueryCitationList(getSessionBean().getSessUser().getMyCredential());
        if (Objects.nonNull(queryList) && !queryList.isEmpty())
        {
            querySelected = queryList.stream().findFirst().orElse(null);
        }
        if (appendResultsToList == false)
        {
            if (Objects.nonNull(citationList) && !citationList.isEmpty())
            {
                citationList.clear();
            }
        }
        selectedCodeElement = null;
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Query reset ", ""));
        
        configureParameters();
    }

    /**
     * Event listener for clear citation list
     *
     * @param ev
     */
    public void clearCitationList(ActionEvent ev) {
        if (Objects.nonNull(citationList) && !citationList.isEmpty())
        {
            citationList.clear();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Event List Reset!", ""));
        }
        
    }

    /**
     * method for navigating page to parent CECase
     *
     * @param heavy
     * @return navTo
     */
    public String onCitationViewCeCase(CitationCECasePropertyHeavy heavy) {
        CaseCoordinator cc = getCaseCoordinator();
        SessionBean sb = getSessionBean();
        String navTo = "";
        try
        {
            navTo = sb.navigateToPageCorrespondingToObject(cc.cecase_getCECase(heavy.getCecaseID(), getSessionBean().getSessUser()));
        } catch (IntegrationException | BObStatusException | AuthorizationException ex)
        {
            System.out.println(ex.toString());
        }
        if (navTo.equals(""))
        {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Cannot load CECase for Citation ID:" + heavy.getCitationID(), ""));
        }
        return navTo;
    }
    
    public void storeSelectedElement(ActionEvent ev) {
        if (Objects.nonNull(selectedCodeElement))
        {
            searchParamsSelected.setCodeElement_val(selectedCodeElement);
        }
    }

    /**
     * Listener for user requests to view the citation log
     *
     * @param ev
     */
    public void onViewCitationLogLinkClick(ActionEvent ev) {
        System.out.println("CitationSearchBB.onViewCitationLogLinkClick");
    }
    
    public QueryCitation getQuerySelected() {
        return querySelected;
    }
    
    public void setQuerySelected(QueryCitation querySelected) {
        this.querySelected = querySelected;
    }
    
    public List<QueryCitation> getQueryList() {
        return queryList;
    }
    
    public void setQueryList(List<QueryCitation> queryList) {
        this.queryList = queryList;
    }
    
    public SearchParamsCitation getSearchParamsSelected() {
        return searchParamsSelected;
    }
    
    public void setSearchParamsSelected(SearchParamsCitation searchParamsSelected) {
        this.searchParamsSelected = searchParamsSelected;
    }
    
    public boolean isAppendResultsToList() {
        return appendResultsToList;
    }
    
    public void setAppendResultsToList(boolean appendResultsToList) {
        this.appendResultsToList = appendResultsToList;
    }
    
    public List<CitationViolationStatusEnum> getCitationViolationStatusEnumList() {
        return citationViolationStatusEnumList;
    }
    
    public void setCitationViolationStatusEnumList(List<CitationViolationStatusEnum> citationViolationStatusEnumList) {
        this.citationViolationStatusEnumList = citationViolationStatusEnumList;
    }
    
    public List<CitationStatus> getCitationStatusList() {
        return citationStatusList;
    }
    
    public void setCitationStatusList(List<CitationStatus> citationStatusList) {
        this.citationStatusList = citationStatusList;
    }
    
    public List<EnforceableCodeElement> getEnfCodeElementList() {
        return enfCodeElementList;
    }
    
    public void setEnfCodeElementList(List<EnforceableCodeElement> enfCodeElementList) {
        this.enfCodeElementList = enfCodeElementList;
    }
    
    public List<CitationCECasePropertyHeavy> getCitationList() {
        return citationList;
    }
    
    public void setCitationList(List<CitationCECasePropertyHeavy> citationList) {
        this.citationList = citationList;
    }
    
    public EnforceableCodeElement getSelectedCodeElement() {
        return selectedCodeElement;
    }
    
    public void setSelectedCodeElement(EnforceableCodeElement selectedCodeElement) {
        this.selectedCodeElement = selectedCodeElement;
    }
    
}
