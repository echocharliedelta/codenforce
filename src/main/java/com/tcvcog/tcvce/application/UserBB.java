/*
 * Copyright (C) 2018 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application;


import com.tcvcog.tcvce.coordinators.PersonCoordinator;
import com.tcvcog.tcvce.coordinators.UserCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.Human;
import com.tcvcog.tcvce.entities.Person;
import com.tcvcog.tcvce.entities.User;
import com.tcvcog.tcvce.entities.UserAuthorized;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;
import java.time.LocalDateTime;

/**
 * SECURITY CRITICAL:
 * Contains password reset listeners
 * 
 * @author ellen bascomb of apt 31y
 */

public class UserBB extends BackingBeanUtils implements Serializable {

    private UserAuthorized currentUser;
   
    private String formPassword;
    private String formPasswordReentry;
    
    private String formNotes;
    
    private LocalDateTime passwordResetTimestamp;
    
    /**
     * Creates a new instance of UserBB
     */
    public UserBB() {
    }
    
    @PostConstruct
    public void initBean(){
        currentUser = getSessionBean().getSessUser();
        passwordResetTimestamp = getSessionBean().getPasswordResetNoticeTS();
    }
    
   
    
    /**
     * Pass through method called when user settings dialog is displayed
     * @param ev 
     */
    public void initiateUserUpdates(ActionEvent ev){
        currentUser = getSessionBean().getSessUser();
    }
    
  
    
    /**
     * Listener for committing user person updates
     * @param ev 
     */
    public void commitUserHumanLinkUpdates(ActionEvent ev){
        UserCoordinator uc = getUserCoordinator();
        try {
            
            uc.user_updateUserHumanLink(currentUser, currentUser, getSessionBean().getSessPerson());
            
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Successfully udpated your person link to " +  getSessionBean().getSessPerson().getName() + " (ID:" +  getSessionBean().getSessPerson().getHumanID() + ")", ""));
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "You must logout and log back in to see this change", ""));
        } catch (IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Could not update person link, sorry!", ""));
            
        }  catch (AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
        
    }
    
    /**
     * Listener for user to start the password update process
     * @param ev 
     */
    public void onUpdatePasswordInit(ActionEvent ev){
        System.out.println("UserBB.onUpdatePasswordInit");
        
    }
    
    
    /**
     * Listener for users who have completed the password update process
     * @return  
     */
    public String commitPasswordUpdates(){
        
        UserCoordinator uc = getUserCoordinator();
        try { 
            uc.user_checkPasswordPairForCompliance(formPassword, formPasswordReentry);
            uc.user_updateUserPassword_SECURITYCRITICAL(currentUser, currentUser, formPassword);
            getSessionBean().setPasswordResetNoticeTS(LocalDateTime.now());
                
            formPassword = "";
            formPasswordReentry = "";
        } catch (IntegrationException | AuthorizationException | BObStatusException  ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
            return "";
            
        }
        return "sessionReinit";
    }
    
    
  
    /**
     * @return the formPassword
     */
    public String getFormPassword() {
        return formPassword;
    }

   
  
    /**
     * @return the formNotes
     */
    public String getFormNotes() {
        return formNotes;
    }

    
   

    /**
     * @param formPassword the formPassword to set
     */
    public void setFormPassword(String formPassword) {
        this.formPassword = formPassword;
    }

   
    /**
     * @param formNotes the formNotes to set
     */
    public void setFormNotes(String formNotes) {
        this.formNotes = formNotes;
    }

   


    /**
     * @return the currentUser
     */
    public User getCurrentUser() {
        return currentUser;
    }

    /**
     * @param currentUser the currentUser to set
     */
    public void setCurrentUser(UserAuthorized currentUser) {
        this.currentUser = currentUser;
    }

   


    /**
     * @return the formPasswordReentry
     */
    public String getFormPasswordReentry() {
        return formPasswordReentry;
    }

    /**
     * @param formPasswordReentry the formPasswordReentry to set
     */
    public void setFormPasswordReentry(String formPasswordReentry) {
        this.formPasswordReentry = formPasswordReentry;
    }

    /**
     * @return the passwordResetTimestamp
     */
    public LocalDateTime getPasswordResetTimestamp() {
        return passwordResetTimestamp;
    }

    /**
     * @param passwordResetTimestamp the passwordResetTimestamp to set
     */
    public void setPasswordResetTimestamp(LocalDateTime passwordResetTimestamp) {
        this.passwordResetTimestamp = passwordResetTimestamp;
    }

   
  
    
   
}
