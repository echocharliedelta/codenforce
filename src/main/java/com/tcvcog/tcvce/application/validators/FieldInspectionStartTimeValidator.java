/*
 * Copyright (C) 2020 marosco
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application.validators;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.entities.occupancy.OccPermit;
import com.tcvcog.tcvce.occupancy.application.FieldInspectionBB;
import com.tcvcog.tcvce.occupancy.application.OccPeriodBB;
import com.tcvcog.tcvce.util.ComponentIDEnum;

import jakarta.faces.application.FacesMessage;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.validator.FacesValidator;
import jakarta.faces.validator.Validator;
import jakarta.faces.validator.ValidatorException;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Audits the sequence of dates on an occ permit
 *
 * @author Ellen Bascomb
 */
@FacesValidator("fieldInspectionTimeStartValidator")
public class FieldInspectionStartTimeValidator
        extends BackingBeanUtils
        implements Validator {

   
    @Override
    public void validate(FacesContext context,
            UIComponent component,
            Object value) throws ValidatorException {

        System.out.println("fieldInspectionTimeStartValidator.validate");
        FieldInspectionBB finBB = (FieldInspectionBB) context.getApplication().evaluateExpressionGet(context, "#{fieldInspectionBB}", FieldInspectionBB.class);
        
        if (finBB == null) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fatal error: Unable to get a handle on inspectionBB; Aborting validation", "");
            throw new ValidatorException(msg);
        }
        
        String startComponentID = component.getClientId();
        UIComponent compTimeStart = context.getViewRoot().findComponent(startComponentID);
        LocalTime timeStart = extractLocalTimeFromUIComponentString(compTimeStart);
        if(timeStart != null){
            System.out.println("Time Start: " + timeStart.toString());
            finBB.setInspectionTimeStartForValidation(timeStart);
        } else {
            System.out.println("fieldInspectionTimeStartValidator.validate | time start null");
        }
        
        // thanks chatGPT v.3.5
        String[] tokens = startComponentID.split(":");
        
        // Join all tokens except the last one
        StringBuilder parentComponentId = new StringBuilder();
        for (int i = 0; i < tokens.length - 1; i++) {
            if (i > 0) {
                parentComponentId.append(":");
            }
            parentComponentId.append(tokens[i]);
        }
        parentComponentId.append(":");
        String endComponentID = parentComponentId.toString() + ComponentIDEnum.FIN_INIT_COMPONENT_ID_TIME_END.getComponentID();
        
        System.out.println("End time component ID: " + endComponentID);
        
        UIComponent compTimeEnd = context.getViewRoot().findComponent(endComponentID);
        LocalTime timeEnd = extractLocalTimeFromUIComponentString(compTimeEnd);
        if(timeEnd != null){
            finBB.setInspectionTimeEndForValidation(timeEnd);
        } else {
            System.out.println("fieldInspectionTimeStartValidator.validate | end time null");
        }
        
        // this time will never be evaluated -- we're just doing this so end can be evaluated
        
    }
}
