/*
 * Copyright (C) 2020 marosco
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application.validators;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.entities.occupancy.OccPermit;
import com.tcvcog.tcvce.occupancy.application.OccPeriodBB;
import com.tcvcog.tcvce.util.ComponentIDEnum;
import java.time.LocalDate;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.component.UIComponent;
import jakarta.faces.component.UIInput;
import jakarta.faces.context.FacesContext;
import jakarta.faces.validator.FacesValidator;
import jakarta.faces.validator.Validator;
import jakarta.faces.validator.ValidatorException;
import java.time.format.DateTimeFormatter;
import org.primefaces.component.calendar.Calendar;
import org.primefaces.component.datepicker.DatePicker;

/**
 * Audits the sequence of dates on an occ permit
 *
 * @author Ellen Bascomb
 */
@FacesValidator("permitDateValidatorIssuance")
public class PermitDateValidatorIssuance
        extends BackingBeanUtils
        implements Validator {

   
    @Override
    public void validate(FacesContext context,
            UIComponent component,
            Object value) throws ValidatorException {

        System.out.println("permitDateValidatorIssuance.validate");
        OccPeriodBB opbb = (OccPeriodBB) context.getApplication().evaluateExpressionGet(context, "#{occPeriodBB}", OccPeriodBB.class);
        OccPermit permit = opbb.getCurrentOccPermit();
        if (permit == null) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fatal error: Unable to get a handle on permit; Aborting validation", "");
            throw new ValidatorException(msg);
        }

        LocalDate ldtApplication = opbb.getDateForValidationApplication();
        LocalDate ldtFinInit = opbb.getDateForValidationFinInit();
        LocalDate ldtFinReinspect = opbb.getDateForValidationFinRe();
        LocalDate ldtFinFinal = opbb.getDateForValidationFinFinal();
        LocalDate ldtIssuance = opbb.getDateForValidationIssuance();
        LocalDate ldtExpiry = opbb.getDateForValidationExpiry();
        
        if(ldtIssuance == null){
             FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "An issuance date is required", "");
                context.addMessage(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_ISSUANCE.getComponentID(), msg);
                throw new ValidatorException(msg);
        }
        
        if(ldtApplication != null && ldtIssuance.isBefore(ldtApplication)){
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Issuance date must be AFTER the application date", "");
            context.addMessage(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_ISSUANCE.getComponentID(), msg);
            throw new ValidatorException(msg);
        }
        if(ldtFinInit != null && ldtIssuance.isBefore(ldtFinInit)){
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Issuance date must be AFTER the initial inspection date", "");
            context.addMessage(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_ISSUANCE.getComponentID(), msg);
            throw new ValidatorException(msg);
        }
        
        if(ldtFinReinspect != null && ldtIssuance.isBefore(ldtFinReinspect)){
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Issuance date must be AFTER the reinspection date", "");
            context.addMessage(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_ISSUANCE.getComponentID(), msg);
            throw new ValidatorException(msg);
        }
        
        if(ldtFinFinal != null && ldtIssuance.isBefore(ldtFinFinal)){
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Issuance date must be AFTER the final inspection date", "");
            context.addMessage(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_ISSUANCE.getComponentID(), msg);
            throw new ValidatorException(msg);
        }
        
        if(ldtExpiry != null && ldtIssuance.isAfter(ldtExpiry)){
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Issuance date must be BEFORE the expiry date", "");
            context.addMessage(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_ISSUANCE.getComponentID(), msg);
            throw new ValidatorException(msg);
        }
    }
}
