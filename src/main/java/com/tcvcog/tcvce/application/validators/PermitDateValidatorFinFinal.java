/*
 * Copyright (C) 2020 marosco
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application.validators;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.entities.occupancy.OccPermit;
import com.tcvcog.tcvce.occupancy.application.OccPeriodBB;
import com.tcvcog.tcvce.util.ComponentIDEnum;
import java.time.LocalDate;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.component.UIComponent;
import jakarta.faces.component.UIInput;
import jakarta.faces.context.FacesContext;
import jakarta.faces.validator.FacesValidator;
import jakarta.faces.validator.Validator;
import jakarta.faces.validator.ValidatorException;
import java.time.format.DateTimeFormatter;
import org.primefaces.component.calendar.Calendar;
import org.primefaces.component.datepicker.DatePicker;

/**
 * Audits the sequence of dates on an occ permit
 *
 * @author Ellen Bascomb
 */
@FacesValidator("permitDateValidatorFinFinal")
public class PermitDateValidatorFinFinal
        extends BackingBeanUtils
        implements Validator {

   
    @Override
    public void validate(FacesContext context,
            UIComponent component,
            Object value) throws ValidatorException {

        System.out.println("permitDateValidatorFinFinal.validate");
        OccPeriodBB opbb = (OccPeriodBB) context.getApplication().evaluateExpressionGet(context, "#{occPeriodBB}", OccPeriodBB.class);
        OccPermit permit = opbb.getCurrentOccPermit();
        if (permit == null) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fatal error: Unable to get a handle on permit; Aborting validation", "");
            throw new ValidatorException(msg);
        }
        
        LocalDate ldtApplication = opbb.getDateForValidationApplication();
        LocalDate ldtFinInit = opbb.getDateForValidationFinInit();
        LocalDate ldtFinReinspect = opbb.getDateForValidationFinRe();
        LocalDate ldtFinFinal = opbb.getDateForValidationFinFinal();
        LocalDate ldtIssuance = opbb.getDateForValidationIssuance();
        LocalDate ldtExpiry = opbb.getDateForValidationExpiry();

        if(ldtFinFinal != null){
       
           
            
            if(ldtFinInit != null && ldtFinInit.isAfter(ldtFinFinal)){
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Final inspection date  must be after the initial inspection date", "");
                context.addMessage(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_FIN_FINAL.getComponentID(), msg);
                throw new ValidatorException(msg);
                
            }
            
            if(ldtFinReinspect != null && ldtFinReinspect.isAfter(ldtFinFinal)){
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Final inspection date must be AFTER the reinspection date", "");
                context.addMessage(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_FIN_FINAL.getComponentID(), msg);
                throw new ValidatorException(msg);
                
            }
            if(ldtApplication != null && ldtFinFinal.isBefore(ldtApplication)){
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Final inspection date must be AFTER the application date", "");
                context.addMessage(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_FIN_FINAL.getComponentID(), msg);
                throw new ValidatorException(msg);
                
            }
            
            if(ldtIssuance != null && ldtIssuance.isBefore(ldtFinFinal)){
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Certificate issuance date must be AFTER the final inspection date", "");
                context.addMessage(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_FIN_FINAL.getComponentID(), msg);
                throw new ValidatorException(msg);
                
            }
            
            if(ldtExpiry != null && ldtExpiry.isBefore(ldtFinFinal)){
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Certificate Expiry date must be AFTER the final inspection date", "");
                context.addMessage(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_FIN_FINAL.getComponentID(), msg);
                throw new ValidatorException(msg);
                
            }
        }
        // enforce requirement for passed FIN on permittype
        
    }
}
