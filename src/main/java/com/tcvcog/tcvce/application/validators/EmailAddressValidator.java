/*
 * Copyright (C) 2020 marosco
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application.validators;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.entities.occupancy.OccPermit;
import com.tcvcog.tcvce.occupancy.application.OccPeriodBB;
import com.tcvcog.tcvce.util.ComponentIDEnum;

import jakarta.faces.application.FacesMessage;
import jakarta.faces.component.UIComponent;
import jakarta.faces.component.UIInput;
import jakarta.faces.context.FacesContext;
import jakarta.faces.validator.FacesValidator;
import jakarta.faces.validator.Validator;
import jakarta.faces.validator.ValidatorException;
import java.time.LocalDate;

/**
 * Audits the sequence of dates on an occ permit
 *
 * @author Ellen Bascomb
 */
@FacesValidator("emailAddressValidator")
public class EmailAddressValidator
        extends BackingBeanUtils
        implements Validator {

    /**
     * Written guts by chat GPT with edits.
     * @param context
     * @param component
     * @param value
     * @throws ValidatorException 
     */
    @Override
    public void validate(FacesContext context,
            UIComponent component,
            Object value) throws ValidatorException {

        if (value == null || value.toString().trim().isEmpty()) {
            return;
        }
         String inputmaskID = component.getClientId(context);

        String email = (String) value;
        String trimmedEmail = email.trim(); // Trim leading and trailing whitespace

        // Your regex validation logic
        String regex = "^\\s*([\\w\\.-]+)@([\\w\\.-]+)\\s*$";
        if (!trimmedEmail.matches(regex)) {
            FacesMessage msg = new FacesMessage("Please enter a valid email address");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            context.addMessage(inputmaskID, msg);
            throw new ValidatorException(msg);
        }
        UIInput in = (UIInput) component;
        in.setValue(trimmedEmail);
        in.setSubmittedValue(trimmedEmail);

    }
}
