/*
 * Copyright (C) 2020 marosco
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application.validators;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.entities.occupancy.OccPermit;
import com.tcvcog.tcvce.occupancy.application.OccPeriodBB;
import com.tcvcog.tcvce.util.ComponentIDEnum;

import jakarta.faces.application.FacesMessage;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.validator.FacesValidator;
import jakarta.faces.validator.Validator;
import jakarta.faces.validator.ValidatorException;
import java.time.LocalDate;

/**
 * Audits the sequence of dates on an occ permit
 *
 * @author Ellen Bascomb
 */
@FacesValidator("permitDateValidatorApplication")
public class PermitDateValidatorApplication
        extends BackingBeanUtils
        implements Validator {

    
    @Override
    public void validate(FacesContext context,
            UIComponent component,
            Object value) throws ValidatorException {

        System.out.println("PermitDateValidatorApplication.validate");
        OccPeriodBB opbb = (OccPeriodBB) context.getApplication().evaluateExpressionGet(context, "#{occPeriodBB}", OccPeriodBB.class);
        OccPermit permit = opbb.getCurrentOccPermit();
        if (permit == null) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fatal error: Unable to get a handle on permit; Aborting validation", "");
            throw new ValidatorException(msg);
        }

        UIComponent compDateOfApplication = context.getViewRoot().findComponent(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_APPLICATION.getComponentID());
        LocalDate ldtApplication = extractLocalDateFromUIComponentString(compDateOfApplication);
        opbb.setDateForValidationApplication(ldtApplication);
        
        UIComponent compDateOfFinInit = context.getViewRoot().findComponent(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_FIN_INIT.getComponentID());
        LocalDate ldtFinInit = extractLocalDateFromUIComponentString(compDateOfFinInit);
        opbb.setDateForValidationFinInit(ldtFinInit);

        UIComponent compDateOfFinReinspect = context.getViewRoot().findComponent(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_FIN_REINSPECT.getComponentID());
        LocalDate ldtFinReinspect = extractLocalDateFromUIComponentString(compDateOfFinReinspect);
        opbb.setDateForValidationFinRe(ldtFinReinspect);

        UIComponent compDateOfFinFinal = context.getViewRoot().findComponent(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_FIN_FINAL.getComponentID());
        LocalDate ldtFinFinal = extractLocalDateFromUIComponentString(compDateOfFinFinal);
        opbb.setDateForValidationFinFinal(ldtFinFinal);

        UIComponent compDateOfIssuance = context.getViewRoot().findComponent(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_ISSUANCE.getComponentID());
        LocalDate ldtIssuance = extractLocalDateFromUIComponentString(compDateOfIssuance);
        opbb.setDateForValidationIssuance(ldtIssuance);

        UIComponent compDateOfExpiry = context.getViewRoot().findComponent(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_EXPIRY.getComponentID());
        LocalDate ldtExpiry = extractLocalDateFromUIComponentString(compDateOfExpiry);
        opbb.setDateForValidationExpiry(ldtExpiry);

        // all dates except expiry must be before now()
        if (ldtApplication != null && ldtApplication.isAfter(LocalDate.now())) {
           FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Application date must be in the past", "Detailed content");
            context.addMessage(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_APPLICATION.getComponentID(), msg);
            throw new ValidatorException(msg);
        } 
 
    }
}
