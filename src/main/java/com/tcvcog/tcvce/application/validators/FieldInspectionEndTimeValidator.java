/*
 * Copyright (C) 2020 marosco
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application.validators;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.entities.occupancy.OccPermit;
import com.tcvcog.tcvce.occupancy.application.FieldInspectionBB;
import com.tcvcog.tcvce.occupancy.application.OccPeriodBB;
import com.tcvcog.tcvce.util.ComponentIDEnum;

import jakarta.faces.application.FacesMessage;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.validator.FacesValidator;
import jakarta.faces.validator.Validator;
import jakarta.faces.validator.ValidatorException;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Audits the sequence of dates on an occ permit
 *
 * @author Ellen Bascomb
 */
@FacesValidator("fieldInspectionTimeEndValidator")
public class FieldInspectionEndTimeValidator
        extends BackingBeanUtils
        implements Validator {

   
    @Override
    public void validate(FacesContext context,
            UIComponent component,
            Object value) throws ValidatorException {

        System.out.println("fieldInspectionTimeEndValidator.validate");
        FieldInspectionBB finBB = (FieldInspectionBB) context.getApplication().evaluateExpressionGet(context, "#{fieldInspectionBB}", FieldInspectionBB.class);
        
        if (finBB == null) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fatal error: Unable to get a handle on inspectionBB; Aborting validation", "");
            throw new ValidatorException(msg);
        }
        
        LocalTime finStart = finBB.getInspectionTimeStartForValidation();
        LocalTime finEnd = finBB.getInspectionTimeEndForValidation();
        
       
        
        if(finStart != null && finEnd != null){
            if(finEnd.isBefore(finStart)){
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "If a start and end time are provided for this inspection, the END time must occur AFTER the START time", "");
                context.addMessage(component.getClientId(context), msg);
                throw new ValidatorException(msg);
            } else {
                System.out.println("fieldInspectionTimeEndValidator.validate | time end is properly ordered with respect to time start");
            }
        }
        
    }
}
