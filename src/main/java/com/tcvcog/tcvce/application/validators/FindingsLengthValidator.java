/*
 * Copyright (C) 2025 echocdelta
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application.validators;

import com.tcvcog.tcvce.application.CECaseBB;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.validator.FacesValidator;
import jakarta.faces.validator.Validator;
import jakarta.faces.validator.ValidatorException;

/**
 * checks length of findings only on final submit
 * @author echocdelta
 */
@FacesValidator("findingsLengthValidator")
public class FindingsLengthValidator implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object t) throws ValidatorException {
        
        // Evaluate the bean property "requireFindingsOnViolationAdd"
        CECaseBB ceCaseBB = fc.getApplication()
                               .evaluateExpressionGet(fc, "#{ceCaseBB}", CECaseBB.class);

        // If the user is in "auto-save" mode (the boolean is false), skip validation
        if (!ceCaseBB.isRequireFindingsOnViolationAdd()) {
            return; // do nothing; no error thrown
        }

        // Otherwise, we do the length check (or any other logic)
        String submittedFindings = (t == null) ? "" : ((String) t).trim();
        
        // For example, ensure at least 5 characters
        if (submittedFindings.length() < 5) {
            FacesMessage msg = new FacesMessage(
                FacesMessage.SEVERITY_ERROR, 
                "Please enter at least 5 characters", 
                null
            );
            throw new ValidatorException(msg);
        }
        
    }
    
}
