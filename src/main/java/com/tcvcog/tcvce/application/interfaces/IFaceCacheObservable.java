/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application.interfaces;

/**
 * Specifies that the implementing class can know about IFaceCacheObservers
 * who shall be notified of changes that require a cache dump.
 * 
 * @author Ellen Bascomb of Apartment31Y
 */
public interface IFaceCacheObservable { 
    
    /**
     * Implementing classes store in their belly registries it needs to notify when 
     * changes occur
     * @param observer 
     */
    public void registerCacheObserver(IFaceCacheObserver observer);
    
    /**
     * removes observer from the registry
     * @param observer 
     */
    public void removeCacheObserver(IFaceCacheObserver observer);
    
    
    /**
     * All observers in the registry should be poked to flush their cache entries
     */
    public void notifyCacheObservers();
    
}
