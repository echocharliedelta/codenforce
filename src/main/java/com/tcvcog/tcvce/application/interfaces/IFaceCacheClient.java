/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application.interfaces;

/**
 * Implemented by the integration classes whose getters are wired to use
 * a cache manager when caching is enabled. Implementing classes have a boolean 
 * switch to control if caching is enabled
 * 
 * @author Ellen Bascomb of Apartment31Y
 */
public interface IFaceCacheClient {
   
   
    
    /**
     * Implementing classes should tell their manager that they are 
     * clients who desperately need strict management lest they wander
     * off into the abyss of stale object hell.
     */
    public void registerClientWithManager();
    
    
}
