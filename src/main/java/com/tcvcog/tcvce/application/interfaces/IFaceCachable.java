/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application.interfaces;

import com.tcvcog.tcvce.entities.IFace_keyIdentified;

/**
 * Marks an object that knows its own cache key
 * @author pierre15
 */
public interface IFaceCachable  {
    
    /**
     * The implementing class must wire up a unique ID for the 
     * associative array in the Cache. For some subfamilies like
     * HumanLink and MailingAddressLink these cache keys might
     * need to be cleverly made unique within their entire family 
     * @return the key the cache
     */
    public int getCacheKey();
    
    
}
