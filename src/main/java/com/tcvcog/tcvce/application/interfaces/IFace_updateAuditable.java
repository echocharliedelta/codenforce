package com.tcvcog.tcvce.application.interfaces;

public interface IFace_updateAuditable<T extends Enum<T>> {

    Class<T> getFieldDumpEnum();
}
