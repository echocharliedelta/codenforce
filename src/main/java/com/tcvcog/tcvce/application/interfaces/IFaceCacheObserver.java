/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application.interfaces;

/**
 * Used by business objects who have in their belly cached objects
 * 
 * @author ellen bascomb of apartment 31Y
 */
public interface IFaceCacheObserver {
    
    public void setCacheManager(IFaceCacheManager mngr);
    public IFaceCacheManager getCacheManager();
    
    /**
     * Go tell your cache manager to flush yourself. The implementing class
     * will need to have a handle it its relevant cache manager and 
     * this method's implementation can then call flushObjectFromCache
     * on IFaceCacheManager
     */
    public void flushDependentObjectCache();
    
}
