/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application.interfaces;

/**
 * Used by application scoped tools that have caches in them and requires that they have a 
 * flushAllCaches() method
 * @author pierre15
 */
public interface IFaceCacheManager {
    
    
    /**
     * Triggers initialization of each cache with max entry count
     */
    public void initCaching();
    
    /**
     * Impelmentors should call clearAll() on all caches it manages.
     */
    public void flushAllCaches();
    
    /**
     * This method will be called by CacheObserver implementations
     * and their own primary key (which is the cache key) will be passed to 
     * this method.A side-effect liable approach in implementation 
     * will be that each of the CacheManager's caches will have their invalidate(int i)
     * called and it's possible that with overlapping DB keys that business object Y 
     * might be invalidated when X is invalidated if they share the same DB key.
     * 
     * As of first implementation, not all coordinator clients of this class are 
     * through this crudle method. The cache managers expose their caches 
     * through simple getters and clients can do their own clearing since 
     * they may not have a full object, only an ID.
     * 
     * @param cable
     */
    public void flushObjectFromCache(IFaceCachable cable);
    
    
    /**
     * The override cousin to flushObjectFromCache(IFaceCachable cable)
     * which also must accept any arbitrary cacheKey and sloppily
     * and perhaps wastefully dump records in ANY CACHE with that key.
     * This is required in an attempt to not demand that 
     * coordinator classes know which cache stores which objects, 
     * only that their assigned cache manager must be told to flush 
     * caches on object updates.
     * 
     * @param cacheKey 
     */
    public void flushObjectFromCache(int cacheKey);
    
    
    /**
     * Tell caches to dump their size
     */
    public void writeCacheStats();
    
    /**
     * Mangers hold a list of their clients, which they can iterate over and enable or disable caching
     * @param client 
     */
    public void registerCacheClient(IFaceCacheClient client);
    
    /**
     * Removes a client from registration
     * @param client 
     */
    public void removeCacheClient(IFaceCacheClient client);
    
    /**
     * flips each client to not use caching facilities
     */
    public void disableClientCaching();
    
    /**
     * tell each client to start using caching facilities
     */
    public void enableClientCaching();
 
    /**
     * returns the current caching state
     * @return 
     */
    public boolean isCachingEnabled();
  
}
