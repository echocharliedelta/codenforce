package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.PropertyCoordinator;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.Icon;
import com.tcvcog.tcvce.entities.PropertyUseType;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;

import java.io.Serializable;
import java.util.List;

import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;

import java.util.Objects;

public class PropertyUseTypeBB extends BackingBeanUtils implements Serializable {

    private List<PropertyUseType> putList;
    private PropertyUseType currentPut;

    private List<Icon> iconList;
    private boolean editModePropertyUseTypeInfo;
    private boolean propertyUseTypeCreateMode;

    private boolean includeInactivePropertyUseTypes;

    /**
     * Creates a new
     * instance of PropertyUseTypeBB
     */
    public PropertyUseTypeBB() {
    }

    @PostConstruct
    public void initBean() {
        System.out.println("PropertyUseType BB: Init");
        editModePropertyUseTypeInfo = false;
        propertyUseTypeCreateMode = false;
        includeInactivePropertyUseTypes = false;
        refreshPutList();
        if (Objects.nonNull(putList) && !putList.isEmpty()) {
            currentPut = putList.get(0);
        }
    }

    public void refreshPutList() {
        SystemCoordinator sc = getSystemCoordinator();
        try {
            putList = sc.getPutList();
            setIconList(sc.getIconList());
        } catch (IntegrationException ex) {
            System.out.println(ex);
        }
    }

    public void viewPropertyUseType(PropertyUseType useType) {
        currentPut = useType;
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Viewing Property Use ID: " + useType.getName(), ""));
    }

    /**
     * Listener for users to click the add or the edit button
     *
     * @param ev
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public void togglePropertyUseTypeAddEditButtonChange(ActionEvent ev) throws BObStatusException {
        System.out.println("toggle PropertyUseType AddEditButtonChange");
        if (editModePropertyUseTypeInfo) {
            if (Objects.nonNull(currentPut) && currentPut.getTypeID() == 0) {
                onInsertPropertyUseTypeCommit();
            } else {
                onUpdatePropertyUseTypeCommit();
            }
            refreshPutList();
        }
        editModePropertyUseTypeInfo = !editModePropertyUseTypeInfo;
    }

    /**
     * Begins the PropertyUseType creation process
     *
     * @param ev
     */
    public void onPropertyUseTypeAddInit(ActionEvent ev) {
        System.out.println("PropertyUseTypeBB.onPropertyUseTypeAddInit");
        PropertyCoordinator pc = getPropertyCoordinator();
        currentPut = pc.getPropertyUseTypeSkeleton(getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
        editModePropertyUseTypeInfo = true;
    }

    /**
     * Listener for users to cancel their Property Use Type add/edit operation
     *
     * @param ev
     */
    public void onOperationPropertyUseTypeAddEditAbort(ActionEvent ev) {
        System.out.println("PropertyUseTypeBB.togglePropertyUseTypeAddEditButtonChange: ABORT");
        editModePropertyUseTypeInfo = false;
        propertyUseTypeCreateMode = false;
    }

    public void onInsertPropertyUseTypeCommit() {
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            currentPut = pc.insertPropertyUseType(currentPut, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Successful Insert Property Use Type!", ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
        }
    }

    public void onUpdatePropertyUseTypeCommit() {
        PropertyCoordinator pc = getPropertyCoordinator();
        try {
            pc.updatePropertyUseType(currentPut, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Property Use type Updated with ID " + currentPut.getTypeID(), ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Failed to Update Property Use Type", ""));
        }
    }

    /**
     * Listener for user request to start the nuking process of a PropertyUseType
     *
     * @param useType
     */
    public void onPropertyUseTypeNukeInitButtonChange(PropertyUseType useType) {
        currentPut = useType;
    }

    /**
     * Listener for user requests to commit a Property Use Type nuke operation
     *
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void onPropertyUseTypeNukeCommitButtonChange() throws AuthorizationException {
        PropertyCoordinator pc = getPropertyCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        try {
            int uses = sc.putCheckForUse(currentPut);
            if (uses == 0) {
                pc.deactivatePropertyUseType(currentPut, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Property Use Type deactivated with ID: " + currentPut.getTypeID(), ""));
            } else {
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "PropertyUseType is in use " + uses + " times. Could not remove", ""));
            }
            refreshPutList();
            if (Objects.nonNull(putList) && !putList.isEmpty()) {
                currentPut = putList.get(0);
            } else {
                currentPut = null;
            }
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
        }
    }

    /**
     * @return the putList
     */
    public List<PropertyUseType> getPutList() {
        return putList;
    }

    /**
     * @param putList the putList to set
     */
    public void setPutList(List<PropertyUseType> putList) {
        this.putList = putList;
    }

    /**
     * @return the currentPut
     */
    public PropertyUseType getCurrentPut() {
        return currentPut;
    }

    /**
     * @param currentPut the currentPut to set
     */
    public void setCurrentPut(PropertyUseType currentPut) {
        this.currentPut = currentPut;
    }

    /**
     * @return the iconList
     */
    public List<Icon> getIconList() {
        return iconList;
    }

    /**
     * @param iconList the iconList to set
     */
    public void setIconList(List<Icon> iconList) {
        this.iconList = iconList;
    }

    public boolean isEditModePropertyUseTypeInfo() {
        return editModePropertyUseTypeInfo;
    }

    public void setEditModePropertyUseTypeInfo(boolean editModePropertyUseTypeInfo) {
        this.editModePropertyUseTypeInfo = editModePropertyUseTypeInfo;
    }

    public boolean isPropertyUseTypeCreateMode() {
        return propertyUseTypeCreateMode;
    }

    public void setPropertyUseTypeCreateMode(boolean propertyUseTypeCreateMode) {
        this.propertyUseTypeCreateMode = propertyUseTypeCreateMode;
    }

    public boolean isIncludeInactivePropertyUseTypes() {
        return includeInactivePropertyUseTypes;
    }

    public void setIncludeInactivePropertyUseTypes(boolean includeInactivePropertyUseTypes) {
        this.includeInactivePropertyUseTypes = includeInactivePropertyUseTypes;
    }
}
