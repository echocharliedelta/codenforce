package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.coordinators.SearchCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.domain.SearchException;
import com.tcvcog.tcvce.entities.BOBSource;
import com.tcvcog.tcvce.entities.CodeViolationPropCECaseHeavy;
import com.tcvcog.tcvce.entities.IntensityClass;
import com.tcvcog.tcvce.entities.search.QueryCodeViolation;
import com.tcvcog.tcvce.entities.search.SearchParamsCodeViolation;
import com.tcvcog.tcvce.session.SessionBean;
import com.tcvcog.tcvce.util.Constants;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CodeViolationSearchBB extends BackingBeanUtils {
    
    private QueryCodeViolation querySelected;
    private List<QueryCodeViolation> queryList;
    private SearchParamsCodeViolation searchParamsSelected;
    private boolean appendResultsToList;
    private List<CodeViolationPropCECaseHeavy> codeViolationList;
    private List<IntensityClass> severityList;
    private List<BOBSource> sourceList;

    /**
     * Creates a new instance of CodeViolationSearchBB
     */
    public CodeViolationSearchBB() {
    }
    
    @PostConstruct
    public void initBean() {
        SearchCoordinator sc = getSearchCoordinator();
        SystemCoordinator sysCor = getSystemCoordinator();
        queryList = sc.buildQueryCodeViolation(getSessionBean().getSessUser().getMyCredential());
        appendResultsToList = false;
        codeViolationList = new ArrayList<>();
        try
        {
            severityList = sysCor.getIntensitySchemaWithClasses(
                    getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("intensityschema_violationseverity"), getSessionBean().getSessMuni())
                    .getClassList();
            sourceList = sysCor.getBobSourceListComplete();
        } catch (IntegrationException ex)
        {
            System.out.println(ex);
        }
        
        if (Objects.isNull(querySelected) && !queryList.isEmpty())
        {
            querySelected = queryList.stream().findFirst().orElse(null);
        }

        // setup search
        configureParameters();
    }

    /**
     * Action listener for the user's request to run the query
     *
     * @param event
     */
    public void executeQuery(ActionEvent event) {
        SearchCoordinator sc = getSearchCoordinator();
        if (Objects.nonNull(querySelected))
        {
            System.out.println("CodeViolationSearchBB.executeQuery | querySelected: " + querySelected.getQueryTitle());
        }
        List<CodeViolationPropCECaseHeavy> cvList;
        try
        {
            cvList = sc.runQuery(querySelected).getBOBResultList();
            if (!appendResultsToList && Objects.nonNull(codeViolationList))
            {
                codeViolationList.clear();
            }
            if (Objects.nonNull(cvList) && !cvList.isEmpty())
            {
                codeViolationList.addAll(cvList);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Your search completed with " + cvList.size() + " results", ""));
            } else
            {
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Your search had no results", ""));
            }
        } catch (SearchException ex)
        {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to complete search! ", ""));
        }
    }

    /**
     * Event listener for resetting a query after it's run
     *
     * @param event
     */
    public void resetQuery(ActionEvent event) {
        SearchCoordinator sc = getSearchCoordinator();
        queryList = sc.buildQueryCodeViolation(getSessionBean().getSessUser().getMyCredential());
        if (Objects.nonNull(queryList) && !queryList.isEmpty())
        {
            querySelected = queryList.stream().findFirst().orElse(null);
        }
        if (appendResultsToList == false)
        {
            if (Objects.nonNull(codeViolationList) && !codeViolationList.isEmpty())
            {
                codeViolationList.clear();
            }
        }
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Query reset ", ""));
        
        configureParameters();
    }
    
    public void clearCodeViolationList(ActionEvent ev) {
        if (Objects.nonNull(codeViolationList) && !codeViolationList.isEmpty())
        {
            codeViolationList.clear();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "CodeViolation List Reset!", ""));
        }
        
    }

    /**
     * Sets up search parameters for properties
     */
    private void configureParameters() {
        if (Objects.nonNull(querySelected)
                && Objects.nonNull(querySelected.getParamsList())
                && !querySelected.getParamsList().isEmpty())
        {
            
            setSearchParamsSelected(querySelected.getParamsList().get(0));
        }
        
    }

    /**
     * Listener method for changes in the selected query; Updates search params
     * and UI updates based on this changed value
     */
    public void changeQuerySelected() {
        System.out.println("CodeViolationSearchBB.changeQuerySelected | querySelected: " + querySelected.getQueryTitle());
        configureParameters();
        
    }

    /**
     * Listener for user requests to view the code violation log
     *
     * @param ev
     */
    public void onViewCodeViolationLogLinkClick(ActionEvent ev) {
        System.out.println("CodeViolationSearchBB.onViewCodeViolationLogLinkClick");
    }

    /**
     * method for navigating page to parent CECase
     *
     * @param heavy
     * @return navTo
     */
    public String onViolationViewCeCase(CodeViolationPropCECaseHeavy heavy) {
        CaseCoordinator cc = getCaseCoordinator();
        SessionBean sb = getSessionBean();
        String navTo = "";
        try
        {
            navTo = sb.navigateToPageCorrespondingToObject(cc.cecase_getCECase(heavy.getCeCaseID(), getSessionBean().getSessUser()));
        } catch (IntegrationException | BObStatusException | AuthorizationException ex)
        {
            System.out.println(ex.toString());
        }
        if (navTo.equals(""))
        {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Cannot load CECase for Violation ID:" + heavy.getViolationID(), ""));
        }
        return navTo;
    }
    
    public QueryCodeViolation getQuerySelected() {
        return querySelected;
    }
    
    public void setQuerySelected(QueryCodeViolation querySelected) {
        this.querySelected = querySelected;
    }
    
    public List<QueryCodeViolation> getQueryList() {
        return queryList;
    }
    
    public void setQueryList(List<QueryCodeViolation> queryList) {
        this.queryList = queryList;
    }
    
    public SearchParamsCodeViolation getSearchParamsSelected() {
        return searchParamsSelected;
    }
    
    public void setSearchParamsSelected(SearchParamsCodeViolation searchParamsSelected) {
        this.searchParamsSelected = searchParamsSelected;
    }
    
    public boolean isAppendResultsToList() {
        return appendResultsToList;
    }
    
    public void setAppendResultsToList(boolean appendResultsToList) {
        this.appendResultsToList = appendResultsToList;
    }
    
    public List<CodeViolationPropCECaseHeavy> getCodeViolationList() {
        return codeViolationList;
    }
    
    public void setCodeViolationList(List<CodeViolationPropCECaseHeavy> codeViolationList) {
        this.codeViolationList = codeViolationList;
    }
    
    public List<IntensityClass> getSeverityList() {
        return severityList;
    }
    
    public void setSeverityList(List<IntensityClass> severityList) {
        this.severityList = severityList;
    }
    
    public List<BOBSource> getSourceList() {
        return sourceList;
    }
    
    public void setSourceList(List<BOBSource> sourceList) {
        this.sourceList = sourceList;
    }
    
}
