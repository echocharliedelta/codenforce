/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application;

import jakarta.annotation.PostConstruct;
import java.io.Serializable;
import java.util.List;
import org.primefaces.model.TreeNode;

/**
 * Provides expand and collapse tools for prime faces treenodes
 * 
 * @author pierre15
 */
public class TreeToolsBB implements Serializable{
 
    @PostConstruct
    public void initBean()  {
        System.out.println("TreeToolsBB.initBean");
    }
    
    
    /**
     * Listener for user requests to expand all nodes
     * @param treeRoot 
     */
    public void expandAllNodes(TreeNode treeRoot){
        collapseOrExpandAllNodesInTree(treeRoot, true);
    }
    
    /**
     * Listener for user requests to collapse all nodes
     * @param treeRoot 
     */
    public void collapseAllNodes(TreeNode treeRoot){
        collapseOrExpandAllNodesInTree(treeRoot, false);
    }
    
    /**
     * Recursive function for expanding or collapsing out
     * @param n
     * @param expandedNode 
     */
    public void collapseOrExpandAllNodesInTree(TreeNode n, boolean expandedNode){
        if(n != null){
            for(TreeNode node: (List<TreeNode>) n.getChildren()){
                collapseOrExpandAllNodesInTree(node, expandedNode);
            }
            n.setExpanded(expandedNode);
        }
    }
}
