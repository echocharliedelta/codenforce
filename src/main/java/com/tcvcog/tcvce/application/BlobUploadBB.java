/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.coordinators.BlobCoordinator;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.BlobTypeException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.Blob;
import com.tcvcog.tcvce.entities.IFace_BlobHolder;
import com.tcvcog.tcvce.entities.Municipality;
import jakarta.faces.application.FacesMessage;
import java.io.IOException;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * July 2023 Request scoped bean for file upload
 * @author Ellen Bascomb of Apartment 31Y
 */
public class BlobUploadBB extends BackingBeanUtils {
    
      /**
     * Listener for user requests to upload a file and attach to case
     * I ask the coordinator for the current Blob_Holder and interrogate it
     * for information about where to store its blobs in the DB
     *
     * @param uploadEvent
     */
    public void onBlobUploadCommitButtonChange(FileUploadEvent uploadEvent) {
        // as the session for our blob holder
        // client UIs must set this up for me to know who to connect 
        // the blob to
        
        IFace_BlobHolder currentBlobHolder = getSessionBean().getSessBlobHolder();
        System.out.println("BlobUploadBB.onBlobUploadCommitButtonChange | Beginning storage cycle!");
//        extractAndStoreBlobListComponentToUpdate();
        if (currentBlobHolder != null && currentBlobHolder.getBlobLinkEnum() != null){
            if(uploadEvent != null ) {

                try {
                    BlobCoordinator blobc = getBlobCoordinator();

                    Blob blob = blobc.generateBlobSkeleton(getSessionBean().getSessUser());
                    blob.setFilename(uploadEvent.getFile().getFileName());
                    blob.setBytes(uploadEvent.getFile().getContent());
                    // May 2023: refactored to store blobs on the public side so sometime we only have a muni light
                    // since public users don't get a muni data heavy
                    Municipality blobMuni;
                    if(getSessionBean().getSessMuni() != null){
                        blobMuni = getSessionBean().getSessMuni();
                    } else {
                        blobMuni = getSessionBean().getSessMuniLight();
                    }

                    Blob freshBlob = blobc.insertBlobAndInsertMetadataAndLinkToParent(
                                                    blob, 
                                                    currentBlobHolder, 
                                                    getSessionBean().getSessUser(), 
                                                    blobMuni);

                    if (freshBlob != null) {
                        System.out.println("BlobUploadBB.onBlobUploadCommitButtonChange | fresh blob ID: " + freshBlob.getPhotoDocID());

                        getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO,
                                    "Upload success! File " + blob.getFilename() + " Is stored with PhotoDoc ID: " + freshBlob.getPhotoDocID(), ""));
                    }
//                    refreshCurrentBlobHolder();
//                    sendUpdatedBlobListToSessionForSenderRefresh();

                } catch (IntegrationException | IOException | BlobException | BlobTypeException | BObStatusException ex) {
                    System.out.println("BlobUploadBB.onBlobUploadCommitButtonChange | upload failed! " + ex);
                    System.out.println(ex);
                    getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Fatal error on upload of file: [cannot list filename]", ""));
                }
            }
        }
    }
    
}
