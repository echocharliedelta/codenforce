/*
 * Copyright (C) 2018 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application;

import com.tcvcog.tcvce.session.SessionBean;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.entities.CECase;
import com.tcvcog.tcvce.entities.CECaseDataHeavy;
import com.tcvcog.tcvce.entities.CodeSource;
import com.tcvcog.tcvce.entities.FocusedObjectEnum;
import com.tcvcog.tcvce.entities.NavigationItem;
import com.tcvcog.tcvce.entities.NavigationSubItem;
import com.tcvcog.tcvce.entities.Person;
import com.tcvcog.tcvce.entities.Property;
import com.tcvcog.tcvce.entities.occupancy.OccPeriod;
import com.tcvcog.tcvce.entities.PropertyDataHeavy;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.faces.event.ActionEvent;

/**
 * Home for methods that determine where to route users to based on the
 * type of object they click. These guts also coordinate loading related objects
 * such as a case's property when the case is clicked.
 * 
 * 
 * @author Xiaohong Chen & ELLEN Bascomb of Apartment 31Y
 * 
 */
public class NavigationBB extends BackingBeanUtils implements Serializable {

    private boolean noActiveUser;
    private boolean noActiveCase;
    private boolean noActiveProperty;
    private boolean noActiveInspection;
    private boolean noActiveSource;
    private boolean noActivePerson;

    private List<NavigationItem> NavList;

    private List<NavigationItem> sideBarNavList;
    
    /**
     * Creates a new instance of NavigationBB
     */
    public NavigationBB() {
    }

    //Xiaohong
    @PostConstruct
    public void initBean() {
        // Load Navigation lists from SystemCoordinator and place
        // in member variables here
        SystemCoordinator sc = getSystemCoordinator();
        NavList = sc.navList();
        sideBarNavList = sc.buildSideBarNavList(getSessionBean().getSessUser());

        System.out.println("NavigationBB.initBean");

    }
    
    /**
     * Premier method for getting user back to dashboard;
     * I'll refresh CEAR lists and FieldFin lists
     * @return the nav string for dash
     */
    public String navigateToDashboard(){
        getSessionBean().setSessionDomain(FocusedObjectEnum.MUNI_DASHBOARD);
        getSessionInspectionConductor().refreshFieldFinLists(null);
        getSessionBean().refreshCEARUnprocessedList();
        return getSessionBean().getSessionDomain().getNavString();
    }
    
    public String getCurrentDashBoardInfo() {
        SessionBean s = getSessionBean();
        try {
            String info = s.getSessMuni().getMuniName();
            return "Current Municipality: " + info;
        } catch (Exception ex) {
            return "Current Municipality: ";
        }

    }

 

    public String getCurrentCEInfo() {
        SessionBean s = getSessionBean();
        try {
            String caseName = s.getSessCECase().getCaseName();
            String caseId = String.valueOf(s.getSessCECase().getCaseID());
            return "Current Case: " + caseName + " | ID: " + caseId;
        } catch (Exception ex) {
            return "Current Case: " + " | ID: ";
        }

    }

    public String getCurrentPersonInfo() {
        SessionBean sb = getSessionBean();
        try {
            String personName = sb.getSessPerson().getFirstName() 
            + " " + sb.getSessPerson().getLastName(); 
            
            String personId = String.valueOf(sb.getSessPerson().getHumanID());
            return "Current Person: " + personName + " | ID: " + personId;
        } catch (Exception ex) {
            return "Current Person: " + " | ID: ";
        }

    }

    public String getCurrentPeriodInfo() {
        SessionBean s = getSessionBean();
        try {
            String periodId = String.valueOf(s.getSessOccPeriod().getPeriodID());
//            String periodType = s.getSessOccPeriod().getType().getTitle();
            return "Current Permit file ID: " + periodId;
        } catch (Exception ex) {
            return "Current Period: " + " | ID: ";
        }

    }


    public Map<String, String> navCategoryMap() {

        HashMap<String, String> categoryMap;
        categoryMap = new HashMap<>();

        if(NavList != null && !NavList.isEmpty()){

            for (int i = 0; i < NavList.size(); i++) {
                NavigationItem navitem = (NavigationItem) NavList.get(i);
                List subnavList = navitem.getSubNavitem();
                String categoryName = navitem.getValue();
                for (int m = 0; m < subnavList.size(); m++) {
                    NavigationSubItem subnavitem = (NavigationSubItem) subnavList.get(m);
                    String pagePath = subnavitem.extractPagePath();
                    categoryMap.put(pagePath, categoryName);
                }
            }
        }
        return categoryMap;
    }

    public String getCurrentPageNavItemValue() {
        String currentViewPagePath = getviewPagePath();
        return navCategoryMap().get(currentViewPagePath);
    }

    public String getviewPagePath() {
        FacesContext fc = FacesContext.getCurrentInstance();
        String viewID = fc.getViewRoot().getViewId();
        return viewID;
    }

    /**
     * Listener to view the muni property DH
     * @return 
     */
    public String navigateToMuniPropertyDH(){
        try {
            System.out.println("NavigationBB.navigateToMuniPropertyDH with parcelID: " + getSessionBean().getSessMuni().getMuniPropertyDH().getCountyParcelID());
            return getSessionBean().navigateToPageCorrespondingToObject(getSessionBean().getSessMuni().getMuniPropertyDH());
        } catch (BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(),
                            ""));
        }
        return "";
    }
 
    /**
     * Listener for user clicks of the "case list" link in the session cecase bar
     * @param ev 
     */
    public void viewSessionCECaseList(ActionEvent ev){
        System.out.println("NavigationBB.viewSessionCECaseList");
    }
    
    public List<NavigationItem> getNavList() {
        return NavList;
    }

    public void setNavList(List<NavigationItem> NavList) {
        this.NavList = NavList;
    }

    public List<NavigationItem> getSideBarNavList() {
        return sideBarNavList;
    }

    public void setSideBarNavList(List<NavigationItem> sideBarNavList) {
        this.sideBarNavList = sideBarNavList;
    }
    
    
    public String onPropertyListItemSelect(PropertyDataHeavy prop){
        String navTo = "";
        try {
            navTo = getSessionBean().navigateToPageCorrespondingToObject(prop);
        } catch (BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(),
                            ""));
        }
        return navTo;
    }
    
    
    /**
     * Listener for user requests to view a cecase from the active object box navlist
     * @param cse
     * @return 
     */
    public String onCECaseListItemSelect(CECase cse){
        String navTo = "";
        try {
            navTo = getSessionBean().navigateToPageCorrespondingToObject(cse);
        } catch (BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(),
                            ""));
        }
        return navTo;
        
    }

    /**
     * Listener for user requests to view an occ period from the active object box navlist
     * @param period
     * @return
     */
    public String onOccPeriodListItemSelect(OccPeriod period){
        
        try {
            return getSessionBean().navigateToPageCorrespondingToObject(period);
        } catch (BObStatusException | AuthorizationException ex) {
            System.out.println(ex);

            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
        }

        return "";
    }
    
    public String onViewPersonListItemSelect(Person pers) {
       String navTo = "";
        try {
            navTo = getSessionBean().navigateToPageCorrespondingToObject(pers);
        } catch (BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(),
                            ""));
        }
        return navTo;
    }
    
    public String onOrdinancesLinkClick(){
        return "codeElementManage";
    }
    
    public String onCodebookLinkClick(){
        return "codeSetManage";
        
    }
    
    public String onChecklistsLinkClick(){
        return "checklistTools";
        
    }
    
    /**
     * listener for user requests to see address manager
     * @param ev 
     */
    public void onAddressManagerViewLinkClick(ActionEvent ev){
        System.out.println("View Addresses!");
    }

    /**
     * @return the noActiveCase
     */
    public boolean isNoActiveCase() {
        CECaseDataHeavy c = getSessionBean().getSessCECase();
        noActiveCase = (c == null);
        return noActiveCase;
    }

    /**
     * @return the noActiveProperty
     */
    public boolean isNoActiveProperty() {
        Property p = getSessionBean().getSessProperty();
        noActiveProperty = (p == null);
        return noActiveProperty;
    }

    /**
     * @return the noActiveInspection
     */
    public boolean isNoActiveInspection() {
        return noActiveInspection;
    }

    /**
     * @param noActiveCase the noActiveCase to set
     */
    public void setNoActiveCase(boolean noActiveCase) {
        this.noActiveCase = noActiveCase;
    }

    /**
     * @param noActiveProperty the noActiveProperty to set
     */
    public void setNoActiveProperty(boolean noActiveProperty) {
        this.noActiveProperty = noActiveProperty;
    }

    /**
     * @param noActiveInspection the noActiveInspection to set
     */
    public void setNoActiveInspection(boolean noActiveInspection) {
        this.noActiveInspection = noActiveInspection;
    }

    /**
     * @return the noActiveSource
     */
    public boolean isNoActiveSource() {
        CodeSource cs = getSessionBean().getSessCodeSource();
        noActiveSource = (cs == null);
        return noActiveSource;
    }

    /**
     * @param noActiveSource the noActiveSource to set
     */
    public void setNoActiveSource(boolean noActiveSource) {
        this.noActiveSource = noActiveSource;
    }

    /**
     * @return the noActivePerson
     */
    public boolean isNoActivePerson() {
        Person p = getSessionBean().getSessPerson();
        noActivePerson = (p == null);
        return noActivePerson;
    }

    /**
     * @param noActivePerson the noActivePerson to set
     */
    public void setNoActivePerson(boolean noActivePerson) {
        this.noActivePerson = noActivePerson;
    }

    /**
     * @return the noActiveUser
     */
    public boolean isNoActiveUser() {
        noActiveUser = (getSessionBean().getSessUser() == null);
        return noActiveUser;
    }

    /**
     * @param noActiveUser the noActiveUser to set
     */
    public void setNoActiveUser(boolean noActiveUser) {
        this.noActiveUser = noActiveUser;
    }

}
