/*
 * Copyright (C) 2018 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.application;


import com.tcvcog.tcvce.coordinators.CodeCoordinator;
import com.tcvcog.tcvce.coordinators.MunicipalityCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.CodeElement;
import com.tcvcog.tcvce.entities.CodeSource;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.TreeNodeOrdinanceWrapper;
import com.tcvcog.tcvce.util.MessageBuilderParams;
import java.io.Serializable;
import java.util.List;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.TreeNode;

/**
 * Backing bean for codeElementManage.xhtml
 * Provides facilities for adding, updating, and nuking 
 * Code Sources
 * Code Elements (Ordinances)
 * Code Guide records
 * 
 * Since this was created back in 2016, when reviewing in 2024, I realize that code
 * source objects don't actually have their list of elements in their belly which is 
 * a nonstandard construction
 * 
 * @author ellen bascomb of apt 31y
 */
public class CodeElementBB 
        extends BackingBeanUtils 
        implements Serializable{


    final String FACESPAGE_NAV_CODEELEMENTMANAGE = "codeElementManage";
    
    private List<CodeElement> codeElementList;
    private List<CodeElement> codeElementListFiltered;
    private CodeElement currentElement;
    private TreeNode<TreeNodeOrdinanceWrapper<CodeElement>> elementTreeView;

    private List<CodeSource> codeSourceList;
    private CodeSource currentCodeSource;
    private boolean cloneSourceMode;
    private CodeSource cloneParentSource;
    
    private Municipality formCodeSourceMuniSelected;
    private boolean codeSourceListIncludeDeactivated;
    
    private boolean permissionAllowCodeSourceEdit;
    
    private String formNoteText;
    private boolean ceNotesAppendMode;
    private String ordinanceFilterText;
    
    private boolean editModeAllHeaders;
    private boolean expandOrdTableRows;
    private boolean includeSourceNameAndYear;

    /**
     * Creates a new instance of CodeElementBB
     */
    public CodeElementBB() {
    }
    
   /**
    * Initializes the bean
    */
    @PostConstruct
    public void initBean() {
        codeSourceListIncludeDeactivated = false;
        refreshCodeSourceList();
        if(getSessionBean().getSessCodeSource() != null){
            currentCodeSource = getSessionBean().getSessCodeSource();
        } else if(codeSourceList != null && !codeSourceList.isEmpty()){
            currentCodeSource = codeSourceList.get(0);
        }
        refreshElementListOfCurrentCodeSource();
        configurePermissionsForCurrentCodeSource();
        ordinanceFilterText = "";
        editModeAllHeaders = false;
        expandOrdTableRows =false;
        includeSourceNameAndYear = true;
    }
    
    /**
     * Asks the CodeCoordinator to set the boolean permissions flag on this bean 
     * for code source editing permissions.
     */
    private void configurePermissionsForCurrentCodeSource(){
        CodeCoordinator cc = getCodeCoordinator();
        permissionAllowCodeSourceEdit = cc.permissionsCheckpointManageCodeSource(currentCodeSource, getSessionBean().getSessUser());
    }
    
    /**
     * Fetches a new code source list
     * @param ev
     */
    public void refreshCodeSourceList(){
        CodeCoordinator cc = getCodeCoordinator();
        try {
            codeSourceList = cc.getCodeSourceList(codeSourceListIncludeDeactivated);
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }
        
    }
    
    // *************************************************************
    // *********************CODE SOURCES****************************
    // *************************************************************
    
    
    /**
     * Listener for user requests to view code elements by selecting a source
     * @param source 
     */
    public void onCodeSourceViewButtonChange(CodeSource source){
        currentCodeSource = source;
        ordinanceFilterText = "";
        getSessionBean().setSessCodeSource(source);
        configurePermissionsForCurrentCodeSource();
        refreshElementListOfCurrentCodeSource();
        buildCodeElementTreeFromSource();
    }
    
    /**
     * Asks the coordinator for a tree view of our current code source
     */
    private void buildCodeElementTreeFromSource(){
        CodeCoordinator cc = getCodeCoordinator();
        if(currentCodeSource != null){
            try {
                elementTreeView = cc.buildTreeFromCodeSet(codeElementList);
            } catch (BObStatusException ex) {
                System.out.println("Unable to build tree from code source");
            }
        }
    }
    
    
    /**
     * Listener for user requests to start a source update operation
     * @param source 
     */
    public void onCodeSourceUpdateInitButtonChange(CodeSource source){
        currentCodeSource = source;
        MunicipalityCoordinator mc = getMuniCoordinator();
        // to avoid db cycles, codeSources contain flattened muni code and name and not a muni object
        try {
            formCodeSourceMuniSelected = mc.getMuni(currentCodeSource.getMuniCodeFlattened());
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
                        getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_ERROR,
                               ex.getMessage(),""));
        }
        
    }
    
    
    
    /**
     * Listener for user requests to finalize a source update operation
     * @param ev
     */
    public void onCodeSourceUpdateCommitButtonChange(ActionEvent ev){
        CodeCoordinator cc = getCodeCoordinator();
        try {
            if(formCodeSourceMuniSelected != null && currentCodeSource != null){
                System.out.println("CodeElementBB.onCodeSourceUpdateCommitButtonChange");
                currentCodeSource.setMuniCodeFlattened(formCodeSourceMuniSelected.getMuniCode());
            }
            cc.updateCodeSourceMetadata(currentCodeSource, getSessionBean().getSessUser());
            refreshCodeSourceList();
            getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_INFO,
                               "Updated code source: " + currentCodeSource.getSourceName(),""));
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_ERROR,
                               ex.getMessage(),""));
        }
    }
    
    /**
     * Listener for user requests to initiate a source remove operation
     * @param source 
     */
    public void onCodeSourceNukeInitButtonChange(CodeSource source){
        currentCodeSource = source;
    }
    
    /**
     * listener for users to stop the editing/updating process
     * @param ev 
     */
    public void onSourceAddEditCancel(ActionEvent ev){
        System.out.println("CodeElementBB.onSourceAddEditCancel");
    }
    
    
    /**
     * Listener for user requests to finalize a source removal operation
     * @param ev
     */
    public void onCodeSourceNukeCommitButtonChange(ActionEvent ev){
        
        CodeCoordinator cc = getCodeCoordinator();
        try {
            cc.deactivateCodeSource(currentCodeSource, getSessionBean().getSessUser());
            refreshCodeSourceList();
            getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_INFO,
                               "Poof! Code source " + currentCodeSource.getSourceName() + " has been Nuked!"
                                + "",""));
            
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_ERROR,
                               ex.getMessage(),""));
                        

        }
    }
    
    /**
     * Listener for user requests to start the source creation operation
     */
    public void onCodeSourceAddInitButtonChange(){
        CodeCoordinator cc = getCodeCoordinator();
        currentCodeSource = cc.getCodeSourceSkeleton();
        formCodeSourceMuniSelected = getSessionBean().getSessMuni();
        
    }
    
    /**
     * Listener for user requests to finalize the source creation process
     * @param ev
     */
    public void onCodeSourceAddCommitButtonChange(ActionEvent ev){
        CodeCoordinator cc = getCodeCoordinator();
            try {
                if(formCodeSourceMuniSelected != null && currentCodeSource != null){
                    currentCodeSource.setMuniCodeFlattened(formCodeSourceMuniSelected.getMuniCode());
                }
                if(!cloneSourceMode){
                    int freshID = cc.addNewCodeSource(currentCodeSource, 
                                        getSessionBean().getSessUser(), 
                                        getSessionBean().getSessMuni().getProfile());
                    currentCodeSource = cc.getCodeSource(freshID);
                    System.out.println("CodeElementBB.onCodeSourceAddCommitButtonChange");
                    refreshCodeSourceList();
                    getFacesContext().addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_INFO,
                                       "Succcessfully added new code source!",""));
                } else {
                    System.out.println("Routing to clone commit");
                    onCloneSourceCommit();
                }
            } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                           new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                 ex.getMessage(),""));
            }
    }
    
    
    /**
     * listener for user requests to clone the current source to a new source
     * @param ev 
     */
    public void onCloneSourceInitButtonChange(ActionEvent ev){
        CodeCoordinator cc = getCodeCoordinator();
        System.out.println("CodeElementBB.onCloneSourceInitButtonChange");
        cloneParentSource = currentCodeSource;
        currentCodeSource = cc.getCodeSourceSkeleton();
        cloneSourceMode = true;
        
    }
    
    /**
     * Writes the new code source to the DB as a clone of another 
     * source
     */
    public void onCloneSourceCommit(){
        CodeCoordinator cc = getCodeCoordinator();
        System.out.println("CodeElementBB.onCloneSourceCommit");
        try {
            currentCodeSource = cc.cloneCodeSource(cloneParentSource, currentCodeSource, getSessionBean().getSessUser());
            onCodeSourceViewButtonChange(currentCodeSource);
            refreshCodeSourceList();
            getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_INFO,
                               "Succcessfully cloned " + cloneParentSource.getSourceName() + " to " + currentCodeSource.getSourceName(),""));
            getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_INFO,
                               "Check the notes on the new source for clone operation details!",""));
            cloneSourceMode = false;
            cloneParentSource = null;
        } catch (BObStatusException | AuthorizationException | IntegrationException  ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_ERROR,
                             ex.getMessage(),""));
            
        } 
    }
    
    // *************************************************************
    // **************CODE ELEMENTS (ORDINANCES)*********************
    // *************************************************************
   
    
    /**
     * Utility container for population logic
     */
    private void refreshElementListOfCurrentCodeSource(){
        
        CodeCoordinator cc = getCodeCoordinator();
        if(currentCodeSource == null) return;
        try {
            codeElementList = cc.getCodeElemements(currentCodeSource);
            codeElementListFiltered = codeElementList;
            if(codeElementList != null && !codeElementList.isEmpty()){
                System.out.println("Loaded code element list of size: " + codeElementList.size());
            }
            System.out.println("");
            getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_INFO,
                               "Viewing ordinances in " + getCurrentCodeSource().getSourceName(),""));
        } catch (IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to load ordiance list: " + ex.getMessage(),""));
            
        }
        
    }
    
    /**
     * Pro forma listener for help box pop up
     * @param ev 
     */
    public void onViewHelpButtonClick(ActionEvent ev){
        
    }

    
    /**
     * Gets a fresh copy of the current element
     */
    private void refreshCurrentElement(){
        CodeCoordinator cc = getCodeCoordinator();
        if(currentElement != null){
            try {
                currentElement = cc.getCodeElement(currentElement.getElementID());
            } catch (IntegrationException ex) {
                System.out.println(ex);
            }
        }
    }
    
    /**
     * Internal organ for placing the current code element at the top of the
     * ele list for easy checking of add/update
     */
    private void placeCurrentElementAtHeadOfList(){
        if(currentElement != null && codeElementListFiltered != null){
            codeElementListFiltered.remove(currentElement);
            codeElementListFiltered.add(0, currentElement);
        }
    }

    
    /**
     * Listener for user requests to remove a code element
     * @param ele
     
     */
    public void onElementNukeInitButtonChange(CodeElement ele){
       currentElement = ele;
        
    }
    
    public String onElementNukeButtonChange() {
     
        CodeCoordinator cc = getCodeCoordinator();
        try {
            cc.deactivateCodeElement(currentElement, getSessionBean().getSessUser());
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Deactivated code element " + currentElement.getElementID(),
                            ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                          ex.getMessage(),
                            "This is probably because it has been included in a code book"));
        }
        return FACESPAGE_NAV_CODEELEMENTMANAGE;
    }
    
    
    
    public void onElementAddInitButtonChange(ActionEvent ev){
        if(currentCodeSource != null){
            CodeCoordinator cc = getCodeCoordinator();
            currentElement = cc.getCodeElementSkeleton(currentCodeSource);
            System.out.println("CodeElementBB.onElementUpdateInitButtonChange:  Updating " + currentElement.getElementID() );
        } 
        
    }
    
    /**
     * Allows users to pre-poulate a new ordinance with exactly the same ch, 
     * sec, sub sec, and sub sub sec
     * @param ele 
     */
    public void onElementAddBasedOnExistingOrdLinkClick(CodeElement ele){
        CodeCoordinator cc = getCodeCoordinator();
        onElementAddInitButtonChange(null);
        try {
            currentElement = cc.cloneCodeElementChapSec(ele, currentElement);
        } catch (BObStatusException ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                          ex.getMessage(), ""));
        }
        getFacesContext().addMessage(null,
               new FacesMessage(FacesMessage.SEVERITY_INFO,
                     "Cloned ord ID " + ele.getElementID() + "! Overwrite the new ordinance ch/sec info.", ""));
        
        
    }
    
    /**
     * Start new element update
     * @param ele
     * @throws InvocationTargetException 
     */
    public void onElementUpdateInitButtonChange(CodeElement ele) throws InvocationTargetException {
        System.out.println("CodeElementBB.onElementUpdateInitButtonChange:  Updating " + ele.getElementID() );
        currentElement = ele;
        formNoteText = "";
        setupHeaderCandidateForElementEdit(currentElement);
        onCodeElementPrepareNotes();
        
    }
    
    /**
     * Logic to inject the current static header into the candidate for editing
     * @param ele 
     */
    private void setupHeaderCandidateForElementEdit(CodeElement ele){
        if(ele != null){
            ele.setHeaderStringCandidate(ele.getHeaderStringStatic());
        }
    }
    
    
    /**
     * Listener for ordinance update commits
     * @param ev 
     */
    public void onElementUpdateCommitButtonChange(ActionEvent ev){
         CodeCoordinator cc = getCodeCoordinator();
        try {
            // we don't need this
//            onCodeElementCommitNotes(null);
            cc.updateCodeElement(currentElement, getSessionBean().getSessUser());
            onWriteSingleOrdStaticHeader(currentElement);
            refreshCurrentElement();
            placeCurrentElementAtHeadOfList();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Updated code element " + currentElement.getElementID(),
                            ""));
             
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(),
                            "This must be corrected by the System Administrator"));
             getFacesContext().validationFailed();
        }
    }
    
    /**
     * listener for user requests to commit the current ordinance to the DB with INSERT
     * @return 
     */
    public String onElementAddCommitButtonChange() {
        CodeCoordinator cc = getCodeCoordinator();
        
        
        try {
            int freshID = cc.insertCodeElement(currentElement, getSessionBean().getSessUser());
            currentElement.setElementID(freshID);
            onWriteSingleOrdStaticHeader(currentElement);
            refreshCurrentElement();
            placeCurrentElementAtHeadOfList();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Added new code element",""));
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                           ex.getMessage(), ""));
            getFacesContext().validationFailed();
        }

        return FACESPAGE_NAV_CODEELEMENTMANAGE;
    }
    
    /**
     * Listener to start the header edit process for all ords in the selected source
     * @param ev
     */
    public void onEditAllHeadersToggle(ActionEvent ev){
        CodeCoordinator cc = getCodeCoordinator();
        System.out.println("CodeElementBB.onEditAllHeadersToggle");
        if(editModeAllHeaders){
            writeStaticHeadersForAllOrdsInSource();
        } else {
            // generate candidate headers from existing static headers
            cc.generateHeaderStringCandidates(codeElementList);
        }
        expandOrdTableRows = !expandOrdTableRows;
        editModeAllHeaders = !editModeAllHeaders;
    }
    
    
    /**
     * Dummy listener to show custom header rendered in the dialog
     * @param ev 
     */
    public void onShowCustomHeaderPreview(ActionEvent ev){
        System.out.println("CodeElementBB.onShowCustomHeaderPreview");
    }
    
    /**
     * turns on expanded rows true on data table
     * @param ev
     */
    public void onExpandAllElementRows(ActionEvent ev){
        expandOrdTableRows = true;
        System.out.println("onExpandAllElementRows");
    }
    
    /**
     * Flips edit mode of headers to false
     * @param ev
     */
    public void onHeaderBatchEditAbort(ActionEvent ev){
        editModeAllHeaders = false;
        System.out.println("CodeElementBB.onHeaderBatchEditAbort");
        getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Done editing headers without batch save",""));
    }
    
    /**
     * Sends a single code element to update its header
     * @param ele 
     */
    public void onWriteSingleOrdStaticHeader(CodeElement ele){
        try {
            System.out.println("CodeElementBB.onWriteSingleOrdStaticHeader | " + ele.getElementID());
            CodeCoordinator cc = getCodeCoordinator();
            cc.updateCodeElementHeaderStringStatic(ele, currentCodeSource, getSessionBean().getSessUser());
            ele = cc.getCodeElement(ele.getElementID());
            
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "New header written for " + ele.getHeaderStringStatic(),""));
        } catch (IntegrationException | AuthorizationException ex) {
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                           ex.getMessage(), ""));
            
        }
        
    }
    
    /**
     * Asks the codecoordinator for the given element's dynamically generated 
     * header string assuming IRC format
     * @param ele 
     * @param useIRC determines if the coordinator method will be called
     * that assumes IRC recurisve listings
     */
    public void onGenerateHeaderStringIRCForSingleOrd(CodeElement ele, boolean useIRC){
        CodeCoordinator cc = getCodeCoordinator();
        cc.generateHeaderForCodeElement(ele, useIRC, includeSourceNameAndYear);
//        codeElementList.add(codeElementList.indexOf(ele), ele);
        System.out.println("CodeElementBB.onRegenerateHeaderStringIRC | " + ele.getHeaderString());
        getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Header regenerated",""));
    }   
    
    
    /**
     * Asks coordinator to generate candidate headers for all ords
     * @param useIRC
     */
    public void onGenerateHeaderStringForAllElements(boolean useIRC){
        CodeCoordinator cc =getCodeCoordinator();
        codeElementList = cc.generateHeaderStringForElementList(codeElementList, useIRC, includeSourceNameAndYear);
        System.out.println("CodeElementBB.onGenerateHeaderStringForAllElements");
    }
    
    
    /**
     * Asks the coordinator to update all static headers from 
     * the form fields
     */
    private void writeStaticHeadersForAllOrdsInSource(){
        CodeCoordinator cc = getCodeCoordinator();
        try {
            cc.updateCodeElementListHeaderString(codeElementList, currentCodeSource, getSessionBean().getSessUser());
            refreshElementListOfCurrentCodeSource();
            System.out.println("CodeElementBB.writeStaticHeadersForAllOrdsInSource");
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "New headers written to all ordinances",""));
        } catch (IntegrationException | AuthorizationException ex) {
            System.out.println("ex");
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                           ex.getMessage(), ""));
        }
        
    }
    
    /**
     * Starts the raw text editng process
     * @param ele 
     */
    public void onOrdRawTextEditInit(CodeElement ele){
        currentElement = ele;
    }
    
    /**
     * Listener to commit updates to an ord raw text
     * @param ev 
     */
    public void onOrdRawTextEditCommit(ActionEvent ev){
        CodeCoordinator cc = getCodeCoordinator();
        try {
            cc.updateCodeElementTechnicalTextOnly(currentElement, getSessionBean().getSessUser());
            refreshCurrentElement();
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                           "Successfully updated ordinance technical text", ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException  ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                           ex.getMessage(), ""));
        } 
        
    }
    
    /**
     * listener for user requests to cancel ord raw text edit
     * @param ev 
     */
    public void onOrdRawTextEditAbort(ActionEvent ev){
        System.out.println("CodeElementBB.Raw text edit abort");
    }
    
    
    // *************************************************************
    // *************************** NOTES ***************************
    // *************************************************************
   
    
    
    public void onNoteInitButtonChange(CodeElement ce) {
        currentElement = ce;
        formNoteText = "";
        ceNotesAppendMode = false;
    }
    
    /**
     * Listener for Preparing notes for CodeElement
     */
    public void onCodeElementPrepareNotes() throws InvocationTargetException {
        SystemCoordinator sc = getSystemCoordinator();
        String fieldDump = "";
        fieldDump = sc.getFieldDump(currentElement);
        System.out.println(fieldDump);
        setFormNoteText(fieldDump);
    }
    
    /**
     * Listener for user Requests to commit notes to the CodeElement
     *
     * @param ev
     */
    public void onCodeElementCommitNotes(ActionEvent ev) {
        SystemCoordinator sc = getSystemCoordinator();
        if (Objects.nonNull(currentElement))
        {
            MessageBuilderParams mbp = new MessageBuilderParams(currentElement.getNotes(), null, null, formNoteText, getSessionBean().getSessUser(), null);
            currentElement.setNotes(sc.universalAppendNoteBlock(mbp));
        }
    }
     
     /**
     * Listener for user Requests to commit notes to the codeElement
     *
     * @param ev
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void onCodeElementNoteCommitButtonChage() throws AuthorizationException {
        SystemCoordinator sc = getSystemCoordinator();
        CodeCoordinator cc = getCodeCoordinator();
        if (Objects.nonNull(currentElement))
        {
            MessageBuilderParams mbp = new MessageBuilderParams(currentElement.getNotes(), null, null, formNoteText, getSessionBean().getSessUser(), null);
            currentElement.setNotes(sc.appendNoteBlock(mbp));
            try
            {
                cc.updateCodeElement(currentElement, getSessionBean().getSessUser());
            } catch (BObStatusException | IntegrationException ex)
            {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "FATAL Error on code element note commit", ""));
            }
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Successfully added note to code element", ""));

        }
        formNoteText = "";
        changeCeNotesAppendMode();

    }
     
    public void changeCeNotesAppendMode() {
        ceNotesAppendMode = !ceNotesAppendMode;
    }

    public void onCodeElementAbortAppendNotes() {
        formNoteText = "";
        changeCeNotesAppendMode();
    }
    
     /**
     * To build CodeElementListFiltered with respect to ordinance filter text
     */
    private void applyOrdinanceFilter() {
        CodeCoordinator cc = getCodeCoordinator();
        codeElementListFiltered = cc.codeElementAction_configureCodeElementListFiltered(codeElementList, ordinanceFilterText);
    }

    /**
     * To clear applied ordinace filter
     */
    private void clearOrdinanceFilter() {
        ordinanceFilterText = "";
        codeElementListFiltered = codeElementList;
    }

    /**
     * Listener for user requests to apply ordinance filter
     */
    public void onApplyOrdinanceFilter() {
        applyOrdinanceFilter();
    }

    /**
     * Listener for user requests to clear ordinance filter
     */
    public void onClearOrdinanceFilter() {
        clearOrdinanceFilter();
    }
 
    // *************************************************************
    // ******************* GETTERS AND SETTERS *********************
    // *************************************************************
   

    /**
     * @param currentCodeSource the currentCodeSource to set
     */
    public void setCurrentCodeSource(CodeSource currentCodeSource) {
        this.currentCodeSource = currentCodeSource;
    }

    /**
     * @return the currentElement
     */
    public CodeElement getCurrentElement() {
        
        return currentElement;
    }

    /**
     * @param currentElement the currentElement to set
     */
    public void setCurrentElement(CodeElement currentElement) {
        this.currentElement = currentElement;
    }

   

    /**
     * @return the codeSourceList
     */
    public List<CodeSource> getCodeSourceList() {
        return codeSourceList;
    }

    /**
     * @param codeSourceList the codeSourceList to set
     */
    public void setCodeSourceList(List<CodeSource> codeSourceList) {
        this.codeSourceList = codeSourceList;
    }

    /**
     * @return the currentCodeSource
     */
    public CodeSource getCurrentCodeSource() {
        return currentCodeSource;
    }

    /**
     * @return the codeElementList
     */
    public List<CodeElement> getCodeElementList() {
        return codeElementList;
    }

    /**
     * @param codeElementList the codeElementList to set
     */
    public void setCodeElementList(List<CodeElement> codeElementList) {
        this.codeElementList = codeElementList;
    }

    /**
     * @return the formNoteText
     */
    public String getFormNoteText() {
        return formNoteText;
    }

    /**
     * @param formNoteText the formNoteText to set
     */
    public void setFormNoteText(String formNoteText) {
        this.formNoteText = formNoteText;
    }

    /**
     * @return the codeElementListFiltered
     */
    public List<CodeElement> getCodeElementListFiltered() {
        return codeElementListFiltered;
    }

    /**
     * @param codeElementListFiltered the codeElementListFiltered to set
     */
    public void setCodeElementListFiltered(List<CodeElement> codeElementListFiltered) {
        this.codeElementListFiltered = codeElementListFiltered;
    }

    /**
     * @return the codeSourceListIncludeDeactivated
     */
    public boolean isCodeSourceListIncludeDeactivated() {
        return codeSourceListIncludeDeactivated;
    }

    /**
     * @param codeSourceListIncludeDeactivated the codeSourceListIncludeDeactivated to set
     */
    public void setCodeSourceListIncludeDeactivated(boolean codeSourceListIncludeDeactivated) {
        this.codeSourceListIncludeDeactivated = codeSourceListIncludeDeactivated;
    }

    /**
     * @return the permissionAllowCodeSourceEdit
     */
    public boolean isPermissionAllowCodeSourceEdit() {
        return permissionAllowCodeSourceEdit;
    }

    /**
     * @param permissionAllowCodeSourceEdit the permissionAllowCodeSourceEdit to set
     */
    public void setPermissionAllowCodeSourceEdit(boolean permissionAllowCodeSourceEdit) {
        this.permissionAllowCodeSourceEdit = permissionAllowCodeSourceEdit;
    }

    /**
     * @return the formCodeSourceMuniSelected
     */
    public Municipality getFormCodeSourceMuniSelected() {
        return formCodeSourceMuniSelected;
    }

    /**
     * @param formCodeSourceMuniSelected the formCodeSourceMuniSelected to set
     */
    public void setFormCodeSourceMuniSelected(Municipality formCodeSourceMuniSelected) {
        this.formCodeSourceMuniSelected = formCodeSourceMuniSelected;
    }

    public boolean isCeNotesAppendMode() {
        return ceNotesAppendMode;
    }

    public void setCeNotesAppendMode(boolean ceNotesAppendMode) {
        this.ceNotesAppendMode = ceNotesAppendMode;
    }
    
    public String getOrdinanceFilterText() {
        return ordinanceFilterText;
    }

    public void setOrdinanceFilterText(String ordinanceFilterText) {
        this.ordinanceFilterText = ordinanceFilterText;
    }

    /**
     * @return the editModeAllHeaders
     */
    public boolean isEditModeAllHeaders() {
        return editModeAllHeaders;
    }

    /**
     * @param editModeAllHeaders the editModeAllHeaders to set
     */
    public void setEditModeAllHeaders(boolean editModeAllHeaders) {
        this.editModeAllHeaders = editModeAllHeaders;
    }

    /**
     * @return the expandOrdTableRows
     */
    public boolean isExpandOrdTableRows() {
        return expandOrdTableRows;
    }

    /**
     * @param expandOrdTableRows the expandOrdTableRows to set
     */
    public void setExpandOrdTableRows(boolean expandOrdTableRows) {
        this.expandOrdTableRows = expandOrdTableRows;
    }

    /**
     * @return the includeSourceNameAndYear
     */
    public boolean isIncludeSourceNameAndYear() {
        return includeSourceNameAndYear;
    }

    /**
     * @param includeSourceNameAndYear the includeSourceNameAndYear to set
     */
    public void setIncludeSourceNameAndYear(boolean includeSourceNameAndYear) {
        this.includeSourceNameAndYear = includeSourceNameAndYear;
    }

    /**
     * @return the elementTreeView
     */
    public TreeNode<TreeNodeOrdinanceWrapper<CodeElement>> getElementTreeView() {
        return elementTreeView;
    }

    /**
     * @param elementTreeView the elementTreeView to set
     */
    public void setElementTreeView(TreeNode<TreeNodeOrdinanceWrapper<CodeElement>> elementTreeView) {
        this.elementTreeView = elementTreeView;
    }

    /**
     * @return the cloneSourceMode
     */
    public boolean isCloneSourceMode() {
        return cloneSourceMode;
    }

    /**
     * @param cloneSourceMode the cloneSourceMode to set
     */
    public void setCloneSourceMode(boolean cloneSourceMode) {
        this.cloneSourceMode = cloneSourceMode;
    }

    /**
     * @return the cloneParentSource
     */
    public CodeSource getCloneParentSource() {
        return cloneParentSource;
    }

    /**
     * @param cloneParentSource the cloneParentSource to set
     */
    public void setCloneParentSource(CodeSource cloneParentSource) {
        this.cloneParentSource = cloneParentSource;
    }
    
    
}
