/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * POJO representing a task that users must attend to.
 * 
 * @author pierre15
 */
public class AdministrativeFlag {
    
    private AdministrativeFlagTypeEnum flagTypeEnum;
    private String flagMessage;
    private Icon icon;
    private int parentID;

    public AdministrativeFlag(AdministrativeFlagTypeEnum flagTypeEnum, String flagMessage, Icon icon, int parentID) {
        this.flagTypeEnum = flagTypeEnum;
        this.flagMessage = flagMessage;
        this.icon = icon;
        this.parentID = parentID;
    }
    
    /**
     * no args
     */
    public AdministrativeFlag(){
        
    }

    /**
     * @return the flagTypeEnum
     */
    public AdministrativeFlagTypeEnum getFlagTypeEnum() {
        return flagTypeEnum;
    }

    /**
     * @param flagTypeEnum the flagTypeEnum to set
     */
    public void setFlagTypeEnum(AdministrativeFlagTypeEnum flagTypeEnum) {
        this.flagTypeEnum = flagTypeEnum;
    }

    /**
     * @return the flagMessage
     */
    public String getFlagMessage() {
        return flagMessage;
    }

    /**
     * @param flagMessage the flagMessage to set
     */
    public void setFlagMessage(String flagMessage) {
        this.flagMessage = flagMessage;
    }

    /**
     * @return the icon
     */
    public Icon getIcon() {
        return icon;
    }

    /**
     * @param icon the icon to set
     */
    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    /**
     * @return the parentID
     */
    public int getParentID() {
        return parentID;
    }

    /**
     * @param parentID the parentID to set
     */
    public void setParentID(int parentID) {
        this.parentID = parentID;
    }
    
}
