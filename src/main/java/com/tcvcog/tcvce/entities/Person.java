/*
 * Copyright (C) 2017 Turtle Creek Valley Council of Governements
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.application.interfaces.IFace_Loggable;
import com.tcvcog.tcvce.util.Constants;
import java.util.List;
import com.tcvcog.tcvce.application.interfaces.IFaceActivatableBOB;

/**
 * Model object representing a person in the system. A Person has a type
 coordinated through the enum @PersonType. Contains getters and setters for
 database fields related to a Person, stored in the person table. 
 * 
 * @author Eric Darsow
 */
public  class       Person 
        extends     Human
        implements  IFace_Loggable,
                    IFace_addressListHolder,
                    IFaceActivatableBOB,
                    IFaceSessionSyncTarget{
    
    final static LinkedObjectSchemaEnum PERSON_ADDRESS_LOSE = LinkedObjectSchemaEnum.MailingaddressHuman;
    final static LinkedObjectSchemaEnum HUMAN_HUMAN_LOSE = LinkedObjectSchemaEnum.HumanHuman;
    
    
    protected List<MailingAddressLink> mailingAddressLinkList;
    protected List<ContactEmail> emailList;
    protected List<ContactPhone> phoneList;
    protected String mailingAddressListPretty;
    protected String emailListPretty;
    protected String phoneListPretty;
    
    // human linking and aliasing
    
    protected List<HumanAlias> aliasList;
    

    /**
     * Method for cloning Person objects
     * 
     * @param per The person we would like to clone
     */
    public Person(Person per) {
       super(per);
       this.mailingAddressLinkList = per.mailingAddressLinkList;
       this.emailList = per.emailList;
       this.phoneList = per.phoneList;
        
       this.mailingAddressListPretty = per.mailingAddressListPretty;
       this.emailListPretty = per.emailListPretty;
       this.phoneListPretty = per.phoneListPretty;
       
       this.aliasList = per.aliasList;
    }
    
    /**
     * Creates a person out of a human
     * With none of this class's person fields populated
     * @param h 
     */
    public Person(Human h){
      super(h);
        
        
    }

    /**
     * Reverse compat method to help with humanization
     * that returns the humanID
     * @return 
     */
    public int getPersonID(){
        return humanID;
    
    }
    
    
    @Override
    public void accept(IFaceSessionSyncVisitor visitor) {
        visitor.visit(this);
    }

    
    /**
     * Concatenates the pretty printed mad links, phones, and emails
     * to one HTML rich text blob
     * @return 
     */
    public String getPrettyContactStringComplete(){
        StringBuilder sb = new StringBuilder();
        if(mailingAddressListPretty != null){
            sb.append(mailingAddressListPretty);
            sb.append(Constants.FMT_HTML_BREAK);
        }
        if(phoneListPretty != null){
            sb.append(phoneListPretty);
            sb.append(Constants.FMT_HTML_BREAK);
        }
        if(emailListPretty != null){
            sb.append(emailListPretty);
            sb.append(Constants.FMT_HTML_BREAK);
        }
        
        return removeLeadingTrailingDoubleBreaks(sb.toString());
        
    }
    
    
    
    /**
     * Strips double breaks or leading/trailing breaks
     * Written by chat GPT! Thanks chat for turning my brain to mush
     * @param input
     * @return 
     */
    public static String removeLeadingTrailingDoubleBreaks(String input) {
        if(input != null){
            // Remove leading, trailing, or double <br> or <br /> tags
            input = input.replaceAll("^(<br>|<br\\s*/?>)+", ""); // Leading
            input = input.replaceAll("(<br>|<br\\s*/?>)+$", ""); // Trailing
            input = input.replaceAll("(<br>|<br\\s*/?>)+(<br>|<br\\s*/?>)+", "<br />"); // Double
        }
        return input;
    }
    
    
    /**
     * Utility method for extracting the first address
     * in the list
     * @return the address at position 0 or null if no addresses present
     */
    public MailingAddress getPrimaryMailingAddress(){
        if(mailingAddressLinkList != null && !mailingAddressLinkList.isEmpty()){
            return mailingAddressLinkList.get(0);
        }
        return null;
        
    }
    
    
    /**
     * Adaptor method for reverse compat when Person's had address fields
     * right on their chest
     * @return 
     */
    public String getAddressStreet(){
        MailingAddress ma = getPrimaryMailingAddress();
        if(ma != null){
            return ma.buildingNo + " " + ma.getStreet();
        }
        return "";
    }
    
    /**
     * Adaptor method for reverse compat when Person's had address fields
     * right on their chest
     * @return 
     */
    public String getAddressCity(){
        MailingAddress ma = getPrimaryMailingAddress();
        if(ma != null){
            return ma.street.getCityStateZip().getCity();
        }
        return "";
    }

    /**
     * Adaptor method for reverse compat when Person's had address fields
     * right on their chest
     * @return 
     */
    public String getAddressState(){
        MailingAddress ma = getPrimaryMailingAddress();
        if(ma != null){
            return ma.street.getCityStateZip().getState();
        }
        return "";
    }
    
    /**
     * Adaptor method for reverse compat when Person's had address fields
     * right on their chest
     * @return 
     */
    public String getAddressZip(){
        MailingAddress ma = getPrimaryMailingAddress();
        if(ma != null){
            return ma.street.getCityStateZip().getZipCode();
        }
        return "";
    }
    
    /**
     * Adaptor method for reverse compat when Person's had address fields
     * right on their chest
     * @return 
     */

    public String getEmail(){
        ContactEmail em = getPrimaryEmail();
        if(em != null){
            return em.emailaddress;
        }
        return "";
    }


    /**
     * Utility method for extracting the first email
     * in the list
     * @return the email at position 0 or null if no email present
     */
    public ContactEmail getPrimaryEmail(){
        if(emailList != null && !emailList.isEmpty()){
            return emailList.get(0);
        }
        return null;
        
    }
    
    /**
     * Utility method for extracting the first phone number
     * in the list
     * @return the phone at position 0 or null if no phone present
     */
    public ContactPhone getPrimaryPhone(){
        if(phoneList != null && !phoneList.isEmpty()){
            return phoneList.get(0);
        }
        return null;
        
    }
    
    
    /**
     * Adaptor method for legacy compatability with Person objects
     * who had first and last names
     * @return empty string!!!!
     */    
    public String getFirstName(){
        return "";
    }
    
    /**
     * Adaptor method for legacy compatability with Person objects
     * who had first and last names
     * @return the underlying Human's full name, i.e. their name
     */
    public String getLastName(){
        return name;
    }

    /**
     * @return the emailList
     */
    public List<ContactEmail> getEmailList() {
        return emailList;
    }

    /**
     * @return the phoneList
     */
    public List<ContactPhone> getPhoneList() {
        return phoneList;
    }

    /**
     * @return the mailingAddressLinkList
     */
    @Override
    public List<MailingAddressLink> getMailingAddressLinkList() {
        return mailingAddressLinkList;
    }

    /**
     * @param emailList the emailList to set
     */
    public void setEmailList(List<ContactEmail> emailList) {
        this.emailList = emailList;
    }

    /**
     * @param phoneList the phoneList to set
     */
    public void setPhoneList(List<ContactPhone> phoneList) {
        this.phoneList = phoneList;
    }

    /**
     * @param mailingAddressLinkList the mailingAddressLinkList to set
     */
    @Override
    public void setMailingAddressLinkList(List<MailingAddressLink> mailingAddressLinkList) {
        this.mailingAddressLinkList = mailingAddressLinkList;
    }

    

    @Override
    public LinkedObjectSchemaEnum getLinkedObjectSchemaEnum() {
        return PERSON_ADDRESS_LOSE;
    }

    @Override
    public int getTargetObjectPK() {
        return humanID;
    }

    /**
     * @return the mailingAddressListPretty
     */
    public String getMailingAddressListPretty() {
        return mailingAddressListPretty;
    }

    /**
     * @return the emailListPretty
     */
    public String getEmailListPretty() {
        return emailListPretty;
    }

    /**
     * @return the phoneListPretty
     */
    public String getPhoneListPretty() {
        return phoneListPretty;
    }

    /**
     * @param mailingAddressListPretty the mailingAddressListPretty to set
     */
    public void setMailingAddressListPretty(String mailingAddressListPretty) {
        this.mailingAddressListPretty = mailingAddressListPretty;
    }

    /**
     * @param emailListPretty the emailListPretty to set
     */
    public void setEmailListPretty(String emailListPretty) {
        this.emailListPretty = emailListPretty;
    }

    /**
     * @param phoneListPretty the phoneListPretty to set
     */
    public void setPhoneListPretty(String phoneListPretty) {
        this.phoneListPretty = phoneListPretty;
    }

    /**
     * @return the aliasList
     */
    public List<HumanAlias> getAliasList() {
        return aliasList;
    }

    /**
     * @param aliasList the aliasList to set
     */
    public void setAliasList(List<HumanAlias> aliasList) {
        this.aliasList = aliasList;
    }

    @Override
    public String getParentHumanFriendlyDescriptiveString() {
        StringBuilder sb = new StringBuilder("Person: ");
        sb.append(this.name);
        sb.append(" | ID: ");
        sb.append(this.getHumanID());
        return sb.toString();
    }

  
}
