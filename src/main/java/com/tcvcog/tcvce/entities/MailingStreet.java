/*
 * Copyright (C) 2021 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.application.interfaces.IFace_updateAuditable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Represents a single street
 * @author Ellen Bascomb of 31Y
 */
public  class   MailingStreet 
        extends TrackedEntity
        implements IFace_noteHolder, 
                   IFace_updateAuditable<MailingStreetFieldsEnum>, 
                   Comparable<Object>,
                   IFace_Mergable{
   
    private int streetID;
    private String name;
    private MailingCityStateZip cityStateZip;
    private String notes;
    private boolean poBox;
    
    final String TABLE_NAME = "mailingstreet";
    final String PK_NAME = "streetid";
    

    /**
     * @return the streetID
     */
    public int getStreetID() {
        return streetID;
    }

    /** Streets name are always uppercase. This should really be enforced on the way 
     * into the DB but it wasn't so here we are.
     * @return the name
     */
    public String getName() {
        if(name != null){
            return name.toUpperCase();
        }
        return name;
    }

    /**
     * @return the cityStateZip
     */
    public MailingCityStateZip getCityStateZip() {
        return cityStateZip;
    }

    /**
     * @return the notes
     */
    @Override
    public String getNotes() {
        return notes;
    }

    /**
     * @return the poBox
     */
    public boolean isPoBox() {
        return poBox;
    }

  

    /**
     * @param streetID the streetID to set
     */
    public void setStreetID(int streetID) {
        this.streetID = streetID;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param cityStateZip the cityStateZip to set
     */
    public void setCityStateZip(MailingCityStateZip cityStateZip) {
        this.cityStateZip = cityStateZip;
    }

    /**
     * @param notes the notes to set
     */
    @Override
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @param poBox the poBox to set
     */
    public void setPoBox(boolean poBox) {
        this.poBox = poBox;
    }

  

    @Override
    public String getPKFieldName() {
        return PK_NAME;
    }

    @Override
    public int getDBKey() {
        return streetID;
    }

    @Override
    public String getDBTableName() {
       return TABLE_NAME;
    }

    @Override
    public String getNoteHolderFriendlyName() {
        return "Mailing Street";
        
    }

    @Override
    public Class<MailingStreetFieldsEnum> getFieldDumpEnum() {
        return MailingStreetFieldsEnum.class;
    }
    
    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof MailingStreet stin){
            if(this.name != null && stin.getName() != null){
                return this.name.compareTo(stin.getName());
            }
        }
        return 0;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.streetID;
        hash = 29 * hash + Objects.hashCode(this.name);
        hash = 29 * hash + Objects.hashCode(this.cityStateZip);
        hash = 29 * hash + Objects.hashCode(this.notes);
        hash = 29 * hash + (this.poBox ? 1 : 0);
        hash = 29 * hash + Objects.hashCode(this.TABLE_NAME);
        hash = 29 * hash + Objects.hashCode(this.PK_NAME);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MailingStreet other = (MailingStreet) obj;
        return this.streetID == other.streetID;
    }

    @Override
    public String getPrimaryDescriptiveString() {
        return getName();
    }

    @Override
    public String getSecondaryDescriptiveString() {
        if(cityStateZip != null){
            String csz = cityStateZip.getDefaultCity() + ", " + cityStateZip.getState() + " " + cityStateZip.getZipCode();
            return csz;
        }
        return "";
    }
    
    
    
    
}
