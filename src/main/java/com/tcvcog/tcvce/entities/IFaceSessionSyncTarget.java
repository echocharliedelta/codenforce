/*
 * Copyright (C) 2025 echocdelta
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Interface for objects which when made the session
 * focus require coordination of other session objects, such as
 * when focusing on a property, the occ period and cases need to 
 * reflect that property's guts.
 * 
 * Implements the visitor pattern with help from the great and terrible ChatGPT
 * 
 * @author echocdelta
 */
public interface IFaceSessionSyncTarget {
    /**
     * Implementing classes need to call visit and pass themselves in.
     * @param visitor 
     */
    public void accept(IFaceSessionSyncVisitor visitor);
}
