/*
 * Copyright (C) 2025 echocdelta
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Arbitrary container for properties associated somehow, such as in a subdivision
 * or strip mall or set of row houses, etc.
 * 
 * @author echocdelta
 */
public class ParcelGroup extends TrackedEntity{
    private static String PARCELGROUP_TABLE = "parcelgroup";
    private static String PARCELGROUP_PKFIELD = "parcelgroupid";
    
    private int parcelGroupID;
    private String groupName;
    private Municipality muni;
    private String notes;
    
    
    
    

    @Override
    public String getPKFieldName() {
        return getPARCELGROUP_PKFIELD();
    }

    @Override
    public int getDBKey() {
        return getParcelGroupID();
    }

    @Override
    public String getDBTableName() {
        return getPARCELGROUP_TABLE();
    }

    /**
     * @return the PARCELGROUP_TABLE
     */
    public static String getPARCELGROUP_TABLE() {
        return PARCELGROUP_TABLE;
    }

    /**
     * @param aPARCELGROUP_TABLE the PARCELGROUP_TABLE to set
     */
    public static void setPARCELGROUP_TABLE(String aPARCELGROUP_TABLE) {
        PARCELGROUP_TABLE = aPARCELGROUP_TABLE;
    }

    /**
     * @return the PARCELGROUP_PKFIELD
     */
    public static String getPARCELGROUP_PKFIELD() {
        return PARCELGROUP_PKFIELD;
    }

    /**
     * @param aPARCELGROUP_PKFIELD the PARCELGROUP_PKFIELD to set
     */
    public static void setPARCELGROUP_PKFIELD(String aPARCELGROUP_PKFIELD) {
        PARCELGROUP_PKFIELD = aPARCELGROUP_PKFIELD;
    }

    /**
     * @return the parcelGroupID
     */
    public int getParcelGroupID() {
        return parcelGroupID;
    }

    /**
     * @param parcelGroupID the parcelGroupID to set
     */
    public void setParcelGroupID(int parcelGroupID) {
        this.parcelGroupID = parcelGroupID;
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the muni
     */
    public Municipality getMuni() {
        return muni;
    }

    /**
     * @param muni the muni to set
     */
    public void setMuni(Municipality muni) {
        this.muni = muni;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }
   
    
}
