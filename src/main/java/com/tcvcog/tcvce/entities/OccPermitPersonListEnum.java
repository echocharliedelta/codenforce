/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.entities;

/**
 * Enumerates the six fields on an occ permit that can hold persons
 * As of FEB 2023, owner/seller was broken into CURRENT_OWNER and PROPERTY_SELLER
 * and Buyer/Tenant was broken into NEW_OWNER and NON_SALE_TENANTS
 * 
 * @author sylvia
 */
public enum OccPermitPersonListEnum {
    CURRENT_OWNER("Property Owner(s)", "default_linkedobjectrole_occpermit_human_left"),
    PROPERTY_SELLER("Property Seller(s)", "default_linkedobjectrole_occpermit_human_left"),
    NEW_OWNER("Property Buyer(s)", "default_linkedobjectrole_occpermit_human_right"),
    NON_SALE_TENANTS("Tenant(s)", "default_linkedobjectrole_occpermit_human_right"),
    MANAGERS("Property manager(s)", "default_linkedobjectrole_occpermit_human_manager"),
    SALE_WITH_TENANTS("Poperty Sale with Tenant(s)", "default_linkedobjectrole_occpermit_human_tenant"),
    CUSTOM_PERSON_LABEL("Custom person link label", null);
    
    private final String label;
    private final String roleDBFixedValueLookupKey;
    
    private OccPermitPersonListEnum(String l, String k){
        label = l;
        roleDBFixedValueLookupKey = k;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @return the roleDBFixedValueLookupKey
     */
    public String getRoleDBFixedValueLookupKey() {
        return roleDBFixedValueLookupKey;
    }
}
