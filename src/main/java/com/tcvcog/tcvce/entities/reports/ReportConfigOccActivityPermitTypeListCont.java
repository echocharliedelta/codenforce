/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities.reports;

import com.tcvcog.tcvce.entities.occupancy.FieldInspectionLight;
import com.tcvcog.tcvce.entities.occupancy.OccPeriodDataHeavy;
import com.tcvcog.tcvce.entities.occupancy.OccPermitType;
import java.util.ArrayList;
import java.util.List;

/**
 * A hacky class that represents a flattened Mapping of
 * permit types to permits for use in displaying counts and permits by type
 * in the permitting report;
 * 
 * @author pierre15
 */
public class ReportConfigOccActivityPermitTypeListCont {
   private OccPermitType permitType;
   private List<OccPeriodDataHeavy> occPeriodHeavyList;
   private List<FieldInspectionLight> finReinspectionList;
   private List<FieldInspectionLight> finInitialList;

    public ReportConfigOccActivityPermitTypeListCont(OccPermitType permitType, List<OccPeriodDataHeavy> permitHeavyList) {
        this.permitType = permitType;
        this.occPeriodHeavyList = permitHeavyList;
        finInitialList = new ArrayList<>();
        finReinspectionList = new ArrayList<>();
               
    }

   
   
    /**
     * @return the permitType
     */
    public OccPermitType getPermitType() {
        return permitType;
    }

    /**
     * @param permitType the permitType to set
     */
    public void setPermitType(OccPermitType permitType) {
        this.permitType = permitType;
    }

    /**
     * @return the occPeriodHeavyList
     */
    public List<OccPeriodDataHeavy> getOccPeriodHeavyList() {
        return occPeriodHeavyList;
    }

    /**
     * @param occPeriodHeavyList the occPeriodHeavyList to set
     */
    public void setOccPeriodHeavyList(List<OccPeriodDataHeavy> occPeriodHeavyList) {
        this.occPeriodHeavyList = occPeriodHeavyList;
    }

    /**
     * @return the finInitialList
     */
    public List<FieldInspectionLight> getFinInitialList() {
        return finInitialList;
    }

    /**
     * @param finInitialList the finInitialList to set
     */
    public void setFinInitialList(List<FieldInspectionLight> finInitialList) {
        this.finInitialList = finInitialList;
    }

    /**
     * @return the finReinspectionList
     */
    public List<FieldInspectionLight> getFinReinspectionList() {
        return finReinspectionList;
    }

    /**
     * @param finReinspectionList the finReinspectionList to set
     */
    public void setFinReinspectionList(List<FieldInspectionLight> finReinspectionList) {
        this.finReinspectionList = finReinspectionList;
    }
   
}
