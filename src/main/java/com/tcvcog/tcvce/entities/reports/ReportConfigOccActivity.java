/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities.reports;

import com.tcvcog.tcvce.entities.CECase;
import com.tcvcog.tcvce.entities.CodeViolationPropCECaseHeavy;
import com.tcvcog.tcvce.entities.occupancy.FieldInspection;
import com.tcvcog.tcvce.entities.occupancy.OccPeriodDataHeavy;
import com.tcvcog.tcvce.entities.occupancy.OccPermitPropUnitHeavy;
import com.tcvcog.tcvce.entities.occupancy.OccPermitType;
import java.util.List;
import java.util.Map;

/**
 * Assembly location for occ permitting report including optionally
 * the inspections that feed into these permits.
 * 
 * @author pierre15
 */
public class ReportConfigOccActivity 
        extends Report {
    
    private List<OccPermitPropUnitHeavy> permitListRaw;
    private List<OccPeriodDataHeavy> periodListRaw;
    private Map<OccPermitType, List<OccPeriodDataHeavy>> permitTypeListMap;
    private List<ReportConfigOccActivityPermitTypeListCont> permitTypeListContainerList;
    
    private List<FieldInspection> finListCE;
    private List<FieldInspection> finListOccFail;
    private List<FieldInspection> finListOccPass;
    private int finListOccPassFailTotal;
    private List<ReportConfigOccActivityFinListCont> finListContainerList;
    
    
    private List<CodeViolationPropCECaseHeavy> migratedViolations;
    private List<CECase> migratedViolationCaseContainers;
    
    // permitting switches
    
    private boolean includePermitNumber;
    private boolean includePermitIssingOfficer;
    private boolean includePermitApplicationAndInspectionDates;
    private boolean includeInspectionCountForPermits;
    
    // Occ application control switches
    private int occApplicationCountFreeEntry;
    private boolean showOccApplicationCountFreeEntry;
    
    // detail section switches
    private boolean detailPermits; 
    private boolean detailFieldInspections;
    
    // Inspection switches
    private boolean activateSectionFieldInspections;
        
    private boolean countOccPeriodInspections;
    private boolean countCECaseInspections;
    
    private boolean showInspectionMetaData;
    private boolean showInspectionFailedItems;
    private boolean showInspectionFailedItemsFindings;
    
    private boolean includeInspectionsOutsideDateRangeForIssuedPermits;

    // violation switches
    private boolean activateTransferredViolationCount;
    private boolean detailMigratedViolations;
    
    /** 
     * Not used in first round OCT 2023
     */
    private boolean showInspectionsBackingPermits; 
    
    /**
     * @return the permitTypeListMap
     */
    public Map<OccPermitType, List<OccPeriodDataHeavy>> getPermitTypeListMap() {
        return permitTypeListMap;
    }

    /**
     * @param permitTypeListMap the permitTypeListMap to set
     */
    public void setPermitTypeListMap(Map<OccPermitType, List<OccPeriodDataHeavy>> permitTypeListMap) {
        this.permitTypeListMap = permitTypeListMap;
    }

    /**
     * @return the migratedViolations
     */
    public List<CodeViolationPropCECaseHeavy> getMigratedViolations() {
        return migratedViolations;
    }

    /**
     * @param migratedViolations the migratedViolations to set
     */
    public void setMigratedViolations(List<CodeViolationPropCECaseHeavy> migratedViolations) {
        this.migratedViolations = migratedViolations;
    }

    /**
     * @return the migratedViolationCaseContainers
     */
    public List<CECase> getMigratedViolationCaseContainers() {
        return migratedViolationCaseContainers;
    }

    /**
     * @param migratedViolationCaseContainers the migratedViolationCaseContainers to set
     */
    public void setMigratedViolationCaseContainers(List<CECase> migratedViolationCaseContainers) {
        this.migratedViolationCaseContainers = migratedViolationCaseContainers;
    }

    /**
     * @return the occApplicationCountFreeEntry
     */
    public int getOccApplicationCountFreeEntry() {
        return occApplicationCountFreeEntry;
    }

    /**
     * @param occApplicationCountFreeEntry the occApplicationCountFreeEntry to set
     */
    public void setOccApplicationCountFreeEntry(int occApplicationCountFreeEntry) {
        this.occApplicationCountFreeEntry = occApplicationCountFreeEntry;
    }

    /**
     * @return the showOccApplicationCountFreeEntry
     */
    public boolean isShowOccApplicationCountFreeEntry() {
        return showOccApplicationCountFreeEntry;
    }

    /**
     * @param showOccApplicationCountFreeEntry the showOccApplicationCountFreeEntry to set
     */
    public void setShowOccApplicationCountFreeEntry(boolean showOccApplicationCountFreeEntry) {
        this.showOccApplicationCountFreeEntry = showOccApplicationCountFreeEntry;
    }

    /**
     * @return the showInspectionsBackingPermits
     */
    public boolean isShowInspectionsBackingPermits() {
        return showInspectionsBackingPermits;
    }

    /**
     * @param showInspectionsBackingPermits the showInspectionsBackingPermits to set
     */
    public void setShowInspectionsBackingPermits(boolean showInspectionsBackingPermits) {
        this.showInspectionsBackingPermits = showInspectionsBackingPermits;
    }

    /**
     * @return the showInspectionMetaData
     */
    public boolean isShowInspectionMetaData() {
        return showInspectionMetaData;
    }

    /**
     * @param showInspectionMetaData the showInspectionMetaData to set
     */
    public void setShowInspectionMetaData(boolean showInspectionMetaData) {
        this.showInspectionMetaData = showInspectionMetaData;
    }

    /**
     * @return the showInspectionFailedItems
     */
    public boolean isShowInspectionFailedItems() {
        return showInspectionFailedItems;
    }

    /**
     * @param showInspectionFailedItems the showInspectionFailedItems to set
     */
    public void setShowInspectionFailedItems(boolean showInspectionFailedItems) {
        this.showInspectionFailedItems = showInspectionFailedItems;
    }

    /**
     * @return the showInspectionFailedItemsFindings
     */
    public boolean isShowInspectionFailedItemsFindings() {
        return showInspectionFailedItemsFindings;
    }

    /**
     * @param showInspectionFailedItemsFindings the showInspectionFailedItemsFindings to set
     */
    public void setShowInspectionFailedItemsFindings(boolean showInspectionFailedItemsFindings) {
        this.showInspectionFailedItemsFindings = showInspectionFailedItemsFindings;
    }

    /**
     * @return the activateSectionFieldInspections
     */
    public boolean isActivateSectionFieldInspections() {
        return activateSectionFieldInspections;
    }

    /**
     * @param activateSectionFieldInspections the activateSectionFieldInspections to set
     */
    public void setActivateSectionFieldInspections(boolean activateSectionFieldInspections) {
        this.activateSectionFieldInspections = activateSectionFieldInspections;
    }

    /**
     * @return the includeInspectionsOutsideDateRangeForIssuedPermits
     */
    public boolean isIncludeInspectionsOutsideDateRangeForIssuedPermits() {
        return includeInspectionsOutsideDateRangeForIssuedPermits;
    }

    /**
     * @param includeInspectionsOutsideDateRangeForIssuedPermits the includeInspectionsOutsideDateRangeForIssuedPermits to set
     */
    public void setIncludeInspectionsOutsideDateRangeForIssuedPermits(boolean includeInspectionsOutsideDateRangeForIssuedPermits) {
        this.includeInspectionsOutsideDateRangeForIssuedPermits = includeInspectionsOutsideDateRangeForIssuedPermits;
    }

    /**
     * @return the includePermitNumber
     */
    public boolean isIncludePermitNumber() {
        return includePermitNumber;
    }

    /**
     * @param includePermitNumber the includePermitNumber to set
     */
    public void setIncludePermitNumber(boolean includePermitNumber) {
        this.includePermitNumber = includePermitNumber;
    }

    /**
     * @return the includePermitIssingOfficer
     */
    public boolean isIncludePermitIssingOfficer() {
        return includePermitIssingOfficer;
    }

    /**
     * @param includePermitIssingOfficer the includePermitIssingOfficer to set
     */
    public void setIncludePermitIssingOfficer(boolean includePermitIssingOfficer) {
        this.includePermitIssingOfficer = includePermitIssingOfficer;
    }

   

    /**
     * @return the permitListRaw
     */
    public List<OccPermitPropUnitHeavy> getPermitListRaw() {
        return permitListRaw;
    }

    /**
     * @param permitListRaw the permitListRaw to set
     */
    public void setPermitListRaw(List<OccPermitPropUnitHeavy> permitListRaw) {
        this.permitListRaw = permitListRaw;
    }

    /**
     * @return the permitTypeListContainerList
     */
    public List<ReportConfigOccActivityPermitTypeListCont> getPermitTypeListContainerList() {
        return permitTypeListContainerList;
    }

    /**
     * @param permitTypeListContainerList the permitTypeListContainerList to set
     */
    public void setPermitTypeListContainerList(List<ReportConfigOccActivityPermitTypeListCont> permitTypeListContainerList) {
        this.permitTypeListContainerList = permitTypeListContainerList;
    }

    /**
     * @return the detailPermits
     */
    public boolean isDetailPermits() {
        return detailPermits;
    }

    /**
     * @param detailPermits the detailPermits to set
     */
    public void setDetailPermits(boolean detailPermits) {
        this.detailPermits = detailPermits;
    }

    /**
     * @return the periodListRaw
     */
    public List<OccPeriodDataHeavy> getPeriodListRaw() {
        return periodListRaw;
    }

    /**
     * @param periodListRaw the periodListRaw to set
     */
    public void setPeriodListRaw(List<OccPeriodDataHeavy> periodListRaw) {
        this.periodListRaw = periodListRaw;
    }

    /**
     * @return the includePermitApplicationAndInspectionDates
     */
    public boolean isIncludePermitApplicationAndInspectionDates() {
        return includePermitApplicationAndInspectionDates;
    }

    /**
     * @param includePermitApplicationAndInspectionDates the includePermitApplicationAndInspectionDates to set
     */
    public void setIncludePermitApplicationAndInspectionDates(boolean includePermitApplicationAndInspectionDates) {
        this.includePermitApplicationAndInspectionDates = includePermitApplicationAndInspectionDates;
    }

    /**
     * @return the detailFieldInspections
     */
    public boolean isDetailFieldInspections() {
        return detailFieldInspections;
    }

    /**
     * @param detailFieldInspections the detailFieldInspections to set
     */
    public void setDetailFieldInspections(boolean detailFieldInspections) {
        this.detailFieldInspections = detailFieldInspections;
    }

    /**
     * @return the countOccPeriodInspections
     */
    public boolean isCountOccPeriodInspections() {
        return countOccPeriodInspections;
    }

    /**
     * @param countOccPeriodInspections the countOccPeriodInspections to set
     */
    public void setCountOccPeriodInspections(boolean countOccPeriodInspections) {
        this.countOccPeriodInspections = countOccPeriodInspections;
    }

    /**
     * @return the countCECaseInspections
     */
    public boolean isCountCECaseInspections() {
        return countCECaseInspections;
    }

    /**
     * @param countCECaseInspections the countCECaseInspections to set
     */
    public void setCountCECaseInspections(boolean countCECaseInspections) {
        this.countCECaseInspections = countCECaseInspections;
    }

    /**
     * @return the finListOccFail
     */
    public List<FieldInspection> getFinListOccFail() {
        return finListOccFail;
    }

    /**
     * @param finListOccFail the finListOccFail to set
     */
    public void setFinListOccFail(List<FieldInspection> finListOccFail) {
        this.finListOccFail = finListOccFail;
    }

    /**
     * @return the finListCE
     */
    public List<FieldInspection> getFinListCE() {
        return finListCE;
    }

    /**
     * @param finListCE the finListCE to set
     */
    public void setFinListCE(List<FieldInspection> finListCE) {
        this.finListCE = finListCE;
    }

    /**
     * @return the finListContainerList
     */
    public List<ReportConfigOccActivityFinListCont> getFinListContainerList() {
        return finListContainerList;
    }

    /**
     * @param finListContainerList the finListContainerList to set
     */
    public void setFinListContainerList(List<ReportConfigOccActivityFinListCont> finListContainerList) {
        this.finListContainerList = finListContainerList;
    }

    /**
     * @return the includeInspectionCountForPermits
     */
    public boolean isIncludeInspectionCountForPermits() {
        return includeInspectionCountForPermits;
    }

    /**
     * @param includeInspectionCountForPermits the includeInspectionCountForPermits to set
     */
    public void setIncludeInspectionCountForPermits(boolean includeInspectionCountForPermits) {
        this.includeInspectionCountForPermits = includeInspectionCountForPermits;
    }

    /**
     * @return the detailMigratedViolations
     */
    public boolean isDetailMigratedViolations() {
        return detailMigratedViolations;
    }

    /**
     * @param detailMigratedViolations the detailMigratedViolations to set
     */
    public void setDetailMigratedViolations(boolean detailMigratedViolations) {
        this.detailMigratedViolations = detailMigratedViolations;
    }

    /**
     * @return the activateTransferredViolationCount
     */
    public boolean isActivateTransferredViolationCount() {
        return activateTransferredViolationCount;
    }

    /**
     * @param activateTransferredViolationCount the activateTransferredViolationCount to set
     */
    public void setActivateTransferredViolationCount(boolean activateTransferredViolationCount) {
        this.activateTransferredViolationCount = activateTransferredViolationCount;
    }

    /**
     * @return the finListOccPass
     */
    public List<FieldInspection> getFinListOccPass() {
        return finListOccPass;
    }

    /**
     * @param finListOccPass the finListOccPass to set
     */
    public void setFinListOccPass(List<FieldInspection> finListOccPass) {
        this.finListOccPass = finListOccPass;
    }

    /**
     * @return the finListOccPassFailTotal
     */
    public int getFinListOccPassFailTotal() {
        return finListOccPassFailTotal;
    }

    /**
     * @param finListOccPassFailTotal the finListOccPassFailTotal to set
     */
    public void setFinListOccPassFailTotal(int finListOccPassFailTotal) {
        this.finListOccPassFailTotal = finListOccPassFailTotal;
    }
    
    
    
    
}
