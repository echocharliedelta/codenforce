/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities.reports;

import com.tcvcog.tcvce.entities.occupancy.FieldInspection;
import java.util.List;

/**
 * Organization container for field inspection report detail
 * @author pierre15
 */
public class ReportConfigOccActivityFinListCont {
    private String title;
    private List<FieldInspection> finList;

    public ReportConfigOccActivityFinListCont(String title, List<FieldInspection> finList) {
        this.title = title;
        this.finList = finList;
    }

    /**
     * @return the finList
     */
    public List<FieldInspection> getFinList() {
        return finList;
    }

    /**
     * @param finList the finList to set
     */
    public void setFinList(List<FieldInspection> finList) {
        this.finList = finList;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
    
}
