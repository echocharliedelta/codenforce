/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * Container for configuring a violation batch operation. Contains 
 * a list of violations, the parent case, various form attributes
 * used in applying those operations to the given case
 * @author pierre15
 */
public class CodeViolationBatchOperationConfiguration {
    
    private CodeViolationBatchOperationEnum operation;
    private List<CodeViolation> selectedCodeViolations;
    private final CECaseDataHeavy ceCase;
    private final UserAuthorized requestingUser;
    private String operationReason;
    private LocalDateTime dateOfRecordForOperation;
    
    // specific to extend stip comp
    private LocalDateTime updatedStipulatedComplianceDate;
    private int complianceExtensionDays;
    private boolean formExtendStipCompUsingDate;
    
    // logging
    private StringBuilder operationLog;
    private int successCount = 0;
    private int errorCount = 0;
    private int processCount = 0;
    
    
    
    public CodeViolationBatchOperationConfiguration(CECaseDataHeavy cse, UserAuthorized ua){
        ceCase = cse;
        requestingUser = ua;
        
    }
    
    /**
     * Checks for empty list
     * @return 
     */
    public boolean isAllowBatchOperationToCommit(){
        return selectedCodeViolations != null && !selectedCodeViolations.isEmpty();
    }
    
    /**
     * Adds 1 to success count
     */
    public void incrementSuccessCount(){
        successCount++;
    }
    
    /**
     * adds 1 to error count
     */
    public void incrementErrorCount(){
        errorCount++;
    }
    
    /**
     * adds one to overall process count
     */
    public void incrementProcessCount(){
        processCount++;
    }
    
    /**
     * Iterates over the list of violations in my belly and assembles 
     * the earliest compliance date
     * @return 
     */
    public LocalDateTime getEarliestStipulatedComplianceDate(){
        return selectedCodeViolations == null ? null : selectedCodeViolations.stream()
                            .map(CodeViolation::getStipulatedComplianceDate) // Extract the compliance dates
                            .filter(Objects::nonNull) // Remove null values
                            .min(LocalDateTime::compareTo) // Find the earliest date
                            .orElse(null); // Return null if no dates exist

    }
    
    
    /**
     * Iterates over the list of violations in my belly and assembles 
     * the latest date of record
     * @return 
     */
    public LocalDateTime getLatestViolationDate(){
        return selectedCodeViolations == null ? null : selectedCodeViolations.stream()
                            .map(CodeViolation::getDateOfRecord) // Extract the compliance dates
                            .filter(Objects::nonNull) // Remove null values
                            .max(LocalDateTime::compareTo) // Find the earliest date
                            .orElse(null); // Return null if no dates exist

    }
    

    /**
     * @return the operation
     */
    public CodeViolationBatchOperationEnum getOperation() {
        return operation;
    }

    /**
     * @param operation the operation to set
     */
    public void setOperation(CodeViolationBatchOperationEnum operation) {
        this.operation = operation;
    }

    /**
     * @return the selectedCodeViolations
     */
    public List<CodeViolation> getSelectedCodeViolations() {
        return selectedCodeViolations;
    }

    /**
     * @param selectedCodeViolations the selectedCodeViolations to set
     */
    public void setSelectedCodeViolations(List<CodeViolation> selectedCodeViolations) {
        this.selectedCodeViolations = selectedCodeViolations;
    }

    /**
     * @return the ceCase
     */
    public CECaseDataHeavy getCeCase() {
        return ceCase;
    }

    

    /**
     * @return the requestingUser
     */
    public UserAuthorized getRequestingUser() {
        return requestingUser;
    }

   

    /**
     * @return the operationReason
     */
    public String getOperationReason() {
        return operationReason;
    }

    /**
     * @param operationReason the operationReason to set
     */
    public void setOperationReason(String operationReason) {
        this.operationReason = operationReason;
    }

    /**
     * @return the dateOfRecordForOperation
     */
    public LocalDateTime getDateOfRecordForOperation() {
        return dateOfRecordForOperation;
    }

    /**
     * @param dateOfRecordForOperation the dateOfRecordForOperation to set
     */
    public void setDateOfRecordForOperation(LocalDateTime dateOfRecordForOperation) {
        this.dateOfRecordForOperation = dateOfRecordForOperation;
    }

    /**
     * @return the updatedStipulatedComplianceDate
     */
    public LocalDateTime getUpdatedStipulatedComplianceDate() {
        return updatedStipulatedComplianceDate;
    }

    /**
     * @param updatedStipulatedComplianceDate the updatedStipulatedComplianceDate to set
     */
    public void setUpdatedStipulatedComplianceDate(LocalDateTime updatedStipulatedComplianceDate) {
        this.updatedStipulatedComplianceDate = updatedStipulatedComplianceDate;
    }

    /**
     * @return the complianceExtensionDays
     */
    public int getComplianceExtensionDays() {
        return complianceExtensionDays;
    }

    /**
     * @param complianceExtensionDays the complianceExtensionDays to set
     */
    public void setComplianceExtensionDays(int complianceExtensionDays) {
        this.complianceExtensionDays = complianceExtensionDays;
    }

    /**
     * @return the formExtendStipCompUsingDate
     */
    public boolean isFormExtendStipCompUsingDate() {
        return formExtendStipCompUsingDate;
    }

    /**
     * @param formExtendStipCompUsingDate the formExtendStipCompUsingDate to set
     */
    public void setFormExtendStipCompUsingDate(boolean formExtendStipCompUsingDate) {
        this.formExtendStipCompUsingDate = formExtendStipCompUsingDate;
    }

    public String getLog(){
        if(operationLog != null) {
            return operationLog.toString();
        } else {
            return "No log";
            
        }
    }
    
    /**
     * @return the operationLog
     */
    public StringBuilder getOperationLog() {
        return operationLog;
    }

    /**
     * @param operationLog the operationLog to set
     */
    public void setOperationLog(StringBuilder operationLog) {
        this.operationLog = operationLog;
    }

    /**
     * @return the successCount
     */
    public int getSuccessCount() {
        return successCount;
    }

    /**
     * @return the errorCount
     */
    public int getErrorCount() {
        return errorCount;
    }

    /**
     * @return the processCount
     */
    public int getProcessCount() {
        return processCount;
    }
    
    
}
