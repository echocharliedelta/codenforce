/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Represents the levels of organization for a given ordinance
 * @author pierre15
 */
public enum OrdinanceHierarchyEnum {
    ROOT(0, "Root"),
    CODE_SOURCE(1, "Code Source"),
    CHAPTER(2, "Chapter"),
    SECTION(3, "Section"),
    SUB_SECTION(4, "Sub Section"),
    SUB_SUB_SECTION(5, "Sub Sub Section"),
    ORDINANCE(6, "Ordinance");
    
    
    protected final int generation;
    protected final String title;
    
    private OrdinanceHierarchyEnum(int g, String t){
        generation = g;
        title = t;
    }

    /**
     * @return the generation
     */
    public int getGeneration() {
        return generation;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }
    
}
