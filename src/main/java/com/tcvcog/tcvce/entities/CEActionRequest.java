 /*
 * Copyright (C) 2017 ellen bascomb of apt 31y
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.application.IFace_humanHeadlessEmitter;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import com.tcvcog.tcvce.util.DateTimeUtil;
import com.tcvcog.tcvce.application.interfaces.IFaceActivatableBOB;

/**
 * Models the entity: code enforcement action request.This object blends data from several database tables to create
 a conceptual model of a single request.
 
 The JSF page for submitting a request act on an CEActionRequest object
 and will edit the values of member variables
 on this object and can then ask for the request to be read into the DB.
 
 A reference to an CEActionRequest object will be attached to the 
 Visit object such that both public and logged in users will have access
 to an CEActionRequest to do with as they please (print, etc.)
 * @author Eric Darsow
 */

public class        CEActionRequest 
        extends     TrackedEntity 
        implements  Serializable,
                    IFace_BlobHolder,
                    IFace_humanListHolder,
                    IFaceActivatableBOB{
    
    private final static LinkedObjectSchemaEnum HUMANLINK_UPSTREAM_POOL = null;
    
    private final static BlobLinkEnum BLOB_LINK = BlobLinkEnum.CEACTION_REQUEST;
    private final static BlobLinkEnum BLOP_UPSPTREAM_POOL = BlobLinkEnum.PROPERTY;
    private final static HumanHeadlessEnum HEADLESS_DOMAIN = HumanHeadlessEnum.CEACTION_REQUEST;
    
    private final static String CEAR_TABLE = "ceactionrequest";
    private final static String CEAR_PK = "requestid";
    
    
    // requests no longer have a status--remove when fully updated
    // for the full case model
    //private RequestStatus requestStatus;
    
    private int requestID;
    private int requestPublicCC;
    private boolean paccEnabled;
    
    private int parcelKey;
    private CEActionRequestIssueType issue;
    private String requestDescription;
    
    private CEActionRequestStatus requestStatus;
    private boolean notAtKnownAddress;
    private String nonaddressableDescription;
    
    private Municipality muni;
    
    private String requestorName;
    private String requestorPhone;
    
    private ContactPhoneType requestorPhoneType;
    private String requestorEmail;
    private PersonType requestorPersonType;
    
    private boolean anonymitiyRequested;
    
    /**
     * Ellen decided to keep this switch as true if the cear comes from an internal user
     */
    private boolean internalUserSubmitter;
    
    private int caseID;
    private LocalDateTime caseAttachmentTimeStamp;
    private User caseAttachmentUser;
    
    private java.time.LocalDateTime dateOfRecord;
    
    private long daysSinceSubmission;
    private boolean urgent;
    
    private List<BlobLight> blobList; 
    
    private String muniNotes;
    private String publicExternalNotes;
    
    private List<HumanLink> humanLinkList;
    
    /**
     * A VERY hacky way to deal with print formatting in Chrome
     */
    private boolean insertPageBreakBefore = true;
    
    // these are populated on the lookup when the linked
    // tables with the String values are selected
    
    
    
    /**
     * Creates a new instance of ActionRequest
     * @param cear
     */
    public CEActionRequest(CEActionRequest cear) {
        if(cear != null){
            this.requestID = cear.requestID;
            this.requestPublicCC = cear.requestPublicCC;
            this.paccEnabled = cear.paccEnabled;
            
            this.parcelKey = cear.parcelKey;
            this.issue = cear.issue;
            this.requestDescription = cear.requestDescription;
            
            this.requestStatus = cear.requestStatus;
            this.notAtKnownAddress = cear.notAtKnownAddress;
            this.nonaddressableDescription = cear.nonaddressableDescription;
            
            this.muni = cear.muni;
            this.requestorName = cear.requestorName;
            this.requestorPhone = cear.requestorPhone;
            
            this.requestorPhoneType = cear.requestorPhoneType;
            this.requestorEmail = cear.requestorEmail;
            
            this.requestorPersonType = cear.requestorPersonType;
            this.internalUserSubmitter = cear.internalUserSubmitter;
            this.anonymitiyRequested = cear.anonymitiyRequested;
            this.humanLinkList = cear.humanLinkList;
            
            this.caseID = cear.caseID;
            this.caseAttachmentUser = cear.caseAttachmentUser;
            this.daysSinceSubmission = cear.daysSinceSubmission;
            this.urgent = cear.urgent;
            
            this.muniNotes = cear.muniNotes;
            this.publicExternalNotes = cear.publicExternalNotes;

            this.caseAttachmentTimeStamp = cear.caseAttachmentTimeStamp;
            this.dateOfRecord = cear.dateOfRecord;
            this.blobList = cear.blobList;
            
            this.createdTS = cear.createdTS;
            this.createdBy = cear.createdBy;
            this.createdByUserID = cear.createdByUserID;
            
            this.lastUpdatedTS = cear.lastUpdatedTS;
            this.lastUpdatedByUserID = cear.lastUpdatedByUserID;
            this.lastUpdatedBy = cear.lastUpdatedBy;
            
            this.deactivatedTS = cear.deactivatedTS;
            this.deactivatedBy = cear.deactivatedBy;
            this.deactivatedByUserID = cear.deactivatedByUserID;
        }

    }
    
    /**
     * Creates a new instance of ActionRequest
     */
    public CEActionRequest() {
    }

    /**
     * @return the requestID
     */
    public int getRequestID() {
        return requestID;
    }

    /**
     * @param requestID the requestID to set
     */
    public void setRequestID(int requestID) {
        this.requestID = requestID;
    }

    /**
     * @return the blobList
     */
    @Override
    public List<BlobLight> getBlobList() {
        return blobList;
    }

    /**
     * @param blobList the blobList to set
     */
    @Override
    public void setBlobList(List<BlobLight> blobList) {
        this.blobList = blobList;
    }

    /**
     * @return the nonaddressableDescription
     */
    public String getNonaddressableDescription() {
        return nonaddressableDescription;
    }

    /**
     * @param nonaddressableDescription the nonaddressableDescription to set
     */
    public void setNonaddressableDescription(String nonaddressableDescription) {
        this.nonaddressableDescription = nonaddressableDescription;
    }

    /**
     * @return the notAtKnownAddress
     */
    public boolean isNotAtKnownAddress() {
        return notAtKnownAddress;
    }

    /**
     * @param notAtKnownAddress the notAtKnownAddress to set
     */
    public void setNotAtKnownAddress(boolean notAtKnownAddress) {
        this.notAtKnownAddress = notAtKnownAddress;
    }

    /**
     * @return the requestDescription
     */
    public String getRequestDescription() {
        return requestDescription;
    }

    /**
     * @param requestDescription the requestDescription to set
     */
    public void setRequestDescription(String requestDescription) {
        this.requestDescription = requestDescription;
    }

    /**
     * @return the urgent
     */
    public boolean isUrgent() {
        return urgent;
    }

    /**
     * @param urgent the urgent to set
     */
    public void setUrgent(boolean urgent) {
        this.urgent = urgent;
    }

    /**
     * @return the dateOfRecord
     */
    public LocalDateTime getDateOfRecord() {
        return dateOfRecord;
    }

    /**
     * @param dateOfRecord the dateOfRecord to set
     */
    public void setDateOfRecord(LocalDateTime dateOfRecord) {
        this.dateOfRecord = dateOfRecord;
    }

  

    /**
     * @return the muniNotes
     */
    public String getMuniNotes() {
        return muniNotes;
    }

    /**
     * @param muniNotes the muniNotes to set
     */
    public void setMuniNotes(String muniNotes) {
        this.muniNotes = muniNotes;
    }

    /**
     * @return the publicExternalNotes
     */
    public String getPublicExternalNotes() {
        return publicExternalNotes;
    }

    /**
     * @param publicExternalNotes the publicExternalNotes to set
     */
    public void setPublicExternalNotes(String publicExternalNotes) {
        this.publicExternalNotes = publicExternalNotes;
    }

  
    /**
     * @return the requestPublicCC
     */
    public int getRequestPublicCC() {
        return requestPublicCC;
    }

    /**
     * @param requestPublicCC the requestPublicCC to set
     */
    public void setRequestPublicCC(int requestPublicCC) {
        this.requestPublicCC = requestPublicCC;
    }

    




    /**
     * @return the anonymitiyRequested
     */
    public boolean isAnonymitiyRequested() {
        return anonymitiyRequested;
    }

    /**
     * @param anonymitiyRequested the anonymitiyRequested to set
     */
    public void setAnonymitiyRequested(boolean anonymitiyRequested) {
        this.anonymitiyRequested = anonymitiyRequested;
    }

    /**
     * @return the muni
     */
    public Municipality getMuni() {
        return muni;
    }

    /**
     * @param muni the muni to set
     */
    public void setMuni(Municipality muni) {
        this.muni = muni;
    }

    /**
     * @return the caseID
     */
    public int getCaseID() {
        return caseID;
    }

    /**
     * @param caseID the caseID to set
     */
    public void event(int caseID) {
        this.caseID = caseID;
    }

    /**
     * @return the daysSinceSubmission
     */
    public long getDaysSinceSubmission() {
        return daysSinceSubmission;
    }

    /**
     * @param daysSinceSubmission the daysSinceSubmission to set
     */
    public void setDaysSinceSubmission(long daysSinceSubmission) {
        this.daysSinceSubmission = daysSinceSubmission;
    }

    /**
     * @return the requestStatus
     */
    public CEActionRequestStatus getRequestStatus() {
        return requestStatus;
    }

    /**
     * @param requestStatus the requestStatus to set
     */
    public void setRequestStatus(CEActionRequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }
    
    public void setCaseID(int i){
        this.caseID = i;
    }

  
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + this.requestID;
        hash = 73 * hash + this.requestPublicCC;
        hash = 73 * hash + Objects.hashCode(this.requestStatus);
        hash = 73 * hash + Objects.hashCode(this.muni);
        hash = 73 * hash + this.caseID;
        hash = 73 * hash + Objects.hashCode(this.dateOfRecord);
        hash = 73 * hash + (int) (this.daysSinceSubmission ^ (this.daysSinceSubmission >>> 32));
        hash = 73 * hash + (this.notAtKnownAddress ? 1 : 0);
        hash = 73 * hash + Objects.hashCode(this.nonaddressableDescription);
        hash = 73 * hash + Objects.hashCode(this.requestDescription);
        hash = 73 * hash + (this.urgent ? 1 : 0);
        hash = 73 * hash + (this.anonymitiyRequested ? 1 : 0);
        hash = 73 * hash + Objects.hashCode(this.muniNotes);
        hash = 73 * hash + Objects.hashCode(this.publicExternalNotes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CEActionRequest other = (CEActionRequest) obj;
        if (this.requestID != other.requestID) {
            return false;
        }
        if (this.requestPublicCC != other.requestPublicCC) {
            return false;
        }
        if (this.caseID != other.caseID) {
            return false;
        }
        if (this.daysSinceSubmission != other.daysSinceSubmission) {
            return false;
        }
        if (this.notAtKnownAddress != other.notAtKnownAddress) {
            return false;
        }
        if (this.urgent != other.urgent) {
            return false;
        }
        if (this.anonymitiyRequested != other.anonymitiyRequested) {
            return false;
        }
        if (!Objects.equals(this.nonaddressableDescription, other.nonaddressableDescription)) {
            return false;
        }
        if (!Objects.equals(this.requestDescription, other.requestDescription)) {
            return false;
        }
        if (!Objects.equals(this.muniNotes, other.muniNotes)) {
            return false;
        }
        if (!Objects.equals(this.publicExternalNotes, other.publicExternalNotes)) {
            return false;
        }
        if (!Objects.equals(this.requestStatus, other.requestStatus)) {
            return false;
        }
        if (!Objects.equals(this.muni, other.muni)) {
            return false;
        }
        if (!Objects.equals(this.dateOfRecord, other.dateOfRecord)) {
            return false;
        }
        return true;
    }

    /**
     * @return the paccEnabled
     */
    public boolean isPaccEnabled() {
        return paccEnabled;
    }

    /**
     * @param paccEnabled the paccEnabled to set
     */
    public void setPaccEnabled(boolean paccEnabled) {
        this.paccEnabled = paccEnabled;
    }

    /**
     * @return the caseAttachmentUser
     */
    public User getCaseAttachmentUser() {
        return caseAttachmentUser;
    }

    /**
     * @param caseAttachmentUser the caseAttachmentUser to set
     */
    public void setCaseAttachmentUser(User caseAttachmentUser) {
        this.caseAttachmentUser = caseAttachmentUser;
    }

    /**
     * @return the caseAttachmentTimeStamp
     */
    public LocalDateTime getCaseAttachmentTimeStamp() {
        return caseAttachmentTimeStamp;
    }

    /**
     * @param caseAttachmentTimeStamp the caseAttachmentTimeStamp to set
     */
    public void setCaseAttachmentTimeStamp(LocalDateTime caseAttachmentTimeStamp) {
        this.caseAttachmentTimeStamp = caseAttachmentTimeStamp;
    }

    /**
     * @return the insertPageBreakBefore
     */
    public boolean isInsertPageBreakBefore() {
        return insertPageBreakBefore;
    }

    /**
     * @param insertPageBreakBefore the insertPageBreakBefore to set
     */
    public void setInsertPageBreakBefore(boolean insertPageBreakBefore) {
        this.insertPageBreakBefore = insertPageBreakBefore;
    }

    /**
     * @return the issue
     */
    public CEActionRequestIssueType getIssue() {
        return issue;
    }

    /**
     * @param issue the issue to set
     */
    public void setIssue(CEActionRequestIssueType issue) {
        this.issue = issue;
    }


    @Override
    public BlobLinkEnum getBlobLinkEnum() {
        return BLOB_LINK;
    }

    @Override
    public int getParentObjectID() {
        return requestID;
    }

    @Override
    public BlobLinkEnum getBlobUpstreamPoolEnum() {
        return BLOP_UPSPTREAM_POOL;
    }

    @Override
    public int getBlobUpstreamPoolEnumPoolFeederID() {
        return parcelKey;
    }

   

    /**
     * @return the requestorName
     */
    public String getRequestorName() {
        return requestorName;
    }

    /**
     * @param requestorName the requestorName to set
     */
    public void setRequestorName(String requestorName) {
        this.requestorName = requestorName;
    }

    /**
     * @return the requestorPhone
     */
    public String getRequestorPhone() {
        return requestorPhone;
    }

    /**
     * @return the requestorPhoneType
     */
    public ContactPhoneType getRequestorPhoneType() {
        return requestorPhoneType;
    }

    /**
     * @return the requestorEmail
     */
    public String getRequestorEmail() {
        return requestorEmail;
    }

    /**
     * @param requestorPhone the requestorPhone to set
     */
    public void setRequestorPhone(String requestorPhone) {
        this.requestorPhone = requestorPhone;
    }

    /**
     * @param requestorPhoneType the requestorPhoneType to set
     */
    public void setRequestorPhoneType(ContactPhoneType requestorPhoneType) {
        this.requestorPhoneType = requestorPhoneType;
    }

    /**
     * @param requestorEmail the requestorEmail to set
     */
    public void setRequestorEmail(String requestorEmail) {
        this.requestorEmail = requestorEmail;
    }

    @Override
    public String getPKFieldName() {
        return CEAR_PK;
    }

    @Override
    public int getDBKey() {
        return requestID;
    }

    @Override
    public String getDBTableName() {
        return CEAR_TABLE;
    }

    /**
     * @return the parcelKey
     */
    public int getParcelKey() {
        return parcelKey;
    }

    /**
     * @param parcelKey the parcelKey to set
     */
    public void setParcelKey(int parcelKey) {
        this.parcelKey = parcelKey;
    }

    /**
     * @return the requestorPersonType
     */
    public PersonType getRequestorPersonType() {
        return requestorPersonType;
    }

    /**
     * @param requestorPersonType the requestorPersonType to set
     */
    public void setRequestorPersonType(PersonType requestorPersonType) {
        this.requestorPersonType = requestorPersonType;
    }

    /**
     * @return the internalUserSubmitter
     */
    public boolean isInternalUserSubmitter() {
        return internalUserSubmitter;
    }

    /**
     * @param internalUserSubmitter the internalUserSubmitter to set
     */
    public void setInternalUserSubmitter(boolean internalUserSubmitter) {
        this.internalUserSubmitter = internalUserSubmitter;
    }

    @Override
    public List<HumanLink> gethumanLinkList() {
        return humanLinkList;
    }

    @Override
    public void sethumanLinkList(List<HumanLink> hll) {
        humanLinkList = hll;
    }

    @Override
    public LinkedObjectSchemaEnum getHUMAN_LINK_SCHEMA_ENUM() {
        return LinkedObjectSchemaEnum.CEACtionRequestHuman;
    }

    @Override
    public int getHostPK() {
        return requestID;
    }

    @Override
    public String getDescriptionString() {
        StringBuilder sb = new StringBuilder();
        if(this.issue != null){
            sb.append(this.issue.getName());
        }
        if(this.requestStatus != null){
            sb.append(" | Status: ");
            sb.append(this.requestStatus.statusTitle);
        }
        return sb.toString();
    }

    @Override
    public LinkedObjectSchemaEnum getUpstreamHumanLinkPoolEnum() {
        return HUMANLINK_UPSTREAM_POOL;
    }

    @Override
    public int getUpstreamHumanLinkPoolFeederID() {
        return 0;
    }
    
    
}