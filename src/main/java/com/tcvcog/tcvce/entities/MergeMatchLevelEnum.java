/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Enumerates a strickness scale which can be used as both a merge strictness
 * floor or a determination of mathability. 
 * 
 * Used by SystemCoordinator and called merge determination methods.
 * 
 * @author pierre15
 */
public enum MergeMatchLevelEnum {
    PERFECT_EQID("Perfect match","Require equal IDs", 3),
    STRICT("Strict match","Require all fields matching", 2),
    MODERATE("Moderate match","Effectively the same", 1),
    NO_MATCH("No match", "Not qualifying as a match", 0);
    
    private final int rank;
    private final String label;
    private final String description;

    private MergeMatchLevelEnum(String label, String description, int r) {
        this.label = label;
        this.description = description;
        this.rank = r;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return the rank
     */
    public int getRank() {
        return rank;
    }
    
    
}
