/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Contains an alert event and perhaps its stop event which 
 * mark a property as having a special condition
 * 
 * @author pierre15
 */
public class PropertyAlert {
   
    private EventCnF alertEvent;
    private EventCnF alertStopEvent;

    public PropertyAlert(EventCnF ae) {
        this.alertEvent = ae;
    }

    
    
    public boolean isAlertActive(){
        return alertEvent != null && alertStopEvent == null;
    }
    
    // GENERIC GETTERS AND SETTERS
    
    /**
     * @return the alertStopEvent
     */
    public EventCnF getAlertStopEvent() {
        return alertStopEvent;
    }

    /**
     * @param alertStopEvent the alertStopEvent to set
     */
    public void setAlertStopEvent(EventCnF alertStopEvent) {
        this.alertStopEvent = alertStopEvent;
    }

    /**
     * @return the alertEvent
     */
    public EventCnF getAlertEvent() {
        return alertEvent;
    }

    /**
     * @param alertEvent the alertEvent to set
     */
    public void setAlertEvent(EventCnF alertEvent) {
        this.alertEvent = alertEvent;
    }
    
}
