/*
 * Copyright (C) 2017 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;
 
/**
 * Data Heavy subclass of CodeViolation for allowing violations to roam
 * free, untethered from the metal constrains of their host CECase
 * 
 * @author ellen bascomb of apt 31y
 */
public  class       CodeViolationPropCECaseHeavy  
        extends     CodeViolation {
    
   private String muniName;
   private int muniCode;
    
   private String propertyAddress;
   private int propertyID;
   
   private String ceCaseName;
       
   
   public CodeViolationPropCECaseHeavy(CodeViolation cv){
       super(cv);
       
       
   }

    /**
     * @return the muniName
     */
    public String getMuniName() {
        return muniName;
    }

    /**
     * @return the muniCode
     */
    public int getMuniCode() {
        return muniCode;
    }

    /**
     * @return the propertyAddress
     */
    public String getPropertyAddress() {
        return propertyAddress;
    }

    /**
     * @return the propertyID
     */
    public int getPropertyID() {
        return propertyID;
    }

    /**
     * @return the ceCaseName
     */
    public String getCeCaseName() {
        return ceCaseName;
    }

    /**
     * @param muniName the muniName to set
     */
    public void setMuniName(String muniName) {
        this.muniName = muniName;
    }

    /**
     * @param muniCode the muniCode to set
     */
    public void setMuniCode(int muniCode) {
        this.muniCode = muniCode;
    }

    /**
     * @param propertyAddress the propertyAddress to set
     */
    public void setPropertyAddress(String propertyAddress) {
        this.propertyAddress = propertyAddress;
    }

    /**
     * @param propertyID the propertyID to set
     */
    public void setPropertyID(int propertyID) {
        this.propertyID = propertyID;
    }

    /**
     * @param ceCaseName the ceCaseName to set
     */
    public void setCeCaseName(String ceCaseName) {
        this.ceCaseName = ceCaseName;
    }

    

   

}
