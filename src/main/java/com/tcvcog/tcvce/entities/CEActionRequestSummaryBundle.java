/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import java.util.List;
import java.util.Map;

/**
 * DTO for report section on CEARs. Contains totals and row-level objects
 * 
 * @author pierre15
 */
public class CEActionRequestSummaryBundle {
    
   private List<CEActionRequestIssueRow> issueRowList;
   
   private List<CEActionRequest> requestsInPeriod;
   private Map<CEActionRequestStatus, Long> totalByOutcome;
   private Long outcomeTabulatedTotal;
   private List<CEActionRequest> unprocessedCEARSBeforeReporting;

    /**
     * @return the issueRowList
     */
    public List<CEActionRequestIssueRow> getIssueRowList() {
        return issueRowList;
    }

    /**
     * @param issueRowList the issueRowList to set
     */
    public void setIssueRowList(List<CEActionRequestIssueRow> issueRowList) {
        this.issueRowList = issueRowList;
    }

   

    /**
     * @return the totalByOutcome
     */
    public Map<CEActionRequestStatus, Long> getTotalByOutcome() {
        return totalByOutcome;
    }

    /**
     * @param totalByOutcome the totalByOutcome to set
     */
    public void setTotalByOutcome(Map<CEActionRequestStatus, Long> totalByOutcome) {
        this.totalByOutcome = totalByOutcome;
    }

    /**
     * @return the unprocessedCEARSBeforeReporting
     */
    public List<CEActionRequest> getUnprocessedCEARSBeforeReporting() {
        return unprocessedCEARSBeforeReporting;
    }

    /**
     * @param unprocessedCEARSBeforeReporting the unprocessedCEARSBeforeReporting to set
     */
    public void setUnprocessedCEARSBeforeReporting(List<CEActionRequest> unprocessedCEARSBeforeReporting) {
        this.unprocessedCEARSBeforeReporting = unprocessedCEARSBeforeReporting;
    }

    /**
     * @return the requestsInPeriod
     */
    public List<CEActionRequest> getRequestsInPeriod() {
        return requestsInPeriod;
    }

    /**
     * @param requestsInPeriod the requestsInPeriod to set
     */
    public void setRequestsInPeriod(List<CEActionRequest> requestsInPeriod) {
        this.requestsInPeriod = requestsInPeriod;
    }

    /**
     * @return the outcomeTabulatedTotal
     */
    public Long getOutcomeTabulatedTotal() {
        return outcomeTabulatedTotal;
    }

    /**
     * @param outcomeTabulatedTotal the outcomeTabulatedTotal to set
     */
    public void setOutcomeTabulatedTotal(Long outcomeTabulatedTotal) {
        this.outcomeTabulatedTotal = outcomeTabulatedTotal;
    }
    
}
