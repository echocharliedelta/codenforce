/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Represents the step sequence for occ permit flow
 * @author pierre15
 */
public enum FieldFinRoutingStepEnum {
    PROPERTY (1, "Property"),
    PATHWAY (2, "Pathway"),
    PERIODS_CASES_EXISTING (3, "Review existing cases or files"),
    PERIOD_CASE_NEW (4, "Create new case or file");
    
    
    private final int stepNumber1Based;
    private final String stepTitle;
    
    private FieldFinRoutingStepEnum(int no, String t){
        stepNumber1Based = no;
        stepTitle = t;
    }

    /**
     * @return the stepNumber1Based
     */
    public int getStepNumber1Based() {
        return stepNumber1Based;
    }

    /**
     * @return the stepTitle
     */
    public String getStepTitle() {
        return stepTitle;
    }
    
    
}
