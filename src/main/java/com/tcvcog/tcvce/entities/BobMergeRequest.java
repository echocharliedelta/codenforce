/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.util.Constants;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Bundle of fields for requesting and reviewing merge results for a set of MailingAddress
 * objects.
 * 
 * @author Ellem Bascomb of apartmen 31Y
 * @param <BOBToMerge> Business object to be merged
 */
public class BobMergeRequest<BOBToMerge extends IFace_Mergable> {
    
    private List<BOBToMerge> bobsToMerge;
    private StringBuilder mergeLog;
    
    private LocalDateTime mergeReviewTS;
    private boolean mergeAllowed;
    
    private LocalDateTime mergeCompleteTS;
    
    /**
     * The object into which we merge the list of bobs to emrge
     */
    private BOBToMerge mergeTarget;
    private UserAuthorized requestingUser;

    public BobMergeRequest(){
        mergeLog = new StringBuilder();
    }
    
    
    /**
     * Logic block to make sure we have a target object and at least 1 in our bob list
     * @return 
     */
    public boolean isAllowMergeCommence(){
        return mergeTarget != null && bobsToMerge != null && !bobsToMerge.isEmpty();
    }
    
    
    /**
     * tacks on the given string to an internal merge log
     * @param s
     * @param appendBreak 
     */
    public void appendToMergeLog(Object s, boolean appendBreak){
        if(mergeLog != null && s != null){
            System.out.println("Merge log entry: " + String.valueOf(s));
            mergeLog.append(String.valueOf(s));
            if(appendBreak){
                mergeLog.append(Constants.FMT_HTML_BREAK);
            }
        }
    }
    
    
    
    /**
     * @return the bobsToMerge
     */
    public List<BOBToMerge> getBobsToMerge() {
        return bobsToMerge;
    }

    /**
     * @param bobsToMerge the bobsToMerge to set
     */
    public void setBobsToMerge(List<BOBToMerge> bobsToMerge) {
        this.bobsToMerge = bobsToMerge;
    }

    /**
     * @return the mergeLog
     */
    public String getMergeLog() {
        if(mergeLog != null){
            return mergeLog.toString();
        }
        return "[empty merge log]";
    }

    /**
     * @param mergeLog the mergeLog to set
     */
    public void setMergeLog(StringBuilder mergeLog) {
        this.mergeLog = mergeLog;
    }

    /**
     * @return the mergeTarget
     */
    public BOBToMerge getMergeTarget() {
        return mergeTarget;
    }

    /**
     * @param mergeTarget the mergeTarget to set
     */
    public void setMergeTarget(BOBToMerge mergeTarget) {
        this.mergeTarget = mergeTarget;
    }

    /**
     * @return the requestingUser
     */
    public UserAuthorized getRequestingUser() {
        return requestingUser;
    }

    /**
     * @param requestingUser the requestingUser to set
     */
    public void setRequestingUser(UserAuthorized requestingUser) {
        this.requestingUser = requestingUser;
    }

    /**
     * @return the mergeAllowed
     */
    public boolean isMergeAllowed() {
        return mergeAllowed;
    }

    /**
     * @param mergeAllowed the mergeAllowed to set
     */
    public void setMergeAllowed(boolean mergeAllowed) {
        this.mergeAllowed = mergeAllowed;
    }

   
    /**
    

    /**
     * @return the mergeReviewTS
     */
    public LocalDateTime getMergeReviewTS() {
        return mergeReviewTS;
    }

    /**
     * @param mergeReviewTS the mergeReviewTS to set
     */
    public void setMergeReviewTS(LocalDateTime mergeReviewTS) {
        this.mergeReviewTS = mergeReviewTS;
    }

    /**
     * @return the mergeCompleteTS
     */
    public LocalDateTime getMergeCompleteTS() {
        return mergeCompleteTS;
    }

    /**
     * @param mergeCompleteTS the mergeCompleteTS to set
     */
    public void setMergeCompleteTS(LocalDateTime mergeCompleteTS) {
        this.mergeCompleteTS = mergeCompleteTS;
    }

 
}
