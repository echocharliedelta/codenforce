/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Fields provide CECase one-touch button creation default values.
 * 
 * @author ellen bascomb of apartment 31Y
 */
public class CECaseOneClickTemplate implements Serializable, Comparable{
    private int oneClickID;
    private Municipality muni;
    
    private String title;
    private String defaultCaseName;
    
    private EnforceableCodeElement violatedOrdinance;
    private EventCategory originationEventCategory;
    private NoticeOfViolationTemplate novTemplate;
    
    private Icon icon;
    private String notes;
    
    private int displayOrder;
    
    private boolean autoFinalizeNOV;
    
    private LocalDateTime deactivatedTS;
    

    /**
     * @return the oneClickID
     */
    public int getOneClickID() {
        return oneClickID;
    }

    /**
     * @param oneClickID the oneClickID to set
     */
    public void setOneClickID(int oneClickID) {
        this.oneClickID = oneClickID;
    }

    /**
     * @return the muni
     */
    public Municipality getMuni() {
        return muni;
    }

    /**
     * @param muni the muni to set
     */
    public void setMuni(Municipality muni) {
        this.muni = muni;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the defaultCaseName
     */
    public String getDefaultCaseName() {
        return defaultCaseName;
    }

    /**
     * @param defaultCaseName the defaultCaseName to set
     */
    public void setDefaultCaseName(String defaultCaseName) {
        this.defaultCaseName = defaultCaseName;
    }

    /**
     * @return the violatedOrdinance
     */
    public EnforceableCodeElement getViolatedOrdinance() {
        return violatedOrdinance;
    }

    /**
     * @param violatedOrdinance the violatedOrdinance to set
     */
    public void setViolatedOrdinance(EnforceableCodeElement violatedOrdinance) {
        this.violatedOrdinance = violatedOrdinance;
    }

    /**
     * @return the originationEventCategory
     */
    public EventCategory getOriginationEventCategory() {
        return originationEventCategory;
    }

    /**
     * @param originationEventCategory the originationEventCategory to set
     */
    public void setOriginationEventCategory(EventCategory originationEventCategory) {
        this.originationEventCategory = originationEventCategory;
    }

    /**
     * @return the novTemplate
     */
    public NoticeOfViolationTemplate getNovTemplate() {
        return novTemplate;
    }

    /**
     * @param novTemplate the novTemplate to set
     */
    public void setNovTemplate(NoticeOfViolationTemplate novTemplate) {
        this.novTemplate = novTemplate;
    }

    /**
     * @return the icon
     */
    public Icon getIcon() {
        return icon;
    }

    /**
     * @param icon the icon to set
     */
    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    /**
     * @return the deactivatedTS
     */
    public LocalDateTime getDeactivatedTS() {
        return deactivatedTS;
    }

    /**
     * @param deactivatedTS the deactivatedTS to set
     */
    public void setDeactivatedTS(LocalDateTime deactivatedTS) {
        this.deactivatedTS = deactivatedTS;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return the displayOrder
     */
    public int getDisplayOrder() {
        return displayOrder;
    }

    /**
     * @param displayOrder the displayOrder to set
     */
    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof CECaseOneClickTemplate oneClick){
            Integer myOrder = displayOrder;
            Integer theirOrder = oneClick.displayOrder;
            return myOrder.compareTo(theirOrder);
        }
        return 0;
    }

    /**
     * @return the autoFinalizeNOV
     */
    public boolean isAutoFinalizeNOV() {
        return autoFinalizeNOV;
    }

    /**
     * @param autoFinalizeNOV the autoFinalizeNOV to set
     */
    public void setAutoFinalizeNOV(boolean autoFinalizeNOV) {
        this.autoFinalizeNOV = autoFinalizeNOV;
    }
    
}
