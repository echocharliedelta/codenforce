/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * To avoid DB cycles, this subclass is the only one with a status set. 
 * The superclass is held directly by citations but this 
 * subclass is held by CE Cases
 * 
 * @author pierre15
 */
public class CodeViolationStatusHeavy extends CodeViolation {
    
    protected CodeViolationStatusEnum status;
    protected String statusString;
    
    public CodeViolationStatusHeavy(CodeViolation cv){
        super(cv);
    }

    
    /**
     * @return the status
     */
    public CodeViolationStatusEnum getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(CodeViolationStatusEnum status) {
        this.status = status;
    }
    
    /**
     * @return the statusString
     */
    public String getStatusString() {
        return statusString;
    }

    /**
     * @param statusString the statusString to set
     */
    public void setStatusString(String statusString) {
        this.statusString = statusString;
    }

    
}
