/*
 * Copyright (C) 2019 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.entities.occupancy.OccChecklistTemplate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import com.tcvcog.tcvce.application.interfaces.IFaceActivatableBOB;

/**
 * Contains oodles of configuration flags along with municipality identifying info
 * including contacts and ordinance information.
 * 
 * In my belly I also have a municipality profile, which contains a potentially 
 * shared set of configuration switches governing the permission systems and more
 * 
 * @author sylvia
 */
public  class       MunicipalityDataHeavy 
        extends     Municipality
        implements  IFace_humanListHolder,
                    IFace_BlobHolder, 
                    IFace_noteHolder,
                    IFaceActivatableBOB{
    
    final static LinkedObjectSchemaEnum HUMAN_LINK_SCHEMA_ENUM = LinkedObjectSchemaEnum.MuniHuman;
    private final static BlobLinkEnum BLOP_LINK_INFO = BlobLinkEnum.MUNICIPALITY;
    private final static BlobLinkEnum BLOP_UPSPTREAM_POOL = null;
    private final static String FRIENDLY_NAME = "Municipality Data-Heavy";
    
    private int muniProfileID;
    private MuniProfile profile;
    private boolean activeInProgram;
    
    private int muniOfficePropertyId;
    private PropertyDataHeavy muniPropertyDH;
    private int defaultOccPeriodID;

    //contact
    private MailingAddress contactAddress;
    private UserMuniAuthPeriod muniManagerUMAP;
    private UserMuniAuthPeriod muniPrimaryOfficerUMAP;
    private UserMuniAuthPeriod muniAdminContactUMAP;
    
    
    private int population;
    private int populationYear;
    
    private int defaultCodeSetID;
    private CodeSet codeSet;
    private List<CodeSource> issuingCodeSourceList;
    
    private PrintStyle defaultNOVPrintStyle;
    private BlobLight defaultMuniHeaderImage;
    
    private String mapThumbFilePath; //added by wwalk 3.31.23
    static final String MAP_THUMB_DEFAULT_PATH = "/images/map_thumbnails/default.png"; //added by wwalk 3.31.23
    
    private int defaultMuniHeaderImageWidthPX;
    
    private boolean enableCodeEnforcement;
    private boolean enableOccupancy;
    private boolean enablePublicCEActionRequestSubmissions;
    
    private boolean enablePublicCEActionRequestInfo;
    private boolean enablePublicOccPermitApp;
    private boolean enablePublicOccInspectionTODOs;
    

    private String notes; 
    
    // These are configured in the muni manage page 
    private List<County> countyList;
    private List<CourtEntity> courtEntities;
    private List<MailingCityStateZip> zipList;
    
    // these are not configured in munimanage page
    private List<User> userListMuniActiveStaffAndUp;
    private List<User> swornOfficerList;
    private List<BlobLight> blobList;
    private List<HumanLink> humanLinkList;
    
    
    /**
     * Stats
     */
    private int propertyCount;
    
     /**
      * Pre-Credential constructor
      * @Deprecated 
      * @param m 
      */
    public MunicipalityDataHeavy(Municipality m){
        this.muniName = m.getMuniName();
        this.muniCode = m.getMuniCode();
    }
    
    public MunicipalityDataHeavy(){
        
    }
    
     /***************************************************************************
     * ************             SPECIAL GETTERS             **********************
     * *************************************************************************
     */
    
    /**
     * Convenience getter that reaches into the muni property DH and gets its
     * primary address and returns it
     * @return the MailingAddress of the muni prop DH OR Null; caller must check
     */
    public MailingAddress getMuniAddress(){
        if(muniPropertyDH != null && muniPropertyDH.getAddress() != null){
            return muniPropertyDH.getAddress();
        } else {
            return null;
        }
    }
    
    
     /***************************************************************************
     * ************             INSPECTION TOOLS          **********************
     * *************************************************************************
     */
    
    private OccChecklistTemplate defaultInspectionChecklist;
  
    /**
     * @return the population
     */
    public int getPopulation() {
        return population;
    }

    /**
     * @return the activeInProgram
     */
    public boolean isActiveInProgram() {
        return activeInProgram;
    }

    /**
     * @return the codeSet
     */
    public CodeSet getCodeSet() {
        return codeSet;
    }

    /**
     * @return the issuingCodeSource
     */
    public List<CodeSource> getIssuingCodeSourceList() {
        return issuingCodeSourceList;
    }

    /**
     * @return the profile
     */
    public MuniProfile getProfile() {
        return profile;
    }

    /**
     * @return the enableCodeEnforcement
     */
    public boolean isEnableCodeEnforcement() {
        return enableCodeEnforcement;
    }

    /**
     * @return the enableOccupancy
     */
    public boolean isEnableOccupancy() {
        return enableOccupancy;
    }

    /**
     * @return the enablePublicCEActionRequestSubmissions
     */
    public boolean isEnablePublicCEActionRequestSubmissions() {
        return enablePublicCEActionRequestSubmissions;
    }

    /**
     * @return the enablePublicCEActionRequestInfo
     */
    public boolean isEnablePublicCEActionRequestInfo() {
        return enablePublicCEActionRequestInfo;
    }

    /**
     * @return the enablePublicOccPermitApp
     */
    public boolean isEnablePublicOccPermitApp() {
        return enablePublicOccPermitApp;
    }

    /**
     * @return the enablePublicOccInspectionTODOs
     */
    public boolean isEnablePublicOccInspectionTODOs() {
        return enablePublicOccInspectionTODOs;
    }

  

    /**
     * @return the muniOfficePropertyId
     */
    public int getMuniOfficePropertyId() {
        if(muniPropertyDH != null){
            return muniPropertyDH.getParcelKey();
        }
        return muniOfficePropertyId;
    }

    /**
     * @return the notes
     */
    @Override
    public String getNotes() {
        return notes;
    }

  

    /**
     * @return the userListMuniActiveStaffAndUp
     */
    public List<User> getUserListMuniActiveStaffAndUp() {
        return userListMuniActiveStaffAndUp;
    }

    /**
     * @return the courtEntities
     */
    public List<CourtEntity> getCourtEntities() {
        return courtEntities;
    }

   
    /**
     * @param population the population to set
     */
    public void setPopulation(int population) {
        this.population = population;
    }

    /**
     * @param activeInProgram the activeInProgram to set
     */
    public void setActiveInProgram(boolean activeInProgram) {
        this.activeInProgram = activeInProgram;
    }

    /**
     * @param codeSet the codeSet to set
     */
    public void setCodeSet(CodeSet codeSet) {
        this.codeSet = codeSet;
    }

    /**
     * @param issuingCodeSource the issuingCodeSource to set
     */
    public void setIssuingCodeSourceList(List<CodeSource> issuingCodeSource) {
        this.issuingCodeSourceList = issuingCodeSource;
    }

    /**
     * @param profile the profile to set
     */
    public void setProfile(MuniProfile profile) {
        this.profile = profile;
    }

    /**
     * @param enableCodeEnforcement the enableCodeEnforcement to set
     */
    public void setEnableCodeEnforcement(boolean enableCodeEnforcement) {
        this.enableCodeEnforcement = enableCodeEnforcement;
    }

    /**
     * @param enableOccupancy the enableOccupancy to set
     */
    public void setEnableOccupancy(boolean enableOccupancy) {
        this.enableOccupancy = enableOccupancy;
    }

    /**
     * @param enablePublicCEActionRequestSubmissions the enablePublicCEActionRequestSubmissions to set
     */
    public void setEnablePublicCEActionRequestSubmissions(boolean enablePublicCEActionRequestSubmissions) {
        this.enablePublicCEActionRequestSubmissions = enablePublicCEActionRequestSubmissions;
    }

    /**
     * @param enablePublicCEActionRequestInfo the enablePublicCEActionRequestInfo to set
     */
    public void setEnablePublicCEActionRequestInfo(boolean enablePublicCEActionRequestInfo) {
        this.enablePublicCEActionRequestInfo = enablePublicCEActionRequestInfo;
    }

    /**
     * @param enablePublicOccPermitApp the enablePublicOccPermitApp to set
     */
    public void setEnablePublicOccPermitApp(boolean enablePublicOccPermitApp) {
        this.enablePublicOccPermitApp = enablePublicOccPermitApp;
    }

    /**
     * @param enablePublicOccInspectionTODOs the enablePublicOccInspectionTODOs to set
     */
    public void setEnablePublicOccInspectionTODOs(boolean enablePublicOccInspectionTODOs) {
        this.enablePublicOccInspectionTODOs = enablePublicOccInspectionTODOs;
    }

   
    /**
     * @param muniOfficePropertyId the muniOfficePropertyId to set
     */
    public void setMuniOfficePropertyId(int muniOfficePropertyId) {
        this.muniOfficePropertyId = muniOfficePropertyId;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

  
    /**
     * @param userListMuniActiveStaffAndUp the userListMuniActiveStaffAndUp to set
     */
    public void setUserListMuniActiveStaffAndUp(List<User> userListMuniActiveStaffAndUp) {
        this.userListMuniActiveStaffAndUp = userListMuniActiveStaffAndUp;
    }

    /**
     * @param courtEntities the courtEntities to set
     */
    public void setCourtEntities(List<CourtEntity> courtEntities) {
        this.courtEntities = courtEntities;
    }

      @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.population;
        hash = 97 * hash + (this.isActiveInProgram() ? 1 : 0);
        hash = 97 * hash + Objects.hashCode(this.codeSet);
        hash = 97 * hash + Objects.hashCode(this.issuingCodeSourceList);
        hash = 97 * hash + Objects.hashCode(this.profile);
        hash = 97 * hash + (this.enableCodeEnforcement ? 1 : 0);
        hash = 97 * hash + (this.enableOccupancy ? 1 : 0);
        hash = 97 * hash + (this.enablePublicCEActionRequestSubmissions ? 1 : 0);
        hash = 97 * hash + (this.enablePublicCEActionRequestInfo ? 1 : 0);
        hash = 97 * hash + (this.enablePublicOccPermitApp ? 1 : 0);
        hash = 97 * hash + (this.enablePublicOccInspectionTODOs ? 1 : 0);
        hash = 97 * hash + this.muniOfficePropertyId;
        hash = 97 * hash + Objects.hashCode(this.notes);
        hash = 97 * hash + Objects.hashCode(this.getUserListMuniActiveStaffAndUp());
        hash = 97 * hash + Objects.hashCode(this.courtEntities);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MunicipalityDataHeavy other = (MunicipalityDataHeavy) obj;
        if (this.population != other.population) {
            return false;
        }
        if (this.isActiveInProgram() != other.isActiveInProgram()) {
            return false;
        }
        if (this.enableCodeEnforcement != other.enableCodeEnforcement) {
            return false;
        }
        if (this.enableOccupancy != other.enableOccupancy) {
            return false;
        }
        if (this.enablePublicCEActionRequestSubmissions != other.enablePublicCEActionRequestSubmissions) {
            return false;
        }
        if (this.enablePublicCEActionRequestInfo != other.enablePublicCEActionRequestInfo) {
            return false;
        }
        if (this.enablePublicOccPermitApp != other.enablePublicOccPermitApp) {
            return false;
        }
        if (this.enablePublicOccInspectionTODOs != other.enablePublicOccInspectionTODOs) {
            return false;
        }
        if (this.muniOfficePropertyId != other.muniOfficePropertyId) {
            return false;
        }
        if (!Objects.equals(this.notes, other.notes)) {
            return false;
        }
        if (!Objects.equals(this.codeSet, other.codeSet)) {
            return false;
        }
        if (!Objects.equals(this.issuingCodeSourceList, other.issuingCodeSourceList)) {
            return false;
        }
        if (!Objects.equals(this.profile, other.profile)) {
            return false;
        }
       
        if (!Objects.equals(this.courtEntities, other.courtEntities)) {
            return false;
        }
        return true;
    }

    /**
     * @return the muniPropertyDH
     */
    public PropertyDataHeavy getMuniPropertyDH() {
        return muniPropertyDH;
    }

    /**
     * @param muniPropertyDH the muniPropertyDH to set
     */
    public void setMuniPropertyDH(PropertyDataHeavy muniPropertyDH) {
        this.muniPropertyDH = muniPropertyDH;
    }

    /**
     * @return the defaultOccPeriodID
     */
    public int getDefaultOccPeriodID() {
        return defaultOccPeriodID;
    }

    /**
     * @param defaultOccPeriodID the defaultOccPeriodID to set
     */
    public void setDefaultOccPeriodID(int defaultOccPeriodID) {
        this.defaultOccPeriodID = defaultOccPeriodID;
    }

    /**
     * @return the propertyCount
     */
    public int getPropertyCount() {
        return propertyCount;
    }

    /**
     * @param propertyCount the propertyCount to set
     */
    public void setPropertyCount(int propertyCount) {
        this.propertyCount = propertyCount;
    }


    @Override
    public List<HumanLink> gethumanLinkList() {
        return humanLinkList;
    }

    @Override
    public void sethumanLinkList(List<HumanLink> hll) {
        humanLinkList = hll;
    }

    @Override
    public LinkedObjectSchemaEnum getHUMAN_LINK_SCHEMA_ENUM() {
        return HUMAN_LINK_SCHEMA_ENUM;
    }


    @Override
    public int getHostPK() {
        return muniCode;
    }

    /**
     * @return the swornOfficerList
     */
    public List<User> getSwornOfficerList() {
        return swornOfficerList;
    }

    /**
     * @param swornOfficerList the swornOfficerList to set
     */
    public void setSwornOfficerList(List<User> swornOfficerList) {
        this.swornOfficerList = swornOfficerList;
    }

    @Override
    public void setBlobList(List<BlobLight> bl) {
        this.blobList = bl;
    }

    @Override
    public List<BlobLight> getBlobList() {
        return blobList;
    }

    @Override
    public BlobLinkEnum getBlobLinkEnum() {
        return BLOP_LINK_INFO;
    }

    @Override
    public int getParentObjectID() {
        return muniCode;
    }

    @Override
    public BlobLinkEnum getBlobUpstreamPoolEnum() {
        return BLOP_UPSPTREAM_POOL;
    }

    @Override
    public int getBlobUpstreamPoolEnumPoolFeederID() {
        return 0;
    }

    /**
     * @return the zipList
     */
    public List<MailingCityStateZip> getZipList() {
        return zipList;
    }

    /**
     * @param zipList the zipList to set
     */
    public void setZipList(List<MailingCityStateZip> zipList) {
        this.zipList = zipList;
    }

    /**
     * @return the defaultMuniHeaderImage
     */
    public BlobLight getDefaultMuniHeaderImage() {
        return defaultMuniHeaderImage;
    }

    /**
     * @param defaultMuniHeaderImage the defaultMuniHeaderImage to set
     */
    public void setDefaultMuniHeaderImage(BlobLight defaultMuniHeaderImage) {
        this.defaultMuniHeaderImage = defaultMuniHeaderImage;
    }

    /**
     * @return the defaultMuniHeaderImageWidthPX
     */
    public int getDefaultMuniHeaderImageWidthPX() {
        return defaultMuniHeaderImageWidthPX;
    }

    /**
     * @param defaultMuniHeaderImageWidthPX the defaultMuniHeaderImageWidthPX to set
     */
    public void setDefaultMuniHeaderImageWidthPX(int defaultMuniHeaderImageWidthPX) {
        this.defaultMuniHeaderImageWidthPX = defaultMuniHeaderImageWidthPX;
    }
    
    
    //added by wwalk 3.31.23
    /**
     * @param mapThumbFilePath the mapThumbFilePath to set
     */
    public void setMapThumbFilePath(String mapThumbFilePath) {
        this.mapThumbFilePath = mapThumbFilePath;
    }
    
    public String getMapThumbFilePath(){
        return mapThumbFilePath;
    }

    @Override
    public String getNoteHolderFriendlyName() {
        return FRIENDLY_NAME;
    }

    /**
     * @return the contactAddress
     */
    public MailingAddress getContactAddress() {
        return contactAddress;
    }

    /**
     * @param contactAddress the contactAddress to set
     */
    public void setContactAddress(MailingAddress contactAddress) {
        this.contactAddress = contactAddress;
    }

   

    /**
     * @return the populationYear
     */
    public int getPopulationYear() {
        return populationYear;
    }

    /**
     * @param populationYear the populationYear to set
     */
    public void setPopulationYear(int populationYear) {
        this.populationYear = populationYear;
    }

    /**
     * @return the defaultNOVPrintStyle
     */
    public PrintStyle getDefaultNOVPrintStyle() {
        return defaultNOVPrintStyle;
    }

    /**
     * @param defaultNOVPrintStyle the defaultNOVPrintStyle to set
     */
    public void setDefaultNOVPrintStyle(PrintStyle defaultNOVPrintStyle) {
        this.defaultNOVPrintStyle = defaultNOVPrintStyle;
    }

    /**
     * @return the countyList
     */
    public List<County> getCountyList() {
        return countyList;
    }

    /**
     * @param countyList the countyList to set
     */
    public void setCountyList(List<County> countyList) {
        this.countyList = countyList;
    }

    /**
     * @return the muniProfileID
     */
    public int getMuniProfileID() {
        if(profile != null){
            return profile.getProfileID();
        }
        return muniProfileID;
    }

    /**
     * @param muniProfileID the muniProfileID to set
     */
    public void setMuniProfileID(int muniProfileID) {
        this.muniProfileID = muniProfileID;
    }

    /**
     * @return the defaultCodeSetID
     */
    public int getDefaultCodeSetID() {
        return defaultCodeSetID;
    }

    /**
     * @param defaultCodeSetID the defaultCodeSetID to set
     */
    public void setDefaultCodeSetID(int defaultCodeSetID) {
        this.defaultCodeSetID = defaultCodeSetID;
    }

    @Override
    public String getDescriptionString() {
        return muniName;
    }

    @Override
    public LinkedObjectSchemaEnum getUpstreamHumanLinkPoolEnum() {
        return null;
    }

    @Override
    public int getUpstreamHumanLinkPoolFeederID() {
        return 0;
    }

    /**
     * @return the muniManagerUMAP
     */
    public UserMuniAuthPeriod getMuniManagerUMAP() {
        return muniManagerUMAP;
    }

    /**
     * @param muniManagerUMAP the muniManagerUMAP to set
     */
    public void setMuniManagerUMAP(UserMuniAuthPeriod muniManagerUMAP) {
        this.muniManagerUMAP = muniManagerUMAP;
    }

    /**
     * @return the muniPrimaryOfficerUMAP
     */
    public UserMuniAuthPeriod getMuniPrimaryOfficerUMAP() {
        return muniPrimaryOfficerUMAP;
    }

    /**
     * @param muniPrimaryOfficerUMAP the muniPrimaryOfficerUMAP to set
     */
    public void setMuniPrimaryOfficerUMAP(UserMuniAuthPeriod muniPrimaryOfficerUMAP) {
        this.muniPrimaryOfficerUMAP = muniPrimaryOfficerUMAP;
    }

    /**
     * @return the muniAdminContactUMAP
     */
    public UserMuniAuthPeriod getMuniAdminContactUMAP() {
        return muniAdminContactUMAP;
    }

    /**
     * @param muniAdminContactUMAP the muniAdminContactUMAP to set
     */
    public void setMuniAdminContactUMAP(UserMuniAuthPeriod muniAdminContactUMAP) {
        this.muniAdminContactUMAP = muniAdminContactUMAP;
    }

    /**
     * @return the defaultInspectionChecklist
     */
    public OccChecklistTemplate getDefaultInspectionChecklist() {
        return defaultInspectionChecklist;
    }

    /**
     * @param defaultInspectionChecklist the defaultInspectionChecklist to set
     */
    public void setDefaultInspectionChecklist(OccChecklistTemplate defaultInspectionChecklist) {
        this.defaultInspectionChecklist = defaultInspectionChecklist;
    }
  
    
}
