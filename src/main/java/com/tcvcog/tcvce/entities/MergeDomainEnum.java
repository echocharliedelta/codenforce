/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Declarations of object families that can be merged; As of first implementation
 * in June 2024 this Enum used only by AddressBB for Street and Address merging
 * which mirror each other. Person merging is a complex mess so is not enumerated
 * here yet for simplicity.
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */
public enum MergeDomainEnum {
    STREET("Street", "Streets", true, false, "road"),
    MAILINGADDRESS("Mailing addresss", "Mailing addresses", false, true, "store");
    
    private final String label;
    private final String labelPlural;
    /**
     * A completely awful and terrible solution to having nice
     * flags for UI reuse purposes
     */
    private final boolean enableStreetMerging;
    /**
     * A completely awful and terrible solution to having nice
     * flags for UI reuse purposes. In theory i'll need a boolean flag
     * for each new enum value and have to adjust the constructor
     * again EACH TIME.
     */
    private final boolean enableAddressMerging;
    private final String materialIcon;
    
    private MergeDomainEnum(String l, String lp, boolean str, boolean mad, String matic){
        label = l;
        labelPlural = lp;
        enableStreetMerging = str;
        enableAddressMerging = mad;
        materialIcon = matic;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @return the enableStreetMerging
     */
    public boolean isEnableStreetMerging() {
        return enableStreetMerging;
    }

   

    /**
     * @return the enableAddressMerging
     */
    public boolean isEnableAddressMerging() {
        return enableAddressMerging;
    }

    /**
     * @return the materialIcon
     */
    public String getMaterialIcon() {
        return materialIcon;
    }

    /**
     * @return the labelPlural
     */
    public String getLabelPlural() {
        return labelPlural;
    }

    

    
}
