/*
 * Copyright (C) 2022 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * A subclass of UMAP that contains the three fields that the UMAP only
 * allows to be written during object construction: Muni, rank, and officer yes/no
 * 
 * A bit of a hacky workaround, but at least code can't just call the setter 
 * for setRole and make a sys admin; they'd have to change the actual DB record
 * 
 * @author sylvia
 */
public class UserMuniAuthPeriodInsertion 
        extends UserMuniAuthPeriod{
    
    private Municipality insertMuni;
    private RoleType insertRole;
    private boolean insertOfficer;
    
    /**
     * Constructor that passes in the given object for muni
     * null for role, and false for ceo to the superclass constructor
     * @param m
     */
    public UserMuniAuthPeriodInsertion(Municipality m){
        super(m, null, false);
    }

    /**
     * @return the insertMuni
     */
    public Municipality getInsertMuni() {
        return insertMuni;
    }

    /**
     * @return the insertRole
     */
    public RoleType getInsertRole() {
        return insertRole;
    }

    /**
     * @return the insertOfficer
     */
    public boolean isInsertOfficer() {
        return insertOfficer;
    }

    /**
     * @param insertMuni the insertMuni to set
     */
    public void setInsertMuni(Municipality insertMuni) {
        this.insertMuni = insertMuni;
    }

    /**
     * @param insertRole the insertRole to set
     */
    public void setInsertRole(RoleType insertRole) {
        this.insertRole = insertRole;
    }

    /**
     * @param insertOfficer the insertOfficer to set
     */
    public void setInsertOfficer(boolean insertOfficer) {
        this.insertOfficer = insertOfficer;
    }
    
}
