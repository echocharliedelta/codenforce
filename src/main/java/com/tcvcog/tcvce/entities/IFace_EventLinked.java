/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import java.util.List;

/**
 * Implemented by objects which aren't actual parents of events, but who can 
 * get linked to events in the course of their operation, such as NOVs who have
 * follow-up events in their bellies, or citations which have events related to their creation etc.
 * 
 * @author pierre15
 */
public interface IFace_EventLinked {
    
    /**
     * INject the events linked to the implementing class
     * @param evList 
     */
    public void setLinkedEvents(List<EventCnF> evList);
    
    /**
     * The list of events linked to the implementing class
     * @return 
     */
    public List<EventCnF> getLinkedEvents();
    
    /**
     * Facility for grabbing the events linked to this object
     * @return the enum constant that contains the FK field in the DB for the 
     * implementing objet
     */
    public EventLinkEnum getEventLinkEnum();
    
    /**
     * The actual key value
     * @return 
     */
    public int getLinkTargetPrimaryKey();
    
}
