/*
 * Copyright (C) 2022 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Enumerates the objects that extend UMAPTrackedEntity and therefore
 * can be processed by the third generation permissions systems
 * 
 * @author sylvia
 */
public enum UMAPTrackedEnum {
    CODESOURCE,
    CODESET,
    MUNICIPALITY,
    MUNIPROFILE,
    CECASE,
    MAILING_ADDRESS_UNIT,
    COURT_ENTITY,
    LINKED_OBJECT_ROLE,
    OCC_INSPECTION_DETERMINATION,
    OCC_INSPECTION_CAUSE,
    OCC_INSPECTION_CHECKLIST,
    PROPERTY_USE_TYPE,
    OCC_PERMIT_TYPE,
    CODEELEMENTGUIDEENTRY;
}
