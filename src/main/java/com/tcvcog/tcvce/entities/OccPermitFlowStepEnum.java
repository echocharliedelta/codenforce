/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Represents the step sequence for occ permit flow
 * @author pierre15
 */
public enum OccPermitFlowStepEnum {
    PERMIT_TYPE (1, "Certificate Type"),
    DATES (2, "Dates"),
    PERSONS (3, "Persons"),
    REMARKS (4, "Remarks"),
    REVIEW (5,  "Review"),
    ISSUE (6, "Print, Issue, and Record");
    
    private final int stepNumber1Based;
    private final String stepTitle;
    
    private OccPermitFlowStepEnum(int no, String t){
        stepNumber1Based = no;
        stepTitle = t;
    }

    /**
     * @return the stepNumber1Based
     */
    public int getStepNumber1Based() {
        return stepNumber1Based;
    }

    /**
     * @return the stepTitle
     */
    public String getStepTitle() {
        return stepTitle;
    }
    
    
}
