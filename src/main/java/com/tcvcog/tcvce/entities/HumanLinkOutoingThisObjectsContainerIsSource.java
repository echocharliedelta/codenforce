/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * This is an abhorrent spectacle of OO design: I am a neutered 
 * HumanLink so when viewing the target of a HumanLink users can
 * see link sources. I.e. if Ellen IS OWNER OF VBONE LLC, when users
 * view VBONE LLC they'll be able to see that Ellen is their owner, i.e.
 * Ellen is the link Source.
 * 
 * We can't use normal HumanLinks here because a HumanLInk extends Person which
 * has HumanLinks in its belly and that's ripe for an infinite Loop. So this class
 * contains flattened fields minus the source's HumanLinks.
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */
public class HumanLinkOutoingThisObjectsContainerIsSource {
    private int sourceHumanID;
    private int targetHumanID;
    private String linkTargetHumanName;
    private LinkedObjectRole sourceToTargetLinkedObjectRole;
    private int humanLinkID;

    /**
     * @return the sourceHumanID
     */
    public int getSourceHumanID() {
        return sourceHumanID;
    }

    /**
     * @param sourceHumanID the sourceHumanID to set
     */
    public void setSourceHumanID(int sourceHumanID) {
        this.sourceHumanID = sourceHumanID;
    }

    /**
     * @return the linkTargetHumanName
     */
    public String getLinkTargetHumanName() {
        return linkTargetHumanName;
    }

    /**
     * @param linkTargetHumanName the linkTargetHumanName to set
     */
    public void setLinkTargetHumanName(String linkTargetHumanName) {
        this.linkTargetHumanName = linkTargetHumanName;
    }

    /**
     * @return the sourceToTargetLinkedObjectRole
     */
    public LinkedObjectRole getSourceToTargetLinkedObjectRole() {
        return sourceToTargetLinkedObjectRole;
    }

    /**
     * @param sourceToTargetLinkedObjectRole the sourceToTargetLinkedObjectRole to set
     */
    public void setSourceToTargetLinkedObjectRole(LinkedObjectRole sourceToTargetLinkedObjectRole) {
        this.sourceToTargetLinkedObjectRole = sourceToTargetLinkedObjectRole;
    }

    /**
     * @return the humanLinkID
     */
    public int getHumanLinkID() {
        return humanLinkID;
    }

    /**
     * @param humanLinkID the humanLinkID to set
     */
    public void setHumanLinkID(int humanLinkID) {
        this.humanLinkID = humanLinkID;
    }

    /**
     * @return the targetHumanID
     */
    public int getTargetHumanID() {
        return targetHumanID;
    }

    /**
     * @param targetHumanID the targetHumanID to set
     */
    public void setTargetHumanID(int targetHumanID) {
        this.targetHumanID = targetHumanID;
    }
}
