/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.util.Constants;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Contains raw String fields for a building, street, and ZIP Code
 * and some match candidates and selected match for use with the quick add
 * facility
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */
public class MailingAddressQuickAddBundle {
   
    private String quickAddBuilding;
    private String quickAddStreet;
    private String quickAddZIPCode;
    private String matchLog;
    /**
     * If this member is NULL then the fields haven't been matched against the DB;
     * If an empty list, the matching has occurred and no close match was found.
     * If a non-empty list, candidates exist for user review.
     */
    private List<MailingAddress> madCandidateList;
    /**
     * If null, AND the madCandidateList is an EMPTY list, then write a new address
     * using the form fields.
     * If not null, activate this as the session address for linking.
     */
    private MailingAddress chosenMADMatch;
    
    private LocalDateTime validAddressFieldsMatchingComplete;
    
    private String toWriteBuildingNo;
    private String toWriteStreet;
    private List<MailingStreet> toWriteStreetCandidateList;
    private MailingCityStateZip toWriteZIPCode;
    
    private final StringBuilder logBuilder;

    public MailingAddressQuickAddBundle() {
        logBuilder = new StringBuilder();
    }
    
    /**
     * Slaps not null inputs to the internal log builder;
     * @param s 
     */
    public void append(String s){
        if(logBuilder != null && s != null){
            logBuilder.append(s);
        }
    }
    
    
    /**
     * Dumps log
     * @return log or empty string
     */
    public String getLog(){
        if(logBuilder != null){
            return logBuilder.toString();
        }
        return "";
    }
    
    /**
     * Organizes the building, street, and ZIP for pretty printing
     * @return 
     */
    public String getFormattedAddressToWriteEscapeFalse(){
        if(toWriteBuildingNo != null && toWriteStreet != null && toWriteZIPCode != null){
            StringBuilder sb = new StringBuilder();
            sb.append(toWriteBuildingNo);
            sb.append(Constants.FMT_SPACE_LITERAL);
            if(toWriteStreetCandidateList != null && !toWriteStreetCandidateList.isEmpty()){
                sb.append(toWriteStreetCandidateList.get(0).getName());
                sb.append(" (existing street record");
                if(toWriteStreetCandidateList.size() > 1){
                    sb.append("; chose first of ");
                    sb.append(toWriteStreetCandidateList.size());
                    sb.append(" existing streets");
                }
                sb.append(")");
            } else {
                sb.append(toWriteStreet);
            }
            sb.append(Constants.FMT_HTML_BREAK);
            sb.append(toWriteZIPCode.toString());
            return sb.toString();
        }
        return "";
    }

    /**
     * @return the chosenMADMatch
     */
    public MailingAddress getChosenMADMatch() {
        return chosenMADMatch;
    }

    /**
     * @param chosenMADMatch the chosenMADMatch to set
     */
    public void setChosenMADMatch(MailingAddress chosenMADMatch) {
        this.chosenMADMatch = chosenMADMatch;
    }

    /**
     * @return the madCandidateList
     */
    public List<MailingAddress> getMadCandidateList() {
        return madCandidateList;
    }

    /**
     * @param madCandidateList the madCandidateList to set
     */
    public void setMadCandidateList(List<MailingAddress> madCandidateList) {
        this.madCandidateList = madCandidateList;
    }

    /**
     * @return the quickAddZIPCode
     */
    public String getQuickAddZIPCode() {
        return quickAddZIPCode;
    }

    /**
     * @param quickAddZIPCode the quickAddZIPCode to set
     */
    public void setQuickAddZIPCode(String quickAddZIPCode) {
        this.quickAddZIPCode = quickAddZIPCode;
    }

    /**
     * @return the quickAddStreet
     */
    public String getQuickAddStreet() {
        return quickAddStreet;
    }

    /**
     * @param quickAddStreet the quickAddStreet to set
     */
    public void setQuickAddStreet(String quickAddStreet) {
        this.quickAddStreet = quickAddStreet;
    }

    /**
     * @return the quickAddBuilding
     */
    public String getQuickAddBuilding() {
        return quickAddBuilding;
    }

    /**
     * @param quickAddBuilding the quickAddBuilding to set
     */
    public void setQuickAddBuilding(String quickAddBuilding) {
        this.quickAddBuilding = quickAddBuilding;
    }

    /**
     * @return the matchLog
     */
    public String getMatchLog() {
        return matchLog;
    }

    /**
     * @param matchLog the matchLog to set
     */
    public void setMatchLog(String matchLog) {
        this.matchLog = matchLog;
    }

    /**
     * @return the toWriteZIPCode
     */
    public MailingCityStateZip getToWriteZIPCode() {
        return toWriteZIPCode;
    }

    /**
     * @param toWriteZIPCode the toWriteZIPCode to set
     */
    public void setToWriteZIPCode(MailingCityStateZip toWriteZIPCode) {
        this.toWriteZIPCode = toWriteZIPCode;
    }

    /**
     * @return the toWriteStreet
     */
    public String getToWriteStreet() {
        return toWriteStreet;
    }

    /**
     * @param toWriteStreet the toWriteStreet to set
     */
    public void setToWriteStreet(String toWriteStreet) {
        this.toWriteStreet = toWriteStreet;
    }

    /**
     * @return the toWriteBuildingNo
     */
    public String getToWriteBuildingNo() {
        return toWriteBuildingNo;
    }

    /**
     * @param toWriteBuildingNo the toWriteBuildingNo to set
     */
    public void setToWriteBuildingNo(String toWriteBuildingNo) {
        this.toWriteBuildingNo = toWriteBuildingNo;
    }

    /**
     * @return the validAddressFieldsMatchingComplete
     */
    public LocalDateTime getValidAddressFieldsMatchingComplete() {
        return validAddressFieldsMatchingComplete;
    }

    /**
     * @param validAddressFieldsMatchingComplete the validAddressFieldsMatchingComplete to set
     */
    public void setValidAddressFieldsMatchingComplete(LocalDateTime validAddressFieldsMatchingComplete) {
        this.validAddressFieldsMatchingComplete = validAddressFieldsMatchingComplete;
    }

    /**
     * @return the toWriteStreetCandidateList
     */
    public List<MailingStreet> getToWriteStreetCandidateList() {
        return toWriteStreetCandidateList;
    }

    /**
     * @param toWriteStreetCandidateList the toWriteStreetCandidateList to set
     */
    public void setToWriteStreetCandidateList(List<MailingStreet> toWriteStreetCandidateList) {
        this.toWriteStreetCandidateList = toWriteStreetCandidateList;
    }

   
    
    
}
