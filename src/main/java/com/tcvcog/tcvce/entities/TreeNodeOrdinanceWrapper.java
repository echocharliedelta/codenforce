/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Holds an ordinance for display in a tree.This class can be used for both non-leaf (body) nodes and leaf nodes. 
 * A body node by definition has a null ordinance and an enum that is not level six: ordinance
 * @author pierre15
 * @param <T>
 */
public class TreeNodeOrdinanceWrapper<T extends CodeElement> {
    
    protected OrdinanceHierarchyEnum hiearchyEnum;
    protected String displayString;
    protected T ordinance;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.hiearchyEnum);
        hash = 59 * hash + Objects.hashCode(this.displayString);
        hash = 59 * hash + Objects.hashCode(this.ordinance);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TreeNodeOrdinanceWrapper other = (TreeNodeOrdinanceWrapper) obj;
        
        if(this.ordinance != null && other.ordinance != null){
            if(this.ordinance.getElementID() == other.ordinance.getElementID()){
                return true;
            }
        } else { // no ordiance, so we're a non-leaf node
            if(this.displayString != null && other.displayString != null){
                return this.displayString.equals(other.displayString);
            } else {
                if(this.hiearchyEnum != null && other.hiearchyEnum != null){
                    return Objects.equals(this.hiearchyEnum, other.hiearchyEnum);
                }
            }
        }
        return false;
    }

    public TreeNodeOrdinanceWrapper(OrdinanceHierarchyEnum hiearchyEnum, String displayString, T ordinance) {
        this.hiearchyEnum = hiearchyEnum;
        this.displayString = displayString;
        this.ordinance = ordinance;
    }
    
    /**
     * Checks if this is a leaf node
     * @return 
     */
    public boolean isLeafNode(){
        return ordinance != null;
    }
    
    /**
     * A hacky method for returning a list of ancestor generations in order
     * from root to current node based on this node's generation. So if this node 
     * is a sub section node, its ancestor list will be Source, Chapter, Section. 
     * It's parent will be the generation that is the last item in the list.
     * 
     * This could probably be replaced with recursion and this would allow
     * easy expansion of the tree builder to a tree of arbitrary number of generations
     * @return all of this node's ancestors, starting at the top of the hierarchy
     */
    public List<OrdinanceHierarchyEnum> getAncestors(){
        List<OrdinanceHierarchyEnum> anList = new ArrayList<>();
        if(hiearchyEnum != null){
            switch (hiearchyEnum) {
                case CODE_SOURCE -> {
                    // source parent is the root
                }
                case CHAPTER -> {
                    anList.add(OrdinanceHierarchyEnum.CODE_SOURCE);
                }
                case SECTION -> {
                    anList.add(OrdinanceHierarchyEnum.CODE_SOURCE);
                    anList.add(OrdinanceHierarchyEnum.CHAPTER);
                }
                case SUB_SECTION -> {
                    anList.add(OrdinanceHierarchyEnum.CODE_SOURCE);
                    anList.add(OrdinanceHierarchyEnum.CHAPTER);
                    anList.add(OrdinanceHierarchyEnum.SECTION);
                }
                case SUB_SUB_SECTION -> {
                    anList.add(OrdinanceHierarchyEnum.CODE_SOURCE);
                    anList.add(OrdinanceHierarchyEnum.CHAPTER);
                    anList.add(OrdinanceHierarchyEnum.SECTION);
                    anList.add(OrdinanceHierarchyEnum.SUB_SECTION);
                }
                default -> {
                    // no enums to add
                }
            }
            // parent is the root
            // nothing to do
                    }
        
        return anList;
    }

    /**
     * @return the hiearchyEnum
     */
    public OrdinanceHierarchyEnum getHiearchyEnum() {
        return hiearchyEnum;
    }

    /**
     * @param hiearchyEnum the hiearchyEnum to set
     */
    public void setHiearchyEnum(OrdinanceHierarchyEnum hiearchyEnum) {
        this.hiearchyEnum = hiearchyEnum;
    }

    /**
     * @return the displayString
     */
    public String getDisplayString() {
        return displayString;
    }

    /**
     * @param displayString the displayString to set
     */
    public void setDisplayString(String displayString) {
        this.displayString = displayString;
    }

    /**
     * @return the ordinance
     */
    public T getOrdinance() {
        return ordinance;
    }

    /**
     * @param ordinance the ordinance to set
     */
    public void setOrdinance(T ordinance) {
        this.ordinance = ordinance;
    }
    
    
    
}
