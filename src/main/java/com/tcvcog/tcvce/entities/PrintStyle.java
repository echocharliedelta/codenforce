/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import java.io.Serializable;
import java.util.Objects;

/**
 * Represents a bundle of print settings, such as where to print an address
 * @author sylvia
 */
public class PrintStyle  implements Serializable, IFaceCachable{
    
    
    
    private int styleID;
    private String description;
    private int headerWidthDefault;
    
    /**
     * Not supported by browsers when ECD checked ages ago.
     */
    private boolean browserHeaderFooterEnabled;
    
    private int nov_page_margin_top;
    private int nov_addressee_margin_left;
    private int nov_addressee_margin_top;
    private int nov_text_margin_top;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.styleID;
        hash = 37 * hash + Objects.hashCode(this.description);
        hash = 37 * hash + this.headerWidthDefault;
        hash = 37 * hash + this.nov_page_margin_top;
        hash = 37 * hash + this.nov_addressee_margin_left;
        hash = 37 * hash + this.nov_addressee_margin_top;
        hash = 37 * hash + this.nov_text_margin_top;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PrintStyle other = (PrintStyle) obj;
        if (this.styleID != other.styleID) {
            return false;
        }
        return true;
    }
    
     @Override
    public int getCacheKey() {
        return getStyleID();
    }
    
    
    public int getDBKey(){
        return styleID;
    }

    /**
     * @return the nov_addressee_margin_top
     */
    public int getNov_addressee_margin_top() {
        return nov_addressee_margin_top;
    }

    /**
     * @param nov_addressee_margin_top the nov_addressee_margin_top to set
     */
    public void setNov_addressee_margin_top(int nov_addressee_margin_top) {
        this.nov_addressee_margin_top = nov_addressee_margin_top;
    }

    /**
     * @return the styleID
     */
    public int getStyleID() {
        return styleID;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return the headerWidthDefault
     */
    public int getHeaderWidthDefault() {
        return headerWidthDefault;
    }


    /**
     * @return the nov_page_margin_top
     */
    public int getNov_page_margin_top() {
        return nov_page_margin_top;
    }

    /**
     * @return the nov_addressee_margin_left
     */
    public int getNov_addressee_margin_left() {
        return nov_addressee_margin_left;
    }

    /**
     * @param styleID the styleID to set
     */
    public void setStyleID(int styleID) {
        this.styleID = styleID;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @param headerWidthDefault the headerWidthDefault to set
     */
    public void setHeaderWidthDefault(int headerWidthDefault) {
        this.headerWidthDefault = headerWidthDefault;
    }

    /**
     * @param nov_page_margin_top the nov_page_margin_top to set
     */
    public void setNov_page_margin_top(int nov_page_margin_top) {
        this.nov_page_margin_top = nov_page_margin_top;
    }

    /**
     * @param nov_addressee_margin_left the nov_addressee_margin_left to set
     */
    public void setNov_addressee_margin_left(int nov_addressee_margin_left) {
        this.nov_addressee_margin_left = nov_addressee_margin_left;
    }

    /**
     * @return the nov_text_margin_top
     */
    public int getNov_text_margin_top() {
        return nov_text_margin_top;
    }

    /**
     * @param nov_text_margin_top the nov_text_margin_top to set
     */
    public void setNov_text_margin_top(int nov_text_margin_top) {
        this.nov_text_margin_top = nov_text_margin_top;
    }


    /**
     * @return the browserHeaderFooterEnabled
     */
    public boolean isBrowserHeaderFooterEnabled() {
        return browserHeaderFooterEnabled;
    }

    /**
     * @param browserHeaderFooterEnabled the browserHeaderFooterEnabled to set
     */
    public void setBrowserHeaderFooterEnabled(boolean browserHeaderFooterEnabled) {
        this.browserHeaderFooterEnabled = browserHeaderFooterEnabled;
    }
    
    
}
