package com.tcvcog.tcvce.entities;

public enum MailingAddressLinkFieldsEnum {
    LINKEDOBJECTROLE("LinkedObjectRole", "Role"),
    PRIORITY("Priority", "Priority");

    private final String fieldName;
    private final String friendlyName;

    private MailingAddressLinkFieldsEnum(String fieldName, String friendlyName) {
        this.fieldName = fieldName;
        this.friendlyName = friendlyName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

}
