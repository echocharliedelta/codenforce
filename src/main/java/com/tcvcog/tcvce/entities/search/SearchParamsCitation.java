package com.tcvcog.tcvce.entities.search;

import com.tcvcog.tcvce.entities.CitationStatus;
import com.tcvcog.tcvce.entities.CitationViolationStatusEnum;
import com.tcvcog.tcvce.entities.EnforceableCodeElement;

public class SearchParamsCitation extends SearchParams {

    public static final String DBFIELD_MUNI = "p.muni_municode";
    public static final String DBFIELD_ACTIVE = "ct.deactivatedts";

    //filter Citation-1
    private boolean citationId_ctl;
    private int citationId_val;

    //filter Citation-2
    private boolean docketNo_ctl;
    private String docketNo_val;

    //filter Citation-3
    private boolean linkStatus_ctl;
    private CitationViolationStatusEnum linkStatus_val;

    //filter Citation-4
    private boolean logEntryStatus_ctl;
    private CitationStatus logEntryStatus_val;

    //filter Citation-5
    private boolean codeElement_ctl;
    private EnforceableCodeElement codeElement_val;

    // filter citation-6
    private boolean citationNumber_ctl;
    private String citationNumber_val;
    
    public SearchParamsCitation() {
    }

    public SearchParamsCitation(SearchParamsCitation params) {
        super(params);
        this.citationId_ctl = params.citationId_ctl;
        this.citationId_val = params.citationId_val;
        this.docketNo_ctl = params.docketNo_ctl;
        this.docketNo_val = params.docketNo_val;
        this.linkStatus_ctl = params.linkStatus_ctl;
        this.linkStatus_val = params.linkStatus_val;
        this.logEntryStatus_ctl = params.logEntryStatus_ctl;
        this.logEntryStatus_val = params.logEntryStatus_val;
        this.codeElement_ctl = params.codeElement_ctl;
        this.codeElement_val = params.codeElement_val;
    }

    public boolean isCitationId_ctl() {
        return citationId_ctl;
    }

    public void setCitationId_ctl(boolean citationId_ctl) {
        this.citationId_ctl = citationId_ctl;
    }

    public int getCitationId_val() {
        return citationId_val;
    }

    public void setCitationId_val(int citationId_val) {
        this.citationId_val = citationId_val;
    }

    public boolean isDocketNo_ctl() {
        return docketNo_ctl;
    }

    public void setDocketNo_ctl(boolean docketNo_ctl) {
        this.docketNo_ctl = docketNo_ctl;
    }

    public String getDocketNo_val() {
        return docketNo_val;
    }

    public void setDocketNo_val(String docketNo_val) {
        this.docketNo_val = docketNo_val;
    }

    public boolean isLinkStatus_ctl() {
        return linkStatus_ctl;
    }

    public void setLinkStatus_ctl(boolean linkStatus_ctl) {
        this.linkStatus_ctl = linkStatus_ctl;
    }

    public CitationViolationStatusEnum getLinkStatus_val() {
        return linkStatus_val;
    }

    public void setLinkStatus_val(CitationViolationStatusEnum linkStatus_val) {
        this.linkStatus_val = linkStatus_val;
    }

    public boolean isLogEntryStatus_ctl() {
        return logEntryStatus_ctl;
    }

    public void setLogEntryStatus_ctl(boolean logEntryStatus_ctl) {
        this.logEntryStatus_ctl = logEntryStatus_ctl;
    }

    public CitationStatus getLogEntryStatus_val() {
        return logEntryStatus_val;
    }

    public void setLogEntryStatus_val(CitationStatus logEntryStatus_val) {
        this.logEntryStatus_val = logEntryStatus_val;
    }

    public boolean isCodeElement_ctl() {
        return codeElement_ctl;
    }

    public void setCodeElement_ctl(boolean codeElement_ctl) {
        this.codeElement_ctl = codeElement_ctl;
    }

    public EnforceableCodeElement getCodeElement_val() {
        return codeElement_val;
    }

    public void setCodeElement_val(EnforceableCodeElement codeElement_val) {
        this.codeElement_val = codeElement_val;
    }

    /**
     * @return the citationNumber_ctl
     */
    public boolean isCitationNumber_ctl() {
        return citationNumber_ctl;
    }

    /**
     * @param citationNumber_ctl the citationNumber_ctl to set
     */
    public void setCitationNumber_ctl(boolean citationNumber_ctl) {
        this.citationNumber_ctl = citationNumber_ctl;
    }

    /**
     * @return the citationNumber_val
     */
    public String getCitationNumber_val() {
        return citationNumber_val;
    }

    /**
     * @param citationNumber_val the citationNumber_val to set
     */
    public void setCitationNumber_val(String citationNumber_val) {
        this.citationNumber_val = citationNumber_val;
    }

}
