package com.tcvcog.tcvce.entities.search;

public enum SearchParamsFieldInspectionDateFieldsEnum implements IFace_dateFieldHolder {
    CREATIONTS(             "Creation DB timestamp",
                            "occinspection.createdts"),
    
    LASTUPDATED(            "Last update",
                            "occinspection.lastupdatedts"),
    
    DEACTIVATED(            "Deactivated timestamp",
                            "occinspection.deactivatedts"),
    
    EFFECTIVEDATE(          "Effective Date",
                            "occinspection.effectivedate"),
    
    TIMESTART(              "Time Start Date",
                            "occinspection.timestart"),
    
    TIMEEND(                "Time End Date",
                            "occinspection.timeend"),
    
    DETERMINATIONDATE(      "Determination Date",
                            "occinspection.determinationts"),
    
    FIELD_INITIATED_ROUTING("Field initated routing timestamp",
                            "occinspection.fieldinitroutingts"),
    
    DISPATCH_CREATIONTS   ( "Dispatch creation timestamp",
                            "occinspectiondispatch.createdts"),
    
    DISPATCH_RETRIEVALTS   ( "Retrieval by field user timestamp",
                            "occinspectiondispatch.retrievalts"),
    
    DISPATCH_SYNCTS   (     "Dispatch syncrhonized back up to server",
                            "occinspectiondispatch.synchronizationts"),
    
    DISPATCH_DEACTS   (     "Dispatch deactivated timestamp",
                            "occinspectiondispatch.deactivatedts"),
    
    DISPATCH_LASTUPDATETS   ( "Dispatch last update timestamp",
                            "occinspectiondispatch.lastupdatedts");

    private final String title;
    private final String dbField;

    private SearchParamsFieldInspectionDateFieldsEnum(String t, String db) {
        this.title = t;
        this.dbField = db;
    }

    /**
     *
     * @return
     */
    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String extractDateFieldString() {
        return dbField;
    }
    
    

}
