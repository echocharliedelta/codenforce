/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.entities.search;

import com.tcvcog.tcvce.entities.RoleType;

/**
 *
 * @author sylvia
 */
public  enum QueryCECaseEnum 
        implements IFace_RankLowerBounded{
    
    OPENCASES(              "All open cases", 
                            "Code enforcement case containing any unresolved violations", 
                            RoleType.MuniReader, 
                            true,
                            true),
    
    EXPIRED_TIMEFRAMES(     "Overdue cases", 
                            "Cases with one or more violations with expired compliance timeframes", 
                            RoleType.MuniReader, 
                            true,
                            false),
    
    CURRENT_TIMEFRAMES(     "Within compliance timeframe", 
                            "Cases whose violations are all insdie compliance timeframes", 
                            RoleType.MuniReader, 
                            true,
                            false),
    
    /**
     * As one of the fanciest queries in all of CnF, I build a multi-param
     * Query that will count cases open as of the end date of a period (like a reporting
     * period) but not that were opened during that period
     */
    OPEN_ASOFENDDATE(     "Cases that were open as of the end of a given date range", 
                            "Excludes cases opened during that date range but includes "
                                    + "those that were open at the start of the range", 
                            RoleType.MuniReader, 
                            true,
                            false),
    
    /**
     * Designed to be used to count total open cases as of the start of a period
     * and will therefore NOT address a period of opening cases in that period which
     *  would be an exceptional case and is addressed by the OPEN_ASOFENDDATE 
     * value in this enumeration.
     * 
     * Intended use requires setting the start and end date range to the same date
     * 
     * 
     */
    OPEN_ASOFGIVENDATE(     "Cases that were open as of a given date", 
                            "Only casese that were open at the end date", 
                            RoleType.MuniReader, 
                            true,
                            true),
    
    OPENED_INDATERANGE(     "Opened in a given date range", 
                            "Cases opened from start to end date", 
                            RoleType.MuniReader, 
                            true,
                            true),
    
    CLOSED_CASES(          "Closed cases in any time period", 
                            "Any closed cases", 
                            RoleType.MuniReader, 
                            true,
                            true),
    
    UNRESOLVED_CITATIONS(   "Outstanding citations", 
                            "Cases with filed citations and are in court system with unpaid citations", 
                            RoleType.MuniReader, 
                            true,
                            false),
    
    ANY_ACTIVITY_7Days(     "Any case activity in past week",
                            "Cases with any new events in the past 7 days", 
                            RoleType.MuniReader, 
                            true,
                            false),
    
    ANY_ACTIVITY_30Days(    "Any case activyt in past month",
                            "Cases with any new events in the past 30 days", 
                            RoleType.MuniReader, 
                            true,
                            false),
    
    PROPERTY(               "CE cases by property",
                            "CE Cases attached to a single specified property", 
                            RoleType.MuniReader, 
                            true,
                            false),
    
    PACC(                   "CE cases by public access control code",
                            "All cases by PACC", 
                            RoleType.MuniStaff, 
                            true,
                            false),
    
    CUSTOM(                 "Custom case query", 
                            "Customized search parameters", 
                            RoleType.MuniReader, 
                            true,
                            true),
    
    MUNI_ALL(               "All active cases in current muni",
                            "Including closed cases",
                            RoleType.MuniStaff,
                            true,
                            true);
    
    private final String title;
    private final String desc;
    private final RoleType requiredRoleMin;
    private final boolean log;
    private final boolean active; 
    
    private QueryCECaseEnum(String t, String l, RoleType minRoleType, boolean lg, boolean act){
        this.desc = l;
        this.title = t;
        this.log = lg;
        if(minRoleType != null){
            this.requiredRoleMin = minRoleType;
        } else {
            this.requiredRoleMin = RoleType.MuniStaff;
        }
        this.active = act;
    }
    
    public String getDesc(){
        return desc;
    }
    
    public String getTitle(){
        return title;
    }

    
    public boolean logQueryRun(){
        return log;
    }

    @Override
    public RoleType getRequiredRoleMin() {
        return requiredRoleMin;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }
    
    
}
