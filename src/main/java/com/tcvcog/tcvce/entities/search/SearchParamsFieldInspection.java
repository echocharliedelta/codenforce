package com.tcvcog.tcvce.entities.search;

import com.tcvcog.tcvce.entities.EventRealm;
import com.tcvcog.tcvce.entities.User;

public class SearchParamsFieldInspection extends SearchParams {

    public static final String DBFIELD_MUNI = "parcel.muni_municode";
    public static final String DBFIELD_ACTIVE = "occinspection.deactivatedts";

    private boolean determination_ctl;
    private boolean determination_val;

    private boolean dt_qualifies_ctl;
    private boolean dt_qualifies_val;
    
    private boolean reinspection_ctl;
    private boolean reinspection_val;

    private boolean domain_ctl;
    private EventRealm domain_val;
    
    /**
     * Grabs inspections that were started from the handheld app
     * and therefore need a property case/period home.
     * Requires muni filter on
     */
    private boolean fieldInit_ctl;
    private boolean fieldInitMuniOccPeriod_val;
    private int fieldInitMuniOccPeriodID;
    private boolean fieldInitNonNullLocDescr_val;
    
    /**
     * From within field inspections that were initiated on the handheld.
     * Requires fieldInit_ctl to be true and fieldInit_val to be true;
     */
    private boolean fieldRoutingStatus_ctl;
    private boolean fieldRoutingStatusRouted_val;
    
    /**
     * This pair of controls grabs FINS that have been dispatched to the field application
     * and may or may not have been completed and synchronized
     */
    private boolean fieldDispatch_ctl;
    /**
     * retrieval means a handheld user has beamed the FIN down to their handheld client
     */
    private boolean fieldDispatchRetrievedByMobile_ctl;
    private boolean fieldDispatchRetrievedByMobile_val;
    /**
     * when true, only FINs which have been dispatched AND completed through upward synchronization 
     */
    private boolean fieldDispatchCycleComplete_ctl;
    private boolean fieldDispatchCycleComplete_val;

    public SearchParamsFieldInspection() {
    }

    public SearchParamsFieldInspectionDateFieldsEnum[] getDateFieldList() {
        SearchParamsFieldInspectionDateFieldsEnum[] fields = SearchParamsFieldInspectionDateFieldsEnum.values();
        return fields;
    }

    public SearchParamsFieldInspectionUserFieldsEnum[] getUserFieldList() {
        SearchParamsFieldInspectionUserFieldsEnum[] fields = SearchParamsFieldInspectionUserFieldsEnum.values();
        return fields;
    }

    public boolean isDetermination_ctl() {
        return determination_ctl;
    }

    public void setDetermination_ctl(boolean determination_ctl) {
        this.determination_ctl = determination_ctl;
    }

    public boolean isDetermination_val() {
        return determination_val;
    }

    public void setDetermination_val(boolean determination_val) {
        this.determination_val = determination_val;
    }

    public boolean isReinspection_ctl() {
        return reinspection_ctl;
    }

    public void setReinspection_ctl(boolean reinspection_ctl) {
        this.reinspection_ctl = reinspection_ctl;
    }

    public boolean isReinspection_val() {
        return reinspection_val;
    }

    public void setReinspection_val(boolean reinspection_val) {
        this.reinspection_val = reinspection_val;
    }

    public boolean isDt_qualifies_ctl() {
        return dt_qualifies_ctl;
    }

    public void setDt_qualifies_ctl(boolean dt_qualifies_ctl) {
        this.dt_qualifies_ctl = dt_qualifies_ctl;
    }

    public boolean isDt_qualifies_val() {
        return dt_qualifies_val;
    }

    public void setDt_qualifies_val(boolean dt_qualifies_val) {
        this.dt_qualifies_val = dt_qualifies_val;
    }

    /**
     * @return the domain_val
     */
    public EventRealm getDomain_val() {
        return domain_val;
    }

    /**
     * @param domain_val the domain_val to set
     */
    public void setDomain_val(EventRealm domain_val) {
        this.domain_val = domain_val;
    }

    /**
     * @return the domain_ctl
     */
    public boolean isDomain_ctl() {
        return domain_ctl;
    }

    /**
     * @param domain_ctl the domain_ctl to set
     */
    public void setDomain_ctl(boolean domain_ctl) {
        this.domain_ctl = domain_ctl;
    }

    /**
     * @return the fieldRoutingStatusRouted_val
     */
    public boolean isFieldRoutingStatusRouted_val() {
        return fieldRoutingStatusRouted_val;
    }

    /**
     * @param fieldRoutingStatusRouted_val the fieldRoutingStatusRouted_val to set
     */
    public void setFieldRoutingStatusRouted_val(boolean fieldRoutingStatusRouted_val) {
        this.fieldRoutingStatusRouted_val = fieldRoutingStatusRouted_val;
    }

    /**
     * @return the fieldRoutingStatus_ctl
     */
    public boolean isFieldRoutingStatus_ctl() {
        return fieldRoutingStatus_ctl;
    }

    /**
     * @param fieldRoutingStatus_ctl the fieldRoutingStatus_ctl to set
     */
    public void setFieldRoutingStatus_ctl(boolean fieldRoutingStatus_ctl) {
        this.fieldRoutingStatus_ctl = fieldRoutingStatus_ctl;
    }

    /**
     * @return the fieldInitMuniOccPeriod_val
     */
    public boolean isFieldInitMuniOccPeriod_val() {
        return fieldInitMuniOccPeriod_val;
    }

    /**
     * @param fieldInitMuniOccPeriod_val the fieldInitMuniOccPeriod_val to set
     */
    public void setFieldInitMuniOccPeriod_val(boolean fieldInitMuniOccPeriod_val) {
        this.fieldInitMuniOccPeriod_val = fieldInitMuniOccPeriod_val;
    }

    /**
     * @return the fieldInit_ctl
     */
    public boolean isFieldInit_ctl() {
        return fieldInit_ctl;
    }

    /**
     * @param fieldInit_ctl the fieldInit_ctl to set
     */
    public void setFieldInit_ctl(boolean fieldInit_ctl) {
        this.fieldInit_ctl = fieldInit_ctl;
    }

    /**
     * @return the fieldDispatch_ctl
     */
    public boolean isFieldDispatch_ctl() {
        return fieldDispatch_ctl;
    }

    /**
     * @param fieldDispatch_ctl the fieldDispatch_ctl to set
     */
    public void setFieldDispatch_ctl(boolean fieldDispatch_ctl) {
        this.fieldDispatch_ctl = fieldDispatch_ctl;
    }

    /**
     * @return the fieldDispatchCycleComplete_val
     */
    public boolean isFieldDispatchCycleComplete_val() {
        return fieldDispatchCycleComplete_val;
    }

    /**
     * @param fieldDispatchCycleComplete_val the fieldDispatchCycleComplete_val to set
     */
    public void setFieldDispatchCycleComplete_val(boolean fieldDispatchCycleComplete_val) {
        this.fieldDispatchCycleComplete_val = fieldDispatchCycleComplete_val;
    }

    /**
     * @return the fieldInitNonNullLocDescr_val
     */
    public boolean isFieldInitNonNullLocDescr_val() {
        return fieldInitNonNullLocDescr_val;
    }

    /**
     * @param fieldInitNonNullLocDescr_val the fieldInitNonNullLocDescr_val to set
     */
    public void setFieldInitNonNullLocDescr_val(boolean fieldInitNonNullLocDescr_val) {
        this.fieldInitNonNullLocDescr_val = fieldInitNonNullLocDescr_val;
    }

    /**
     * @return the fieldInitMuniOccPeriodID
     */
    public int getFieldInitMuniOccPeriodID() {
        return fieldInitMuniOccPeriodID;
    }

    /**
     * @param fieldInitMuniOccPeriodID the fieldInitMuniOccPeriodID to set
     */
    public void setFieldInitMuniOccPeriodID(int fieldInitMuniOccPeriodID) {
        this.fieldInitMuniOccPeriodID = fieldInitMuniOccPeriodID;
    }

    /**
     * @return the fieldDispatchRetrievedByMobile_val
     */
    public boolean isFieldDispatchRetrievedByMobile_val() {
        return fieldDispatchRetrievedByMobile_val;
    }

    /**
     * @param fieldDispatchRetrievedByMobile_val the fieldDispatchRetrievedByMobile_val to set
     */
    public void setFieldDispatchRetrievedByMobile_val(boolean fieldDispatchRetrievedByMobile_val) {
        this.fieldDispatchRetrievedByMobile_val = fieldDispatchRetrievedByMobile_val;
    }

    /**
     * @return the fieldDispatchRetrievedByMobile_ctl
     */
    public boolean isFieldDispatchRetrievedByMobile_ctl() {
        return fieldDispatchRetrievedByMobile_ctl;
    }

    /**
     * @param fieldDispatchRetrievedByMobile_ctl the fieldDispatchRetrievedByMobile_ctl to set
     */
    public void setFieldDispatchRetrievedByMobile_ctl(boolean fieldDispatchRetrievedByMobile_ctl) {
        this.fieldDispatchRetrievedByMobile_ctl = fieldDispatchRetrievedByMobile_ctl;
    }

    /**
     * @return the fieldDispatchCycleComplete_ctl
     */
    public boolean isFieldDispatchCycleComplete_ctl() {
        return fieldDispatchCycleComplete_ctl;
    }

    /**
     * @param fieldDispatchCycleComplete_ctl the fieldDispatchCycleComplete_ctl to set
     */
    public void setFieldDispatchCycleComplete_ctl(boolean fieldDispatchCycleComplete_ctl) {
        this.fieldDispatchCycleComplete_ctl = fieldDispatchCycleComplete_ctl;
    }

    

}
