package com.tcvcog.tcvce.entities.search;

import com.tcvcog.tcvce.entities.Credential;
import com.tcvcog.tcvce.entities.occupancy.FieldInspectionLight;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class QueryFieldInspection extends Query<FieldInspectionLight> {

    private QueryFieldInspectionEnum queryName;
    private List<SearchParamsFieldInspection> searchParamsList;
    private List<FieldInspectionLight> results;

    public QueryFieldInspection(QueryFieldInspectionEnum qName,
            List<SearchParamsFieldInspection> params,
            Credential c) {
        super(c);
        queryName = qName;
        searchParamsList = new ArrayList<>();
        if (params != null)
        {
            searchParamsList.addAll(params);
        }
        results = new ArrayList<>();
    }

    public void addToResults(List<FieldInspectionLight> list) {
        results.addAll(list);
    }

    @Override
    public List getBOBResultList() {
        return results;
    }

    @Override
    public void addBObListToResults(List l) {
        results = l;
    }

    public List<SearchParamsFieldInspection> getSearchParamsList() {
        return searchParamsList;
    }

    public void setSearchParamsList(List<SearchParamsFieldInspection> searchParamsList) {
        this.searchParamsList = searchParamsList;
    }

    public List<FieldInspectionLight> getResults() {
        return results;
    }

    public void setResults(List<FieldInspectionLight> results) {
        this.results = results;
    }

    public QueryFieldInspectionEnum getQueryName() {
        return queryName;
    }

    @Override
    public List<SearchParamsFieldInspection> getParamsList() {
        return searchParamsList;
    }

    @Override
    public SearchParamsFieldInspection getPrimaryParams() {
        if (searchParamsList != null && !searchParamsList.isEmpty())
        {
            return searchParamsList.stream().findFirst().orElse(null);
        }
        return null;
    }

    @Override
    public void addParams(SearchParams params) {
        if (params != null && params instanceof SearchParamsFieldInspection)
        {
            searchParamsList.add((SearchParamsFieldInspection) params);
        }
    }

    @Override
    public int getParamsListSize() {
        int size = 0;
        if (searchParamsList != null)
        {
            return searchParamsList.size();
        }
        return size;
    }

    @Override
    public String getQueryTitle() {
        return queryName.getTitle();
    }

    @Override
    public void clearResultList() {
        if (results != null)
        {
            results.clear();
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.searchParamsList);
        hash = 47 * hash + Objects.hashCode(this.results);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final QueryFieldInspection other = (QueryFieldInspection) obj;
        return this.queryName == other.queryName;
    }

}
