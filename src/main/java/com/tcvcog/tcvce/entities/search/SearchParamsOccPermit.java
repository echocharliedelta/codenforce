/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.entities.search;

import com.tcvcog.tcvce.entities.occupancy.OccPermitType;
import java.util.List;

/**
 *
 * @author sylvia
 */
public class        SearchParamsOccPermit 
        extends     SearchParams {
    
    public static final String MUNI_DBFIELD = "municipality.municode";
    
    private boolean permitTypeList_ctl;
    private List<OccPermitType> permitTypeList_val;
    
    private boolean draft_ctl;
    private boolean draft_val;
    
    private boolean nullified_ctl;
    private boolean nullified_val;
    
    private boolean referenceNo_ctl;
    private String referenceNo_val;
    
    
    
    public SearchParamsOccPermitDateFieldsEnum[] getDateFieldList(){
       SearchParamsOccPermitDateFieldsEnum[] fields = SearchParamsOccPermitDateFieldsEnum.values();
       return fields;
   }
   
   public SearchParamsOccPermitUserFieldsEnum[] getUserFieldList(){
       SearchParamsOccPermitUserFieldsEnum[] fields = SearchParamsOccPermitUserFieldsEnum.values();
       return fields;
   }

    /**
     * @return the permitTypeList_ctl
     */
    public boolean isPermitTypeList_ctl() {
        return permitTypeList_ctl;
    }

    /**
     * @return the permitTypeList_val
     */
    public List<OccPermitType> getPermitTypeList_val() {
        return permitTypeList_val;
    }

    /**
     * @param permitTypeList_ctl the permitTypeList_ctl to set
     */
    public void setPermitTypeList_ctl(boolean permitTypeList_ctl) {
        this.permitTypeList_ctl = permitTypeList_ctl;
    }

    /**
     * @param permitTypeList_val the permitTypeList_val to set
     */
    public void setPermitTypeList_val(List<OccPermitType> permitTypeList_val) {
        this.permitTypeList_val = permitTypeList_val;
    }

    /**
     * @return the draft_ctl
     */
    public boolean isDraft_ctl() {
        return draft_ctl;
    }

    /**
     * @return the draft_val
     */
    public boolean isDraft_val() {
        return draft_val;
    }

    /**
     * @param draft_ctl the draft_ctl to set
     */
    public void setDraft_ctl(boolean draft_ctl) {
        this.draft_ctl = draft_ctl;
    }

    /**
     * @param draft_val the draft_val to set
     */
    public void setDraft_val(boolean draft_val) {
        this.draft_val = draft_val;
    }

    /**
     * @return the nullified_ctl
     */
    public boolean isNullified_ctl() {
        return nullified_ctl;
    }

    /**
     * @param nullified_ctl the nullified_ctl to set
     */
    public void setNullified_ctl(boolean nullified_ctl) {
        this.nullified_ctl = nullified_ctl;
    }

    /**
     * @return the nullified_val
     */
    public boolean isNullified_val() {
        return nullified_val;
    }

    /**
     * @param nullified_val the nullified_val to set
     */
    public void setNullified_val(boolean nullified_val) {
        this.nullified_val = nullified_val;
    }

    /**
     * @return the referenceNo_ctl
     */
    public boolean isReferenceNo_ctl() {
        return referenceNo_ctl;
    }

    /**
     * @param referenceNo_ctl the referenceNo_ctl to set
     */
    public void setReferenceNo_ctl(boolean referenceNo_ctl) {
        this.referenceNo_ctl = referenceNo_ctl;
    }

    /**
     * @return the referenceNo_val
     */
    public String getReferenceNo_val() {
        return referenceNo_val;
    }

    /**
     * @param referenceNo_val the referenceNo_val to set
     */
    public void setReferenceNo_val(String referenceNo_val) {
        this.referenceNo_val = referenceNo_val;
    }
    
    
  
   
   
    
}
