package com.tcvcog.tcvce.entities.search;

import com.tcvcog.tcvce.entities.RoleType;

public enum QueryFieldInspectionEnum implements IFace_RankLowerBounded {
    NOTFINALIZED("All field inspections that have not been finalized",
            "All field inspections that have not been finalized Yet",
            RoleType.MuniReader,
            2,
            true,
            true,
            false),
    
    REINSPECTION("All field inspections which are reinspections",
            "All field inspections which are reinspections",
            RoleType.MuniReader,
            2,
            true,
            true,
            false),
    
    PAST7DAYS("Field inspections conducted during the past 7 days",
            "Field inspections conducted during the past 7 days",
            RoleType.MuniReader,
            2,
            true,
            true,
            false),
    
    PAST30DAYS("Field inspections conducted during the past 30 days",
            "Field inspections conducted during the past 30 days",
            RoleType.MuniReader,
            2,
            true,
            true,
            false),
    
    INSPECTOR("Field inspections conducted by a chosen inspector.",
            "Field inspections conducted by a chosen inspector.",
            RoleType.MuniReader,
            2,
            true,
            true,
            false),
    
    QUALIFIESPASS("Field inspections whose determination qualifies as a pass",
            "Field inspections whose determination qualifies as a pass",
            RoleType.MuniReader,
            2,
            true,
            true,
            false),
    
    QUALIFIESFAIL("Field inspections whose determination qualifies as a fail",
            "Field inspections whose determination qualifies as a fail",
            RoleType.MuniReader,
            2,
            true,
            true,
            false),
    
    OCC_REPORT_FIN_PASSORFAIL("Failed inspections in a date range",
            "Used by occ activity report",
            RoleType.MuniReader,
            2,
            false,
            true,
            false),
    
    FIELD_INITIATED_FINS_REQUIRING_ROUTING("Inspections initiated in the field (not dispatched) that need a home based on attachment to muni occ period",
            "Which are assigned by the app to the muni's default occ period and hence have not been routed yet",
            RoleType.MuniReader,
            2,
            false,
            true,
            false),
    
    
    FIELD_INITIATED_FINS_REQUIRING_ROUTING_NONNULL_LOC("Inspections initiated in the field (not dispatched) that need a home based on a non-null location descriptor",
            "Which are assigned by the app to the muni's default occ period and hence have not been routed yet",
            RoleType.MuniReader,
            2,
            false,
            true,
            false),
    
    FIELD_INITIATED_FINS_ROUTING_COMPLETE("Inspections initiated in the field that have been routed to a proper home",
            "Which have been assigned to an occ period or ce case",
            RoleType.MuniReader,
            2,
            false,
            true,
            false),
    
    DISPATCHED_FINS_NOT_SYNCHRONIZED("Inspections dispatched and awaiting completion in the field",
            "These are in the officer's field queue",
            RoleType.MuniReader,
            2,
            false,
            true,
            false),
    
    
    DISPATCHED_FINS_COMPLETED("Inspections dispatched to the field appplication which have been completed and synchronized back to server",
            "No further action required on these; informational query only",
            RoleType.MuniReader,
            2,
            false,
            true,
            false),
    
    
    RECENTLY_UPLOADED("Inspection uploaded from the mobile tool within the last week",
            "Includes dispached inspections only",
            RoleType.MuniReader,
            2,
            false,
            true,
            false),
    
    CUSTOM("Custom",
            "Custom",
            RoleType.MuniReader,
            4,
            true,
            true,
            true);

    private final String title;
    private final String desc;
    private final RoleType requiredRoleMin;
    private final int minRank;
    private final boolean log;
    private final boolean active;
    private final boolean showAllControls;

    private QueryFieldInspectionEnum(String t, String l, RoleType minRoleType, int rnk, boolean lg, boolean act, boolean show) {
        this.desc = l;
        this.title = t;
        if (minRoleType != null)
        {
            this.requiredRoleMin = minRoleType;
        } else
        {
            this.requiredRoleMin = RoleType.MuniStaff;
        }
        this.minRank = rnk;
        this.log = lg;
        this.active = act;
        this.showAllControls = show;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public RoleType getRequiredRoleMin() {
        return requiredRoleMin;
    }

    public boolean isLog() {
        return log;
    }

    public boolean isActive() {
        return active;
    }

    public boolean isShowAllControls() {
        return showAllControls;
    }

    public int getMinRank() {
        return minRank;
    }

}
