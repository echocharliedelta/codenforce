/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.entities.search;

import com.tcvcog.tcvce.entities.RoleType;

/**
 *
 * @author sylvia
 */
public  enum        QueryCEAREnum 
        implements  IFace_RankLowerBounded{
    
    UNPROCESSED(        "Needs review by officer", 
                        "Code enforcement action requests that have not been reviewed", 
                        RoleType.MuniReader, 
                        true,
                        true,
                        false),
    
    ATTACHED_TO_CECASE( "Assigned to a case", 
                        "Code enforcement action requests from the past 30 days that was either used to create a new case or was attached to an existing code enforcement case", 
                        RoleType.MuniReader, 
                        true,
                        true,
                        false),
    
    PACC(               "Query by public access control code (PACC)", 
                        "Return only requests with matching public access control codes", 
                        RoleType.MuniReader, 
                        false,
                        true,
                        false),
    
    ISSUE_TYPE(         "Query by the concern issue type selected on input form", 
                        "Return only requests which match the selected issue type", 
                        RoleType.MuniReader, 
                        false,
                        true,
                        false),
    
    STATUS(             "Query by the request status", 
                        "Return only requests which match the selected CEAR status", 
                        RoleType.MuniReader, 
                        false,
                        true,
                        false),
    
    
    NON_ADDRESSABLE(    "Requests whose location marked as non-addressable", 
                        "Return only requests which were marked as not existing at a specific property, or that the property of concern could not be found using the form's search tool.", 
                        RoleType.MuniReader, 
                        false,
                        true,
                        false),
    
    URGENT(             "Requests concerning immediate human health and safety", 
                        "Return only requests which were marked as urgent from a human safety standpoint.", 
                        RoleType.MuniReader, 
                        false,
                        true,
                        false),
    
    PROPERTY(           "Query by property", 
                        "Attached to a given property", 
                        RoleType.MuniReader, 
                        false,
                        true,
                        false),
    
    CUSTOM(             "Custom configuration", 
                        "Results based on the injected search parameters", 
                        RoleType.MuniReader, 
                        true,
                        true,
                        true);
    
    private final String title;
    private final String desc;
    private final RoleType requiredRoleMin;
    private final boolean log;
    private final boolean active;
    private final boolean showAllControls;
    
    private QueryCEAREnum(String t, String l, RoleType minRoleType, boolean lg, boolean act, boolean show){
        this.desc = l;
        this.title = t;
        if(minRoleType != null){
            this.requiredRoleMin = minRoleType;
        } else {
            this.requiredRoleMin = RoleType.MuniStaff;
        }
        this.log = lg;
        this.active = act;
        this.showAllControls = show;
    }
    
    public String getDesc(){
        return desc;
    }
    
    public String getTitle(){
        return title;
    }

    public boolean logQueryRun(){
        return log;
    }

    @Override
    public RoleType getRequiredRoleMin() {
        return requiredRoleMin;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @return the showAllControls
     */
    public boolean isShowAllControls() {
        return showAllControls;
    }
    
    
}
