/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.entities.search;

import com.tcvcog.tcvce.entities.RoleType;

/**
 * The home of all pre-built queries against the codeviolation table
 * @author Ellen Bascomb of Apartment 31Y
 */
public  enum QueryCodeViolationEnum 
        implements IFace_RankLowerBounded{
    
   
    MUNI_ALL(               "All violations in all in current muni",
                            "Including violations with compliance achieved",
                            RoleType.MuniReader,
                            true,
                            false,
                            true),
    
    LOGGED_PAST30_NOV_CITMAYBE    ("Logged on any case with NOV in past 30 days",
                            "These violations may have have been cited",
                            RoleType.MuniReader,
                            true,
                            false,
                            true),
    
    
    LOGGED_PAST7_NOV_CITMAYBE ("Logged on any case with NOV in past 7 days",
                            "These violations may have been cited",
                            RoleType.MuniReader,
                            true,
                            false,
                            true),
    
    
    VIOLATIONS_INSIDE_COMPLIANCE_WINDOW   ("Currently within compliance window",
                            "Violations without an actual compliance date and whose stipulated compliance date is AFTER the moment the search is executed.",
                            RoleType.MuniReader,
                            true,
                            false,
                            true),
    
    COMPLIANCE_WINDOW_EXPIRED_NOTCITED  ("Expired compliance timeframe but not cited",
                            "Violations whose stipulated compliance date is BEFORE the moment the search is executed AND which are not linked to any citaiton object; These are violations that require officer attention: cite or communicate with owner and update stipulated compliance dates",
                            RoleType.MuniReader,
                            true,
                            false,
                            true),
    
    COMP_PAST7              ("Compliance achieved in past 7 days",
                            "Actual compliance date is within the past 7 calendar days",
                            RoleType.MuniReader,
                            true,
                            false,
                            true),
    
    COMP_PAST30             ("Compliance achieved in past 30 days",
                            "Actual compliance date is within the past 30 calendar days",
                            RoleType.MuniReader,
                            true,
                            false,
                            true),
    
    STIP_NEXT7              ("Compliance stipulated in upcoming 7 days",
                            "Regardless of ciation status, but these should, in theory, not be cited",
                            RoleType.MuniReader,
                            true,
                            false,
                            true),
    
    STIP_NEXT30             ("Compliance stipulated in upcoming 30 days",
                            "Regardless of ciation status, but these should, in theory, not be cited",
                            RoleType.MuniReader,
                            true,
                            false,
                            true),
    
    CITED_PAST7            ("Cited in the past 7 days",
                            "Includes violations for which compliance has been achieved",
                            RoleType.MuniReader,
                            true,
                            false,
                            true),
    
    CITED_PAST30            ("Cited in past 30 days",
                            "Includes violations for which compliance has been achieved",
                            RoleType.MuniReader,
                            true,
                            false,
                            true),
    
    NOCOMP_CITED_ANYTIME   ("Violation without compliance (or nullification or deactivation or transfer) but citation",
                            "These are violations inside the court system for which compliance has NOT been achieved",
                            RoleType.MuniReader,
                            true,
                            false,
                            true),
    
    LOGGED_IN_DATE_RANGE      ("Attached to a case during date range",
                            "Regardless of NOV and Citation status or compliance status",
                            RoleType.MuniReader,
                            true,
                            false,
                            true),
    
    LOGGED_NO_NOV_EVER      ("Attached to a case but not included in a notice EVER",
                            "These are violations that require immediate officer attention: a notice of violation letter should be created and sent to property owner",
                            RoleType.MuniReader,
                            true,
                            false,
                            true),
    
    LOGGED_CITED_NONOV      ("AUDIT: Attached to a case and cited without a notice",
                            "Violations should not be cited unless property owners have been given proper notice, so this query is for auditing purposes to find improperly cited violations",
                            RoleType.MuniReader,
                            true,
                            false,
                            true),
    
    ALL_OUTSTANDING         ("All Unresolved Violations",
                            "Any violation without an actual compliance date, regardless of citation or NOV status",
                            RoleType.MuniReader,
                            true,
                            false,
                            true),
    
    NULLIFIED                ("All nullified violations",
                              "Any code violation which has a nullified timestamp",
                              RoleType.MuniReader,
                              true,
                              false,
                              true),
    
    TRANSFERRRED             ("All transferred violations",
                              "Any code violation which has a transfer timestamp",
                              RoleType.MuniReader,
                              true,
                              false,
                              true),
    
    CUSTOM                   ( "Custom", 
                               "Custom", 
                               RoleType.MuniReader,
                               true,
                               true,
                               true);
    
    
    private final String title;
    private final String desc;
    private final RoleType requiredRoleMin;
    private final boolean log;
    private final boolean showAllControls;
    private final boolean active; 
    
    private QueryCodeViolationEnum(String t, String l, RoleType minRoleType, boolean lg, boolean showall, boolean act){
        this.desc = l;
        this.title = t;
        this.log = lg;
        if(minRoleType != null){
            this.requiredRoleMin = minRoleType;
        } else {
            this.requiredRoleMin = RoleType.MuniReader;
        }
        this.showAllControls = showall;
        this.active = act;
    }
    
    public String getDesc(){
        return desc;
    }
    
    public String getTitle(){
        return title;
    }

    
    public boolean logQueryRun(){
        return log;
    }

    @Override
    public RoleType getRequiredRoleMin() {
        return requiredRoleMin;
    }

    public boolean isShowAllControls() {
        return showAllControls;
    }

    public boolean isActive() {
        return active;
    }
    
}
