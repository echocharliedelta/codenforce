/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.entities.search;

import com.tcvcog.tcvce.entities.RoleType;

/**
 *
 * @author sylvia
 */
public  enum        QueryPersonEnum 
        implements  IFace_RankLowerBounded{
    
    PERSON_NAME(        "Name",
                        "Searches through name field only; case insensitive",
                        RoleType.MuniReader,
                        true,
                        false,
                        true),
    
    PHONE(              "Phone",
                        "Searches through phone number field only; case insensitive",
                        RoleType.MuniReader,
                        true,
                        false,
                        false),
    
    EMAIL(              "Email address",
                        "Searches through email field only; case insensitive",
                        RoleType.MuniReader,
                        true,
                        false,
                        false),
    
    JOB_TITLE (         "Job title",
                        "Searches through job title field only; case insensitive",
                        RoleType.MuniReader,
                        true,
                        false,
                        false),
    
    MINORS_ONLY(        "All persons under 18",
                        "Searches for all persons marked as under 18",
                        RoleType.MuniReader,
                        true,
                        false,
                        false),
    
    
    BUSINESSES(        "All businesses",
                        "Returns all records flagged as a business entity",
                        RoleType.MuniReader,
                        true,
                        false,
                        true),
    
    
    MULIT_HUMANS(        "All Multi-humans",
                        "Returns all records flagged as representing more than one human",
                        RoleType.MuniReader,
                        true,
                        false,
                        true),
    
    HUMAN_ID(           "Search by person ID",
                        "Searches for persons whose ID contains the given number sequence",
                        RoleType.MuniReader,
                        true,
                        false,
                        true),
    
    MERGED(             "Merged person - configure in advanced",
                        "Activates merged record searching tools",
                        RoleType.MuniReader,
                        true,
                        false,
                        true),
    
    CUSTOM(             "Custom", 
                        "Custom", 
                        RoleType.MuniReader,
                        true,
                        true,
                        true);
    
    private final String title;
    private final String desc;
    private final RoleType requiredRoleMin;
    private final boolean log;
    private final boolean showAllControls;
    private final boolean active;
    
    private QueryPersonEnum(String t, String l, RoleType minRoleType, boolean lg, boolean showAll, boolean act){
        
        this.title = t;
        this.desc = l;
        if(minRoleType != null){
            this.requiredRoleMin = minRoleType;
        } else {
            this.requiredRoleMin = RoleType.MuniStaff;
        }
        this.log = lg;
        this.showAllControls = showAll;
        this.active = act;
    }
    
    public String getDesc(){
        return desc;
    }
    
    public String getTitle(){
        return title;
    }

    
    
    public boolean logQueryRun(){
        return log;
    }

    /**
     * @return the requiredRoleMin
     */
    @Override
    public RoleType getRequiredRoleMin() {
        return requiredRoleMin;
    }

    /**
     * @return the showAllControls
     */
    public boolean isShowAllControls() {
        return showAllControls;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }
    
    
}