package com.tcvcog.tcvce.entities.search;

public enum SearchParamsFieldInspectionUserFieldsEnum implements IFace_userFieldHolder {
    CREATOR("Logged in case by",
            "occinspection.createdby_userid"),
    
    LAST_UPDATOR("Last updated by",
                "occinspection.lastupdatedby_userid"),
    
    DEACTIVATOR("Deactivated by",
                "occinspection.deactivatedby_userid"),
    
    INSPECTOR(  "Inspected by",
                "occinspection.inspector_userid"),
    
    DISPATCHED_BY   (   "Dispatching user",
                        "occinspectiondispatch.createdby_userid"),
    
    DISPATCH_RETRIEVED_BY   (   "Dispatch retrieved by",
                                "occinspectiondispatch.retrievedby_userid"),
    
    DISPATCHED_DEAC_BY   (  "Dispatch deactiated by",
                            "occinspectiondispatch.deactivatedby_userid");
    
    private final String title;
    private final String dbField;

    private SearchParamsFieldInspectionUserFieldsEnum(String t, String db) {
        this.title = t;
        this.dbField = db;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String extractUserFieldString() {
        return dbField;
    }
    
    
    

}
