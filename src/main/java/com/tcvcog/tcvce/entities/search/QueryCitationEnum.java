package com.tcvcog.tcvce.entities.search;

import com.tcvcog.tcvce.entities.RoleType;

public enum QueryCitationEnum implements IFace_RankLowerBounded {
    CITATTIONID("Query by Citation ID",
            "All citations by ID",
            RoleType.MuniReader,
            false,
            false),
    
    CITATION_NUMBER("Query by citation number",
            "Created by officer when filing",
            RoleType.MuniReader,
            false,
            false),
    
    DOCKETNO("Query by Citation Docket Number",
            "All citations by docket number",
            RoleType.MuniReader,
            false,
            false),
    LINKSTATUS("Query by CitationCodeViolationLink Status",
            "All citations by codeViolation Link Status",
            RoleType.MuniReader,
            false,
            false),
    LOGENTRY("Query by StatusLogEntry Status",
            "All citations by status log entry",
            RoleType.MuniReader,
            false,
            false),
    ORDINANCE("Query by Ordinance",
            "All citations by Ordinance",
            RoleType.MuniReader,
            false,
            false),
    CUSTOM("Custom",
            "All custom citations",
            RoleType.MuniReader,
            false,
            true);

    private final String title;
    private final String desc;
    private final RoleType requiredRoleMin;
    private final boolean log;
    private final boolean showAllControls;

    private QueryCitationEnum(String title, String desc, RoleType requiredRoleMin, boolean log, boolean showAllControls) {
        this.title = title;
        this.desc = desc;
        if (requiredRoleMin != null)
        {
            this.requiredRoleMin = requiredRoleMin;
        } else
        {
            this.requiredRoleMin = RoleType.MuniStaff;
        }
        this.log = log;
        this.showAllControls = showAllControls;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    @Override
    public RoleType getRequiredRoleMin() {
        return requiredRoleMin;
    }

    public boolean isLog() {
        return log;
    }

    public boolean isShowAllControls() {
        return showAllControls;
    }

}
