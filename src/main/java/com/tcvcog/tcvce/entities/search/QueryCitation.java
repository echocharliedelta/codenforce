package com.tcvcog.tcvce.entities.search;

import com.tcvcog.tcvce.entities.CitationCECasePropertyHeavy;
import com.tcvcog.tcvce.entities.Credential;
import java.util.ArrayList;
import java.util.List;

public class QueryCitation extends Query {

    private QueryCitationEnum queryName;
    private List<SearchParamsCitation> searchParamsList;
    private List<CitationCECasePropertyHeavy> results;

    public QueryCitation(QueryCitationEnum qName, List<SearchParamsCitation> params, Credential c) {
        super(c);
        searchParamsList = new ArrayList<>();
        if (params != null)
        {
            searchParamsList.addAll(params);
        }
        queryName = qName;
        results = new ArrayList<>();
    }

    @Override
    public List<CitationCECasePropertyHeavy> getBOBResultList() {
        return results;
    }

    @Override
    public void addBObListToResults(List l) {
        results = l;
    }

    @Override
    public List<SearchParamsCitation> getParamsList() {
        return searchParamsList;
    }

    @Override
    public SearchParamsCitation getPrimaryParams() {
        if (searchParamsList != null && !searchParamsList.isEmpty())
        {
            return searchParamsList.stream().findFirst().orElse(null);
        }
        return null;
    }

    @Override
    public void addParams(SearchParams params) {
        if (params instanceof SearchParamsCitation)
        {
            searchParamsList.add((SearchParamsCitation) params);
        }
    }

    @Override
    public int getParamsListSize() {
        return searchParamsList.size();
    }

    @Override
    public String getQueryTitle() {
        return queryName.getTitle();
    }

    @Override
    public void clearResultList() {
        if (results != null)
        {
            results.clear();
        }
    }

    public QueryCitationEnum getQueryName() {
        return queryName;
    }

    public void setQueryName(QueryCitationEnum queryName) {
        this.queryName = queryName;
    }

    public void addToResults(List<CitationCECasePropertyHeavy> citations) {
        results.addAll(citations);
    }

}
