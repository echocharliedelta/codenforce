/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.entities.search;

import com.tcvcog.tcvce.entities.RoleType;

/**
 *
 * @author sylvia
 */
public  enum     QueryOccPermitEnum 
        implements IFace_RankLowerBounded{
    
    
    
    FINALIZED_PAST_30DAYS(  "Permits issued in the past 30 days", 
                            "Not including those nullified after finalization", 
                            RoleType.MuniReader, 
                            false,
                            false),
    
    ALL_FINALIZED(          "All finalized permits", 
                            "Not including those nullified after finalization", 
                            RoleType.MuniReader, 
                            false,
                            false),
    
    NOT_FINALIZED(          "All unfinalized (draft) permits", 
                            "No reference number assigned", 
                            RoleType.MuniReader, 
                            false,
                            false),
    
    REFERENCE_NUMBER(       "Search by assigned reference number", 
                            "Assigned at finalization", 
                            RoleType.MuniReader, 
                            false,
                            false),
    
    ONLY_NULLIFIED(         "Nullified permits any time period",
                            "Regardless of user or file status",
                            RoleType.MuniReader,
                            false,
                            false),
    
    PERMIT_BY_TYPE(         "Permits by type",
                            "Select type(s)",
                            RoleType.MuniReader,
                            false,
                            false),
    
    
    CUSTOM(                 "Custom", 
                            "Custom", 
                            RoleType.MuniReader, 
                            false,
                            true);
    
    private final String title;
    private final String desc;
    private final RoleType requiredRoleMin;
    private final boolean log;
    private final boolean showAllControls;
    
    private QueryOccPermitEnum(String t, String l, RoleType minRoleType, boolean lg, boolean sa){
        this.desc = l;
        this.title = t;
         if(minRoleType != null){
            this.requiredRoleMin = minRoleType;
        } else {
            this.requiredRoleMin = RoleType.MuniStaff;
        }
        this.log = lg;
        this.showAllControls = sa;
    }
    
    public String getDesc(){
        return desc;
    }
    
    public String getTitle(){
        return title;
    }

    
    public boolean logQueryRun(){
        return log;
    }

    @Override
    public RoleType getRequiredRoleMin() {
        return requiredRoleMin;
    }
    
    
}
