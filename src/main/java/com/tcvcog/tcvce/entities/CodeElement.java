/*
 * Copyright (C) 2017 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.application.interfaces.IFace_updateAuditable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * The root class of the Code Element family!
 * @author ellen bascomb of apt 31y
 */
public class CodeElement
        extends TrackedEntity
        implements Comparable<Object>, IFace_updateAuditable<CodeElementFieldsEnum> {

    final static String CODE_ELEMENT_TABLE_NAME = "public.codeelement";
    final static String CODE_ELEMENT_PKFIELD = "elementid";
    
    
    protected int elementID;

    protected CodeElementGuideEntry guideEntry;
    protected int guideEntryID;
    protected CodeSource source;
    
    /**
     * Java side only to reveal logic block choice when
     * generating ordinance header string
     */
    protected boolean pureIRCFOrmat;
    
    protected int ordchapterNo;
    
    protected String ordchapterTitle;
    protected String ordSecNum;
    protected String ordSecTitle;
    
    protected String ordSubSecNum;
    protected String ordSubSecTitle;
    
    protected String ordSubSubSecNum;
    protected String ordSubSubSecTitle;
    
    protected String ordTechnicalText;
    
    protected String ordHumanFriendlyText;
    protected boolean active;
    protected boolean usingInjectedValues;
    
    protected String resourceURL;
    
    protected String notes;
    protected int legacyID;
    
    protected String headerString;
    protected String nonIrcStyleHeaderString;
    protected String headerStringLog;
    
    protected String headerStringCandidate;
    protected String headerStringStatic;

    /** Humanization Object standard fields **/
   

    public CodeElement() {}

    /**
     * Clone like constructor
     * @param codeElement 
     */
    public CodeElement(CodeElement codeElement) {
        if(codeElement != null){

            this.elementID = codeElement.getElementID();
            this.guideEntry = codeElement.getGuideEntry();
            this.guideEntryID = codeElement.getGuideEntryID();
            this.source = codeElement.getSource();
            this.ordchapterNo = codeElement.getOrdchapterNo();
            this.ordchapterTitle = codeElement.getOrdchapterTitle();
            this.ordSecNum = codeElement.getOrdSecNum();
            this.ordSecTitle = codeElement.getOrdSecTitle();
            this.ordSubSecNum = codeElement.getOrdSubSecNum();
            this.ordSubSecTitle = codeElement.getOrdSubSecTitle();
            this.ordSubSubSecNum = codeElement.getOrdSubSubSecNum();
            this.ordSubSubSecTitle = codeElement.getOrdSubSubSecTitle();
            this.ordTechnicalText = codeElement.getOrdTechnicalText();
            this.ordHumanFriendlyText = codeElement.getOrdHumanFriendlyText();
            this.active = codeElement.isActive();
            this.usingInjectedValues = codeElement.isUsingInjectedValues();
            this.resourceURL = codeElement.getResourceURL();
            this.notes = codeElement.getNotes();
            this.legacyID = codeElement.getLegacyID();
            this.headerString = codeElement.getHeaderString();
            this.nonIrcStyleHeaderString = codeElement.getNonIrcStyleHeaderString();
            this.headerStringLog = codeElement.getHeaderStringLog();
            
            this.createdTS = codeElement.getCreatedTS();
            this.createdBy = codeElement.getCreatedBy();
            this.lastUpdatedTS = codeElement.getLastUpdatedTS();
            this.lastUpdatedBy = codeElement.getLastUpdatedBy();
            this.deactivatedTS = codeElement.getOseDeactivatedTS();
            this.deactivatedBy = codeElement.getDeactivatedBy();
            
            this.pureIRCFOrmat = codeElement.isPureIRCFOrmat();
            
            this.headerStringStatic = codeElement.getHeaderStringStatic();
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + this.elementID;
        hash = 67 * hash + Objects.hashCode(this.guideEntry);
        hash = 67 * hash + this.guideEntryID;
        hash = 67 * hash + Objects.hashCode(this.source);
        hash = 67 * hash + (this.pureIRCFOrmat ? 1 : 0);
        hash = 67 * hash + this.ordchapterNo;
        hash = 67 * hash + Objects.hashCode(this.ordchapterTitle);
        hash = 67 * hash + Objects.hashCode(this.ordSecNum);
        hash = 67 * hash + Objects.hashCode(this.ordSecTitle);
        hash = 67 * hash + Objects.hashCode(this.ordSubSecNum);
        hash = 67 * hash + Objects.hashCode(this.ordSubSecTitle);
        hash = 67 * hash + Objects.hashCode(this.ordSubSubSecNum);
        hash = 67 * hash + Objects.hashCode(this.ordSubSubSecTitle);
        hash = 67 * hash + Objects.hashCode(this.ordTechnicalText);
        hash = 67 * hash + Objects.hashCode(this.ordHumanFriendlyText);
        hash = 67 * hash + (this.active ? 1 : 0);
        hash = 67 * hash + (this.usingInjectedValues ? 1 : 0);
        hash = 67 * hash + Objects.hashCode(this.resourceURL);
        hash = 67 * hash + Objects.hashCode(this.notes);
        hash = 67 * hash + this.legacyID;
        hash = 67 * hash + Objects.hashCode(this.headerString);
        hash = 67 * hash + Objects.hashCode(this.nonIrcStyleHeaderString);
        hash = 67 * hash + Objects.hashCode(this.headerStringLog);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CodeElement other = (CodeElement) obj;
        return this.elementID == other.elementID;
    }
    
   
    /**
     * Compares based on chapter no, then sec no, then subsec no, then subsub sec no
     * @param o
     * @return 
     */
    @Override
    public int compareTo(Object o) {
        if(o == null){
            throw new NullPointerException("Cannot compare myself to a null");
        }
        if(!(o instanceof CodeElement)){
            throw new ClassCastException("Cannot cast given object to CodeElement");
        }
        CodeElement inel = (CodeElement) o;
        
        int compChapterNo = 0;
        
        if(this.ordchapterNo < inel.ordchapterNo){
            compChapterNo = -1;
        } else if(this.ordchapterNo > inel.ordchapterNo){
            compChapterNo = 1;
        }
        
        // Same chapter, so now we have to use sec number
        if(compChapterNo != 0){
            return compChapterNo;
        } else {
            if(ordSecNum != null && inel.ordSecNum != null){
                int compSecNo = this.ordSecNum.compareTo(inel.ordSecNum);
                if(compSecNo != 0){
                    return compSecNo;
                } else{
                    if(ordSubSecNum != null && inel.ordSubSecNum != null){
                        int compSubSecNo = this.ordSubSecNum.compareTo((inel.ordSubSecNum));
                        if(compSubSecNo != 0){
                            return compSubSecNo;
                        } else{
                            if(ordSubSubSecNum != null && inel.ordSubSubSecNum != null){
                                return this.ordSubSubSecNum.compareTo(inel.ordSubSubSecNum);
                            } else {
                                return compSubSecNo;
                            }
                        }
                    } else {
                        return compSecNo;
                    }
                }
            } else {
                return compChapterNo;
            }
        }
    }

    
    
     /**
      * SPECIAL getter that allows for reverse compatibility with
      * dyanmically generated headers
     * @return the headerString
     */
    public String getHeaderString() {
       
        if(headerStringStatic != null){
            return headerStringStatic;
        }
        return headerString;
    }


  
   

    /**
     * @return the deactivatedTS
     */
    public LocalDateTime getOseDeactivatedTS() {
        return deactivatedTS;
    }
   
    /**
     * @return the elementID
     */
    public int getElementID() {
        return elementID;
    }

    /**
     * @param elementID the elementID to set
     */
    public void setElementID(int elementID) {
        this.elementID = elementID;
    }

    /**
     * @return the ordchapterNo
     */
    public int getOrdchapterNo() {
        return ordchapterNo;
    }

    /**
     * @param ordchapterNo the ordchapterNo to set
     */
    public void setOrdchapterNo(int ordchapterNo) {
        this.ordchapterNo = ordchapterNo;
    }

    /**
     * @return the ordchapterTitle
     */
    public String getOrdchapterTitle() {
        return ordchapterTitle;
    }

    /**
     * @param ordchapterTitle the ordchapterTitle to set
     */
    public void setOrdchapterTitle(String ordchapterTitle) {
        this.ordchapterTitle = ordchapterTitle;
    }

    /**
     * @return the ordSecNum
     */
    public String getOrdSecNum() {
        return ordSecNum;
    }

    /**
     * @param ordSecNum the ordSecNum to set
     */
    public void setOrdSecNum(String ordSecNum) {
        this.ordSecNum = ordSecNum;
    }

    /**
     * @return the ordSecTitle
     */
    public String getOrdSecTitle() {
        return ordSecTitle;
    }

    /**
     * @param ordSecTitle the ordSecTitle to set
     */
    public void setOrdSecTitle(String ordSecTitle) {
        this.ordSecTitle = ordSecTitle;
    }

    /**
     * @return the ordSubSecNum
     */
    public String getOrdSubSecNum() {
        return ordSubSecNum;
    }

    /**
     * @param ordSubSecNum the ordSubSecNum to set
     */
    public void setOrdSubSecNum(String ordSubSecNum) {
        this.ordSubSecNum = ordSubSecNum;
    }

    /**
     * @return the ordSubSecTitle
     */
    public String getOrdSubSecTitle() {
        return ordSubSecTitle;
    }

    /**
     * @param ordSubSecTitle the ordSubSecTitle to set
     */
    public void setOrdSubSecTitle(String ordSubSecTitle) {
        this.ordSubSecTitle = ordSubSecTitle;
    }

    /**
     * @return the ordTechnicalText
     */
    public String getOrdTechnicalText() {
        return ordTechnicalText;
    }

    /**
     * @param ordTechnicalText the ordTechnicalText to set
     */
    public void setOrdTechnicalText(String ordTechnicalText) {
        this.ordTechnicalText = ordTechnicalText;
    }

    /**
     * @return the ordHumanFriendlyText
     */
    public String getOrdHumanFriendlyText() {
        return ordHumanFriendlyText;
    }

    /**
     * @param ordHumanFriendlyText the ordHumanFriendlyText to set
     */
    public void setOrdHumanFriendlyText(String ordHumanFriendlyText) {
        this.ordHumanFriendlyText = ordHumanFriendlyText;
    }

    
    /**
     * @return the isActive
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param isActive the isActive to set
     */
    public void setActive(boolean isActive) {
        this.active = isActive;
    }



    /**
     * @return the resourceURL
     */
    public String getResourceURL() {
        return resourceURL;
    }

    /**
     * @param resourceURL the resourceURL to set
     */
    public void setResourceURL(String resourceURL) {
        this.resourceURL = resourceURL;
    }

   
  

    /**
     * @return the source
     */
    public CodeSource getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(CodeSource source) {
        this.source = source;
    }

    /**
     * @return the guideEntry
     */
    public CodeElementGuideEntry getGuideEntry() {
        return guideEntry;
    }

    /**
     * @param guideEntry the guideEntry to set
     */
    public void setGuideEntry(CodeElementGuideEntry guideEntry) {
        this.guideEntry = guideEntry;
    }

    /**
     * @return the guideEntryID
     */
    public int getGuideEntryID() {
        return guideEntryID;
    }

    /**
     * @param guideEntryID the guideEntryID to set
     */
    public void setGuideEntryID(int guideEntryID) {
        this.guideEntryID = guideEntryID;
    }

   
    /**
     * @param headerString the headerString to set
     */
    public void setHeaderString(String headerString) {
        this.headerString = headerString;
    }

    /**
     * @return the ordSubSubSecNum
     */
    public String getOrdSubSubSecNum() {
        return ordSubSubSecNum;
    }

    /**
     * @param ordSubSubSecNum the ordSubSubSecNum to set
     */
    public void setOrdSubSubSecNum(String ordSubSubSecNum) {
        this.ordSubSubSecNum = ordSubSubSecNum;
    }

    /**
     * @return the useInjectedValues
     */
    public boolean isUsingInjectedValues() {
        return usingInjectedValues;
    }

    /**
     * @param usingInjectedValues the useInjectedValues to set
     */
    public void setUsingInjectedValues(boolean usingInjectedValues) {
        this.usingInjectedValues = usingInjectedValues;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return the legacyID
     */
    public int getLegacyID() {
        return legacyID;
    }

    /**
     * @param legacyID the legacyID to set
     */
    public void setLegacyID(int legacyID) {
        this.legacyID = legacyID;
    }

    /**
     * @return the ordSubSubSecTitle
     */
    public String getOrdSubSubSecTitle() {
        return ordSubSubSecTitle;
    }

    /**
     * @param ordSubSubSecTitle the ordSubSubSecTitle to set
     */
    public void setOrdSubSubSecTitle(String ordSubSubSecTitle) {
        this.ordSubSubSecTitle = ordSubSubSecTitle;
    }

    /**
     * @return the pureIRCFOrmat
     */
    public boolean isPureIRCFOrmat() {
        return pureIRCFOrmat;
    }

    /**
     * @param pureIRCFOrmat the pureIRCFOrmat to set
     */
    public void setPureIRCFOrmat(boolean pureIRCFOrmat) {
        this.pureIRCFOrmat = pureIRCFOrmat;
    }

    /**
     * @return nonIrcStyleHeaderString
     */
    public String getNonIrcStyleHeaderString() {
        return nonIrcStyleHeaderString;
    }

    /**
     * @param nonIrcStyleHeaderString the nonIrcStyleHeaderString to set
     */
    public void setNonIrcStyleHeaderString(String nonIrcStyleHeaderString) {
        this.nonIrcStyleHeaderString = nonIrcStyleHeaderString;
    }

    @Override
    public String getPKFieldName() {
        return CODE_ELEMENT_PKFIELD;
    }

    @Override
    public int getDBKey() {
        return elementID;
    }

    @Override
    public String getDBTableName() {
        return CODE_ELEMENT_TABLE_NAME;
    }

    /**
     * @return the headerStringLog
     */
    public String getHeaderStringLog() {
        return headerStringLog;
    }

    /**
     * @param headerStringLog the headerStringLog to set
     */
    public void setHeaderStringLog(String headerStringLog) {
        this.headerStringLog = headerStringLog;
    }

    @Override
    public Class<CodeElementFieldsEnum> getFieldDumpEnum() {
        return CodeElementFieldsEnum.class;
    }

    /**
     * @return the headerStringStatic
     */
    public String getHeaderStringStatic() {
        return headerStringStatic;
    }

    /**
     * @param headerStringStatic the headerStringStatic to set
     */
    public void setHeaderStringStatic(String headerStringStatic) {
        this.headerStringStatic = headerStringStatic;
    }

    /**
     * @return the headerStringCandidate
     */
    public String getHeaderStringCandidate() {
        return headerStringCandidate;
    }

    /**
     * @param headerStringCandidate the headerStringCandidate to set
     */
    public void setHeaderStringCandidate(String headerStringCandidate) {
        this.headerStringCandidate = headerStringCandidate;
    }

}
