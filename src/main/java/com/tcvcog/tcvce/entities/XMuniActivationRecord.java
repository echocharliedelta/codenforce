/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import java.time.LocalDateTime;

/**
 * Maps a record in the xmuniactivation table to record start and stop of 
 * xmuni mode
 * @author Ellen bascomb of apartment 31Y
 */
public class XMuniActivationRecord {
    private int activationID;
    private Municipality xMuni;
    private Human humanConduit;
    private LinkedObjectRole linkConduitRole;
    private int parentKey;
    private LocalDateTime activationTS;
    private UserMuniAuthPeriod activatorUMAP;
    private LocalDateTime exitTS;
    
    private HumanLinkParentObjectInfoHeavy humanLinkConduit;
    
    /**
     * @return the exitTS
     */
    public LocalDateTime getExitTS() {
        return exitTS;
    }

    /**
     * @param exitTS the exitTS to set
     */
    public void setExitTS(LocalDateTime exitTS) {
        this.exitTS = exitTS;
    }

    /**
     * @return the activationID
     */
    public int getActivationID() {
        return activationID;
    }

    /**
     * @return the xMuni
     */
    public Municipality getxMuni() {
        return xMuni;
    }

    /**
     * @return the humanConduit
     */
    public Human getHumanConduit() {
        return humanConduit;
    }

    /**
     * @return the linkConduitRole
     */
    public LinkedObjectRole getLinkConduitRole() {
        return linkConduitRole;
    }

    /**
     * @return the parentKey
     */
    public int getParentKey() {
        return parentKey;
    }

    /**
     * @return the activationTS
     */
    public LocalDateTime getActivationTS() {
        return activationTS;
    }

    /**
     * @return the activatorUMAP
     */
    public UserMuniAuthPeriod getActivatorUMAP() {
        return activatorUMAP;
    }

    /**
     * @param activationID the activationID to set
     */
    public void setActivationID(int activationID) {
        this.activationID = activationID;
    }

    /**
     * @param xMuni the xMuni to set
     */
    public void setxMuni(Municipality xMuni) {
        this.xMuni = xMuni;
    }

    /**
     * @param humanConduit the humanConduit to set
     */
    public void setHumanConduit(Human humanConduit) {
        this.humanConduit = humanConduit;
    }

    /**
     * @param linkConduitRole the linkConduitRole to set
     */
    public void setLinkConduitRole(LinkedObjectRole linkConduitRole) {
        this.linkConduitRole = linkConduitRole;
    }

    /**
     * @param parentKey the parentKey to set
     */
    public void setParentKey(int parentKey) {
        this.parentKey = parentKey;
    }

    /**
     * @param activationTS the activationTS to set
     */
    public void setActivationTS(LocalDateTime activationTS) {
        this.activationTS = activationTS;
    }

    /**
     * @param activatorUMAP the activatorUMAP to set
     */
    public void setActivatorUMAP(UserMuniAuthPeriod activatorUMAP) {
        this.activatorUMAP = activatorUMAP;
    }

    /**
     * @return the humanLinkConduit
     */
    public HumanLinkParentObjectInfoHeavy getHumanLinkConduit() {
        return humanLinkConduit;
    }

    /**
     * @param humanLinkConduit the humanLinkConduit to set
     */
    public void setHumanLinkConduit(HumanLinkParentObjectInfoHeavy humanLinkConduit) {
        this.humanLinkConduit = humanLinkConduit;
    }
    
    
    
}
