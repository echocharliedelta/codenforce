/*
 * Copyright (C) 2020 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.application.interfaces.IFace_updateAuditable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Encapsulates a mailing address
 * @author sylvia
 */
public  class   MailingAddress 
        extends TrackedEntity
        implements  IFace_keyIdentified,
                    IFace_noteHolder, 
                    IFace_updateAuditable<MailingAddressFieldsEnum>,
                    IFace_Mergable,
                    Comparable<Object>{
    
    final static String TABLE_NAME = "mailingaddress";
    final static String PK_FIELD = "addressid";
    final static String HF_NAME = "Mailing Address";
    
    protected int addressID;
    
    protected boolean useBuildingNo;
    protected String buildingNo;
    
    protected MailingStreet street;
    
    protected boolean usePOBox;
    protected int poBox;
    
    protected boolean useSecondaryUnitIdentifier;
    protected String secondaryUnitIdentifier;
    
    protected boolean useAttention;
    protected String attn;
    
    protected LocalDateTime verifiedTS;
    protected User verifiedBy;
    protected BOBSource verifiedSource;
    protected BOBSource source;
    protected String notes;
    
    protected String addressString;
    protected String addressPretty2LineEscapeFalse;
    protected String addressPretty1Line;
    
    /**
     * No arg constructor
     */
    public MailingAddress(){
        
    }
    
    /**
     * Transfers member values
     * @param mad 
     */
    public MailingAddress(MailingAddress mad){
          
        this.addressID = mad.addressID;
        this.buildingNo = mad.buildingNo;
        this.street = mad.street;
        this.verifiedTS = mad.verifiedTS;
        this.source = mad.source;
        this.notes = mad.notes;
        
        this.poBox = mad.poBox;
        this.usePOBox = mad.usePOBox;
        
        this.useSecondaryUnitIdentifier = mad.useSecondaryUnitIdentifier;
        this.secondaryUnitIdentifier = mad.secondaryUnitIdentifier;
        
        this.useAttention = mad.useAttention;
        this.attn = mad.attn;
        
        this.addressPretty1Line = mad.addressPretty1Line;
        this.addressPretty2LineEscapeFalse= mad.addressPretty2LineEscapeFalse;
        
        this.createdBy = mad.createdBy;
        this.createdTS = mad.createdTS;

        this.lastUpdatedBy = mad.lastUpdatedBy;
        this.lastUpdatedTS = mad.lastUpdatedTS;

        this.deactivatedBy = mad.deactivatedBy;
        this.deactivatedTS = mad.deactivatedTS;
    }
    
    
    public String getAddressString(){
        return "Use addressPretty2LineEscapeFalse or addressPretty1Line";
        
    }
    
    /**
     * @return the addressID
     */
    public int getAddressID() {
        return addressID;
    }

    /**
     * @return the buildingNo
     */
    public String getBuildingNo() {
        return buildingNo;
    }

   
    /**
     * @return the poBox
     */
    public int getPoBox() {
        return poBox;
    }

    /**
     * @return the verifiedTS
     */
    public LocalDateTime getVerifiedTS() {
        return verifiedTS;
    }

    /**
     * @return the source
     */
    public BOBSource getSource() {
        return source;
    }

    /**
     * @param addressID the addressID to set
     */
    public void setAddressID(int addressID) {
        this.addressID = addressID;
    }

    /**
     * @param buildingNo the buildingNo to set
     */
    public void setBuildingNo(String buildingNo) {
        this.buildingNo = buildingNo;
    }

    /**
     * @param poBox the poBox to set
     */
    public void setPoBox(int poBox) {
        this.poBox = poBox;
    }

    /**
     * @param verifiedTS the verifiedTS to set
     */
    public void setVerifiedTS(LocalDateTime verifiedTS) {
        this.verifiedTS = verifiedTS;
    }

    /**
     * @param source the source to set
     */
    public void setSource(BOBSource source) {
        this.source = source;
    }

    

    @Override
    public int getDBKey() {
        return addressID;
    }

    
    @Override
    public String getNotes() {
        return notes;
    }

    @Override
    public void setNotes(String n) {
        notes = n;
    }

  
    @Override
    public String getPKFieldName() {
        return PK_FIELD;
    }

    @Override
    public String getDBTableName() {
        return TABLE_NAME;
    }

    /**
     * @return the street
     */
    public MailingStreet getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(MailingStreet street) {
        this.street = street;
    }

    /**
     * @return the verifiedBy
     */
    public User getVerifiedBy() {
        return verifiedBy;
    }

    /**
     * @param verifiedBy the verifiedBy to set
     */
    public void setVerifiedBy(User verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    /**
     * @return the verifiedSource
     */
    public BOBSource getVerifiedSource() {
        return verifiedSource;
    }

    /**
     * @param verifiedSource the verifiedSource to set
     */
    public void setVerifiedSource(BOBSource verifiedSource) {
        this.verifiedSource = verifiedSource;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.addressID;
        hash = 83 * hash + Objects.hashCode(this.buildingNo);
        hash = 83 * hash + Objects.hashCode(this.street);
        hash = 83 * hash + this.poBox;
        hash = 83 * hash + Objects.hashCode(this.verifiedTS);
        hash = 83 * hash + Objects.hashCode(this.verifiedBy);
        hash = 83 * hash + Objects.hashCode(this.verifiedSource);
        hash = 83 * hash + Objects.hashCode(this.source);
        hash = 83 * hash + Objects.hashCode(this.notes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MailingAddress other = (MailingAddress) obj;
        if (this.addressID != other.addressID) {
            return false;
        }
        return true;
    }

    @Override
    public String getNoteHolderFriendlyName() {
        return HF_NAME;
    }

    /**
     * Always upper case
     * @return the addressPretty2LineEscapeFalse
     */
    public String getAddressPretty2LineEscapeFalse() {
        if(addressPretty2LineEscapeFalse != null){
            return addressPretty2LineEscapeFalse.toUpperCase();
        }
        return addressPretty2LineEscapeFalse;
    }

    /** Addresses are always upper case
     * @return the addressPretty1Line
     */
    public String getAddressPretty1Line() {
        if(addressPretty1Line != null){
            return addressPretty1Line.toUpperCase();
        }
        return addressPretty1Line;
    }

    /**
     * @param addressPretty2LineEscapeFalse the addressPretty2LineEscapeFalse to set
     */
    public void setAddressPretty2LineEscapeFalse(String addressPretty2LineEscapeFalse) {
        
        this.addressPretty2LineEscapeFalse = addressPretty2LineEscapeFalse;
    }

    /**
     * @param addressPretty1Line the addressPretty1Line to set
     */
    public void setAddressPretty1Line(String addressPretty1Line) {
        this.addressPretty1Line = addressPretty1Line;
    }

    /**
     * @return the secondaryUnitIdentifier
     */
    public String getSecondaryUnitIdentifier() {
        return secondaryUnitIdentifier;
    }

    /**
     * @param secondaryUnitIdentifier the secondaryUnitIdentifier to set
     */
    public void setSecondaryUnitIdentifier(String secondaryUnitIdentifier) {
        this.secondaryUnitIdentifier = secondaryUnitIdentifier;
    }

    /**
     * @return the useSecondaryUnitIdentifier
     */
    public boolean isUseSecondaryUnitIdentifier() {
        return useSecondaryUnitIdentifier;
    }

    /**
     * @param useSecondaryUnitIdentifier the useSecondaryUnitIdentifier to set
     */
    public void setUseSecondaryUnitIdentifier(boolean useSecondaryUnitIdentifier) {
        this.useSecondaryUnitIdentifier = useSecondaryUnitIdentifier;
    }

    /**
     * @return the attn
     */
    public String getAttn() {
        if(attn != null){
            return attn.toUpperCase();
        }
        return attn;
    }

    /**
     * @param attn the attn to set
     */
    public void setAttn(String attn) {
        this.attn = attn;
    }

    /**
     * @return the useAttention
     */
    public boolean isUseAttention() {
        return useAttention;
    }

    /**
     * @param useAttention the useAttention to set
     */
    public void setUseAttention(boolean useAttention) {
        this.useAttention = useAttention;
    }

    /**
     * @return the usePOBox
     */
    public boolean isUsePOBox() {
        return usePOBox;
    }

    /**
     * @param usePOBox the usePOBox to set
     */
    public void setUsePOBox(boolean usePOBox) {
        this.usePOBox = usePOBox;
    }

    /**
     * @return the useBuildingNo
     */
    public boolean isUseBuildingNo() {
        return useBuildingNo;
    }

    /**
     * @param useBuildingNo the useBuildingNo to set
     */
    public void setUseBuildingNo(boolean useBuildingNo) {
        this.useBuildingNo = useBuildingNo;
    }

    @Override
    public Class<MailingAddressFieldsEnum> getFieldDumpEnum() {
        return MailingAddressFieldsEnum.class;
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof MailingAddress mad){
            if(this.buildingNo != null && mad.getBuildingNo() != null){
                return this.buildingNo.compareTo(mad.getBuildingNo());
            }
        } else {
            return 0;
        }
        return 0;
    }

    @Override
    public String getPrimaryDescriptiveString() {
        return buildingNo;
    }

    @Override
    public String getSecondaryDescriptiveString() {
        return addressPretty1Line;
    }

  
}
