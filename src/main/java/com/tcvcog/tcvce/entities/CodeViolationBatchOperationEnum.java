/*
 * Copyright (C) 2018 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Specifies pathways for batch operations and related switches and controls 
 * for both UI and BB and Controller pathways
 * @author ellen bascomb of apt 31y
 */
public enum CodeViolationBatchOperationEnum {

    STIPCOMP_EXTENSION(                     "Extend stipulated compliance date", 
                                            "Reason for extended compliance date",
                                            "codeviolation_stipcompdateextension",
                                            true
                                                ),
    COMPLIANCE(                             "Record compliance", 
                                            "Mitigation actions observed",
                                            "complianceEvent",
                                            true
                                                ),
    NULLIFY(                                "Nullify violation", 
                                            "Reason for nullification",
                                            "codeviolation_nullify",
                                            true
                                                );
    
    
    private final String label;
    private final String operationReasonFieldLabel;
    private final String eventCatLoggingKey;
    private final boolean allowOperationWithCitationLink;
    
    private CodeViolationBatchOperationEnum(String label, String reason, String k, boolean allowWithCit){
        this.label = label;
        this.operationReasonFieldLabel = reason;
        eventCatLoggingKey = k;
        this.allowOperationWithCitationLink = allowWithCit;
    }
    
    public String getLabel(){
        return label;
    }

    /**
     * @return the allowOperationWithCitationLink
     */
    public boolean isAllowOperationWithCitationLink() {
        return allowOperationWithCitationLink;
    }

    /**
     * @return the eventCatLoggingKey
     */
    public String getEventCatLoggingKey() {
        return eventCatLoggingKey;
    }

    /**
     * @return the operationReasonFieldLabel
     */
    public String getOperationReasonFieldLabel() {
        return operationReasonFieldLabel;
    }
}


