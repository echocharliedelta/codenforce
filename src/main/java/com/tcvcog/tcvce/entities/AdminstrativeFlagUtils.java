/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Holds utilities
 * @author pierre15
 */
public class AdminstrativeFlagUtils {
    
    /**
     * Recursive accumulator of all flags
     * @param flagHolder
     * @return 
     */
    public static List<AdministrativeFlag> collectAllAdminFlags(IFaceAdministrativeFlagHolder flagHolder){
        
        List<AdministrativeFlag> flagList = new ArrayList<>();
        if(flagHolder == null){
            return flagList;
        }
        
        for(IFaceAdministrativeFlagHolder holder: flagHolder.getFlagHoldingChildren()){
            flagList.addAll(collectAllAdminFlags(holder));
        }
        
        return flagList;
        
    }
    
}
