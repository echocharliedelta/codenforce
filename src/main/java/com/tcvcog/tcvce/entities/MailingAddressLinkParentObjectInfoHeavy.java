/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * A back-flip class that inverts the usual object composition of a
 * mailing address link holder and instead this subclass of a MAD link
 * contains String descriptions of the parent object. 
 * 
 * Used for reverse mailing address searches. e.g. which objects link to 
 * 8392 CHICORA ST? 
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */
public class    MailingAddressLinkParentObjectInfoHeavy
       extends  MailingAddressLink{
    
    protected boolean externalToSessionMuni;
    protected Municipality muiniParent;
    protected boolean allowCrossMuniView;
    protected String parentObjectDescription;
    
    public MailingAddressLinkParentObjectInfoHeavy(MailingAddressLink madLink){
        super(madLink);
    }

    /**
     * @return the parentObjectDescription
     */
    public String getParentObjectDescription() {
        return parentObjectDescription;
    }

    /**
     * @param parentObjectDescription the parentObjectDescription to set
     */
    public void setParentObjectDescription(String parentObjectDescription) {
        this.parentObjectDescription = parentObjectDescription;
    }

    /**
     * @return the allowCrossMuniView
     */
    public boolean isAllowCrossMuniView() {
        return allowCrossMuniView;
    }

    /**
     * @param allowCrossMuniView the allowCrossMuniView to set
     */
    public void setAllowCrossMuniView(boolean allowCrossMuniView) {
        this.allowCrossMuniView = allowCrossMuniView;
    }

    /**
     * @return the muiniParent
     */
    public Municipality getMuiniParent() {
        return muiniParent;
    }

    /**
     * @param muiniParent the muiniParent to set
     */
    public void setMuiniParent(Municipality muiniParent) {
        this.muiniParent = muiniParent;
    }

    /**
     * @return the externalToSessionMuni
     */
    public boolean isExternalToSessionMuni() {
        return externalToSessionMuni;
    }

    /**
     * @param externalToSessionMuni the externalToSessionMuni to set
     */
    public void setExternalToSessionMuni(boolean externalToSessionMuni) {
        this.externalToSessionMuni = externalToSessionMuni;
    }
    
}
