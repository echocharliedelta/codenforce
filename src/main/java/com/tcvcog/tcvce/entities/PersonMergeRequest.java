/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Contains object settings to instruct the PersonCoordinator to 
 * conduct a person-person merge operation. This object, once digested, 
 * will yield a PersonMergeLog
 * 
 * @author Ellen Bascomb of Apartment31Y
 */
public class PersonMergeRequest {
    private UserAuthorized requestingUser;
    
    /**
     * The merge target--the record being preserved
     */
   private PersonLinkHeavy personPrimary;
   /**
    * The record to be squashed and deactivated at the end of the merge operation
    * whose links will be transferred to the primary person
    */
   private PersonLinkHeavy personSubordinate;
   
   private MergeMatchLevelEnum matchLevel;
   
   private boolean initialCheckMergeAllowed;
   private LocalDateTime initialCheckMergeTS;
   private String initialMergeCheckMessage;
   
   // MERGE OPERATION CONFIGURATION PARAMS AND SWITCHES
   private boolean aliasSubordinateName;
   /**
    * hidden contacts have a negative priority, usually -9
    */
   private boolean transferHiddenContactObjects;
   /**
    * If transferHiddenContactObjects is TRUE, this option applies
    * and if true the freshly linked contact objects will have a positive priority
    */
   private boolean unhideAllHiddenContactObjects;

  

    /**
     * @return the personPrimary
     */
    public PersonLinkHeavy getPersonPrimary() {
        return personPrimary;
    }

    /**
     * @param personPrimary the personPrimary to set
     */
    public void setPersonPrimary(PersonLinkHeavy personPrimary) {
        this.personPrimary = personPrimary;
    }

    /**
     * @return the personSubordinate
     */
    public PersonLinkHeavy getPersonSubordinate() {
        return personSubordinate;
    }

    /**
     * @param personSubordinate the personSubordinate to set
     */
    public void setPersonSubordinate(PersonLinkHeavy personSubordinate) {
        this.personSubordinate = personSubordinate;
    }

    /**
     * @return the aliasSubordinateName
     */
    public boolean isAliasSubordinateName() {
        return aliasSubordinateName;
    }

    /**
     * @param aliasSubordinateName the aliasSubordinateName to set
     */
    public void setAliasSubordinateName(boolean aliasSubordinateName) {
        this.aliasSubordinateName = aliasSubordinateName;
    }

 

    /**
     * @return the unhideAllHiddenContactObjects
     */
    public boolean isUnhideAllHiddenContactObjects() {
        return unhideAllHiddenContactObjects;
    }

    /**
     * @param unhideAllHiddenContactObjects the unhideAllHiddenContactObjects to set
     */
    public void setUnhideAllHiddenContactObjects(boolean unhideAllHiddenContactObjects) {
        this.unhideAllHiddenContactObjects = unhideAllHiddenContactObjects;
    }

    /**
     * @return the transferHiddenContactObjects
     */
    public boolean isTransferHiddenContactObjects() {
        return transferHiddenContactObjects;
    }

    /**
     * @param transferHiddenContactObjects the transferHiddenContactObjects to set
     */
    public void setTransferHiddenContactObjects(boolean transferHiddenContactObjects) {
        this.transferHiddenContactObjects = transferHiddenContactObjects;
    }

    /**
     * @return the requestingUser
     */
    public UserAuthorized getRequestingUser() {
        return requestingUser;
    }

    /**
     * @param requestingUser the requestingUser to set
     */
    public void setRequestingUser(UserAuthorized requestingUser) {
        this.requestingUser = requestingUser;
    }

    /**
     * @return the matchLevel
     */
    public MergeMatchLevelEnum getMatchLevel() {
        return matchLevel;
    }

    /**
     * @param matchLevel the matchLevel to set
     */
    public void setMatchLevel(MergeMatchLevelEnum matchLevel) {
        this.matchLevel = matchLevel;
    }

    /**
     * @return the initialCheckMergeTS
     */
    public LocalDateTime getInitialCheckMergeTS() {
        return initialCheckMergeTS;
    }

    /**
     * @param initialCheckMergeTS the initialCheckMergeTS to set
     */
    public void setInitialCheckMergeTS(LocalDateTime initialCheckMergeTS) {
        this.initialCheckMergeTS = initialCheckMergeTS;
    }

    /**
     * @return the initialCheckMergeAllowed
     */
    public boolean isInitialCheckMergeAllowed() {
        return initialCheckMergeAllowed;
    }

    /**
     * @param initialCheckMergeAllowed the initialCheckMergeAllowed to set
     */
    public void setInitialCheckMergeAllowed(boolean initialCheckMergeAllowed) {
        this.initialCheckMergeAllowed = initialCheckMergeAllowed;
    }

    /**
     * @return the initialMergeCheckMessage
     */
    public String getInitialMergeCheckMessage() {
        return initialMergeCheckMessage;
    }

    /**
     * @param initialMergeCheckMessage the initialMergeCheckMessage to set
     */
    public void setInitialMergeCheckMessage(String initialMergeCheckMessage) {
        this.initialMergeCheckMessage = initialMergeCheckMessage;
    }
    
    
}
