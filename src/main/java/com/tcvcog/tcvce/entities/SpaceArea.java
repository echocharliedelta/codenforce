/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Represents a rectangular space, such as a room in a building.
 * Provides internals for converting length and width between
 * feet + inches and total inches (which the DB uses).
 * 
 * @author pierre15
 */
public class SpaceArea {
        
    static final float INCHES_PER_FOOT = 12.0f;
    static final float CUBIC_INCHES_PER_CUBIC_FEET = 144.0f;
    
    private float lengthFT;
    private float legnthIN;
    
    private float widthFT;
    private float widthIN;
    
    public float getLengthTotalInches(){
        return 0.0f;
        
        
    }
    
    public float getWidthTotalInches(){
        
        
        return 0.0f;
        
    }
    
    
    // boring getters and setters
    

    /**
     * @return the lengthFT
     */
    public float getLengthFT() {
        return lengthFT;
    }

    /**
     * @param lengthFT the lengthFT to set
     */
    public void setLengthFT(float lengthFT) {
        this.lengthFT = lengthFT;
    }

    /**
     * @return the legnthIN
     */
    public float getLegnthIN() {
        return legnthIN;
    }

    /**
     * @param legnthIN the legnthIN to set
     */
    public void setLegnthIN(float legnthIN) {
        this.legnthIN = legnthIN;
    }

    /**
     * @return the widthFT
     */
    public float getWidthFT() {
        return widthFT;
    }

    /**
     * @param widthFT the widthFT to set
     */
    public void setWidthFT(float widthFT) {
        this.widthFT = widthFT;
    }

    /**
     * @return the widthIN
     */
    public float getWidthIN() {
        return widthIN;
    }

    /**
     * @param widthIN the widthIN to set
     */
    public void setWidthIN(float widthIN) {
        this.widthIN = widthIN;
    }
    
    
    
}
