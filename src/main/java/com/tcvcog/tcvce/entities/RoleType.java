/*
 * Copyright (C) 2018 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Embodies the official ranks of the entire CodeNForce system!!
 * 
 * @author ellen bascomb of apt 31y
 */
public enum RoleType {
    
    // Developer rank was removed during the permissions overhaul of December 2022
    
    SysAdmin("System Administrator", 4),
    
    // NOTICE that until Version 1.0.9, CODE_ENFORCEMENT_OFFICER was
    // here at rank 5 but was removed when officer level permissions are actually
    // a shared attribute of all of these enum-delimited roles

    MuniManager("Manager", 3),
    MuniStaff("Staff", 2),
    MuniReader("Read Only", 1),
    Public("Public", 0); 
   
    private final String label;
    private final int rank;
    
    private RoleType(String label, int rnk){
        this.label = label;
        this.rank = rnk;
    }
    
    public String getLabel(){
        return label;
    }
    
    public int getRank(){
        return rank;
    }
    
}
