/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.entities;

/**
 * Enumerates the three possible uses for a text block on an occ permit
 * @author sylvia
 */
public enum TextBlockPermitFieldEnum {
    STIPULATIONS("Permit Stipulations", "textblock_cat_permit_stipulations" ),
    NOTICES("Permit Notices", "textblock_cat_permit_notices"),
    COMMENTS("Permit Comments", "textblock_cat_permit_comments");
    
    private final String label;
    private final String dbFixedValueLookup;
    
    private TextBlockPermitFieldEnum(String l, String db){
        label = l;
        dbFixedValueLookup = db;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @return the dbFixedValueLookup
     */
    public String getDbFixedValueLookup() {
        return dbFixedValueLookup;
    }
    
}
