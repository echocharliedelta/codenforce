package com.tcvcog.tcvce.entities;

public enum MultiObjectTypeEnum {
    OccInspectionDetermination(
            "OccInspectionDetermination",
            "/restricted/cogadmin/occInspectionDeterminationManage.xhtml",
            "OccInspection Determination"
    ),
    OccInspectionCause(
            "OccInspectionCause",
            "/restricted/cogadmin/occInspectionCauseManage.xhtml",
            "OccInspection Cause"
    ),
    PropertyUseType(
            "PropertyUseType",
            "/restricted/cogadmin/propertyUseTypeManage.xhtml",
            "PropertyUse Type"
    ),
    IntensityClass(
            "IntensityClass",
            "/restricted/cogadmin/intensityManage.xhtml",
            "Intensity/Severity rating"
    ),
    CitationStatus(
            "CitationStatus",
            "/restricted/cogadmin/citationStatusManage.xhtml",
            "Citation Status Log Status"
    ),
    ActionRequestIssueType(
            "CEActionRequestIssueType",
            "/restricted/cogadmin/cearIssueTypeManage.xhtml",
            "Action request issue type"
    ) ;

    private final String value;
    private final String includePath;
    private final String description;

    MultiObjectTypeEnum(String value, String includePath, String description) {
        this.value = value;
        this.includePath = includePath;
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public String getIncludePath() {
        return includePath;
    }

    public String getDescription() {
        return description;
    }
}
