/*
 * Copyright (C) 2018 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import java.util.Objects;

/**
 *
 * @author ellen bascomb of apt 31y
 */
public class    Municipality
        extends UMAPTrackedEntity
        implements IFaceCachable{
    
    protected int muniCode;
    protected String muniName;
    
    private final static String TABLE_NAME = "municipality";
    private final static String MUNI_PK = "municode";
    private final static UMAPTrackedEnum UMAP_TRACKED_ENUM = UMAPTrackedEnum.MUNICIPALITY;

     /**
     * Convenience getter for deactivation; This distinction between
     * deactivated and not enabled only applies to muni. This method 
     * returns the life or death status. isActiveInProgram reflects 
     * the enabled or not enabled (snooze switch) kind of flag
     * @return 
     */
    public boolean isSystemDeactivated(){
        return Objects.nonNull(deactivatedTS);
        
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + this.muniCode;
        hash = 19 * hash + Objects.hashCode(this.muniName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Municipality other = (Municipality) obj;
        if (this.muniCode != other.muniCode) {
            return false;
        }
        
        return true;
    }

    
    
    /**
     * @return the muniCode
     */
    public int getMuniCode() {
        return muniCode;
    }

    /**
     * @return the muniName
     */
    public String getMuniName() {
        return muniName;
    }

    /**
     * @param muniCode the muniCode to set
     */
    public void setMuniCode(int muniCode) {
        this.muniCode = muniCode;
    }

    /**
     * @param muniName the muniName to set
     */
    public void setMuniName(String muniName) {
        this.muniName = muniName;
    }

    @Override
    public UMAPTrackedEnum getUMAPTrackedEntityEnum() {
        return UMAP_TRACKED_ENUM;
    }

    @Override
    public String getPKFieldName() {
        return MUNI_PK;
    }

    @Override
    public int getDBKey() {
        return muniCode;
    }

    @Override
    public String getDBTableName() {
        return TABLE_NAME;
    }

    @Override
    public int getCacheKey() {
        return muniCode;
    }
    
   

  
    
}
