/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * The much-awaited template object for Notice of Violation, called letters
 * on the UI
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */
public class NoticeOfViolationTemplate implements Serializable{
    private int templateID;
    private NoticeOfViolationType templateType;
    private Municipality muni;
    private String title;
    private String templateTextEscapeFalse;
    private LocalDateTime deactivatedTS;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.templateID;
        hash = 97 * hash + Objects.hashCode(this.templateType);
        hash = 97 * hash + Objects.hashCode(this.muni);
        hash = 97 * hash + Objects.hashCode(this.title);
        hash = 97 * hash + Objects.hashCode(this.templateTextEscapeFalse);
        hash = 97 * hash + Objects.hashCode(this.deactivatedTS);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NoticeOfViolationTemplate other = (NoticeOfViolationTemplate) obj;
        return this.templateID == other.templateID;
    }

    /**
     * @return the templateID
     */
    public int getTemplateID() {
        return templateID;
    }

    /**
     * @param templateID the templateID to set
     */
    public void setTemplateID(int templateID) {
        this.templateID = templateID;
    }

    /**
     * @return the templateType
     */
    public NoticeOfViolationType getTemplateType() {
        return templateType;
    }

    /**
     * @param templateType the templateType to set
     */
    public void setTemplateType(NoticeOfViolationType templateType) {
        this.templateType = templateType;
    }

    /**
     * @return the muni
     */
    public Municipality getMuni() {
        return muni;
    }

    /**
     * @param muni the muni to set
     */
    public void setMuni(Municipality muni) {
        this.muni = muni;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the templateTextEscapeFalse
     */
    public String getTemplateTextEscapeFalse() {
        return templateTextEscapeFalse;
    }

    /**
     * @param ttef the templateTextEscapeFalse to set
     */
    public void setTemplateTextEscapeFalse(String ttef ){
        this.templateTextEscapeFalse = ttef;
    }

    /**
     * @return the deactivatedTS
     */
    public LocalDateTime getDeactivatedTS() {
        return deactivatedTS;
    }

    /**
     * @param deactivatedTS the deactivatedTS to set
     */
    public void setDeactivatedTS(LocalDateTime deactivatedTS) {
        this.deactivatedTS = deactivatedTS;
    }
    
    
}
