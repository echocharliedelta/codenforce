/*
 * Copyright (C) 2019 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * PRIMARY SECURITY OBJECT
 * 
 * A credential is issued to a user making it a valid UserAuthorized (User subclass).
 * 
 * Credential contains 5 permissions switches that generally encode a hierarchical 
 * permissions ladder in which users have a permissions ceiling at and below 
 * which they are fully authorized. 
 * 
 * Above which they will fail role floor permissions tests, meaning the ceiling must
 * meet or exceed the desired floor for an operation to succeed.
 * 
 * Code officer permissions can be injected at any level MuniStaff or better. 
 * A MuniStaff 
 * 
 * 
 * @author sylvia
 */
public class Credential implements Serializable{
    
    private final UserMuniAuthPeriod governingAuthPeriod;
    
    private final String signature;
    
    private final boolean hasSysAdminPermissions;
    private final boolean hasMuniManagerPermissions;
    
    private final boolean hasMuniStaffPermissions;
    private final boolean hasMuniReaderPermissions;

    public Credential(          UserMuniAuthPeriod uap,
                                boolean admin,
                                boolean cogstaff,
                                boolean munistaff,
                                boolean munireader){
        
        governingAuthPeriod = uap;
        hasSysAdminPermissions = admin;
        hasMuniManagerPermissions = cogstaff;
        hasMuniStaffPermissions = munistaff;
        hasMuniReaderPermissions = munireader;
        
        StringBuilder sb = new StringBuilder();
        sb.append("MC:");
        sb.append(governingAuthPeriod.getMuni().muniCode);
        sb.append("|");
        sb.append("UID:");
        sb.append(governingAuthPeriod.getUserID());
        sb.append("|");
        sb.append("UMAPID:");
        sb.append(governingAuthPeriod.getUserMuniAuthPeriodID());
        sb.append("|");
        sb.append("UMAPEVALTS:");
        sb.append(governingAuthPeriod.getValidityEvaluatedTS());
        sb.append("|");
        sb.append("UMAPISVALTS:");
        sb.append(governingAuthPeriod.getValidatedTS());
        sb.append("|");
//        sb.append("UMAPLOGID:");
//        sb.append(governingAuthPeriod.getPeriodActivityLogBook().get(0));
//        sb.append("|");
        sb.append("POSTCON:");
        sb.append("|");
        signature = sb.toString();
    }
    
    /**
     * Special getter that returns of boolean from inside the UMAP;
     * This little back flip is to maintain reverse compatibility with the legacy 
     * Credential structure designed during "first pass" authorization design
     * 
     * @return the hasEnfOfficialPermissions
     */
    public boolean isHasEnfOfficialPermissions() {
        boolean ceo = false;
        if(this.governingAuthPeriod != null && this.governingAuthPeriod.isCodeOfficer()){
            ceo = true;
        }
        return ceo;
    }
    
    /**
     * Adapter method designed for disabling controls for read only users
     * by returning true if the user only has read only permissions,
     * which is to say if the user does NOT have muni staff permissions (or greater).
     * 
     * While faces pages can always negate a check of muni staff permissions, as this 
     * method does internally, but author judged reading disabled="#{backingBean.permissionsRestrictedToReadOnly}"
     * to be clearer than disabled="#{!backingBean.hasMuniStaffPermissions}
     * 
     * In other words, this method allows lexical alignment between the 
     * backing bean member name and its use on the front end
     * 
     * @return true if the user should only be allowed read only access
     */
    public boolean isPermissionRestrictedToReadOnly(){
        return !hasMuniStaffPermissions;
    }
    
    /**
     * Adapter method designed for not rendering a control for read only users.
     * Bean authors can alwa
     * @return 
     */
    public boolean isPermissionDoNotRenderForReadOnly(){
        return hasMuniStaffPermissions;
    }

    /**
     * @return the signature
     */
    public String getSignature() {
        return signature;
    }

    /**
     * @return the hasSysAdminPermissions
     */
    public boolean isHasSysAdminPermissions() {
        return hasSysAdminPermissions;
    }

    /**
     * @return the hasMuniManagerPermissions
     */
    public boolean isHasMuniManagerPermissions() {
        return hasMuniManagerPermissions;
    }



    /**
     * @return the hasMuniStaffPermissions
     */
    public boolean isHasMuniStaffPermissions() {
        return hasMuniStaffPermissions;
    }

    /**
     * @return the hasMuniReaderPermissions
     */
    public boolean isHasMuniReaderPermissions() {
        return hasMuniReaderPermissions;
    }

    /**
     * @return the governingAuthPeriod
     */
    public UserMuniAuthPeriod getGoverningAuthPeriod() {
        return governingAuthPeriod;
    }

    
}
