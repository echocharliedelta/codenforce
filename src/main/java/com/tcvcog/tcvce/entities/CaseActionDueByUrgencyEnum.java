/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Used by the case priority system for categorizing action dates as 
 * either time critical e.g. a court hearing date/time or Advisory
 * @author pierre15
 */
public enum CaseActionDueByUrgencyEnum {
    
      
    TIME_CRITICAL("Date/Time critical action", "caseStageInvestigationIconID"),
    ADVISORY("Advisory action due by", "caseStageEnforcementIconID");
    
    private final String label;
    private final String iconPropertyLookup;
    
    private CaseActionDueByUrgencyEnum(String label, String icon){
        this.label = label;
        this.iconPropertyLookup = icon;
    }
    
    public String getLabel(){
        return label;
    }
    
    public String getIconPropertyLookup(){
        return iconPropertyLookup;
    }
    
    
}
