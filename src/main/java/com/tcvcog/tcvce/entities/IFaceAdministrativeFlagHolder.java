/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import java.util.List;

/**
 * Implementing class can hold and receive administrative flags, and other
 * flag holders.
 * 
 * @author pierre15
 */
public interface IFaceAdministrativeFlagHolder {
    
    /**
     * Implementers return their flags.
     * @return all the flags
     */
    public List<AdministrativeFlag> getAdminFlags();
    
    /**
     * Take in a list of flags
     * @param flags 
     */
    public void setAdminFlags(List<AdministrativeFlag> flags);
    
    /**
     * Allow for appending a single flag
     * @param flag 
     */
    public void appendAdminFlag(AdministrativeFlag flag);
    
    /**
     * Implementers should return an empty list if they have no flag holding
     * children.
     * @return the children flag holders
     */
    public List<IFaceAdministrativeFlagHolder> getFlagHoldingChildren();
    
    
    
}
