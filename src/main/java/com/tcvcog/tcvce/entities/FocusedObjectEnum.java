/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Represents the system's current session object, or known as the 
 * primary object
 * 
 * @author ELLEN BASCOMB of APARTMENT 31Y
 */
public enum FocusedObjectEnum {
    MUNI_DASHBOARD("missionControl", "focus-dashboard", EventRealm.UNIVERSAL), 
    CODE_BOOK("codeSetManage", "focus-code", EventRealm.UNIVERSAL), 
    PROPERTY("propertyInfo", "focus-property", EventRealm.PARCEL),
    PERSON("", "focus-person", EventRealm.UNIVERSAL),
    MAILING_ADDRESS("", "", EventRealm.UNIVERSAL), 
    CECASE("ceCaseProfile", "focus-cecase", EventRealm.CODE_ENFORCEMENT),
    OCCPERIOD("occPeriodWorkflow", "focus-occperiod", EventRealm.OCCUPANCY),
    OCCPERMIT("occPeriodWorkflow", "focus-occperiod", EventRealm.OCCUPANCY),
    UNKNOWN("","", EventRealm.UNIVERSAL);
    
    private final String navString;
    private final String cssClass;
    private final EventRealm eventRealmMapping;
    
    private FocusedObjectEnum(String nav, String css, EventRealm dom){
        navString = nav;
        cssClass = css;
        eventRealmMapping = dom;
    }

    /**
     * @return the navString
     */
    public String getNavString() {
        return navString;
    }

    /**
     * @return the cssClass
     */
    public String getCssClass() {
        return cssClass;
    }

    /**
     * @return the eventRealmMapping
     */
    public EventRealm getEventRealmMapping() {
        return eventRealmMapping;
    }
    
}
