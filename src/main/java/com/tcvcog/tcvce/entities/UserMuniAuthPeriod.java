/*
 * Copyright (C) 2019 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import com.tcvcog.tcvce.util.DateTimeUtil;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * Represents the ability of a particular user to access codeNforce within
 * a given municipality with a particular rank from start to stop date
 * and optionally with code officer permissions.
 * 
 * As of December 2022, CodeOfficer is no longer a rank but rather is a boolean
 * flag on this object. Records of the loginmuniauthperiod populate 
 * most of these fields
 * 
 * @author sylvia
 */
public class        UserMuniAuthPeriod 
        extends     BOb 
        implements  Serializable, 
                    Comparable<UserMuniAuthPeriod>,
                    IFaceCachable{

    private int userMuniAuthPeriodID;

    private final Municipality muni;
    private int userID;

    private final RoleType role;
    
    private List<UserMuniAuthPeriodLogEntry> periodActivityLogBook;

    /**
     * Added 3 DEC 2023 to allow the entire search subsystem to access
     * muni profile objects and its many permissions switches and defaults.
     * 
     * As of the first incarnation of this member, we're just using the 
     * default property address link role on the profile.
     */
    private MuniProfile muniProfile;
    
    
    /**
     * Timestamped business logic for UMAP validity is implemented.
     * Works in tandem with the member validatedTS: if this is a valid UMAP,
     * both this and validatedTS will have the same value. An invalid UAMP
     * will be timestamped when valuated but denied a validatedTS.
     * 
     * For Javaland only since validity is based on the current date/time
     */
    private LocalDateTime validityEvaluatedTS;
    
    /**
     * Security signaling timestamp upon validation by the UserCoordinator
     * For Javaland only since validity is based on the current date/time
     */
    private LocalDateTime validatedTS;

    /**
     * SECURITY CRITICAL
     * Leftmost bound of the period of authorization created by this object. 
     * 
     */
    private LocalDateTime startDate;
    
    /**
     * SECURITY CRITICAL
     * Rightmost bound of this authorization period. This period is not valid
     * if system time is after this moment. 
     */
    
    private LocalDateTime stopDate;
    /**
     * Any date value allowed here to deactivate record.
     * Generally, the timestamp's temporal representation is not considered, '
     * only that an act of deactivation has occurred (or, potentially could be
     * recorded as occurring in the future)
     */
    private LocalDateTime recorddeactivatedTS;

    private LocalDateTime createdTS;

    private int createdByUserID;
    private String notes;
    private int assignmentRelativeOrder;
    
    /**
     * This Code officer flag is checked by a variety of credential check points
     * throughout the system, such as issuing a citation. Flags on the
     * MuniProfile control if this flag's status must be respected by
     * various coordinator lockout methods methods
     */
    private final boolean codeOfficer;
    
    private LocalDateTime oathTS;
    private int oathCourtEntityID;
    private BlobLight oathBlob;
    
    /**
     * Locks in the new AuthPeriod to a muni, role, and officer flag to prevent
     * post-instantiation adjustment of these fields 
     * @param m 
     * @param rt 
     * @param ceo 
     */
    public UserMuniAuthPeriod(Municipality m, RoleType rt, boolean ceo){
        muni = m;
        role = rt;
        codeOfficer = ceo;
    }

    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + this.userMuniAuthPeriodID;
        hash = 67 * hash + Objects.hashCode(this.muni);
        hash = 67 * hash + this.userID;
        hash = 67 * hash + Objects.hashCode(this.periodActivityLogBook);
        hash = 67 * hash + Objects.hashCode(this.startDate);
        hash = 67 * hash + Objects.hashCode(this.stopDate);
        hash = 67 * hash + Objects.hashCode(this.recorddeactivatedTS);
        hash = 67 * hash + Objects.hashCode(this.role);
        hash = 67 * hash + Objects.hashCode(this.createdTS);
        hash = 67 * hash + this.createdByUserID;
        hash = 67 * hash + Objects.hashCode(this.notes);
        hash = 67 * hash + this.assignmentRelativeOrder;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserMuniAuthPeriod other = (UserMuniAuthPeriod) obj;
        if (this.userMuniAuthPeriodID != other.userMuniAuthPeriodID) {
            return false;
        }
//        if (this.userID != other.userID) {
//            return false;
//        }
//        if (this.createdByUserID != other.createdByUserID) {
//            return false;
//        }
//        if (this.assignmentRelativeOrder != other.assignmentRelativeOrder) {
//            return false;
//        }
//        if (!Objects.equals(this.notes, other.notes)) {
//            return false;
//        }
//        if (!Objects.equals(this.muni, other.muni)) {
//            return false;
//        }
//        if (!Objects.equals(this.periodActivityLogBook, other.periodActivityLogBook)) {
//            return false;
//        }
//        if (!Objects.equals(this.startDate, other.startDate)) {
//            return false;
//        }
//        if (!Objects.equals(this.stopDate, other.stopDate)) {
//            return false;
//        }
//        if (!Objects.equals(this.recorddeactivatedTS, other.recorddeactivatedTS)) {
//            return false;
//        }
//        if (this.role != other.role) {
//            return false;
//        }
//        if (!Objects.equals(this.createdTS, other.createdTS)) {
//            return false;
//        }
        return true;
    }
    
  
    /**
     * Orders UMAP for session creation by choosing the highest role rank 
     * in the list. Tied rank valid UMAPs are assigned based on assingment order
     * @param o
     * @return 
     */
    @Override
    public int compareTo(UserMuniAuthPeriod o) {
        if(o == null || this.role == null || o.getRole() == null){
            return 0;
        }
        if(this.role.getRank() > o.getRole().getRank()){
            return 1;
        } else if(this.role.getRank() < o.getRole().getRank()){
                return -1;
        } else {
            return 0;
        }
    }
  
    
  
  
    /**
     * @return the userID
     */
    public int getUserID() {
        return userID;
    }

    /**
     * @return the startDate
     */
    public LocalDateTime getStartDate() {
        return startDate;
    }

    /**
     * @return the stopDate
     */
    public LocalDateTime getStopDate() {
        return stopDate;
    }

   

    /**
     * @return the recorddeactivatedTS
     */
    public LocalDateTime getRecorddeactivatedTS() {
        return recorddeactivatedTS;
    }

    /**
     * @return the role
     */
    public RoleType getRole() {
        return role;
    }

    /**
     * @return the userMuniAuthPeriodID
     */
    public int getUserMuniAuthPeriodID() {
        return userMuniAuthPeriodID;
    }

   
    /**
     * @param userID the userID to set
     */
    public void setUserID(int userID) {
        this.userID = userID;
    }


    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    /**
     * @param stopDate the stopDate to set
     */
    public void setStopDate(LocalDateTime stopDate) {
        this.stopDate = stopDate;
    }

   
    /**
     * @param recorddeactivatedTS the recorddeactivatedTS to set
     */
    public void setRecorddeactivatedTS(LocalDateTime recorddeactivatedTS) {
        this.recorddeactivatedTS = recorddeactivatedTS;
    }


    /**
     * @param userMuniAuthPeriodID the userMuniAuthPeriodID to set
     */
    public void setUserMuniAuthPeriodID(int userMuniAuthPeriodID) {
        this.userMuniAuthPeriodID = userMuniAuthPeriodID;
    }

   
    /**
     * @return the muni
     */
    public Municipality getMuni() {
        return muni;
    }

 

    /**
     * @return the createdTS
     */
    public LocalDateTime getCreatedTS() {
        return createdTS;
    }

   

    /**
     * @param createdTS the createdTS to set
     */
    public void setCreatedTS(LocalDateTime createdTS) {
        this.createdTS = createdTS;
    }


    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return the periodActivityLogBook
     */
    public List<UserMuniAuthPeriodLogEntry> getPeriodActivityLogBook() {
        return periodActivityLogBook;
    }

    /**
     * @param periodActivityLogBook the periodActivityLogBook to set
     */
    public void setPeriodActivityLogBook(List<UserMuniAuthPeriodLogEntry> periodActivityLogBook) {
        this.periodActivityLogBook = periodActivityLogBook;
    }

    /**
     * @return the createdByUserID
     */
    public int getCreatedByUserID() {
        return createdByUserID;
    }

    /**
     * @param createdByUserID the createdByUserID to set
     */
    public void setCreatedByUserID(int createdByUserID) {
        this.createdByUserID = createdByUserID;
    }

    /**
     * @return the assignmentRelativeOrder
     */
    public int getAssignmentRelativeOrder() {
        return assignmentRelativeOrder;
    }

    /**
     * @param assignmentRelativeOrder the assignmentRelativeOrder to set
     */
    public void setAssignmentRelativeOrder(int assignmentRelativeOrder) {
        this.assignmentRelativeOrder = assignmentRelativeOrder;
    }

    /**
     * @return the validatedTS
     */
    public LocalDateTime getValidatedTS() {
        return validatedTS;
    }

    /**
     * @param validatedTS the validatedTS to set
     */
    public void setValidatedTS(LocalDateTime validatedTS) {
        this.validatedTS = validatedTS;
    }

    /**
     * @return the validityEvaluatedTS
     */
    public LocalDateTime getValidityEvaluatedTS() {
        return validityEvaluatedTS;
    }

    /**
     * @param validityEvaluatedTS the validityEvaluatedTS to set
     */
    public void setValidityEvaluatedTS(LocalDateTime validityEvaluatedTS) {
        this.validityEvaluatedTS = validityEvaluatedTS;
    }

    /**
     * @return return oathCourtEntityID
     */
    public int getOathCourtEntityID() {
        return oathCourtEntityID;
    }

    /**
     * @param oathCourtEntityID the oathCourtEntityID to set
     */
    public void setOathCourtEntityID(int oathCourtEntityID) {
        this.oathCourtEntityID = oathCourtEntityID;
    }

    /**
     * @return the oathTS
     */
    public LocalDateTime getOathTS() {
        return oathTS;
    }

    /**
     * @param oathTS the oathTS to set
     */
    public void setOathTS(LocalDateTime oathTS) {
        this.oathTS = oathTS;
    }

    /**
     * @return the oathBlob
     */
    public BlobLight getOathBlob() {
        return oathBlob;
    }

    /**
     * @param oathBlob the oathBlob to set
     */
    public void setOathBlob(BlobLight oathBlob) {
        this.oathBlob = oathBlob;
    }

    /**
     * @return the codeOfficer
     */
    public boolean isCodeOfficer() {
        return codeOfficer;
    }

    /**
     * @return the muniProfile
     */
    public MuniProfile getMuniProfile() {
        return muniProfile;
    }

    /**
     * @param muniProfile the muniProfile to set
     */
    public void setMuniProfile(MuniProfile muniProfile) {
        this.muniProfile = muniProfile;
    }

    @Override
    public int getCacheKey() {
        return userMuniAuthPeriodID;
    }

    
}