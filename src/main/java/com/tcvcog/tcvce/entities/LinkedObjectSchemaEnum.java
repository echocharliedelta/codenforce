/*
 * Copyright (C) 2021 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Enumerates the schemas for linked object roles and maps these roles
 * to actual DB identifiers
 * 
 * @author sylvia
 */
public enum LinkedObjectSchemaEnum {
    
    OccApplicationHuman (   
                            "Occupancy Application",
                            "public.occpermitapplicationhuman",
                            "OccApplicationHuman", 
                            "???", 
                            "",
                            null,
                            "???",
                            LinkedObjectFamilyEnum.HUMAN,
                            false,
                            FocusedObjectEnum.OCCPERIOD,
                            false,
                            false,
                            4813,
                            true
                        ),  // for Jurplel to update
    
    CECaseHuman         (   
                            "Code Enf. Case",
                            "public.humancecase", 
                            "CECaseHuman", 
                            "linkid", 
                            "cecase_caseid",
                            null,
                            "humancecase_linkid_seq",
                            LinkedObjectFamilyEnum.HUMAN,
                            false,
                            FocusedObjectEnum.CECASE,
                            true,
                            false,
                            23,
                            true
        
                        ), 
    
    OccPeriodHuman      (
                            "Occupancy Period",
                            "public.humanoccperiod",
                            "OccPeriodHuman", 
                            "linkid", 
                            "occperiod_periodid",
                            null,
                            "humanoccperiod_linkid_seq",
                            LinkedObjectFamilyEnum.HUMAN,
                            false,
                            FocusedObjectEnum.OCCPERIOD,
                            true,
                            false,
                            409 ,
                            true  
                        ), 
    
    
    OccPermitHuman      (
                            "Occupancy Permit",
                            "public.occpermithuman",
                            "OccPermitHuman", 
                            "linkid", 
                            "occpermit_permitid",
                            null,
                            "occpermithuman_linkid_seq",
                            LinkedObjectFamilyEnum.HUMAN,
                            false,
                            FocusedObjectEnum.OCCPERIOD,
                            true,
                            true,
                            641   ,
                            true
                        ), 
    
    ParcelHuman         (   
                            "Parcel",
                            "public.humanparcel",
                            "ParcelHuman", 
                            "linkid", 
                            "parcel_parcelkey",
                            null,
                            "humanparcel_linkid_seq",
                            LinkedObjectFamilyEnum.HUMAN,
                            false,
                            FocusedObjectEnum.PROPERTY,
                            true,
                            false,
                            2591,
                            true
                        ), 
    
    ParcelUnitHuman     (   
                            "Parcel Unit",
                            "public.humanparcelunit",
                            "ParcelUnitHuman", 
                            "linkid", 
                            "parcelunit_unitid",
                            null,
                            "parcelunithuman_linkid_seq",
                            LinkedObjectFamilyEnum.HUMAN,
                            false,
                            FocusedObjectEnum.PROPERTY,
                            true,
                            false,
                            1609,
                            true
                        ),
    
    CitationHuman       (
                            "Citation",
                            "public.citationhuman",
                            "CitationHuman", 
                            "linkid", 
                            "citation_citationid",
                            null,
                            "citationhuman_seq",
                            LinkedObjectFamilyEnum.HUMAN,
                            true,
                            FocusedObjectEnum.CECASE,
                            true,
                            true,
                            2063,
                            true
                        ), 
    
    CitationDocketHuman       (
                            "Citation Docket",
                            "public.citationdocketnohuman",
                            "CitationDocketHuman", 
                            "linkid", 
                            "docketno_docketid",
                            null,
                            "citationdockethuman_linkid_seq",
                            LinkedObjectFamilyEnum.HUMAN,
                            true,
                            FocusedObjectEnum.CECASE,
                            false,
                            true,
                            3659,
                            true
                        ), 
    
    EventHuman          (
                            "Event",
                            "public.eventhuman",
                            "EventHuman", 
                            "linkid", 
                            "event_eventid",
                            null,
                            "eventhuman_linkid_seq",
                            LinkedObjectFamilyEnum.HUMAN,
                            false,
                            FocusedObjectEnum.MUNI_DASHBOARD,
                            true,
                            false,
                            4447,
                            true
                        ), 
    
    MuniHuman           (
                            "Municipality",
                            "public.humanmuni",
                            "MuniHuman",
                            "linkid", 
                            "muni_municode",
                            null,
                            "humanmuni_linkid_seq",
                            LinkedObjectFamilyEnum.HUMAN,
                            false,
                            FocusedObjectEnum.MUNI_DASHBOARD,
                            true,
                            false,
                            1619,
                            true
                        ),
    // this would be more aptly named "HumanMailingAddress"
    MailingaddressHuman (   
                            "Person",
                            "humanmailingaddress",
                            "MailingaddressHuman", 
                            "linkid",
                            "humanmailing_humanid",
                            "humanmailing_addressid",
                            "humanmailing_linkid_seq",
                            LinkedObjectFamilyEnum.MAILING,
                            false,
                            FocusedObjectEnum.PROPERTY,
                            true,
                            false,
                            3257,
                            true
                        ), 
    ParcelMailingaddress  (
                            "Parcel",
                            "parcelmailingaddress",
                            "ParcelMailingaddress", 
                            "linkid", 
                            "parcel_parcelkey",
                            "mailingaddress_addressid",
                            "parcelmailing_linkid_seq",
                            LinkedObjectFamilyEnum.MAILING,
                            false,
                            FocusedObjectEnum.PROPERTY,
                            true,
                            false,
                            3947,
                            true
                        ),
    
    ParcelUnitMailingAddress  (
                            "Property Unit",
                            "parcelunitmailingaddress",
                            "ParcelUnitMailingAddress", 
                            "linkid", 
                            "parcelunit_unitid",
                            "mailingaddress_addressid",
                            "parcelunitmailingaddress_linkid_seq",
                            LinkedObjectFamilyEnum.MAILING,
                            false,
                            FocusedObjectEnum.PROPERTY,
                            true,
                            false,
                            1039,
                            true
                        ),
    
    /**
     * ECD doesn't think this is being used at all as of March 2024
     * 
     */
    CITATION_CODEVIOLATION  (
                            "Citation",
                            "citationviolation",
                            "", 
                            "citationviolationid", 
                            "codeviolation_violationid",
                            null,
                            "citationviolation_cvid_seq",
                            LinkedObjectFamilyEnum.MAILING, // this doesn't make any sense here
                            false,
                            FocusedObjectEnum.CECASE,
                            true,
                            false,
                            2777,
                            true
                            
                        ),
    CEACtionRequestHuman  (
                            "Code Enf Action Request",
                            "public.ceactionrequesthuman",
                            "CEACtionRequestHuman", 
                            "linkid", 
                            "ceactionrequest_requestid",
                            "human_humanid",
                            "ceactionrequesthuman_seq",
                            LinkedObjectFamilyEnum.HUMAN,
                            false,
                            FocusedObjectEnum.CECASE,
                            true,
                            false,
                            1567,
                            true
                            
                        ),
    
    HumanHuman  (
                            "Person",
                            "public.humanhuman",
                            "HumanHuman", 
                            "linkid", 
                            "humantarget_humanid",
                            "human_humanid",
                            "humanhuman_linkid_seq",
                            LinkedObjectFamilyEnum.HUMAN,
                            true,
                            FocusedObjectEnum.CECASE,
                            true,
                            false,
                            4603,
                            false
                            
                        );

    
    private final String TARGET_OBJECT_FRIENDLY_NAME;
    private final String LINKING_TABLE_NAME;
    private final String LINK_ROLE_SCHEMA_TYPE_STRING;
    
    private final String LINKING_TABLE_PK_FIELD;
    private final String TARGET_TABLE_FK_FIELD;
    private final String LINKED_OBJECT_FK_FIELD;  // This is only used with address links
                                                    // human_humanid is hard-coded for human links!! grrr
    private final String LINKING_TABLE_SEQ_ID;
    private final LinkedObjectFamilyEnum FAMILY;
    private final boolean ALLOW_CROSS_MUNI_VIEW;
    private final FocusedObjectEnum CROSS_MUNI_OBJECT_PAGE_VIEW;
    private final boolean ACTIVELINK;
    private final boolean FORCE_LINK_FROM_UPSTREAM_POOL;
    /**
     * Created to cope with the exceptional case of a human-human link which
     * doesn't have a containing parent object with a dedicated page for JSF routing.
     * As of march 2024, only HumanHuman links have this set to false
     */
    private final boolean HAS_NAVIGABLE_PARENT;
    /**
     * Used during caching - this is multiplied by the
     * linkID for a unique cache key
     */
    private final int HASH_KEY;

    private LinkedObjectSchemaEnum   (
                                        String friendly,
                                        String ltn, 
                                        String ts, 
                                        String ltpk, 
                                        String ttfk,
                                        String lofk,
                                        String seqid,
                                        LinkedObjectFamilyEnum fam,
                                        boolean xmuniv,
                                        FocusedObjectEnum foe,
                                        boolean active,
                                        boolean forcePoolLink,
                                        int hk,
                                        boolean parentNav
                                    )    {
        TARGET_OBJECT_FRIENDLY_NAME = friendly;
        LINKING_TABLE_NAME = ltn;
        LINK_ROLE_SCHEMA_TYPE_STRING = ts;
        LINKING_TABLE_PK_FIELD = ltpk;
        TARGET_TABLE_FK_FIELD = ttfk;
        LINKED_OBJECT_FK_FIELD = lofk;
        LINKING_TABLE_SEQ_ID = seqid;
        FAMILY = fam;
        ALLOW_CROSS_MUNI_VIEW = xmuniv;
        CROSS_MUNI_OBJECT_PAGE_VIEW = foe;
        ACTIVELINK = active;
        FORCE_LINK_FROM_UPSTREAM_POOL = forcePoolLink;
        HASH_KEY = hk;
        HAS_NAVIGABLE_PARENT = parentNav;
    }
    
    public String getLinkingTableName(){
        return LINKING_TABLE_NAME;
    }
    
    public String getRoleSChemaTypeString(){
        return LINK_ROLE_SCHEMA_TYPE_STRING;
    }
    
    public String getLinkingTablePKField(){
        return LINKING_TABLE_PK_FIELD;
    }
    
    public String getTargetTableFKField(){
        return TARGET_TABLE_FK_FIELD;
    }
    
    public String getLinkingTableSequenceID(){
        return LINKING_TABLE_SEQ_ID;
    
    }
    public LinkedObjectFamilyEnum getLinkedObjectFamilyEnum(){
        return FAMILY;
    }

    /**
     * @return the TARGET_OBJECT_FRIENDLY_NAME
     */
    public String getTARGET_OBJECT_FRIENDLY_NAME() {
        return TARGET_OBJECT_FRIENDLY_NAME;
    }

    /**
     * @return the ACTIVELINK
     */
    public boolean isACTIVELINK() {
        return ACTIVELINK;
    }

    /**
     * @return the LINKED_OBJECT_FK_FIELD
     */
    public String getLINKED_OBJECT_FK_FIELD() {
        return LINKED_OBJECT_FK_FIELD;
    }

    /**
     * @return the ALLOW_CROSS_MUNI_VIEW
     */
    public boolean isALLOW_CROSS_MUNI_VIEW() {
        return ALLOW_CROSS_MUNI_VIEW;
    }

    /**
     * @return the CROSS_MUNI_OBJECT_PAGE_VIEW
     */
    public FocusedObjectEnum getCROSS_MUNI_OBJECT_PAGE_VIEW() {
        return CROSS_MUNI_OBJECT_PAGE_VIEW;
    }

    /**
     * @return the FORCE_LINK_FROM_UPSTREAM_POOL
     */
    public boolean isFORCE_LINK_FROM_UPSTREAM_POOL() {
        return FORCE_LINK_FROM_UPSTREAM_POOL;
    }

    /**
     * @return the HAS_NAVIGABLE_PARENT
     */
    public boolean isHAS_NAVIGABLE_PARENT() {
        return HAS_NAVIGABLE_PARENT;
    }

    /**
     * @return the HASH_KEY
     */
    public int getHASH_KEY() {
        return HASH_KEY;
    }
    
}
