/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Specifies the actions that the PermissionsCoordinator reviews:
 * View
 * Edit
 * Deactivate
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */
public enum PermissionsGovernedOperationEnum {
    /**
     * Caller wants to know if the accompanying user can see this object
     * and perhaps all its sub-objects as well
     */
    VIEW,
    /**
     * Caller inquiry: Can the user start the process of making a fresh 
     * object of this type?
     */
    INIT,
    /**
     * Can user undertake any kind of edit/update operation, excluding
     * deactivation. See DEAC
     */
    EDIT,
    /**
     * Caller inquiry: Can user deactivate the associated object.
     */
    DEAC;
}
