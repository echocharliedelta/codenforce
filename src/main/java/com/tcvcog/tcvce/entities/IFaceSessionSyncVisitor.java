/*
 * Copyright (C) 2025 echocdelta
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.entities.occupancy.OccPeriodDataHeavy;
import com.tcvcog.tcvce.entities.occupancy.OccPermit;

/**
 * Works with IFaceSessionSyncTarget and IFaceSessionSynchronizer to provide
 * session object synchronization using the Visitor pattern. With help from THE
 * great and terrible ChatGPT
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */
public interface IFaceSessionSyncVisitor {
    
    public void visit(PropertyDataHeavy pdh);
    public void visit(Person p);
    public void visit(CECaseDataHeavy csedh);
    public void visit(OccPeriodDataHeavy opdh);
    public void visit(OccPermit permit);
    
}
