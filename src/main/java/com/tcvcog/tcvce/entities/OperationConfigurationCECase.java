/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import java.util.List;

/**
 * Intermediate superclass for CE Case operations such as a closure or reopening
 * 
 * @author pierre15
 * @param <T>
 */
public abstract class OperationConfigurationCECase<T extends CECaseDataHeavy> extends OperationConfiguration<T> {
    
    
    private List<EventCategory> documentingEventCandidateEventCategoryList;
    private EventCategory documentingEventCategorySelected;
    private String operationReason;

    
    public OperationConfigurationCECase(T target, UserAuthorized requestor){
        super(target, requestor);
        
    }
  

    /**
     * @return the documentingEventCandidateEventCategoryList
     */
    public List<EventCategory> getDocumentingEventCandidateEventCategoryList() {
        return documentingEventCandidateEventCategoryList;
    }

    /**
     * @param documentingEventCandidateEventCategoryList the documentingEventCandidateEventCategoryList to set
     */
    public void setDocumentingEventCandidateEventCategoryList(List<EventCategory> documentingEventCandidateEventCategoryList) {
        this.documentingEventCandidateEventCategoryList = documentingEventCandidateEventCategoryList;
    }

    /**
     * @return the documentingEventCategorySelected
     */
    public EventCategory getDocumentingEventCategorySelected() {
        return documentingEventCategorySelected;
    }

    /**
     * @param documentingEventCategorySelected the documentingEventCategorySelected to set
     */
    public void setDocumentingEventCategorySelected(EventCategory documentingEventCategorySelected) {
        this.documentingEventCategorySelected = documentingEventCategorySelected;
    }

    /**
     * @return the operationReason
     */
    public String getOperationReason() {
        return operationReason;
    }

    /**
     * @param operationReason the operationReason to set
     */
    public void setOperationReason(String operationReason) {
        this.operationReason = operationReason;
    }
    
}
