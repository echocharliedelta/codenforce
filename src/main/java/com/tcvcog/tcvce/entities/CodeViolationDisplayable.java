/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.entities;

/**
 *
 * @author sylvia
 */
public class CodeViolationDisplayable extends CodeViolation {
    
    public CodeViolationDisplayable(){
        
    }
    
    public CodeViolationDisplayable(CodeViolation cv){
        super(cv);
        
        includeViolationPhotos = true;

    }

    private boolean includeOrdinanceText;
    private boolean includeHumanFriendlyText;
    private boolean includeViolationPhotos;

    /**
     * @return the includeOrdinanceText
     */
    public boolean isIncludeOrdinanceText() {
        return includeOrdinanceText;
    }

    /**
     * @param includeOrdinanceText the includeOrdinanceText to set
     */
    public void setIncludeOrdinanceText(boolean includeOrdinanceText) {
        this.includeOrdinanceText = includeOrdinanceText;
    }

    /**
     * @return the includeHumanFriendlyText
     */
    public boolean isIncludeHumanFriendlyText() {
        return includeHumanFriendlyText;
    }

    /**
     * @return the includeViolationPhotos
     */
    public boolean isIncludeViolationPhotos() {
        return includeViolationPhotos;
    }

    /**
     * @param includeHumanFriendlyText the includeHumanFriendlyText to set
     */
    public void setIncludeHumanFriendlyText(boolean includeHumanFriendlyText) {
        this.includeHumanFriendlyText = includeHumanFriendlyText;
    }

    /**
     * @param includeViolationPhotos the includeViolationPhotos to set
     */
    public void setIncludeViolationPhotos(boolean includeViolationPhotos) {
        this.includeViolationPhotos = includeViolationPhotos;
    }
    
}
