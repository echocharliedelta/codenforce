/*
 * Copyright (C) 2021 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import java.util.List;

/**
 * Specifies a setter and getter for a list of linked humans
 * @author Ellen Bascomb
 */
public interface IFace_humanListHolder {
    
    public List<HumanLink> gethumanLinkList();
    public void sethumanLinkList(List<HumanLink> hll);
    public LinkedObjectSchemaEnum getHUMAN_LINK_SCHEMA_ENUM();
    /**
     * The PK field of the object who holds a list of Humans as linked objects
     * @return 
     */
    public int getHostPK();
    /**
     * Implementer is responsible for assembling a String that gives viewers
     * of human links some identifying context for the parent object, 
     * such as the property's address or the CE case's name or the CEAR's issue & status
     * @return 
     */
    public String getDescriptionString();

    /**
     * HumanLInk holders tell clients through this method
     * which HumanLink holder's human links should be assembled
     * as a candidate pool. This enum's value is realized by
     * also calling getUpstreamHumanLinkPoolFeederID() also on this
     * interface to figure out exactly which object's links should
     * be pooled.
     * 
     * Implementing classes that do not wish to be populated from a Human Link
     * pool should return null.
     * 
     * @return the enum describing the implementing object's HumanLink pool
       Implementing classes that do not wish to be populated from a Human Link
     * pool should return null and no pool will be assembled and obviously
     * no feeder pool restriction will be enforced.
     * 
     */
    public LinkedObjectSchemaEnum getUpstreamHumanLinkPoolEnum();
   
    /**
     * Tool used for assembling the implementing object's upstream
     * person link pool.
     * @return the ID of the pool creator--the parent or 0 if no pool
     * should be assembled
     */
    public int getUpstreamHumanLinkPoolFeederID();
    
    /**
     * unified method to return a boolean signaling this implementers active 
     * status. Inactive objects cannot receive new links
     * @return if the implementing object is active
     */
    public boolean isActive();
    
}
