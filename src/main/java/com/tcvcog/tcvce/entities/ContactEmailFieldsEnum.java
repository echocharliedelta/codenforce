package com.tcvcog.tcvce.entities;

public enum ContactEmailFieldsEnum {
    EMAILADDRESS("EmailAddress", "Email Address"),
    PRIORITY("Priority", "Priority"),
    BOUNCETS("BounceTS", "Bounce");

    private final String fieldName;
    private final String friendlyName;

    private ContactEmailFieldsEnum(String fieldName, String friendlyName) {
        this.fieldName = fieldName;
        this.friendlyName = friendlyName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

}
