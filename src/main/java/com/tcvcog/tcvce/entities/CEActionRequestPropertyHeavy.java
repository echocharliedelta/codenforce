/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * A property carrying subclass of CEActionRequest
 * @author sylvia
 */
public class CEActionRequestPropertyHeavy 
        extends CEActionRequest{
    
    private Property requestProperty;
    
    public CEActionRequestPropertyHeavy(CEActionRequest cear){
        super(cear);
    }
    
    public CEActionRequestPropertyHeavy(CEActionRequestPropertyHeavy cear){
        super(cear);
        this.requestProperty = cear.requestProperty;
    }

    /**
     * @return the requestProperty
     */
    public Property getRequestProperty() {
        return requestProperty;
    }

    /**
     * @param requestProperty the requestProperty to set
     */
    public void setRequestProperty(Property requestProperty) {
        this.requestProperty = requestProperty;
    }
    
}
