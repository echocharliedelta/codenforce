/*
 * Copyright (C) 2019 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.entities.occupancy.OccPeriod;
import com.tcvcog.tcvce.entities.occupancy.OccPeriodDataHeavy;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sylvia
 */
public class        PropertyUnitDataHeavy 
        extends     PropertyUnit
        implements  IFace_humanListHolder,
                    IFace_addressListHolder{
    final static LinkedObjectSchemaEnum HUMAN_LINK_SCHEMA_ENUM = LinkedObjectSchemaEnum.ParcelUnitHuman;
    final static LinkedObjectSchemaEnum HUMAN_LINK_POOL_ENUM = LinkedObjectSchemaEnum.ParcelHuman;

    protected List<OccPeriodDataHeavy> periodList;
    protected List<HumanLink> humanLinkList;
    protected List<MailingAddressLink> mailingAddressLinkList;
    protected List<PropertyUnitChangeOrder> changeOrderList;
    
    
    /**
     * Convenience method for extracting the first mailing address attached to this unit
     * @return 
     */
    public MailingAddressLink getPrimaryMailingAddressLink(){
        if(mailingAddressLinkList != null && !mailingAddressLinkList.isEmpty()){
            return mailingAddressLinkList.get(0);
        }
        return null;
    }
    
    /**
     * Pre-Credential Requiring constructor
     * @param prop 
     */
    public PropertyUnitDataHeavy(PropertyUnit prop){
        super(prop);
        
    }

    public PropertyUnitDataHeavy() {
        periodList = new ArrayList<>();
    }
    
    
  
    /**
     * @return the periodList
     */
    public List<OccPeriodDataHeavy> getPeriodList() {
        return periodList;
    }

    /**
     * @param periodList the periodList to set
     */
    public void setPeriodList(List<OccPeriodDataHeavy> periodList) {
        this.periodList = periodList;
    }

   

    public List<PropertyUnitChangeOrder> getChangeOrderList() {
        return changeOrderList;
    }

    public void setChangeOrderList(List<PropertyUnitChangeOrder> changeOrderList) {
        this.changeOrderList = changeOrderList;
    }
    
    @Override
    public String getParentHumanFriendlyDescriptiveString() {
        StringBuilder sb = new StringBuilder("Property Unit");
        if(getPrimaryMailingAddressLink() != null){
            sb.append(" at address: ");
            sb.append(getPrimaryMailingAddressLink().getAddressPretty1Line());
        }
        
        return sb.toString();
        
    }
  

    
    @Override
    public List<HumanLink> gethumanLinkList() {
        return humanLinkList;
    }

    @Override
    public void sethumanLinkList(List<HumanLink> hll) {
        humanLinkList = hll;
    }

    @Override
    public LinkedObjectSchemaEnum getHUMAN_LINK_SCHEMA_ENUM() {
        return HUMAN_LINK_SCHEMA_ENUM;
    }


    @Override
    public int getHostPK() {
        return unitID;
    }

    @Override
    public String getDescriptionString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getUnitNumber());
        return sb.toString();
    }

    @Override
    public LinkedObjectSchemaEnum getUpstreamHumanLinkPoolEnum() {
        return HUMAN_LINK_POOL_ENUM;
    }

    @Override
    public int getUpstreamHumanLinkPoolFeederID() {
        return parcelKey;
    }

    @Override
    public List<MailingAddressLink> getMailingAddressLinkList() {
        return mailingAddressLinkList;
    }

    @Override
    public void setMailingAddressLinkList(List<MailingAddressLink> ll) {
        mailingAddressLinkList = ll;
    }

    @Override
    public LinkedObjectSchemaEnum getLinkedObjectSchemaEnum() {
        return LinkedObjectSchemaEnum.ParcelUnitMailingAddress;
    }

    @Override
    public int getTargetObjectPK() {
        return unitID;
    }
    
    
}
