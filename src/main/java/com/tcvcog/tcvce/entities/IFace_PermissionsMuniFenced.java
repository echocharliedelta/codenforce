/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Permissions child interface declaring the implementing object
 * as available for viewing/edit/deac by the requesting user
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */
public interface IFace_PermissionsMuniFenced 
        extends IFace_PermissionsGoverned{
    
    /**
     * TO be fenced within its own muni, Implementing classes must return 
     * its parent muni
     * @return this object's governing Municipality for check 
     * against requesting user UMAPs
     */
  public Municipality getGoverningMunicipality();
}
