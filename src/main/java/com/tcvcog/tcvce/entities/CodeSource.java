/*
 * Copyright (C) 2018 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import java.io.Serializable;
import java.util.Objects;

/**
 * An entity that sources government ordinances
 * which are wrapped in enforce-ability blankets
 * containing municipality-specific implementation
 * of potentially shared code sources.
 * 
 * Code sources designated as cross-muni can only be edited by users who meet
 * a rank floor held by the MunicipalityCoordinator which as of 1.0.8 was SYSADMIN
 * 
 * Interestingly, as of August 2024 during clone ord operation, 
 * this object doesn't actually contain in its belly all its code elements!!
 * 
 * 
 * @author ellen bascomb of apt 31y
 */
public  class       CodeSource
        extends UMAPTrackedEntity
        implements  Serializable,
                    IFaceCachable,
                    IFace_noteHolder{
    
    final static UMAPTrackedEnum TRACKED_ENUM = UMAPTrackedEnum.CODESOURCE;
    
    private int sourceID;
    private String sourceName;
    private int sourceYear;
    
    private String sourceDescription;
    private String url;
    private String sourceNotes;
    
    @Override
    public int getCacheKey() {
        return sourceID;
    }

    /**
     * Created to avoid DB cycles, since munis can have sources
     */
    private String muniNameFlattened;
    /**
     * Created to avoid DB cycles, since munis can have sources
     */
    private int muniCodeFlattened;
    
    private boolean crossMuni;
    
    final static String TABLE_CODE_SOURCE = "codesource";
    final static String CODE_SOURCE_PK_FIELD = "sourceid";
    
    /**
     * @return the sourceID
     */
    public int getSourceID() {
        return sourceID;
    }

    /**
     * @param sourceID the sourceID to set
     */
    public void setSourceID(int sourceID) {
        this.sourceID = sourceID;
    }

    /**
     * @return the sourceName
     */
    public String getSourceName() {
        return sourceName;
    }

    /**
     * @param sourceName the sourceName to set
     */
    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    /**
     * @return the sourceYear
     */
    public int getSourceYear() {
        return sourceYear;
    }

    /**
     * @param sourceYear the sourceYear to set
     */
    public void setSourceYear(int sourceYear) {
        this.sourceYear = sourceYear;
    }

    /**
     * @return the sourceDescription
     */
    public String getSourceDescription() {
        return sourceDescription;
    }

    /**
     * @param sourceDescription the sourceDescription to set
     */
    public void setSourceDescription(String sourceDescription) {
        this.sourceDescription = sourceDescription;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the sourceNotes
     */
    public String getSourceNotes() {
        return sourceNotes;
    }

    /**
     * @param sourceNotes the sourceNotes to set
     */
    public void setSourceNotes(String sourceNotes) {
        this.sourceNotes = sourceNotes;
    }

    /**
     * @return the isActive
     */
    public boolean isIsActive() {
        return this.deactivatedTS == null;
    }

    
    
    @Override
    public int hashCode() {
        int hash = 10;
        hash = 300 * hash + this.sourceID;
        hash = 300 * hash + this.sourceYear;
        hash = 300 * hash + Objects.hashCode(this.sourceName);
        hash = 300 * hash + Objects.hashCode(this.sourceDescription);
        hash = 300 * hash + Objects.hashCode(this.url);
        hash = 300 * hash + Objects.hashCode(this.sourceNotes);
        
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CodeSource other = (CodeSource) obj;
        if (this.sourceID != other.sourceID) {
            return false;
        }
   
        return true;
    }
    
    /**
     * Builds a string: SourceName (year)
     * @return 
     */
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder(sourceName);
        if(sourceYear != 0){
            sb.append(" (");
            sb.append(sourceYear);
            sb.append(")");
        }
        return sb.toString();
    }

    /**
     * @return the crossMuni
     */
    public boolean isCrossMuni() {
        return crossMuni;
    }

    /**
     * @param crossMuni the crossMuni to set
     */
    public void setCrossMuni(boolean crossMuni) {
        this.crossMuni = crossMuni;
    }

   

    @Override
    public UMAPTrackedEnum getUMAPTrackedEntityEnum() {
        return TRACKED_ENUM;
    }

    @Override
    public String getPKFieldName() {
        return CODE_SOURCE_PK_FIELD;
    }

    @Override
    public int getDBKey() {
        return sourceID;
    }

    @Override
    public String getDBTableName() {
        return TABLE_CODE_SOURCE;
    }

    /**
     * @return the muniNameFlattened
     */
    public String getMuniNameFlattened() {
        return muniNameFlattened;
    }

    /**
     * @param muniNameFlattened the muniNameFlattened to set
     */
    public void setMuniNameFlattened(String muniNameFlattened) {
        this.muniNameFlattened = muniNameFlattened;
    }

    /**
     * @return the muniCodeFlattened
     */
    public int getMuniCodeFlattened() {
        return muniCodeFlattened;
    }

    /**
     * @param muniCodeFlattened the muniCodeFlattened to set
     */
    public void setMuniCodeFlattened(int muniCodeFlattened) {
        this.muniCodeFlattened = muniCodeFlattened;
    }

    @Override
    public String getNotes() {
        return sourceNotes;
    }

    @Override
    public void setNotes(String n) {
        sourceNotes = n;
    }

    @Override
    public String getNoteHolderFriendlyName() {
        return "Code Source";
        
    }

    
}
