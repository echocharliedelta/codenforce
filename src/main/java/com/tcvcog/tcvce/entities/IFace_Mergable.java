/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Marker interface for objects that can be merged
 * in the database. Use of this interface in backing bean code is
 * not required. The first round of use pertained to Streets and MailingAddresses
 * and their associated backing bean methods. 
 * 
 * Person merging is more complex and the marker interface isn't used in any
 * handy fashion.
 * 
 * @author Ellen Bascomb of apartment 31Y
 */
public interface IFace_Mergable extends IFace_keyIdentified {
    
    /**
     * The toString equivalent
     * @return 
     */
    public String getPrimaryDescriptiveString();
    
    /**
     * A more descriptive string
     * @return 
     */
    public String getSecondaryDescriptiveString();
    
    
}
