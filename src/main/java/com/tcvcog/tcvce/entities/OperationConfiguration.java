/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.util.Constants;
import java.time.LocalDateTime;

/**
 * Root of the OperationConfiguration family of objects
 * @author pierre15
 * @param <T>
 */
public abstract class OperationConfiguration<T extends BOb> {
    
    protected UserAuthorized requestingUser;
    protected UserAuthorized userOfRecord;
    protected LocalDateTime dateOfRecord;
    
    protected boolean allowedToProceed;
    protected LocalDateTime operationAttemptTS;
    protected LocalDateTime operationSuccessTS;
    
    protected T operationTarget;
    protected T refreshedOperationTarget;
    
    private StringBuilder logBuilder = new StringBuilder();
    
    /**
     * At time of creation all OperationConfigurations need a target
     * @param target
     * @param requestor 
     */
    public OperationConfiguration(T target, UserAuthorized requestor){
        operationTarget = target;
        requestingUser = requestor;
    }
    
    
    /**
     * Writes to our operation log
     * @param s
     * @return 
     */
    public StringBuilder appendToOpLog(String s){
        if(logBuilder != null){
            return logBuilder.append(s);
        } else {
            return new StringBuilder(s);
        }
        
    }
    
    /**
     * Erases current op log
     */
    public void flushOpLog(){
        logBuilder = new StringBuilder();
    }
    
    /**
     * Returns a string, hopefully with actually logged data. Will never be null;
     * @return 
     */
    public String getOperationLog(){
        if(logBuilder != null){
            return logBuilder.toString();
        } else {
            return Constants.NO_LOG;
        }
    }
    
    
    
    /**
     * Operations are going to be requestable by anybody 
     * and the user of record can be different, such as when
     * an admin assistant is doing work on behalf of an officer.
     * 
     * This should only be the session's current UserAuthorized.
     * 
     * @return 
     */
    public UserAuthorized getRequestingUser(){
        return requestingUser;
    }
    
    
    /**
     * This may be different from the requesting user.
     * @return 
     */
    public UserAuthorized getUserOfRecord(){
        return userOfRecord;
    }

    /**
     * @param userOfRecord the userOfRecord to set
     */
    public void setUserOfRecord(UserAuthorized userOfRecord) {
        this.userOfRecord = userOfRecord;
    }

    /**
     * @param requestingUser the requestingUser to set
     */
    public void setRequestingUser(UserAuthorized requestingUser) {
        this.requestingUser = requestingUser;
    }

    /**
     * @return the dateOfRecord
     */
    public LocalDateTime getDateOfRecord() {
        return dateOfRecord;
    }

    /**
     * @param dateOfRecord the dateOfRecord to set
     */
    public void setDateOfRecord(LocalDateTime dateOfRecord) {
        this.dateOfRecord = dateOfRecord;
    }

    /**
     * @return the operationSuccessTS
     */
    public LocalDateTime getOperationSuccessTS() {
        return operationSuccessTS;
    }

    /**
     * @param operationSuccessTS the operationSuccessTS to set
     */
    public void setOperationSuccessTS(LocalDateTime operationSuccessTS) {
        this.operationSuccessTS = operationSuccessTS;
    }

    /**
     * @return the operationTarget
     */
    public T getOperationTarget() {
        return operationTarget;
    }

    /**
     * @param operationTarget the operationTarget to set
     */
    public void setOperationTarget(T operationTarget) {
        this.operationTarget = operationTarget;
    }

    /**
     * @return the allowedToProceed
     */
    public boolean isAllowedToProceed() {
        return allowedToProceed;
    }

    /**
     * @param allowedToProceed the allowedToProceed to set
     */
    public void setAllowedToProceed(boolean allowedToProceed) {
        this.allowedToProceed = allowedToProceed;
    }

    /**
     * @return the operationAttemptTS
     */
    public LocalDateTime getOperationAttemptTS() {
        return operationAttemptTS;
    }

    /**
     * @param operationAttemptTS the operationAttemptTS to set
     */
    public void setOperationAttemptTS(LocalDateTime operationAttemptTS) {
        this.operationAttemptTS = operationAttemptTS;
    }

    /**
     * @return the refreshedOperationTarget
     */
    public T getRefreshedOperationTarget() {
        return refreshedOperationTarget;
    }

    /**
     * @param refreshedOperationTarget the refreshedOperationTarget to set
     */
    public void setRefreshedOperationTarget(T refreshedOperationTarget) {
        this.refreshedOperationTarget = refreshedOperationTarget;
    }
    
    
    
}
