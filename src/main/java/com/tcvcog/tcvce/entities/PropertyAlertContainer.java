/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Stores active and inactive property alerts
 *
 * @author pierre15
 */
public class PropertyAlertContainer {

    private final List<PropertyAlert> alertList;
    private List<PropertyAlert> alertListActive;

    /**
     * Convenience method for returning true if there are active alerts
     *
     * @return
     */
    public boolean isRenderAlerts() {
        return alertListActive != null && !alertListActive.isEmpty();
    }

    /**
     * Constructor and configuration mechanism for this alert conatiner:
     * I'll take in the alert list and decide which are active, and place
     * that active subset in alertListActive
     * @param alertList 
     */
    public PropertyAlertContainer(List<PropertyAlert> alertList) {
        this.alertList = alertList;
        alertListActive = new ArrayList<>();
        if (alertList != null && !alertList.isEmpty()) {
            for (PropertyAlert alert : alertList) {
                if (alert.isAlertActive()) {
                    alertListActive.add(alert);
                }
            }
        }
        
    }

    /**
     * returns only active alerts
     *
     * @return
     */
    public List<PropertyAlert> getAlertsActive() {

        return alertListActive;
    }

   

    /**
     * @return the alertList
     */
    public List<PropertyAlert> getAlertList() {
        return alertList;
    }


}
