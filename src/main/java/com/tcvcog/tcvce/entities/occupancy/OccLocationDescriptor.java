/*
 * Copyright (C) 2018 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities.occupancy;

import com.tcvcog.tcvce.entities.Municipality;
import java.time.LocalDateTime;
import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import java.util.Objects;

/**
 *
 * @author sylvia
 */
public class OccLocationDescriptor implements IFaceCachable{
    
    final static String DB_PK_FIELD = "locationdescriptionid";
    final static String LOC_TABLE = "occlocationdescriptor";
    
    private int locationID;
    private String locationDescription;
    
    /**
     * @Deprecated replaced by a floor no on each space
     */
    private int buildingFloorNo;
    private Municipality muni;
    
    private String locationString1Line;
    private String locationString2LineEscapeFalse;
    private final int defaultLocID;
    
    private LocalDateTime deactivatedTS;
    
    /**
     * Constructor
     * @param dlid default location id which is used to generate the return value isDefaultLocation
     */
    public OccLocationDescriptor(int dlid){
        defaultLocID = dlid;
    }

    /**
     * hacky getter with logic for comparing to the default ID
     * @return the defaultLocation
     */
    public boolean isDefaultLocation() {
        return locationID == defaultLocID;
    }
    
    /**
     * @return the locationID
     */
    public int getLocationID() {
        return locationID;
    }

    /**
     * @return the locationDescription
     */
    public String getLocationDescription() {
        return locationDescription;
    }

    /**
     * @param locationID the locationID to set
     */
    public void setLocationID(int locationID) {
        this.locationID = locationID;
    }

    /**
     * @param locationDescription the locationDescription to set
     */
    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

   

    /**
     * @return the buildingFloorNo
     */
    public int getBuildingFloorNo() {
        return buildingFloorNo;
    }

    /**
     * @param buildingFloorNo the buildingFloorNo to set
     */
    public void setBuildingFloorNo(int buildingFloorNo) {
        this.buildingFloorNo = buildingFloorNo;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.locationID;
        hash = 97 * hash + Objects.hashCode(this.locationDescription);
        hash = 97 * hash + this.buildingFloorNo;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OccLocationDescriptor other = (OccLocationDescriptor) obj;
        if (this.locationID != other.locationID) {
            return false;
        }
        return true;
    }

    /* ****************************************************************
     * *********            CACHING                         ***********
     * ****************************************************************
     */
    
    @Override
    public int getCacheKey() {
        return locationID;
    }

    
    /**
     * @return the locationString2LineEscapeFalse
     */
    public String getLocationString2LineEscapeFalse() {
        return locationString2LineEscapeFalse;
    }

    /**
     * @param locationString2LineEscapeFalse the locationString2LineEscapeFalse to set
     */
    public void setLocationString2LineEscapeFalse(String locationString2LineEscapeFalse) {
        this.locationString2LineEscapeFalse = locationString2LineEscapeFalse;
    }

    /**
     * @return the locationString1Line
     */
    public String getLocationString1Line() {
        return locationString1Line;
    }

    /**
     * @param locationString1Line the locationString1Line to set
     */
    public void setLocationString1Line(String locationString1Line) {
        this.locationString1Line = locationString1Line;
    }

    /**
     * @return the muni
     */
    public Municipality getMuni() {
        return muni;
    }

    /**
     * @param muni the muni to set
     */
    public void setMuni(Municipality muni) {
        this.muni = muni;
    }

    /**
     * @return the deactivatedTS
     */
    public LocalDateTime getDeactivatedTS() {
        return deactivatedTS;
    }

    /**
     * @param deactivatedTS the deactivatedTS to set
     */
    public void setDeactivatedTS(LocalDateTime deactivatedTS) {
        this.deactivatedTS = deactivatedTS;
    }

  
    
    
    
}
