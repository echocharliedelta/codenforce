/*
 * Copyright (C) 2021 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities.occupancy;

import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.UMAPTrackedEntity;
import com.tcvcog.tcvce.entities.UMAPTrackedEnum;
import java.io.Serializable;
import java.util.Objects;

public class OccInspectionCause extends UMAPTrackedEntity implements Serializable, IFaceCachable {

    private int causeID;

    private Municipality muni;
    
    private String title;
    private String description;

    private String notes;
    
    private final static String TABLE_NAME = "occinspectioncause";
    private final static String OCCINSPECTIONCAUSE_PK = "causeid";
    private final static UMAPTrackedEnum UMAP_TRACKED_ENUM = UMAPTrackedEnum.OCC_INSPECTION_CAUSE;

    public OccInspectionCause() {}

    public OccInspectionCause(OccInspectionCause occInspectionCause) {
        this.causeID = occInspectionCause.getCauseID();
        this.title = occInspectionCause.getTitle();
        this.description = occInspectionCause.getDescription();
        this.notes = occInspectionCause.getNotes();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + this.causeID;
        hash = 37 * hash + Objects.hashCode(this.muni);
        hash = 37 * hash + Objects.hashCode(this.title);
        hash = 37 * hash + Objects.hashCode(this.description);
        hash = 37 * hash + Objects.hashCode(this.notes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OccInspectionCause other = (OccInspectionCause) obj;
        return this.causeID == other.causeID;
    }

    /* ****************************************************************
     * *********            CACHING                         ***********
     * ****************************************************************
     */
    
    @Override
    public int getCacheKey() {
        return causeID;
    }

    
    public int getCauseID() {
        return causeID;
    }

    public void setCauseID(int causeID) {
        this.causeID = causeID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
    
    @Override
    public String getDBTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPKFieldName() {
        return OCCINSPECTIONCAUSE_PK;
    }

    @Override
    public int getDBKey() {
        return causeID;
    }

    @Override
    public UMAPTrackedEnum getUMAPTrackedEntityEnum() {
        return UMAP_TRACKED_ENUM;
    }

    /**
     * @return the muni
     */
    public Municipality getMuni() {
        return muni;
    }

    /**
     * @param muni the muni to set
     */
    public void setMuni(Municipality muni) {
        this.muni = muni;
    }
}
