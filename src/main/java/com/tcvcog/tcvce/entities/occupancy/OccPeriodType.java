/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities.occupancy;

import com.tcvcog.tcvce.entities.EventCategory;
import com.tcvcog.tcvce.occupancy.application.TaskChain;
import com.tcvcog.tcvce.entities.Municipality;
import java.util.List;

/**
 * Represents a classification point for an occupancy period
 * @author pierre15
 */
public class OccPeriodType {
    
    private int occPeriodTypeID;
    private String title;
    private String description;
    private TaskChain chain;
    private Municipality muni;
    private boolean active;
    private List<OccPermitType> permitTypes;
    private EventCategory defaultOriginationEventCategory;
    private int reinspectionWindowDays;
    
    private OccChecklistTemplate defaultInspectionChecklist;

    /**
     * @return the occPeriodTypeID
     */
    public int getOccPeriodTypeID() {
        return occPeriodTypeID;
    }

    /**
     * @param occPeriodTypeID the occPeriodTypeID to set
     */
    public void setOccPeriodTypeID(int occPeriodTypeID) {
        this.occPeriodTypeID = occPeriodTypeID;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the chain
     */
    public TaskChain getChain() {
        return chain;
    }

    /**
     * @param chain the chain to set
     */
    public void setChain(TaskChain chain) {
        this.chain = chain;
    }

    /**
     * @return the muni
     */
    public Municipality getMuni() {
        return muni;
    }

    /**
     * @param muni the muni to set
     */
    public void setMuni(Municipality muni) {
        this.muni = muni;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * @return the permitTypes
     */
    public List<OccPermitType> getPermitTypes() {
        return permitTypes;
    }

    /**
     * @param permitTypes the permitTypes to set
     */
    public void setPermitTypes(List<OccPermitType> permitTypes) {
        this.permitTypes = permitTypes;
    }

    /**
     * @return the reinspectionWindowDays
     */
    public int getReinspectionWindowDays() {
        return reinspectionWindowDays;
    }

    /**
     * @param reinspectionWindowDays the reinspectionWindowDays to set
     */
    public void setReinspectionWindowDays(int reinspectionWindowDays) {
        this.reinspectionWindowDays = reinspectionWindowDays;
    }

    /**
     * @return the defaultOriginationEventCategory
     */
    public EventCategory getDefaultOriginationEventCategory() {
        return defaultOriginationEventCategory;
    }

    /**
     * @param defaultOriginationEventCategory the defaultOriginationEventCategory to set
     */
    public void setDefaultOriginationEventCategory(EventCategory defaultOriginationEventCategory) {
        this.defaultOriginationEventCategory = defaultOriginationEventCategory;
    }

    /**
     * @return the defaultInspectionChecklist
     */
    public OccChecklistTemplate getDefaultInspectionChecklist() {
        return defaultInspectionChecklist;
    }

    /**
     * @param defaultInspectionChecklist the defaultInspectionChecklist to set
     */
    public void setDefaultInspectionChecklist(OccChecklistTemplate defaultInspectionChecklist) {
        this.defaultInspectionChecklist = defaultInspectionChecklist;
    }
    
}
