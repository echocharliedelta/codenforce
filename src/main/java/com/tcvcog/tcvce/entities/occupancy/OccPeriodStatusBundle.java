/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities.occupancy;

/**
 * Holds an Occupancy period's status enum and an assignment log
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */
public class OccPeriodStatusBundle {
    
    private OccPeriodStatusEnum statusEnum;
    private final StringBuilder statusLog;
    private final StringBuilder statusNote;
    
    final static String HTML_LINEBREAK = "<br />";

    public OccPeriodStatusBundle(){
        statusLog = new StringBuilder();
        statusNote = new StringBuilder();
    }
    
    /**
     * @return the statusEnum
     */
    public OccPeriodStatusEnum getStatusEnum() {
        return statusEnum;
    }

    /**
     * @param statusEnum the statusEnum to set
     */
    public void setStatusEnum(OccPeriodStatusEnum statusEnum) {
        this.statusEnum = statusEnum;
    }

    /**
     * @return the statusLog
     */
    public String getStatusLog() {
        if(statusLog != null){
            return statusLog.toString();
        } else {
            return "";
        }
    }

    /**
     * The string to be appended to the status log. This method will
     * automatically insert the newline after each append
     * @param entry 
     */
    public void appendToStatusLog(String entry) {
        if(this.statusLog != null){
            statusLog.append(entry);
            statusLog.append(HTML_LINEBREAK);
        }
    }

    /**
     * @return the statusNote
     */
    public String getStatusNote() {
        if(statusNote != null){
            return statusNote.toString();
        } else {
            return "";
        }
    }

    /**
     * Tacks on the given string to the user note
     * @param note 
     */
    public void appendToStatusNote(String note) {
        if(statusNote != null){
            statusNote.append(note);
            statusNote.append(HTML_LINEBREAK);
        }
    }
    
}
