/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities.occupancy;

/**
 *
 * @author pierre15
 */
public enum OccPermitGoverningStatusEnum {
    
    GOVERNING_PERMIT_CHOSEN("Governing permit selected", true),
    NO_FINALIZED_PERMIT( "No Finalized permit", false),
    CONFLICTING_PERMITS( "Conflict in governing selection", false);
    
    protected final String title;
    protected final boolean officialSelection;
    
    private OccPermitGoverningStatusEnum(String t, boolean os){
        title = t;
        officialSelection = os;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return the officialSelection
     */
    public boolean isOfficialSelection() {
        return officialSelection;
    }
    
}
