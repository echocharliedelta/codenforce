/*
 * Copyright (C) 2018 Adam Gutonski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities.occupancy;

import com.tcvcog.tcvce.entities.Fee;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.OccPermitPersonListEnum;
import com.tcvcog.tcvce.entities.TextBlock;
import com.tcvcog.tcvce.entities.UMAPTrackedEntity;
import com.tcvcog.tcvce.entities.UMAPTrackedEnum;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;


/**
 *
 * @author Adam Gutonski and Sylvia and Ellen Bascomb
 */
public class OccPermitType 
        extends UMAPTrackedEntity implements Cloneable, Serializable {
    
    private final static String TABLE_NAME = "occpermittype";
    private final static String UNKNOWN_PERSON_LINK_LABEL = "UNLABELED PERSON LINKS";
    private final static String OCCPERMITTYPE_PK = "typeid";
    private final static UMAPTrackedEnum UMAP_TRACKED_ENUM = UMAPTrackedEnum.OCC_PERMIT_TYPE;
    
    private int typeID;
    private Municipality muni;
    private String title;
    private String authorizeduses;
    private String description;
  
    /**
     * Not implemented as of July 2024
     */
    private List<Fee> permittedFees;
    
    private boolean permittable;
    private boolean occupancyFamilyPermit;
    
    private boolean requireInspectionPass;
    
    // person link: left col
    private boolean personColumnLeftActivate;
    private OccPermitPersonListEnum personColumnLeftLabel;
    private boolean requireLPersonLinkLeftCol;
    private String personColumnLeftCustomLabel;
    
    // col connecting text, usually "TO"
    private String personColumnLinkingText;
    
    // person link: right col
    private boolean personColumnRightActivate;
    private OccPermitPersonListEnum personColumnRightLabel;
    private boolean requireLPersonLinkRightCol;
    private String personColumnRightCustomLabel;
    
    // person link: manager col
    private boolean personColumnManagerActivate;
    private boolean requireManager;
    
    // person link: tenant across sale col
    private boolean personColumnTenantActivate;
    private boolean requireTenant;
    
    private boolean requireZeroBalance;
    
    private int baseRuleSetID;
    
    private boolean requireLeaseLink;
    private boolean allowthirdpartyinspection;
    
    private boolean commercial;
    
    private int defaultValidityPeriodDays;
    
    private String permitTitle;
    private boolean permitDisplayMuniAddress;
    
    private boolean expires;

    private List<TextBlock> preaddTextBlocksStipulations;
    private List<TextBlock> preaddTextBlocksNotices;
    private List<TextBlock> preaddTextBlocksComments;
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
    // SPECIAL GETTERS 
    
    /**
     * Form helper getter for configuration of permit left column of person links
     * @return TRUE if this type has a left column label enum and that enum's value is CUSTOM label;
     */
    public boolean isActivateCustomInputPersonLinkLeftCol(){
        return personColumnLeftLabel != null && personColumnLeftLabel == OccPermitPersonListEnum.CUSTOM_PERSON_LABEL;
    }
    
    /**
     * Form helper getter for configuring permit types right column header
     * @return TRUE if this type has a right column label enum and that enum's value is CUSTOM label;
     */
    public boolean isActivateCustomInputPersonLinkRightCol(){
        return personColumnRightLabel != null && personColumnRightLabel == OccPermitPersonListEnum.CUSTOM_PERSON_LABEL;
    }
    
    /**
     * Special adapter getter that will return the person link label that is either the enum value OR 
     * the custom text entered during configuration. 
     * @return A string representing the left column person link list
     */
    public String getPersonColumnLeftGeneratedLabel(){
        if(personColumnLeftLabel != null){
            if(personColumnLeftLabel == OccPermitPersonListEnum.CUSTOM_PERSON_LABEL){
                return personColumnLeftCustomLabel;
            } else {
                return personColumnLeftLabel.getLabel();
            }
        }
        return UNKNOWN_PERSON_LINK_LABEL;
    }
    
    /**
     * Special adapter getter that will return the person link label that is either the enum value OR 
     * the custom text entered during configuration. 
     * @return String representing the right hand column person link list
     */
    public String getPersonColumnRightGeneratedLabel(){
        if(personColumnRightLabel != null){
            if(personColumnRightLabel == OccPermitPersonListEnum.CUSTOM_PERSON_LABEL){
                return personColumnRightCustomLabel;
            } else {
                return personColumnRightLabel.getLabel();
            }
        }
        return UNKNOWN_PERSON_LINK_LABEL;
    }
    
    
    
    // REGULAR GETTERS
    
    /**
     * @return the typeid
     */
    public int getTypeID() {
        return typeID;
    }

    /**
     * @return the muni
     */
    public Municipality getMuni() {
        return muni;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return the authorizeduses
     */
    public String getAuthorizeduses() {
        return authorizeduses;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

   
    /**
     * @return the permittable
     */
    public boolean isPermittable() {
        return permittable;
    }

    /**
     * @return the requireInspectionPass
     */
    public boolean isPassedInspectionRequired() {
        return isRequireInspectionPass();
    }

    /**
     * @return the requireLeaseLink
     */
    public boolean isRequireLeaseLink() {
        return requireLeaseLink;
    }


    /**
     * @return the allowthirdpartyinspection
     */
    public boolean isAllowthirdpartyinspection() {
        return allowthirdpartyinspection;
    }

    /**
     * @return the commercial
     */
    public boolean isCommercial() {
        return commercial;
    }


    /**
     * @param typeid the typeid to set
     */
    public void setTypeID(int typeid) {
        this.typeID = typeid;
    }

    /**
     * @param muni the muni to set
     */
    public void setMuni(Municipality muni) {
        this.muni = muni;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @param authorizeduses the authorizeduses to set
     */
    public void setAuthorizeduses(String authorizeduses) {
        this.authorizeduses = authorizeduses;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

   

    /**
     * @param permittable the permittable to set
     */
    public void setPermittable(boolean permittable) {
        this.permittable = permittable;
    }

    /**
     * @param passedInspectionRequired the requireInspectionPass to set
     */
    public void setPassedInspectionRequired(boolean passedInspectionRequired) {
        this.setRequireInspectionPass(passedInspectionRequired);
    }

    /**
     * @param requireLeaseLink the requireLeaseLink to set
     */
    public void setRequireLeaseLink(boolean requireLeaseLink) {
        this.requireLeaseLink = requireLeaseLink;
    }
    
    /**
     * @param allowthirdpartyinspection the allowthirdpartyinspection to set
     */
    public void setAllowthirdpartyinspection(boolean allowthirdpartyinspection) {
        this.allowthirdpartyinspection = allowthirdpartyinspection;
    }

    /**
     * @param commercial the commercial to set
     */
    public void setCommercial(boolean commercial) {
        this.commercial = commercial;
    }


    /**
     * @return the defaultValidityPeriodDays
     */
    public int getDefaultValidityPeriodDays() {
        return defaultValidityPeriodDays;
    }

    /**
     * @param defaultValidityPeriodDays the defaultValidityPeriodDays to set
     */
    public void setDefaultValidityPeriodDays(int defaultValidityPeriodDays) {
        this.defaultValidityPeriodDays = defaultValidityPeriodDays;
    }


    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + this.typeID;
        hash = 23 * hash + Objects.hashCode(this.muni);
        hash = 23 * hash + Objects.hashCode(this.title);
        hash = 23 * hash + Objects.hashCode(this.authorizeduses);
        hash = 23 * hash + Objects.hashCode(this.description);
        hash = 23 * hash + (this.permittable ? 1 : 0);
        hash = 23 * hash + (this.isRequireInspectionPass() ? 1 : 0);
        hash = 23 * hash + (this.requireLeaseLink ? 1 : 0);
        hash = 23 * hash + (this.allowthirdpartyinspection ? 1 : 0);
        hash = 23 * hash + (this.commercial ? 1 : 0);
        hash = 23 * hash + this.defaultValidityPeriodDays;
        hash = 23 * hash + Objects.hashCode(this.permittedFees);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OccPermitType other = (OccPermitType) obj;
        return this.typeID == other.typeID;
    }

    /**
     * @return the permitTitle
     */
    public String getPermitTitle() {
        return permitTitle;
    }

    /**
     * @param permitTitle the permitTitle to set
     */
    public void setPermitTitle(String permitTitle) {
        this.permitTitle = permitTitle;
    }

  
    public List<Fee> getPermittedFees() {
        return permittedFees;
    }

    public void setPermittedFees(List<Fee> permittedFees) {
        this.permittedFees = permittedFees;
    }

    /**
     * @return the defaultRuleSetID
     */
    public int getBaseRuleSetID() {
        return baseRuleSetID;
    }

    /**
     * @param baseRuleSetID the defaultRuleSetID to set
     */
    public void setBaseRuleSetID(int baseRuleSetID) {
        this.baseRuleSetID = baseRuleSetID;
    }

    /**
     * @return the expires
     */
    public boolean isExpires() {
        return expires;
    }

    /**
     * @param expires the expires to set
     */
    public void setExpires(boolean expires) {
        this.expires = expires;
    }

    /**
     * @return the requireManager
     */
    public boolean isRequireManager() {
        return requireManager;
    }

    /**
     * @param requireManager the requireManager to set
     */
    public void setRequireManager(boolean requireManager) {
        this.requireManager = requireManager;
    }

    /**
     * @return the requireTenant
     */
    public boolean isRequireTenant() {
        return requireTenant;
    }

    /**
     * @param requireTenant the requireTenant to set
     */
    public void setRequireTenant(boolean requireTenant) {
        this.requireTenant = requireTenant;
    }

    /**
     * @return the requireZeroBalance
     */
    public boolean isRequireZeroBalance() {
        return requireZeroBalance;
    }

    /**
     * @param requireZeroBalance the requireZeroBalance to set
     */
    public void setRequireZeroBalance(boolean requireZeroBalance) {
        this.requireZeroBalance = requireZeroBalance;
    }

    /**
     * @return the requireInspectionPass
     */
    public boolean isRequireInspectionPass() {
        return requireInspectionPass;
    }

    /**
     * @param requireInspectionPass the requireInspectionPass to set
     */
    public void setRequireInspectionPass(boolean requireInspectionPass) {
        this.requireInspectionPass = requireInspectionPass;
    }

    @Override
    public String getDBTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPKFieldName() {
        return OCCPERMITTYPE_PK;
    }

    @Override
    public int getDBKey() {
        return typeID;
    }

    @Override
    public UMAPTrackedEnum getUMAPTrackedEntityEnum() {
        return UMAP_TRACKED_ENUM;
    }

    /**
     * @return the personColumnLinkingText
     */
    public String getPersonColumnLinkingText() {
        return personColumnLinkingText;
    }

    /**
     * @param personColumnLinkingText the personColumnLinkingText to set
     */
    public void setPersonColumnLinkingText(String personColumnLinkingText) {
        this.personColumnLinkingText = personColumnLinkingText;
    }

    /**
     * @return the personColumnRightLabel
     */
    public OccPermitPersonListEnum getPersonColumnRightLabel() {
        return personColumnRightLabel;
    }

    /**
     * @param personColumnRightLabel the personColumnRightLabel to set
     */
    public void setPersonColumnRightLabel(OccPermitPersonListEnum personColumnRightLabel) {
        this.personColumnRightLabel = personColumnRightLabel;
    }

    /**
     * @return the personColumnLeftLabel
     */
    public OccPermitPersonListEnum getPersonColumnLeftLabel() {
        return personColumnLeftLabel;
    }

    /**
     * @param personColumnLeftLabel the personColumnLeftLabel to set
     */
    public void setPersonColumnLeftLabel(OccPermitPersonListEnum personColumnLeftLabel) {
        this.personColumnLeftLabel = personColumnLeftLabel;
    }

    /**
     * @return the personColumnLeftActivate
     */
    public boolean isPersonColumnLeftActivate() {
        return personColumnLeftActivate;
    }

    /**
     * @param personColumnLeftActivate the personColumnLeftActivate to set
     */
    public void setPersonColumnLeftActivate(boolean personColumnLeftActivate) {
        this.personColumnLeftActivate = personColumnLeftActivate;
    }

    /**
     * @return the requireLPersonLinkLeftCol
     */
    public boolean isRequireLPersonLinkLeftCol() {
        return requireLPersonLinkLeftCol;
    }

    /**
     * @param requireLPersonLinkLeftCol the requireLPersonLinkLeftCol to set
     */
    public void setRequireLPersonLinkLeftCol(boolean requireLPersonLinkLeftCol) {
        this.requireLPersonLinkLeftCol = requireLPersonLinkLeftCol;
    }

    /**
     * @return the personColumnLeftCustomLabel
     */
    public String getPersonColumnLeftCustomLabel() {
        return personColumnLeftCustomLabel;
    }

    /**
     * @param personColumnLeftCustomLabel the personColumnLeftCustomLabel to set
     */
    public void setPersonColumnLeftCustomLabel(String personColumnLeftCustomLabel) {
        this.personColumnLeftCustomLabel = personColumnLeftCustomLabel;
    }

    /**
     * @return the personColumnRightActivate
     */
    public boolean isPersonColumnRightActivate() {
        return personColumnRightActivate;
    }

    /**
     * @param personColumnRightActivate the personColumnRightActivate to set
     */
    public void setPersonColumnRightActivate(boolean personColumnRightActivate) {
        this.personColumnRightActivate = personColumnRightActivate;
    }

    /**
     * @return the requireLPersonLinkRightCol
     */
    public boolean isRequireLPersonLinkRightCol() {
        return requireLPersonLinkRightCol;
    }

    /**
     * @param requireLPersonLinkRightCol the requireLPersonLinkRightCol to set
     */
    public void setRequireLPersonLinkRightCol(boolean requireLPersonLinkRightCol) {
        this.requireLPersonLinkRightCol = requireLPersonLinkRightCol;
    }

    /**
     * @return the personColumnRightCustomLabel
     */
    public String getPersonColumnRightCustomLabel() {
        return personColumnRightCustomLabel;
    }

    /**
     * @param personColumnRightCustomLabel the personColumnRightCustomLabel to set
     */
    public void setPersonColumnRightCustomLabel(String personColumnRightCustomLabel) {
        this.personColumnRightCustomLabel = personColumnRightCustomLabel;
    }

    /**
     * @return the personColumnManagerActivate
     */
    public boolean isPersonColumnManagerActivate() {
        return personColumnManagerActivate;
    }

    /**
     * @param personColumnManagerActivate the personColumnManagerActivate to set
     */
    public void setPersonColumnManagerActivate(boolean personColumnManagerActivate) {
        this.personColumnManagerActivate = personColumnManagerActivate;
    }

    /**
     * @return the personColumnTenantActivate
     */
    public boolean isPersonColumnTenantActivate() {
        return personColumnTenantActivate;
    }

    /**
     * @param personColumnTenantActivate the personColumnTenantActivate to set
     */
    public void setPersonColumnTenantActivate(boolean personColumnTenantActivate) {
        this.personColumnTenantActivate = personColumnTenantActivate;
    }

    /**
     * @return the occupancyFamilyPermit
     */
    public boolean isOccupancyFamilyPermit() {
        return occupancyFamilyPermit;
    }

    /**
     * @param occupancyFamilyPermit the occupancyFamilyPermit to set
     */
    public void setOccupancyFamilyPermit(boolean occupancyFamilyPermit) {
        this.occupancyFamilyPermit = occupancyFamilyPermit;
    }

    /**
     * @return the preaddTextBlocksStipulations
     */
    public List<TextBlock> getPreaddTextBlocksStipulations() {
        return preaddTextBlocksStipulations;
    }

    /**
     * @param preaddTextBlocksStipulations the preaddTextBlocksStipulations to set
     */
    public void setPreaddTextBlocksStipulations(List<TextBlock> preaddTextBlocksStipulations) {
        this.preaddTextBlocksStipulations = preaddTextBlocksStipulations;
    }

    /**
     * @return the preaddTextBlocksNotices
     */
    public List<TextBlock> getPreaddTextBlocksNotices() {
        return preaddTextBlocksNotices;
    }

    /**
     * @param preaddTextBlocksNotices the preaddTextBlocksNotices to set
     */
    public void setPreaddTextBlocksNotices(List<TextBlock> preaddTextBlocksNotices) {
        this.preaddTextBlocksNotices = preaddTextBlocksNotices;
    }

    /**
     * @return the preaddTextBlocksComments
     */
    public List<TextBlock> getPreaddTextBlocksComments() {
        return preaddTextBlocksComments;
    }

    /**
     * @param preaddTextBlocksComments the preaddTextBlocksComments to set
     */
    public void setPreaddTextBlocksComments(List<TextBlock> preaddTextBlocksComments) {
        this.preaddTextBlocksComments = preaddTextBlocksComments;
    }

    /**
     * @return the permitDisplayMuniAddress
     */
    public boolean isPermitDisplayMuniAddress() {
        return permitDisplayMuniAddress;
    }

    /**
     * @param permitDisplayMuniAddress the permitDisplayMuniAddress to set
     */
    public void setPermitDisplayMuniAddress(boolean permitDisplayMuniAddress) {
        this.permitDisplayMuniAddress = permitDisplayMuniAddress;
    }

   
}
