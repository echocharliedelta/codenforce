/*
 * Copyright (C) 2021 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities.occupancy;

import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import com.tcvcog.tcvce.entities.EventCategory;
import com.tcvcog.tcvce.entities.Icon;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.UMAPTrackedEntity;
import com.tcvcog.tcvce.entities.UMAPTrackedEnum;
import java.io.Serializable;

public class OccInspectionDetermination 
        extends UMAPTrackedEntity 
        implements Serializable,
                    IFaceCachable{

    private int determinationID;

    private Municipality muni;
    private String title;
    private String description;

    private String notes;
    private boolean qualifiesAsPassed;
    private boolean documentationOnly;
    
    private Icon icon;

    // This could be the ID? depends if the object is too heavy with it's icon and stuff
    private EventCategory eventCategory;

    private final static String TABLE_NAME = "occinspectiondetermination";
    private final static String OCCINSPECTIONDETERMINATION_PK = "determinationid";
    private final static UMAPTrackedEnum UMAP_TRACKED_ENUM = UMAPTrackedEnum.OCC_INSPECTION_DETERMINATION;

    public OccInspectionDetermination() {}

    public OccInspectionDetermination(OccInspectionDetermination occInspectionDetermination) {
        this.determinationID = occInspectionDetermination.getDeterminationID();
        this.title = occInspectionDetermination.getTitle();
        this.description = occInspectionDetermination.getDescription();
        this.notes = occInspectionDetermination.getNotes();
        this.eventCategory = occInspectionDetermination.getEventCategory();
    }
    
    
    @Override
    public int getCacheKey() {
        return determinationID;
    }
    
    /**
     * Builds a nice description of outcome with pass/fail/documentation only appended
     * @return 
     */
    public String getTitleWithOutcome(){
        StringBuilder sb = new StringBuilder(title);
        sb.append(" ");
        if(!documentationOnly){
            if(qualifiesAsPassed){
                sb.append("(Pass)");
            } else {
                sb.append("(Fail)");
            }
        } else {
                sb.append("(Documentation only)");
        }
        return sb.toString();
    }
    
    // getters and setters
    

    public int getDeterminationID() {
        return determinationID;
    }

    public void setDeterminationID(int determinationID) {
        this.determinationID = determinationID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public EventCategory getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(EventCategory eventCategory) {
        this.eventCategory = eventCategory;
    }

    /**
     * @return the qualifiesAsPassed
     */
    public boolean isQualifiesAsPassed() {
        return qualifiesAsPassed;
    }

    /**
     * @param qualifiesAsPassed the qualifiesAsPassed to set
     */
    public void setQualifiesAsPassed(boolean qualifiesAsPassed) {
        this.qualifiesAsPassed = qualifiesAsPassed;
    }

        @Override
    public String getDBTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPKFieldName() {
        return OCCINSPECTIONDETERMINATION_PK;
    }

    @Override
    public int getDBKey() {
        return determinationID;
    }

    @Override
    public UMAPTrackedEnum getUMAPTrackedEntityEnum() {
        return UMAP_TRACKED_ENUM;
    }

    /**
     * @return the muni
     */
    public Municipality getMuni() {
        return muni;
    }

    /**
     * @param muni the muni to set
     */
    public void setMuni(Municipality muni) {
        this.muni = muni;
    }

    /**
     * @return the documentationOnly
     */
    public boolean isDocumentationOnly() {
        return documentationOnly;
    }

    /**
     * @param documentationOnly the documentationOnly to set
     */
    public void setDocumentationOnly(boolean documentationOnly) {
        this.documentationOnly = documentationOnly;
    }

    /**
     * @return the icon
     */
    public Icon getIcon() {
        return icon;
    }

    /**
     * @param icon the icon to set
     */
    public void setIcon(Icon icon) {
        this.icon = icon;
    }

}
