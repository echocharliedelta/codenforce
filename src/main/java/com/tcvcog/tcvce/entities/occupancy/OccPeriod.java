/*
 * Copyright (C) 2021 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities.occupancy;

import com.tcvcog.tcvce.application.IFace_pinnable;
import com.tcvcog.tcvce.application.interfaces.IFace_Loggable;
import com.tcvcog.tcvce.entities.*;
import com.tcvcog.tcvce.util.DateTimeUtil;
import com.tcvcog.tcvce.util.viewoptions.ViewOptionsActiveHiddenListsEnum;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import com.tcvcog.tcvce.application.interfaces.IFaceActivatableBOB;
import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheObserver;

/**
 * Primary Business Object BOB for holding data about Occupancy Periods
 * 
 * @author Ellen Bascomb
 */
public  class       OccPeriod 
        extends     TrackedEntity  
        implements  IFace_Loggable,
                    IFace_EventHolder,
                    Comparable<OccPeriod>,
                    IFace_noteHolder,
                    IFace_pinnable,
                    IFace_stateful,
                    IFace_PermissionsCreatorRightsPreserved,
                    IFace_PermissionsManagerOverseen,
                    IFaceActivatableBOB,
                    IFace_PermissionsMuniFenced,
                    IFace_officerManaged,
                    IFaceCachable{
    
    final static String OCCPERIOD_TABLE_NAME = "occperiod";
    final static String OCCPERIOD_PK_FIELD = "periodid";
    final static String OCCPERIOD_HFNAME = "Occupancy Period";
    
    final static EventRealm OCC_DOMAIN = EventRealm.OCCUPANCY;
    
    final static String PIN_TABLE_NAME = "public.occperiodpin";
    final static String PIN_FK_FIELD = "occperiod_periodid";
    
    protected int periodID;
    
    protected OccPeriodType periodType;
    
    /**
     * Flattened field for parent property address to avoid composition loops
     */
    protected String propertyAddressStringEscapeFalse;
    /**
     * flattened field to avoid cycles
     */
    protected int parentParcelKey;
    /**
     * Super parent muni
     */
    protected String parentParcelIDCounty;
    protected Municipality muni;
        
    /**
     * Occ Periods are actually composed inside a property unit so this upward pointing 
     * ID is for  QA
     */
    protected int propertyUnitID;
    /**
     * Added for display of permit file unit associations in lists, even though a permit file
     * actually lives inside a property unit
     */
    protected String propertyUnitNumber;
    
    protected FieldInspection governingInspection;
    
    protected User manager;
    
    protected boolean pinned;
    protected User pinner;
    
    protected User periodTypeCertifiedBy;
    protected LocalDateTime periodTypeCertifiedTS;
    protected List<EventCnF> eventList;
    
    protected BOBSource source;
    
    protected LocalDateTime startDate;
    protected LocalDateTime startDateCertifiedTS;
    protected User startDateCertifiedBy;
    
    protected LocalDateTime endDate;
    protected LocalDateTime endDateCertifiedTS;
    protected User endDateCertifiedBy;

    protected User authorizedBy;
    protected LocalDateTime authorizedTS;
    
    protected boolean overrideTypeConfig;
    
    protected String notes;
    
    /**
     * @Deprecated replaced with deactivatedts and user
     */
    protected boolean active;
    
    // Used during initiation to store the origination event
    protected EventCategory originationEventCategory;
    protected List<OccPermit> permitList;
    
    protected OccPermit governingOccPermit;
    protected OccPermitGoverningStatusEnum governingOccPermitStatus;

    public OccPeriod() {
    }

    public OccPeriod(OccPeriod otherPeriod) {
        if(otherPeriod != null){

            this.periodID = otherPeriod.getPeriodID();
            
            this.periodType = otherPeriod.periodType;
            this.propertyAddressStringEscapeFalse = otherPeriod.getPropertyAddressStringEscapeFalse();
            this.parentParcelKey = otherPeriod.getParentParcelKey();
            this.muni = otherPeriod.getMuni();
            
            this.propertyUnitID = otherPeriod.getPropertyUnitID();
            this.propertyUnitNumber = otherPeriod.getPropertyUnitNumber();
            

            this.manager = otherPeriod.getManager();
            this.periodTypeCertifiedBy = otherPeriod.getPeriodTypeCertifiedBy();
            this.periodTypeCertifiedTS = otherPeriod.getPeriodTypeCertifiedTS();

            this.eventList = otherPeriod.getEventList();
            this.source = otherPeriod.getSource();

            this.startDate = otherPeriod.getStartDate();
            this.startDateCertifiedBy = otherPeriod.getStartDateCertifiedBy();
            this.startDateCertifiedTS = otherPeriod.getStartDateCertifiedTS();

            this.endDate = otherPeriod.getEndDate();
            this.endDateCertifiedBy = otherPeriod.getEndDateCertifiedBy();
            this.endDateCertifiedTS = otherPeriod.getEndDateCertifiedTS();

            this.authorizedBy = otherPeriod.getAuthorizedBy();
            this.authorizedTS = otherPeriod.getAuthorizedTS();

            this.overrideTypeConfig = otherPeriod.isOverrideTypeConfig();
            
            this.governingInspection = otherPeriod.getGoverningInspection();
            this.permitList = otherPeriod.permitList;
            this.governingOccPermit = otherPeriod.governingOccPermit;
            this.governingOccPermitStatus = otherPeriod.governingOccPermitStatus;
            
            this.active = otherPeriod.active; // deprecated
            this.notes = otherPeriod.getNotes();
            
            this.createdBy = otherPeriod.createdBy;
            this.createdTS = otherPeriod.createdTS;

            this.lastUpdatedBy = otherPeriod.lastUpdatedBy;
            this.lastUpdatedTS = otherPeriod.lastUpdatedTS;

            this.deactivatedBy = otherPeriod.deactivatedBy;
            this.deactivatedTS = otherPeriod.deactivatedTS;
        }
    }
    
    

    @Override
    public int compareTo(OccPeriod op) {
        // written by CHAT GPT unknown version
       if (this.startDate == null && op.startDate == null) {
        return 0;
        } else if (this.startDate == null) {
            return -1;
        } else if (op.startDate == null) {
            return 1;
        } else {
            return this.startDate.compareTo(op.startDate);
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.periodID;
        hash = 53 * hash + Objects.hashCode(this.periodType);
        hash = 53 * hash + Objects.hashCode(this.propertyAddressStringEscapeFalse);
        hash = 53 * hash + this.parentParcelKey;
        hash = 53 * hash + Objects.hashCode(this.parentParcelIDCounty);
        hash = 53 * hash + Objects.hashCode(this.muni);
        hash = 53 * hash + this.propertyUnitID;
        hash = 53 * hash + Objects.hashCode(this.propertyUnitNumber);
        hash = 53 * hash + Objects.hashCode(this.governingInspection);
        hash = 53 * hash + Objects.hashCode(this.manager);
        hash = 53 * hash + (this.pinned ? 1 : 0);
        hash = 53 * hash + Objects.hashCode(this.pinner);
        hash = 53 * hash + Objects.hashCode(this.periodTypeCertifiedBy);
        hash = 53 * hash + Objects.hashCode(this.periodTypeCertifiedTS);
        hash = 53 * hash + Objects.hashCode(this.eventList);
        hash = 53 * hash + Objects.hashCode(this.source);
        hash = 53 * hash + Objects.hashCode(this.startDate);
        hash = 53 * hash + Objects.hashCode(this.startDateCertifiedTS);
        hash = 53 * hash + Objects.hashCode(this.startDateCertifiedBy);
        hash = 53 * hash + Objects.hashCode(this.endDate);
        hash = 53 * hash + Objects.hashCode(this.endDateCertifiedTS);
        hash = 53 * hash + Objects.hashCode(this.endDateCertifiedBy);
        hash = 53 * hash + Objects.hashCode(this.authorizedBy);
        hash = 53 * hash + Objects.hashCode(this.authorizedTS);
        hash = 53 * hash + (this.overrideTypeConfig ? 1 : 0);
        hash = 53 * hash + Objects.hashCode(this.notes);
        hash = 53 * hash + (this.active ? 1 : 0);
        hash = 53 * hash + Objects.hashCode(this.originationEventCategory);
        hash = 53 * hash + Objects.hashCode(this.permitList);
        hash = 53 * hash + Objects.hashCode(this.governingOccPermit);
        hash = 53 * hash + Objects.hashCode(this.governingOccPermitStatus);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OccPeriod other = (OccPeriod) obj;
        return this.periodID == other.periodID;
    }
    
    /* ****************************************************************
     * *********            CACHING                         ***********
     * ****************************************************************
     */
    
    @Override
    public int getCacheKey() {
        return periodID;
    }

    

    public long getPeriodAge() {
        if(endDate != null){
            return DateTimeUtil.getTimePeriodAsDays(startDate, endDate);
        } else {
            return DateTimeUtil.getTimePeriodAsDays(startDate, LocalDateTime.now());
        }
    }

    /**
     * Utility method for checking age of the occ period
     * @param ageEndTime
     * @return 
     */
    public long getPeriodAgeAsOf(LocalDateTime ageEndTime){
        return DateTimeUtil.getTimePeriodAsDays(startDate, ageEndTime);
    }
    
      /**
     * Convenience method for iterating over permit list and only returning finalized 
     * that have not been 
     * @return 
     */
    public List<OccPermit> getPermitsFinalized(){
        List<OccPermit> pl = new ArrayList<>();
        if(permitList != null && !permitList.isEmpty()){
            for(OccPermit perm: permitList){
                if( perm.getFinalizedts() != null && perm.getNullifiedTS() == null){
                    pl.add(perm);
                }
            }
        }
        return pl;
    }
    
    
    /**
     * @return the periodID
     */
    public int getPeriodID() {
        return periodID;
    }

    /**
     * @return the propertyUnitID
     */
    public int getPropertyUnitID() {
        return propertyUnitID;
    }
  
    /**
     * @return the manager
     */
    public User getManager() {
        return manager;
    }

    /**
     * @return the periodTypeCertifiedBy
     */
    public User getPeriodTypeCertifiedBy() {
        return periodTypeCertifiedBy;
    }

    /**
     * @return the periodTypeCertifiedTS
     */
    public LocalDateTime getPeriodTypeCertifiedTS() {
        return periodTypeCertifiedTS;
    }

    /**
     * @return the source
     */
    public BOBSource getSource() {
        return source;
    }


    /**
     * @return the startDate
     */
    public LocalDateTime getStartDate() {
        return startDate;
    }

    /**
     * @return the startDateCertifiedTS
     */
    public LocalDateTime getStartDateCertifiedTS() {
        return startDateCertifiedTS;
    }

    /**
     * @return the startDateCertifiedBy
     */
    public User getStartDateCertifiedBy() {
        return startDateCertifiedBy;
    }

    /**
     * @return the prettified startdate
     */
    public String getStartDatePretty() {
        if(startDate != null){
            return DateTimeUtil.getPrettyDate(startDate);
        }
        return null;
    }

    public String getStartDatePrettyNoTime() {
        if(startDate != null){
            return DateTimeUtil.getPrettyDateNoTime(startDate);
        }
        return null;
    }

    /**
     * @return the endDate
     */
    public LocalDateTime getEndDate() {
        return endDate;
    }

    /**
     * @return the endDateCertifiedTS
     */
    public LocalDateTime getEndDateCertifiedTS() {
        return endDateCertifiedTS;
    }

    /**
     * @return the endDateCertifiedBy
     */
    public User getEndDateCertifiedBy() {
        return endDateCertifiedBy;
    }

    /**
     * @return the prettified enddate
     */
    public String getEndDatePretty() {
        if(endDate != null){
            return DateTimeUtil.getPrettyDate(endDate);
        }
        return null;
    }

    public String getEndDatePrettyNoTime() {
        if(endDate != null){
            return DateTimeUtil.getPrettyDateNoTime(endDate);
        }
        return null;
    }

    /**
     * @return the authorizedTS
     */
    public LocalDateTime getAuthorizedTS() {
        return authorizedTS;
    }

    /**
     * @return the authorizedBy
     */
    public User getAuthorizedBy() {
        return authorizedBy;
    }

    /**
     * @return the overrideTypeConfig
     */
    public boolean isOverrideTypeConfig() {
        return overrideTypeConfig;
    }

    /**
     * @return the notes
     */
    @Override
    public String getNotes() {
        return notes;
    }

    /**
     * @param periodID the periodID to set
     */
    public void setPeriodID(int periodID) {
        this.periodID = periodID;
    }

    /**
     * @param propertyUnitID the propertyUnitID to set
     */
    public void setPropertyUnitID(int propertyUnitID) {
        this.propertyUnitID = propertyUnitID;
    }
    

    /**
     * @param manager the manager to set
     */
    public void setManager(User manager) {
        this.manager = manager;
    }

    /**
     * @param periodTypeCertifiedBy the periodTypeCertifiedBy to set
     */
    public void setPeriodTypeCertifiedBy(User periodTypeCertifiedBy) {
        this.periodTypeCertifiedBy = periodTypeCertifiedBy;
    }

    /**
     * @param periodTypeCertifiedTS the periodTypeCertifiedTS to set
     */
    public void setPeriodTypeCertifiedTS(LocalDateTime periodTypeCertifiedTS) {
        this.periodTypeCertifiedTS = periodTypeCertifiedTS;
    }

    /**
     * @param source the source to set
     */
    public void setSource(BOBSource source) {
        this.source = source;
    }

   

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    /**
     * @param startDateCertifiedTS the startDateCertifiedTS to set
     */
    public void setStartDateCertifiedTS(LocalDateTime startDateCertifiedTS) {
        this.startDateCertifiedTS = startDateCertifiedTS;
    }

    /**
     * @param startDateCertifiedBy the startDateCertifiedBy to set
     */
    public void setStartDateCertifiedBy(User startDateCertifiedBy) {
        this.startDateCertifiedBy = startDateCertifiedBy;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    /**
     * @param endDateCertifiedTS the endDateCertifiedTS to set
     */
    public void setEndDateCertifiedTS(LocalDateTime endDateCertifiedTS) {
        this.endDateCertifiedTS = endDateCertifiedTS;
    }

    /**
     * @param endDateCertifiedBy the endDateCertifiedBy to set
     */
    public void setEndDateCertifiedBy(User endDateCertifiedBy) {
        this.endDateCertifiedBy = endDateCertifiedBy;
    }

    /**
     * @param authorizedTS the authorizedTS to set
     */
    public void setAuthorizedTS(LocalDateTime authorizedTS) {
        this.authorizedTS = authorizedTS;
    }

    /**
     * @param authorizedBy the authorizedBy to set
     */
    public void setAuthorizedBy(User authorizedBy) {
        this.authorizedBy = authorizedBy;
    }

    /**
     * @param overrideTypeConfig the overrideTypeConfig to set
     */
    public void setOverrideTypeConfig(boolean overrideTypeConfig) {
        this.overrideTypeConfig = overrideTypeConfig;
    }

    /**
     * @param notes the notes to set
     */
    @Override
    public void setNotes(String notes) {
        this.notes = notes;
    }
   
    
    /**
     * @return the governingInspection
     */
    public FieldInspection getGoverningInspection() {
        return governingInspection;
    }

    /**
     * @param governingInspection the governingInspection to set
     */
    public void setGoverningInspection(FieldInspection governingInspection) {
        this.governingInspection = governingInspection;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

   
    /**
     *
     * @param lst
     */
    @Override
    public void setEventList(List<EventCnF> lst) {
        eventList = lst;
    }

    /** 
     * Iterates over the BOb's internal list of events and selects only those
     * specified by the enum constant's member vals
     *
     * @param voahle an instance of the enum representing which events you want
     * @return
     */
    @Override
    public List<EventCnF> getEventList(ViewOptionsActiveHiddenListsEnum voahle) {
        List<EventCnF> visEventList = new ArrayList<>();
        if (eventList != null) {
            for (EventCnF ev : eventList) {
                switch (voahle) {
                    case VIEW_ACTIVE_HIDDEN:
                        if (ev.isActive() && ev.isHidden()) {
                            visEventList.add(ev);
                        }
                        break;
                    case VIEW_ACTIVE_NOTHIDDEN:
                        if (ev.isActive() && !ev.isHidden()) {
                            visEventList.add(ev);
                        }
                        break;
                    case VIEW_ALL:
                        visEventList.add(ev);
                        break;
                    case VIEW_INACTIVE:
                        if (!ev.isActive()) {
                            visEventList.add(ev);
                        }
                        break;
                    default:
                        visEventList.add(ev);
                } // close switch
            } // close for
        } // close null check
        return visEventList;
    }
    
    /**
     * Retrieves all events
     * @return 
     */
    @Override
    public List<EventCnF> getEventList(){
        return eventList;
    }

    @Override
    public int getBObID() {
        return periodID;
    }

    @Override
    public String getPKFieldName() {
        return OCCPERIOD_PK_FIELD;
    }

    @Override
    public int getDBKey() {
        return periodID;
    }

    @Override
    public String getDBTableName() {
        return OCCPERIOD_TABLE_NAME;
    }

    @Override
    public String getNoteHolderFriendlyName() {
        return OCCPERIOD_HFNAME;
    }

    @Override
    public EventRealm getEventDomain() {
        return OCC_DOMAIN;
    }

    /**
     * @return the originationEventCategory
     */
    public EventCategory getOriginationEventCategory() {
        return originationEventCategory;
    }

    /**
     * @param originationEventCategory the originationEventCategory to set
     */
    public void setOriginationEventCategory(EventCategory originationEventCategory) {
        this.originationEventCategory = originationEventCategory;
    }

    /**
     * @return the permitList
     */
    public List<OccPermit> getPermitList() {
        return permitList;
    }

    /**
     * @param permitList the permitList to set
     */
    public void setPermitList(List<OccPermit> permitList) {
        this.permitList = permitList;
    }

    /**
     * @return the pinned
     */
    @Override
    public boolean isPinned() {
        return pinned;
    }

    /**
     * @return the pinner
     */
    @Override
    public User getPinner() {
        return pinner;
    }

    /**
     * @param pinner the pinner to set
     */
    @Override
    public void setPinner(User pinner) {
        this.pinner = pinner;
    }

    @Override
    public String getPinTableFKString() {
        return PIN_FK_FIELD;
    }

    @Override
    public String getPinTableName() {
        return PIN_TABLE_NAME;
    }

    /**
     * @param pinned the pinned to set
     */
    public void setPinned(boolean pinned) {
        this.pinned = pinned;
    }

    @Override
    public StateEnum getState() {
        if(permitList == null || permitList.isEmpty()){
            return StateEnum.OPEN;
        }
        for(OccPermit pmt: permitList){
            if(pmt.getFinalizedts() != null){
                return StateEnum.CLOSED;
            }
        }
        return StateEnum.OPEN;
    }

    /**
     * @return the propertyUnitNumber
     */
    public String getPropertyUnitNumber() {
        return propertyUnitNumber;
    }

    /**
     * @param propertyUnitNumber the propertyUnitNumber to set
     */
    public void setPropertyUnitNumber(String propertyUnitNumber) {
        this.propertyUnitNumber = propertyUnitNumber;
    }

    @Override
    public int getCreatorUserID() {
        return getCreatedByUserID();
    }

    @Override
    public User getManagerOverseer() {
        return manager;
    }

    /**
     * @return the periodType
     */
    public OccPeriodType getPeriodType() {
        return periodType;
    }

    /**
     * @param periodType the periodType to set
     */
    public void setPeriodType(OccPeriodType periodType) {
        this.periodType = periodType;
    }

    /**
     * @return the propertyAddressStringEscapeFalse
     */
    public String getPropertyAddressStringEscapeFalse() {
        return propertyAddressStringEscapeFalse;
    }

    /**
     * @param propertyAddressStringEscapeFalse the propertyAddressStringEscapeFalse to set
     */
    public void setPropertyAddressStringEscapeFalse(String propertyAddressStringEscapeFalse) {
        this.propertyAddressStringEscapeFalse = propertyAddressStringEscapeFalse;
    }

    /**
     * @return the parentParcelKey
     */
    public int getParentParcelKey() {
        return parentParcelKey;
    }

    /**
     * @param parentParcelKey the parentParcelKey to set
     */
    public void setParentParcelKey(int parentParcelKey) {
        this.parentParcelKey = parentParcelKey;
    }

    /**
     * @return the muni
     */
    public Municipality getMuni() {
        return muni;
    }

    /**
     * @param muni the muni to set
     */
    public void setMuni(Municipality muni) {
        this.muni = muni;
    }

    @Override
    public Municipality getGoverningMunicipality() {
        return muni;
    }

    /**
     * @return the parentParcelIDCounty
     */
    public String getParentParcelIDCounty() {
        return parentParcelIDCounty;
    }

    /**
     * @param parentParcelIDCounty the parentParcelIDCounty to set
     */
    public void setParentParcelIDCounty(String parentParcelIDCounty) {
        this.parentParcelIDCounty = parentParcelIDCounty;
    }

    /**
     * @return the governingOccPermit
     */
    public OccPermit getGoverningOccPermit() {
        return governingOccPermit;
    }

    /**
     * @param governingOccPermit the governingOccPermit to set
     */
    public void setGoverningOccPermit(OccPermit governingOccPermit) {
        this.governingOccPermit = governingOccPermit;
    }

    /**
     * @return the governingOccPermitStatus
     */
    public OccPermitGoverningStatusEnum getGoverningOccPermitStatus() {
        return governingOccPermitStatus;
    }

    /**
     * @param governingOccPermitStatus the governingOccPermitStatus to set
     */
    public void setGoverningOccPermitStatus(OccPermitGoverningStatusEnum governingOccPermitStatus) {
        this.governingOccPermitStatus = governingOccPermitStatus;
    }

    @Override
    public User getOfficerManage() {
        return manager;
    }


}