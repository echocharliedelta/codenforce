/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities.occupancy;

import com.tcvcog.tcvce.entities.HumanLink;
import com.tcvcog.tcvce.entities.OccPermitPersonListEnum;
import java.util.List;

/**
 * Contains the four person fields and the permit comments fields 
 * for storage DURING the amendment process. This object will get populated 
 * on amendment init and used to write previous values to the permit comments
 * @author pierre15
 */
public class OccPermitPreAmendmentFields {
    
    private int parentPermitID;
    
    private List<HumanLink> preAmendmentOwnerSellerLinkList;
    private List<HumanLink> preAmendmentBuyerTenantLinkList;
    private List<HumanLink> preAmendmentManagerLinkList;
    private List<HumanLink> preAmendmentTenantLinkList;
    

  
    /**
     * @return the preAmendmentOwnerSellerLinkList
     */
    public List<HumanLink> getPreAmendmentOwnerSellerLinkList() {
        return preAmendmentOwnerSellerLinkList;
    }

    /**
     * @param preAmendmentOwnerSellerLinkList the preAmendmentOwnerSellerLinkList to set
     */
    public void setPreAmendmentOwnerSellerLinkList(List<HumanLink> preAmendmentOwnerSellerLinkList) {
        this.preAmendmentOwnerSellerLinkList = preAmendmentOwnerSellerLinkList;
    }

    

    /**
     * @return the preAmendmentBuyerTenantLinkList
     */
    public List<HumanLink> getPreAmendmentBuyerTenantLinkList() {
        return preAmendmentBuyerTenantLinkList;
    }

    /**
     * @param preAmendmentBuyerTenantLinkList the preAmendmentBuyerTenantLinkList to set
     */
    public void setPreAmendmentBuyerTenantLinkList(List<HumanLink> preAmendmentBuyerTenantLinkList) {
        this.preAmendmentBuyerTenantLinkList = preAmendmentBuyerTenantLinkList;
    }

  

    /**
     * @return the preAmendmentManagerLinkList
     */
    public List<HumanLink> getPreAmendmentManagerLinkList() {
        return preAmendmentManagerLinkList;
    }

    /**
     * @param preAmendmentManagerLinkList the preAmendmentManagerLinkList to set
     */
    public void setPreAmendmentManagerLinkList(List<HumanLink> preAmendmentManagerLinkList) {
        this.preAmendmentManagerLinkList = preAmendmentManagerLinkList;
    }

   
    /**
     * @return the preAmendmentTenantLinkList
     */
    public List<HumanLink> getPreAmendmentTenantLinkList() {
        return preAmendmentTenantLinkList;
    }

    /**
     * @param preAmendmentTenantLinkList the preAmendmentTenantLinkList to set
     */
    public void setPreAmendmentTenantLinkList(List<HumanLink> preAmendmentTenantLinkList) {
        this.preAmendmentTenantLinkList = preAmendmentTenantLinkList;
    }

    /**
     * @return the parentPermitID
     */
    public int getParentPermitID() {
        return parentPermitID;
    }

    /**
     * @param parentPermitID the parentPermitID to set
     */
    public void setParentPermitID(int parentPermitID) {
        this.parentPermitID = parentPermitID;
    }

  
    
    
}
