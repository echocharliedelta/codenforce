/*
 * Copyright (C) 2018 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities.occupancy;

import com.tcvcog.tcvce.entities.BlobLight;
import com.tcvcog.tcvce.entities.CodeSource;
import com.tcvcog.tcvce.entities.EventCnF;
import com.tcvcog.tcvce.entities.EventLinkEnum;
import com.tcvcog.tcvce.entities.HumanLink;
import com.tcvcog.tcvce.entities.IFace_EventLinked;
import com.tcvcog.tcvce.entities.IFace_PermissionsMuniProfileGoverned;
import com.tcvcog.tcvce.entities.IFace_humanListHolder;
import com.tcvcog.tcvce.entities.LinkedObjectSchemaEnum;
import com.tcvcog.tcvce.entities.OccPermitPersonListEnum;
import com.tcvcog.tcvce.entities.ParcelInfo;
import com.tcvcog.tcvce.entities.Person;
import com.tcvcog.tcvce.entities.TextBlock;
import com.tcvcog.tcvce.entities.TrackedEntity;
import com.tcvcog.tcvce.entities.User;
import com.tcvcog.tcvce.entities.UserMuniAuthPeriod;
import com.tcvcog.tcvce.util.Constants;
import j2html.TagCreator;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import com.tcvcog.tcvce.application.interfaces.IFaceActivatableBOB;
import com.tcvcog.tcvce.entities.IFaceSessionSyncTarget;
import com.tcvcog.tcvce.entities.IFaceSessionSyncVisitor;

/**
 * BOB representing both an in process and finalized occ permit
 * @author ellen bascomb of apt 31y
 */
public class OccPermit extends TrackedEntity 
    implements IFaceActivatableBOB,
                IFace_PermissionsMuniProfileGoverned,
                Comparable<OccPermit>,
                IFace_EventLinked,
                IFace_humanListHolder,
                IFaceSessionSyncTarget{
    
    final static String TABLE_NAME = "occpermit";
    final static String PERMIT_PK = "permitid";
    final static LinkedObjectSchemaEnum OCCPERMIT_HUMAN_ENUM = LinkedObjectSchemaEnum.OccPermitHuman;
    
    private int permitID;
    // used for storing municipality-generated IDs associated with the permit
    private String referenceNo;
    private int periodID;
    private int parentParcelKey;
    
    /**
     * Contains critical switches for permit display and auditing requirements
     */
    private OccPermitType permitType;
    private String notes;
    
    private LocalDateTime finalizedts;
    private User finalizedBy;
    
    private LocalDateTime nullifiedTS;
    private User nullifiedBy;
    
    // EVENT STUFF
    private List<EventCnF> linkedEvents;
    
    
    
    // *******************************************************************************
    // ************** FINALIZATION RULE OVERRIDE FACILITY ****************************
    // *******************************************************************************
       
    private LocalDateTime adminOverrideTS;
    private UserMuniAuthPeriod adminOverrideUMAP;
    
    /**
     * Convenience method for determining if this permit has had an admin override
     * @return 
     */
    public boolean isOverrideEnabled(){
        return adminOverrideTS != null && adminOverrideUMAP != null;
    }
    
    
    // ********************************************************************
    // ********************** DYNAMIC FIELDS ******************************
    // ***** USED DURING THE CONFIGURATION OF THE PERMIT AND THESE  *******
    // ***** ARE READ INTO THE STATIC FIELDS BY THE COORDINATOR'S   *******
    // ***** updateOccPermitStaticFields(...) method                *******
    // ********************************************************************
    
    private LocalDateTime dynamicLastSaveTS;
    private int dynamicLastStep;
    
    private ParcelInfo parcelInfo;
    
    private String dynamicPopulationLog;
    
    private boolean inspectionRequirmentPass;
    private String inspectionRequirmentLog;
    
    private LocalDateTime dynamicPopulationReadyForFinalizationTS;
    private LocalDateTime finalizationAuditPassTS;
    
    private OccPermitPersonListEnum ownerSellerType;
    private Integer[] ownerSellerLinkIDList;
    private List<HumanLink> ownerSellerLinkList;
    
    private String dynamicPersonsColumnLink;
    
    private OccPermitPersonListEnum buyerTenantType;
    private Integer[] buyerTenantLinkIDList;
    private List<HumanLink> buyerTenantLinkList;
    
    private Integer[] managerLinkIDList;
    private List<HumanLink> managerLinkList;
    
    private Integer[] tenantLinkIDList;
    private List<HumanLink> tenantLinkList;
    
    private User issuingOfficer;
    private Person issuingOfficerPerson;
    private List<CodeSource> issuingCodeSourceList;
    
    private List<TextBlock> textBlocks_stipulations;
    private String text_stipulations;
    
    private List<TextBlock> textBlocks_notice;
    private String text_notices;
    // both combined for the comments static field
    private List<TextBlock> textBlocks_comments;
    private String text_comments;
    
    private LocalDate dynamicDateOfApplication;
    private OccPermitApplication dynamicsDateOfApplicationAppRef;
    
    private LocalDate dynamicInitialInspection;
    private FieldInspectionLight dynamicInitialInspectionFINRef;
            
    private LocalDate dynamicreinspectiondate;
    private FieldInspectionLight dynamicReInspectionFINRef;
    
    private LocalDate dynamicfinalinspection;
    private FieldInspectionLight dynamicFinalInspectionFINRef;
    
    private LocalDate dynamicdateofissue;
    private LocalDate dynamicDateExpiry;
    
    private List<HumanLink> humanLinkList;
    
    
    // ********************************************************************
    // ********************** STATIC FIELDS *******************************
    // ********************************************************************
    
    private String finalizationAuditLog;
    
    private int staticheaderphotodocid;
    private int staticheaderWidthPx;
    
    private LocalDate staticdateofapplication;
    private LocalDate staticinitialinspection;
    private LocalDate staticreinspectiondate;
    private LocalDate staticfinalinspection;
    private LocalDate staticdateofissue;
    private LocalDate staticdateofexpiry;
    
    private String statictitle;
    private String staticmuniaddress;
    private boolean staticdisplaymuniaddress;
    private String staticpropertyinfo;
    
    private boolean staticdisplayperslinkleftcol;
    private String staticownersellerlabel;
    private String staticownerseller;
    
    private String staticcolumnlink;
    
    private boolean staticdisplayperslinkrightcol;
    private String staticbuyertenantlabel;
    private String staticbuyertenant;
    
    /**
     * @Deprecated by ECD once I moved logic into config permit from WWALK implementation
     */
    private String configurePermitOnlyPropertyInfo; //added by wwalk 12.26.22 for reportOccPermit.xhtml
    
    private String staticproposeduse;   // from parcel info
    private String staticusecode;       // from parcel info
    private String staticconstructiontype;  // from parcelinfo
    
    private String staticpropclass;     // from parcelinfo
    
    private String staticofficername; // from dynamic field on permit
    private String derivedOfficerNameOnly;
    
    private int staticOfficerSignaturePhotoDocID;
    private String staticissuedundercodesourceid;   // from chosen code source
    
    private String staticstipulations;
    private String staticcomments;
    private String staticnotice;
    
    private boolean staticdisplayperslinkmanagers;
    private String staticmanager;
    private boolean staticdisplayperslinktenants;
    private String statictenants;
    
    private String staticleaseterm;
    
    private String staticleasestatus;
    private String staticpaymentstatus;
    
    
    // *******************************************************************************
    // ********************** AMENDMENT AND REVOKE FIELDS ****************************
    // *******************************************************************************
    
    private OccPermitPreAmendmentFields preAmendmentFields;
    
    private LocalDateTime amendementInitTS;
    private LocalDateTime amendementCommitTS;
    private UserMuniAuthPeriod amendmendedByUmap;
    private User amendedByUser;
    
    private LocalDateTime revokedTS;
    private UserMuniAuthPeriod revokedByUMAP;
    private User revokedByUser;
    private String revokedReason;
    
    
    /**
     * Logic switch for declaring the current permit is in Amendment mode
     * @return true if this permit is in amendment mode, meaning
     * it has a non null amendment init timestamp and a null 
     * amendment commit timestamp
     */
    public boolean isCurrentPermitAmendmentMode(){
        if(amendementInitTS != null && amendementCommitTS == null){
                return true;
        }
        return false;
    }
    
    
    // ********************************************************************
    // **************** CONSTRUCTOR AND OTHER GARBAGE *********************
    // ********************************************************************
    
    
    public OccPermit(){
      configureInternals();
    }
  
    private void configureInternals(){
        dynamicPopulationLog = new String();
        finalizationAuditLog = new String();
        issuingCodeSourceList = new ArrayList<>();     
     }
    
    /**
     * Constructor of an OccPermit object given an
     * OccPermit object
     * @param permit
     */
    public OccPermit(OccPermit permit){
        configureInternals();
        
        this.permitID = permit.permitID;
        this.referenceNo = permit.referenceNo;
        this.periodID = permit.periodID;
        this.parentParcelKey = permit.parentParcelKey;
        
        this.permitType = permit.permitType;
        this.notes = permit.notes;
        
        this.finalizedts = permit.finalizedts;
        this.finalizedBy = permit.finalizedBy;
        
        this.nullifiedTS = permit.nullifiedTS;
        this.nullifiedBy = permit.nullifiedBy;
        
        this.linkedEvents = permit.linkedEvents;
        
        this.adminOverrideTS = permit.adminOverrideTS;
        this.adminOverrideUMAP = permit.adminOverrideUMAP;
        
        // dynamic fields
        
        this.dynamicLastSaveTS = permit.dynamicLastSaveTS;
        this.dynamicLastStep= permit.dynamicLastStep;
        
        this.parcelInfo = permit.parcelInfo;
        
        this.dynamicPopulationLog = permit.dynamicPopulationLog;
        
        this.inspectionRequirmentPass = permit.inspectionRequirmentPass;
        this.inspectionRequirmentLog = permit.inspectionRequirmentLog;
        
        this.dynamicPopulationReadyForFinalizationTS = permit.dynamicPopulationReadyForFinalizationTS;
        this.finalizationAuditPassTS = permit.finalizationAuditPassTS;
        
        this.ownerSellerType = permit.ownerSellerType;
        this.ownerSellerLinkIDList = permit.ownerSellerLinkIDList;
        this.ownerSellerLinkList = permit.ownerSellerLinkList;
        
        this.dynamicPersonsColumnLink = permit.dynamicPersonsColumnLink;
        
        this.buyerTenantType = permit.buyerTenantType;
        this.buyerTenantLinkIDList = permit.buyerTenantLinkIDList;
        this.buyerTenantLinkList = permit.buyerTenantLinkList;
        
        this.managerLinkIDList = permit.managerLinkIDList;
        this.managerLinkList = permit.managerLinkList;
        
        this.tenantLinkIDList = permit.tenantLinkIDList;
        this.tenantLinkList = permit.tenantLinkList;
        
        this.issuingOfficer = permit.issuingOfficer;
        this.issuingOfficerPerson = permit.issuingOfficerPerson;
        this.issuingCodeSourceList = permit.issuingCodeSourceList;
        
        this.textBlocks_stipulations = permit.textBlocks_stipulations;
        this.text_stipulations = permit.text_stipulations;
        
        this.textBlocks_notice = permit.textBlocks_notice;
        this.text_notices = permit.text_notices;
        
        this.textBlocks_comments = permit.textBlocks_comments;
        this.text_comments = permit.text_comments;
        
        this.dynamicDateOfApplication = permit.dynamicDateOfApplication;
        this.dynamicsDateOfApplicationAppRef = permit.dynamicsDateOfApplicationAppRef;
        
        this.dynamicInitialInspection = permit.dynamicInitialInspection;
        this.dynamicInitialInspectionFINRef = permit.dynamicInitialInspectionFINRef;
        
        this.dynamicreinspectiondate = permit.dynamicreinspectiondate;
        this.dynamicReInspectionFINRef = permit.dynamicReInspectionFINRef;
        
        this.dynamicfinalinspection = permit.dynamicfinalinspection;
        this.dynamicFinalInspectionFINRef = permit.dynamicFinalInspectionFINRef;
        
        this.dynamicdateofissue = permit.dynamicdateofissue;
        this.dynamicDateExpiry = permit.dynamicDateExpiry;
        
        this.humanLinkList = permit.humanLinkList;
        
        // *************
        // static fields 
        // *************
        
        this.finalizationAuditLog = permit.finalizationAuditLog;
        
        this.staticheaderphotodocid = permit.staticheaderphotodocid;
        this.staticheaderWidthPx = permit.staticheaderWidthPx;
        
        this.staticdateofapplication = permit.staticdateofapplication;
        this.staticinitialinspection = permit.staticinitialinspection;
        this.staticreinspectiondate = permit.staticreinspectiondate;
        this.staticfinalinspection = permit.staticfinalinspection;
        this.staticdateofissue = permit.staticdateofissue;
        this.staticdateofexpiry = permit.staticdateofexpiry;
        
        this.statictitle = permit.statictitle;
        this.staticmuniaddress = permit.staticmuniaddress;
        this.staticdisplaymuniaddress = permit.staticdisplaymuniaddress;
        this.staticpropertyinfo = permit.staticpropertyinfo;
        
        this.staticdisplayperslinkleftcol = permit.staticdisplayperslinkleftcol;
        this.staticownersellerlabel = permit.staticownersellerlabel;
        this.staticownerseller = permit.staticownerseller;
        
        this.staticcolumnlink = permit.staticcolumnlink;
        
        this.staticdisplayperslinkrightcol = permit.staticdisplayperslinkrightcol;
        this.staticbuyertenantlabel = permit.staticbuyertenantlabel;
        this.staticbuyertenant = permit.staticbuyertenant;
        
        
        
        
        this.configurePermitOnlyPropertyInfo = permit.configurePermitOnlyPropertyInfo;
        
        this.staticproposeduse = permit.staticproposeduse;
        this.staticusecode = permit.staticusecode;
        this.staticconstructiontype = permit.staticconstructiontype;
        
        this.staticpropclass   = permit.staticpropclass;   
        
        this.staticofficername = permit.staticofficername; 
        this.derivedOfficerNameOnly = permit.derivedOfficerNameOnly;
        
        this.staticOfficerSignaturePhotoDocID = permit.staticOfficerSignaturePhotoDocID;
        this.staticissuedundercodesourceid   = permit.staticissuedundercodesourceid;   
        
        this.staticstipulations = permit.staticstipulations;
        this.staticcomments = permit.staticcomments;
        this.staticnotice = permit.staticnotice;

        this.staticdisplayperslinkmanagers = permit.staticdisplayperslinkmanagers;
        this.staticmanager = permit.staticmanager;
        this.staticdisplayperslinktenants = permit.staticdisplayperslinktenants;
        this.statictenants = permit.statictenants;
        
        this.staticleaseterm = permit.staticleaseterm;
        
        this.staticleasestatus = permit.staticleasestatus;
        this.staticpaymentstatus = permit.staticpaymentstatus;
        
        // ****
        // amendment and revoke
        // ****
        
        this.preAmendmentFields = permit.preAmendmentFields;
        
        this.amendementInitTS = permit.amendementInitTS;
        this.amendementCommitTS = permit.amendementCommitTS;
        this.amendmendedByUmap = permit.amendmendedByUmap;
        this.amendedByUser = permit.amendedByUser;
        
        this.revokedTS = permit.revokedTS;
        this.revokedByUMAP = permit.revokedByUMAP;
        this.revokedByUser = permit.revokedByUser;
        this.revokedReason = permit.revokedReason;
        
        
        
        this.createdTS = permit.createdTS;
        this.createdBy = permit.createdBy;
        this.createdByUserID = permit.createdByUserID;

        this.lastUpdatedTS = permit.lastUpdatedTS;
        this.lastUpdatedBy = permit.lastUpdatedBy;
        this.lastUpdatedByUserID = permit.lastUpdatedByUserID;

        this.deactivatedBy = permit.deactivatedBy;
        this.deactivatedTS = permit.deactivatedTS;
        this.deactivatedByUserID = permit.deactivatedByUserID;
    }
    
    
    @Override
    public void accept(IFaceSessionSyncVisitor visitor) {
        visitor.visit(this);
    }

    
    
      /**
     * Tacks whatever String is passed in and appends it to the config log.
     * At the end of calls to this method, this class's member dynamicPopulationLog
     * contains a simple String of the most current version of the log and can 
     * therefore be accessed through the regular getter
     * @param s 
     */
    public void appendToDynamicPopulationLog(String s){
        if(dynamicPopulationLog != null && s != null){
            StringBuilder sb = new StringBuilder(dynamicPopulationLog);
            sb.append(s);
            sb.append(Constants.FMT_HTML_BREAK);
            dynamicPopulationLog = sb.toString();
            
        }
    }

    /**
     * Clears the dynamic population log
     */
    public void clearDynamicPopulationLog(){
        dynamicPopulationLog = new String();
    }
    
    /**
     * Tacks on input to the finalization audit log
     * @param s 
     */
    public void appendToFinalizationAuditLog(String s){
        if(finalizationAuditLog != null && s != null){
            System.out.println("OccPermit.appendToFinalizationAuditLog | permitID " + permitID + " | log: " + s);
            StringBuilder sb = new StringBuilder(finalizationAuditLog);
            sb.append(s);
            sb.append(Constants.FMT_HTML_BREAK);
            finalizationAuditLog = sb.toString();
        }
        
    }
    
    
     /**
      * Special getter that splits on <br> to get only the officer's first name for reporting
      * 
     * @return the derivedOfficerNameOnly
     */
    public String getDerivedOfficerNameOnly() {
        if(staticofficername != null){
            if(staticofficername.contains("<br")){
                return staticofficername.split("<br>")[0];
            } else {
                return staticofficername;
            }
        }
        return derivedOfficerNameOnly;
    }

    /**
     * Hacky method for removing double breaks with single breaks for reporting
     * @return 
     */
    public String getStaticPropertyInfoBRCleaned(){
        if(staticpropertyinfo != null){
            return staticpropertyinfo.replace("<br><br>","<br>");
        }
        else {
            return "";
        }
    }
    
    /**
     * Builds a master list of all human links in this permit's belly--
     * namely the dynamic person links
     * @return 
     */
    public List<HumanLink> getCompletePermitHumanLinkList(){
        List<HumanLink> linkList = new ArrayList<>();
        if(ownerSellerLinkList != null && !ownerSellerLinkList.isEmpty()){
            linkList.addAll(ownerSellerLinkList);
        }
        if(buyerTenantLinkList != null && !buyerTenantLinkList.isEmpty()){
            linkList.addAll(buyerTenantLinkList);
        }
        if(managerLinkList != null && !managerLinkList.isEmpty()){
            linkList.addAll(managerLinkList);
        }
        if(tenantLinkList != null && !tenantLinkList.isEmpty()){
            linkList.addAll(tenantLinkList);
        }
        return linkList;
    }
    
    
    /**
     * Clears finalization audit log
     */
    public void clearFinalizationAuditLog(){
        finalizationAuditLog = new String();
    }
    /**
     * @return the permitID
     */
    public int getPermitID() {
        return permitID;
    }

    /**
     * @return the referenceNo
     */
    public String getReferenceNo() {
        return referenceNo;
    }

   


    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param permitID the permitID to set
     */
    public void setPermitID(int permitID) {
        this.permitID = permitID;
    }

    /**
     * @param referenceNo the referenceNo to set
     */
    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    
    
   

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }


    /**
     * @return the periodID
     */
    public int getPeriodID() {
        return periodID;
    }

    /**
     * @param periodID the periodID to set
     */
    public void setPeriodID(int periodID) {
        this.periodID = periodID;
    }

   

    /**
     * @return the finalizedts
     */
    public LocalDateTime getFinalizedts() {
        return finalizedts;
    }

    /**
     * @return the staticdateofapplication
     */
    public LocalDate getStaticdateofapplication() {
        return staticdateofapplication;
    }

    /**
     * @return the staticinitialinspection
     */
    public LocalDate getStaticinitialinspection() {
        return staticinitialinspection;
    }

    /**
     * @return the staticreinspectiondate
     */
    public LocalDate getStaticreinspectiondate() {
        return staticreinspectiondate;
    }

    /**
     * @return the staticfinalinspection
     */
    public LocalDate getStaticfinalinspection() {
        return staticfinalinspection;
    }

    /**
     * @return the staticdateofissue
     */
    public LocalDate getStaticdateofissue() {
        return staticdateofissue;
    }

    /**
     * @return the statictitle
     */
    public String getStatictitle() {
        return statictitle;
    }

    /**
     * @return the staticmuniaddress
     */
    public String getStaticmuniaddress() {
        return staticmuniaddress;
    }

    /**
     * @return the staticpropertyinfo
     */
    public String getStaticpropertyinfo() {
        return staticpropertyinfo;
    }

    /**
     * @return the staticownerseller
     */
    public String getStaticownerseller() {
        return staticownerseller;
    }

    /**
     * @return the staticcolumnlink
     */
    public String getStaticcolumnlink() {
        return staticcolumnlink;
    }

    /**
     * @return the staticbuyertenant
     */
    public String getStaticbuyertenant() {
        return staticbuyertenant;
    }

    /**
     * @return the staticproposeduse
     */
    public String getStaticproposeduse() {
        return staticproposeduse;
    }

    /**
     * @return the staticusecode
     */
    public String getStaticusecode() {
        return staticusecode;
    }

    /**
     * @return the staticpropclass
     */
    public String getStaticpropclass() {
        return staticpropclass;
    }

    /**
     * @return the staticofficername
     */
    public String getStaticofficername() {
        return staticofficername;
    }

    /**
     * @return the staticissuedundercodesourceid
     */
    public String getStaticissuedundercodesourceid() {
        return staticissuedundercodesourceid;
    }

    /**
     * @return the staticstipulations
     */
    public String getStaticstipulations() {
        return staticstipulations;
    }

    /**
     * @return the staticcomments
     */
    public String getStaticcomments() {
        return staticcomments;
    }

    /**
     * @return the staticmanager
     */
    public String getStaticmanager() {
        return staticmanager;
    }

    /**
     * @return the statictenants
     */
    public String getStatictenants() {
        return statictenants;
    }

    /**
     * @return the staticleaseterm
     */
    public String getStaticleaseterm() {
        return staticleaseterm;
    }

    /**
     * @return the staticleasestatus
     */
    public String getStaticleasestatus() {
        return staticleasestatus;
    }

    /**
     * @return the staticpaymentstatus
     */
    public String getStaticpaymentstatus() {
        return staticpaymentstatus;
    }

    /**
     * @return the staticnotice
     */
    public String getStaticnotice() {
        return staticnotice;
    }

    /**
     * @param finalizedts the finalizedts to set
     */
    public void setFinalizedts(LocalDateTime finalizedts) {
        this.finalizedts = finalizedts;
    }

    /**
     * @param staticdateofapplication the staticdateofapplication to set
     */
    public void setStaticdateofapplication(LocalDate staticdateofapplication) {
        this.staticdateofapplication = staticdateofapplication;
    }

    /**
     * @param staticinitialinspection the staticinitialinspection to set
     */
    public void setStaticinitialinspection(LocalDate staticinitialinspection) {
        this.staticinitialinspection = staticinitialinspection;
    }

    /**
     * @param staticreinspectiondate the staticreinspectiondate to set
     */
    public void setStaticreinspectiondate(LocalDate staticreinspectiondate) {
        this.staticreinspectiondate = staticreinspectiondate;
    }

    /**
     * @param staticfinalinspection the staticfinalinspection to set
     */
    public void setStaticfinalinspection(LocalDate staticfinalinspection) {
        this.staticfinalinspection = staticfinalinspection;
    }

    /**
     * @param staticdateofissue the staticdateofissue to set
     */
    public void setStaticdateofissue(LocalDate staticdateofissue) {
        this.staticdateofissue = staticdateofissue;
    }

    /**
     * @param statictitle the statictitle to set
     */
    public void setStatictitle(String statictitle) {
        this.statictitle = statictitle;
    }

    /**
     * @param staticmuniaddress the staticmuniaddress to set
     */
    public void setStaticmuniaddress(String staticmuniaddress) {
        this.staticmuniaddress = staticmuniaddress;
    }

    /**
     * @param staticpropertyinfo the staticpropertyinfo to set
     */
    public void setStaticpropertyinfo(String staticpropertyinfo) {
        this.staticpropertyinfo = staticpropertyinfo;
    }
    
    /**
     * @param configurePermitOnlyPropertyInfo the configurePermitOnlyPropertyInfo to
     */

    /**
     * @param staticownerseller the staticownerseller to set
     */
    public void setStaticownerseller(String staticownerseller) {
        this.staticownerseller = staticownerseller;
    }

    /**
     * @param staticcolumnlink the staticcolumnlink to set
     */
    public void setStaticcolumnlink(String staticcolumnlink) {
        this.staticcolumnlink = staticcolumnlink;
    }

    /**
     * @param staticbuyertenant the staticbuyertenant to set
     */
    public void setStaticbuyertenant(String staticbuyertenant) {
        this.staticbuyertenant = staticbuyertenant;
    }

    /**
     * @param staticproposeduse the staticproposeduse to set
     */
    public void setStaticproposeduse(String staticproposeduse) {
        this.staticproposeduse = staticproposeduse;
    }

    /**
     * @param staticusecode the staticusecode to set
     */
    public void setStaticusecode(String staticusecode) {
        this.staticusecode = staticusecode;
    }

    /**
     * @param staticpropclass the staticpropclass to set
     */
    public void setStaticpropclass(String staticpropclass) {
        this.staticpropclass = staticpropclass;
    }

    /**
     * @param staticofficername the staticofficername to set
     */
    public void setStaticofficername(String staticofficername) {
        this.staticofficername = staticofficername;
    }

    /**
     * @param staticissuedundercodesourceid the staticissuedundercodesourceid to set
     */
    public void setStaticissuedundercodesourceid(String staticissuedundercodesourceid) {
        this.staticissuedundercodesourceid = staticissuedundercodesourceid;
    }

    /**
     * @param staticstipulations the staticstipulations to set
     */
    public void setStaticstipulations(String staticstipulations) {
        this.staticstipulations = staticstipulations;
    }

    /**
     * @param staticcomments the staticcomments to set
     */
    public void setStaticcomments(String staticcomments) {
        this.staticcomments = staticcomments;
    }

    /**
     * @param staticmanager the staticmanager to set
     */
    public void setStaticmanager(String staticmanager) {
        this.staticmanager = staticmanager;
    }

    /**
     * @param statictenants the statictenants to set
     */
    public void setStatictenants(String statictenants) {
        this.statictenants = statictenants;
    }

    /**
     * @param staticleaseterm the staticleaseterm to set
     */
    public void setStaticleaseterm(String staticleaseterm) {
        this.staticleaseterm = staticleaseterm;
    }

    /**
     * @param staticleasestatus the staticleasestatus to set
     */
    public void setStaticleasestatus(String staticleasestatus) {
        this.staticleasestatus = staticleasestatus;
    }

    /**
     * @param staticpaymentstatus the staticpaymentstatus to set
     */
    public void setStaticpaymentstatus(String staticpaymentstatus) {
        this.staticpaymentstatus = staticpaymentstatus;
    }

    /**
     * @param staticnotice the staticnotice to set
     */
    public void setStaticnotice(String staticnotice) {
        this.staticnotice = staticnotice;
    }

    @Override
    public String getPKFieldName() {
        return PERMIT_PK;
    }

    @Override
    public int getDBKey() {
        return permitID;
    }

    @Override
    public String getDBTableName() {
        return TABLE_NAME;
    }

    /**
     * @return the finalizedBy
     */
    public User getFinalizedBy() {
        return finalizedBy;
    }

    /**
     * @param finalizedBy the finalizedBy to set
     */
    public void setFinalizedBy(User finalizedBy) {
        this.finalizedBy = finalizedBy;
    }

    /**
     * @return the staticconstructiontype
     */
    public String getStaticconstructiontype() {
        return staticconstructiontype;
    }

    /**
     * @param staticconstructiontype the staticconstructiontype to set
     */
    public void setStaticconstructiontype(String staticconstructiontype) {
        this.staticconstructiontype = staticconstructiontype;
    }

    /**
     * @return the ownerSellerLinkList
     */
    public List<HumanLink> getOwnerSellerLinkList() {
        return ownerSellerLinkList;
    }

    /**
     * @return the buyerTenantLinkList
     */
    public List<HumanLink> getBuyerTenantLinkList() {
        return buyerTenantLinkList;
    }

    /**
     * @return the managerLinkList
     */
    public List<HumanLink> getManagerLinkList() {
        return managerLinkList;
    }

    /**
     * @return the tenantLinkList
     */
    public List<HumanLink> getTenantLinkList() {
        return tenantLinkList;
    }

    /**
     * @return the parcelInfo
     */
    public ParcelInfo getParcelInfo() {
        return parcelInfo;
    }

    /**
     * @return the issuingOfficer
     */
    public User getIssuingOfficer() {
        return issuingOfficer;
    }

    /**
     * @return the textBlocks_stipulations
     */
    public List<TextBlock> getTextBlocks_stipulations() {
        return textBlocks_stipulations;
    }

    /**
     * @return the textBlocks_notice
     */
    public List<TextBlock> getTextBlocks_notice() {
        return textBlocks_notice;
    }

    /**
     * @return the textBlocks_comments
     */
    public List<TextBlock> getTextBlocks_comments() {
        return textBlocks_comments;
    }

    /**
     * @return the text_comments
     */
    public String getText_comments() {
        return text_comments;
    }

    /**
     * @return the dynamicDateOfApplication
     */
    public LocalDate getDynamicDateOfApplication() {
        return dynamicDateOfApplication;
    }

    /**
     * @return the dynamicInitialInspection
     */
    public LocalDate getDynamicInitialInspection() {
        return dynamicInitialInspection;
    }

    /**
     * @return the dynamicreinspectiondate
     */
    public LocalDate getDynamicreinspectiondate() {
        return dynamicreinspectiondate;
    }

    /**
     * @return the dynamicfinalinspection
     */
    public LocalDate getDynamicfinalinspection() {
        return dynamicfinalinspection;
    }

    /**
     * @return the dynamicdateofissue
     */
    public LocalDate getDynamicdateofissue() {
        return dynamicdateofissue;
    }
    

    /**
     * @param ownerSellerLinkList the ownerSellerLinkList to set
     */
    public void setOwnerSellerLinkList(List<HumanLink> ownerSellerLinkList) {
        this.ownerSellerLinkList = ownerSellerLinkList;
    }

    /**
     * @param buyerTenantLinkList the buyerTenantLinkList to set
     */
    public void setBuyerTenantLinkList(List<HumanLink> buyerTenantLinkList) {
        this.buyerTenantLinkList = buyerTenantLinkList;
    }

    /**
     * @param managerLinkList the managerLinkList to set
     */
    public void setManagerLinkList(List<HumanLink> managerLinkList) {
        this.managerLinkList = managerLinkList;
    }

    /**
     * @param tenantLinkList the tenantLinkList to set
     */
    public void setTenantLinkList(List<HumanLink> tenantLinkList) {
        this.tenantLinkList = tenantLinkList;
    }

    /**
     * @param parcelInfo the parcelInfo to set
     */
    public void setParcelInfo(ParcelInfo parcelInfo) {
        this.parcelInfo = parcelInfo;
    }

    /**
     * @param issuingOfficer the issuingOfficer to set
     */
    public void setIssuingOfficer(User issuingOfficer) {
        this.issuingOfficer = issuingOfficer;
    }

    /**
     * @param textBlocks_stipulations the textBlocks_stipulations to set
     */
    public void setTextBlocks_stipulations(List<TextBlock> textBlocks_stipulations) {
        this.textBlocks_stipulations = textBlocks_stipulations;
    }

    /**
     * @param textBlocks_notice the textBlocks_notice to set
     */
    public void setTextBlocks_notice(List<TextBlock> textBlocks_notice) {
        this.textBlocks_notice = textBlocks_notice;
    }

    /**
     * @param textBlocks_comments the textBlocks_comments to set
     */
    public void setTextBlocks_comments(List<TextBlock> textBlocks_comments) {
        this.textBlocks_comments = textBlocks_comments;
    }

    /**
     * @param text_comments the text_comments to set
     */
    public void setText_comments(String text_comments) {
        this.text_comments = text_comments;
    }

    /**
     * @param dynamicDateOfApplication the dynamicDateOfApplication to set
     */
    public void setDynamicDateOfApplication(LocalDate dynamicDateOfApplication) {
        this.dynamicDateOfApplication = dynamicDateOfApplication;
    }

    /**
     * @param dynamicInitialInspection the dynamicInitialInspection to set
     */
    public void setDynamicInitialInspection(LocalDate dynamicInitialInspection) {
        this.dynamicInitialInspection = dynamicInitialInspection;
    }

    /**
     * @param dynamicreinspectiondate the dynamicreinspectiondate to set
     */
    public void setDynamicreinspectiondate(LocalDate dynamicreinspectiondate) {
        this.dynamicreinspectiondate = dynamicreinspectiondate;
    }

    /**
     * @param dynamicfinalinspection the dynamicfinalinspection to set
     */
    public void setDynamicfinalinspection(LocalDate dynamicfinalinspection) {
        this.dynamicfinalinspection = dynamicfinalinspection;
    }

    /**
     * @param dynamicdateofissue the dynamicdateofissue to set
     */
    public void setDynamicdateofissue(LocalDate dynamicdateofissue) {
        this.dynamicdateofissue = dynamicdateofissue;
    }

    /**
     * @return the issuingCodeSourceList
     */
    public List<CodeSource> getIssuingCodeSourceList() {
        return issuingCodeSourceList;
    }

    /**
     * @param issuingCodeSourceList the issuingCodeSourceList to set
     */
    public void setIssuingCodeSourceList(List<CodeSource> issuingCodeSourceList) {
        this.issuingCodeSourceList = issuingCodeSourceList;
    }

    /**
     * @return the nullifiedTS
     */
    public LocalDateTime getNullifiedTS() {
        return nullifiedTS;
    }

    /**
     * @return the nullifiedBy
     */
    public User getNullifiedBy() {
        return nullifiedBy;
    }

    /**
     * @param nullifiedTS the nullifiedTS to set
     */
    public void setNullifiedTS(LocalDateTime nullifiedTS) {
        this.nullifiedTS = nullifiedTS;
    }

    /**
     * @param nullifiedBy the nullifiedBy to set
     */
    public void setNullifiedBy(User nullifiedBy) {
        this.nullifiedBy = nullifiedBy;
    }

    /**
     * @return the dynamicPopulationLog
     */
    public String getDynamicPopulationLog() {
        return dynamicPopulationLog;
    }

    /**
     * @param dynamicPopulationLog the dynamicPopulationLog to set
     */
    public void setDynamicPopulationLog(String dynamicPopulationLog) {
        this.dynamicPopulationLog = dynamicPopulationLog;
    }

    /**
     * @return the dynamicPopulationReadyForFinalizationTS
     */
    public LocalDateTime getDynamicPopulationReadyForFinalizationTS() {
        return dynamicPopulationReadyForFinalizationTS;
    }

    /**
     * @param dynamicPopulationReadyForFinalizationTS the dynamicPopulationReadyForFinalizationTS to set
     */
    public void setDynamicPopulationReadyForFinalizationTS(LocalDateTime dynamicPopulationReadyForFinalizationTS) {
        this.dynamicPopulationReadyForFinalizationTS = dynamicPopulationReadyForFinalizationTS;
    }

    /**
     * @return the dynamicsDateOfApplicationAppRef
     */
    public OccPermitApplication getDynamicsDateOfApplicationAppRef() {
        return dynamicsDateOfApplicationAppRef;
    }

    /**
     * @return the dynamicInitialInspectionFINRef
     */
    public FieldInspectionLight getDynamicInitialInspectionFINRef() {
        return dynamicInitialInspectionFINRef;
    }

    /**
     * @return the dynamicReInspectionFINRef
     */
    public FieldInspectionLight getDynamicReInspectionFINRef() {
        return dynamicReInspectionFINRef;
    }

    /**
     * @return the dynamicFinalInspectionFINRef
     */
    public FieldInspectionLight getDynamicFinalInspectionFINRef() {
        return dynamicFinalInspectionFINRef;
    }

    /**
     * @param dynamicsDateOfApplicationAppRef the dynamicsDateOfApplicationAppRef to set
     */
    public void setDynamicsDateOfApplicationAppRef(OccPermitApplication dynamicsDateOfApplicationAppRef) {
        this.dynamicsDateOfApplicationAppRef = dynamicsDateOfApplicationAppRef;
    }

    /**
     * @param dynamicInitialInspectionFINRef the dynamicInitialInspectionFINRef to set
     */
    public void setDynamicInitialInspectionFINRef(FieldInspectionLight dynamicInitialInspectionFINRef) {
        this.dynamicInitialInspectionFINRef = dynamicInitialInspectionFINRef;
    }

    /**
     * @param dynamicReInspectionFINRef the dynamicReInspectionFINRef to set
     */
    public void setDynamicReInspectionFINRef(FieldInspectionLight dynamicReInspectionFINRef) {
        this.dynamicReInspectionFINRef = dynamicReInspectionFINRef;
    }

    /**
     * @param dynamicFinalInspectionFINRef the dynamicFinalInspectionFINRef to set
     */
    public void setDynamicFinalInspectionFINRef(FieldInspectionLight dynamicFinalInspectionFINRef) {
        this.dynamicFinalInspectionFINRef = dynamicFinalInspectionFINRef;
    }

    /**
     * @return the finalizationAuditLog
     */
    public String getFinalizationAuditLog() {
        return finalizationAuditLog;
    }

    /**
     * @param finalizationAuditLog the finalizationAuditLog to set
     */
    public void setFinalizationAuditLog(String finalizationAuditLog) {
        this.finalizationAuditLog = finalizationAuditLog;
    }

    /**
     * @return the finalizationAuditPassTS
     */
    public LocalDateTime getFinalizationAuditPassTS() {
        return finalizationAuditPassTS;
    }

    /**
     * @param finalizationAuditPassTS the finalizationAuditPassTS to set
     */
    public void setFinalizationAuditPassTS(LocalDateTime finalizationAuditPassTS) {
        this.finalizationAuditPassTS = finalizationAuditPassTS;
    }

    /**
     * @return the dynamicDateExpiry
     */
    public LocalDate getDynamicDateExpiry() {
        return dynamicDateExpiry;
    }

    /**
     * @param dynamicDateExpiry the dynamicDateExpiry to set
     */
    public void setDynamicDateExpiry(LocalDate dynamicDateExpiry) {
        this.dynamicDateExpiry = dynamicDateExpiry;
    }

    /**
     * @return the staticdateofexpiry
     */
    public LocalDate getStaticdateofexpiry() {
        return staticdateofexpiry;
    }

    /**
     * @param staticdateofexpiry the staticdateofexpiry to set
     */
    public void setStaticdateofexpiry(LocalDate staticdateofexpiry) {
        this.staticdateofexpiry = staticdateofexpiry;
    }

    /**
     * @return the permitType
     */
    public OccPermitType getPermitType() {
        return permitType;
    }

    /**
     * @param permitType the permitType to set
     */
    public void setPermitType(OccPermitType permitType) {
        this.permitType = permitType;
    }

    /**
     * @return the issuingOfficerPerson
     */
    public Person getIssuingOfficerPerson() {
        return issuingOfficerPerson;
    }

    /**
     * @param issuingOfficerPerson the issuingOfficerPerson to set
     */
    public void setIssuingOfficerPerson(Person issuingOfficerPerson) {
        this.issuingOfficerPerson = issuingOfficerPerson;
    }


    /**
     * @return the staticOfficerSignaturePhotoDocID
     */
    public int getStaticOfficerSignaturePhotoDocID() {
        return staticOfficerSignaturePhotoDocID;
    }

    /**
     * @param staticOfficerSignaturePhotoDocID the staticOfficerSignaturePhotoDocID to set
     */
    public void setStaticOfficerSignaturePhotoDocID(int staticOfficerSignaturePhotoDocID) {
        this.staticOfficerSignaturePhotoDocID = staticOfficerSignaturePhotoDocID;
    }

  
    /**
     * @return the staticownersellerlabel
     */
    public String getStaticownersellerlabel() {
        return staticownersellerlabel;
    }

    /**
     * @return the staticbuyertenantlabel
     */
    public String getStaticbuyertenantlabel() {
        return staticbuyertenantlabel;
    }

    /**
     * @param staticownersellerlabel the staticownersellerlabel to set
     */
    public void setStaticownersellerlabel(String staticownersellerlabel) {
        this.staticownersellerlabel = staticownersellerlabel;
    }

    /**
     * @param staticbuyertenantlabel the staticbuyertenantlabel to set
     */
    public void setStaticbuyertenantlabel(String staticbuyertenantlabel) {
        this.staticbuyertenantlabel = staticbuyertenantlabel;
    }

    /**
     * @return the ownerSellerType
     */
    public OccPermitPersonListEnum getOwnerSellerType() {
        return ownerSellerType;
    }

    /**
     * @return the buyerTenantType
     */
    public OccPermitPersonListEnum getBuyerTenantType() {
        return buyerTenantType;
    }

    /**
     * @param ownerSellerType the ownerSellerType to set
     */
    public void setOwnerSellerType(OccPermitPersonListEnum ownerSellerType) {
        this.ownerSellerType = ownerSellerType;
    }

    /**
     * @param buyerTenantType the buyerTenantType to set
     */
    public void setBuyerTenantType(OccPermitPersonListEnum buyerTenantType) {
        this.buyerTenantType = buyerTenantType;
    }

    /**
     * @return the staticheaderphotodocid
     */
    public int getStaticheaderphotodocid() {
        return staticheaderphotodocid;
    }

    /**
     * @param staticheaderphotodocid the staticheaderphotodocid to set
     */
    public void setStaticheaderphotodocid(int staticheaderphotodocid) {
        this.staticheaderphotodocid = staticheaderphotodocid;
    }

    /**
     * @return the staticheaderWidthPx
     */
    public int getStaticheaderWidthPx() {
        return staticheaderWidthPx;
    }

    /**
     * @param staticheaderWidthPx the staticheaderWidthPx to set
     */
    public void setStaticheaderWidthPx(int staticheaderWidthPx) {
        this.staticheaderWidthPx = staticheaderWidthPx;
    }
    
    /**
     * Prioritizes static date of issuance, then dynamic date of issuance
     * then falls back to createdts
     * @param perm
     * @return 
     */
    private LocalDate determineGoverningDate(OccPermit perm){
        if(perm != null){
            
        if(this.getStaticdateofissue() != null){
                return perm.getStaticdateofissue();
            } else if(this.getDynamicdateofissue() != null){
                return perm.getDynamicdateofissue();
            } else if(this.getCreatedTS() != null && perm.getCreatedTS() != null){
                return LocalDate.from(perm.getCreatedTS());
            }
        }
        return null;
    }
    
    /**
     * Runs the current permit's date fields through the logic block
     * for determining the governing date of the permit 
     * @return 
     */
    public LocalDate getGoverningDate(){
       return determineGoverningDate(this);
    }

    /**
     * Note: this class has a natural ordering that is inconsistent with equals.
     * @param permit
     * @return 
     */
    @Override
    public int compareTo(OccPermit permit ) {
        if(permit == null){
            return 0;
        }
        
        LocalDate myDate = determineGoverningDate(this);
        LocalDate thatDate = determineGoverningDate(permit);
        if(myDate != null && thatDate != null){
            return myDate.compareTo(thatDate);
        } else {
            return 0;
        }
       
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + this.permitID;
        hash = 67 * hash + Objects.hashCode(this.referenceNo);
        hash = 67 * hash + this.periodID;
        hash = 67 * hash + this.parentParcelKey;
        hash = 67 * hash + Objects.hashCode(this.permitType);
        hash = 67 * hash + Objects.hashCode(this.notes);
        hash = 67 * hash + Objects.hashCode(this.finalizedts);
        hash = 67 * hash + Objects.hashCode(this.finalizedBy);
        hash = 67 * hash + Objects.hashCode(this.nullifiedTS);
        hash = 67 * hash + Objects.hashCode(this.nullifiedBy);
        hash = 67 * hash + Objects.hashCode(this.linkedEvents);
        hash = 67 * hash + Objects.hashCode(this.dynamicLastSaveTS);
        hash = 67 * hash + this.dynamicLastStep;
        hash = 67 * hash + Objects.hashCode(this.parcelInfo);
        hash = 67 * hash + Objects.hashCode(this.dynamicPopulationLog);
        hash = 67 * hash + Objects.hashCode(this.dynamicPopulationReadyForFinalizationTS);
        hash = 67 * hash + Objects.hashCode(this.finalizationAuditPassTS);
        hash = 67 * hash + Objects.hashCode(this.ownerSellerType);
        hash = 67 * hash + Arrays.deepHashCode(this.ownerSellerLinkIDList);
        hash = 67 * hash + Objects.hashCode(this.ownerSellerLinkList);
        hash = 67 * hash + Objects.hashCode(this.dynamicPersonsColumnLink);
        hash = 67 * hash + Objects.hashCode(this.buyerTenantType);
        hash = 67 * hash + Arrays.deepHashCode(this.buyerTenantLinkIDList);
        hash = 67 * hash + Objects.hashCode(this.buyerTenantLinkList);
        hash = 67 * hash + Arrays.deepHashCode(this.managerLinkIDList);
        hash = 67 * hash + Objects.hashCode(this.managerLinkList);
        hash = 67 * hash + Arrays.deepHashCode(this.tenantLinkIDList);
        hash = 67 * hash + Objects.hashCode(this.tenantLinkList);
        hash = 67 * hash + Objects.hashCode(this.issuingOfficer);
        hash = 67 * hash + Objects.hashCode(this.issuingOfficerPerson);
        hash = 67 * hash + Objects.hashCode(this.issuingCodeSourceList);
        hash = 67 * hash + Objects.hashCode(this.textBlocks_stipulations);
        hash = 67 * hash + Objects.hashCode(this.text_stipulations);
        hash = 67 * hash + Objects.hashCode(this.textBlocks_notice);
        hash = 67 * hash + Objects.hashCode(this.text_notices);
        hash = 67 * hash + Objects.hashCode(this.textBlocks_comments);
        hash = 67 * hash + Objects.hashCode(this.text_comments);
        hash = 67 * hash + Objects.hashCode(this.dynamicDateOfApplication);
        hash = 67 * hash + Objects.hashCode(this.dynamicsDateOfApplicationAppRef);
        hash = 67 * hash + Objects.hashCode(this.dynamicInitialInspection);
        hash = 67 * hash + Objects.hashCode(this.dynamicInitialInspectionFINRef);
        hash = 67 * hash + Objects.hashCode(this.dynamicreinspectiondate);
        hash = 67 * hash + Objects.hashCode(this.dynamicReInspectionFINRef);
        hash = 67 * hash + Objects.hashCode(this.dynamicfinalinspection);
        hash = 67 * hash + Objects.hashCode(this.dynamicFinalInspectionFINRef);
        hash = 67 * hash + Objects.hashCode(this.dynamicdateofissue);
        hash = 67 * hash + Objects.hashCode(this.dynamicDateExpiry);
        hash = 67 * hash + Objects.hashCode(this.humanLinkList);
        hash = 67 * hash + Objects.hashCode(this.finalizationAuditLog);
        hash = 67 * hash + this.staticheaderphotodocid;
        hash = 67 * hash + this.staticheaderWidthPx;
        hash = 67 * hash + Objects.hashCode(this.staticdateofapplication);
        hash = 67 * hash + Objects.hashCode(this.staticinitialinspection);
        hash = 67 * hash + Objects.hashCode(this.staticreinspectiondate);
        hash = 67 * hash + Objects.hashCode(this.staticfinalinspection);
        hash = 67 * hash + Objects.hashCode(this.staticdateofissue);
        hash = 67 * hash + Objects.hashCode(this.staticdateofexpiry);
        hash = 67 * hash + Objects.hashCode(this.statictitle);
        hash = 67 * hash + Objects.hashCode(this.staticmuniaddress);
        hash = 67 * hash + (this.staticdisplaymuniaddress ? 1 : 0);
        hash = 67 * hash + Objects.hashCode(this.staticpropertyinfo);
        hash = 67 * hash + Objects.hashCode(this.staticownersellerlabel);
        hash = 67 * hash + Objects.hashCode(this.staticownerseller);
        hash = 67 * hash + Objects.hashCode(this.configurePermitOnlyPropertyInfo);
        hash = 67 * hash + Objects.hashCode(this.staticcolumnlink);
        hash = 67 * hash + Objects.hashCode(this.staticbuyertenantlabel);
        hash = 67 * hash + Objects.hashCode(this.staticbuyertenant);
        hash = 67 * hash + Objects.hashCode(this.staticproposeduse);
        hash = 67 * hash + Objects.hashCode(this.staticusecode);
        hash = 67 * hash + Objects.hashCode(this.staticconstructiontype);
        hash = 67 * hash + Objects.hashCode(this.staticpropclass);
        hash = 67 * hash + Objects.hashCode(this.staticofficername);
        hash = 67 * hash + Objects.hashCode(this.derivedOfficerNameOnly);
        hash = 67 * hash + this.staticOfficerSignaturePhotoDocID;
        hash = 67 * hash + Objects.hashCode(this.staticissuedundercodesourceid);
        hash = 67 * hash + Objects.hashCode(this.staticstipulations);
        hash = 67 * hash + Objects.hashCode(this.staticcomments);
        hash = 67 * hash + Objects.hashCode(this.staticmanager);
        hash = 67 * hash + Objects.hashCode(this.statictenants);
        hash = 67 * hash + Objects.hashCode(this.staticleaseterm);
        hash = 67 * hash + Objects.hashCode(this.staticleasestatus);
        hash = 67 * hash + Objects.hashCode(this.staticpaymentstatus);
        hash = 67 * hash + Objects.hashCode(this.staticnotice);
        hash = 67 * hash + Objects.hashCode(this.preAmendmentFields);
        hash = 67 * hash + Objects.hashCode(this.amendementInitTS);
        hash = 67 * hash + Objects.hashCode(this.amendementCommitTS);
        hash = 67 * hash + Objects.hashCode(this.amendmendedByUmap);
        hash = 67 * hash + Objects.hashCode(this.amendedByUser);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OccPermit other = (OccPermit) obj;
        return this.permitID == other.permitID;
    }
    
    

    /**
     * @return the parentParcelKey
     */
    public int getParentParcelKey() {
        return parentParcelKey;
    }

    /**
     * @param parentParcelKey the parentParcelKey to set
     */
    public void setParentParcelKey(int parentParcelKey) {
        this.parentParcelKey = parentParcelKey;
    }

    @Override
    public void setLinkedEvents(List<EventCnF> evList) {
        linkedEvents = evList;
    }

    @Override
    public List<EventCnF> getLinkedEvents() {
        return linkedEvents;
    }

    @Override
    public EventLinkEnum getEventLinkEnum() {
        return EventLinkEnum.OCCPERMIT;
    }

    @Override
    public int getLinkTargetPrimaryKey() {
        return permitID;
    }

    /**
     * @return the dynamicLastStep
     */
    public int getDynamicLastStep() {
        return dynamicLastStep;
    }

    /**
     * @param dynamicLastStep the dynamicLastStep to set
     */
    public void setDynamicLastStep(int dynamicLastStep) {
        this.dynamicLastStep = dynamicLastStep;
    }

    /**
     * @return the dynamicLastSaveTS
     */
    public LocalDateTime getDynamicLastSaveTS() {
        return dynamicLastSaveTS;
    }

    /**
     * @param dynamicLastSaveTS the dynamicLastSaveTS to set
     */
    public void setDynamicLastSaveTS(LocalDateTime dynamicLastSaveTS) {
        this.dynamicLastSaveTS = dynamicLastSaveTS;
    }

    /**
     * @return the ownerSellerLinkIDList
     */
    public Integer[] getOwnerSellerLinkIDList() {
        return ownerSellerLinkIDList;
    }

    /**
     * @param ownerSellerLinkIDList the ownerSellerLinkIDList to set
     */
    public void setOwnerSellerLinkIDList(Integer[] ownerSellerLinkIDList) {
        this.ownerSellerLinkIDList = ownerSellerLinkIDList;
    }

    /**
     * @return the buyerTenantLinkIDList
     */
    public Integer[] getBuyerTenantLinkIDList() {
        return buyerTenantLinkIDList;
    }

    /**
     * @param buyerTenantLinkIDList the buyerTenantLinkIDList to set
     */
    public void setBuyerTenantLinkIDList(Integer[] buyerTenantLinkIDList) {
        this.buyerTenantLinkIDList = buyerTenantLinkIDList;
    }

    /**
     * @return the managerLinkIDList
     */
    public Integer[] getManagerLinkIDList() {
        return managerLinkIDList;
    }

    /**
     * @param managerLinkIDList the managerLinkIDList to set
     */
    public void setManagerLinkIDList(Integer[] managerLinkIDList) {
        this.managerLinkIDList = managerLinkIDList;
    }

    /**
     * @return the tenantLinkIDList
     */
    public Integer[] getTenantLinkIDList() {
        return tenantLinkIDList;
    }

    /**
     * @param tenantLinkIDList the tenantLinkIDList to set
     */
    public void setTenantLinkIDList(Integer[] tenantLinkIDList) {
        this.tenantLinkIDList = tenantLinkIDList;
    }

    /**
     * @return the text_notices
     */
    public String getText_notices() {
        return text_notices;
    }

    /**
     * @param text_notices the text_notices to set
     */
    public void setText_notices(String text_notices) {
        this.text_notices = text_notices;
    }

    /**
     * @return the text_stipulations
     */
    public String getText_stipulations() {
        return text_stipulations;
    }

    /**
     * @param text_stipulations the text_stipulations to set
     */
    public void setText_stipulations(String text_stipulations) {
        this.text_stipulations = text_stipulations;
    }

    /**
     * @return the dynamicPersonsColumnLink
     */
    public String getDynamicPersonsColumnLink() {
        return dynamicPersonsColumnLink;
    }

    /**
     * @param dynamicPersonsColumnLink the dynamicPersonsColumnLink to set
     */
    public void setDynamicPersonsColumnLink(String dynamicPersonsColumnLink) {
        this.dynamicPersonsColumnLink = dynamicPersonsColumnLink;
    }

    @Override
    public List<HumanLink> gethumanLinkList() {
        return humanLinkList;
    }

    @Override
    public void sethumanLinkList(List<HumanLink> hll) {
        humanLinkList = hll;
    }

    @Override
    public LinkedObjectSchemaEnum getHUMAN_LINK_SCHEMA_ENUM() {
        return OCCPERMIT_HUMAN_ENUM;
    }

    @Override
    public int getHostPK() {
        return permitID;
    }

    @Override
    public String getDescriptionString() {
        return "Occupancy Permit ID: " + permitID;
    }

    @Override
    public LinkedObjectSchemaEnum getUpstreamHumanLinkPoolEnum() {
        return LinkedObjectSchemaEnum.OccPeriodHuman;
    }

    @Override
    public int getUpstreamHumanLinkPoolFeederID() {
        return periodID;
    }

    /**
     * @return the amendmendedByUmap
     */
    public UserMuniAuthPeriod getAmendmendedByUmap() {
        return amendmendedByUmap;
    }

    /**
     * @param amendmendedByUmap the amendmendedByUmap to set
     */
    public void setAmendmendedByUmap(UserMuniAuthPeriod amendmendedByUmap) {
        this.amendmendedByUmap = amendmendedByUmap;
    }

    /**
     * @return the amendementCommitTS
     */
    public LocalDateTime getAmendementCommitTS() {
        return amendementCommitTS;
    }

    /**
     * @param amendementCommitTS the amendementCommitTS to set
     */
    public void setAmendementCommitTS(LocalDateTime amendementCommitTS) {
        this.amendementCommitTS = amendementCommitTS;
    }

    /**
     * @return the amendementInitTS
     */
    public LocalDateTime getAmendementInitTS() {
        return amendementInitTS;
    }

    /**
     * @param amendementInitTS the amendementInitTS to set
     */
    public void setAmendementInitTS(LocalDateTime amendementInitTS) {
        this.amendementInitTS = amendementInitTS;
    }

    /**
     * @return the preAmendmentFields
     */
    public OccPermitPreAmendmentFields getPreAmendmentFields() {
        return preAmendmentFields;
    }

    /**
     * @param preAmendmentFields the preAmendmentFields to set
     */
    public void setPreAmendmentFields(OccPermitPreAmendmentFields preAmendmentFields) {
        this.preAmendmentFields = preAmendmentFields;
    }

    /**
     * @return the amendedByUser
     */
    public User getAmendedByUser() {
        return amendedByUser;
    }

    /**
     * @param amendedByUser the amendedByUser to set
     */
    public void setAmendedByUser(User amendedByUser) {
        this.amendedByUser = amendedByUser;
    }

    /**
     * @return the staticdisplaymuniaddress
     */
    public boolean isStaticdisplaymuniaddress() {
        return staticdisplaymuniaddress;
    }

    /**
     * @param staticdisplaymuniaddress the staticdisplaymuniaddress to set
     */
    public void setStaticdisplaymuniaddress(boolean staticdisplaymuniaddress) {
        this.staticdisplaymuniaddress = staticdisplaymuniaddress;
    }

    /**
     * @return the revokedTS
     */
    public LocalDateTime getRevokedTS() {
        return revokedTS;
    }

    /**
     * @param revokedTS the revokedTS to set
     */
    public void setRevokedTS(LocalDateTime revokedTS) {
        this.revokedTS = revokedTS;
    }

    /**
     * @return the revokedByUMAP
     */
    public UserMuniAuthPeriod getRevokedByUMAP() {
        return revokedByUMAP;
    }

    /**
     * @param revokedByUMAP the revokedByUMAP to set
     */
    public void setRevokedByUMAP(UserMuniAuthPeriod revokedByUMAP) {
        this.revokedByUMAP = revokedByUMAP;
    }

    /**
     * @return the revokedReason
     */
    public String getRevokedReason() {
        return revokedReason;
    }

    /**
     * @param revokedReason the revokedReason to set
     */
    public void setRevokedReason(String revokedReason) {
        this.revokedReason = revokedReason;
    }

    /**
     * @return the revokedByUser
     */
    public User getRevokedByUser() {
        return revokedByUser;
    }

    /**
     * @param revokedByUser the revokedByUser to set
     */
    public void setRevokedByUser(User revokedByUser) {
        this.revokedByUser = revokedByUser;
    }

    /**
     * @return the staticdisplayperslinktenants
     */
    public boolean isStaticdisplayperslinktenants() {
        return staticdisplayperslinktenants;
    }

    /**
     * @param staticdisplayperslinktenants the staticdisplayperslinktenants to set
     */
    public void setStaticdisplayperslinktenants(boolean staticdisplayperslinktenants) {
        this.staticdisplayperslinktenants = staticdisplayperslinktenants;
    }

    /**
     * @return the staticdisplayperslinkmanagers
     */
    public boolean isStaticdisplayperslinkmanagers() {
        return staticdisplayperslinkmanagers;
    }

    /**
     * @param staticdisplayperslinkmanagers the staticdisplayperslinkmanagers to set
     */
    public void setStaticdisplayperslinkmanagers(boolean staticdisplayperslinkmanagers) {
        this.staticdisplayperslinkmanagers = staticdisplayperslinkmanagers;
    }

    /**
     * @return the staticdisplayperslinkrightcol
     */
    public boolean isStaticdisplayperslinkrightcol() {
        return staticdisplayperslinkrightcol;
    }

    /**
     * @param staticdisplayperslinkrightcol the staticdisplayperslinkrightcol to set
     */
    public void setStaticdisplayperslinkrightcol(boolean staticdisplayperslinkrightcol) {
        this.staticdisplayperslinkrightcol = staticdisplayperslinkrightcol;
    }

    /**
     * @return the staticdisplayperslinkleftcol
     */
    public boolean isStaticdisplayperslinkleftcol() {
        return staticdisplayperslinkleftcol;
    }

    /**
     * @param staticdisplayperslinkleftcol the staticdisplayperslinkleftcol to set
     */
    public void setStaticdisplayperslinkleftcol(boolean staticdisplayperslinkleftcol) {
        this.staticdisplayperslinkleftcol = staticdisplayperslinkleftcol;
    }

    /**
     * @return the adminOverrideTS
     */
    public LocalDateTime getAdminOverrideTS() {
        return adminOverrideTS;
    }

    /**
     * @param adminOverrideTS the adminOverrideTS to set
     */
    public void setAdminOverrideTS(LocalDateTime adminOverrideTS) {
        this.adminOverrideTS = adminOverrideTS;
    }

    /**
     * @return the adminOverrideUMAP
     */
    public UserMuniAuthPeriod getAdminOverrideUMAP() {
        return adminOverrideUMAP;
    }

    /**
     * @param adminOverrideUMAP the adminOverrideUMAP to set
     */
    public void setAdminOverrideUMAP(UserMuniAuthPeriod adminOverrideUMAP) {
        this.adminOverrideUMAP = adminOverrideUMAP;
    }

    /**
     * @return the inspectionRequirmentLog
     */
    public String getInspectionRequirmentLog() {
        return inspectionRequirmentLog;
    }

    /**
     * @param inspectionRequirmentLog the inspectionRequirmentLog to set
     */
    public void setInspectionRequirmentLog(String inspectionRequirmentLog) {
        this.inspectionRequirmentLog = inspectionRequirmentLog;
    }

    /**
     * @return the inspectionRequirmentPass
     */
    public boolean isInspectionRequirmentPass() {
        return inspectionRequirmentPass;
    }

    /**
     * @param inspectionRequirmentPass the inspectionRequirmentPass to set
     */
    public void setInspectionRequirmentPass(boolean inspectionRequirmentPass) {
        this.inspectionRequirmentPass = inspectionRequirmentPass;
    }

    
    
    
}
