/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities.occupancy;

import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import com.tcvcog.tcvce.entities.EventRealm;
import com.tcvcog.tcvce.entities.Person;
import com.tcvcog.tcvce.entities.TrackedEntity;
import com.tcvcog.tcvce.entities.User;
import com.tcvcog.tcvce.occupancy.application.FieldInspectionReInspectionConfig;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * Created to be the data light version of the classical field inspection
 * which drags around all its checklist gunk with its gazillion DB Calls
 * BlobLights 
 * @author Ellen Bascomb
 */
public class    FieldInspectionLight 
        extends TrackedEntity 
        implements Comparable<FieldInspectionLight>, IFaceCachable{
    
    final static String INSPECTION_TABLE_NAME = "occinspection";
    final static String INSPECTION_PK_FIELD = "inspectionid";
    
    
    /* ****************************************************************
     * *********            LINKING & IDs                   ***********
     * ****************************************************************
     */
    protected int inspectionID;
    /** ID for inspection that this inspection is a follow up to (if any) **/
    protected int followUpToInspectionID;
    protected int checklistTemplateID;
    
    protected EventRealm domainEnum;
    /** ID for the OccPeriod that this FieldInspection is for **/
    protected int occPeriodID;
    protected int cecaseID;
    
    
    /* ****************************************************************
     * *********            PARENT OBJECT INFO              ***********
     * ****************************************************************
     */
    // these TWO are temporary holders before assembling final string
    protected String occPeriodUnitNo;
    protected String ceCaseName;
    
    protected String parentPropertyAddress1Line;
    protected String parentPropertyAddress2LineEscapeFalse;
    protected String containingCECaseOccPeriodDescription;
    
    
    /* ****************************************************************
     * *********            TIMES & DATES                   ***********
     * ****************************************************************
     */
    protected LocalDate inspectionDate;
    protected LocalTime inspectionTimeStart;
    protected LocalTime inspectionTimeEnd;
    
    /* ****************************************************************
     * *********            REINSPECTION                    ***********
     * ****************************************************************
     */
    
    protected FieldInspectionReInspectionConfig reinspectionConfig;
    
    /* ****************************************************************
     * *********            META DATA                       ***********
     * ****************************************************************
     */
    
    protected User inspector;
    
    protected OccInspectionDetermination determination;
    protected User determinationBy;
    protected LocalDateTime determinationTS;
    
    protected OccInspectionCause cause;
    protected String editingLockMessage;

    protected int pacc;
    protected boolean enablePacc;
    
    protected int maxOccupantsAllowed;
    protected int numBedrooms;
    protected int numBathrooms;
    
    protected Person thirdPartyInspector;
    protected LocalDateTime thirdPartyInspectorApprovalTS;
    protected User thirdPartyApprovalBy;

    protected String notesPreInspection;

    protected String remarks;
    protected String generalComments;
    
    protected OccInspectionDispatch dispatch;
    protected boolean dispatchOnCreationCommit;
    
    /* ****************************************************************
     * *********            FIELD INITIATED FIN STUFF       ***********
     * ****************************************************************
     */
    
    protected String rawPropertyAddress;
    protected String rawPropertyUnit;
    protected User routingByUser;
    protected LocalDateTime routingTS;
    
    /* ****************************************************************
     * *********            CACHING                         ***********
     * ****************************************************************
     */
    
    @Override
    public int getCacheKey() {
        return inspectionID;
    }
    
    
    
    /* ****************************************************************
     * *********            CONSTRUCTORS                    ***********
     * ****************************************************************
     */
    
    public FieldInspectionLight() {
        
        
        
    }

   /**
    * Populates all the fields
     * @param finLight
    */
    public FieldInspectionLight(FieldInspectionLight finLight) {
       this.inspectionID = finLight.inspectionID;
        this.followUpToInspectionID = finLight.followUpToInspectionID;
        this.checklistTemplateID = finLight.checklistTemplateID;
        this.domainEnum = finLight.domainEnum;
        this.occPeriodID = finLight.occPeriodID;
        this.cecaseID = finLight.cecaseID;
        this.occPeriodUnitNo = finLight.occPeriodUnitNo;
        this.ceCaseName = finLight.ceCaseName;
        this.parentPropertyAddress1Line = finLight.parentPropertyAddress1Line;
        this.parentPropertyAddress2LineEscapeFalse = finLight.parentPropertyAddress2LineEscapeFalse;
        this.containingCECaseOccPeriodDescription = finLight.containingCECaseOccPeriodDescription;
        
        this.inspectionDate = finLight.inspectionDate;
        this.inspectionTimeStart = finLight.inspectionTimeStart;
        this.inspectionTimeEnd = finLight.inspectionTimeEnd;
        
        this.inspector = finLight.inspector;
        this.determination = finLight.determination;
        this.determinationBy = finLight.determinationBy;
        this.determinationTS = finLight.determinationTS;
        this.cause = finLight.cause;
        this.editingLockMessage = finLight.editingLockMessage;
        this.pacc = finLight.pacc;
        this.enablePacc = finLight.enablePacc;
        this.maxOccupantsAllowed = finLight.maxOccupantsAllowed;
        this.numBedrooms = finLight.numBedrooms;
        this.numBathrooms = finLight.numBathrooms;
        this.thirdPartyInspector = finLight.thirdPartyInspector;
        this.thirdPartyInspectorApprovalTS = finLight.thirdPartyInspectorApprovalTS;
        this.thirdPartyApprovalBy = finLight.thirdPartyApprovalBy;
        this.notesPreInspection = finLight.notesPreInspection;
        this.remarks = finLight.remarks;
        this.generalComments = finLight.generalComments;
        this.dispatch = finLight.dispatch;
        this.dispatchOnCreationCommit = finLight.dispatchOnCreationCommit;
        
        // field initiated
        this.rawPropertyAddress = finLight.rawPropertyAddress;
        this.rawPropertyUnit = finLight.rawPropertyUnit;
        this.routingByUser = finLight.routingByUser;
        this.routingTS = finLight.routingTS;
    }

    
     /**
     * Utility method for determining which date to use for comparison.
     * Note that inspections that haven't been approved probably won't have an
     * effective date of record so we should just use the creation timestamp
     * @param ins
     * @return the selected date for comparison
     */
    private LocalDateTime getDateForComparison(FieldInspectionLight ins){
        if(ins.getEffectiveDateOfRecord() == null){
            return ins.getCreatedTS();
        } else {
            return ins.getEffectiveDateOfRecord();
        }
    }
    

    @Override
    public int compareTo(FieldInspectionLight ins) {
        int compRes = getDateForComparison(this).compareTo(getDateForComparison(ins));
        return compRes;
    }
    
    /**
     * Derived boolean attribute to indicate if this inspection was
     * initiated in the field and has not yet been linked to the proper object. 
     * This method does so by looking at the raw prop address and/or unit
     * along with the routing TS.
     * @return True if this inspection is homeless
     */
    public boolean isRoutingRequired(){
        if(rawPropertyAddress != null || rawPropertyUnit != null){
            if(routingTS == null){
                return true;
            }
        }
        return false;
    }
    
    /**
     * Convenience getter for determining if this fin is dispatched, in which case
     * all sorts of controls will be deactivated
     * @return 
     */
    public boolean isDispatched(){
        return dispatch != null && dispatch.getSynchronizationTS() == null && dispatch.getDeactivatedTS() == null;
    }
    
    public boolean isDispatchAllowed(){
        return dispatch == null || (dispatch != null && dispatch.getDeactivatedTS() == null);
    }
    
    /**
     * Concatenates the raw property address and raw property unit address 
     * into one convenient String
     * @return 
     */
    public String getCombinedRawLocation(){
        StringBuilder sb = new StringBuilder();
        if(rawPropertyAddress != null){
            sb.append(rawPropertyAddress);
        }
        if(rawPropertyUnit != null){
            sb.append("; Unit: ");
            sb.append(rawPropertyUnit);
        }
        return sb.toString();
        
    }
    
    /**
     * Convenience method for finalization status; only looks at det object, not the TS 
     * @return true if we have a determination that's not null
     */
    public boolean isFinalized(){
        return determination != null;
    }
        
    /**
     * Convenience method that returns true if this inspection is a reinspection, 
     * meaning its followUpToReinspectionID is not ZERO
     * @return if this is a reinspection
     */
    public boolean isReinspection(){
        return followUpToInspectionID != 0;
    }
    
    // ADAPTOR GETTERS AND SETTERS: THESE ARE THE LEGACY NAMES AND RETURN TYPES
    // THAT HAVE BEEN ADAPTED TO WORK WITH THE UPDATED LOCALDATE AND LOCALTIME
    // MEMBER TYPES THAT WORK WITH THE PROPER UI COMPONENTS FOR DATES ONLY AND TIMES ONLY
    
      /**
       * Adaptor getter that gets us a LocalDateTime with the same legacy
       * getter name from the upgraded LocalDate typed member called inspectionDate
     * @return the effectiveDateOfRecord
     */
    public LocalDateTime getEffectiveDateOfRecord() {
        if(inspectionDate != null){
            return inspectionDate.atStartOfDay();
        }
        return null ;
    }

   

    /**
     * Adaptor getter for legacy compatibility that gets you a LocalDateTime
     * from the upgraded LocalTime member, meaning today's date at this inspection's
     * start time
     * @return the timeStart
     */
    public LocalDateTime getTimeStart() {
        if(inspectionTimeStart != null){
            LocalDate today = LocalDate.now();
            return LocalDateTime.of(today, inspectionTimeStart);
        }
        return null;
    }

   

    /**
     *  Adaptor getter for legacy compatibility that gets you a LocalDateTime
     * from the upgraded LocalTime member, meaning today's date at this inspection's
     * end time
     * @return the timeEnd
     */
    public LocalDateTime getTimeEnd() {
        if(inspectionTimeEnd != null){
            LocalDate today = LocalDate.now();
            return LocalDateTime.of(today, inspectionTimeEnd);
        }
        return null;
    }

    
    
    
    
    
    // REGULAR OLD GETTERS AND SETTERS
    
    
    
    
    /**
     * @return the parentPropertyAddress2LineEscapeFalse
     */
    public String getParentPropertyAddress2LineEscapeFalse() {
        return parentPropertyAddress2LineEscapeFalse;
    }

    /**
     * @return the containingCECaseOccPeriodDescription
     */
    public String getContainingCECaseOccPeriodDescription() {
        return containingCECaseOccPeriodDescription;
    }

    /**
     * @param parentPropertyAddress2LineEscapeFalse the parentPropertyAddress2LineEscapeFalse to set
     */
    public void setParentPropertyAddress2LineEscapeFalse(String parentPropertyAddress2LineEscapeFalse) {
        this.parentPropertyAddress2LineEscapeFalse = parentPropertyAddress2LineEscapeFalse;
    }

    /**
     * @param containingCECaseOccPeriodDescription the containingCECaseOccPeriodDescription to set
     */
    public void setContainingCECaseOccPeriodDescription(String containingCECaseOccPeriodDescription) {
        this.containingCECaseOccPeriodDescription = containingCECaseOccPeriodDescription;
    }

    /**
     * @return the inspectionID
     */
    public int getInspectionID() {
        return inspectionID;
    }

    /**
     * @param inspectionID the inspectionID to set
     */
    public void setInspectionID(int inspectionID) {
        this.inspectionID = inspectionID;
    }

    /**
     * @return the followUpToInspectionID
     */
    public int getFollowUpToInspectionID() {
        return followUpToInspectionID;
    }

    /**
     * @param followUpToInspectionID the followUpToInspectionID to set
     */
    public void setFollowUpToInspectionID(int followUpToInspectionID) {
        this.followUpToInspectionID = followUpToInspectionID;
    }

    /**
     * @return the checklistTemplateID
     */
    public int getChecklistTemplateID() {
        return checklistTemplateID;
    }

    /**
     * @param checklistTemplateID the checklistTemplateID to set
     */
    public void setChecklistTemplateID(int checklistTemplateID) {
        this.checklistTemplateID = checklistTemplateID;
    }

    /**
     * @return the domainEnum
     */
    public EventRealm getDomainEnum() {
        return domainEnum;
    }

    /**
     * @param domainEnum the domainEnum to set
     */
    public void setDomainEnum(EventRealm domainEnum) {
        this.domainEnum = domainEnum;
    }

    /**
     * @return the occPeriodID
     */
    public int getOccPeriodID() {
        return occPeriodID;
    }

    /**
     * @param occPeriodID the occPeriodID to set
     */
    public void setOccPeriodID(int occPeriodID) {
        this.occPeriodID = occPeriodID;
    }

    /**
     * @return the cecaseID
     */
    public int getCecaseID() {
        return cecaseID;
    }

    /**
     * @param cecaseID the cecaseID to set
     */
    public void setCecaseID(int cecaseID) {
        this.cecaseID = cecaseID;
    }

  

    /**
     * @return the inspector
     */
    public User getInspector() {
        return inspector;
    }

    /**
     * @param inspector the inspector to set
     */
    public void setInspector(User inspector) {
        this.inspector = inspector;
    }

    /**
     * @return the dispatch
     */
    public OccInspectionDispatch getDispatch() {
        return dispatch;
    }

    /**
     * @param dispatch the dispatch to set
     */
    public void setDispatch(OccInspectionDispatch dispatch) {
        this.dispatch = dispatch;
    }

    /**
     * @return the determination
     */
    public OccInspectionDetermination getDetermination() {
        return determination;
    }

    /**
     * @param determination the determination to set
     */
    public void setDetermination(OccInspectionDetermination determination) {
        this.determination = determination;
    }

    /**
     * @return the determinationBy
     */
    public User getDeterminationBy() {
        return determinationBy;
    }

    /**
     * @param determinationBy the determinationBy to set
     */
    public void setDeterminationBy(User determinationBy) {
        this.determinationBy = determinationBy;
    }

    /**
     * @return the determinationTS
     */
    public LocalDateTime getDeterminationTS() {
        return determinationTS;
    }

    /**
     * @param determinationTS the determinationTS to set
     */
    public void setDeterminationTS(LocalDateTime determinationTS) {
        this.determinationTS = determinationTS;
    }

    @Override
    public String getPKFieldName() {
        return INSPECTION_PK_FIELD;
    }

    @Override
    public int getDBKey() {
        return inspectionID;
    }

    @Override
    public String getDBTableName() {
        return INSPECTION_TABLE_NAME;
    }

    /**
     * @return the pacc
     */
    public int getPacc() {
        return pacc;
    }

    /**
     * @param pacc the pacc to set
     */
    public void setPacc(int pacc) {
        this.pacc = pacc;
    }

    /**
     * @return the enablePacc
     */
    public boolean isEnablePacc() {
        return enablePacc;
    }

    /**
     * @param enablePacc the enablePacc to set
     */
    public void setEnablePacc(boolean enablePacc) {
        this.enablePacc = enablePacc;
    }

    

    /**
     * @return the maxOccupantsAllowed
     */
    public int getMaxOccupantsAllowed() {
        return maxOccupantsAllowed;
    }

    /**
     * @param maxOccupantsAllowed the maxOccupantsAllowed to set
     */
    public void setMaxOccupantsAllowed(int maxOccupantsAllowed) {
        this.maxOccupantsAllowed = maxOccupantsAllowed;
    }

    /**
     * @return the numBedrooms
     */
    public int getNumBedrooms() {
        return numBedrooms;
    }

    /**
     * @param numBedrooms the numBedrooms to set
     */
    public void setNumBedrooms(int numBedrooms) {
        this.numBedrooms = numBedrooms;
    }

    /**
     * @return the numBathrooms
     */
    public int getNumBathrooms() {
        return numBathrooms;
    }

    /**
     * @param numBathrooms the numBathrooms to set
     */
    public void setNumBathrooms(int numBathrooms) {
        this.numBathrooms = numBathrooms;
    }

    /**
     * @return the thirdPartyInspector
     */
    public Person getThirdPartyInspector() {
        return thirdPartyInspector;
    }

    /**
     * @param thirdPartyInspector the thirdPartyInspector to set
     */
    public void setThirdPartyInspector(Person thirdPartyInspector) {
        this.thirdPartyInspector = thirdPartyInspector;
    }

    /**
     * @return the thirdPartyInspectorApprovalTS
     */
    public LocalDateTime getThirdPartyInspectorApprovalTS() {
        return thirdPartyInspectorApprovalTS;
    }

    /**
     * @param thirdPartyInspectorApprovalTS the thirdPartyInspectorApprovalTS to set
     */
    public void setThirdPartyInspectorApprovalTS(LocalDateTime thirdPartyInspectorApprovalTS) {
        this.thirdPartyInspectorApprovalTS = thirdPartyInspectorApprovalTS;
    }

    /**
     * @return the thirdPartyApprovalBy
     */
    public User getThirdPartyApprovalBy() {
        return thirdPartyApprovalBy;
    }

    /**
     * @param thirdPartyApprovalBy the thirdPartyApprovalBy to set
     */
    public void setThirdPartyApprovalBy(User thirdPartyApprovalBy) {
        this.thirdPartyApprovalBy = thirdPartyApprovalBy;
    }

    /**
     * @return the notesPreInspection
     */
    public String getNotesPreInspection() {
        return notesPreInspection;
    }

    /**
     * @param notesPreInspection the notesPreInspection to set
     */
    public void setNotesPreInspection(String notesPreInspection) {
        this.notesPreInspection = notesPreInspection;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the generalComments
     */
    public String getGeneralComments() {
        return generalComments;
    }

    /**
     * @param generalComments the generalComments to set
     */
    public void setGeneralComments(String generalComments) {
        this.generalComments = generalComments;
    }

    /**
     * @return the cause
     */
    public OccInspectionCause getCause() {
        return cause;
    }

    /**
     * @param cause the cause to set
     */
    public void setCause(OccInspectionCause cause) {
        this.cause = cause;
    }

    /**
     * @return the editingLockMessage
     */
    public String getEditingLockMessage() {
        return editingLockMessage;
    }

    /**
     * @param editingLockMessage the editingLockMessage to set
     */
    public void setEditingLockMessage(String editingLockMessage) {
        this.editingLockMessage = editingLockMessage;
    }

    /**
     * @return the ceCaseName
     */
    public String getCeCaseName() {
        return ceCaseName;
    }

    /**
     * @param ceCaseName the ceCaseName to set
     */
    public void setCeCaseName(String ceCaseName) {
        this.ceCaseName = ceCaseName;
    }

    /**
     * @return the occPeriodUnitNo
     */
    public String getOccPeriodUnitNo() {
        return occPeriodUnitNo;
    }

    /**
     * @param occPeriodUnitNo the occPeriodUnitNo to set
     */
    public void setOccPeriodUnitNo(String occPeriodUnitNo) {
        this.occPeriodUnitNo = occPeriodUnitNo;
    }

    /**
     * @return the parentPropertyAddress1Line
     */
    public String getParentPropertyAddress1Line() {
        return parentPropertyAddress1Line;
    }

    /**
     * @param parentPropertyAddress1Line the parentPropertyAddress1Line to set
     */
    public void setParentPropertyAddress1Line(String parentPropertyAddress1Line) {
        this.parentPropertyAddress1Line = parentPropertyAddress1Line;
    }

    /**
     * @return the routingTS
     */
    public LocalDateTime getRoutingTS() {
        return routingTS;
    }

    /**
     * @param routingTS the routingTS to set
     */
    public void setRoutingTS(LocalDateTime routingTS) {
        this.routingTS = routingTS;
    }

    /**
     * @return the routingByUser
     */
    public User getRoutingByUser() {
        return routingByUser;
    }

    /**
     * @param routingByUser the routingByUser to set
     */
    public void setRoutingByUser(User routingByUser) {
        this.routingByUser = routingByUser;
    }

    /**
     * @return the rawPropertyUnit
     */
    public String getRawPropertyUnit() {
        return rawPropertyUnit;
    }

    /**
     * @param rawPropertyUnit the rawPropertyUnit to set
     */
    public void setRawPropertyUnit(String rawPropertyUnit) {
        this.rawPropertyUnit = rawPropertyUnit;
    }

    /**
     * @return the rawPropertyAddress
     */
    public String getRawPropertyAddress() {
        return rawPropertyAddress;
    }

    /**
     * @param rawPropertyAddress the rawPropertyAddress to set
     */
    public void setRawPropertyAddress(String rawPropertyAddress) {
        this.rawPropertyAddress = rawPropertyAddress;
    }

    /**
     * @return the dispatchOnCreationCommit
     */
    public boolean isDispatchOnCreationCommit() {
        return dispatchOnCreationCommit;
    }

    /**
     * @param dispatchOnCreationCommit the dispatchOnCreationCommit to set
     */
    public void setDispatchOnCreationCommit(boolean dispatchOnCreationCommit) {
        this.dispatchOnCreationCommit = dispatchOnCreationCommit;
    }

    /**
     * @return the inspectionDate
     */
    public LocalDate getInspectionDate() {
        return inspectionDate;
    }

    /**
     * @param inspectionDate the inspectionDate to set
     */
    public void setInspectionDate(LocalDate inspectionDate) {
        this.inspectionDate = inspectionDate;
    }

    /**
     * @return the inspectionTimeStart
     */
    public LocalTime getInspectionTimeStart() {
        return inspectionTimeStart;
    }

    /**
     * @param inspectionTimeStart the inspectionTimeStart to set
     */
    public void setInspectionTimeStart(LocalTime inspectionTimeStart) {
        this.inspectionTimeStart = inspectionTimeStart;
    }

    /**
     * @return the inspectionTimeEnd
     */
    public LocalTime getInspectionTimeEnd() {
        return inspectionTimeEnd;
    }

    /**
     * @param inspectionTimeEnd the inspectionTimeEnd to set
     */
    public void setInspectionTimeEnd(LocalTime inspectionTimeEnd) {
        this.inspectionTimeEnd = inspectionTimeEnd;
    }

    /**
     * @return the reinspectionConfig
     */
    public FieldInspectionReInspectionConfig getReinspectionConfig() {
        return reinspectionConfig;
    }

    /**
     * @param reinspectionConfig the reinspectionConfig to set
     */
    public void setReinspectionConfig(FieldInspectionReInspectionConfig reinspectionConfig) {
        this.reinspectionConfig = reinspectionConfig;
    }

    

  
    
}
