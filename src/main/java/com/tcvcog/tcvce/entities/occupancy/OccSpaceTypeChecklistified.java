/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.entities.occupancy;

import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import java.util.List;
import java.time.LocalDateTime;


/**
 * Wrapper around an OccSpaceType to contain the checklist related metadata
 * @author Ellen Bascomb of Apartment 31Y
 */
public class OccSpaceTypeChecklistified
        extends OccSpaceType
        implements IFaceCachable{
    
    private int checklistSpaceTypeID;
    private int checklistParentID; // populated for auditing
    private boolean required;
    private boolean autoAddOnInspectionInit;
    private String notes;
    private List<OccSpaceElement> codeElementList;
    private LocalDateTime deactivatedTS;

    public OccSpaceTypeChecklistified(OccSpaceType ost){
        super(ost);
        
    }
    
    public OccSpaceTypeChecklistified(OccSpaceTypeChecklistified ostc){
        super(ostc);
        checklistSpaceTypeID = ostc.getChecklistSpaceTypeID();
        checklistParentID = ostc.getChecklistParentID();
        required = ostc.isRequired();
        autoAddOnInspectionInit = ostc.isAutoAddOnInspectionInit();

        notes = ostc.getNotes();
        codeElementList = ostc.getCodeElementList();
        deactivatedTS = ostc.getDeactivatedTS();
    }
    
    public OccSpaceTypeChecklistified(){
        
    }
    
    /**
     * Clones the given OSTC
     * @return 
     * @throws java.lang.CloneNotSupportedException 
     */
    public OccSpaceTypeChecklistified copy() {
        
        OccSpaceTypeChecklistified ostc = new OccSpaceTypeChecklistified(this);
       
        return ostc;
    }
    
    
    @Override
    public int getCacheKey() {
        return checklistSpaceTypeID;
    }
    
    
    
    /**
     * @return the codeElementList
     */
    public List<OccSpaceElement> getCodeElementList() {
        return codeElementList;
    }

    /**
     * @param codeElementList the codeElementList to set
     */
    public void setCodeElementList(List<OccSpaceElement> codeElementList) {
        this.codeElementList = codeElementList;
    }

    /**
     * @return the required
     */
    public boolean isRequired() {
        return required;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param required the required to set
     */
    public void setRequired(boolean required) {
        this.required = required;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return the checklistSpaceTypeID
     */
    public int getChecklistSpaceTypeID() {
        return checklistSpaceTypeID;
    }

    /**
     * @param checklistSpaceTypeID the checklistSpaceTypeID to set
     */
    public void setChecklistSpaceTypeID(int checklistSpaceTypeID) {
        this.checklistSpaceTypeID = checklistSpaceTypeID;
    }

    /**
     * @return the checklistParentID
     */
    public int getChecklistParentID() {
        return checklistParentID;
    }

    /**
     * @param checklistParentID the checklistParentID to set
     */
    public void setChecklistParentID(int checklistParentID) {
        this.checklistParentID = checklistParentID;
    }

    /**
     * @return the deactivatedTS
     */
    public LocalDateTime getDeactivatedTS() {
        return deactivatedTS;
    }

    /**
     * @param deactivatedTS the deactivatedTS to set
     */
    public void setDeactivatedTS(LocalDateTime deactivatedTS) {
        this.deactivatedTS = deactivatedTS;
    }

    /**
     * @return the autoAddOnInspectionInit
     */
    public boolean isAutoAddOnInspectionInit() {
        return autoAddOnInspectionInit;
    }

    /**
     * @param autoAddOnInspectionInit the autoAddOnInspectionInit to set
     */
    public void setAutoAddOnInspectionInit(boolean autoAddOnInspectionInit) {
        this.autoAddOnInspectionInit = autoAddOnInspectionInit;
    }

}
