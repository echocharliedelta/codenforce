/*
 * Copyright (C) 2018 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities.occupancy;

import com.tcvcog.tcvce.entities.*;
import java.util.Comparator;

/**
 * Time-aware statuses of occupancy periods (aka permit files)
 * @author ellen bascomb of apt 31y
 */
public enum OccPeriodStatusEnum {
    
    APPLICATION_AWAITING_REVIEW(           "Application awaiting review",
                                            "Ready for a review of completeness, correctnes, and payment.",
                                            40, 
                                            true,
                                            "pending",
                                            "per-priority-openblue"),
    
    PRE_INSPECTION(                         "Pre-Inspection Review", 
                                            "Pending the scheduling of inspection or waiting for inspection date",
                                            45, 
                                            true,
                                            "playlist_add_check",
                                            "per-priority-openblue"),
   
    
    INSPECTION_IN_PROCESS(                  "Inspection started but not finalized", 
                                            "Finalize the inspection.",
                                            50, 
                                            true,
                                            "checklist",
                                            "per-priority-actionrequiredyellow"),
   
    FAILED_FIN_REINSPECTIONWINDOW_INSIDE     ("Failed initial inspection; Inside reinspection window", 
                                            "Applicant must schedule reinspection and officer perform.",
                                            80, 
                                            true,
                                            "pending_actions",
                                            "per-priority-abandoneddarkyellow"),
  
    FAILED_FIN_REINSPECTIONWINDOW_EXPIRED   ("Failed initial inspection; OUTSIDE reinspection window", 
                                            "",
                                            95, 
                                            true,
                                            "alarm_on",
                                            "per-priority-actionpastduered"),
  
    PRE_PERMIT(                             "Passed inspection; prepare permit", 
                                            "",
                                            30, 
                                            true,
                                            "approval_delegation",
                                            "per-priority-actionrequiredyellow"),
  
    TEMPPERMIT_ISSUED_VALIDITYWINDOW_INSIDE ("Valid temporary certificate issued", 
                                            "",
                                            20, 
                                            true,
                                            "hourglass_empty",
                                            "per-priority-monitoringgreen"),
  
    TEMPPERMIT_ISSUED_VALIDITYWINDOW_EXPIRED("Expired temporary certificate", 
                                            "",
                                            100, 
                                            true,
                                            "notifications_active",
                                            "per-priority-actionpastduered"),
  
    NONEXPPERMIT_ISSUED(                    "Non-expiring permit issued", 
                                            "",
                                            10, 
                                            false,
                                            "workspace_premium",
                                            "per-priority-monitoringgreen"),
  
    FORCE_CLOSE(                            "File force closed by officer", 
                                            "",
                                            3, 
                                            false,
                                            "tab_close",
                                            "per-priority-closedgray"),
  
    SEQUENCE_ANOMALY(                       "File components configured nonconventionally", 
                                            "",
                                            1, 
                                            true,
                                            "warning",
                                            "per-priority-abandoneddarkyellow"),
  
    UNKNOWN(                                "Unknown", 
                                            "",
                                            0, 
                                            true,
                                            "psychology_alt",
                                            "per-priority-closedgray");
  
    
    private final String label;
    private final String description;
    private final Integer severity;
    private final boolean openPeriod;
    private final String iconPropertyLookup;
    private final String cssClass;
    
    private OccPeriodStatusEnum(String label, String descr, int sev, boolean isOpen, String iconLkup, String css){
        this.label = label;
        this.description = descr;
        this.severity = sev;
        this.iconPropertyLookup = iconLkup;
        this.openPeriod = isOpen;
        this.cssClass = css;
    }
    
    
    /**
     * ALlows for ordering of this enum by severity
     */
   public static Comparator<OccPeriodStatusEnum> severityComparator = new Comparator<OccPeriodStatusEnum>(){
        @Override
        public int compare(OccPeriodStatusEnum o1, OccPeriodStatusEnum o2) {
            // OPEN AI WROTE THIS
            Integer sev1 = (o1 != null) ? o1.getSeverity() : null;
            Integer sev2 = (o2 != null) ? o2.getSeverity() : null;

            if (sev1 == null && sev2 == null) {
                return 0; // Both are null, consider them equal
            } else if (sev1 == null) {
                return -1; // Null is considered smaller than non-null
            } else if (sev2 == null) {
                return 1; // Non-null is considered larger than null
            } else {
                return sev1.compareTo(sev2);
            }
        }
   };
    
    public String getLabel(){
        return label;
    }
    
    public int getOrder(){
        return getSeverity();
    }
    
    public String getIconPropertyLookup(){
        return iconPropertyLookup;
    }

    /**
     * @return the openPeriod
     */
    public boolean isOpenPeriod() {
        return openPeriod;
    }

    /**
     * @return the cssClass
     */
    public String getCssClass() {
        return cssClass;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return the severity
     */
    public Integer getSeverity() {
        return severity;
    }

}


