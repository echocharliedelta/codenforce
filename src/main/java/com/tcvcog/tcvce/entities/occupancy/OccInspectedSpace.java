/*
 * Copyright (C) 2018 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities.occupancy;

import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import com.tcvcog.tcvce.entities.CodeElement;
import com.tcvcog.tcvce.entities.SpaceArea;
import com.tcvcog.tcvce.entities.TreeNodeOrdinanceWrapper;
import com.tcvcog.tcvce.entities.User;
import com.tcvcog.tcvce.util.OISEComparator;
import com.tcvcog.tcvce.util.viewoptions.ViewOptionsOccChecklistItemsEnum;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;
import org.primefaces.model.TreeNode;

/**
 * Subclass of Space: stores inspection-specific data about each space element
 * that is part of the super class. When a space is inspected, the ArrayList of CodeElements
 * in the Space are wrapped in an inspection blanket and added to the 
 * inspectedElementList which captures the compliance status, comp date, and notes.
 * 
 * WARNING: DIMENSION SQ FT TRIGGER CALLS IN ALL THE DIMENSION SETTERS!!
 * 
 * @author ellen bascomb of apt 31y, Technology Rediscovery LLC 
 */
public class OccInspectedSpace
        implements  Serializable, 
                    Cloneable, 
                    Comparable<OccInspectedSpace>,
                    IFaceCachable{

    static final double MIN_SPACE_SQ_FT_FOR_DISPLAY = 0.1d;
    
    static final double INCHES_PER_FOOT = 12.0d;
    static final double CUBIC_INCHES_PER_CUBIC_FEET = 144.0d;
    
    private int inspectedSpaceID;
    
    protected OccSpaceTypeChecklistified type;
    
    private User addedToChecklistBy;
    private LocalDateTime addedToChecklistTS;
    
    private OccInspectableStatus status;
    
    private List<OccInspectedSpaceElement> inspectedElementList;
    private List<OccInspectedSpaceElement> failedElementListConfirmationPending;
    private List<OccInspectedSpaceElement> inspectedElementListVisible;
    private ViewOptionsOccChecklistItemsEnum viewSetting;
    
    private TreeNode<TreeNodeOrdinanceWrapper<OccInspectedSpaceElement>> inspectedElementTree;
    
    /**
     * Mostly deprecated
     */
    private List<OccInsElementGroup> inspectedElementGroupList;
    private Map<OccInspectionStatusEnum, List<OccInspectedSpaceElement>> elementStatusMap;
    
    /**
     * @Deprecated replaced with simple string
     */
    private OccLocationDescriptor location;
    
    private String locationString;
    private int floorNumber;


    // space params
    
    private double dim1LengthFeet;
    private double dim1LengthInches;
    
    private double dim1WidthFeet;
    private double dim1WidthInches;
    
    private double dim1TotalSqFeet;
    
    // area 2 -- same as area 1 :(
    private double dim2LengthFeet;
    private double dim2LengthInches;
    
    private double dim2WidthFeet;
    private double dim2WidthInches;
    
    private double dim2TotalSqFeet;
    
    // NOT USED YET
    private SpaceArea area1;
    // NOT USED YET
    private SpaceArea area2;
    
    // not used yet 
    private double dimCeilingHeightInches;
    
    private int maxOccPersons;
    private int maxSleepingPersons;
    
    private int egressCount;
    
    private boolean bedroom;
    private boolean living;
    private boolean kitchen;
    private boolean bathroom;
    private boolean finishedSpace;
    
    private String notes;

    private User deactivatedBy;
    private LocalDateTime deactivatedTS;
    
    //This field exists just to make it easier to find the inspection associated
    //with an inspected space.
    private int inspectionID;

    public OccInspectedSpace() {}

    public OccInspectedSpace(OccInspectedSpace space) {
        this.inspectedSpaceID = space.getInspectedSpaceID();
        this.type = space.getType();

        this.inspectedElementList = space.getInspectedElementList();
        this.inspectedElementListVisible = space.getInspectedElementListVisible();
        this.viewSetting = space.getViewSetting();

        this.inspectedElementGroupList = space.getInspectedElementGroupList();
        this.elementStatusMap = space.getElementStatusMap();
        
        this.location = space.getLocation();

        this.addedToChecklistBy = space.getAddedToChecklistBy();
        this.addedToChecklistTS = space.getAddedToChecklistTS();

        this.status = space.getStatus();

        this.inspectionID = space.getInspectionID();
    }
    
    /**
     * Computed field
     * @return 
     */
    public double getSpaceTotalSqFt(){
        return Math.round(dim1TotalSqFeet + dim2TotalSqFeet);
    }
    
    /**
     * Boolean switch for only displaying sq foot if over a tenth of a sq ft.
     * @return 
     */
    public boolean isDisplayTotalSqFt(){
        return getSpaceTotalSqFt() > MIN_SPACE_SQ_FT_FOR_DISPLAY;
    }
    
    // space area logic
     /**
     * Manual config after changes in the space dimension valuesedit-inspections-location-var
     * And this is using a form that contains feet and inches, but the DB is inches only
     */
    public void onDimensionValueChange(){
        dim1TotalSqFeet = computeTotalSqFeetFromDimensions(dim1LengthFeet, dim1LengthInches, dim1WidthFeet, dim1WidthInches);
        dim2TotalSqFeet = computeTotalSqFeetFromDimensions(dim2LengthFeet, dim2LengthInches, dim2WidthFeet, dim2WidthInches);
    }
    
    /**
     * Implements simple math for converting length and width of a rectangle to sq ft area
     * @param lft
     * @param lin
     * @param wft
     * @param win
     * @return 
     */
    private double computeTotalSqFeetFromDimensions(double lft, double lin, double wft, double win){
        
        double lintotal = (lft * INCHES_PER_FOOT) + lin;
        double wintotal = (wft * INCHES_PER_FOOT) + win;
        
        return (lintotal * wintotal) / CUBIC_INCHES_PER_CUBIC_FEET;
    }
    
    @Override
    public int getCacheKey() {
        return inspectedSpaceID;
    }

    /**
     * Configures the inspectedElementListVisible on this class
     * @param vo 
     */
    public void configureVisibleElementList(ViewOptionsOccChecklistItemsEnum vo) {
        viewSetting = vo;
        inspectedElementListVisible = new ArrayList<>();

        for(OccInspectedSpaceElement oise: inspectedElementList){
            switch(viewSetting){
                case ALL_ITEMS:
                    inspectedElementListVisible.add(oise);
                    break;
                case FAILED_ITEMS_ONLY:
                    // look for failed items
                    if(checkForFailure(oise)){
                        inspectedElementListVisible.add(oise);
                    } 
                    break;
                case FAILED_PASSEDWPHOTOFINDING:
                    if(checkForFailure(oise) || checkForPassPhotoOrFinding(oise)){
                        inspectedElementListVisible.add(oise);
                    }
                    break;
                case PASSED_AND_FAILED: 
                    if(checkForCompliance(oise) || checkForFailure(oise)){
                        inspectedElementListVisible.add(oise);
                    }
                    break;
                case PASSED_ITEMS:
                    if(checkForCompliance(oise)){
                        inspectedElementListVisible.add(oise);
                    }
                    break;
                case UNISPECTED_ITEMS_ONLY:
                    // look for failed items
                    if(checkForNoninspection(oise)){
                        inspectedElementListVisible.add(oise);
                    } 
                    break;
                default:
                    inspectedElementListVisible.add(oise);
            }
        }
        // sort by violated first
        OISEComparator oiseComp = new OISEComparator();
        Collections.sort(inspectedElementList, oiseComp);
    }
    
    /**
     * Convenience method for extracting passed and failed items for the tree view
     * @return only passed and failed items
     */
    public List<OccInspectedSpaceElement> getInspectedElementListPassFail(){
        configureVisibleElementList(ViewOptionsOccChecklistItemsEnum.PASSED_AND_FAILED);
        return inspectedElementListVisible;
    }
    
    /**
     * Internal orgran for inspected space element logic
     * @param oise
     * @return if the element should be included in a visible liste
     */
    private boolean checkForFailure(OccInspectedSpaceElement oise){
        return oise.getComplianceGrantedTS() == null && oise.getLastInspectedTS() != null;
        
    }
    
    /**
     * Internal orgran for inspected space element logic
     * @param oise
     * @return if the element should be included in a visible liste
     */
    private boolean checkForCompliance(OccInspectedSpaceElement oise){
        return oise.getComplianceGrantedTS() != null;
        
    }
    
    /**
     * Internal orgran for inspected space element logic
     * @param oise
     * @return if the element should be included in a visible liste
     */
    private boolean checkForNoninspection(OccInspectedSpaceElement oise){
        return oise.getComplianceGrantedTS() == null && oise.getLastInspectedTS() == null;
    }
    
    /**
     * Internal orgran for inspected space element logic
     * @param oise
     * @return if the element should be included in a visible liste
     */
    private boolean checkForPassPhotoOrFinding(OccInspectedSpaceElement oise){
        return checkForCompliance(oise) 
                && ((oise.getBlobList() != null && !oise.getBlobList().isEmpty())
                        || (oise.getInspectionNotes() != null && !oise.getInspectionNotes().equals("")));
        
    }
    
    public List<OccInspectedSpaceElement> getElementListPass(){
        if(elementStatusMap != null){
            return elementStatusMap.get(OccInspectionStatusEnum.PASS);
        }
        return new ArrayList<>();
    }
    
    public List<OccInspectedSpaceElement> getElementListFail(){
        if(elementStatusMap != null){
            return elementStatusMap.get(OccInspectionStatusEnum.VIOLATION);
        }
        return new ArrayList<>();
        
    }
    public List<OccInspectedSpaceElement> getElementListNotIns(){
        if(elementStatusMap != null){
            return elementStatusMap.get(OccInspectionStatusEnum.NOTINSPECTED);
        }
        return new ArrayList<>();
        
    }

    /**
     * @Deprecated
     * @return 
     */
    public List<OccLocationDescriptor> getAllUniqueLocationDescriptors() {
        Set<OccLocationDescriptor> locationDescriptors = new HashSet();

        for (OccInspectedSpaceElement inspectedSpaceElement : inspectedElementList) {
            locationDescriptors.add(inspectedSpaceElement.getLocation());
        }

        List<OccLocationDescriptor> locationDescriptorList = new ArrayList();
        locationDescriptorList.addAll(locationDescriptors);
        return locationDescriptorList;

    }
    
    public List<CodeElement> getInspectedCodeElementsWithoutShell() {
        List<CodeElement> eleList = new ArrayList<>();
        if(inspectedElementList != null){
            Iterator<OccInspectedSpaceElement> iter = inspectedElementList.iterator();
            while(iter.hasNext()){
                eleList.add(iter.next());
            }
        }
        return eleList;
    }
    

    /**
     * @return the inspectedElementList
     */
    public List<OccInspectedSpaceElement> getInspectedElementList() {
        return inspectedElementList;
    }

    /**
     * @param inspectedElementList the inspectedElementList to set
     */
    public void setInspectedElementList(List<OccInspectedSpaceElement> inspectedElementList) {
        this.inspectedElementList = inspectedElementList;
    }

    /**
     * @return the location
     */
    public OccLocationDescriptor getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(OccLocationDescriptor location) {
        this.location = location;
    }

   
    /**
     * @return the spaceType
     */
    public OccSpaceTypeChecklistified getType() {
        return type;
    }

  

    /**
     * @param type the spaceType to set
     */
    public void setType(OccSpaceTypeChecklistified type) {
        this.type = type;
    }


    /**
     * @return the addedToChecklistBy
     */
    public User getAddedToChecklistBy() {
        return addedToChecklistBy;
    }

    /**
     * @return the addedToChecklistTS
     */
    public LocalDateTime getAddedToChecklistTS() {
        return addedToChecklistTS;
    }

    /**
     * @param addedToChecklistBy the addedToChecklistBy to set
     */
    public void setAddedToChecklistBy(User addedToChecklistBy) {
        this.addedToChecklistBy = addedToChecklistBy;
    }

    /**
     * @param addedToChecklistTS the addedToChecklistTS to set
     */
    public void setAddedToChecklistTS(LocalDateTime addedToChecklistTS) {
        this.addedToChecklistTS = addedToChecklistTS;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.inspectedElementList);
        hash = 53 * hash + Objects.hashCode(this.location);
        hash = 53 * hash + Objects.hashCode(this.type);
        hash = 53 * hash + Objects.hashCode(this.addedToChecklistBy);
        hash = 53 * hash + Objects.hashCode(this.addedToChecklistTS);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OccInspectedSpace other = (OccInspectedSpace) obj;
        if (!Objects.equals(this.inspectedElementList, other.inspectedElementList)) {
            return false;
        }
        if (!Objects.equals(this.location, other.location)) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.addedToChecklistBy, other.addedToChecklistBy)) {
            return false;
        }
        if (!Objects.equals(this.addedToChecklistTS, other.addedToChecklistTS)) {
            return false;
        }
        return true;
    }

    /**
     * @return the inspectedSpaceID
     */
    public int getInspectedSpaceID() {
        return inspectedSpaceID;
    }

    /**
     * @param inspectedSpaceID the inspectedSpaceID to set
     */
    public void setInspectedSpaceID(int inspectedSpaceID) {
        this.inspectedSpaceID = inspectedSpaceID;
    }

    

    /**
     * @return the status
     */
    public OccInspectableStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(OccInspectableStatus status) {
        this.status = status;
    }

    /**
     * @return the inspectedElementListVisible
     */
    public List<OccInspectedSpaceElement> getInspectedElementListVisible() {
        
        return inspectedElementListVisible;
    }

    /**
     * @param inspectedElementListVisible the inspectedElementListVisible to set
     */
    public void setInspectedElementListVisible(List<OccInspectedSpaceElement> inspectedElementListVisible) {
        this.inspectedElementListVisible = inspectedElementListVisible;
    }

    /**
     * @return the viewSetting
     */
    public ViewOptionsOccChecklistItemsEnum getViewSetting() {
        
        return viewSetting;
    }

    /**
     * @param viewSetting the viewSetting to set
     */
    public void setViewSetting(ViewOptionsOccChecklistItemsEnum viewSetting) {
        this.viewSetting = viewSetting;
    }

    @Override
    public int compareTo(OccInspectedSpace o) {
        if(o == null){
            return 0;
        }
        if(this.floorNumber == o.floorNumber){
            if(locationString != null && o.locationString != null){
                return this.locationString.compareTo(o.locationString);
            }
        } else {
            return Integer.valueOf(this.floorNumber).compareTo(Integer.valueOf(o.floorNumber));
        }
        if(o.addedToChecklistTS == null || this.addedToChecklistTS == null){
            return 0;
        }
        return this.addedToChecklistTS.compareTo(o.addedToChecklistTS);
    }

    public int getInspectionID() {
        return inspectionID;
    }

    public void setInspectionID(int inspectionID) {
        this.inspectionID = inspectionID;
    }

    /**
     * @return the inspectedElementGroupList
     */
    public List<OccInsElementGroup> getInspectedElementGroupList() {
        return inspectedElementGroupList;
    }

    /**
     * @param inspectedElementGroupList the inspectedElementGroupList to set
     */
    public void setInspectedElementGroupList(List<OccInsElementGroup> inspectedElementGroupList) {
        this.inspectedElementGroupList = inspectedElementGroupList;
    }

    /**
     * @return the elementStatusMap
     */
    public Map<OccInspectionStatusEnum, List<OccInspectedSpaceElement>> getElementStatusMap() {
        return elementStatusMap;
    }

    /**
     * @param elementStatusMap the elementStatusMap to set
     */
    public void setElementStatusMap(Map<OccInspectionStatusEnum, List<OccInspectedSpaceElement>> elementStatusMap) {
        this.elementStatusMap = elementStatusMap;
    }

    public User getDeactivatedBy() {
        return deactivatedBy;
    }

    public void setDeactivatedBy(User deactivatedBy) {
        this.deactivatedBy = deactivatedBy;
    }

    public LocalDateTime getDeactivatedTS() {
        return deactivatedTS;
    }

    public void setDeactivatedTS(LocalDateTime deactivatedTS) {
        this.deactivatedTS = deactivatedTS;
    }

    /**
     * @return the inspectedElementTree
     */
    public TreeNode<TreeNodeOrdinanceWrapper<OccInspectedSpaceElement>> getInspectedElementTree() {
        return inspectedElementTree;
    }

    /**
     * @param inspectedElementTree the inspectedElementTree to set
     */
    public void setInspectedElementTree(TreeNode<TreeNodeOrdinanceWrapper<OccInspectedSpaceElement>> inspectedElementTree) {
        this.inspectedElementTree = inspectedElementTree;
    }

    /**
     * @return the failedElementListConfirmationPending
     */
    public List<OccInspectedSpaceElement> getFailedElementListConfirmationPending() {
        return failedElementListConfirmationPending;
    }

    /**
     * @param failedElementListConfirmationPending the failedElementListConfirmationPending to set
     */
    public void setFailedElementListConfirmationPending(List<OccInspectedSpaceElement> failedElementListConfirmationPending) {
        this.failedElementListConfirmationPending = failedElementListConfirmationPending;
    }

    /**
     * @return the floorNumber
     */
    public int getFloorNumber() {
        return floorNumber;
    }

    /**
     * @param floorNumber the floorNumber to set
     */
    public void setFloorNumber(int floorNumber) {
        this.floorNumber = floorNumber;
    }

    /**
     * @return the dim1LengthInches
     */
    public double getDim1LengthInches() {
        return dim1LengthInches;
    }

    /**
     * @param dim1LengthInches the dim1LengthInches to set
     */
    public void setDim1LengthInches(double dim1LengthInches) {
        this.dim1LengthInches = dim1LengthInches;
        onDimensionValueChange();
    }

    /**
     * @return the dim1WidthInches
     */
    public double getDim1WidthInches() {
        return dim1WidthInches;
    }

    /**
     * @param dim1WidthInches the dim1WidthInches to set
     */
    public void setDim1WidthInches(double dim1WidthInches) {
        this.dim1WidthInches = dim1WidthInches;
        onDimensionValueChange();
    }

    /**
     * @return the dim1TotalSqFeet
     */
    public double getDim1TotalSqFeet() {
        return dim1TotalSqFeet;
    }

    /**
     * @param dim1TotalSqFeet the dim1TotalSqFeet to set
     */
    public void setDim1TotalSqFeet(double dim1TotalSqFeet) {
        this.dim1TotalSqFeet = dim1TotalSqFeet;
        onDimensionValueChange();
    }

    /**
     * @return the dim2LengthInches
     */
    public double getDim2LengthInches() {
        return dim2LengthInches;
    }

    /**
     * @param dim2LengthInches the dim2LengthInches to set
     */
    public void setDim2LengthInches(double dim2LengthInches) {
        this.dim2LengthInches = dim2LengthInches;
        onDimensionValueChange();
    }

    /**
     * @return the dim2WidthInches
     */
    public double getDim2WidthInches() {
        return dim2WidthInches;
    }

    /**
     * @param dim2WidthInches the dim2WidthInches to set
     */
    public void setDim2WidthInches(double dim2WidthInches) {
        this.dim2WidthInches = dim2WidthInches;
        onDimensionValueChange();
    }

    /**
     * @return the dim2TotalSqFeet
     */
    public double getDim2TotalSqFeet() {
        return dim2TotalSqFeet;
    }

    /**
     * @param dim2TotalSqFeet the dim2TotalSqFeet to set
     */
    public void setDim2TotalSqFeet(double dim2TotalSqFeet) {
        this.dim2TotalSqFeet = dim2TotalSqFeet;
    }

    /**
     * @return the dimCeilingHeightInches
     */
    public double getDimCeilingHeightInches() {
        return dimCeilingHeightInches;
    }

    /**
     * @param dimCeilingHeightInches the dimCeilingHeightInches to set
     */
    public void setDimCeilingHeightInches(int dimCeilingHeightInches) {
        this.dimCeilingHeightInches = dimCeilingHeightInches;
    }

    /**
     * @return the maxOccPersons
     */
    public int getMaxOccPersons() {
        return maxOccPersons;
    }

    /**
     * @param maxOccPersons the maxOccPersons to set
     */
    public void setMaxOccPersons(int maxOccPersons) {
        this.maxOccPersons = maxOccPersons;
    }

    /**
     * @return the maxSleepingPersons
     */
    public int getMaxSleepingPersons() {
        return maxSleepingPersons;
    }

    /**
     * @param maxSleepingPersons the maxSleepingPersons to set
     */
    public void setMaxSleepingPersons(int maxSleepingPersons) {
        this.maxSleepingPersons = maxSleepingPersons;
    }

    /**
     * @return the egressCount
     */
    public int getEgressCount() {
        return egressCount;
    }

    /**
     * @param egressCount the egressCount to set
     */
    public void setEgressCount(int egressCount) {
        this.egressCount = egressCount;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return the locationString
     */
    public String getLocationString() {
        return locationString;
    }

    /**
     * @param locationString the locationString to set
     */
    public void setLocationString(String locationString) {
        this.locationString = locationString;
    }

    /**
     * @return the bedroom
     */
    public boolean isBedroom() {
        return bedroom;
    }

    /**
     * @param bedroom the bedroom to set
     */
    public void setBedroom(boolean bedroom) {
        this.bedroom = bedroom;
    }

    /**
     * @return the living
     */
    public boolean isLiving() {
        return living;
    }

    /**
     * @param living the living to set
     */
    public void setLiving(boolean living) {
        this.living = living;
    }

    /**
     * @return the kitchen
     */
    public boolean isKitchen() {
        return kitchen;
    }

    /**
     * @param kitchen the kitchen to set
     */
    public void setKitchen(boolean kitchen) {
        this.kitchen = kitchen;
    }

    /**
     * @return the bathroom
     */
    public boolean isBathroom() {
        return bathroom;
    }

    /**
     * @param bathroom the bathroom to set
     */
    public void setBathroom(boolean bathroom) {
        this.bathroom = bathroom;
    }

    /**
     * @return the finishedSpace
     */
    public boolean isFinishedSpace() {
        return finishedSpace;
    }

    /**
     * @param finishedSpace the finishedSpace to set
     */
    public void setFinishedSpace(boolean finishedSpace) {
        this.finishedSpace = finishedSpace;
    }

    /**
     * @return the area1
     */
    public SpaceArea getArea1() {
        return area1;
    }

    /**
     * @param area1 the area1 to set
     */
    public void setArea1(SpaceArea area1) {
        this.area1 = area1;
    }

    /**
     * @return the area2
     */
    public SpaceArea getArea2() {
        return area2;
    }

    /**
     * @param area2 the area2 to set
     */
    public void setArea2(SpaceArea area2) {
        this.area2 = area2;
    }

   
    /**
     * @return the dim2WidthFeet
     */
    public double getDim2WidthFeet() {
        return dim2WidthFeet;
    }

    /**
     * @param dim2WidthFeet the dim2WidthFeet to set
     */
    public void setDim2WidthFeet(double dim2WidthFeet) {
        this.dim2WidthFeet = dim2WidthFeet;
        onDimensionValueChange();
    }

    /**
     * @return the dim2LengthFeet
     */
    public double getDim2LengthFeet() {
        return dim2LengthFeet;
    }

    /**
     * @param dim2LengthFeet the dim2LengthFeet to set
     */
    public void setDim2LengthFeet(double dim2LengthFeet) {
        this.dim2LengthFeet = dim2LengthFeet;
        onDimensionValueChange();
    }

    /**
     * @return the dim1WidthFeet
     */
    public double getDim1WidthFeet() {
        return dim1WidthFeet;
    }

    /**
     * @param dim1WidthFeet the dim1WidthFeet to set
     */
    public void setDim1WidthFeet(double dim1WidthFeet) {
        this.dim1WidthFeet = dim1WidthFeet;
        onDimensionValueChange();
    }

    /**
     * @return the dim1LengthFeet
     */
    public double getDim1LengthFeet() {
        return dim1LengthFeet;
    }

    /**
     * @param dim1LengthFeet the dim1LengthFeet to set
     */
    public void setDim1LengthFeet(double dim1LengthFeet) {
        this.dim1LengthFeet = dim1LengthFeet;
        onDimensionValueChange();
    }

   
}