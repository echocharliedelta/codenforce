/*
 * Copyright (C) 2021 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities.occupancy;

import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.entities.BlobLight;
import com.tcvcog.tcvce.entities.BlobLinkEnum;
import com.tcvcog.tcvce.entities.EventCnF;
import com.tcvcog.tcvce.entities.EventLinkEnum;
import com.tcvcog.tcvce.entities.EventRealm;
import com.tcvcog.tcvce.entities.IFace_BlobHolder;
import com.tcvcog.tcvce.entities.IFace_EventLinked;
import com.tcvcog.tcvce.entities.IFace_PermissionsMuniProfileGoverned;
import com.tcvcog.tcvce.entities.User;
import com.tcvcog.tcvce.util.viewoptions.ViewOptionsOccChecklistItemsEnum;

import java.time.LocalDateTime;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Referred to as a FieldInspectionDataHeavy even though this class's name doesn't
 * follow that convention of appending DataHeavy onto subclass names that 
 * contain a bunch of inner objects. This nonconformity was allowed
 * due to the extensive use of FieldInspection class references
 * when we broke out the lighter superclass to help with load times.
 * 
 * 
 * This whole class represents a "clipboard," where each item in the inspectedSpaceList
 * represents a sheet of paper listing all the code elements that were inspected in that particular space
 * and of that particular type with its own injected location descriptor.
 *
 * Every sheet of paper i.e. an element in the inspectedSpaceList can only be of one space type--that space type
 * lives inside of the OccInspectedSpace, and that space type was birthed from an OccSpaceTypeInspectionDirective.
 *
 * It is this object that gets passed to the "inspectionaction" methods on the occ coordinator to house the logic for
 * setting the right member variables on OccInspectedSpace and their OccInspectedSpaceElement classes.
 *
 * @author Adam Gutonski and Sylvia
 */
public  class       FieldInspection
        extends     FieldInspectionLight 
        implements  IFace_BlobHolder,
                    IFace_PermissionsMuniProfileGoverned, 
                    IFace_EventLinked{
    
    private final static BlobLinkEnum BLOB_LINK_ENUM = BlobLinkEnum.FIELD_INSPECTION;
    private final static BlobLinkEnum BLOP_UPSPTREAM_POOL_OCC = BlobLinkEnum.OCC_PERIOD;
    private final static BlobLinkEnum BLOP_UPSPTREAM_POOL_CECASE = BlobLinkEnum.CE_CASE;

    private boolean readyForPassedCertification;
    
    /**
     * As inspections unfold, ChecklistSpaceTypes are turned into OccInspectedSpace 
     * objects
     */
    private OccChecklistTemplate checklistTemplate;
    
    private List<OccInspectedSpace> inspectedSpaceList;
    private List<OccInspectedSpace> inspectedSpaceListVisible;
    private ViewOptionsOccChecklistItemsEnum viewSetting;
    private boolean includeEmptySpaces;
    
    private List<BlobLight> blobList;
    
    private List<EventCnF> linkedEvents;
    
    /**
     * Populates the super class fields
     * @param finLight 
     */
    public FieldInspection(FieldInspectionLight finLight){
        super(finLight);
    }
    
    /**
     * Populates all members
     * @param fin 
     */
    public FieldInspection(FieldInspection fin){
        if(fin != null){
            this.createdBy = fin.createdBy;
            this.createdTS = fin.createdTS;
            this.lastUpdatedBy = fin.lastUpdatedBy;
            this.lastUpdatedTS = fin.lastUpdatedTS;
            this.deactivatedBy = fin.deactivatedBy;
            this.deactivatedTS = fin.deactivatedTS;
            this.checklistTemplate = fin.checklistTemplate;
            this.viewSetting = fin.viewSetting;
            this.includeEmptySpaces = fin.includeEmptySpaces;

            this.inspectedSpaceList = fin.inspectedSpaceList;
            this.inspectedSpaceListVisible = fin.inspectedSpaceListVisible;
            this.blobList = fin.blobList;

        }
        viewSetting = ViewOptionsOccChecklistItemsEnum.ALL_ITEMS;
    }
    
    
    /**
     * No Arg constructor for FieldInspections:
     * I setup lists
     */
    public FieldInspection() {

        inspectedSpaceList = new ArrayList<>();
        inspectedSpaceListVisible = new ArrayList<>();
        viewSetting = ViewOptionsOccChecklistItemsEnum.ALL_ITEMS;
    }
    
    /**
     * Logic container for determining if the browser users can edit the inspection, 
     * and this is based on the status of the dispatch object, if any, and the inspections
     * finalization status
     * @return if the browser UI should allow users to edit inspection fields
     */
    public boolean isBrowserUserEditingAllowed(){
        if(deactivatedTS != null){
            return false;
        }
        if(dispatch != null){
            if(dispatch.getSynchronizationTS() == null && dispatch.getDeactivatedTS() == null){
                return false;
            }
        }
        return true;
    }
    
    /** 
     * Special getter for subsets of elements
     * @return the inspectedSpaceListVisible
     */
    public List<OccInspectedSpace> getInspectedSpaceListVisible() {
        

        return inspectedSpaceListVisible;
    }

    /**
     * Adapter who returns this blob holder's list size in a safe way for the UI elements to display
     * 
     * @return 
     */
    public int getBlobListSafeSize(){
        if(blobList != null){
            return blobList.size();
        } else {
            return 0;
        }
    }
    
    /**
     * sorts space list
     */
    public void configureSpaceList(){
        if(inspectedSpaceList != null && !inspectedSpaceList.isEmpty()){
            Collections.sort(inspectedSpaceList);
            Collections.reverse(inspectedSpaceList);
        }
    }

    /**
     * Uses the Inspection's view setting (the given one if not null)
     * to build a list of visible inspected spaces based on 
     * @param viewEnum if not null, I become this inspection's default view setting
     */
    public void configureVisibleSpaceElementList(ViewOptionsOccChecklistItemsEnum viewEnum) {
        if(viewEnum != null){
            viewSetting = viewEnum;
        }
        
        inspectedSpaceListVisible.clear();
        for (OccInspectedSpace ois : inspectedSpaceList) { 
            ois.configureVisibleElementList(viewSetting);
            if(!ois.getInspectedElementListVisible().isEmpty()
                    || (ois.getInspectedElementListVisible().isEmpty() && includeEmptySpaces)){
                inspectedSpaceListVisible.add(ois);
            }
        } // close for over inspectedspaces
        if(inspectedSpaceListVisible != null && !inspectedSpaceListVisible.isEmpty()){
            Collections.sort(inspectedSpaceListVisible);
            Collections.reverse(inspectedSpaceListVisible);
        }
    }
    
     /**
     * I extract all failed elements who have a true flag on their migrate to CE case
     * in their belly
     * @param respectMigrationFlag when true only include ord in list if marked for migration in fin, 
     * otherwise add all failed ords; As of June 2024 this flag is not being used by the migration facility
     * @return a list, perhaps with eces to include in a case
     * @throws BObStatusException 
     */
    public List<OccInspectedSpaceElement> extractFailedItemsForCECaseMigration(boolean respectMigrationFlag) 
            throws BObStatusException{
        
        List<OccInspectedSpaceElement> eceList = new ArrayList<>();
        if(inspectedSpaceList != null && !inspectedSpaceList.isEmpty()){
            for(OccInspectedSpace ois: inspectedSpaceList){
                for(OccInspectedSpaceElement oise: ois.getElementListFail()){
                    if(respectMigrationFlag){
                        if(oise.isMigrateToCaseOnFail()){
                            eceList.add(oise);
                        }
                    } else {
                        eceList.add(oise);
                    }
                }
            }
        } 
        return eceList;
    }
    
    /**
     * Adaptor method for UI that just wants failed items
     * @return 
     */
    public List<OccInspectedSpaceElement> getFailedItems(){
        try {
            return extractFailedItemsForCECaseMigration(false);
        } catch (BObStatusException ex) {
            System.out.println(ex);
            return new ArrayList<>();
        }
    }

    /**
     * Asks each inspected space for its unique location descriptors and
     * builds a nice list
     * @return a list, perhaps containing one or more unique location descriptors
     */
    public List<OccLocationDescriptor> getAllUniqueLocationDescriptors() {
        Set<OccLocationDescriptor> locationDescriptors = new HashSet();

        for (OccInspectedSpace inspectedSpace : inspectedSpaceList) {
            locationDescriptors.add(inspectedSpace.getLocation());
            locationDescriptors.addAll(inspectedSpace.getAllUniqueLocationDescriptors());
        }

        List<OccLocationDescriptor> locationDescriptorList = new ArrayList();
        locationDescriptorList.addAll(locationDescriptors);
        return locationDescriptorList;
    }

    /**
     * @return the size of the inspectedSpaceList
     */
    public int getInspectedSpaceListSize() {
        int size = 0;
        if (inspectedSpaceList != null)
            size = inspectedSpaceList.size();

        return size;
    }
    
    
    public double getTotalInspectedSqFt(){
        if(inspectedSpaceList != null && !inspectedSpaceList.isEmpty()){
            return inspectedSpaceList.stream().mapToDouble(space -> space.getSpaceTotalSqFt()).sum();
        } else {
            return 0.0;
        }
    }
    
    public double getTotalOccupancyCount(){
        if(inspectedSpaceList != null && !inspectedSpaceList.isEmpty()){
            return inspectedSpaceList.stream().mapToDouble(space -> space.getMaxOccPersons()).sum();
        } else {
            return 0.0;
        }
        
    }
    
    public double getTotalSleepingPersonMax(){
        if(inspectedSpaceList != null && !inspectedSpaceList.isEmpty()){
            return inspectedSpaceList.stream().mapToDouble(space -> space.getMaxSleepingPersons()).sum();
        } else {
            return 0.0;
        }
        
    }
    
    public double getTotalEgressCount(){
        if(inspectedSpaceList != null && !inspectedSpaceList.isEmpty()){
            return inspectedSpaceList.stream().mapToDouble(space -> space.getEgressCount()).sum();
        } else {
            return 0.0;
        }
        
    }
    
    public int getTotalViolatedOrdCount(){
        if(inspectedSpaceList != null && !inspectedSpaceList.isEmpty()){
            return inspectedSpaceList.stream().mapToInt(space -> space.getElementListFail().size()).sum();
        } else {
            return 0;
        }
        
    }

    // ********************************************************
    // **************** GETTERS AND SETTERS *******************
    // ********************************************************
    
    
    public List<OccInspectedSpace> getInspectedSpaceList() {
        return inspectedSpaceList;
    }

    /**
     * @return the checklistTemplate
     */
    public OccChecklistTemplate getChecklistTemplate() {
        return checklistTemplate;
    }

    /**
     * @param checklistTemplate the checklistTemplate to set
     */
    public void setChecklistTemplate(OccChecklistTemplate checklistTemplate) {
        this.checklistTemplate = checklistTemplate;
    }


    /**
     * @param inspectedSpaceList the inspectedSpaceList to set
     */
    public void setInspectedSpaceList(List<OccInspectedSpace> inspectedSpaceList) {
        this.inspectedSpaceList = inspectedSpaceList;
    }

    // These are generated by intellij--sorry for the format but it does what you expect

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FieldInspection that = (FieldInspection) o;
        return inspectionID == that.inspectionID;
    }

    @Override
    public int hashCode() {
        return Objects.hash(inspectionID, followUpToInspectionID, inspector, createdBy, createdTS, lastUpdatedBy, lastUpdatedTS, occPeriodID, checklistTemplate, inspectedSpaceList, inspectedSpaceListVisible, viewSetting, includeEmptySpaces, inspectionDate,  determination, determinationBy, determinationTS);
    }

   

    /**
     * @param inspectedSpaceListVisible the inspectedSpaceListVisible to set
     */
    public void setInspectedSpaceListVisible(List<OccInspectedSpace> inspectedSpaceListVisible) {
        this.inspectedSpaceListVisible = inspectedSpaceListVisible;
    }

   
    /**
     * @return the viewSetting
     */
    public ViewOptionsOccChecklistItemsEnum getViewSetting() {
        return viewSetting;
    }

    /**
     * @param viewSetting the viewSetting to set
     */
    public void setViewSetting(ViewOptionsOccChecklistItemsEnum viewSetting) {

        this.viewSetting = viewSetting;
    }

    /**
     * @return the includeEmtpySpaces
     */
    public boolean isIncludeEmptySpaces() {
        return includeEmptySpaces;
    }

    /**
     * @param includeEmptySpaces the includeEmtpySpaces to set
     */
    public void setIncludeEmptySpaces(boolean includeEmptySpaces) {
        this.includeEmptySpaces = includeEmptySpaces;
    }

    
   
    @Override
    public void setBlobList(List<BlobLight> bl) {
        this.blobList = bl;
    }

    @Override
    public List<BlobLight> getBlobList() {
        return blobList;
    }

    @Override
    public BlobLinkEnum getBlobLinkEnum() {
        return BLOB_LINK_ENUM;
    }

    @Override
    public int getParentObjectID() {
        return inspectionID;
    }

    @Override
    public BlobLinkEnum getBlobUpstreamPoolEnum() {
        if(domainEnum == EventRealm.CODE_ENFORCEMENT){
            return BLOP_UPSPTREAM_POOL_CECASE;
        } else {
            return BLOP_UPSPTREAM_POOL_OCC;
        }
    }

    @Override
    public int getBlobUpstreamPoolEnumPoolFeederID() {
        if(domainEnum == EventRealm.CODE_ENFORCEMENT){
            return cecaseID;
        } else {
            return occPeriodID;
        }
    }

    /**
     * @return the readyForPassedCertification
     */
    public boolean isReadyForPassedCertification() {
        return readyForPassedCertification;
    }

    /**
     * @param readyForPassedCertification the readyForPassedCertification to set
     */
    public void setReadyForPassedCertification(boolean readyForPassedCertification) {
        this.readyForPassedCertification = readyForPassedCertification;
    }

    @Override
    public void setLinkedEvents(List<EventCnF> evList) {
        linkedEvents = evList;
    }

    @Override
    public List<EventCnF> getLinkedEvents() {
        return linkedEvents;
    }

    @Override
    public EventLinkEnum getEventLinkEnum() {
        return EventLinkEnum.FIELD_INSPECTION;
    }

    @Override
    public int getLinkTargetPrimaryKey() {
        return inspectionID;
    }
  
}
