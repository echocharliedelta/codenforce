/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.application.interfaces.IFace_Loggable;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import com.tcvcog.tcvce.application.interfaces.IFaceActivatableBOB;

/**
 * Foundational entity for the system: Property
 That is an extension of a parcel with mailing mailingAddressLinkList and units
 * 
 * @author Ellen Bascomb
 */

public class        Property 
        extends     Parcel 
        implements  IFace_Loggable,
                    IFace_addressListHolder,
                    IFaceActivatableBOB,
                    IFace_PermissionsMuniFenced,
                    IFace_PermissionsMuniProfileGoverned,
                    Comparable<Property>{
    
    final static LinkedObjectSchemaEnum PROPERTY_ADDRESS_LOSE = LinkedObjectSchemaEnum.ParcelMailingaddress;
    
    protected List<PropertyUnit> unitList;
    protected List<MailingAddressLink> mailingAddressLinkList;

    /**
     * Creates a new instance of Property
     * @param par
     */

    public Property(Parcel par) {
      super(par);
    }
    
    public Property(Property prop){
        super(prop);
        this.unitList = prop.unitList;
        this.mailingAddressLinkList = prop.mailingAddressLinkList;
    }

    
    @Override
    public Municipality getGoverningMunicipality() {
        return muni;
    }

 
  
    
 
    /**
     * Extracts the address from this property's address list
     * with index 0 or null if none is in the list
     * @return 
     */
    public MailingAddressLink getAddress(){
        if(mailingAddressLinkList != null && !mailingAddressLinkList.isEmpty()){
            return mailingAddressLinkList.get(0);
        } else {
            return null;
        }
        
    }
    
    /**
     * Utility method for extracting the first linked mailing address in a given
     * list of mailing address links
     * @return 
     */
    public MailingAddressLink getPrimaryAddressLink(){
        if(mailingAddressLinkList != null && !mailingAddressLinkList.isEmpty()){
            return mailingAddressLinkList.get(0);
            
        }
        return null;
    }
    
    /**
     * Utility for returning address links that aren't the 
     * primary link for use in the address search row expansion
     * @return 
     */
    public List<MailingAddressLink> getSecondaryAddressLinks(){
         if(mailingAddressLinkList != null 
                 && !mailingAddressLinkList.isEmpty()
                 && mailingAddressLinkList.size() > 1){
            return mailingAddressLinkList.subList(1, mailingAddressLinkList.size());
        } else {
             return new ArrayList<>();
         }
        
    }
    
    /**
     * Convenience method for turning on labels for additional addresses
     * @return 
     */
    public boolean isPropertyContainsSecondaryAddressLinks(){
        return mailingAddressLinkList != null && mailingAddressLinkList.size() > 1;
    }
    
    /**
     * Convenience method for reverse comptability with property IDs
     * @return the internal parcelKEY
     * 
     */
    public int getPropertyID(){
        return parcelKey;
    }



    /**
     * @return the unitList
     */
    public List<PropertyUnit> getUnitList() {
        return unitList;
    }

    /**
     * @param unitList the unitList to set
     */
    public void setUnitList(List<PropertyUnit> unitList) {
        this.unitList = unitList;
    }

  

    /**
     * @return the mailingAddressLinkList
     */
    @Override
    public List<MailingAddressLink> getMailingAddressLinkList() {
        return mailingAddressLinkList;
    }

    /**
     * @param addresses the mailingAddressLinkList to set
     */
    @Override
    public void setMailingAddressLinkList(List<MailingAddressLink> addresses) {
        this.mailingAddressLinkList = addresses;
    }

    @Override
    public LinkedObjectSchemaEnum getLinkedObjectSchemaEnum() {
           return PROPERTY_ADDRESS_LOSE;
    }

    @Override
    public int getTargetObjectPK() {
        return parcelKey;
    }

    /**
     * Note: this class has a natural ordering that is inconsistent with equals.
     * @param prop
     * @return 
     */
    @Override
    public int compareTo(Property prop) {
        int compAddr = 0;
        MailingAddressLink thisMad;
        MailingAddressLink inMad;
        if(Objects.nonNull(this.mailingAddressLinkList) 
                && !this.mailingAddressLinkList.isEmpty() 
                && Objects.nonNull(prop.mailingAddressLinkList) 
                && !prop.mailingAddressLinkList.isEmpty()){
            // pull out our addresses
            // we're expecting the list's head to be the correct address based on 
            // that ordering logic
            thisMad = this.mailingAddressLinkList.get(0);
            inMad = prop.getMailingAddressLinkList().get(0);
        // we don't have mailing addresses for both properties
        // so we cannot order them
        } else {
            return 0;
        }
        
        // If we have normal integer building numbers, then compare with ints
        Integer thisNo;
        Integer incomingNo;
        try {
            thisNo = Integer.parseInt(thisMad.getBuildingNo());
            incomingNo = Integer.parseInt(inMad.getBuildingNo());
            if(thisNo < incomingNo){
                compAddr = -1;
            } else if(thisNo > incomingNo){
                compAddr = 1;
            }
            // we're done if we can compare on int building numbers and they are 
            // not the same integer
            if(compAddr != 0){
                return compAddr;
            }
            // we have the same integer building number, so compare in street name by alpha
            if(Objects.nonNull(thisMad.getStreet()) 
                    && Objects.nonNull(inMad.getStreet())
                    && Objects.nonNull(thisMad.getStreet().getName())
                    && Objects.nonNull(inMad.getStreet().getName())){
                return thisMad.getStreet().getName().compareTo(inMad.getStreet().getName());
            } else {
                // we don't have street name for comparison, so send back 0
                return compAddr;
            }
        // we don't have normal ints for building numbers
        } catch(NumberFormatException ex){
            // do string comparison for building no instead
            return thisMad.getBuildingNo().compareTo(inMad.getBuildingNo());
        }
    }

    @Override
    public String getParentHumanFriendlyDescriptiveString() {
        StringBuilder sb = new StringBuilder("Property at ");
        if(this.getAddress() != null){
            sb.append(this.getAddress().getAddressPretty1Line());
        }
        sb.append(" | Parcel ID: ");
        sb.append(this.getCountyParcelID());
        return sb.toString();
        
    }
    
    
  
}
