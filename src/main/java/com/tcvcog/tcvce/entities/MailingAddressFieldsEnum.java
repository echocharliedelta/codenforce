package com.tcvcog.tcvce.entities;

public enum MailingAddressFieldsEnum {
    BUILDINGNO("BuildingNo", "Building No"),
    USEBUILDINGNO("UseBuildingNo", "Use Building Number"),
    POBOX("PoBox", "PO Box"),
    USEPOBOX("UsePOBox", "Use PO Box"),
    SECONDARYUNITIDENTIFIER("SecondaryUnitIdentifier", "Unit"),
    USESECONDARYUNITIDENTIFIER("UseSecondaryUnitIdentifier", "Use Secondary Unit Identifier"),
    ATTN("Attn", "Attention"),
    USEATTENTION("UseAttention", "Use Attention"),
    SOURCE("Source", "Source"),
    VERIFIEDTS("VerifiedTS", "Verified"),
    VERIFIEDSOURCE("VerifiedSource", "Verification Source");

    private final String fieldName;
    private final String friendlyName;

    private MailingAddressFieldsEnum(String fieldName, String friendlyName) {
        this.fieldName = fieldName;
        this.friendlyName = friendlyName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

}
