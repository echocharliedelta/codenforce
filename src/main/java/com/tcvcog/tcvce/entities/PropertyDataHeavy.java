/*
 * Copyright (C) 2018 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;


import com.tcvcog.tcvce.entities.occupancy.FieldInspectionLight;
import com.tcvcog.tcvce.entities.occupancy.OccPeriodDataHeavy;
import com.tcvcog.tcvce.entities.occupancy.OccPeriodStatusEnum;
import com.tcvcog.tcvce.util.viewoptions.ViewOptionsActiveHiddenListsEnum;
import com.tcvcog.tcvce.util.viewoptions.ViewOptionsProposalsEnum;
import java.util.ArrayList;
import java.util.List;

/**
 *  The object rich subclass of Property including all the property's 
 * cases, person links, and listified units
 * @author ellen bascomb of apartment 31Y
 */
public  class       PropertyDataHeavy 
        extends     Property 
        implements  IFace_humanListHolder,
                    IFace_BlobHolder,
                    IFace_EventHolder,
                    IFaceSessionSyncTarget{
    
    final static LinkedObjectSchemaEnum HUMAN_LINK_SCHEMA_ENUIM = LinkedObjectSchemaEnum.ParcelHuman;
    final static BlobLinkEnum PROPDH_LINK_ENUM = BlobLinkEnum.PROPERTY;
    
    /**
     * A property is the highest level blob holder, so it has no upstream pool
     */
    final static BlobLinkEnum PROPDH_UPSTRREAM_BLOB_POOL = null;
    
    protected OccPeriodStatusEnum maxStatusOccupancyPeriods;
    protected PriorityEnum maxCECasePriority;
    
    protected List<HumanLink> humanLinkList;
    protected HumanLink ownerLink;
    protected HumanLink managerLink;
    protected List<Person> personList;
    
    private List<CECaseDataHeavy> ceCaseList;
    private List<CEActionRequest> cearList;
    private List<PropertyUnitDataHeavy> unitWithListsList;
    private List<BlobLight> blobList;
    
    private List<FieldInspectionLight> fieldInspectionListCombined;
    
    private BlobLight broadviewPhoto;
    
    // Events and alerts
    private List<EventCnF> eventList;
    private PropertyAlertContainer propertyAlerts;
    
    private List<EventCnF> eventListCumulative;
    
    // deac facility
    private boolean allowDeactivation;
    private String deactivationAuditLog;
    
    
    
    public PropertyDataHeavy(Property prop){
        super(prop);
    }
    
    /**
     * Theoretically we store the credential signature of the user who
     * retrieves any data heavy bob, but for now, this is in holding
     * @param prop
     * @param cred 
     */
    public PropertyDataHeavy(Property prop, Credential cred){
        super(prop);
        
         
        this.mailingAddressLinkList = prop.getMailingAddressLinkList();
        
    }
    
    public List<OccPeriodDataHeavy> getCompletePeriodList() {
        List<OccPeriodDataHeavy> perList = new ArrayList<>();

        if (unitWithListsList != null && !unitWithListsList.isEmpty()) {
            for (PropertyUnitDataHeavy pudh: unitWithListsList) {
                if (pudh.getPeriodList() != null && !pudh.getPeriodList().isEmpty()) {
                    perList.addAll(pudh.getPeriodList());
                }
            }
        }
        
        return perList;
    }
    
    
    @Override
    public void accept(IFaceSessionSyncVisitor visitor) {
        visitor.visit(this);
    }
    
    /**
     * Extracts proposals from all PropertyInfo cases attached to property
     * @param vope
     * @return 
     */
    public List assembleProposalList(ViewOptionsProposalsEnum vope) {
        List<Proposal> proposalList = new ArrayList<>();
        List<Proposal> proposalListVisible = new ArrayList<>();
        
        if (!proposalList.isEmpty()) {
            for (Proposal p : proposalList) {
                switch (vope) {
                    case VIEW_ALL:
                        proposalListVisible.add(p);
                        break;
                    case VIEW_ACTIVE_HIDDEN:
                        if (p.isActive() && p.isHidden()) {
                            proposalListVisible.add(p);
                        }
                        break;
                    case VIEW_ACTIVE_NOTHIDDEN:
                        if (p.isActive() && !p.isHidden() && !p.getDirective().isRefuseToBeHidden()) {
                            proposalListVisible.add(p);
                        }
                        break;
                    case VIEW_EVALUATED:
                        if (p.getResponseTS() != null) {
                            proposalListVisible.add(p);
                        }
                        break;
                    case VIEW_INACTIVE:
                        if (!p.isActive()) {
                            proposalListVisible.add(p);
                        }
                        break;
                    case VIEW_NOT_EVALUATED:
                        if (p.getResponseTS() == null) {
                            proposalListVisible.add(p);
                        }
                        break;
                    default:
                        proposalListVisible.add(p);
                } // switch
            } // for
        } // if
        return proposalListVisible;
    }
    
    /**
     * Organizes all available events on all sub objects
     * @return 
     */
    public List<EventCnF> extractCumulativeEventList(){
        if(eventListCumulative == null){
            eventListCumulative = new ArrayList<>();
        }
        if(ceCaseList != null && !ceCaseList.isEmpty()){
            for(CECase cse: ceCaseList){
                eventListCumulative.addAll(cse.getEventList());
            }
        }
        for(OccPeriodDataHeavy opdh: getCompletePeriodList()){
            eventListCumulative.addAll(opdh.getEventList());
        }
        eventListCumulative.addAll(eventList);
        return eventListCumulative;
    }
    
    
    /**
     * Gets the cumulative event list; must be generated with a 
 manual call to extractCumulativeEventList
     * 
     * @return the eventListCumulative
     */
    public List<EventCnF> getEventListCumulative() {
        
        return eventListCumulative;
    }
    
    /**
     * Writes null the cumulative event list
     */
    public void resetEventListCumulative(){
        eventListCumulative = null;
    }

    
    
    /**
     * @return the ceCaseList
     */
    public List<CECaseDataHeavy> getCeCaseList() {
        return ceCaseList;
    }



    /**
     * @param ceCaseList the ceCaseList to set
     */
    public void setCeCaseList(List<CECaseDataHeavy> ceCaseList) {
        this.ceCaseList = ceCaseList;
    }


    /**
     * @return the blobList
     */
    @Override
    public List<BlobLight> getBlobList() {
        return blobList;
    }

    /**
     * @param blobList the blobList to set
     */
    @Override
    public void setBlobList(List<BlobLight> blobList) {
        this.blobList = blobList;
    }

    /**
     * @return the unitWithListsList
     */
    public List<PropertyUnitDataHeavy> getUnitWithListsList() {
        return unitWithListsList;
    }

    /**
     * @param unitWithListsList the unitWithListsList to set
     */
    public void setUnitWithListsList(List<PropertyUnitDataHeavy> unitWithListsList) {
        this.unitWithListsList = unitWithListsList;
    }

   @Override
    public List<HumanLink> gethumanLinkList() {
        return humanLinkList;
    }

    @Override
    public void sethumanLinkList(List<HumanLink> hll) {
        humanLinkList = hll;
    }

    @Override
    public LinkedObjectSchemaEnum getHUMAN_LINK_SCHEMA_ENUM() {
        return HUMAN_LINK_SCHEMA_ENUIM;
    }

    
    @Override
    public int getHostPK() {
        return parcelKey;
    }

    @Override
    public BlobLinkEnum getBlobLinkEnum() {
        return PROPDH_LINK_ENUM;
    }

    @Override
    public int getParentObjectID() {
        return parcelKey;
    }

    @Override
    public BlobLinkEnum getBlobUpstreamPoolEnum() {
        return PROPDH_UPSTRREAM_BLOB_POOL;
    }

    @Override
    public int getBlobUpstreamPoolEnumPoolFeederID() {
        return 0;
    }

    /**
     * @return the broadviewPhoto
     */
    public BlobLight getBroadviewPhoto() {
        return broadviewPhoto;
    }

    /**
     * @param broadviewPhoto the broadviewPhoto to set
     */
    public void setBroadviewPhoto(BlobLight broadviewPhoto) {
        this.broadviewPhoto = broadviewPhoto;
    }

    /**
     * @return the cearList
     */
    public List<CEActionRequest> getCearList() {
        return cearList;
    }

    /**
     * @param cearList the cearList to set
     */
    public void setCearList(List<CEActionRequest> cearList) {
        this.cearList = cearList;
    }

    @Override
    public String getDescriptionString() {
        StringBuilder sb = new StringBuilder();
        if(this.getAddress() != null){
            sb.append(this.getAddress().addressPretty1Line);
        }
        return sb.toString();
    }

    @Override
    public LinkedObjectSchemaEnum getUpstreamHumanLinkPoolEnum() {
        return null;
    }

    @Override
    public int getUpstreamHumanLinkPoolFeederID() {
        return 0;
    }

    /**
     * @return the fieldInspectionListCombined
     */
    public List<FieldInspectionLight> getFieldInspectionListCombined() {
        return fieldInspectionListCombined;
    }

    /**
     * @param fieldInspectionListCombined the fieldInspectionListCombined to set
     */
    public void setFieldInspectionListCombined(List<FieldInspectionLight> fieldInspectionListCombined) {
        this.fieldInspectionListCombined = fieldInspectionListCombined;
    }

    /**
     * @return the ownerLink
     */
    public HumanLink getOwnerLink() {
        return ownerLink;
    }

    /**
     * @param ownerLink the ownerLink to set
     */
    public void setOwnerLink(HumanLink ownerLink) {
        this.ownerLink = ownerLink;
    }

    /**
     * @return the managerLink
     */
    public HumanLink getManagerLink() {
        return managerLink;
    }

    /**
     * @param managerLink the managerLink to set
     */
    public void setManagerLink(HumanLink managerLink) {
        this.managerLink = managerLink;
    }

    /**
     

    /**
     * @return the maxStatusOccupancyPeriods
     */
    public OccPeriodStatusEnum getMaxStatusOccupancyPeriods() {
        return maxStatusOccupancyPeriods;
    }

    /**
     * @param maxStatusOccupancyPeriods the maxStatusOccupancyPeriods to set
     */
    public void setMaxStatusOccupancyPeriods(OccPeriodStatusEnum maxStatusOccupancyPeriods) {
        this.maxStatusOccupancyPeriods = maxStatusOccupancyPeriods;
    }

    /**
     * @return the maxCECasePriority
     */
    public PriorityEnum getMaxCECasePriority() {
        return maxCECasePriority;
    }

    /**
     * @param maxCECasePriority the maxCECasePriority to set
     */
    public void setMaxCECasePriority(PriorityEnum maxCECasePriority) {
        this.maxCECasePriority = maxCECasePriority;
    }

     @Override
    public EventRealm getEventDomain() {
        return PARCEL_DOMAIN;
    }

    @Override
    public void setEventList(List<EventCnF> evList) {
        eventList = evList;
    }

    @Override
    public List<EventCnF> getEventList(ViewOptionsActiveHiddenListsEnum evViewOpt) {
        return eventList;
    }

    @Override
    public List<EventCnF> getEventList() {
        return eventList;
    }

    

    @Override
    public int getBObID() {
        return parcelKey;
    }
    
     /**
     * Parcels don't have a manager, but cecases and occ periods do, 
     * and their managers need the ability to edit events and blobs on 
     * their overseen cases, so this is a hacky way of reusing that code
     * through the interface hierarchy of creator rigths preserved and
     * manager overseen.
     * @return 
     */
    @Override
    public User getManagerOverseer() {
        return null;
    }

    @Override
    public int getCreatorUserID() {
        return createdByUserID;
    }

    /**
     * @param eventListCumulative the eventListCumulative to set
     */
    public void setEventListCumulative(List<EventCnF> eventListCumulative) {
        this.eventListCumulative = eventListCumulative;
    }

    /**
     * @return the propertyAlerts
     */
    public PropertyAlertContainer getPropertyAlerts() {
        return propertyAlerts;
    }

    /**
     * @param propertyAlerts the propertyAlerts to set
     */
    public void setPropertyAlerts(PropertyAlertContainer propertyAlerts) {
        this.propertyAlerts = propertyAlerts;
    }

    /**
     * @return the deactivationAuditLog
     */
    public String getDeactivationAuditLog() {
        return deactivationAuditLog;
    }

    /**
     * @param deactivationAuditLog the deactivationAuditLog to set
     */
    public void setDeactivationAuditLog(String deactivationAuditLog) {
        this.deactivationAuditLog = deactivationAuditLog;
    }

    /**
     * @return the allowDeactivation
     */
    public boolean isAllowDeactivation() {
        return allowDeactivation;
    }

    /**
     * @param allowDeactivation the allowDeactivation to set
     */
    public void setAllowDeactivation(boolean allowDeactivation) {
        this.allowDeactivation = allowDeactivation;
    }


}
