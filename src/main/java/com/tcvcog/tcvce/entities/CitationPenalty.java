/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Embodies a penalty imposed on a violation or at the citation level
 * @author pierre15
 */
public class CitationPenalty extends TrackedEntity{
    static String PENALTY_PK = "penaltyid";
    static String PENALTY_TABLE = "citationpenalty";
    
    private int penaltyID;
    private CitationPenaltyTypeEnum penaltyType;
    private BigDecimal fineAmount;
    private int jailDays;
    private String notes;
    private LocalDate dateOfRecord;
    private int citationID;
    private int citationViolationID;

    @Override
    public String getPKFieldName() {
        return PENALTY_PK;
    }

    @Override
    public int getDBKey() {
        return penaltyID;
    }

    @Override
    public String getDBTableName() {
        return PENALTY_TABLE;
    }

    /**
     * @return the PENALTY_PK
     */
    public static String getPENALTY_PK() {
        return PENALTY_PK;
    }

    /**
     * @param aPENALTY_PK the PENALTY_PK to set
     */
    public static void setPENALTY_PK(String aPENALTY_PK) {
        PENALTY_PK = aPENALTY_PK;
    }

    /**
     * @return the PENALTY_TABLE
     */
    public static String getPENALTY_TABLE() {
        return PENALTY_TABLE;
    }

    /**
     * @param aPENALTY_TABLE the PENALTY_TABLE to set
     */
    public static void setPENALTY_TABLE(String aPENALTY_TABLE) {
        PENALTY_TABLE = aPENALTY_TABLE;
    }

    /**
     * @return the penaltyID
     */
    public int getPenaltyID() {
        return penaltyID;
    }

    /**
     * @param penaltyID the penaltyID to set
     */
    public void setPenaltyID(int penaltyID) {
        this.penaltyID = penaltyID;
    }

    /**
     * @return the penaltyType
     */
    public CitationPenaltyTypeEnum getPenaltyType() {
        return penaltyType;
    }

    /**
     * @param penaltyType the penaltyType to set
     */
    public void setPenaltyType(CitationPenaltyTypeEnum penaltyType) {
        this.penaltyType = penaltyType;
    }

    /**
     * @return the fineAmount
     */
    public BigDecimal getFineAmount() {
        return fineAmount;
    }

    /**
     * @param fineAmount the fineAmount to set
     */
    public void setFineAmount(BigDecimal fineAmount) {
        this.fineAmount = fineAmount;
    }

    /**
     * @return the jailDays
     */
    public int getJailDays() {
        return jailDays;
    }

    /**
     * @param jailDays the jailDays to set
     */
    public void setJailDays(int jailDays) {
        this.jailDays = jailDays;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return the dateOfRecord
     */
    public LocalDate getDateOfRecord() {
        return dateOfRecord;
    }

    /**
     * @param dateOfRecord the dateOfRecord to set
     */
    public void setDateOfRecord(LocalDate dateOfRecord) {
        this.dateOfRecord = dateOfRecord;
    }

    /**
     * @return the citationID
     */
    public int getCitationID() {
        return citationID;
    }

    /**
     * @param citationID the citationID to set
     */
    public void setCitationID(int citationID) {
        this.citationID = citationID;
    }

    /**
     * @return the citationViolationID
     */
    public int getCitationViolationID() {
        return citationViolationID;
    }

    /**
     * @param citationViolationID the citationViolationID to set
     */
    public void setCitationViolationID(int citationViolationID) {
        this.citationViolationID = citationViolationID;
    }
    
    
    
}
