/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Represents possible penalty types
 * @author pierre15
 */
public enum CitationPenaltyTypeEnum {
    
    COURTFEECOST("Court fee / cost"),
    FINEOFFICERREQUESTED("Fine requested by officer"),
    FINEDISTRICTCOURTIMPOSED("Fine imposed by district court"),
    FINEAPPEALIMPOSED("Fine imposed on appeal"),
    FINEJAILDAYS("Jail days imposed"),
    OTHER("Other");
    
    private CitationPenaltyTypeEnum(String l){
        label = l;
    }
    
    protected final String label;

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }
    
}
