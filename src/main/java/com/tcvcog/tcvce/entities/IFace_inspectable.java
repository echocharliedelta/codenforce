/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.application.interfaces.IFaceActivatableBOB;
import com.tcvcog.tcvce.entities.occupancy.FieldInspectionLight;
import java.util.List;

/**
 * Declares an object as a holder of occupancy inspections
 * as of March 2022, this includes Occupancy Periods and CE Cases
 * @author sylvia
 */
public interface IFace_inspectable extends IFaceActivatableBOB {
    
    /**
     * We expect implementing classes to be able to describe themselves in a pretty way
     * @return shold be a property address then a unit perhaps then a cecase name or permit file type
     */
    public String getProperyCaseFileDescriptiveStringEscapeFalse();
    
    /**
     * Retrieves the master inspection list
     * @return 
     */
    public List<FieldInspectionLight> getInspectionList();
    
    /**
     * Retrieves a subset of the results from getInspectionList() which are
     * finalized.
     * @return 
     */
    public List<FieldInspectionLight> getInspectionListFinalized();
    
    /**
     * Retrieves a subset of getInspectionList() which are not yet finalized, 
     * meaning in process. 
     * @return 
     */
    public List<FieldInspectionLight> getInspectionListInProcess();
    
    /**
     * Injects a given List of FieldInspectionLight objects into the holding member
     * inspectable
     * @param inspectionList 
     */
    public void setInspectionList(List<FieldInspectionLight> inspectionList);
    
    /**
     * For CE cases this is a property ID, for an occ period host this is an occ period ID
     * @return 
     */
    public int getHostPK();
    
    /**
     * Implementing class returns an instance of this system domain enum 
     * which is not just used in the Event world
     * @return 
     */
    public EventRealm getDomainEnum();
    
    /**
     * Returns the User who has been assigned (and therefore authorized) to the 
     * implementing class as the key staff person coordinating work on the file or case
     * @return 
     */
    public User getManager();
    
    /**
     * Implementing classes which are considered "closed" should not allow 
     * new inspections. This will disable inspection creation facilities.
     * @return true if the implementing class should allow new inspections, i.e. 
     * they are in an open state.
     */
    public boolean isNewInspectionsAllowed();
    
    
}
