/*
 * Copyright (C) 2025 echocdelta
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Implemented by SessionConductors who need to hear about
 * new session focus objects to align their members accordingly.
 * 
 * @author echocdelta
 */
public interface IFaceSessionSynchronizer {
    /**
     * Implementing classes must know how to undertake member sync, 
     * probably by asking what type the target is and asking it about
     * its inner guts
     * @param target 
     */
    public void synchronizeToSessionFocusObject(IFaceSessionSyncTarget target);
    
}
