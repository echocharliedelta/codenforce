/*
 * Copyright (C) 2017 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import com.tcvcog.tcvce.entities.occupancy.OccSpaceElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.primefaces.model.TreeNode;

/**
 * Called a Code Book to users, a CodeSet is a collection of EnforceableCodeElement
 objects which are CodeElement objects wrapped in enforcability guidelines
 * 
 * @author ellen bascomb of apt 31y
 */
public  class       CodeSet
        extends     UMAPTrackedEntity
        implements  Serializable, IFace_keyIdentified, IFaceCachable {
    
    private int codeSetID;
    
    /**
     * Refactored in April 2023 to replace a Municipality object during a
     * leak hunting expedition; see also CodeSource
     */
    private int muniCodeFlattened;
    /**
     * Refactored in April 2023 to replace a Municipality object during a
     * leak hunting expedition
     */
    private String muniNameFlattened;
    
    private String codeSetName;
    private String codeSetDescription;
    protected boolean active;
    private List<EnforceableCodeElement> enfCodeElementList;
    private TreeNode<TreeNodeOrdinanceWrapper<EnforceableCodeElement>> treeView;
    
    
    final static String TABLE_CODE_SET = "codeset";
    final static String CODE_SET_PKFIELD = "codesetid";
    final static UMAPTrackedEnum TRACKED_ENUM = UMAPTrackedEnum.CODESET;
    
    @Override
    public String getPKFieldName() {
        return CODE_SET_PKFIELD;
    }

    @Override
    public int getDBKey() {
        return codeSetID;
        
    }

    @Override
    public String getDBTableName() {
        return TABLE_CODE_SET;
    }
    
    @Override
    public UMAPTrackedEnum getUMAPTrackedEntityEnum() {
        return TRACKED_ENUM;
    }

    /**
     * @return the codeSetID
     */
    public int getCodeSetID() {
        return codeSetID;
    }

    /**
     * @param codeSetID the codeSetID to set
     */
    public void setCodeSetID(int codeSetID) {
        this.codeSetID = codeSetID;
    }

     
    /**
     * Iterates over all ordinances and checks for the given element
     * @param ose
     * @return if the given ose is in my belly
     */
    public boolean contains(OccSpaceElement ose){
        if(ose != null && enfCodeElementList != null && !enfCodeElementList.isEmpty()){
            for(EnforceableCodeElement ece: enfCodeElementList){
                if(ece.getElementID() == ose.elementID){
                    return true;
                }
            }
        } 
        return false;
    }
    

    /**
     * @return the codeSetName
     */
    public String getCodeSetName() {
        return codeSetName;
    }

    /**
     * @param codeSetName the codeSetName to set
     */
    public void setCodeSetName(String codeSetName) {
        this.codeSetName = codeSetName;
    }

    /**
     * @return the codeSetDescription
     */
    public String getCodeSetDescription() {
        return codeSetDescription;
    }

    /**
     * @param codeSetDescription the codeSetDescription to set
     */
    public void setCodeSetDescription(String codeSetDescription) {
        this.codeSetDescription = codeSetDescription;
    }

   
  

    /**
     * @return the enfCodeElementList
     */
    public List<EnforceableCodeElement> getEnfCodeElementList() {
        return enfCodeElementList;
    }

    /**
     * @param enfCodeElementList the enfCodeElementList to set
     */
    public void setEnfCodeElementList(List<EnforceableCodeElement> enfCodeElementList) {
        this.enfCodeElementList = enfCodeElementList;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 200 * hash + this.codeSetID;
        hash = 200 * hash + Objects.hashCode(this.codeSetName);
        hash = 200 * hash + Objects.hashCode(this.codeSetDescription);
        
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CodeSet other = (CodeSet) obj;
        if (this.codeSetID != other.codeSetID) {
            return false;
        }
        if (!Objects.equals(this.codeSetName, other.codeSetName)) {
            return false;
        }
        if (!Objects.equals(this.codeSetDescription, other.codeSetDescription)) {
            return false;
        }
        return true;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * @return the muniCodeFlattened
     */
    public int getMuniCodeFlattened() {
        return muniCodeFlattened;
    }

    /**
     * @param muniCodeFlattened the muniCodeFlattened to set
     */
    public void setMuniCodeFlattened(int muniCodeFlattened) {
        this.muniCodeFlattened = muniCodeFlattened;
    }

    /**
     * @return the muniNameFlattened
     */
    public String getMuniNameFlattened() {
        return muniNameFlattened;
    }

    /**
     * @param muniNameFlattened the muniNameFlattened to set
     */
    public void setMuniNameFlattened(String muniNameFlattened) {
        this.muniNameFlattened = muniNameFlattened;
    }

    /**
     * @return the treeView
     */
    public TreeNode<TreeNodeOrdinanceWrapper<EnforceableCodeElement>> getTreeView() {
        return treeView;
    }

    /**
     * @param treeView the treeView to set
     */
    public void setTreeView(TreeNode<TreeNodeOrdinanceWrapper<EnforceableCodeElement>> treeView) {
        this.treeView = treeView;
    }

    @Override
    public int getCacheKey() {
        return codeSetID;
    }

    
}
