/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Enumerates the possible outcomes during user processing of 
 * Code Enforcement Action Requests, CEAR objects
 * @author Ellen Bascomb of Apartment 31Y
 */
public enum CEARProcessingRouteEnum {
    UNPROCESSED         (   "actionRequestInitialStatusCode",
                            "CEAR has not been routed yet",
                            "cear-route-reset-dialog-var",
                            "cear-reset-form"),
    
    ATTACH_TO_NEW_CASE(     "actionRequestNewCaseStatusCode", 
                            "Attach request to a new code enforcement case",
                            "cecase-add-dialog-var",
                            "cecase-add-dialog cecase-add-form"),
    
    ATTACH_TO_EXISTING_CASE("actionRequestExistingCaseStatusCode",
                            "Attach request to an existing code enforcement case",
                            "cear-route-excase-dialog-var",
                            "cear-route-excase-dialog cear-route-excase-form"),
    
    INVALID_REQUEST(        "actionRequestInvalidStatusCode", 
                            "Invalid or incomplete request",
                            "cear-route-invalid-dialog-var",
                            "cear-route-invalid-form"),
    
    NO_VIOLATION_FOUND(     "actionRequestNoViolationStatusCode", 
                            "No violation found",
                            "cear-route-noviolation-dialog-var",
                            "cear-route-noviolation-form");
 
    private final String CEAR_STATUS_STRING_FOR_DB_KEY_LOOKUP;
    private final String TITLE;
    private final String PAGE_DIALOG_ID;
    private final String COMPONENTS_TO_UPDATE;
    
    private CEARProcessingRouteEnum(String k, String t, String dialog, String comp){
        CEAR_STATUS_STRING_FOR_DB_KEY_LOOKUP = k;
        TITLE = t;
        PAGE_DIALOG_ID = dialog;
        COMPONENTS_TO_UPDATE = comp;
    }

    /**
     * @return the CEAR_STATUS_STRING_FOR_DB_KEY_LOOKUP
     */
    public String getCEAR_STATUS_STRING_FOR_DB_KEY_LOOKUP() {
        return CEAR_STATUS_STRING_FOR_DB_KEY_LOOKUP;
    }

    /**
     * @return the TITLE
     */
    public String getTITLE() {
        return TITLE;
    }

    /**
     * @return the PAGE_DIALOG_ID
     */
    public String getPAGE_DIALOG_ID() {
        return PAGE_DIALOG_ID;
    }

    /**
     * @return the COMPONENTS_TO_UPDATE
     */
    public String getCOMPONENTS_TO_UPDATE() {
        return COMPONENTS_TO_UPDATE;
    }
}
