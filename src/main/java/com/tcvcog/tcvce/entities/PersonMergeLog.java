/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.util.Constants;
import java.time.LocalDateTime;

/**
 * Captures the details of a person merge operation: a primary person is chosen
 * and the subordinate record is deactivated and its inner bits are 
 * re-linked to the primary.
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */
public class PersonMergeLog {
    
    private int mergeID;
    private Person personPrimary;
    private Person personSubordinate;
    private User mergingUser;
    private String notes;
    private String mergeLog;
    private StringBuilder logBuilder;
    
    private LocalDateTime mergeTS; 
    private LocalDateTime successTS;
    private LocalDateTime fatalFailureTS;
    
    
    public PersonMergeLog() {
        initLog();
    }
    
    /**
     * Sets our three key fields: primary person, sub, and user
     * @param req 
     */
    public PersonMergeLog(PersonMergeRequest req){
        personPrimary = req.getPersonPrimary();
        personSubordinate = req.getPersonSubordinate();
        mergingUser = req.getRequestingUser();
        initLog();
    }
    
    private void initLog(){
        logBuilder = new StringBuilder();
        
    }
    
    /**
     * Writes a single line to the merge log and then appends an HTML break
     * @param s to append to the log.
     */
    public void appendToMergeLog(String s){
        if(s != null && logBuilder != null){
            writeToSOUT(s);
            logBuilder.append(s);
        }
    }
    
    private void writeToSOUT(String s){
        System.out.println("Merge log: " + s);
    }
    
    /**
     * Utility to slap on the given string with a break at the end
     * @param s 
     * @param withBreak will throw on a {@literal <br>} 
     */
    public void appendToMergeLog(String s, boolean withBreak){
        if(s != null && logBuilder != null){
            writeToSOUT(s);
            logBuilder.append(s);
            if(withBreak){
                logBuilder.append(Constants.FMT_HTML_BREAK);
            }
        }
    }
    
    /**
     * Special getter that calls StringBuilder#toString()
     * @return 
     */
    public String getAssembledMergeLog(){
        if(logBuilder != null){
            return logBuilder.toString();
        } else {
            return "";
        }
    }
    
    /**
     * @return the mergeID
     */
    public int getMergeID() {
        return mergeID;
    }

    /**
     * @param mergeID the mergeID to set
     */
    public void setMergeID(int mergeID) {
        this.mergeID = mergeID;
    }

    /**
     * @return the personPrimary
     */
    public Person getPersonPrimary() {
        return personPrimary;
    }

    /**
     * @param personPrimary the personPrimary to set
     */
    public void setPersonPrimary(Person personPrimary) {
        this.personPrimary = personPrimary;
    }

    /**
     * @return the personSubordinate
     */
    public Person getPersonSubordinate() {
        return personSubordinate;
    }

    /**
     * @param personSubordinate the personSubordinate to set
     */
    public void setPersonSubordinate(Person personSubordinate) {
        this.personSubordinate = personSubordinate;
    }

    /**
     * @return the mergeTS
     */
    public LocalDateTime getMergeTS() {
        return mergeTS;
    }

    /**
     * @param mergeTS the mergeTS to set
     */
    public void setMergeTS(LocalDateTime mergeTS) {
        this.mergeTS = mergeTS;
    }

    /**
     * @return the mergingUser
     */
    public User getMergingUser() {
        return mergingUser;
    }

    /**
     * @param mergingUser the mergingUser to set
     */
    public void setMergingUser(User mergingUser) {
        this.mergingUser = mergingUser;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return the mergeLog
     */
    public String getMergeLog() {
        return mergeLog;
    }

    /**
     * @param mergeLog the mergeLog to set
     */
    public void setMergeLog(String mergeLog) {
        this.mergeLog = mergeLog;
    }

    /**
     * @return the fatalFailureTS
     */
    public LocalDateTime getFatalFailureTS() {
        return fatalFailureTS;
    }

    /**
     * @param fatalFailureTS the fatalFailureTS to set
     */
    public void setFatalFailureTS(LocalDateTime fatalFailureTS) {
        this.fatalFailureTS = fatalFailureTS;
    }

    /**
     * @return the successTS
     */
    public LocalDateTime getSuccessTS() {
        return successTS;
    }

    /**
     * @param successTS the successTS to set
     */
    public void setSuccessTS(LocalDateTime successTS) {
        this.successTS = successTS;
    }
    
}
