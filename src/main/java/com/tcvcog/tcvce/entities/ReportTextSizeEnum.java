package com.tcvcog.tcvce.entities;

public enum ReportTextSizeEnum {
  
    LARGE("Large font", 1.2),
    UNSACLED("Normal / Unscaled", 1.0),
    MEDIUM("Medium", 0.9),
    SMALLER("Smaller", 0.7),
    SMALLEST("Smallest", 0.6),
    TINY("Tiny", 0.5);

    private final String fieldName;
    private final double bodyScaleEm;

    private ReportTextSizeEnum(String fieldName, double em) {
        this.fieldName = fieldName;
        bodyScaleEm = em;
    }

    public String getFieldName() {
        return fieldName;
    }


    /**
     * @return the bodyScaleEm
     */
    public double getBodyScaleEm() {
        return bodyScaleEm;
    }

}
