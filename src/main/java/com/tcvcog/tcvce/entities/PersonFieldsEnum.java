package com.tcvcog.tcvce.entities;

public enum PersonFieldsEnum {
    NAME("Name", "Name"),
    JOBTITLE("JobTitle", "Job Title"),
    MULTIHUMAN("MultiHuman", "Multi Person"),
    BUSINESSENTITY("BusinessEntity", "Business Entity"),
    SOURCE("Source", "Record Source"),
    UNDER18("Under18", "Under18"),
    DOB("Dob", "Date of birth"),
    DECEASEDDATE("DeceasedDate", "Deceased Date");

    private final String fieldName;
    private final String friendlyName;

    private PersonFieldsEnum(String fieldName, String friendlyName) {
        this.fieldName = fieldName;
        this.friendlyName = friendlyName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

}
