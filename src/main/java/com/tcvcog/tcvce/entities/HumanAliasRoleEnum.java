/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Declares the alias role on a HumanAlias
 * @author pierre15
 */
public enum HumanAliasRoleEnum {
    
    CountyOfficialName("Offical County listing"),
    SpellingVariant("Spelling variation"),
    NickName("Nickname"),
    GeneralAlias("General alias"),
    MergedSubordianteName("Subordinate person name during merge");
    
    private final String friendlyName;

    private HumanAliasRoleEnum(String friendlyName) {
        this.friendlyName = friendlyName;
    }


    public String getFriendlyName() {
        return friendlyName;
    }
}
