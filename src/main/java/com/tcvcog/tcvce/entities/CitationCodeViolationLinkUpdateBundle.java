/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Convenience container for a list of citation-violation links to update, 
 * a list of those to ignore, the requested status to update, 
 * and a batch operation log
 * @author pierre15
 */
public class CitationCodeViolationLinkUpdateBundle {
    private CitationViolationStatusEnum batchedStatusRequest;
    
    private List<CitationCodeViolationLinkUpdateStatus> cvLinksToUpdate;
    private List<CitationCodeViolationLinkUpdateStatus> cvLinksToSkip;
    
    private Citation parentCitation;
    
    private String log;
    
    private UserAuthorized requestingUser;
    
    
    /**
     * Sets up our internal lists 
     * @param cit 
     * @param termList terminal list
     * @param tul in process links that are eligible for status updates
     */
    public CitationCodeViolationLinkUpdateBundle(Citation cit, List<CitationCodeViolationLinkUpdateStatus> termList, List<CitationCodeViolationLinkUpdateStatus> tul){
        cvLinksToSkip = termList;
        cvLinksToUpdate = tul;
        
        parentCitation = cit;
    }
    
    /**
     * Injects the given status into all the CV links which are not terminal
     * @param stat 
     */
    public void registerBatchStatus(CitationViolationStatusEnum stat){
        if(cvLinksToUpdate != null && !cvLinksToUpdate.isEmpty()){
            for(CitationCodeViolationLinkUpdateStatus cv: cvLinksToUpdate){
                cv.setUpdatedStatus(stat);
            }
        }
        this.batchedStatusRequest = stat;
    }
    

    /**
     * @return the batchedStatusRequest
     */
    public CitationViolationStatusEnum getBatchedStatusRequest() {
        return batchedStatusRequest;
    }

    /**
     * @param batchedStatusRequest the batchedStatusRequest to set
     */
    public void setBatchedStatusRequest(CitationViolationStatusEnum batchedStatusRequest) {
        this.batchedStatusRequest = batchedStatusRequest;
    }

    /**
     * @return the cvLinksToUpdate
     */
    public List<CitationCodeViolationLinkUpdateStatus> getCvLinksToUpdate() {
        return cvLinksToUpdate;
    }

    /**
     * @param cvLinksToUpdate the cvLinksToUpdate to set
     */
    public void setCvLinksToUpdate(List<CitationCodeViolationLinkUpdateStatus> cvLinksToUpdate) {
        this.cvLinksToUpdate = cvLinksToUpdate;
    }

    /**
     * @return the cvLinksToSkip
     */
    public List<CitationCodeViolationLinkUpdateStatus> getCvLinksToSkip() {
        return cvLinksToSkip;
    }

    /**
     * @param cvLinksToSkip the cvLinksToSkip to set
     */
    public void setCvLinksToSkip(List<CitationCodeViolationLinkUpdateStatus> cvLinksToSkip) {
        this.cvLinksToSkip = cvLinksToSkip;
    }

    /**
     * @return the log
     */
    public String getLog() {
        return log;
    }

    /**
     * @param log the log to set
     */
    public void setLog(String log) {
        this.log = log;
    }

    /**
     * @return the parentCitation
     */
    public Citation getParentCitation() {
        return parentCitation;
    }

    /**
     * @param parentCitation the parentCitation to set
     */
    public void setParentCitation(Citation parentCitation) {
        this.parentCitation = parentCitation;
    }

    /**
     * @return the requestingUser
     */
    public UserAuthorized getRequestingUser() {
        return requestingUser;
    }

    /**
     * @param requestingUser the requestingUser to set
     */
    public void setRequestingUser(UserAuthorized requestingUser) {
        this.requestingUser = requestingUser;
    }
    
    
    
}
