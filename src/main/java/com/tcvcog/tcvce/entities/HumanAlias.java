/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.application.interfaces.IFaceCachable;

/**
 * Represents a variant on a Human's name but not, say, an LLC the human owns. 
 * For that use human-human linking with a role
 * @author pierre15
 */
public class HumanAlias 
        extends TrackedEntity
        implements IFaceCachable{
    
    public static String HUMAN_ALIAS_PK_FIELD = "aliasid";
    public static String HUMAN_ALIAS_TABLE_NAME = "humanalias";
    public static String ALIASID_SEQ_ID = "humanalias_aliasid_seq";
    
    
    private int aliasID;
    private int humanID;
    private String aliasName;
    
    private BOBSource source;
    
    private String notes;
    
    private HumanAliasRoleEnum aliasRoleEnum;

    @Override
    public String getPKFieldName() {
        return HUMAN_ALIAS_PK_FIELD;
    }

    @Override
    public int getDBKey() {
        return getAliasID();
    }

    @Override
    public String getDBTableName() {
        return HUMAN_ALIAS_TABLE_NAME;
    }

    /**
     * @return the humanID
     */
    public int getHumanID() {
        return humanID;
    }

    /**
     * @param humanID the humanID to set
     */
    public void setHumanID(int humanID) {
        this.humanID = humanID;
    }

    /**
     * @return the aliasName
     */
    public String getAliasName() {
        return aliasName;
    }

    /**
     * @param aliasName the aliasName to set
     */
    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    /**
     * @return the aliasID
     */
    public int getAliasID() {
        return aliasID;
    }

    /**
     * @param aliasID the aliasID to set
     */
    public void setAliasID(int aliasID) {
        this.aliasID = aliasID;
    }

    /**
     * @return the aliasRoleEnum
     */
    public HumanAliasRoleEnum getAliasRoleEnum() {
        return aliasRoleEnum;
    }

    /**
     * @param aliasRoleEnum the aliasRoleEnum to set
     */
    public void setAliasRoleEnum(HumanAliasRoleEnum aliasRoleEnum) {
        this.aliasRoleEnum = aliasRoleEnum;
    }

    /**
     * @return the source
     */
    public BOBSource getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(BOBSource source) {
        this.source = source;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public int getCacheKey() {
        return aliasID;
    }
    
    
}
