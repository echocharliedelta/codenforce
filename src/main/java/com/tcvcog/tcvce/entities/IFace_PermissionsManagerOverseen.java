/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Implemented by cecases and occ periods which have a manager who has special
 * permissions associated with objects attached to the parent case or period.
 * 
 * 
 * @author ellen bascomb of apartmen 31Y
 */
public interface IFace_PermissionsManagerOverseen 
        extends IFace_PermissionsCreatorRightsPreserved{
    /**
     * For the implementing classes this will be the case or occ period manager
     * @return 
     */
    public User getManagerOverseer();
    
}
