package com.tcvcog.tcvce.entities;

public enum CodeElementFieldsEnum {
    ORDCHAPTERNO("OrdchapterNo", " Ord Chapter Number"),
    ORDCHAPTERTITLE("OrdchapterTitle", "Ord Chapter Title"),
    ORDSECNUM("OrdSecNum", "Ord Section Number"),
    ORDSECTITLE("OrdSecTitle", "Ord Section Title"),
    ORDSUBSECNUM("OrdSubSecNum", "Ord Sub Section Number"),
    ORDSUBSECTITLE("OrdSubSecTitle", "Ord Sub Section Title"),
    ORDSUBSUBSECNUM("OrdSubSubSecNum", "Ord Sub Sub Section Number"),
    ORDSUBSUBSECTITLE("OrdSubSubSecTitle", "Ord Sub Sub Section Title"),
    ORDTECHNICALTEXT("ordTechnicalText", "Ord Technical Text");

    private final String fieldName;
    private final String friendlyName;

    private CodeElementFieldsEnum(String fieldName, String friendlyName) {
        this.fieldName = fieldName;
        this.friendlyName = friendlyName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

}
