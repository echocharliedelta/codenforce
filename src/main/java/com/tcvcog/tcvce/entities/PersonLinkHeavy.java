/*
 * Copyright (C) 2019 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import java.util.List;

/**
 * The Listified version of a Person. This person knows its own set of object links!
 * 
 * @author sylvia
 */
public  class PersonLinkHeavy 
        extends Person 
        implements IFace_humanListHolder {
    
    private List<HumanLink> incomingHumanHumanLinks;
    private List<HumanLink> humanNonHumanLinkList;
    
    private List<HumanLinkOutoingThisObjectsContainerIsSource> outgoingHumanLinksFromThisHuman;
    
    private List<PersonMergeLog> mergeLogs;
    
    /**
     * Dead on arrival method to follow pattern of other BObs whose previous
     * DataHeavy versions did not require Credentials to Instantiate
     * 
     * @param p to be injected into the superclass members
     * 
     */
     public PersonLinkHeavy(Person p){
        super(p);
    }

    /**
     * @return the humanLinkList
     */
    @Override
    public List<HumanLink> gethumanLinkList() {
        return incomingHumanHumanLinks;
    }

    /**
     * @param humanLinkList the humanLinkList to set
     */
    @Override
    public void sethumanLinkList(List<HumanLink> humanLinkList) {
        this.incomingHumanHumanLinks = humanLinkList;
    }

    /**
     * @return the outgoingHumanLinksFromThisHuman
     */
    public List<HumanLinkOutoingThisObjectsContainerIsSource> getOutgoingHumanLinksFromThisHuman() {
        return outgoingHumanLinksFromThisHuman;
    }

    /**
     * @param outgoingHumanLinksFromThisHuman the outgoingHumanLinksFromThisHuman to set
     */
    public void setOutgoingHumanLinksFromThisHuman(List<HumanLinkOutoingThisObjectsContainerIsSource> outgoingHumanLinksFromThisHuman) {
        this.outgoingHumanLinksFromThisHuman = outgoingHumanLinksFromThisHuman;
    }
    
     @Override
    public LinkedObjectSchemaEnum getHUMAN_LINK_SCHEMA_ENUM() {
        return HUMAN_HUMAN_LOSE;
    }

    @Override
    public int getHostPK() {
        return humanID;
    }

    @Override
    public String getDescriptionString() {
        return "Person named " + name + " with ID: " + humanID;
        
    }

    /**
     * Human-human links don't have an upstream
     * @return 
     */
    @Override
    public LinkedObjectSchemaEnum getUpstreamHumanLinkPoolEnum() {
        return null;
    }

    /**
     * Human-human links don't have an upstream
     * @return 
     */
    @Override
    public int getUpstreamHumanLinkPoolFeederID() {
        return 0;
    }

    /**
     * @return the humanNonHumanLinkList
     */
    public List<HumanLink> getHumanNonHumanLinkList() {
        return humanNonHumanLinkList;
    }

    /**
     * @param humanNonHumanLinkList the humanNonHumanLinkList to set
     */
    public void setHumanNonHumanLinkList(List<HumanLink> humanNonHumanLinkList) {
        this.humanNonHumanLinkList = humanNonHumanLinkList;
    }

    /**
     * @return the mergeLogs
     */
    public List<PersonMergeLog> getMergeLogs() {
        return mergeLogs;
    }

    /**
     * @param mergeLogs the mergeLogs to set
     */
    public void setMergeLogs(List<PersonMergeLog> mergeLogs) {
        this.mergeLogs = mergeLogs;
    }
}
