/*  
 * Copyright (C) 2018 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.interfaces.IFace_Loggable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import com.tcvcog.tcvce.util.Constants;
import com.tcvcog.tcvce.util.DateTimeUtil;
import java.time.LocalDate;
import java.time.ZoneOffset;
import com.tcvcog.tcvce.application.interfaces.IFaceActivatableBOB;
import com.tcvcog.tcvce.application.interfaces.IFaceCachable;

/**
 * Behold the base class of the EventCnF family!
 * @author ellen bascomb of apt 31y
 */
public  class       EventCnF 
        extends     TrackedEntity
        implements  Comparable<Object>,
                    IFace_Loggable,
                    IFace_noteHolder,
                    IFaceActivatableBOB,
                    IFace_humanListHolder,
                    IFace_PermissionsCreatorRightsPreserved,
                    IFace_EventLinked,
                    IFaceCachable{
    
    final static LinkedObjectSchemaEnum HUMAN_LINK_SCHEMA_ENUM = LinkedObjectSchemaEnum.EventHuman;
    final static String EVENT_FRIENDLY_NAME = "Event";
    final static String EVENT_TABLE_NAME = "event";
    final static String EVENT_PK_FIELD = "eventid";
    
    protected int eventID;
    protected EventCategory category;
    protected String description;
    
    
    protected List<HumanLink> humanLinkList;
    
    /**
     * Indicates to clients which BOb identifier to use
     * Alternatively, one could have only one field for the ID
     * and use Domain to control what we do with that ID
     * But that seemed to likely lead to more confusion since the DB
     * has two columns, since each is keyed differently
     */
    protected EventRealm domain;
    
    //Events can be attached to one of these three parent objects. If more than
    // one of these three is NON zero then we have a configuration error--a major one!
    protected int ceCaseID;
    protected int occPeriodID;
    protected int parcelKey;
    
    protected List<EventCnF> eventList;
    
    // flattened fields for showing event relationships without
    // cycle risk
    protected String parcelAddressTwoLineEscapeFalse;
    protected String parcelAddressOneLine;
    protected String caseName;
    protected String propertyUnitNumber;
    protected String casePeriodPropertyString;
    
    protected LocalDateTime timeStart;
    protected LocalDateTime timeEnd;
    
    protected boolean active;
    protected String notes;
    
    /**
     * Only for use in JavaLand; no DB col for hiding
     */
    protected boolean hidden;
    protected long duration;
    
    
    public EventCnF(){
        
        
    }
    
    public EventCnF(EventCnF ev){
        
        this.eventID = ev.eventID;
        this.category = ev.category;
        this.description = ev.description;
        
        this.domain = ev.domain;
        this.ceCaseID = ev.ceCaseID;
        this.occPeriodID = ev.occPeriodID;
        
        this.parcelAddressTwoLineEscapeFalse = ev.parcelAddressTwoLineEscapeFalse;
        this.parcelAddressOneLine = ev.parcelAddressOneLine;
        this.casePeriodPropertyString = ev.casePeriodPropertyString;
        
        this.timeStart = ev.timeStart;
        this.timeEnd = ev.timeEnd;
        
        this.active = ev.active;

        this.hidden = ev.hidden;
        this.notes = ev.notes;
        
        this.createdBy = ev.createdBy;
        this.createdTS = ev.createdTS;

        this.lastUpdatedBy = ev.lastUpdatedBy;
        this.lastUpdatedTS = ev.lastUpdatedTS;

        this.deactivatedBy = ev.deactivatedBy;
        this.deactivatedTS = ev.deactivatedTS;
    }
    
    /**
     * Mini logic block for determining if this event requires follow up and
     * as of last instantiation, has not had such follow up.
     * 
     * @return 
     */
    public boolean isFollowUpNeeded(){
       if(category != null && category.isFollowUpEvent()){
            if(eventList == null){
                return true;
            } else {
                if(eventList.isEmpty()){
                    return true;
                }
            }
        } 
       return false;
    }
    
    /**
     * Convenience method for adding to an event's description 
     * without having to manage what's already in the field.
     * Just send me a String and I'll slap it on the end of the
     * existing description.
     * @param textToAppend 
     */
    public void appendToDescription(String textToAppend){
        if(textToAppend != null){
            StringBuilder sb = new StringBuilder();
            if(Objects.nonNull(description) && !description.equals("") && !description.equals(" ")){
                sb.append(description);
                sb.append(Constants.FMT_SEMICOLON);
                sb.append(Constants.FMT_SPACE_LITERAL);
            }
            sb.append(textToAppend);
            description = sb.toString();
        }
    }
    
    /**
     * Takes in a start time and will set the end time based on either the given
     * nonzero duration or the cat's default duration
     * 
     * @param start the event's start time
     * @param durationMins if not 0, i'll override the cat default duration
     */
    public void configureEventTimesFromStartTime(LocalDateTime start, int durationMins){
       if(start != null){
           timeStart = start;
           if(durationMins != 0){
                timeEnd = timeStart.plusMinutes(durationMins);
           } else if(category != null){
                timeEnd = timeStart.plusMinutes(category.getDefaultDurationMins());
           }
           if(timeStart != null && timeEnd != null){
                System.out.println("EventCnF.ConfigureEventTimesFromStartTime | start time: " + timeStart.toString());
                System.out.println("EventCnF.ConfigureEventTimesFromStartTime | New end time: " + timeEnd.toString());
           }
       } 
    }
    
    /**
     * Extracts ONLY the date portion of the event start time for table sorting.
     * @return 
     */
    public LocalDate getEventStartDateOnly(){
        if(timeStart != null){
            return timeStart.toLocalDate();
        }
        else {
            return null;
        }
    }
    
    /**
     * Convenience method for returning the non-zero id in the set of parent
     * key fields: case, period, and property
     * @return the parent key, or -9 for all zero parent keys
     */
    public int getParentID(){
        if(ceCaseID != 0){
            return ceCaseID;
        }
        if(parcelKey != 0){
            return parcelKey;
        }
        if(occPeriodID != 0){
            return occPeriodID;
        }
        return -9;
    }
    
    /**
     * @return the eventID
     */
    public int getEventID() {
        return eventID;
    }

    /**
     * @return the category
     */
    public EventCategory getCategory() {
        return category;
    }

   
    /**
     * Gets the description
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Special: I read for the presence of deac ts
     * @return the active
     */
    @Override
    public boolean isActive() {
        return deactivatedTS == null;
    }

    /**
     * @return the hidden
     */
    public boolean isHidden() {
        return hidden;
    }

    /**
     * @return the notes
     */
    @Override
    public String getNotes() {
        return notes;
    }

    /**
     * @param eventID the eventID to set
     */
    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(EventCategory category) {
        this.category = category;
    }



    /**
     * Do not use me: use appendToDescription(String s) instead
     * so as to not disturb existing content
     * @Deprecated 
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }


   
    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * @param hidden the hidden to set
     */
    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    /**
     * @param notes the notes to set
     */
    @Override
    public void setNotes(String notes) {
        this.notes = notes;
    }

  
    @Override
    public int compareTo(Object e) {
        if(Objects.isNull(e)){
            throw new NullPointerException("Cannot compare null events");
        }
        if(!(e instanceof EventCnF)){
            throw new ClassCastException("Cannot cast incoming object to EventCnF");
        }
        EventCnF inEv = (EventCnF) e;
        int c = 0;
        if(this.timeStart != null && inEv.timeStart != null){
            Long thisEpoch = this.timeStart.toEpochSecond(ZoneOffset.UTC);
            Long incomingEpoch = inEv.getTimeStart().toEpochSecond(ZoneOffset.UTC);
            c = thisEpoch.compareTo(incomingEpoch);
        } else if(this.createdTS != null && inEv.createdTS != null){
            Long thisCTSEpoch = this.createdTS.toEpochSecond(ZoneOffset.UTC);
            Long incomingCTSEpoch = inEv.getCreatedTS().toEpochSecond(ZoneOffset.UTC); 
            c = thisCTSEpoch.compareTo(incomingCTSEpoch);
        } 
        return c;
        
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.eventID;
        hash = 97 * hash + Objects.hashCode(this.category);
        hash = 97 * hash + Objects.hashCode(this.description);
        hash = 97 * hash + (this.active ? 1 : 0);
        hash = 97 * hash + (this.hidden ? 1 : 0);
        hash = 97 * hash + Objects.hashCode(this.notes);
        return hash;
    }

    /**
     * Uses only the event ID for equality checking! Not all fields
     * within the event itself, so this method should not be used to
     * figure out if, say, an event object has had its description changed
     * or not
     * @param obj
     * @return 
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EventCnF other = (EventCnF) obj;
        if (this.eventID != other.eventID) {
            return false;
        }
        return true;
    }

    /**
     * @return the domain
     */
    public EventRealm getDomain() {
        return domain;
    }

    /**
     * @param domain the domain to set
     */
    public void setDomain(EventRealm domain) {
        this.domain = domain;
    }

    /**
     * @return the ceCaseID
     */
    public int getCeCaseID() {
        return ceCaseID;
    }

    /**
     * @return the occPeriodID
     */
    public int getOccPeriodID() {
        return occPeriodID;
    }

    /**
     * @param ceCaseID the ceCaseID to set
     */
    public void setCeCaseID(int ceCaseID) {
        this.ceCaseID = ceCaseID;
    }

    /**
     * @param occPeriodID the occPeriodID to set
     */
    public void setOccPeriodID(int occPeriodID) {
        this.occPeriodID = occPeriodID;
    }

    /**
     * @return the timeStart
     */
    public LocalDateTime getTimeStart() {
        return timeStart;
    }

  

    /**
     * @return the timeEnd
     */
    public LocalDateTime getTimeEnd() {
        
        return timeEnd;
    }

   
    
    public String getTimeStartPretty(){
        return DateTimeUtil.getPrettyDate(timeStart);
        
    }

    public String getTimeEndPretty(){
        return DateTimeUtil.getPrettyDate(timeEnd);
    }
    
    /**
     * @param timeStart the timeStart to set
     */
    public void setTimeStart(LocalDateTime timeStart) {
        this.timeStart = timeStart;
    }

   

    /**
     * @param timeEnd the timeEnd to set
     */
    public void setTimeEnd(LocalDateTime timeEnd) {
        this.timeEnd = timeEnd;
    }

   
    
    @Override
    public List<HumanLink> gethumanLinkList() {
        return humanLinkList;
    }

    @Override
    public void sethumanLinkList(List<HumanLink> hll) {
        humanLinkList = hll;
    }

    @Override
    public LinkedObjectSchemaEnum getHUMAN_LINK_SCHEMA_ENUM() {
        return HUMAN_LINK_SCHEMA_ENUM;
    }



    @Override
    public int getHostPK() {
        return eventID;
    }

    @Override
    public String getNoteHolderFriendlyName() {
        return EVENT_FRIENDLY_NAME;
    }

    @Override
    public String getPKFieldName() {
        return EVENT_PK_FIELD;
    }

    @Override
    public int getDBKey() {
        return this.eventID;
    }

    @Override
    public String getDBTableName() {
        return EVENT_TABLE_NAME;
    }

    /**
     * @return the duration
     */
    public long getDuration() {
       
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(long duration) {
        this.duration = duration;
    }

    /**
     * Returns the parcel key ONLY of events attached directly to a parcel.
     * For events attached to cecases and occ periods this method will return 0;
     * To get their containing parcels, use the special method on EventCoordinator
     * @return the parcelKey
     */
    public int getParcelKey() {
        return parcelKey;
    }

    /**
     * @param parcelKey the parcelKey to set
     */
    public void setParcelKey(int parcelKey) {
        this.parcelKey = parcelKey;
    }

    @Override
    public int getCreatorUserID() {
        return getCreatedByUserID();
    }

    @Override
    public String getDescriptionString() {
        StringBuilder sb = new StringBuilder();
        if(this.getCategory() != null){
            sb.append(this.getCategory().getEventCategoryTitle());
        }
        return sb.toString();
    }

    @Override
    public LinkedObjectSchemaEnum getUpstreamHumanLinkPoolEnum() {
        if(domain != null){
            if(domain == EventRealm.CODE_ENFORCEMENT){
                return LinkedObjectSchemaEnum.CECaseHuman;
            } else if(domain == EventRealm.OCCUPANCY){
                return LinkedObjectSchemaEnum.OccPeriodHuman;
            } else if(domain == EventRealm.PARCEL){
                return LinkedObjectSchemaEnum.ParcelHuman;
            } 
        }
        return null;
    }

    @Override
    public int getUpstreamHumanLinkPoolFeederID() {
        if(domain == EventRealm.CODE_ENFORCEMENT){
                return ceCaseID;
            } else if(domain == EventRealm.OCCUPANCY){
                return occPeriodID;
            } else if(domain == EventRealm.PARCEL){
                return parcelKey;
            } 
        return 0;
    }

    /**
     * @return the propertyUnitNumber
     */
    public String getPropertyUnitNumber() {
        return propertyUnitNumber;
    }

    /**
     * @param propertyUnitNumber the propertyUnitNumber to set
     */
    public void setPropertyUnitNumber(String propertyUnitNumber) {
        this.propertyUnitNumber = propertyUnitNumber;
    }

    /**
     * @return the caseName
     */
    public String getCaseName() {
        return caseName;
    }

    /**
     * @param caseName the caseName to set
     */
    public void setCaseName(String caseName) {
        this.caseName = caseName;
    }

    /**
     * @return the casePeriodPropertyString
     */
    public String getCasePeriodPropertyString() {
        return casePeriodPropertyString;
    }

    /**
     * @param casePeriodPropertyString the casePeriodPropertyString to set
     */
    public void setCasePeriodPropertyString(String casePeriodPropertyString) {
        this.casePeriodPropertyString = casePeriodPropertyString;
    }

    /**
     * @return the parcelAddressTwoLineEscapeFalse
     */
    public String getParcelAddressTwoLineEscapeFalse() {
        return parcelAddressTwoLineEscapeFalse;
    }

    /**
     * @param parcelAddressTwoLineEscapeFalse the parcelAddressTwoLineEscapeFalse to set
     */
    public void setParcelAddressTwoLineEscapeFalse(String parcelAddressTwoLineEscapeFalse) {
        this.parcelAddressTwoLineEscapeFalse = parcelAddressTwoLineEscapeFalse;
    }

    /**
     * @return the parcelAddressOneLine
     */
    public String getParcelAddressOneLine() {
        return parcelAddressOneLine;
    }

    /**
     * @param parcelAddressOneLine the parcelAddressOneLine to set
     */
    public void setParcelAddressOneLine(String parcelAddressOneLine) {
        this.parcelAddressOneLine = parcelAddressOneLine;
    }

    /**
     * @return the eventList
     */
    public List<EventCnF> getEventList() {
        return eventList;
    }

    /**
     * @param eventList the eventList to set
     */
    public void setEventList(List<EventCnF> eventList) {
        this.eventList = eventList;
    }

    @Override
    public void setLinkedEvents(List<EventCnF> evList) {
        eventList = evList;
    }

    @Override
    public List<EventCnF> getLinkedEvents() {
        return eventList;
    }

    @Override
    public EventLinkEnum getEventLinkEnum() {
        return EventLinkEnum.EVENT;
    }

    @Override
    public int getLinkTargetPrimaryKey() {
        return eventID;
    }

    @Override
    public int getCacheKey() {
        return eventID;
    }

    
}