/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.entities;

import java.util.ArrayList;
import java.util.List;
import com.tcvcog.tcvce.application.interfaces.IFace_Loggable;
import com.tcvcog.tcvce.util.viewoptions.ViewOptionsEventRulesEnum;
import com.tcvcog.tcvce.util.viewoptions.ViewOptionsProposalsEnum;
import com.tcvcog.tcvce.entities.occupancy.FieldInspection;
import com.tcvcog.tcvce.entities.occupancy.FieldInspectionLight;
import com.tcvcog.tcvce.util.Constants;
import com.tcvcog.tcvce.application.interfaces.IFaceActivatableBOB;
import com.tcvcog.tcvce.application.interfaces.IFaceEventRuleGoverned;
import java.util.Objects;

/**
 *  Listified CECase object 
 * 
 * @author Ellen Bascomb (Apartment 31Y)
 */
public class CECaseDataHeavy
        extends CECase
        implements  Cloneable,
                    IFaceEventRuleGoverned,
                    IFace_Loggable,
                    IFace_PaymentHolder,
                    IFace_BlobHolder,
                    IFace_humanListHolder,
                    IFace_inspectable,
                    IFace_PermissionsMuniFenced,
                    IFace_officerManaged,
                    IFaceSessionSyncTarget{

    final static LinkedObjectSchemaEnum HUMAN_LINK_POOL = LinkedObjectSchemaEnum.ParcelHuman;
    
    // accessed through methods specified in the interfaces
    final static LinkedObjectSchemaEnum HUMAN_LINK_SCHEMA_ENUM = LinkedObjectSchemaEnum.CECaseHuman;
    final static BlobLinkEnum BLOB_LINK_ENUM = BlobLinkEnum.CE_CASE;
    final static BlobLinkEnum BLOB_LINK_UPSTREAM_POOL = BlobLinkEnum.PROPERTY;
    
    protected List<HumanLink> humanLinkList;
    
    protected List<FieldInspectionLight> inspectionList;
    private List<BlobLight> blobList;
    private List<CEActionRequest> ceActionRequestList;
    
    
    /**
     * Unused member as of NOV 2023
     */
    private List<Proposal> proposalList;
    /**
     * Unused member as of NOV 2023
     */
    private List<EventRuleImplementation> eventRuleList;
    /**
     * Unused member as of NOV 2023
     */
    private List<FeeAssigned> feeList;
    /**
     * Unused member as of NOV 2023
     */
    private List<Payment> paymentList;
    
    public CECaseDataHeavy(CECase cse) {
       super(cse);
    }
    
    public CECaseDataHeavy(CECaseDataHeavy csedh){
        super(csedh);
        this.humanLinkList = csedh.humanLinkList;
        this.inspectionList = csedh.inspectionList;
        this.proposalList = csedh.proposalList;
        this.eventRuleList = csedh.eventRuleList;
        this.ceActionRequestList = csedh.ceActionRequestList;
        this.feeList = csedh.feeList;
        this.paymentList = csedh.paymentList;
        this.blobList = csedh.blobList;
    }

    
    @Override
    public EventRealm discloseEventDomain() {
        return EventRealm.CODE_ENFORCEMENT;
    }
    
    
    @Override
    public void accept(IFaceSessionSyncVisitor visitor) {
        visitor.visit(this);
    }



    /**
     *
     * @return @throws CloneNotSupportedException
     */
    @Override
    public CECaseDataHeavy clone() throws CloneNotSupportedException {
        super.clone();
        return null;
    }
    
    
    @Override
    public String getProperyCaseFileDescriptiveStringEscapeFalse() {
        StringBuilder sb = new StringBuilder();
        sb.append(propertyAddress2LineFlat);
        if(propertyUnitNumberFlat != null){
            sb.append(Constants.FMT_HTML_BREAK);
            sb.append("Unit: ");
            sb.append(propertyUnitNumberFlat);
        }
        sb.append(Constants.FMT_HTML_BREAK);
        sb.append("Case name: ");
        sb.append(caseName);
        return sb.toString();
        
    }

   
    @Override
    public List<EventRuleImplementation> assembleEventRuleList(ViewOptionsEventRulesEnum voere) {
        List<EventRuleImplementation> evRuleList = new ArrayList<>();
        if (eventRuleList != null) {
            for (EventRuleImplementation eri : eventRuleList) {
                switch (voere) {
                    case VIEW_ACTIVE_NOT_PASSED:
                        if (eri.isActiveRuleAbstract()
                                && eri.getPassedRuleTS() == null) {
                            evRuleList.add(eri);
                        }
                        break;
                    case VIEW_ACTIVE_PASSED:
                        if (eri.isActiveRuleAbstract()
                                && eri.getPassedRuleTS() != null) {
                            evRuleList.add(eri);
                        }
                        break;
                    case VIEW_ALL:
                        evRuleList.add(eri);
                        break;
                    case VIEW_INACTIVE:
                        if (!eri.isActiveRuleAbstract()) {
                            evRuleList.add(eri);
                        }
                        break;
                    default:
                        evRuleList.add(eri);
                } // close switch
            } // close loop
        } // close null check
        return evRuleList;
    }

    @Override
    public boolean isAllRulesPassed() {
        boolean allPassed = true;
        for (EventRuleImplementation er : eventRuleList) {
            if (er.getPassedRuleTS() == null) {
                allPassed = false;
                break;
            }
        }
        return allPassed;
    }

    @Override
    public List<Proposal> assembleProposalList(ViewOptionsProposalsEnum vope) {
        List<Proposal> proposalListVisible = new ArrayList<>();
        if (proposalList != null && !proposalList.isEmpty()) {
            for (Proposal p : proposalList) {
                switch (vope) {
                    case VIEW_ALL:
                        proposalListVisible.add(p);
                        break;
                    case VIEW_ACTIVE_HIDDEN:
                        if (p.isActive()
                                && p.isHidden()) {
                            proposalListVisible.add(p);
                        }
                        break;
                    case VIEW_ACTIVE_NOTHIDDEN:
                        if (p.isActive()
                                && !p.isHidden()
                                && !p.getDirective().isRefuseToBeHidden()) {
                            proposalListVisible.add(p);
                        }
                        break;
                    case VIEW_EVALUATED:
                        if (p.getResponseTS() != null) {
                            proposalListVisible.add(p);
                        }
                        break;
                    case VIEW_INACTIVE:
                        if (!p.isActive()) {
                            proposalListVisible.add(p);
                        }
                        break;
                    case VIEW_NOT_EVALUATED:
                        if (p.getResponseTS() == null) {
                            proposalListVisible.add(p);
                        }
                        break;
                    default:
                        proposalListVisible.add(p);
                } // switch
            } // for
        } // if
        return proposalListVisible;
    }

    /**
     * @param eventRuleList the eventRuleList to set
     */
    @Override
    public void setEventRuleList(List<EventRuleImplementation> eventRuleList) {
        this.eventRuleList = eventRuleList;
    }

   

    /**
     * @param ceActionRequestList the ceActionRequestList to set
     */
    public void setCeActionRequestList(List<CEActionRequest> ceActionRequestList) {
        this.ceActionRequestList = ceActionRequestList;
    }

  
    /**
     * @return the proposalList
     */
    public List<Proposal> getProposalList() {
        return proposalList;
    }

    /**
     * @param proposalList the proposalList to set
     */
    @Override
    public void setProposalList(List<Proposal> proposalList) {
        this.proposalList = proposalList;
    }

    /**
     * @return the feeList
     */
    public List<FeeAssigned> getFeeList() {
        return feeList;
    }

    /**
     * @param feeList the feeList to set
     */
    public void setFeeList(List<FeeAssigned> feeList) {
        this.feeList = feeList;
    }

    /**
     * @return the paymentList
     */
    @Override
    public List<Payment> getPaymentList() {
        return paymentList;
    }


    /**
     * @param paymentList the paymentList to set
     */
    @Override
    public void setPaymentList(List<Payment> paymentList) {
        this.paymentList = paymentList;
    }

    /**
     * Takes the general Payment type and converts it to
     *
     * @param paymentList the paymentList to set
     */
    @Override
    public void setPaymentListGeneral(List<Payment> paymentList) {
        List<Payment> skeletonHorde = new ArrayList<>();

        for (Payment p : paymentList) {

            skeletonHorde.add(new Payment(p));

        }

        this.paymentList = skeletonHorde;
    }

  
   

    @Override
    public boolean isOpen() {
        if(statusBundle != null){
            return statusBundle.getPhase().isCaseOpen();
        } 
        return true;
        
    }

    /**
     * @return the ceActionRequestList
     */
    public List<CEActionRequest> getCeActionRequestList() {
        return ceActionRequestList;
    }

    /**
     * @return the blobList
     */
    public List<BlobLight> getBlobList() {
        return blobList;
    }

    /**
     * @param blobList the blobList to set
     */
    public void setBlobList(List<BlobLight> blobList) {
        this.blobList = blobList;
    }
        

    @Override
    public List<HumanLink> gethumanLinkList() {
        return humanLinkList;
    }

    @Override
    public void sethumanLinkList(List<HumanLink> hll) {
        humanLinkList = hll;
    }

    @Override
    public LinkedObjectSchemaEnum getHUMAN_LINK_SCHEMA_ENUM() {
        return HUMAN_LINK_SCHEMA_ENUM;
    }

    

    @Override
    public int getHostPK() {
        return caseID;
    }

    @Override
    public BlobLinkEnum getBlobLinkEnum() {
        return BLOB_LINK_ENUM;
    }

    @Override
    public int getParentObjectID() {
        return caseID;
    }

    @Override
    public BlobLinkEnum getBlobUpstreamPoolEnum() {
        return BLOB_LINK_UPSTREAM_POOL;
    }

    @Override
    public int getBlobUpstreamPoolEnumPoolFeederID() {
        return parcelKey;
    }

    /**
     * @return the inspectionList
     */
    @Override
    public List<FieldInspectionLight> getInspectionList() {
        return inspectionList;
    }

    /**
     * @param inspectionList the inspectionList to set
     */
    @Override
    public void setInspectionList(List<FieldInspectionLight> inspectionList) {
        this.inspectionList = inspectionList;
    }

    @Override
    public EventRealm getDomainEnum() {
        return CECASE_ENUM;
    }

    @Override
    public User getManager() {
        return caseManager;
    }

    @Override
    public boolean isNewInspectionsAllowed() {
        return Objects.isNull(closingDate);
    }

    @Override
    public Municipality getGoverningMunicipality() {
       return muni;
    }

    @Override
    public String getDescriptionString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.caseName);
        return sb.toString();
    }

    @Override
    public LinkedObjectSchemaEnum getUpstreamHumanLinkPoolEnum() {
        return HUMAN_LINK_POOL;
    }

    @Override
    public int getUpstreamHumanLinkPoolFeederID() {
        return parcelKey;
    }


    @Override
    public List<FieldInspectionLight> getInspectionListFinalized() {
        List<FieldInspectionLight> finList = new ArrayList<>();
        if(inspectionList != null && !inspectionList.isEmpty()){
            for(FieldInspectionLight fin: inspectionList){
                if(fin.getDeterminationTS() != null){
                    finList.add(fin);
                }
            }
        }
        return finList;
    }

    @Override
    public List<FieldInspectionLight> getInspectionListInProcess() {
        List<FieldInspectionLight> finList = new ArrayList<>();
        if(inspectionList != null && !inspectionList.isEmpty()){
            for(FieldInspectionLight fin: inspectionList){
                if(fin.getDeterminationTS() == null){
                    finList.add(fin);
                }
            }
        }
        return finList;
    }

    @Override
    public User getOfficerManage() {
        return caseManager;
    }


}
