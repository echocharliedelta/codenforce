/*
 * Copyright (C) 2018 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;


/**
 * Represents the determination of the statuses of all occ periods on a given property
 * @author ellen bascomb of apt 31y
 */
public enum PropertyStatusCodeEnforcement {

    NO_OPEN_CECASES_SOMECLOSED(                     "No open code enforcement cases; one or more closed cases", 
                                                    0, 
                                                    "",
                                                    "occperstatus_unknown"),
    
    NO_CE_CASES(                                    "No open or closed code enforcement cases", 
                                                    0, 
                                                    "",
                                                    "occperstatus_unknown"),
    
    OPEN_CASE_MONITORING(                           "One or more cases in monitoring phase", 
                                                    0, 
                                                    "",
                                                    "occperstatus_unknown"),
    
    OPEN_CASE_EXPIREDCOMPLIANCE(                    "One or more cases with violations outside compliance window", 
                                                    0, 
                                                    "",
                                                    "occperstatus_unknown"),
    
    OPEN_CASE_CITATION(                             "One or more cases with cited violations", 
                                                    0, 
                                                    "",
                                                    "occperstatus_unknown"),
    
    UNKNOWN(                                        "Unable to determine overall occupancy status", 
                                                    0, 
                                                    "",
                                                    "occperstatus_unknown");
    
    private final String label;
    private final int phaseOrder;
    private final String displayCSSClass;
    private final String materialIcon;
    
    private PropertyStatusCodeEnforcement(String label, int ord, String css, String iconLkup){
        this.label = label;
        this.phaseOrder = ord;
        displayCSSClass = css;
        this.materialIcon = iconLkup;
    }
    
    public String getLabel(){
        return label;
    }
    
    public int getOrder(){
        return phaseOrder;
    }
    
    public String getMaterialIcon(){
        return materialIcon;
    }

    /**
     * @return the displayCSSClass
     */
    public String getDisplayCSSClass() {
        return displayCSSClass;
    }
}


