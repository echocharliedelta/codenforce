/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.domain.BObStatusException;

/**
 * Wrapper class for citation-code violation links to allow users to 
 * see the current status and assign a new status
 * 
 * @author pierre15
 */
public class CitationCodeViolationLinkUpdateStatus {
    
   private CitationCodeViolationLink ccvLink;
   private CitationViolationStatusEnum updatedStatus;
   
    /**
     * Convenience method for extracting the updates CCVLink with its new status
     * @return
     * @throws BObStatusException 
     */
   public CitationCodeViolationLink extractUpdatedCVLink() throws BObStatusException{
       if(ccvLink != null){
           ccvLink.setCitVStatus(updatedStatus);
           return ccvLink;
       } else {
           throw new BObStatusException("Cannot inject status into null link");
       }
   }
   
   /**
    * constructor that takes in the original link
    * @param cvl 
    */
   public CitationCodeViolationLinkUpdateStatus (CitationCodeViolationLink cvl){
       ccvLink = cvl;
   }

   /**
    * Constructor for both the link and new status
    * @param cvl
    * @param us 
    */
   public CitationCodeViolationLinkUpdateStatus (CitationCodeViolationLink cvl, CitationViolationStatusEnum us){
       ccvLink = cvl;
       updatedStatus = us;
   }
   
    /**
     * @return the updatedStatus
     */
    public CitationViolationStatusEnum getUpdatedStatus() {
        return updatedStatus;
    }

    /**
     * @param updatedStatus the updatedStatus to set
     */
    public void setUpdatedStatus(CitationViolationStatusEnum updatedStatus) {
        this.updatedStatus = updatedStatus;
    }

    /**
     * @return the ccvLink
     */
    public CitationCodeViolationLink getCcvLink() {
        return ccvLink;
    }

    /**
     * @param ccvLink the ccvLink to set
     */
    public void setCcvLink(CitationCodeViolationLink ccvLink) {
        this.ccvLink = ccvLink;
    }
}
