package com.tcvcog.tcvce.entities;

public enum MailingStreetFieldsEnum {
    NAME("Name", "Street Name");

    private final String fieldName;
    private final String friendlyName;

    private MailingStreetFieldsEnum(String fieldName, String friendlyName) {
        this.fieldName = fieldName;
        this.friendlyName = friendlyName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

}
