/*
 * Copyright (C) 2022 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.domain.BObStatusException;
import java.time.LocalDateTime;

/**
 * Superclass of the third generation of tracked objects which requires both a
 * timestamp for creation, update, and deactivation  as well as the UMAP ID
 * of the user carrying out these operations. 
 * 
 * First implemented in December 2022 with CodeSource and CodeSet objects
 * with an eye for more wide scale implementation in coming months
 * 
 * @author sylvia
 */
public  abstract class  UMAPTrackedEntity 
        extends         BOb
        implements      IFace_keyIdentified {
    
    protected LocalDateTime createdTS;
    protected UserMuniAuthPeriod createdby_UMAP;
    protected int createdBy_UMAPID;

    protected LocalDateTime lastUpdatedTS;
    protected UserMuniAuthPeriod lastUpdatedby_UMAP;
    protected int lastUpdatedBy_UMAPID;

    protected LocalDateTime deactivatedTS;
    protected UserMuniAuthPeriod deactivatedBy_UMAP;
    protected int deactivatedBy_UMAPID;

    public abstract UMAPTrackedEnum getUMAPTrackedEntityEnum();
    
    
    /**
     * @return the createdTS
     */
    public LocalDateTime getCreatedTS() {
        return createdTS;
    }

    /**
     * @return the createdby_UMAP
     */
    public UserMuniAuthPeriod getCreatedby_UMAP() {
        return createdby_UMAP;
    }

    /**
     * @return the lastUpdatedTS
     */
    public LocalDateTime getLastUpdatedTS() {
        return lastUpdatedTS;
    }

    /**
     * @return the lastUpdatedby_UMAP
     */
    public UserMuniAuthPeriod getLastUpdatedby_UMAP() {
        return lastUpdatedby_UMAP;
    }

    /**
     * @return the deactivatedTS
     */
    public LocalDateTime getDeactivatedTS() {
        return deactivatedTS;
    }

    /**
     * @return the deactivatedBy_UMAP
     */
    public UserMuniAuthPeriod getDeactivatedBy_UMAP() {
        return deactivatedBy_UMAP;
    }

    /**
     * @param createdTS the createdTS to set
     */
    public void setCreatedTS(LocalDateTime createdTS) {
        this.createdTS = createdTS;
    }

    /**
     * @param createdby_UMAP the createdby_UMAP to set
     */
    public void setCreatedby_UMAP(UserMuniAuthPeriod createdby_UMAP) {
        this.createdby_UMAP = createdby_UMAP;
    }

    /**
     * @param lastUpdatedTS the lastUpdatedTS to set
     */
    public void setLastUpdatedTS(LocalDateTime lastUpdatedTS) {
        this.lastUpdatedTS = lastUpdatedTS;
    }

    /**
     * @param lastUpdatedby_UMAP the lastUpdatedby_UMAP to set
     */
    public void setLastUpdatedby_UMAP(UserMuniAuthPeriod lastUpdatedby_UMAP) {
        this.lastUpdatedby_UMAP = lastUpdatedby_UMAP;
    }

    /**
     * @param deactivatedTS the deactivatedTS to set
     */
    public void setDeactivatedTS(LocalDateTime deactivatedTS) {
        this.deactivatedTS = deactivatedTS;
    }

    /**
     * @param deactivatedBy_UMAP the deactivatedBy_UMAP to set
     */
    public void setDeactivatedBy_UMAP(UserMuniAuthPeriod deactivatedBy_UMAP) {
        this.deactivatedBy_UMAP = deactivatedBy_UMAP;
    }

    /**
     * @return the deactivatedBy_UMAPID
     */
    public int getDeactivatedBy_UMAPID() {
        if(deactivatedBy_UMAP != null){
            return deactivatedBy_UMAP.getUserMuniAuthPeriodID();
        }
        return deactivatedBy_UMAPID;
    }

    /**
     * @param deactivatedBy_UMAPID the deactivatedBy_UMAPID to set
     * @throws com.tcvcog.tcvce.domain.BObStatusException for inconsistent setting of a 
     * deac ID that doesn't match the object version of this field
     */
    public void setDeactivatedBy_UMAPID(int deactivatedBy_UMAPID) throws BObStatusException {
        if(deactivatedBy_UMAP != null && deactivatedBy_UMAP.getUserMuniAuthPeriodID() != deactivatedBy_UMAPID){
            throw new BObStatusException("incoming deacby umap ID is not equal to the object version of this field");
        }
        this.deactivatedBy_UMAPID = deactivatedBy_UMAPID;
    }

    /**
     * Convenience method for generating a boolean active status from the deac ts
     * @return 
     */
    public boolean isActive(){
        return deactivatedTS == null;
    }
    
    /**
     * @return the lastUpdatedBy_UMAPID
     */
    public int getLastUpdatedBy_UMAPID() {
        if(lastUpdatedby_UMAP != null){
            return lastUpdatedby_UMAP.getUserMuniAuthPeriodID();
        }
        return lastUpdatedBy_UMAPID;
    }

    /**
     * @param lastUpdatedBy_UMAPID the lastUpdatedBy_UMAPID to set
     * @throws com.tcvcog.tcvce.domain.BObStatusException if incoming ID isn't equal to object UMAP ID
     */
    public void setLastUpdatedBy_UMAPID(int lastUpdatedBy_UMAPID) throws BObStatusException {
        if(lastUpdatedby_UMAP != null && lastUpdatedby_UMAP.getUserMuniAuthPeriodID() != lastUpdatedBy_UMAPID){
            throw new BObStatusException("incoming deacby umap ID is not equal to the object version of this field");
        }
        this.lastUpdatedBy_UMAPID = lastUpdatedBy_UMAPID;
    }

    /**
     * @return the createdBy_UMAPID
     */
    public int getCreatedBy_UMAPID() {
        if(createdby_UMAP != null){
            return createdby_UMAP.getUserMuniAuthPeriodID();
        }
        return createdBy_UMAPID;
    }

    /**
     * @param createdBy_UMAPID the createdBy_UMAPID to set
     * @throws com.tcvcog.tcvce.domain.BObStatusException for inconsistent ID with object version
     */
    public void setCreatedBy_UMAPID(int createdBy_UMAPID) throws BObStatusException {
        if(createdby_UMAP != null && createdby_UMAP.getUserMuniAuthPeriodID() != createdBy_UMAPID){
            throw new BObStatusException("incoming deacby umap ID is not equal to the object version of this field");
        }
        this.createdBy_UMAPID = createdBy_UMAPID;
    }
    
}
