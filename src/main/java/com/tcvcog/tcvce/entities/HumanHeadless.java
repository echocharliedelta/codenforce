/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Container for fields that come from somewhere, like a public form, 
 * and are candidates to be injected into an actual Human object, which
 * in May 2023 consists of a name, a phone number, and an email address
 * 
 * And the project of using this object was scrapped in late May 2023
 * 
 * @author sylvia
 */
public class HumanHeadless {
    private String name;
    private String emailAddress;
    private String phoneNumber;
    private ContactPhoneType phoneNumberType;

    public HumanHeadless(String n, String ea, String pn, ContactPhoneType pnt) {
        this.name = n;
        this.emailAddress = ea;
        this.phoneNumber = pn;
        this.phoneNumberType = pnt;
    }

    
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @return the phoneNumberType
     */
    public ContactPhoneType getPhoneNumberType() {
        return phoneNumberType;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param emailAddress the emailAddress to set
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * @param phoneNumber the phoneNumber to set
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @param phoneNumberType the phoneNumberType to set
     */
    public void setPhoneNumberType(ContactPhoneType phoneNumberType) {
        this.phoneNumberType = phoneNumberType;
    }
}
