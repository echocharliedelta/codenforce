package com.tcvcog.tcvce.entities;

public class CitationCECasePropertyHeavy extends Citation {

    private CECase cecaseProUHeavy;
    private CitationStatusLogEntry latestLogEntry;

    public CitationCECasePropertyHeavy() {
    }

    public CitationCECasePropertyHeavy(Citation c) {
        super(c);
    }

    public CitationCECasePropertyHeavy(CECase cecaseProUHeavy) {
        this.cecaseProUHeavy = cecaseProUHeavy;
    }

    public CECase getCecaseProUHeavy() {
        return cecaseProUHeavy;
    }

    public void setCecaseProUHeavy(CECase cecaseProUHeavy) {
        this.cecaseProUHeavy = cecaseProUHeavy;
    }

    public CitationStatusLogEntry getLatestLogEntry() {
        return latestLogEntry;
    }

    public void setLatestLogEntry(CitationStatusLogEntry latestEntry) {
        this.latestLogEntry = latestEntry;
    }

}
