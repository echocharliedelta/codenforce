/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Contains static data related to how an object can be linked to an event.
 * Event links are different from the event's actual parent which as of Dec 2023
 * are CECases, OccPeriods, and Parcels. Linked event objects 
 * include sub-objects of cases and periods like inspections, citations, violations, 
 * and novs
 * 
 * @author pierre15
 */
public enum EventLinkEnum {
    
    EVENT                   ("Event","eventlink_eventid"),
    /**
     * Not implemented as of Major event overhaul in Dec/Jan 2024
     */
    CODE_VIOLATION          ("Code Violation","codeviolation_violationid "),
    
    NOTICE_OF_VIOLATION     ("Notice of Violation","noticeofviolation_noticeid"),           
    CITATION_STATUS         ("Citation status log","citationstatus_citstatid"),
    FIELD_INSPECTION        ("Field Inspection","inspection_inspectionid"),
    OCCPERMIT               ("Occupancy Permit","occpermit_permitid");
    
    
    
    private final String title;
    private final String targetTableFKFieldString;
    
    
    private EventLinkEnum(String t, String fks){
        title = t;
        targetTableFKFieldString = fks;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return the targetTableFKFieldString
     */
    public String getTargetTableFKFieldString() {
        return targetTableFKFieldString;
    }
    
}
