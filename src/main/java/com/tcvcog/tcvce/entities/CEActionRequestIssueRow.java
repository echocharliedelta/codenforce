/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import java.util.List;
import java.util.Map;

/**
 * Represents a record in the CEAR summary table in CECase List reports
 * @author pierre15
 */
public class CEActionRequestIssueRow {
    
    private CEActionRequestIssueType issue;
    private List<CEActionRequest> requestList;
    private Map<CEActionRequestStatus, Integer> issueOutcomeMap;

    
    
    /**
     * @return the issue
     */
    public CEActionRequestIssueType getIssue() {
        return issue;
    }

    /**
     * @param issue the issue to set
     */
    public void setIssue(CEActionRequestIssueType issue) {
        this.issue = issue;
    }

    /**
     * @return the requestList
     */
    public List<CEActionRequest> getRequestList() {
        return requestList;
    }

    /**
     * @param requestList the requestList to set
     */
    public void setRequestList(List<CEActionRequest> requestList) {
        this.requestList = requestList;
    }

    /**
     * @return the issueOutcomeMap
     */
    public Map<CEActionRequestStatus, Integer> getIssueOutcomeMap() {
        return issueOutcomeMap;
    }

    /**
     * @param issueOutcomeMap the issueOutcomeMap to set
     */
    public void setIssueOutcomeMap(Map<CEActionRequestStatus, Integer> issueOutcomeMap) {
        this.issueOutcomeMap = issueOutcomeMap;
    }
    
    
    
}
