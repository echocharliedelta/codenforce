/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import java.util.List;

/**
 * Implemented by Citation and CitationViolation objects
 * which both hold penalties.
 * 
 * @author pierre15
 */
public interface IfaceCitationPenaltyHolder {
    /**
     * Implementing class must be able to ID itself
     * @return 
     */
    public String getHolderNameString();
    
    /**
     * Return the FK field in citationpenalty table;
     * @return 
     */
    public String getFKField();
    
    /**
     * Return the key
     * @return 
     */
    public int getHolderPK();
    
    /**
     * standard getter
     * @return 
     */
    public List<CitationPenalty> getCitationPenaltyList();
    
    /**
     * Standard setter
     * @param cpl 
     */
    public void setCitationPenaltyList(List<CitationPenalty> cpl);
    
    
}
