/*
 * Copyright (C) 2019 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import com.tcvcog.tcvce.entities.occupancy.OccChecklistTemplate;
import com.tcvcog.tcvce.entities.occupancy.OccPermitType;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * Represents a random slew of settings applying to operations across CNF
 * that are customizable by muni, and bundled here for easy municipal onboarding
 * 
 * @author sylvia
 */
public class MuniProfile
    extends     UMAPTrackedEntity
    implements  IFace_noteHolder, IFaceCachable{
    
    private final static String TABLE_NAME = "muniprofile";
    private final static String MUNIPROFILE_PK = "profileid";
    private final static UMAPTrackedEnum UMAP_TRACKED_ENUM = UMAPTrackedEnum.MUNIPROFILE;
    private final static String FRIENDLY_NAME = "Municipality Profile";
    
    private int profileID;
    private String title;
    private String description;
    
    private String notes;
    private List<OccPermitType> occPermitTypeList;
    
    
    private int defaulFailedFINFollowupDays;
    
    // RANDOM SWITCHES
    private boolean dispatchInspectionsByDefault;
    private EventCategory defaultCECaseOriginationCategory;
    
    

    // ------------------ event auto deploys -----------------------------------
    private EventCategory eventCatFINPassed;
    private EventCategory eventCatFINFailed;
    private EventCategory eventCatFailedFINFollowUp;
    
    private EventCategory eventCatPermitIssued;
    private EventCategory eventCatExpiredTCOFollowUp;
    
    
    
    // ------------------ END Event auto deploys -------------------------------
    
    /**
     * @Deprecated 
     */
    private int continuousoccupancybufferdays;
    /**
     * @Deprecated 
     */
    private int novDefaultDaysForFollowup;
    /**
     * @Deprecated 
     */
    private EventRuleSet eventRuleSetCE;
    
    /**
     * Standing by for activation
     */
    private List<Fee> feeList;
    
    private LinkedObjectRole propSearchFilter;
    
    // Auto-load switches
    private boolean autoloadOpenCECases;
    private boolean autoloadOpenOccPeriods;
    
   
    
    /***************************************************************************
     * ************             CASE PRIORITIES           **********************
     * *************************************************************************
     */
    
    // CECASE PRIORITY PARAMETERS
    /**
     * @Deprecated Sept 2024 upgrades v.3.1.x has no administrative buffer
     */
    private int priorityParamDeadlineAdministrativeBufferDays;
    /**
     * @Deprecated Sept 2024 upgrades v.3.1.x does not deliberately account 
     * for mail system delivery latency. Officers bake this into their action date
     */
    private int priorityParamLetterSendBufferDays;
    /**
     * @Deprecated Sept 2024 upgrades v.3.1.x makes determination using action date on 
     * letter AND violation compliance dates
     */
    private boolean prioritizeLetterFollowUpBuffer;
    
    /**
     * Allows officers to kick a case back to green by attaching events to a case
     * whose enactment creates the buffer
     */
    private boolean priorityAllowEventCategoryGreenBuffers;
    
    /**
     * new Version 3.1.x param: officers have this many days before a blue case (opening)
     * becomes yellow (administrative action required)
     */
    private int priorityParamCaseOpeningAdministrativeBuffer;
    
    
    /***************************************************************************
     * ************             PERMISSIONS               **********************
     * *************************************************************************
     */
    
    // Properties
    private boolean managerRequiredAbandonment;
    
    // Code/Ordinances
    private boolean managerRequiredCodeSourceManage;
    private boolean managerRequiredCodeBookManage;
    
    // Inspections
    // --> located in OccInspectionCoordinator
    private boolean managerRequiredChecklistEdit;
    private boolean requireCodeOfficerTrueConductInspection;
    // HIERARCHICALLY RANKED
    private boolean requireCodeOfficerTrueFinalizeInspection;
    private boolean managerRequiredInspectionFinalize;
    
    // CE Case related permissions
    
    // -- > checkpoints located in CaseCoordinator
    // -- > Violations
    private boolean requireCodeOfficerTrueAttachViolToCECase;
    private boolean managerRequiredViolationExtStipComp;
    
    // -- > NOVs
    // -- > checkpoints located in CaseCoordinator
    // HIERARCHICALLY RANKED
    private boolean requireCodeOfficerTrueFinalizeNOV;
    private boolean managerRequiredNOVFinalize;
    
    // -- > Ciations
    // -- > checkpoints located in CaseCoordinator
    // HIERARCHICALLY RANKED
    private boolean requireCodeOfficerTrueOpenCitation;
    private boolean managerRequiredCitationOpenClose;
    
    private boolean requireCodeOfficerTrueUpdateCitation;
    
    // -- > Case closure
    // -- > checkpoints located in CaseCoordinator
    // HIERARCHICALLY RANKED
    private boolean requireCodeOfficerTrueCloseCECase;
    private boolean managerRequiredcCloseCECase;
    
    // Occ Period related permissions
    
    // Permitting
    // HIERARCHICALLY RANKED
    private boolean requireCodeOfficerTrueIssuePermits;
    private boolean managerRequiredPermitFinalize;
    
    /**
     * @return the profileID
     */
    public int getProfileID() {
        return profileID;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @return the continuousoccupancybufferdays
     */
    public int getContinuousoccupancybufferdays() {
        return continuousoccupancybufferdays;
    }

    /**
     * @param profileID the profileID to set
     */
    public void setProfileID(int profileID) {
        this.profileID = profileID;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @param continuousoccupancybufferdays the continuousoccupancybufferdays to set
     */
    public void setContinuousoccupancybufferdays(int continuousoccupancybufferdays) {
        this.continuousoccupancybufferdays = continuousoccupancybufferdays;
    }

    /**
     * @return the eventRuleSetCE
     */
    public EventRuleSet getEventRuleSetCE() {
        return eventRuleSetCE;
    }

    /**
     * @param eventRuleSetCE the eventRuleSetCE to set
     */
    public void setEventRuleSetCE(EventRuleSet eventRuleSetCE) {
        this.eventRuleSetCE = eventRuleSetCE;
    }

    /**
     * @return the occPermitTypeList
     */
    public List<OccPermitType> getOccPermitTypeList() {
        return occPermitTypeList;
    }

    /**
     * @param occPermitTypeList the occPermitTypeList to set
     */
    public void setOccPermitTypeList(List<OccPermitType> occPermitTypeList) {
        this.occPermitTypeList = occPermitTypeList;
    }
    
     @Override
    public int hashCode() {
        int hash = 15;
        hash = 500 * hash + this.profileID;
        hash = 500 * hash + Objects.hashCode(this.title);
        hash = 500 * hash + Objects.hashCode(this.description);
        hash = 500 * hash + Objects.hashCode(this.notes);
        hash = 500 * hash + this.continuousoccupancybufferdays;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MuniProfile other = (MuniProfile) obj;
        if (this.profileID != other.profileID) {
            return false;
        }
        return true;
    }

    /**
     * @return the novDefaultDaysForFollowup
     */
    public int getNovDefaultDaysForFollowup() {
        return novDefaultDaysForFollowup;
    }

    /**
     * @param novDefaultDaysForFollowup the novDefaultDaysForFollowup to set
     */
    public void setNovDefaultDaysForFollowup(int novDefaultDaysForFollowup) {
        this.novDefaultDaysForFollowup = novDefaultDaysForFollowup;
    }

    /**
     * @return the feeList
     */
    public List<Fee> getFeeList() {
        return feeList;
    }

    /**
     * @param feeList the feeList to set
     */
    public void setFeeList(List<Fee> feeList) {
        this.feeList = feeList;
    }

   

    /**
     * @return the priorityParamDeadlineAdministrativeBufferDays
     */
    public int getPriorityParamDeadlineAdministrativeBufferDays() {
        return priorityParamDeadlineAdministrativeBufferDays;
    }

    /**
     * @return the priorityParamLetterSendBufferDays
     */
    public int getPriorityParamLetterSendBufferDays() {
        return priorityParamLetterSendBufferDays;
    }

    /**
     * @return the prioritizeLetterFollowUpBuffer
     */
    public boolean isPrioritizeLetterFollowUpBuffer() {
        return prioritizeLetterFollowUpBuffer;
    }

    /**
     * @return the priorityAllowEventCategoryGreenBuffers
     */
    public boolean isPriorityAllowEventCategoryGreenBuffers() {
        return priorityAllowEventCategoryGreenBuffers;
    }

    /**
     * @param priorityParamDeadlineAdministrativeBufferDays the priorityParamDeadlineAdministrativeBufferDays to set
     */
    public void setPriorityParamDeadlineAdministrativeBufferDays(int priorityParamDeadlineAdministrativeBufferDays) {
        this.priorityParamDeadlineAdministrativeBufferDays = priorityParamDeadlineAdministrativeBufferDays;
    }

    /**
     * @param priorityParamLetterSendBufferDays the priorityParamLetterSendBufferDays to set
     */
    public void setPriorityParamLetterSendBufferDays(int priorityParamLetterSendBufferDays) {
        this.priorityParamLetterSendBufferDays = priorityParamLetterSendBufferDays;
    }

    /**
     * @param prioritizeLetterFollowUpBuffer the prioritizeLetterFollowUpBuffer to set
     */
    public void setPrioritizeLetterFollowUpBuffer(boolean prioritizeLetterFollowUpBuffer) {
        this.prioritizeLetterFollowUpBuffer = prioritizeLetterFollowUpBuffer;
    }

    /**
     * @param priorityAllowEventCategoryGreenBuffers the priorityAllowEventCategoryGreenBuffers to set
     */
    public void setPriorityAllowEventCategoryGreenBuffers(boolean priorityAllowEventCategoryGreenBuffers) {
        this.priorityAllowEventCategoryGreenBuffers = priorityAllowEventCategoryGreenBuffers;
    }

    /**
     * @return the requireCodeOfficerTrueConductInspection
     */
    public boolean isRequireCodeOfficerTrueConductInspection() {
        return requireCodeOfficerTrueConductInspection;
    }

    /**
     * @return the requireCodeOfficerTrueFinalizeInspection
     */
    public boolean isRequireCodeOfficerTrueFinalizeInspection() {
        return requireCodeOfficerTrueFinalizeInspection;
    }

    /**
     * @return the requireCodeOfficerTrueIssuePermits
     */
    public boolean isRequireCodeOfficerTrueIssuePermits() {
        return requireCodeOfficerTrueIssuePermits;
    }

    /**
     * @return the requireCodeOfficerTrueAttachViolToCECase
     */
    public boolean isRequireCodeOfficerTrueAttachViolToCECase() {
        return requireCodeOfficerTrueAttachViolToCECase;
    }

    /**
     * @return the requireCodeOfficerTrueFinalizeNOV
     */
    public boolean isRequireCodeOfficerTrueFinalizeNOV() {
        return requireCodeOfficerTrueFinalizeNOV;
    }

    /**
     * @return the requireCodeOfficerTrueCloseCECase
     */
    public boolean isRequireCodeOfficerTrueCloseCECase() {
        return requireCodeOfficerTrueCloseCECase;
    }

    /**
     * @return the requireCodeOfficerTrueOpenCitation
     */
    public boolean isRequireCodeOfficerTrueOpenCitation() {
        return requireCodeOfficerTrueOpenCitation;
    }

    /**
     * @return the requireCodeOfficerTrueUpdateCitation
     */
    public boolean isRequireCodeOfficerTrueUpdateCitation() {
        return requireCodeOfficerTrueUpdateCitation;
    }

    /**
     * @param requireCodeOfficerTrueConductInspection the requireCodeOfficerTrueConductInspection to set
     */
    public void setRequireCodeOfficerTrueConductInspection(boolean requireCodeOfficerTrueConductInspection) {
        this.requireCodeOfficerTrueConductInspection = requireCodeOfficerTrueConductInspection;
    }

    /**
     * @param requireCodeOfficerTrueFinalizeInspection the requireCodeOfficerTrueFinalizeInspection to set
     */
    public void setRequireCodeOfficerTrueFinalizeInspection(boolean requireCodeOfficerTrueFinalizeInspection) {
        this.requireCodeOfficerTrueFinalizeInspection = requireCodeOfficerTrueFinalizeInspection;
    }

    /**
     * @param requireCodeOfficerTrueIssuePermits the requireCodeOfficerTrueIssuePermits to set
     */
    public void setRequireCodeOfficerTrueIssuePermits(boolean requireCodeOfficerTrueIssuePermits) {
        this.requireCodeOfficerTrueIssuePermits = requireCodeOfficerTrueIssuePermits;
    }

    /**
     * @param requireCodeOfficerTrueAttachViolToCECase the requireCodeOfficerTrueAttachViolToCECase to set
     */
    public void setRequireCodeOfficerTrueAttachViolToCECase(boolean requireCodeOfficerTrueAttachViolToCECase) {
        this.requireCodeOfficerTrueAttachViolToCECase = requireCodeOfficerTrueAttachViolToCECase;
    }

    /**
     * @param requireCodeOfficerTrueFinalizeNOV the requireCodeOfficerTrueFinalizeNOV to set
     */
    public void setRequireCodeOfficerTrueFinalizeNOV(boolean requireCodeOfficerTrueFinalizeNOV) {
        this.requireCodeOfficerTrueFinalizeNOV = requireCodeOfficerTrueFinalizeNOV;
    }

    /**
     * @param requireCodeOfficerTrueCloseCECase the requireCodeOfficerTrueCloseCECase to set
     */
    public void setRequireCodeOfficerTrueCloseCECase(boolean requireCodeOfficerTrueCloseCECase) {
        this.requireCodeOfficerTrueCloseCECase = requireCodeOfficerTrueCloseCECase;
    }

    /**
     * @param requireCodeOfficerTrueOpenCitation the requireCodeOfficerTrueOpenCitation to set
     */
    public void setRequireCodeOfficerTrueOpenCitation(boolean requireCodeOfficerTrueOpenCitation) {
        this.requireCodeOfficerTrueOpenCitation = requireCodeOfficerTrueOpenCitation;
    }

    /**
     * @param requireCodeOfficerTrueUpdateCitation the requireCodeOfficerTrueUpdateCitation to set
     */
    public void setRequireCodeOfficerTrueUpdateCitation(boolean requireCodeOfficerTrueUpdateCitation) {
        this.requireCodeOfficerTrueUpdateCitation = requireCodeOfficerTrueUpdateCitation;
    }

    /**
     * @return the managerRequiredAbandonment
     */
    public boolean isManagerRequiredAbandonment() {
        return managerRequiredAbandonment;
    }

    /**
     * @return the managerRequiredcCloseCECase
     */
    public boolean isManagerRequiredcCloseCECase() {
        return managerRequiredcCloseCECase;
    }

    /**
     * @return the managerRequiredCitationOpenClose
     */
    public boolean isManagerRequiredCitationOpenClose() {
        return managerRequiredCitationOpenClose;
    }

    /**
     * @return the managerRequiredPermitFinalize
     */
    public boolean isManagerRequiredPermitFinalize() {
        return managerRequiredPermitFinalize;
    }

    /**
     * @return the managerRequiredInspectionFinalize
     */
    public boolean isManagerRequiredInspectionFinalize() {
        return managerRequiredInspectionFinalize;
    }

    /**
     * @return the managerRequiredNOVFinalize
     */
    public boolean isManagerRequiredNOVFinalize() {
        return managerRequiredNOVFinalize;
    }

    /**
     * @return the managerRequiredViolationExtStipComp
     */
    public boolean isManagerRequiredViolationExtStipComp() {
        return managerRequiredViolationExtStipComp;
    }

    /**
     * @return the managerRequiredChecklistEdit
     */
    public boolean isManagerRequiredChecklistEdit() {
        return managerRequiredChecklistEdit;
    }

    /**
     * @return the managerRequiredCodeSourceManage
     */
    public boolean isManagerRequiredCodeSourceManage() {
        return managerRequiredCodeSourceManage;
    }

    /**
     * @return the managerRequiredCodeBookManage
     */
    public boolean isManagerRequiredCodeBookManage() {
        return managerRequiredCodeBookManage;
    }

    /**
     * @param managerRequiredAbandonment the managerRequiredAbandonment to set
     */
    public void setManagerRequiredAbandonment(boolean managerRequiredAbandonment) {
        this.managerRequiredAbandonment = managerRequiredAbandonment;
    }

    /**
     * @param managerRequiredcCloseCECase the managerRequiredcCloseCECase to set
     */
    public void setManagerRequiredcCloseCECase(boolean managerRequiredcCloseCECase) {
        this.managerRequiredcCloseCECase = managerRequiredcCloseCECase;
    }

    /**
     * @param managerRequiredCitationOpenClose the managerRequiredCitationOpenClose to set
     */
    public void setManagerRequiredCitationOpenClose(boolean managerRequiredCitationOpenClose) {
        this.managerRequiredCitationOpenClose = managerRequiredCitationOpenClose;
    }

    /**
     * @param managerRequiredPermitFinalize the managerRequiredPermitFinalize to set
     */
    public void setManagerRequiredPermitFinalize(boolean managerRequiredPermitFinalize) {
        this.managerRequiredPermitFinalize = managerRequiredPermitFinalize;
    }

    /**
     * @param managerRequiredInspectionFinalize the managerRequiredInspectionFinalize to set
     */
    public void setManagerRequiredInspectionFinalize(boolean managerRequiredInspectionFinalize) {
        this.managerRequiredInspectionFinalize = managerRequiredInspectionFinalize;
    }

    /**
     * @param managerRequiredNOVFinalize the managerRequiredNOVFinalize to set
     */
    public void setManagerRequiredNOVFinalize(boolean managerRequiredNOVFinalize) {
        this.managerRequiredNOVFinalize = managerRequiredNOVFinalize;
    }

    /**
     * @param managerRequiredViolationExtStipComp the managerRequiredViolationExtStipComp to set
     */
    public void setManagerRequiredViolationExtStipComp(boolean managerRequiredViolationExtStipComp) {
        this.managerRequiredViolationExtStipComp = managerRequiredViolationExtStipComp;
    }

    /**
     * @param managerRequiredChecklistEdit the managerRequiredChecklistEdit to set
     */
    public void setManagerRequiredChecklistEdit(boolean managerRequiredChecklistEdit) {
        this.managerRequiredChecklistEdit = managerRequiredChecklistEdit;
    }

    /**
     * @param managerRequiredCodeSourceManage the managerRequiredCodeSourceManage to set
     */
    public void setManagerRequiredCodeSourceManage(boolean managerRequiredCodeSourceManage) {
        this.managerRequiredCodeSourceManage = managerRequiredCodeSourceManage;
    }

    /**
     * @param managerRequiredCodeBookManage the managerRequiredCodeBookManage to set
     */
    public void setManagerRequiredCodeBookManage(boolean managerRequiredCodeBookManage) {
        this.managerRequiredCodeBookManage = managerRequiredCodeBookManage;
    }

    @Override
    public UMAPTrackedEnum getUMAPTrackedEntityEnum() {
        return UMAP_TRACKED_ENUM;
    }

    @Override
    public String getPKFieldName() {
        return MUNIPROFILE_PK;
    }

    @Override
    public int getDBKey() {
        return profileID;
    }

    @Override
    public String getDBTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getNoteHolderFriendlyName() {
        return FRIENDLY_NAME; 
    }

    /**
     * @return the autoloadOpenOccPeriods
     */
    public boolean isAutoloadOpenOccPeriods() {
        return autoloadOpenOccPeriods;
    }

    /**
     * @param autoloadOpenOccPeriods the autoloadOpenOccPeriods to set
     */
    public void setAutoloadOpenOccPeriods(boolean autoloadOpenOccPeriods) {
        this.autoloadOpenOccPeriods = autoloadOpenOccPeriods;
    }

    /**
     * @return the autoloadOpenCECases
     */
    public boolean isAutoloadOpenCECases() {
        return autoloadOpenCECases;
    }

    /**
     * @param autoloadOpenCECases the autoloadOpenCECases to set
     */
    public void setAutoloadOpenCECases(boolean autoloadOpenCECases) {
        this.autoloadOpenCECases = autoloadOpenCECases;
    }

    /**
     * @return the propSearchFilter
     */
    public LinkedObjectRole getPropSearchFilter() {
        return propSearchFilter;
    }

    /**
     * @param propSearchFilter the propSearchFilter to set
     */
    public void setPropSearchFilter(LinkedObjectRole propSearchFilter) {
        this.propSearchFilter = propSearchFilter;
    }

    /**
     * @return the eventCatExpiredTCOFollowUp
     */
    public EventCategory getEventCatExpiredTCOFollowUp() {
        return eventCatExpiredTCOFollowUp;
    }

    /**
     * @param eventCatExpiredTCOFollowUp the eventCatExpiredTCOFollowUp to set
     */
    public void setEventCatExpiredTCOFollowUp(EventCategory eventCatExpiredTCOFollowUp) {
        this.eventCatExpiredTCOFollowUp = eventCatExpiredTCOFollowUp;
    }

    /**
     * @return the eventCatFailedFINFollowUp
     */
    public EventCategory getEventCatFailedFINFollowUp() {
        return eventCatFailedFINFollowUp;
    }

    /**
     * @param eventCatFailedFINFollowUp the eventCatFailedFINFollowUp to set
     */
    public void setEventCatFailedFINFollowUp(EventCategory eventCatFailedFINFollowUp) {
        this.eventCatFailedFINFollowUp = eventCatFailedFINFollowUp;
    }

    /**
     * @return the defaulFailedFINFollowupDays
     */
    public int getDefaulFailedFINFollowupDays() {
        return defaulFailedFINFollowupDays;
    }

    /**
     * @param defaulFailedFINFollowupDays the defaulFailedFINFollowupDays to set
     */
    public void setDefaulFailedFINFollowupDays(int defaulFailedFINFollowupDays) {
        this.defaulFailedFINFollowupDays = defaulFailedFINFollowupDays;
    }


    /**
     * @return the eventCatFINFailed
     */
    public EventCategory getEventCatFINFailed() {
        return eventCatFINFailed;
    }

    /**
     * @param eventCatFINFailed the eventCatFINFailed to set
     */
    public void setEventCatFINFailed(EventCategory eventCatFINFailed) {
        this.eventCatFINFailed = eventCatFINFailed;
    }

    /**
     * @return the eventCatFINPassed
     */
    public EventCategory getEventCatFINPassed() {
        return eventCatFINPassed;
    }

    /**
     * @param eventCatFINPassed the eventCatFINPassed to set
     */
    public void setEventCatFINPassed(EventCategory eventCatFINPassed) {
        this.eventCatFINPassed = eventCatFINPassed;
    }

    /**
     * @return the eventCatPermitIssued
     */
    public EventCategory getEventCatPermitIssued() {
        return eventCatPermitIssued;
    }

    /**
     * @param eventCatPermitIssued the eventCatPermitIssued to set
     */
    public void setEventCatPermitIssued(EventCategory eventCatPermitIssued) {
        this.eventCatPermitIssued = eventCatPermitIssued;
    }

    /**
     * @return the dispatchInspectionsByDefault
     */
    public boolean isDispatchInspectionsByDefault() {
        return dispatchInspectionsByDefault;
    }

    /**
     * @param dispatchInspectionsByDefault the dispatchInspectionsByDefault to set
     */
    public void setDispatchInspectionsByDefault(boolean dispatchInspectionsByDefault) {
        this.dispatchInspectionsByDefault = dispatchInspectionsByDefault;
    }

    /**
     * @return the defaultCECaseOriginationCategory
     */
    public EventCategory getDefaultCECaseOriginationCategory() {
        return defaultCECaseOriginationCategory;
    }

    /**
     * @param defaultCECaseOriginationCategory the defaultCECaseOriginationCategory to set
     */
    public void setDefaultCECaseOriginationCategory(EventCategory defaultCECaseOriginationCategory) {
        this.defaultCECaseOriginationCategory = defaultCECaseOriginationCategory;
    }

    @Override
    public int getCacheKey() {
        return profileID;
    }

    /**
     * @return the priorityParamCaseOpeningAdministrativeBuffer
     */
    public int getPriorityParamCaseOpeningAdministrativeBuffer() {
        return priorityParamCaseOpeningAdministrativeBuffer;
    }

    /**
     * @param priorityParamCaseOpeningAdministrativeBuffer the priorityParamCaseOpeningAdministrativeBuffer to set
     */
    public void setPriorityParamCaseOpeningAdministrativeBuffer(int priorityParamCaseOpeningAdministrativeBuffer) {
        this.priorityParamCaseOpeningAdministrativeBuffer = priorityParamCaseOpeningAdministrativeBuffer;
    }

   
}
