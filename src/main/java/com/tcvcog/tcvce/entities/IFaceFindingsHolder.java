/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

import java.util.List;

/**
 * Presents clients with access to the findings field as well
 * as the underlying Enforceable code element.
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */
public interface IFaceFindingsHolder {
    
    /**
     * All my implementers hold an ECE
     * @return 
     */
    public EnforceableCodeElement getEnforceableCodeElement();
    
    /**
     * Retrieves the findings
     * @return 
     */
    public String getFindings();
    
    /**
     * Injects the findings
     * @param dings     
     */
    public void setFindings(String dings);
    
    /**
     * Extracts all the candidate findings
     * @return 
     */
    public List<TextBlock> getCannedFindingsCandidates();
    
    /**
     * Injects all the candidate findings
     * @param blocks 
     */
    public void setCannedFindingsCandidates(List<TextBlock> blocks);
    
}
