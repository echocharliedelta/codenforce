package com.tcvcog.tcvce.entities;


public enum ContactPhoneFieldsEnum {
    PHONENUMBER("PhoneNumber", "Phone Number"),
    PHONETYPE("PhoneType", "Phone Type"),
    EXTENSION("Extension", "Extension"),
    PRIORITY("Priority", "Priority"),
    DISCONNECTTS("DisconnectTS", "Disconnected");

    private final String fieldName;
    private final String friendlyName;

    private ContactPhoneFieldsEnum(String fieldName, String friendlyName) {
        this.fieldName = fieldName;
        this.friendlyName = friendlyName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

}
