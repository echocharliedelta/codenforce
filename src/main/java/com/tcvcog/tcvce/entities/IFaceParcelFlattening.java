/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.entities;

/**
 * Marks that the object can provide an address in a single line or a two line string 
 * with an unclosed html break, i.e. XML illegal regular old break
 * 
 * @author pierre15
 */
public interface IFaceParcelFlattening {
    /**
     * Will most certainly include a regular old html break, not closing which requires
     * turnign off HTML escaping so the browser renders the actual break 
     * @return 
     */
    public String getAddressPretty2LineEscapeFalse();
    /**
     * building number space street name
     * @return 
     */
    public String getAddressPretty1Line();
    
    /**
     * As this interface marks that I can identify my parcel, here's how:
     * @return my parent parcel key, i.e. database ID as opposed to external county parcel ID
     */
    public int getParcelKey();
}
