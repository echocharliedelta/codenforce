/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.entities;

import com.tcvcog.tcvce.entities.occupancy.OccPeriodStatusEnum;
import java.util.Comparator;

/**
 * Represents the second iteration of case phasing
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */
public enum PriorityEnum {
    
    OPENING             ("Opening","Blue", 1, true, "cse-priority-openblue", "caseStageInvestigationIconID"),
    MONITORING          ("Monitoring","Green", 2, true, "cse-priority-monitoringgreen", "caseStageEnforcementIconID"),
    MON_OVERRIDE        ("Green Override","Green", 3, true, "cse-priority-monitoringgreen", "caseStageEnforcementIconID"),
    ACTION_REQUIRED     ("Action Required","Yellow", 4, true,  "cse-priority-actionrequiredyellow","caseStageEnforcementIconID"),
    ACTION_PASTDUE      ("Action Past Due","Red", 5, true,  "cse-priority-actionpastduered","caseStagePastdueIconID"),
    CITATION            ("Citation","Purple", 6, true,  "cse-priority-citation","caseStageCitationIconID"),
    REVIEW              ("Review","Dark Green", 7, true,  "cse-priority-review","caseStageReviewIconID"),
    ABANDONMENT_STALL   ("Abandonement Stall","Dark Yellow", 8, true,  "cse-priority-abandoneddarkyellow","caseStageEnforcementIconID"),
    CLOSED              ("Closed","Gray", -1, true,  "cse-priority-closedgray","caseStageClosedIconID"),
    UNKNOWN             ("Unknown","Gray", 0, true,  "cse-priority-unknowngray","caseStageReviewIconID"),
    DEACTIVATED         ("Deactivated case","Gray", 0, true,  "cse-priority-unknowngray","caseStageUnknownIconID"),
    CONTAINER           ("Container","Gray", -1, true,  "cse-priority-containergray","caseStageUnknownIconID");
    
    private final String label;
    private final String color;
    private final int priorityOrder;
    private final boolean qualifiesAsOpen;
    protected final String rowStyleClass;
    private final String iconPropertyLookup;
     
    private PriorityEnum(String label, String color, int ord, boolean os, String rsc, String icon){
        this.label = label;
        this.color = color;
        this.priorityOrder = ord;
        this.qualifiesAsOpen = os;
        this.rowStyleClass = rsc;
        this.iconPropertyLookup = icon;
    }
    
    
     /**
     * ALlows for ordering of this enum by severity
     */
   public static Comparator<PriorityEnum> severityComparator = new Comparator<PriorityEnum>(){
        @Override
        public int compare(PriorityEnum o1, PriorityEnum o2) {
            // OPEN AI WROTE THIS
            Integer sev1 = (o1 != null) ? o1.getPriorityOrder(): null;
            Integer sev2 = (o2 != null) ? o2.getPriorityOrder(): null;

            if (sev1 == null && sev2 == null) {
                return 0; // Both are null, consider them equal
            } else if (sev1 == null) {
                return -1; // Null is considered smaller than non-null
            } else if (sev2 == null) {
                return 1; // Non-null is considered larger than null
            } else {
                return sev1.compareTo(sev2);
            }
        }
   };
    
    
    public String getLabel(){
        return label;
    }
    
    public String getIconPropertyLookup(){
        return iconPropertyLookup;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @return the priorityOrder
     */
    public int getPriorityOrder() {
        return priorityOrder;
    }

    /**
     * @return the qualifiesAsOpen
     */
    public boolean isQualifiesAsOpen() {
        return qualifiesAsOpen;
    }

    /**
     * @return the rowStyleClass
     */
    public String getRowStyleClass() {
        return rowStyleClass;
    }
    
    
    
    
    
    
}
