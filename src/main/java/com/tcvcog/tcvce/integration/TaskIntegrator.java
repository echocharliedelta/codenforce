/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.sun.source.tree.AssignmentTree;
import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.occupancy.application.TaskAssigned;
import com.tcvcog.tcvce.occupancy.application.TaskAssignedRoleEnum;
import com.tcvcog.tcvce.occupancy.application.TaskAssignee;
import com.tcvcog.tcvce.occupancy.application.TaskChain;
import com.tcvcog.tcvce.occupancy.application.TaskDueDateTypeEnum;
import com.tcvcog.tcvce.occupancy.application.TaskLinkage;
import com.tcvcog.tcvce.occupancy.application.TaskSequenceTypeEnum;
import com.tcvcog.tcvce.occupancy.application.TaskSpecification;
import com.tcvcog.tcvce.occupancy.application.TaskTypeEnum;
import com.tcvcog.tcvce.coordinators.BlobCoordinator;
import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.coordinators.CodeCoordinator;
import com.tcvcog.tcvce.coordinators.EventCoordinator;
import com.tcvcog.tcvce.coordinators.MunicipalityCoordinator;
import com.tcvcog.tcvce.coordinators.OccInspectionCoordinator;
import com.tcvcog.tcvce.coordinators.OccupancyCoordinator;
import com.tcvcog.tcvce.coordinators.PaymentCoordinator;
import com.tcvcog.tcvce.coordinators.PersonCoordinator;
import com.tcvcog.tcvce.coordinators.TaskCoordinator;
import com.tcvcog.tcvce.coordinators.UserCoordinator;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.EventRealm;
import com.tcvcog.tcvce.entities.EventCnF;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.UserAuthorized;
import com.tcvcog.tcvce.entities.occupancy.OccPeriod;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Home for DB integration for the task management subsystem of objects
 * 
 * @author Ellen Bascomb of apartment 31Y
 */
public class TaskIntegrator extends BackingBeanUtils {
    
     /*******************************************************************
     ************** TASK SPECIFICATIONS ********************************
     *******************************************************************
     */
    
    /**
     * Getter for TaskSpecification objects
     * 
     * @param specID cannot be zero
     * @return the fully-baked TaskSpecification
     * @throws IntegrationException 
     */
    public TaskSpecification getTaskSpecification(int specID) throws IntegrationException{
        if(specID == 0){
            throw new IntegrationException("cannot get TaskSpecification with ID == 0");
        }
         
        String query = """
                            SELECT   taskspecid, title, description, 
                                     tasktype, duetype, assignmentrolesuggested, 
                                     predecessor_predid, predecessorrole, defaultpredecessorbufferdays, 
                                     eventcatcompletion_catid, governingordinance_elementid, displayorder, 
                                     notificationdefault, expecteddurationmins, active
                                         FROM public.taskspec WHERE taskspecid=?;
                       """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        TaskSpecification spec = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, specID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                spec = generateTaskSpecifciation(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot generate event", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 

        return spec;
    }
    
    /**
     * Generator for TaskSpe objects
     * @param rs with all fields selected
     * @return new object with fields populated.
     */
    private TaskSpecification generateTaskSpecifciation(ResultSet rs) throws SQLException, IntegrationException{
        EventCoordinator ec = getEventCoordinator();
        CodeCoordinator cc = getCodeCoordinator();
        
        TaskSpecification spec = new TaskSpecification();
        
        if(rs != null){
            spec.setTaskSpecID(rs.getInt("taskspecid"));
            spec.setTitle(rs.getString("title"));
            spec.setDescription(rs.getString("description"));
            
            spec.setTaskType(TaskTypeEnum.valueOf(rs.getString("tasktype")));
            spec.setTaskDueDateType(TaskDueDateTypeEnum.valueOf(rs.getString("duetype")));
            spec.setAssignedToRole(TaskAssignedRoleEnum.valueOf(rs.getString("assignmentrolesuggested")));
            
            // set predecessor during configuration phase, but set ID here
            spec.setPredecessorID(rs.getInt("predecessor_predid"));
            spec.setPredecessorRole(TaskSequenceTypeEnum.valueOf(rs.getString("predecessorrole")));
            spec.setPredecessorBufferDaysDefault(rs.getInt("defaultpredecessorbufferdays"));
            
            if(rs.getInt("eventcatcompletion_catid") != 0){
                spec.setEventCategoryForCompletion(ec.getEventCategory(rs.getInt("eventcatcompletion_catid")));
            }
            if(rs.getInt("governingordinance_elementid") != 0){
                spec.setGoverningOrdinance(cc.getCodeElement(rs.getInt("governingordinance_elementid"))); 
            }
            spec.setGeneration(rs.getInt("displayorder"));
            
            spec.setNotificationStatusDefault(rs.getBoolean("notificationdefault"));
            spec.setExpectedCompletionDurationDefault(rs.getInt("expecteddurationmins"));
            spec.setActive(rs.getBoolean("active"));
            
        }
        return spec;
    }
    
    /**
     * Writes new record to the taskspecification table
     * @param spec
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public int insertTaskSpecification(TaskSpecification spec) throws IntegrationException{
        if(spec == null) {
            throw new IntegrationException("Cannot insert null spec");
        }
        int freshSpecID = 0;

        String query = """
                            INSERT INTO public.taskspec(
                            	taskspecid, title, description, 
                                tasktype, duetype, assignmentrolesuggested, 
                                predecessor_predid, predecessorrole, defaultpredecessorbufferdays, 
                                eventcatcompletion_catid, governingordinance_elementid, displayorder, 
                                notificationdefault, expecteddurationmins)
                            	VALUES (DEFAULT, ?, ?, 
                                        ?::tasktypeenum, ?::taskduedatetypeenum, ?::taskassigneeroleenum, 
                                        ?, ?::tasksequencetype, ?, 
                                        ?, ?, ?, 
                                        ?, ?);
                       """; 
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
                stmt.setString(1, spec.getTitle());
                stmt.setString(2, spec.getDescription());
                
                if(spec.getTaskType() != null){
                    stmt.setString(3, spec.getTaskType().name());
                } else {
                    throw new IntegrationException("Cannot insert task spec with null type enum");
                }
                if(spec.getTaskDueDateType()!= null){
                    stmt.setString(4, spec.getTaskDueDateType().name());
                } else {
                    throw new IntegrationException("Cannot insert task spec with null date type enum");
                }
                if(spec.getAssignedToRole() != null){
                    stmt.setString(5, spec.getAssignedToRole().name());
                } else {
                    throw new IntegrationException("Cannot insert task spec with null assignment role enum");
                }
                
                if(spec.getPredecessorID() != 0){
                    stmt.setInt(6, spec.getPredecessorID());
                }else {
                    stmt.setNull(6, java.sql.Types.NULL);
                }
                if(spec.getPredecessorRole() != null){
                    stmt.setString(7, spec.getPredecessorRole().name());
                } else {
                    throw new IntegrationException("Cannot insert task spec with null pred role enum");
                }
                stmt.setInt(8, spec.getPredecessorBufferDaysDefault());
                
                if(spec.getEventCategoryForCompletion() != null){
                    stmt.setInt(9, spec.getEventCategoryForCompletion().getCategoryID());
                } else {
                    stmt.setNull(9, java.sql.Types.NULL);
                }
                if(spec.getGoverningOrdinance() != null){
                    stmt.setInt(10, spec.getGoverningOrdinance().getElementID());
                } else {
                    stmt.setNull(10, java.sql.Types.NULL);
                }
                        
                stmt.setInt(11, spec.getGeneration());
                stmt.setBoolean(12, spec.isNotificationStatusDefault());
                stmt.setInt(13, spec.getExpectedCompletionDurationDefault());
                
            stmt.execute();

            String retrievalQuery = "SELECT currval('taskspecid_seq');";
            stmt = con.prepareStatement(retrievalQuery);

            rs = stmt.executeQuery();
            while (rs.next()) {
                freshSpecID = rs.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert task spec into system", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return freshSpecID;
    }
    
    /**
     * Writes a record to the m:m table for task specs and chains
     * @param spec 
     */
    public void connectTaskSpecToTaskChain(TaskSpecification spec) throws IntegrationException{
        if(spec == null || spec.getTaskSpecID() == 0 || spec.getChainParentID() == 0){
            throw new IntegrationException("Cannot link spec to chain wiht null spec or ids of zero for spec or parent chain");
        }
        String query = """
                            INSERT INTO public.taskspectaskchain(
                            	taskspec_specid, taskchain_chainid, active)
                            	VALUES (?, ?, TRUE);
                       """; 
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
              
            stmt.setInt(1, spec.getTaskSpecID());
            stmt.setInt(2, spec.getChainParentID());
            
            stmt.execute();

           

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert task spec into system", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }
        
    }
    
    
    /**
     * Updates a record of the taskspecification table
     * @param spec 
     */
    public void updateTaskSpecification(TaskSpecification spec) throws IntegrationException{
        String query =  """
                            UPDATE public.taskspec
                            	SET title=?, description=?, 
                                    tasktype=?::tasktypeenum, duetype=?::taskduedatetypeenum, assignmentrolesuggested=?::taskassigneeroleenum, 
                                    predecessor_predid=?, predecessorrole=?::tasksequencetype, defaultpredecessorbufferdays=?, 
                                    eventcatcompletion_catid=?, governingordinance_elementid=?, displayorder=?, 
                                    notificationdefault=?, expecteddurationmins=?
                            	WHERE taskspecid=?;
                        """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            stmt.setString(1, spec.getTitle());
            stmt.setString(2, spec.getDescription());

            if(spec.getTaskType() != null){
                stmt.setString(3, spec.getTaskType().name());
            } else {
                throw new IntegrationException("Cannot insert task spec with null type enum");
            }
            if(spec.getTaskDueDateType()!= null){
                stmt.setString(4, spec.getTaskDueDateType().name());
            } else {
                throw new IntegrationException("Cannot insert task spec with null date type enum");
            }
            if(spec.getAssignedToRole() != null){
                stmt.setString(5, spec.getAssignedToRole().name());
            } else {
                throw new IntegrationException("Cannot insert task spec with null assignment role enum");
            }

            if(spec.getPredecessorID() != 0){
                stmt.setInt(6, spec.getPredecessorID());
            }
            if(spec.getPredecessorRole() != null){
                stmt.setString(7, spec.getPredecessorRole().name());
            } else {
                throw new IntegrationException("Cannot insert task spec with null pred role enum");
            }
            stmt.setInt(8, spec.getPredecessorBufferDaysDefault());

            if(spec.getEventCategoryForCompletion() != null){
                stmt.setInt(9, spec.getEventCategoryForCompletion().getCategoryID());
            } else {
                stmt.setNull(9, java.sql.Types.NULL);
            }
            if(spec.getGoverningOrdinance() != null){
                stmt.setInt(10, spec.getGoverningOrdinance().getElementID());
            } else {
                stmt.setNull(10, java.sql.Types.NULL);
            }

            stmt.setInt(11, spec.getGeneration());
            stmt.setBoolean(12, spec.isNotificationStatusDefault());
            stmt.setInt(13, spec.getExpectedCompletionDurationDefault());
            
            stmt.setInt(14, spec.getTaskSpecID());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update task specification ", ex);

        } finally {
            releasePostgresConnection(con, stmt);
        } 
    }

    /**
     * Toggles active to false on the given TaskSpecification object
     * @param spec 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void deactivateTaskSpecification(TaskSpecification spec) throws IntegrationException{
         String query =  """
                            UPDATE public.taskspec
                            	SET active=FALSE
                            	WHERE taskspecid=?;
                        
                        """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            stmt.setInt(1, spec.getTaskSpecID());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to deactivate task specification ", ex);

        } finally {
            releasePostgresConnection(con, stmt);
        } 
        
    }
    
    
    /*******************************************************************
     * ************ TASK ASSIGNED *************************************
     *******************************************************************
     */
    
    /**
     * getter for TaskAssigned objects
     * 
     * @param assignedID
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public TaskAssigned getTaskAssigned(int assignedID) throws IntegrationException, BObStatusException{
         if(assignedID == 0){
            throw new IntegrationException("cannot get task assigned with ID == 0");
        }
         
        String query = """
                            SELECT  assignmentid, taskspec_specid, occperiod_periodid, displayorderactual, 
                                    startby, predecessorbufferdays, startactualts, 
                                    dueby, assignedtorole_mappingid, remindersenabled, 
                                    completedts, completiondateofrecord, completioncertifiedby_umapid, notes, 
                                    nullifiedts, nullifiedby_umapid, 
                                    deactivatedts, deactivatedby_umapid
                            	FROM public.taskassigned WHERE assignmentid=?;
                       """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        TaskAssigned ta = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, assignedID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                ta = generateTaskAssigned(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot generate task assigned", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return ta;
    }
    
    /**
     * Generator for TaskAssigned objects
     * @param rs
     * @return 
     */
    private TaskAssigned generateTaskAssigned(ResultSet rs) throws SQLException, BObStatusException, IntegrationException{
        TaskCoordinator tc = getTaskCoordinator();
        UserCoordinator uc = getUserCoordinator();
        
        TaskAssigned ta = new TaskAssigned(tc.getTaskSpecification(rs.getInt("taskspec_specid")));
        
        ta.setAssignmentID(rs.getInt("assignmentid"));
        ta.setOccPeriodID(rs.getInt("occperiod_periodid"));
        ta.setDisplayOrderActual(rs.getInt("dispalorderactual"));
        
        if(rs.getTimestamp("startby") != null){
            ta.setDateStartBy(rs.getTimestamp("startby").toLocalDateTime());
        }
        ta.setPredecessorBufferActual(rs.getInt("predecessorbufferdays"));
        if(rs.getTimestamp("startactualts") != null){
            ta.setDateStartActual(rs.getTimestamp("startactualts").toLocalDateTime());
        }
        if(rs.getTimestamp("dueby") != null){
            ta.setDateDueBy(rs.getTimestamp("dueby").toLocalDateTime());
        }
        if(rs.getInt("assignedtorole_mappingid") != 0){
            ta.setAssignedTo(tc.getTaskAssignee(rs.getInt("assignedtorole_mappingid")));
        }
        ta.setRemindersEnabled(rs.getBoolean("remindersenabled"));
        if(rs.getTimestamp("completedts") != null){
            ta.setDateCompletionTS(rs.getTimestamp("completedts").toLocalDateTime());
        }
        if(rs.getTimestamp("completiondateofrecord") != null){
            ta.setDateCompletionDateOfRecord(rs.getTimestamp("completiondateofrecord").toLocalDateTime());
        }
        if(rs.getInt("completioncertifiedby_umapid") != 0){
            ta.setCompletionCertificationByUMAP(uc.auth_getUMAPNotValidated(rs.getInt("completioncertifiedby_umapid")));
        }
        if(rs.getTimestamp("nullifiedts") != null){
            ta.setNullifiedTS(rs.getTimestamp("nullifiedts").toLocalDateTime());
        }
        if(rs.getInt("nullifiedby_umapid") != 0){
            ta.setNullifiedByUMAP(uc.auth_getUMAPNotValidated(rs.getInt("nullifiedby_umapid")));
        }
        if(rs.getTimestamp("deactivatedts") != null){
            ta.setNullifiedTS(rs.getTimestamp("deactivatedts").toLocalDateTime());
        }
        if(rs.getInt("deactivatedby_umapid") != 0){
            ta.setNullifiedByUMAP(uc.auth_getUMAPNotValidated(rs.getInt("deactivatedby_umapid")));
        }
        
        return ta;
        
    }
    
    /**
     * Organizes an ID list of task assignments by occ period
     * @param period
     * @return 
     */
    public List<Integer> getTaskAssignedListByOccPeriod(OccPeriod period) throws IntegrationException{
        if(period == null){
            throw new IntegrationException("cannot get task assigned list by period");
        }
        List<Integer> idl = new ArrayList<>();
        String query = """
                            SELECT  assignmentid
                            	FROM public.taskassigned WHERE occperiod_periodid=? AND deactivatedts IS NULL;
                       """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, period.getPeriodID());
            rs = stmt.executeQuery();
            while (rs.next()) {
                idl.add(rs.getInt("assignmentid"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot generate task assigned list", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return idl;
    }
    
    /**
     * inserts a new record to the taskassigned table
     * @param ta
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public int insertTaskAssigned(TaskAssigned ta) throws IntegrationException{
         if(ta == null) {
            throw new IntegrationException("Cannot insert null spec");
        }
        int freshTAID = 0;

        String query = """
                            INSERT INTO public.taskassigned(
                                        assignmentid, occperiod_periodid, displayorderactual, 
                                        startby, predecessorbufferdays, startactualts, 
                                        dueby, assignedtorole_mappingid, remindersenabled, 
                                        completedts, completioncertifiedby_umapid, notes, 
                                        nullifiedts, nullifiedby_umapid, deactivatedts, 
                                        deactivatedby_umapid, taskspec_specid, completiondateofrecord)
                            	VALUES (DEFAULT, ?, ?, 
                                        ?, ?, ?, 
                                        ?, ?, ?, 
                                        ?, ?, ?, 
                                        NULL, NULL, NULL, 
                                        NULL, ?, ?);
                       """; 
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);

            if(ta.getOccPeriodID() != 0){
                stmt.setInt(1, ta.getOccPeriodID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            stmt.setInt(2, ta.getDisplayOrderActual());
            
            if(ta.getDateStartBy() != null){
                stmt.setTimestamp(3, java.sql.Timestamp.valueOf(ta.getDateStartBy()));
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            stmt.setInt(4, ta.getPredecessorBufferActual());
            if(ta.getDateStartActual() != null){
                stmt.setTimestamp(5, java.sql.Timestamp.valueOf(ta.getDateStartActual()));
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            
            if(ta.getDateDueBy() != null){
                stmt.setTimestamp(6, java.sql.Timestamp.valueOf(ta.getDateDueBy()));
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            
            if(ta.getAssignedTo() != null){
                stmt.setInt(7, ta.getAssignedTo().getMappingID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            stmt.setBoolean(8, ta.isRemindersEnabled());
            
            if(ta.getDateCompletionTS() != null){
                stmt.setTimestamp(9, java.sql.Timestamp.valueOf(ta.getDateCompletionTS()));
            } else {
                stmt.setNull(9, java.sql.Types.NULL);
            }
            if(ta.getCompletionCertificationByUMAP() != null){
                stmt.setInt(10, ta.getCompletionCertificationByUMAP().getUserMuniAuthPeriodID());  
            } else { 
                stmt.setNull(10, java.sql.Types.NULL);
            }
            stmt.setString(11, ta.getNotes());
            
            if(ta.getTaskSpecID() != 0){
                stmt.setInt(12, ta.getTaskSpecID());
            } else {
                throw new IntegrationException("Cannot insert task assigned with parent spec ID of 0");
            }
            if(ta.getDateCompletionDateOfRecord()!= null){
                stmt.setTimestamp(13, java.sql.Timestamp.valueOf(ta.getDateCompletionDateOfRecord()));
            } else {
                stmt.setNull(13, java.sql.Types.NULL);
            }
            stmt.execute();

            String retrievalQuery = "SELECT currval('taskassignedid_seq');";
            stmt = con.prepareStatement(retrievalQuery);

            rs = stmt.executeQuery();
            while (rs.next()) {
                freshTAID = rs.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert task assigned into system", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return freshTAID;
        
    }
    
    
    /**
     * Updates a record in the taskassigned table
     * @param ta 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void updateTaskAssigned(TaskAssigned ta) throws IntegrationException{
        String query =  """
                           UPDATE public.taskassigned
                           	SET occperiod_periodid=?, displayorderactual=?, startby=?, 
                                    predecessorbufferdays=?, startactualts=?, dueby=?, 
                                    assignedtorole_mappingid=?, remindersenabled=?, completedts=?, 
                                    completioncertifiedby_umapid=?, notes=?, nullifiedts=?, 
                                    nullifiedby_umapid=?, taskspec_specid=?, completiondateofrecord=?
                           	WHERE assignmentid=?,;
                        """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            if(ta.getOccPeriodID() != 0){
                stmt.setInt(1, ta.getOccPeriodID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            stmt.setInt(2, ta.getDisplayOrderActual());
            if(ta.getDateStartBy() != null){
                stmt.setTimestamp(3, java.sql.Timestamp.valueOf(ta.getDateStartBy()));
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            
            stmt.setInt(4, ta.getPredecessorBufferActual());
            if(ta.getDateStartActual() != null){
                stmt.setTimestamp(5, java.sql.Timestamp.valueOf(ta.getDateStartActual()));
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            if(ta.getDateDueBy() != null){
                stmt.setTimestamp(6, java.sql.Timestamp.valueOf(ta.getDateDueBy()));
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            
            if(ta.getAssignedTo() != null){
                stmt.setInt(7, ta.getAssignedTo().getMappingID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            stmt.setBoolean(8, ta.isRemindersEnabled());
            if(ta.getDateCompletionTS() != null){
                stmt.setTimestamp(9, java.sql.Timestamp.valueOf(ta.getDateCompletionTS()));
            } else {
                stmt.setNull(9, java.sql.Types.NULL);
            }
            
            if(ta.getCompletionCertificationByUMAP() != null){
                stmt.setInt(10, ta.getCompletionCertificationByUMAP().getUserMuniAuthPeriodID());  
            } else { 
                stmt.setNull(10, java.sql.Types.NULL);
            }
            stmt.setString(11, ta.getNotes());
            
             if(ta.getNullifiedTS() != null){
                stmt.setTimestamp(12, java.sql.Timestamp.valueOf(ta.getNullifiedTS()));
            } else {
                stmt.setNull(12, java.sql.Types.NULL);
            }
            
            if(ta.getNullifiedByUMAP() != null){
                stmt.setInt(13, ta.getNullifiedByUMAP().getUserMuniAuthPeriodID());  
            } else { 
                stmt.setNull(13, java.sql.Types.NULL);
            }
            
            if(ta.getTaskSpecID() != 0){
                stmt.setInt(14, ta.getTaskSpecID());
            } else {
                throw new IntegrationException("Cannot update task assigned with parent spec ID of 0");
            }
            if(ta.getDateCompletionDateOfRecord()!= null){
                stmt.setTimestamp(15, java.sql.Timestamp.valueOf(ta.getDateCompletionDateOfRecord()));
            } else {
                stmt.setNull(15, java.sql.Types.NULL);
            }
          
            stmt.setInt(16, ta.getAssignmentID());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update task assigned ", ex);

        } finally {
            releasePostgresConnection(con, stmt);
        } 
    }
    
    /**
     * Deactivates the record matching the given TaskAssigned object
     * @param ta 
     */
    public void deactivateTaskAssigned(TaskAssigned ta) throws IntegrationException{
        if(ta == null || ta.getDeativatedByUMAP() == null){
            throw new IntegrationException("Cannot deac task assigne with null task or deactivator UMAP");
        } 
        String query =  """
                            UPDATE public.taskassigned
                            	SET deactivatedts=now(), deactivatedby_umapid=?
                            	WHERE assignmentid=?;
                        
                        """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            stmt.setInt(1, ta.getDeativatedByUMAP().getUserMuniAuthPeriodID());
            stmt.setInt(2, ta.getAssignmentID());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to deactivate task assigned", ex);

        } finally {
            releasePostgresConnection(con, stmt);
        } 
    }
    
    /*******************************************************************
     * ************ TASK LINKAGES **************************************
     *******************************************************************
     */
      
    /**
     * Getter for task chains, metadata only
     * @param linkageID
     * @param ua
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public TaskLinkage getTaskLinkage(int linkageID, UserAuthorized ua) throws IntegrationException, BObStatusException, BlobException{
        if(linkageID == 0){
            throw new IntegrationException("cannot get task link with ID == 0");
        }
         
        String query = """
                           SELECT   linkid, taskassigned_taskid, occapplication_appid, 
                                    event_eventid, inspection_inspectionid, permit_permitid, 
                                    document_photodocid, cecase_caseid, 
                                    human_humanid, deactivatedts, deactivatedby_umapid
                            FROM public.taskassignedlinkage 
                            WHERE linkid=?;
                            
                       """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        TaskLinkage linkage = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, linkageID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                linkage = generateTaskLinkage(rs, ua);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot generate task assigned", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return linkage;
        
    }
    
    /**
     * Internal generator for task link objects
     * @param rs
     * @return 
     */
    private TaskLinkage generateTaskLinkage(ResultSet rs, UserAuthorized ua) throws SQLException, IntegrationException, BObStatusException, BlobException{
        TaskLinkage linkage = new TaskLinkage();
        EventCoordinator ec = getEventCoordinator();
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        OccupancyCoordinator oc = getOccupancyCoordinator();
        PaymentCoordinator pc = getPaymentCoordinator();
        BlobCoordinator bc = getBlobCoordinator();
        CaseCoordinator cc =getCaseCoordinator();
        PersonCoordinator persc = getPersonCoordinator();
        UserCoordinator uc =getUserCoordinator();
        
        // NO occ applications yet as of SEPT 2023
        
        if(rs.getInt("event_eventid") != 0){
            linkage.setEvent(ec.getEvent(rs.getInt("event_eventid")));
        }
        if(rs.getInt("inspection_inspectionid") != 0){
            linkage.setFieldInspection(oic.getOccInspectionLight(rs.getInt("inspection_inspectionid")));
        }
        if(rs.getInt("permit_permitid") != 0){
            linkage.setPermit(oc.getOccPermit(rs.getInt("permit_permitid"), ua));
        }
        // removed during midnight push for task alpha 25SEP23
//        if(rs.getInt("payment_paymentid") != 0){
//            linkage.setPayment(pc.getPayment(rs.getInt("payment_paymentid")));
//        }
        if(rs.getInt("document_photodocid") != 0){
            linkage.setBlob(bc.getBlobLight(rs.getInt("document_photodocid")));
        }
        if(rs.getInt("cecase_caseid") != 0){
            linkage.setCeCase(cc.cecase_getCECase(rs.getInt("cecase_caseid"), ua));
        }
        if(rs.getInt("human_humanid") != 0){
            linkage.setHuman(persc.getHuman(rs.getInt("human_humanid")));
        }
        if(rs.getTimestamp("deactivatedts") != null){
            linkage.setDeactivatedTS(rs.getTimestamp("deactivatedts").toLocalDateTime());
        }
        if(rs.getInt("deactivatedby_umapid") != 0){
            linkage.setDeactivatedBy(uc.auth_getUMAPNotValidated(rs.getInt("deactivatedby_umapid")));
        }
        
        return linkage;
    }
    
    /**
     * inserts a new record into the tasklinkage table
     * @param link
     * @return 
     */
    public int insertTaskLinkage(TaskLinkage link) throws IntegrationException{
        if(link == null) {
            throw new IntegrationException("Cannot insert null link");
        }
        int freshLinkID = 0;

        String query = """
                          INSERT INTO public.taskassignedlinkage(
                                        linkid, taskassigned_taskid, occapplication_appid, 
                                        event_eventid, inspection_inspectionid, permit_permitid, 
                                        document_photodocid, cecase_caseid, 
                                        human_humanid, deactivatedts, deactivatedby_umapid)
                          	VALUES (DEFAULT, ?, NULL, 
                                        ?, ?, ?, 
                                        ?, ?, 
                                        ?, NULL, NULL);
                       """; 
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);

            if(link.getTaskAssignedID() != 0){
                stmt.setInt(1, link.getTaskAssignedID());
            } else {
                throw new IntegrationException("cannot write task link with ID of 0 for task ID");
            }
            
            if(link.getEvent() != null){
                stmt.setInt(2, link.getEvent().getEventID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);  
            }
            if(link.getFieldInspection() != null){
                stmt.setInt(3, link.getFieldInspection().getInspectionID() );
            } else {
                stmt.setNull(3, java.sql.Types.NULL);  
            }
            if(link.getPermit() != null){
                stmt.setInt(4, link.getPermit().getPermitID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);  
            }
            
            if(link.getPayment() != null){
                stmt.setInt(5, link.getPayment().getPaymentID());
            } else {
                stmt.setNull(5, java.sql.Types.NULL);  
            }
            if(link.getBlob() != null){
                stmt.setInt(6, link.getBlob().getPhotoDocID());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);  
            }
            if(link.getCeCase() != null){
                stmt.setInt(7, link.getCeCase().getCaseID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);  
            }
            
            if(link.getHuman() != null){
                stmt.setInt(8, link.getHuman().getHumanID());
            } else {
                stmt.setNull(8, java.sql.Types.NULL);  
            }
          
            stmt.execute();

            String retrievalQuery = "SELECT currval('taskassignedlinkid_seq');";
            stmt = con.prepareStatement(retrievalQuery);

            rs = stmt.executeQuery();
            while (rs.next()) {
                freshLinkID = rs.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert task link into system", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return freshLinkID;
    }
    
    
       /**
     * Deactivates the record matching the given TaskAssigned object
     * @param tl 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void deactivateTaskLink(TaskLinkage tl) throws IntegrationException {
        if(tl == null || tl.getLinkID() == 0 || tl.getDeactivatedBy() == null){
            throw new IntegrationException("Cannot deac task link with null link or link id == 0 or deac umap is null");
        } 
        String query =  """
                            UPDATE public.tasklinkages
                            	SET deactivatedts=now(), deactivatedby_umapid=?
                            	WHERE linkid=?;
                        
                        """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            stmt.setInt(1, tl.getDeactivatedBy().getUserMuniAuthPeriodID());
            stmt.setInt(2, tl.getLinkID());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to deactivate task assigned", ex);

        } finally {
            releasePostgresConnection(con, stmt);
        } 
    }
     
    /*******************************************************************
     * ************ TASK CHAINS  ***************************************
     *******************************************************************
     */
    
    /**
     * Getter for task chains, metadata only
     * @param chainID
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public TaskChain getTaskChain(int chainID) throws IntegrationException{
        if(chainID == 0){
            throw new IntegrationException("cannot get task chain with ID == 0");
        }
         
        String query = """
                           SELECT   chainid, title, description, 
                                    municipality_municode, governingordinance_elementid, 
                                    active
                            	FROM public.taskchain WHERE chainid=?; 
                            
                       """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        TaskChain chain = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, chainID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                chain = generateTaskChain(rs);
            }
        } catch (SQLException | BObStatusException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot generate task assigned", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return chain;
        
    }

    /**
     * generator method for task chains--metadata only; coordinator will
     * populate with tasks
     * @param rs
     * @return 
     */
    private TaskChain generateTaskChain(ResultSet rs) throws SQLException, IntegrationException, BObStatusException{
        MunicipalityCoordinator mc = getMuniCoordinator();
        CodeCoordinator cc = getCodeCoordinator();
        
        TaskChain chain = new TaskChain();
        chain.setChainID(rs.getInt("chainid"));
        chain.setTitle(rs.getString("title"));
        chain.setDescription(rs.getString("description"));
        
        if(rs.getInt("municipality_municode") != 0){
            chain.setMuni(mc.getMuni(rs.getInt("municipality_municode")));
        } 
       
        if(rs.getInt("governingordinance_elementid") != 0){
            chain.setGoverningOrdinance(cc.getCodeElement(rs.getInt("governingordinance_elementid")));
        }
        chain.setActive(rs.getBoolean("active"));
            
        return chain;
    }
    
    /**
     * Extracts the IDs of all active task chains from the DB
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public List<Integer> getTaskChainIDListComplete() throws IntegrationException{

        List<Integer> idl = new ArrayList<>();
        String query = """
                           SELECT   chainid
                            	FROM public.taskchain; 
                            
                       """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();
            while (rs.next()) {
                idl.add(rs.getInt("chainid"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot generate task chain list", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return idl;
        
    }
  
    /**
     * Extracts the IDs of all TaskSpecs in a given chain
     * @param chain
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public List<Integer> getTaskSpecifcationsByChain(TaskChain chain) throws IntegrationException{
        List<Integer> specIDList = new ArrayList<>();
        if(chain == null){
            throw new IntegrationException("cannot get specs by chain with null chain"); 
        }
         
        String query = """
                          SELECT taskspec_specid
                          	FROM public.taskspectaskchain WHERE taskchain_chainid=? AND active=TRUE;
                            
                       """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, chain.getChainID());
            rs = stmt.executeQuery();
            while (rs.next()) {
                specIDList.add(rs.getInt("taskspec_specid"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot generate task assigned", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return specIDList;
    }
    
    /**
     * entry point for writing new records to the task chain table
     * @param chain with fields to write
     * @return the ID of the freshly inserted chain
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public int insertTaskChain(TaskChain chain) throws IntegrationException{
         if(chain == null) {
            throw new IntegrationException("Cannot insert null chain");
        }
        int freshChainID = 0;

        String query = """
                         INSERT INTO public.taskchain(
                                        chainid, title, description, 
                                        municipality_municode, governingordinance_elementid, active)
                           	VALUES (DEFAULT, ?, ?, 
                                        ?, ?, TRUE);  
                       """; 
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            
            stmt.setString(1, chain.getTitle());
            stmt.setString(2, chain.getDescription());
            if(chain.getMuni() != null){
                stmt.setInt(3, chain.getMuni().getMuniCode());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            
            if(chain.getGoverningOrdinance() != null){
                stmt.setInt(4, chain.getGoverningOrdinance().getElementID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            
            stmt.execute();

            String retrievalQuery = "SELECT currval('taskchainid_seq');";
            stmt = con.prepareStatement(retrievalQuery);

            rs = stmt.executeQuery();
            while (rs.next()) {
                freshChainID = rs.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert task chain into system", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return freshChainID;
    }
    
    /**
     * Updates the values of the given TaskChain 
     * @param chain 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void updateTaskChain(TaskChain chain) throws IntegrationException{
        if(chain == null) {
            throw new IntegrationException("Cannot update null chain");
        }

        String query = """
                        UPDATE public.taskchain
                        	SET title=?, description=?, municipality_municode=?, governingordinance_elementid=?
                        	WHERE chainid=?;
                       """; 
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            
            stmt.setString(1, chain.getTitle());
            stmt.setString(2, chain.getDescription());
            if(chain.getMuni() != null){
                stmt.setInt(3, chain.getMuni().getMuniCode());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            
            if(chain.getGoverningOrdinance() != null){
                stmt.setInt(4, chain.getGoverningOrdinance().getElementID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            
            stmt.setInt(5, chain.getChainID());
            
            
            stmt.execute();


        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot update task chain in system", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
    }
    
    /**
     * toggles active to false on the given TaskChain
     * @param chain 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void deactivateTaskChain(TaskChain chain) throws IntegrationException{
        if(chain == null) {
            throw new IntegrationException("Cannot update null chain");  
        }

        String query = """
                        UPDATE public.taskchain
                        	SET active=FALSE
                        	WHERE chainid=?;
                       """; 
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, chain.getChainID());
            
            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot deactivate task chain", ex);  
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
    }
    
    
    /**
     * Extracts a record from the taskrolemapping table
     * 
     * @param assignmentID
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public TaskAssignee getTaskAssignee(int assignmentID) throws IntegrationException, BObStatusException{
        TaskCoordinator tc = getTaskCoordinator();
        TaskAssignee assignee = null;
        String sql = """
                     SELECT rolemappingid, tasklrole, notes, active, user_umapid, municipality_municode
                     	FROM public.taskrolemapping WHERE rolemappingid=?
                    """;
       
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                assignee = generateTaskAssignee(rs);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot get a task assignee", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return assignee;
    }
    
    /**
     * Internal generator organ for task assignments
     * @param rs
     * @return 
     */
    private TaskAssignee generateTaskAssignee(ResultSet rs) throws SQLException, BObStatusException, IntegrationException{
        MunicipalityCoordinator mc = getMuniCoordinator();
        UserCoordinator uc = getUserCoordinator();
        
        TaskAssignee nee = new TaskAssignee();
        
        nee.setMappingID(rs.getInt("rolemappingid"));
        nee.setRole(TaskAssignedRoleEnum.valueOf(rs.getString("taskrole")));
        nee.setUser(uc.auth_getUMAPNotValidated(rs.getInt("user_umapid")));
        
        nee.setActive(rs.getBoolean("active"));
        if(rs.getInt("municipality_municode") != 0){
            nee.setMuni(mc.getMuni(rs.getInt("municipality_municode")));
        }
        nee.setNotes(rs.getString("notes"));
        return nee;
    }
    
    
    /**
     * extracts task assignee candidates by muni
     * @param muni, returning all assignments if null
     * @return 
     */
    public List<TaskAssignee> getTaskAssigneesByMuni(Municipality muni) throws IntegrationException, BObStatusException{
        TaskCoordinator tc = getTaskCoordinator();
        List<TaskAssignee> assigneeList = new ArrayList<>();
       
        StringBuilder sb = new StringBuilder();
        sb.append("      SELECT rolemappingid FROM public.taskrolemapping WHERE active=TRUE ");
        if(muni != null){
            sb.append(" AND municipality_municode=?");
        }
        sb.append(";");
        
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sb.toString());
            if(muni != null){
                stmt.setInt(1, muni.getMuniCode());
            }
                
            rs = stmt.executeQuery();
            while (rs.next()) {
                assigneeList.add(tc.getTaskAssignee(rs.getInt("rolemappingid")));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot generate task assigned", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return assigneeList;
    }
    
    
    /**
     * Writes a record to the taskassigned
     * @param assignee 
     * @return id of the fresh assignment  
     * @throws com.tcvcog.tcvce.domain.IntegrationException  
     */
    public int connectUserToRole(TaskAssignee assignee) throws IntegrationException{
        if(assignee == null || assignee.getRole() == null || assignee.getUser() == null || assignee.getMuni() == null) {
            throw new IntegrationException("Cannot insert assignee role with null input assignee or null role or user members");
        }
        int frshMapID = 0;

        String query = """
                            INSERT INTO public.taskrolemapping(
                            	rolemappingid, tasklrole, user_umapid, municipality_municode, notes, active)
                            	VALUES (DEFAULT, ?::taskassigneeroleenum, ?, ?, ?, TRUE);
                       """; 
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
                
            stmt.setString(1, assignee.getRole().name());
            stmt.setInt(2, assignee.getUser().getUserMuniAuthPeriodID());
            stmt.setInt(3, assignee.getMuni().getMuniCode());
            stmt.setString(4, assignee.getNotes());
                      
            stmt.execute();

            String retrievalQuery = "SELECT currval('taskrolemapping_seq');";
            stmt = con.prepareStatement(retrievalQuery);

            rs = stmt.executeQuery();
            while (rs.next()) {
                frshMapID = rs.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert user mapping into system", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return frshMapID;
    }
    
    /**
     * Deactivates a given user-role mapping
     * @param assignee 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void removeUserRoleConection(TaskAssignee assignee) throws IntegrationException{
        if(assignee == null) {
            throw new IntegrationException("Cannot deac null assignment");  
        }

        String query = """
                        UPDATE public.taskrolemapping
                        	SET active=FALSE
                        	WHERE rolemappingid=?;
                       """; 
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, assignee.getMappingID());
            
            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot deactivate assignment", ex);  
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
    }
}
