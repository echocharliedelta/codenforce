/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.integration;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheClient;
import com.tcvcog.tcvce.coordinators.MunicipalityCoordinator;
import com.tcvcog.tcvce.coordinators.OccInspectionCoordinator;
import com.tcvcog.tcvce.coordinators.OccupancyCoordinator;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.occupancy.OccChecklistTemplate;
import com.tcvcog.tcvce.entities.occupancy.OccSpaceElement;
import com.tcvcog.tcvce.entities.occupancy.OccSpaceType;
import com.tcvcog.tcvce.entities.occupancy.OccSpaceTypeChecklistified;
import com.tcvcog.tcvce.integration.CodeIntegrator;
import com.tcvcog.tcvce.integration.MunicipalityIntegrator;
import com.tcvcog.tcvce.integration.SystemIntegrator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import com.tcvcog.tcvce.domain.DatabaseFetchRuntimeException;
import jakarta.annotation.PostConstruct;

/**
 *  Container for methods used in creating an occupancy checklist
 * which is implemented during an occupancy inspection 
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */
public class        OccChecklistIntegrator 
        extends     BackingBeanUtils 
        implements  IFaceCacheClient{
    
    /**
     * Creates a new instance of OccChecklistIntegrator
     */
    public OccChecklistIntegrator() {
        
        
    }
    
    @PostConstruct
    public void initBean(){
        registerClientWithManager();
    }
    
    @Override
    public void registerClientWithManager() {
        getOccInspectionCacheManager().registerCacheClient(this);
    }


    /**
     * Extracts checklist templates by municipality
     * @param muni for record filtering; pass null to extract ALL checklists for management purposes, 
     * including deactivated ones
     * @param includeInactive
     * @return an instance of List, perhaps with checklists
     * @throws IntegrationException 
     */
    public List<Integer> getOccChecklistTemplateList(Municipality muni, boolean includeInactive) throws IntegrationException {
        List<Integer> checklistIDList = new ArrayList<>();
        StringBuilder sb = new StringBuilder(); 
        sb.append("SELECT checklistid FROM public.occchecklist WHERE checklistid IS NOT NULL ");
        if(muni != null){
            sb.append("AND muni_municode=? ");
        }
        if(!includeInactive){
            sb.append("AND deactivatedts IS NULL ");
        }
        sb.append(";"); // get ALL checklist templates
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(sb.toString());
            if(muni != null){
                stmt.setInt(1, muni.getMuniCode()); 
            } else {
                // no need to inject
            }
            rs = stmt.executeQuery();
            while (rs.next()) {
                checklistIDList.add(rs.getInt("checklistid"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot retrieve occinspectionlist", ex);
        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return checklistIDList;
    }
    
    /**
     * Connects an OccSpaceTypeChecklistified to a List of CodeElements. 
     * Note that the 
     *
     * @param ose
     * @throws IntegrationException
     */
    public void attachCodeElementsToSpaceTypeInChecklist(OccSpaceElement ose) throws IntegrationException {
        String sql =    """
                        INSERT INTO public.occchecklistspacetypeelement(
                                    spaceelementid, codesetelement_seteleid, required, checklistspacetype_typeid, lastupdatedts )
                            VALUES (DEFAULT, ?, ?, ?, now());
                        """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            // for each CodeElement in the list passed into the method, make an entry in the spaceelement table
                stmt = con.prepareStatement(sql);
                
                stmt.setInt(1, ose.getCodeSetElementID());
                stmt.setBoolean(2, ose.isRequiredForInspection());
                stmt.setInt(3, ose.getParentSpaceTypeID());
                
                stmt.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("", ex);
        } finally {
             if (con != null) { try { con.close(); } catch (SQLException e) { /* ignored */} }
             if (stmt != null) { try { stmt.close(); } catch (SQLException e) { /* ignored */} }
             if (rs != null) { try { rs.close(); } catch (SQLException ex) { /* ignored */ } }
        } 
    }

   

    /**
     * Builds a list of OccSpaceType IDs from records in the occchecklistspacetype using a checklist ID
     * table
     * @param checklistID
     * @return the list of fully-baked objects
     * @throws IntegrationException 
     */
    public List<Integer> getOccSpaceTypeChecklistifiedIDListByChecklist(int checklistID) throws IntegrationException {
        String query = "SELECT checklistspacetypeid FROM public.occchecklistspacetype WHERE checklist_id=? AND occchecklistspacetype.deactivatedts IS NULL;";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        List<Integer> ostidl = new ArrayList<>();
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, checklistID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                ostidl.add(rs.getInt("checklistspacetypeid"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("getOccSpaceTypeChecklistifiedIDListByChecklist | Error building list of space type IDs by checklist!", ex);
        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return ostidl;
    }

    /**
     * Removes a space type from a checklist
     * 
     * @param ostchk
     * @throws IntegrationException 
     */
    public void deactivateOccSpaceTypeChecklistified(OccSpaceTypeChecklistified ostchk) throws IntegrationException {
        String query = "UPDATE public.occchecklistspacetype SET deactivatedts=now(), lastupdatedts=now()  " 
                + " WHERE checklistspacetypeid = ?;"; 
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, ostchk.getChecklistSpaceTypeID());
            stmt.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot delete space type checklistified--probably because another" + "part of the database has a reference item.", ex);
        } finally {
             releasePostgresConnection(con, stmt);
        } 
    }
    /**
     * Removes a space type from a checklist; a very rare case of actual record deletion!
     * 
     * @param spaceType
     * @throws IntegrationException 
     */
    public void deleteSpaceType(OccSpaceType spaceType) throws IntegrationException {
        String query = "DELETE FROM public.occspacetype\n" 
                + " WHERE spacetypeid= ?;";
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, spaceType.getSpaceTypeID());
            stmt.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot delete space type--probably because another" + "part of the database has a reference item.", ex);
        } finally {
             releasePostgresConnection(con, stmt);
        } 
    }
    
    /**
     * Cache backed getter for templates only; no guts
     * @param checklistID
     * @return
     * @throws IntegrationException 
     */
    public OccChecklistTemplate getChecklistTemplate(int checklistID) throws IntegrationException {
         if(checklistID == 0){
             throw new IntegrationException("cannot get checklist template with an ID of zero");
         }
         if(getOccInspectionCacheManager().isCachingEnabled()){
             return getOccInspectionCacheManager().getCacheInspectionChecklist().get(checklistID, k -> fetchChecklistTemplate(checklistID));
         } else {
             return fetchChecklistTemplate(checklistID);
         }
    }

    /**
     * Retrieves and builds a bare bones checklist, with title and such, but no
     * space types or elements--see the config methods on the OccInspectionCoordinator
     * @param checklistID
     * @return
     * @throws DatabaseFetchRuntimeException
     */
    public OccChecklistTemplate fetchChecklistTemplate(int checklistID) throws DatabaseFetchRuntimeException {
        String checklistTableSELECT = """
                                       SELECT   checklistid, title, description, 
                                                muni_municode, 
                                                createdts, notes, createdby_umapid, 
                                                lastupdatedts, lastupdatedby_umapid, deactivatedts, 
                                                deactivatedby_umapid, defaultinspectioncause
                                       FROM     public.occchecklist
                                       WHERE checklistid = ?;""";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        OccChecklistTemplate template = null;
        try {
            stmt = con.prepareStatement(checklistTableSELECT);
            stmt.setInt(1, checklistID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                template = generateChecklistTemplate(rs);
            }
        } catch (SQLException | BObStatusException  | IntegrationException ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("Unable to build a checklist blueprint from database", ex);
        } finally {
            releasePostgresConnection(con, stmt);
             
        } 
        return template;
    }
    
    
    /**
     * Utility method for populating a OccChecklistTemplate with metadata
     *
     * @param rs containing all metadata fields from the checklist table
     * @return half-baked OccChecklistTemplate object (no element lists)
     */
    private OccChecklistTemplate generateChecklistTemplate(ResultSet rs) throws SQLException, IntegrationException, BObStatusException {
        OccChecklistTemplate chkList = new OccChecklistTemplate();
        MunicipalityCoordinator mc = getMuniCoordinator();
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        SystemIntegrator si = getSystemIntegrator();
        CodeIntegrator ci = getCodeIntegrator();
        
        chkList.setInspectionChecklistID(rs.getInt("checklistid"));
        chkList.setTitle(rs.getString("title"));
        chkList.setDescription(rs.getString("description"));
        chkList.setMuni(mc.getMuni(rs.getInt("muni_municode")));

        if(rs.getTimestamp("createdts") != null){
            chkList.setCreatedTS(rs.getTimestamp("createdts").toLocalDateTime());
        } 
        chkList.setNotes(rs.getString("notes"));
        
        if(rs.getInt("defaultinspectioncause") != 0){
            chkList.setDefaultInspectionCause(oic.getOccInspectioncause(rs.getInt("defaultinspectioncause")));
        }
        
        si.populateUMAPTrackedFields(chkList, rs);
        
        return chkList;
    }

    
    
    /**
     * Extracts a record from occspacetype, with no inspection meta data attached
     *
     * @param spaceTypeID
     * @return
     * @throws IntegrationException
     */
    public OccSpaceType getOccSpaceType(int spaceTypeID) throws IntegrationException {
        OccSpaceType spaceType = null;
        String query = """
                       SELECT spacetypeid, spacetitle, description
                         FROM public.occspacetype WHERE spacetypeid=?;""";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, spaceTypeID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                spaceType = generateOccSpaceType(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Could not create sapce", ex);
        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return spaceType;
    }
    
      /**
     * Generator for OccSpaceType objects
     * @param rs for a single record with all fields SELECTed
     * @return the fully-baked object
     * @throws SQLException
     * @throws IntegrationException 
     */
    private OccSpaceType generateOccSpaceType(ResultSet rs) throws SQLException, IntegrationException {
        OccSpaceType tpe = new OccSpaceType();
        tpe.setSpaceTypeID(rs.getInt("spacetypeid"));
        tpe.setSpaceTypeTitle(rs.getString("spacetitle"));
        tpe.setSpaceTypeDescription(rs.getString("description"));
        return tpe;
    }    
    
    
    /**
     * Extracts all space types from the DB
     * @return
     * @throws IntegrationException 
     */
    public List<Integer> getOccSpaceTypeIDListComplete() throws IntegrationException{
        List<Integer> idl = new ArrayList<>();
        
        String query = """
                       SELECT spacetypeid, spacetitle, description
                         FROM public.occspacetype WHERE deactivatedts IS NULL;
                       """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();
            while (rs.next()) {
                idl.add(rs.getInt("spacetypeid"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Could not create sapce", ex);
        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return idl;
    }

    
      
    /**
     * Updates a record in the occspacetype by ID
     * @param ost to update
     * @throws IntegrationException 
     */
    public void updateSpaceType(OccSpaceType ost) throws IntegrationException {
        String query = """
                       UPDATE public.occspacetype
                          SET spacetitle=?, description=? 
                        WHERE spacetypeid=?;
                       """;
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);
            stmt.setString(1, ost.getSpaceTypeTitle());
            stmt.setString(2, ost.getSpaceTypeDescription());
            
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update space type", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        }
    }

    
    
        
    
    /**
     * Updates a record in the occchecklistspacetype by ID
     * @param ostc
     * @throws IntegrationException 
     */
    public void updateSpaceTypeChecklistified(OccSpaceTypeChecklistified ostc) throws IntegrationException {
        String query = """
                       UPDATE public.occchecklistspacetype
                          SET checklist_id=?, required=?, spacetype_typeid=?, 
                              notes=?, lastupdatedts=now(),autoaddoninspectioninit=? 
                        WHERE checklistspacetypeid=?;
                       """;
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, ostc.getChecklistParentID());
            stmt.setBoolean(2, ostc.isRequired());
            stmt.setInt(3, ostc.getSpaceTypeID());
            stmt.setString(4, ostc.getNotes());
            stmt.setBoolean(5, ostc.isAutoAddOnInspectionInit());
            
            stmt.setInt(6, ostc.getChecklistSpaceTypeID());
            
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update space type", ex);
        } finally {
             releasePostgresConnection(con, stmt);
             
        }
    }
    
    /**
     * Updates a record in the occchecklistspacetype by ID
     * @param ose
     * @throws IntegrationException 
     */
    public void updateOccSpaceElement(OccSpaceElement ose) throws IntegrationException {
        if(ose == null){
            throw new IntegrationException("Cannot update null OccSpaceElement");
        }
        String query = """
                       UPDATE public.occchecklistspacetypeelement
                          SET required=?, lastupdatedts=now() WHERE spaceelementid=?;
                       """;
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);
            stmt.setBoolean(1, ose.isRequiredForInspection());
            stmt.setInt(2, ose.getOccChecklistSpaceTypeElementID());
            
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to OccSpaceElement", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        }
    }

  
    /**
     * Creates a new record in the occspacetype--bare bones type
     * @param spaceType
     * @throws IntegrationException 
     */
    public int insertSpaceType(OccSpaceType spaceType) throws IntegrationException {
        String query = """
                       INSERT INTO public.occspacetype(
                                   spacetypeid, spacetitle, description)
                           VALUES (DEFAULT, ?, ?);
                       """;

        Connection con = null;
         ResultSet rs = null;
         int freshID = 0;
        PreparedStatement stmt = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setString(1, spaceType.getSpaceTypeTitle());
            stmt.setString(2, spaceType.getSpaceTypeDescription());
            
            stmt.execute();
            
            String retrievalQuery = "SELECT currval('spacetype_spacetypeid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();

            while (rs.next()) {
                freshID = rs.getInt(1);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert SpaceType", ex);
        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return freshID;
    }
    
    
    /**
     * Deactivation stamps the given space type
     * @param spaceType
     * @throws IntegrationException 
     */
    public void deactivateSpaceType(OccSpaceType spaceType) throws IntegrationException {
        if(spaceType == null){
            throw new IntegrationException("Cannot deactivate null space type");
        }
        
        String query = """
                       UPDATE public.occspacetype SET deactivatedts=now() WHERE spacetypeid=?;
                       """;

        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, spaceType.getSpaceTypeID());
            
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert SpaceType", ex);
        } finally {
             releasePostgresConnection(con, stmt);
        } 
    }
  
 /**
     * Extracts all space types from the DB
     * @param ostchk
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException if the space type is not unique in the checklist
     */
    public void verifyUniqueChecklistSpaceTypeLink(OccSpaceTypeChecklistified ostchk) throws IntegrationException, BObStatusException{
        
        String query = """
                       SELECT checklistspacetypeid 
                         FROM public.occchecklistspacetype WHERE checklist_id=? AND spacetype_typeid=? AND occchecklistspacetype.deactivatedts IS NULL;
                       """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, ostchk.getChecklistParentID());
            stmt.setInt(2, ostchk.getSpaceTypeID());
            rs = stmt.executeQuery();
            while (rs.next()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Cannot link space type to checklist: Duplicate Mapping. ");
                sb.append("Checklist ID ");
                sb.append(ostchk.getChecklistParentID());
                sb.append(" is already linked to space type ID ");
                sb.append(ostchk.getSpaceTypeID());
                throw new BObStatusException(sb.toString());
            }
            System.out.println("OccChecklistIntegrator: Verified unique mapping of element to space type!");
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Could verify unique space type and checklist", ex);
        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        
    }

    
     
 /**
     * Extracts all space types from the DB
     * @param ose
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException if the element is not unique in the space type
     */
    public void verifyUniqueCodeElementInSpaceType(OccSpaceElement ose) throws IntegrationException, BObStatusException{
        if(ose == null){
            throw new BObStatusException("Cannot check occ space element for uniqueness with null element!");
        }
        if(ose.getCodeSetElementID() == 0 || ose.getParentSpaceTypeID() == 0){
            throw new BObStatusException("Cannot check OccSpaceElement for uniqueness with ID 0 in codeset or parent space type");
        }
        
        String query = "SELECT spaceelementid FROM public.occchecklistspacetypeelement"
                + " WHERE codesetelement_seteleid = ? AND checklistspacetype_typeid=? AND public.occchecklistspacetypeelement.deactivatedts IS NULL;";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, ose.getCodeSetElementID());
            stmt.setInt(2, ose.getParentSpaceTypeID());
            rs = stmt.executeQuery();
            while (rs.next()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Cannot link element to space type: Duplicate Mapping. ");
                sb.append("Space type ID: ");
                sb.append(ose.getParentSpaceTypeID());
                sb.append(" is already linked to code set element ID ");
                sb.append(ose.getCodeSetElementID());
                throw new BObStatusException(sb.toString());
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Could not verify uniqueness of element to space type", ex);
        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        
    }



    /**
     * Inserts a record into the occchecklistspacetype table but does not
     * process its OccSpaceElement list in its belly.
     * @param ost whose checklistParentID is the desired target checklist to which the given space type 
     * shall be linked
     * @return the occ space type checklistified ID
     * @throws IntegrationException 
     */
    public int insertOccChecklistSpaceTypeChecklistified(OccSpaceTypeChecklistified ost) throws IntegrationException {
        String query = """
                       INSERT INTO public.occchecklistspacetype(
                            checklistspacetypeid, checklist_id, required, spacetype_typeid, notes, lastupdatedts, autoaddoninspectioninit) 
                            VALUES (DEFAULT, ?, ?, ?, ?, now(), ?)
                       """;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int freshID = 0;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            
            stmt.setInt(1, ost.getChecklistParentID());
            stmt.setBoolean(2, ost.isRequired());
            stmt.setInt(3, ost.getSpaceTypeID());
            stmt.setString(4, ost.getNotes());
            stmt.setBoolean(5, ost.isAutoAddOnInspectionInit());
            
            stmt.execute();
            
            String retrievalQuery = "SELECT currval('chkliststiceid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();
            
            while (rs.next()) {
                freshID = rs.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert ChecklistSpaceType", ex);
        } finally {
             releasePostgresConnection(con, stmt, rs);
             
        } 
        return freshID;
    }

    /**
     * Writes a deac timestamp to this record
     * 
     * 
     *
     * @param elementToDetach
     * @throws IntegrationException
     */
    public void detachOccSpaceElementFromOccSpaceTypeChecklistified(OccSpaceElement elementToDetach) throws IntegrationException {
        String query = """
                       UPDATE public.occchecklistspacetypeelement
                        	SET deactivatedts=now(), lastupdatedts=now() 
                        	WHERE checklistspacetype_typeid = ? AND codesetelement_seteleid = ?;""";
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, elementToDetach.getParentSpaceTypeID());
            stmt.setInt(2, elementToDetach.getCodeSetElementID());
            stmt.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("OccChecklistIntegrator.detachCodeElementFromOccSpaceTypeChecklistified | Could not remove link", ex);
        } finally {
             releasePostgresConnection(con, stmt);
        } 
    }

    /**
     * Creates a record in occchecklist
     * Updated for 21-JUL'21
     *
     * @param bp
     * @throws IntegrationException
     */
    public int insertChecklistTemplateMetadata(OccChecklistTemplate bp) throws IntegrationException {
        String query = """
                       INSERT INTO public.occchecklist(
                                   checklistid, title, description, 
                                    muni_municode, defaultinspectioncause, notes,
                                    createdts, createdby_umapid, lastupdatedts, lastupdatedby_umapid)
                           VALUES ( DEFAULT, ?, ?, 
                                    ?, ?, ?, 
                                    now(), ?, now(), ?);
                       """;
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement stmt = null;
        int freshID = 0;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setString(1, bp.getTitle());
            stmt.setString(2, bp.getDescription());
            stmt.setInt(3, bp.getMuni().getMuniCode());
           
            
            if(bp.getDefaultInspectionCause() != null){
                stmt.setInt(4, bp.getDefaultInspectionCause().getCauseID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            
            stmt.setString(5, bp.getNotes());
            if(bp.getCreatedBy_UMAPID() != 0){
                stmt.setInt(6, bp.getCreatedBy_UMAPID());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            if(bp.getLastUpdatedBy_UMAPID() != 0){
                stmt.setInt(7, bp.getCreatedBy_UMAPID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            stmt.execute();
            
            String retrievalQuery = "SELECT currval('checklist_checklistid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();

            while (rs.next()) {
                freshID = rs.getInt(1);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return freshID;
    }

    /**
     * Updates a record in occchecklist
     * @param oct
     * @throws IntegrationException
     */
    public void updateChecklistTemplateMetadata(OccChecklistTemplate oct) throws IntegrationException {
        String query = """
                       UPDATE public.occchecklist
                          SET title=?, description=?, muni_municode=?, defaultinspectioncause=?, 
                              notes=?, lastupdatedts=now(), lastupdatedby_umapid=? 
                        WHERE checklistid=?
                       """;
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);
            stmt.setString(1, oct.getTitle());
            stmt.setString(2, oct.getDescription());
            stmt.setInt(3, oct.getMuni().getMuniCode());
            
            if(oct.getDefaultInspectionCause() != null){
                stmt.setInt(4, oct.getDefaultInspectionCause().getCauseID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            
            stmt.setString(5, oct.getNotes());
            if(oct.getLastUpdatedBy_UMAPID() != 0){
                stmt.setInt(6, oct.getLastUpdatedBy_UMAPID());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            stmt.setInt(7, oct.getInspectionChecklistID());
            
            stmt.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot update checklist metatdata!", ex);
        } finally {
             releasePostgresConnection(con, stmt);
             
        } 
    }

 
    /**
     * Asks DB for the ID of the most recently assigned ID
     * @Deprecated  not used as of 9-march-2023
     * @param occInspectionIntegrator
     * @return 
     */
    public int getLastInsertSpaceTypeid(OccInspectionIntegrator occInspectionIntegrator) {
        int lastInsertSpaceTypeid = 0;
        String query =  "SELECT spacetypeid\n" 
                    +   "  FROM public.occchecklistspacetype\n" 
                    +   "  ORDER BY spacetypeid DESC\n" 
                    +   "  LIMIT 1;";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();
            while (rs.next()) {
                lastInsertSpaceTypeid = rs.getInt("spacetypeid");
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return lastInsertSpaceTypeid;
    }
    
    
    /**
     * Cache backed getter for checklistified space types
     * @param ocstid
     * @return
     * @throws IntegrationException 
     */
    public OccSpaceTypeChecklistified getOccSpaceTypeChecklistified(int ocstid) throws IntegrationException {
        if(ocstid == 0){
            throw new IntegrationException("Cannot get space type checklistified with space type id of 0");
        }
        
        if(getOccInspectionCacheManager().isCachingEnabled()){
            return getOccInspectionCacheManager().getCacheOccSpaceTypeChecklistified().get(ocstid, k -> fetchOccSpaceTypeChecklistified(ocstid));
        } else {
            return fetchOccSpaceTypeChecklistified(ocstid);
        }
        
        
    }

    /**
     * Takes in a OccSpace type ID, then uses that to query for
     * all of the codeelements attached to that space. Used to compose a
     * checklist blueprint which can be converted into an implemented checklist
     * 
     * And as of March 2023 is also called when building an actual occ inspeection
     * 
     * @param ocstid the checklistspacetypeID
     * @return a OccSpaceChecklistified object populated with CodeElement objects
     * @throws IntegrationException
     */
    public OccSpaceTypeChecklistified fetchOccSpaceTypeChecklistified(int ocstid) throws DatabaseFetchRuntimeException {
        
         String query = "SELECT 	occchecklistspacetypeelement.spaceelementid, \n" + //inc
                        "	occchecklistspacetypeelement.codesetelement_seteleid, \n" + //inc
                        "	occchecklistspacetypeelement.required AS ocsterequired, \n" + //inc
                        "	occchecklistspacetypeelement.checklistspacetype_typeid, \n" + // join field
                        "	occchecklistspacetypeelement.notes AS ocstenotes,\n" + //inc
                        "	occchecklistspacetypeelement.deactivatedts as ocstedeac,\n" + //inc
                        "	occchecklistspacetype.checklistspacetypeid, \n" + //inc
                        "	occchecklistspacetype.checklist_id, \n" + //inc
                        "	occchecklistspacetype.required AS ocstrequired, \n" + //inc
                        "	occchecklistspacetype.autoaddoninspectioninit AS autoaddoninspectioninit, \n" + //inc
                        "	occchecklistspacetype.spacetype_typeid, \n" + // join field
                        "	occchecklistspacetype.deactivatedts as stdeac, \n" + 
                        "	occchecklistspacetype.notes AS ocstnotes \n" + //inc
                        "FROM    public.occchecklistspacetype \n" +
                        "	LEFT OUTER JOIN public.occchecklistspacetypeelement " +
                        "ON (occchecklistspacetypeelement.checklistspacetype_typeid=occchecklistspacetype.checklistspacetypeid) " +
                        "WHERE 	occchecklistspacetype.checklistspacetypeid=?;";  // removed filter 13 APRIL to not load deactivated space type elements so we can still see the types on inspections that use them
                                                                                 // AND public.occchecklistspacetypeelement.deactivatedts IS NULL;
        Connection con = getPostgresCon();                                      
        ResultSet rs = null;
        PreparedStatement stmt = null;
        List<OccSpaceElement> eleList = new ArrayList<>();
        OccSpaceTypeChecklistified ostc = null;
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, ocstid);
            rs = stmt.executeQuery();
            boolean containerBuilt = false;
            while (rs.next()) {
                // this containerBuilt is a hacky way of using the same while looop for the space type and its elements
                // this is because our left outer join includes copies of the space type info for each element in the resulting table
                if(!containerBuilt){
                    ostc = new OccSpaceTypeChecklistified(getOccSpaceType(rs.getInt("spacetype_typeid")));
                    ostc.setChecklistSpaceTypeID(rs.getInt("checklistspacetypeid"));
                    ostc.setRequired(rs.getBoolean("ocstrequired"));
                    ostc.setAutoAddOnInspectionInit(rs.getBoolean("autoaddoninspectioninit"));
                    ostc.setChecklistParentID(rs.getInt("checklist_id"));
                    ostc.setNotes(rs.getString("ocstnotes"));
                    if(rs.getTimestamp("stdeac") != null){
                        ostc.setDeactivatedTS(rs.getTimestamp("stdeac").toLocalDateTime());
                    }
                    containerBuilt = true;
                }
                
                // Because of this yucky left outer join, we can get back a row to correspond
                // to a type but a type that has no elements to inspect. So this was yielding
                // an occspacelement without any guts and erroring out the insert.
                if(rs.getInt("spaceelementid") != 0){
                    // this call to the code coordinator is basically an antipattern--but for this level of 
                    // complexity of object creation, we'll try this for testing
                    int seteleid = rs.getInt("codesetelement_seteleid");
                    OccSpaceElement osele = new OccSpaceElement(getCodeCoordinator().getEnforcableCodeElement(seteleid));
                    osele.setRequiredForInspection(rs.getBoolean("ocsterequired")); 
                    osele.setOccChecklistSpaceTypeElementID(rs.getInt("spaceelementid")); 
                    osele.setNotes(rs.getString("ocstenotes"));
                    osele.setParentSpaceTypeID(rs.getInt("checklistspacetype_typeid")); // for tracking of parent only
                    if(rs.getTimestamp("ocstedeac") != null){
                        osele.setOseDeactivatedTS(rs.getTimestamp("ocstedeac").toLocalDateTime());
                    }
                    // only inject the active ones
                    if(osele.getOseDeactivatedTS() == null){
                        eleList.add(osele);
                    }
                }
            }
            //inject our list of elements
            if(ostc !=null){
                ostc.setCodeElementList(eleList);
            }
        } catch (SQLException | IntegrationException | BObStatusException ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("OccChecklistIntegrator.getOccSpaceTypeChecklistified | Could not built fancy OccSpaceTypeChecklistified!", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return ostc;
    }

   
   
    
}
