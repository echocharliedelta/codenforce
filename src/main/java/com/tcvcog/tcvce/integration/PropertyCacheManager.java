/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheClient;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import com.tcvcog.tcvce.entities.*;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Holds and manages a family of object caches
 * @author Ellen Bascom of Apartment31Y
 */
@Named("propertyCacheManager")
@ApplicationScoped
public  class       PropertyCacheManager 
        implements  IFaceCacheManager{
    
       
    @Inject
    private CECaseCacheManager ceCaseCacheManager;
    @Inject
    private OccupancyCacheManager occupancyCacheManager;
    @Inject
    private PersonCacheManager personCacheManager;
    
    private boolean cachingEnabled;
    
    private Cache<Integer, Parcel> cacheParcel;
    private Cache<Integer, PropertyUnit> cachePropertyUnit; // done
    private Cache<Integer, PropertyUseType> cachePropertyUseType; // done
    private Cache<Integer, ParcelInfo> cacheParcelInfo;  // done
    
    private Cache<Integer, MailingAddress> cacheMailingAddress; // done
    private Cache<Integer, MailingStreet> cacheMailingStreet;  // done
    private Cache<Integer, MailingCityStateZip> cacheMailingCityStateZip; // done
     
    private Cache<Integer, MailingAddressLink> cacheMailingAddressLink;
    
    private final List<IFaceCacheClient> cacheClientList;
    
    public PropertyCacheManager(){
        cacheClientList = new ArrayList<>();
    }
    
    @PostConstruct
    public void initBean() {
        System.out.println("PropertyCacheManager.initBean");
        initCaching();
    }
    
    /**
     * Instantiates all our caches
     */
    @Override
    public void initCaching(){
        // checklists
        cacheParcel = Caffeine.newBuilder().maximumSize(25000).softValues().build();  // update flush
        cachePropertyUseType = Caffeine.newBuilder().maximumSize(200).softValues().build();
        cacheParcelInfo = Caffeine.newBuilder().maximumSize(25000).softValues().build(); // update flush
        cachePropertyUnit = Caffeine.newBuilder().maximumSize(50000).softValues().build(); //  update flush

        cacheMailingAddress = Caffeine.newBuilder().maximumSize(40000).softValues().build();  // update flush
        cacheMailingStreet = Caffeine.newBuilder().maximumSize(30000).softValues().build(); // update flush
        cacheMailingCityStateZip = Caffeine.newBuilder().maximumSize(66000).softValues().build();
        cacheMailingAddressLink = Caffeine.newBuilder().maximumSize(66000).softValues().build();
    }
    
    @Override
    public void registerCacheClient(IFaceCacheClient client) {
        if(getCacheClientList() != null && !cacheClientList.contains(client)){
            getCacheClientList().add(client);
        }
    }

    @Override
    public void removeCacheClient(IFaceCacheClient client) {
        if(getCacheClientList() != null && getCacheClientList().contains(client)){
            getCacheClientList().remove(client);
        }
        
    }
    
    /**************************************************************************
    ******************              FLUSHING                *******************
    ***************************************************************************/

    /**
     * 
     * Dumps all my caches
     */
    @Override
    public void flushAllCaches(){
        System.out.println("PropertyCacheManager.flushAllCaches");
        if(cacheParcel != null){
            cacheParcel.invalidateAll();
        }
        if(cachePropertyUnit != null){
            cachePropertyUnit.invalidateAll();
        }
        if(cachePropertyUseType != null){
            cachePropertyUseType.invalidateAll();
        }
        if(cacheParcelInfo != null){
            cacheParcelInfo.invalidateAll();
        }
        if(cacheMailingAddress != null){
            cacheMailingAddress.invalidateAll();
        }
        if(cacheMailingStreet != null){
            cacheMailingStreet.invalidateAll();
        }
        if(cacheMailingCityStateZip != null){
            cacheMailingCityStateZip.invalidateAll();
        }
        if(cacheMailingAddressLink != null){
            cacheMailingAddressLink.invalidateAll();
        }
    }
    
    /**
     * Writes stats
     */
    @Override
    public void writeCacheStats(){
        if(cacheParcel != null){
            System.out.println("cacheParcel size: " + cacheParcel.estimatedSize());
        }
        if(cachePropertyUnit != null){
            System.out.println("cachePropertyUnit size: " + cachePropertyUnit.estimatedSize());
        }
        if(cachePropertyUseType != null){
            System.out.println("cachePropertyUseType size: " + cachePropertyUseType.estimatedSize());
        }
        if(cacheParcelInfo != null){
            System.out.println("cacheParcelInfo size: " + cacheParcelInfo.estimatedSize());
        }
        if(cacheMailingAddress != null){
            System.out.println("cacheMailingAddress size: " + cacheMailingAddress.estimatedSize());
        }
        if(cacheMailingStreet != null){
            System.out.println("cacheMailingStreet size: " + cacheMailingStreet.estimatedSize());
        }
        if(cacheMailingCityStateZip != null){
            System.out.println("cacheMailingCityStateZip size: " + cacheMailingCityStateZip.estimatedSize());
        }
        if(cacheMailingAddressLink != null){
            System.out.println("cacheMailingAddressLink size: " + cacheMailingAddressLink.estimatedSize());
        }
    }

    @Override
    public void flushObjectFromCache(IFaceCachable cable) {
        if(cable != null){
            flushKey(cable.getCacheKey());
        }
    }
    
    @Override
    public void flushObjectFromCache(int cacheKey) {
        flushKey(cacheKey);
    }
    
    private void flushKey(int key){
        if(cacheParcel != null){
            cacheParcel.invalidate(key);
        }
        if(cachePropertyUnit != null){
            cachePropertyUnit.invalidate(key);
        }
        if(cachePropertyUseType != null){
            cachePropertyUseType.invalidate(key);
        }
        if(cacheParcelInfo != null){
            cacheParcelInfo.invalidate(key);
        }
        if(cacheMailingAddress != null){
            cacheMailingAddress.invalidate(key);
        }
        if(cacheMailingStreet != null){
            cacheMailingStreet.invalidate(key);
        }
        if(cacheMailingCityStateZip != null){
            cacheMailingCityStateZip.invalidate(key);
        }
        if(cacheMailingAddressLink != null){
            cacheMailingAddressLink.invalidate(key);
        }
    }
    
    /**
     * Catch all address related,
     * which is basically everybody except code
     */
    public void flushAllAddressConnected(){
        // flush all these caches
        flushAllCaches();
        
        // we can leave contacts alone and ony Person objects have address links in them
//        getPersonCacheManager().getCachePerson().invalidateAll();
        personCacheManager.flushAllCaches();
        // so now anybody with persons need to get flushed
        ceCaseCacheManager.flushAllCaches();
        occupancyCacheManager.flushAllCaches();
        // inspections should be safe?
        // event should be safe from addresses
        // blobs should be safe
    }
    
    /**
     * Object-specific flush
     * @param pcl
     */
    public void flush(Parcel pcl){
        flushAllAddressConnected();
        
    }
    
    /**
     * Object-specific flush
     * @param pu
     */
    public void flush(PropertyUnit pu){
        flushAllAddressConnected();
        
    }
    
    /**
     * Object-specific flush
     * @param put
     */
    public void flush(PropertyUseType put){
        flushAllAddressConnected();
        
    }
    
    /**
     * Object-specific flush
     * @param info
     */
    public void flush(ParcelInfo info){
        flushAllAddressConnected();
        
    }
    
    /**
     * Object-specific flush
     * @param strt 
     */
    public void flush(MailingStreet strt){
        flushAllAddressConnected();
        
    }
    
    /**
     * Object-specific flush
     * @param mad
     */
    public void flush(MailingAddress mad){
        flushAllAddressConnected();
        
    }
    
    /**
     * Object-specific flush
     * @param mcsz
     */
    public void flush(MailingCityStateZip mcsz){
        flushAllAddressConnected();
        
    }
    
    /**
     * Object-specific flush
     * @param madLink
     */
    public void flush(MailingAddressLink madLink){
        flushAllAddressConnected();
        
    }
    
   
    // GETTERS AND SETTERS
    
    @Override
    public boolean isCachingEnabled() {
        return cachingEnabled;
    }

    @Override
    public void disableClientCaching() {
        cachingEnabled = false;
    }
    
    @Override
    public void enableClientCaching() {
        cachingEnabled = true;
    }

    /**
     * @return the cacheClientList
     */
    public List<IFaceCacheClient> getCacheClientList() {
        return cacheClientList;
    }

    /**
     * @return the cacheParcel
     */
    public Cache<Integer, Parcel> getCacheParcel() {
        return cacheParcel;
    }

    /**
     * @return the cachePropertyUnit
     */
    public Cache<Integer, PropertyUnit> getCachePropertyUnit() {
        return cachePropertyUnit;
    }

    /**
     * @return the cachePropertyUseType
     */
    public Cache<Integer, PropertyUseType> getCachePropertyUseType() {
        return cachePropertyUseType;
    }

    /**
     * @return the cacheParcelInfo
     */
    public Cache<Integer, ParcelInfo> getCacheParcelInfo() {
        return cacheParcelInfo;
    }

    /**
     * @return the cacheMailingAddress
     */
    public Cache<Integer, MailingAddress> getCacheMailingAddress() {
        return cacheMailingAddress;
    }

    /**
     * @return the cacheMailingStreet
     */
    public Cache<Integer, MailingStreet> getCacheMailingStreet() {
        return cacheMailingStreet;
    }

    /**
     * @return the cacheMailingCityStateZip
     */
    public Cache<Integer, MailingCityStateZip> getCacheMailingCityStateZip() {
        return cacheMailingCityStateZip;
    }

    /**
     * @return the cacheMailingAddressLink
     */
    public Cache<Integer, MailingAddressLink> getCacheMailingAddressLink() {
        return cacheMailingAddressLink;
    }
}
