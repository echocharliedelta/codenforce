/*
 * Copyright (C) 2018 Turtle Creek Valley
 * Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheClient;
import com.tcvcog.tcvce.coordinators.BlobCoordinator;
import com.tcvcog.tcvce.coordinators.CodeCoordinator;
import com.tcvcog.tcvce.coordinators.EventCoordinator;
import com.tcvcog.tcvce.coordinators.MunicipalityCoordinator;
import com.tcvcog.tcvce.coordinators.OccInspectionCoordinator;
import com.tcvcog.tcvce.coordinators.PersonCoordinator;
import com.tcvcog.tcvce.coordinators.PropertyCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.coordinators.UserCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.DatabaseFetchRuntimeException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.domain.MetadataException;
import com.tcvcog.tcvce.entities.CodeSet;
import com.tcvcog.tcvce.entities.CodeSource;
import com.tcvcog.tcvce.entities.County;
import com.tcvcog.tcvce.entities.CourtEntity;
import com.tcvcog.tcvce.entities.MailingCityStateZip;
import com.tcvcog.tcvce.entities.MuniProfile;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.MunicipalityDataHeavy;
import com.tcvcog.tcvce.entities.occupancy.OccPermitType;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import com.tcvcog.tcvce.entities.User;
import jakarta.annotation.PostConstruct;

/**
 * Business object factory for all Things Municipality
 * 
 * @author ellen bascomb of apt 31y
 */
public class MunicipalityIntegrator extends BackingBeanUtils implements Serializable, IFaceCacheClient {
    
    
    /**
     * Creates a new instance of MunicipalityIntegrator
     */
    public MunicipalityIntegrator() {
        
    }
    
    
    @PostConstruct
    public void initBean() {
        registerClientWithManager();
        // called during registration
//        enableCaching();
                

    }
    
    @Override
    public void registerClientWithManager() {
        getSystemMuniCacheManager().registerCacheClient(this);
    }
    
   
    
    final static int DEFAULT_COURT_ENTITY_REL_ORDER = 1;
    
    
    
    
    /**
     * Cache-backed getter for County objects
     * @param county
     * @return
     * @throws IntegrationException
     * @throws DatabaseFetchRuntimeException 
     */
    public County getCounty(int county) throws IntegrationException, DatabaseFetchRuntimeException {
         if(county == 0){
              throw new IntegrationException("Cannot get print style of ID == 0");
          }
          if(getSystemMuniCacheManager().isCachingEnabled()){
              return getSystemMuniCacheManager().getCacheCounty().get(county, k -> fetchCounty(county));
          } else {
              return fetchCounty(county);
          }
          
    }
    
    /**
     * Fetches a county from the DB
     * @param countyID
     * @return the county
     */
    public County fetchCounty(int countyID) throws DatabaseFetchRuntimeException{
        
        PreparedStatement stmt = null;
        County cnty = null;
        Connection con = null;
        
        StringBuilder query = new StringBuilder(); 
        
        query.append("""
                      SELECT countyid, countyname, stateabrev, countyexternalcode, active
                      FROM public.county WHERE countyid=?;
                     """);
        
        ResultSet rs = null;
 
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query.toString());
            stmt.setInt(1, countyID);
            rs = stmt.executeQuery();
            while(rs.next()){
                cnty = new County();
                cnty.setCountyID(rs.getInt("countyid"));
                cnty.setCountyName(rs.getString("countyname"));
                cnty.setStateAbbreviation(rs.getString("stateabrev"));
                cnty.setExternalCode(rs.getString("countyexternalcode"));
                cnty.setActive(rs.getBoolean("active"));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new DatabaseFetchRuntimeException("Exception in MunicipalityIntegrator.fetchCounty", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return cnty;
        
    }
    
    
    /**
     * Fetches all muni codes in which the given user has a valid UMAP with code officer = true
     * @param usr cannot be null
     * @return a list, perhaps with some munis
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     
     */
    public List<Municipality> assembleMuniListByOfficerUser(User usr) throws IntegrationException, BObStatusException{
        if(usr == null){
            throw new IntegrationException("Cannot search for munis with null user");
        }
        
        MunicipalityCoordinator mc = getMuniCoordinator();
        PreparedStatement stmt = null;
        Connection con = null;
        
        List<Municipality> muniList = new ArrayList<>();
        
        StringBuilder query = new StringBuilder(); 
        
        query.append("""
                      SELECT DISTINCT muni_municode FROM loginmuniauthperiod WHERE authuser_userid = ? AND codeofficer = TRUE AND recorddeactivatedts IS NULL;
                     """);
        
        ResultSet rs = null;
 
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query.toString());
            stmt.setInt(1, usr.getUserID());
            rs = stmt.executeQuery();
            while(rs.next()){
                muniList.add(mc.getMuni(rs.getInt("muni_municode")));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Exception in MunicipalityIntegrator.assembleMuniListByOfficerUser", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return muniList;
        
    }
    /**
     * Returns the default code source list by muni
     * @param muni
     * @return the county
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public List<CodeSource> getCodeSourcesByMuni(Municipality muni) throws IntegrationException, BObStatusException{
        if(muni == null){
            throw new IntegrationException("Cannot get code sources by muni with null muni");
        }
        
        CodeCoordinator cc = getCodeCoordinator();
        
        PreparedStatement stmt = null;
        Connection con = null;
        List<CodeSource> sourceList = new ArrayList<>();
        StringBuilder query = new StringBuilder(); 
        
        query.append("""
                      SELECT codesource_sourceid
                      FROM public.municipalitycodesource WHERE muni_municode=?;
                     """);
        
        ResultSet rs = null;
 
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query.toString());
            stmt.setInt(1, muni.getMuniCode());
            rs = stmt.executeQuery();
            while(rs.next()){
                sourceList.add(cc.getCodeSource(rs.getInt("codesource_sourceid")));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new DatabaseFetchRuntimeException("Exception in MunicipalityIntegrator.getCodeSourcesByMuni", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return sourceList;
        
    }
    
    /**
     * Inserts a new county into the county table
     * 
     * @param county
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public int insertCounty(County county) throws IntegrationException, BObStatusException{
        if(county == null){
            throw new BObStatusException("cannot insert null object into county table");
        } 
        
        String query =  """
                         INSERT INTO public.county(
                         countyid, countyname, stateabrev, countyexternalcode, active)
                         VALUES (DEFAULT, ?, ?, ?, TRUE);""";
        PreparedStatement stmt = null;
        Connection con = null;
        ResultSet rs = null; 
        
        int freshCountyID = 0;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setString(1, county.getCountyName());
            stmt.setString(2, county.getStateAbbreviation());
            stmt.setString(3, county.getExternalCode());
            
            stmt.execute(); 

            String retrievalQuery = "SELECT currval('countyid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                freshCountyID = rs.getInt(1);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("cannot insert county into DB, sorry", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
          
         return freshCountyID;
        
    }
    
     /**
     * Inserts a new county into the county table
     * 
     * @param muni
     * @param countyList
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public void linkMuniToCounties(Municipality muni, List<County> countyList) throws IntegrationException, BObStatusException{
        if(muni == null || muni.getMuniCode() == 0 || countyList == null ){
            throw new BObStatusException("cannot link munis and counties with null muni, or empty list");
        } 
        
        String query =  """
                         INSERT INTO public.municipalitycounty
                                (county_countyid, muni_municode)
                         	VALUES (?, ?);
                        """;
        PreparedStatement stmt = null;
        Connection con = null;
        
        try {
            con = getPostgresCon();
            for(County cnty: countyList){
                stmt = con.prepareStatement(query);
                stmt.setInt(1, cnty.getCountyID());
                stmt.setInt(2, muni.getMuniCode());
                stmt.execute(); 
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Error connecting counties to munis", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
          
    }
    
    
     /**
     * Deletes all records in the municipalitycounty table keyed to the given muni.
     * Coordinator will write all new links on each update
     * 
     * @param muni
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public void clearAllMuniCountyLinks(Municipality muni) throws IntegrationException, BObStatusException{
        if(muni == null || muni.getMuniCode() == 0 ){
            throw new BObStatusException("cannot wipe muni county links with null muni");
        } 
        
        String query =  """
                         DELETE FROM public.municipalitycounty
                         	WHERE muni_municode=?;
                        """;
        PreparedStatement stmt = null;
        Connection con = null;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, muni.getMuniCode());
            stmt.execute(); 
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Error wiping connections between counties and munis", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
          
    }
    
    /**
     * Updates a record in the county table
     * @param county
     * @throws BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void updateCounty(County county) throws BObStatusException, IntegrationException{
        if(county == null || county.getCountyID() == 0){
            throw new BObStatusException("cannot update county with null input or county with ID = 0"); 
        } 
        
        String query =  """
                            UPDATE public.county
                         	SET countyname=?, stateabrev=?, countyexternalcode=?, active=?
                         	WHERE countyid=?;
                        """;
        PreparedStatement stmt = null;
        Connection con = null;
        
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setString(1, county.getCountyName());
            stmt.setString(2, county.getStateAbbreviation());
            stmt.setString(3, county.getExternalCode());
            stmt.setBoolean(4, county.isActive());
            stmt.setInt(5, county.getCountyID());
            
            stmt.executeUpdate(); 
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("cannot update county, sorry ", ex); 
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
        
    }
    
    /**
     * Extracts all counties in the DB; This method breaks the common design pattern
     * of returning and ID list and the coordinator iterates and builds the full object '
     * list but counties are so small, this was skipped for expedience. 
     * 
     * @param includeInactive if you want inactive ones, too
     * @return the list of counties
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public List<County> getCountyListComplete(boolean includeInactive) throws IntegrationException, BObStatusException{
       
        PreparedStatement stmt = null;
        Connection con = null;
        StringBuilder query = new StringBuilder(); 
        query.append("""
                      SELECT countyid 
                      FROM public.county WHERE countyid IS NOT NULL  
                     """);
        if(!includeInactive){
            query.append(" AND active = TRUE");
        }
        query.append(";");
                
        ResultSet rs = null;
        List<County> countyList = new ArrayList<>();
 
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query.toString());
            
            rs = stmt.executeQuery();
            while(rs.next()){
                countyList.add(getCounty(rs.getInt("countyid")));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Exception in MunicipalityIntegrator.getCountyListComplete", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return countyList;
        
    }
    
    /**
     * Extracts the single period ID of the given muni's default occ period
     * @param muniCode
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public int getMuniDefaultOccPeriodID(int muniCode) throws IntegrationException, BObStatusException{
       
        PreparedStatement stmt = null;
        Connection con = null;
        StringBuilder query = new StringBuilder(); 
        query.append("""
                      SELECT defaultoccperiod
                      FROM public.municipality WHERE municode=?
                     """);
                
        ResultSet rs = null;
        int occPeriodID = 0;
 
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query.toString());
            stmt.setInt(1, muniCode);
            
            rs = stmt.executeQuery();
            while(rs.next()){
                occPeriodID = rs.getInt("defaultoccperiod");
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Exception in MunicipalityIntegrator.getMuniDefaultOccPeriodID", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return occPeriodID;
        
    }
    
    /**
     * Extracts all associations between a given muni and 0 or more Counties
     * 
     * @param muni
     * @return the list of counties
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public List<County> getCountyList(Municipality muni) throws IntegrationException, BObStatusException{
       if(muni == null || muni.getMuniCode() == 0){
           throw new BObStatusException("Cannot extract county list from a null input or muni with code of 0");
       }
    
        PreparedStatement stmt = null;
        Connection con = null;
        StringBuilder query = new StringBuilder(); 
        query.append("""
                      SELECT county_countyid
                      FROM public.municipalitycounty WHERE muni_municode=?;  
                     """);
                
        ResultSet rs = null;
        List<County> countyList = new ArrayList<>();
 
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query.toString());
            stmt.setInt(1, muni.getMuniCode());
            
            rs = stmt.executeQuery();
            
            while(rs.next()){
                countyList.add(getCounty(rs.getInt("county_countyid")));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Exception in MunicipalityIntegrator.getCountyList (by muni)", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return countyList;
    }
    
     /**
     * Connects a muni profile with the list of permit types in its belly
     * 
     * @param prof
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public void linkMuniProfileToOccPeriodTypes(MuniProfile prof) throws IntegrationException, BObStatusException{
        if(prof == null || prof.getProfileID() == 0 || prof.getOccPermitTypeList() == null ){
            throw new BObStatusException("cannot link munis profiles and occ permit types with null muni, or empty list");
        } 
        
        String query =  """
                         INSERT INTO public.muniprofileoccperiodtype(
                         	muniprofile_profileid, occperiodtype_typeid)
                         	VALUES (?, ?);
                        """;
        PreparedStatement stmt = null;
        Connection con = null;
        
        try {
            con = getPostgresCon();
            for(OccPermitType opt: prof.getOccPermitTypeList()){
                stmt = con.prepareStatement(query);
                stmt.setInt(1, prof.getProfileID());
                stmt.setInt(2, opt.getTypeID());
                stmt.execute(); 
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Error connecting occ permit types to munis", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
          
    }
    
    
     /**
     * Wipes out all links between the given profile and occ permit types
     * 
     * @param prof
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public void clearAllMuniProfileOccPermitTypeLinks(MuniProfile prof) throws IntegrationException, BObStatusException{
        if(prof == null || prof.getProfileID() == 0 ){
            throw new BObStatusException("cannot wipe muni type links with null muni");
        } 
        
        String query =  """
                         DELETE FROM public.muniprofileoccperiodtype
                         	WHERE muniprofile_profileid=?;
                        """;
        PreparedStatement stmt = null;
        Connection con = null;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, prof.getProfileID());
            stmt.execute(); 
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Error wiping connections between counties and munis", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
          
    }
    
    
     /**
     * Connects a muni the list of court entities in its belly
     * 
     * @param muni
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public void linkMuniToCourtEntities(MunicipalityDataHeavy muni) throws IntegrationException, BObStatusException{
        if(muni == null || muni.getMuniCode() == 0 || muni.getCourtEntities() == null ){
            throw new BObStatusException("cannot link munis and court entities with null muni, or empty list");
        } 
        
        String query =  """
                         INSERT INTO public.municourtentity(
                                	muni_municode, courtentity_entityid, relativeorder)
                                	VALUES (?, ?, ?);
                        """;
        PreparedStatement stmt = null;
        Connection con = null;
        
        try {
            con = getPostgresCon();
            for(CourtEntity court: muni.getCourtEntities()){
                stmt = con.prepareStatement(query);
                stmt.setInt(1, muni.getMuniCode());
                stmt.setInt(2, court.getCourtEntityID());
                stmt.setInt(3, DEFAULT_COURT_ENTITY_REL_ORDER);
                stmt.execute(); 
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Error connecting courts to munis", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
          
    }
    
    
     /**
     * Wipes out all links between the given muni and all courts
     * 
     * @param mdh
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public void clearAllMuniCourtLinks(MunicipalityDataHeavy mdh) throws IntegrationException, BObStatusException{
        if(mdh == null || mdh.getMuniCode() == 0 ){
            throw new BObStatusException("cannot wipe muni type links with null muni");
        } 
        
        String query =  """
                         DELETE FROM public.municourtentity
                         	WHERE muni_municode=?;
                        """;
        PreparedStatement stmt = null;
        Connection con = null;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, mdh.getMuniCode());
            stmt.execute(); 
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Error wiping connections between courts and munis", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
          
    }
    
     /**
     * Connects a muni profile with the list of permit types in its belly
     * 
     * @param mdh
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public void linkMuniToZips(MunicipalityDataHeavy mdh) throws IntegrationException, BObStatusException{
        if(mdh == null || mdh.getMuniCode() == 0 || mdh.getZipList() == null || mdh.getZipList().isEmpty()){
            throw new BObStatusException("cannot link munis and zips with null muni, or empty list");
        } 
        
        String query =  """
                         INSERT INTO public.municitystatezip(
                                	muni_municode, citystatezip_id)
                                	VALUES (?, ?);
                        """;
        PreparedStatement stmt = null;
        Connection con = null;
        
        try {
            con = getPostgresCon();
            for(MailingCityStateZip mcsz: mdh.getZipList()){
                stmt = con.prepareStatement(query);
                stmt.setInt(1, mdh.getMuniCode());
                stmt.setInt(2, mcsz.getCityStateZipID());
                stmt.execute(); 
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Error connecting zips to munis", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
          
    }
    
    
    
     /**
     * Wipes out all links between the given muni and all courts
     * 
     * @param mdh
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public void clearAllMuniCodeSourceLinks(MunicipalityDataHeavy mdh) throws IntegrationException, BObStatusException{
        if(mdh == null || mdh.getMuniCode() == 0 ){
            throw new BObStatusException("cannot wipe muni source links with null muni");
        } 
        
        String query =  """
                         DELETE FROM public.municipalitycodesource
                         	WHERE muni_municode=?;
                        """;
        PreparedStatement stmt = null;
        Connection con = null;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, mdh.getMuniCode());
            stmt.execute(); 
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Error wiping connections between sources and munis", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
          
    }
    
     /**
     * Connects a muni profile with the list of permit types in its belly
     * 
     * @param mdh
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public void linkMuniToSource(MunicipalityDataHeavy mdh) throws IntegrationException, BObStatusException{
        if(mdh == null || mdh.getMuniCode() == 0 || mdh.getIssuingCodeSourceList()== null ){
            throw new BObStatusException("cannot link munis and sources with null muni, or empty list");
        } 
        
        String query =  """
                         INSERT INTO public.municipalitycodesource(
                                	muni_municode, codesource_sourceid)
                                	VALUES (?, ?);
                        """;
        PreparedStatement stmt = null;
        Connection con = null;
        
        try {
            con = getPostgresCon();
            for(CodeSource src: mdh.getIssuingCodeSourceList()){
                stmt = con.prepareStatement(query);
                stmt.setInt(1, mdh.getMuniCode());
                stmt.setInt(2, src.getSourceID());
                stmt.execute(); 
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Error connecting sources to munis", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
          
    }
    
    
     /**
     * Wipes out all links between the given profile and occ permit types
     * 
     * @param mdh
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public void clearAllMuniZipLinks(MunicipalityDataHeavy mdh) throws IntegrationException, BObStatusException{
        if(mdh == null || mdh.getMuniCode() == 0 ){
            throw new BObStatusException("cannot wipe muni zip links with null muni");
        } 
        
        String query =  """
                         DELETE FROM public.municitystatezip 
                         	WHERE muni_municode=?;
                        """;
        PreparedStatement stmt = null;
        Connection con = null;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, mdh.getMuniCode());
            stmt.execute(); 
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Error wiping connections between zips and munis", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
          
    }
    
    /**
     * Cached-backed getter for Municipality objects
     * @param muniCode
     * @return
     * @throws IntegrationException
     * @throws DatabaseFetchRuntimeException 
     */
    public Municipality getMuni(int muniCode) throws IntegrationException, DatabaseFetchRuntimeException {
         if(muniCode == 0){
              throw new IntegrationException("Cannot get muni of ID == 0");
          }
          if(getSystemMuniCacheManager().isCachingEnabled()){
              return getSystemMuniCacheManager().getCacheMuni().get(muniCode, k -> fetchMuni(muniCode));
          } else {
              return fetchMuni(muniCode);
          }
          
    }
    
    /**
     * Builds a Municipality object from the municipality table
     * @param muniCode
     * @return
     * @throws DatabaseFetchRuntimeException
     */
    public Municipality fetchMuni(int muniCode) throws DatabaseFetchRuntimeException {
        System.out.println("MunicipalityIntegrator.fetchMuni | fetching from DB Municode: " + muniCode);
        PreparedStatement stmt = null;
        Municipality muni = null;
        Connection con = null;
        // note that muniCode is not returned in this query since it is specified in the WHERE
        StringBuilder query = new StringBuilder(); 
        query.append(" SELECT municode, muniname FROM public.municipality WHERE municode=?;");
        
        ResultSet rs = null;
 
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query.toString());
            stmt.setInt(1, muniCode);
            rs = stmt.executeQuery();
            while(rs.next()){
                muni = generateMuni(rs);
            }
            
        } catch (SQLException ex) {
            System.out.println("MunicipalityIntegrator.fetchMuni | " + ex.toString());
            throw new DatabaseFetchRuntimeException("Exception in MunicipalityIntegrator.fetchMuni", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return muni;
        
    }
    
    /**
     * Extracts a data heavy version of the Muni from the database that includes the MuniProfile object
     * which has a bunch of permissions switches on it
     * @param muniCode
     * @return
     * @throws IntegrationException
     * @throws AuthorizationException
     * @throws BObStatusException 
     */
    public MunicipalityDataHeavy getMunDataHeavy(int muniCode) throws IntegrationException, AuthorizationException, BObStatusException{
        PreparedStatement stmt = null;
        MunicipalityDataHeavy muniComplete = null;
        Connection con = null;
        // note that muniCode is not returned in this query since it is specified in the WHERE
        String query =  """
                            SELECT  municode, muniname, population, 
                                    activeinprogram, defaultcodeset, 
                                    novprintstyle_styleid, profile_profileid, enablecodeenforcement, 
                                    enableoccupancy, enablepublicceactionreqsub, enablepublicceactionreqinfo, 
                                    enablepublicoccpermitapp, enablepublicoccinspectodo,  
                                    notes, defaultoccperiod, 
                                    officeparcel_parcelid, defaultheaderimage_photodocid, defaultheaderimagewidthpx, 
                                    createdts, createdby_umapid, lastupdatedts, 
                                    lastupdatedby_umapid, deactivatedts, deactivatedby_umapid, 
                                    mailingaddress_addressid, populationyear, 
                                    munimnager_umap, primarycodeofficer_umap, admincontact_umap, defaultcodeenfchecklist
                          	FROM public.municipality WHERE municode=?;
                        """;
        ResultSet rs = null;
 
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, muniCode);
            rs = stmt.executeQuery();
            while(rs.next()){
                muniComplete = generateMuniDataHeavy(rs);
            }
            
        } catch (SQLException | MetadataException | BObStatusException | AuthorizationException | BlobException | IntegrationException ex) {
            System.out.println("MunicipalityIntegrator.getMuniDH | " + ex.toString());
            throw new IntegrationException("Exception in MunicipalityIntegrator.getMuniDataHeavy", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return muniComplete;
    }
    
    /**
     * Generator for our light Muni
     * @param rs
     * @return
     * @throws SQLException 
     */
    private Municipality generateMuni(ResultSet rs) throws SQLException{
        Municipality muni = new Municipality();
        
        muni.setMuniCode(rs.getInt("municode"));
        muni.setMuniName(rs.getString("muniname"));
        
        
        return muni;
    }
    
    /**
     * Internal generator for muni data heavy objects
     * @param rs
     * @return
     * @throws SQLException
     * @throws IntegrationException
     * @throws AuthorizationException
     * @throws BObStatusException
     * @throws MetadataException 
     */
    private MunicipalityDataHeavy generateMuniDataHeavy(ResultSet rs) throws SQLException, IntegrationException, AuthorizationException, BObStatusException, MetadataException, BlobException{
        
        CodeCoordinator cc = getCodeCoordinator();
        UserCoordinator uc = getUserCoordinator();
        BlobCoordinator bc = getBlobCoordinator();
        SystemIntegrator si = getSystemIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        PropertyCoordinator propc = getPropertyCoordinator();
        
        
        MunicipalityDataHeavy mdh = new MunicipalityDataHeavy(generateMuni(rs));
    
        mdh.setMuniProfileID(rs.getInt("profile_profileid"));
        mdh.setActiveInProgram(rs.getBoolean("activeinprogram"));             
        
        mdh.setMuniOfficePropertyId(rs.getInt("officeparcel_parcelid"));
        mdh.setDefaultOccPeriodID(rs.getInt("defaultoccperiod"));
        
      
        if(rs.getInt("mailingaddress_addressid") != 0){
            mdh.setContactAddress(propc.getMailingAddress(rs.getInt("mailingaddress_addressid")));
        }
        
        mdh.setPopulation(rs.getInt("population"));
        mdh.setPopulationYear(rs.getInt("populationyear"));
              
        mdh.setDefaultCodeSetID(rs.getInt("defaultcodeset"));
     
        
        if(rs.getInt("novprintstyle_styleid") != 0){
            mdh.setDefaultNOVPrintStyle(sc.getPrintStyle(rs.getInt("novprintstyle_styleid")));
        }
        if(rs.getInt("defaultheaderimage_photodocid") != 0){
            mdh.setDefaultMuniHeaderImage(bc.getBlob(rs.getInt("defaultheaderimage_photodocid")));
        }
        mdh.setDefaultMuniHeaderImageWidthPX(rs.getInt("defaultheaderimagewidthpx"));
        
        mdh.setEnableCodeEnforcement(rs.getBoolean("enablecodeenforcement"));
        mdh.setEnableOccupancy(rs.getBoolean("enableoccupancy"));
        mdh.setEnablePublicCEActionRequestSubmissions(rs.getBoolean("enablepublicceactionreqsub"));
        
        mdh.setEnablePublicCEActionRequestInfo(rs.getBoolean("enablepublicceactionreqinfo"));
        mdh.setEnablePublicOccPermitApp(rs.getBoolean("enablepublicoccpermitapp"));
        mdh.setEnablePublicOccInspectionTODOs(rs.getBoolean("enablepublicoccinspectodo"));

        if(rs.getInt("munimnager_umap") != 0){
            mdh.setMuniManagerUMAP(uc.auth_getUMAPNotValidated(rs.getInt("munimnager_umap")));
        }
        if(rs.getInt("primarycodeofficer_umap") != 0){
            mdh.setMuniPrimaryOfficerUMAP(uc.auth_getUMAPNotValidated(rs.getInt("primarycodeofficer_umap")));
        }
        if(rs.getInt("admincontact_umap") != 0){
            mdh.setMuniAdminContactUMAP(uc.auth_getUMAPNotValidated(rs.getInt("admincontact_umap")));
        }
        
        mdh.setNotes(rs.getString("notes"));
        
        // default checklist
        if(rs.getInt("defaultcodeenfchecklist") != 0){
            OccInspectionCoordinator oic = getOccInspectionCoordinator();
            mdh.setDefaultInspectionChecklist(oic.getChecklistTemplate(rs.getInt("defaultcodeenfchecklist")));
        }
                
        si.populateUMAPTrackedFields(mdh, rs, true);
        
        return mdh;
    }
    
        /**
     * Writes a municipality to the DB
     * @param muni 
     */
    public void insertMuniDataHeavy(MunicipalityDataHeavy muni) throws IntegrationException {

        if(muni == null || muni.getCodeSet() == null || muni.getProfile() == null){
            throw new IntegrationException("Cannot insert new muni with null muni or null codeset");
            
        }
        String query = """
                            INSERT INTO public.municipality(
                                   	municode, muniname, population, populationyear, defaultcodeset, profile_profileid,
                                        createdts, createdby_umapid, lastupdatedts, lastupdatedby_umapid)
                                   	VALUES (?, ?, ?, ?, ?, ?,
                                        now(), ?, now(), ?);
                       """;

        Connection con = null;
        PreparedStatement stmt = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            
            stmt.setInt(1, muni.getMuniCode());
            stmt.setString(2, muni.getMuniName());
            stmt.setInt(3, muni.getPopulation());
            stmt.setInt(4, muni.getPopulationYear());
            stmt.setInt(5, muni.getCodeSet().getCodeSetID());
            stmt.setInt(6, muni.getProfile().getProfileID());
            
            if(muni.getCreatedby_UMAP() != null){
                stmt.setInt(7, muni.getCreatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            // lastupdatedts using pg now()
            if(muni.getLastUpdatedby_UMAP() != null){
                stmt.setInt(8, muni.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(8, java.sql.Types.NULL);
            }
            
            stmt.execute();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Fatal error inserting fresh muni");

        } finally {
            releasePostgresConnection(con, stmt);
        } 
    }
    
      /**
     * Updates a record in the table: municipality
     * @param muni
     * @throws IntegrationException 
     */
    public void updateMuniDataHeavyInfo(MunicipalityDataHeavy muni) throws IntegrationException{
        
        Connection con = null;
        String query =  """
                        UPDATE public.municipality
                         	SET municode=?, muniname=?, population=?, populationyear=? \n
                         	WHERE municode=?;
                        """;
        ResultSet rs = null;
        PreparedStatement stmt = null;
 
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, muni.getMuniCode());
            stmt.setString(2, muni.getMuniName());
            stmt.setInt(3, muni.getPopulation());
            
            
            stmt.setInt(4, muni.getPopulationYear());
            
            stmt.setInt(5, muni.getMuniCode());

            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Exception in MunicipalityIntegrator.updateMuni", ex);

        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
    }
    
      /**
     * Updates a record in the table: municipality
     * @param muni
     * @throws IntegrationException 
     */
    public void updateMuniObjectLinks(MunicipalityDataHeavy muni) throws IntegrationException{
        
        Connection con = null;
        String query =  """
                        UPDATE public.municipality
                         	SET defaultcodeset=?, defaultoccperiod=?, officeparcel_parcelid=?, mailingaddress_addressid=?,
                                    munimnager_umap=?, primarycodeofficer_umap=?, admincontact_umap=?,  
                                    lastupdatedts=now(), lastupdatedby_umapid=?, defaultcodeenfchecklist=? 
                         	WHERE municode=?;
                        """;
        ResultSet rs = null;
        PreparedStatement stmt = null;
 
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            
            if(muni.getCodeSet() != null){
                stmt.setInt(1, muni.getCodeSet().getCodeSetID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            
            if(muni.getDefaultOccPeriodID() != 0){
                stmt.setInt(2, muni.getDefaultOccPeriodID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            if(muni.getMuniOfficePropertyId() != 0){
                stmt.setInt(3, muni.getMuniOfficePropertyId());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            if(muni.getContactAddress() != null){
                stmt.setInt(4, muni.getContactAddress().getAddressID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
           
            
            if(muni.getMuniManagerUMAP()!= null){
                stmt.setInt(5, muni.getMuniManagerUMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            if(muni.getMuniManagerUMAP()!= null){
                stmt.setInt(6, muni.getMuniManagerUMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            if(muni.getMuniManagerUMAP()!= null){
                stmt.setInt(7, muni.getMuniManagerUMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            
            
            //createdby cannot be updated
            // lastupdatedts using pg now()
            if(muni.getLastUpdatedby_UMAP() != null){
                stmt.setInt(8, muni.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(8, java.sql.Types.NULL);
            }
            
            if(muni.getDefaultInspectionChecklist() != null){
                stmt.setInt(9, muni.getDefaultInspectionChecklist().getInspectionChecklistID());
            } else {
                stmt.setNull(9, java.sql.Types.NULL);
            }
            
            
            stmt.setInt(10, muni.getMuniCode());

            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Exception in MunicipalityIntegrator.updateMuniObjectLinks", ex);

        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        
    }
    
      /**
     * Updates a record in the table: municipality
     * @param muni
     * @throws IntegrationException 
     */
    public void updateMuniModules(MunicipalityDataHeavy muni) throws IntegrationException{
        
        Connection con = null;
        String query =  """
                        UPDATE public.municipality
                         	SET activeinprogram=?, enablecodeenforcement=?, enableoccupancy=?, 
                                    enablepublicceactionreqsub=?, enablepublicceactionreqinfo=?, enablepublicoccpermitapp=?, 
                                    enablepublicoccinspectodo=?, lastupdatedts=now(), lastupdatedby_umapid=? 
                         	WHERE municode=?;
                        """;
        ResultSet rs = null;
        PreparedStatement stmt = null;
 
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);

            stmt.setBoolean(1, muni.isActiveInProgram());
            stmt.setBoolean(2, muni.isEnableCodeEnforcement());
            stmt.setBoolean(3, muni.isEnableOccupancy());
            
            stmt.setBoolean(4, muni.isEnablePublicCEActionRequestSubmissions());
            stmt.setBoolean(5, muni.isEnablePublicCEActionRequestInfo());
            stmt.setBoolean(6, muni.isEnablePublicOccPermitApp());
            
            stmt.setBoolean(7, muni.isEnablePublicOccInspectionTODOs());
            
            if(muni.getLastUpdatedby_UMAP() != null){
                stmt.setInt(8, muni.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(8, java.sql.Types.NULL);
            }
            stmt.setInt(9, muni.getMuniCode());

            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Exception in MunicipalityIntegrator.updateMuniModules", ex);

        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        
    }
    
      /**
     * Updates a record in the table: municipality
     * @param muni
     * @throws IntegrationException 
     */
    public void updateMuniPermissions(MunicipalityDataHeavy muni) throws IntegrationException{
        
        Connection con = null;
        String query =  """
                        UPDATE public.municipality
                         	SET profile_profileid=?, 
                                    lastupdatedts=now(), lastupdatedby_umapid=?
                         	WHERE municode=?;
                        """;
        ResultSet rs = null;
        PreparedStatement stmt = null;
 
        try {
            con = getPostgresCon();
             stmt = con.prepareStatement(query);
             
            if(muni.getProfile() != null){
                stmt.setInt(1, muni.getProfile().getProfileID());
            } else {
                throw new IntegrationException("cannot update muni profile with null profile");
            }
            
            if(muni.getLastUpdatedby_UMAP() != null){
                stmt.setInt(2, muni.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            
            stmt.setInt(3, muni.getMuniCode());

            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Exception in MunicipalityIntegrator.updateMuniPermissions", ex);

        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        
    }
    
      /**
     * Updates a record in the table: municipality
     * @param muni
     * @throws IntegrationException 
     */
    public void updateMuniPrinting(MunicipalityDataHeavy muni) throws IntegrationException{
        
        Connection con = null;
        String query =  """
                        UPDATE public.municipality
                         	SET novprintstyle_styleid=?, defaultheaderimage_photodocid=?, defaultheaderimagewidthpx=?, 
                                    lastupdatedts=now(), lastupdatedby_umapid=?
                         	WHERE municode=?;
                        """;
        ResultSet rs = null;
        PreparedStatement stmt = null;
 
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
          
            
            if(muni.getDefaultNOVPrintStyle() != null){
                stmt.setInt(1, muni.getDefaultNOVPrintStyle().getStyleID());
            } else {
                throw new IntegrationException("Cannot update print style with null incoming style object!");
            }
            
            if(muni.getDefaultMuniHeaderImage() != null){
                stmt.setInt(2, muni.getDefaultMuniHeaderImage().getPhotoDocID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            stmt.setInt(3, muni.getDefaultMuniHeaderImageWidthPX());
            // createdts using pg now()
            
            if(muni.getLastUpdatedby_UMAP() != null){
                stmt.setInt(4, muni.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            
            stmt.setInt(5, muni.getMuniCode());

            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Exception in MunicipalityIntegrator.updateMuni", ex);

        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        
    }
    
     /**
     * Deactivates a municipality
     * @param muni
     * @throws BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void deactivateMuni(Municipality muni) throws BObStatusException, IntegrationException{
        
        if(muni == null || muni.getDeactivatedBy_UMAP() == null){
            throw new BObStatusException("Cannot deactivate muni with null muni or null deactivating UMAP!");
            
        }
        
        String query = "UPDATE municipality SET deactivatedts=now(), deactivatedby_umapid=? WHERE municode=?; ";
        Connection con = null;
        PreparedStatement stmt = null;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            
            stmt.setInt(1, muni.getDeactivatedBy_UMAP().getUserMuniAuthPeriodID());
            stmt.setInt(2, muni.getMuniCode());
            stmt.executeUpdate();
            
        } catch (SQLException ex) { 
             System.out.println(ex);
             throw new IntegrationException("Integration error deactivating muni", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
        
    }
   
    /**
     * Extracts all municipality IDs that are active in "the program"
     * @param requireActive
     * @return
     * @throws IntegrationException 
     */
    public List<Integer> getMuniList(boolean requireActive) throws IntegrationException{
        List<Integer> muniIDList = new ArrayList<>();
        StringBuilder query = new StringBuilder();
        query.append("SELECT municode FROM municipality WHERE municode IS NOT NULL ");
        if(requireActive){
            query.append("AND deactivatedts IS NULL");
        }
        query.append(";");
                
        ResultSet rs = null;
        PreparedStatement stmt = null;
        Connection con = getPostgresCon();
 
        try {
            stmt = con.prepareStatement(query.toString());
            rs = stmt.executeQuery();
            while(rs.next()){
                muniIDList.add(rs.getInt("municode"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Exception in MunicipalityIntegrator.getMuniList", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return muniIDList;
    }
   
    /**
     * Extracts all municipalities that are active AND allow CEAR submission
     * @param requireActive
     * @return
     * @throws IntegrationException 
     */
    public List<Municipality> getMuniListAllowCEAR() throws IntegrationException{
        MunicipalityCoordinator mc = getMuniCoordinator();
        
        List<Municipality> mList = new ArrayList<>();
        StringBuilder query = new StringBuilder();
        query.append("SELECT municode FROM municipality WHERE municode IS NOT NULL ");
        query.append("AND activeinprogram = true AND enablepublicceactionreqsub = TRUE;");
                
        ResultSet rs = null;
        PreparedStatement stmt = null;
        Connection con = getPostgresCon();
 
        try {
            stmt = con.prepareStatement(query.toString());
            rs = stmt.executeQuery();
            while(rs.next()){
                mList.add(mc.getMuni(rs.getInt("municode")));
            }
        } catch (SQLException | BObStatusException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Exception in MunicipalityIntegrator.getMuniListAllowCEAR", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        return mList;
    }
   
  
    
    /**
     * Generates a mapping of municode to muni name
     * @return the mapping
     * @throws IntegrationException 
     */
    public Map<Integer, String> getMunicipalityMap() throws IntegrationException{
        Map<Integer, String> muniMap = null;
            
        muniMap = new HashMap<>();
        Connection con = getPostgresCon();
        String query = "SELECT muniCode, muniName FROM municipality;";
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                muniMap.put(rs.getInt("muniCode"),rs.getString("muniName"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Exception in MunicipalityIntegrator.generateCompleteMuniNameIDMap", ex);

        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return muniMap;
    }
    
    
    /**
     * Generates a mapping of municipality name to municode; Remember that the municipality is the only
     * object in CNF in which the primary key is assigned externally, in this case by Allegheny County
     * @return
     * @throws IntegrationException 
     */
    public HashMap<String, Integer> generateCompleteMuniNameIDMap() throws IntegrationException{
        HashMap<String, Integer> muniMap = new HashMap<>();
       
        Connection con = getPostgresCon();
        
        String query = "SELECT muniCode, muniName FROM municipality;";
        ResultSet rs = null;
        PreparedStatement stmt = null;
 
        try {
            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();
            while(rs.next()){
                muniMap.put(rs.getString("muniName"), rs.getInt("muniCode"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Exception in MunicipalityIntegrator.generateCompleteMuniNameIDMap", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        return muniMap;
    }
    
      /**
     * @return the municipalityMap
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public HashMap<String, Integer> getMunicipalityStringIDMap() throws IntegrationException{
        return generateCompleteMuniNameIDMap();
    }

 
    
    /**
     * Cache-backed getter for muni profiles
     * @param profileID
     * @return
     * @throws IntegrationException
     * @throws DatabaseFetchRuntimeException 
     */
    public MuniProfile getMuniProfile(int profileID) throws IntegrationException, DatabaseFetchRuntimeException {
         if(profileID == 0){
              throw new IntegrationException("Cannot get muni of ID == 0");
          }
          if(getSystemMuniCacheManager().isCachingEnabled()){
              return getSystemMuniCacheManager().getCacheMuniProfile().get(profileID, k -> fetchMuniProfile(profileID));
          } else {
              return fetchMuniProfile(profileID);
          }
          
    }
    
     /**
     * Extracts a record from the muniprofile table and builds a MuniProfile Object
     * @param profileID
     * @return
     * @throws DatabaseFetchRuntimeException
     */
    public MuniProfile fetchMuniProfile(int profileID) throws DatabaseFetchRuntimeException{
        MuniProfile mp = null;
        PreparedStatement stmt = null;
        Connection con = null;
        // note that muniCode is not returned in this query since it is specified in the WHERE
        StringBuilder query =  new StringBuilder();
                        
                        query.append("""
                            SELECT profileid, title, description, 
                                    notes, continuousoccupancybufferdays, novfollowupdefaultdays, priorityparamdeadlineadminbufferdays, 
                                    priorityparamnoletterbufferdays, priorityparamprioritizeletterfollowupbuffer, priorityparamalloweventcatgreenbuffers, 
                                    ceoreqconductinspections, ceoreqfinalizeinspections, ceoreqissuepermits, 
                                    ceoreqattachviolationtocase, ceoreqfinalizenov, ceoreqclosecase, 
                                    ceoreqlogcitation, ceorequpdatecitation, managerreqabandonement, 
                                    managerreqclosececase, managerreqcitationopenclose, managerreqpermitfinalize, 
                                    managerreqinspectionfinalize, managerreqnovfinalize, managerreqviolationextstipcomp, 
                                    managerreqchecklistedit, managerreqcodesourcemanage, managerreqcodebookmanage, autoloadopencases, autoloadopenoccperiods,
                                    createdts, createdby_umapid, lastupdatedts, 
                                    lastupdatedby_umapid, deactivatedts, deactivatedby_umapid, parcelmadlinkrolefilter_lorid,
                                    defaultinspectionfollowupwindow, followupeventcatfailedfin_categoryid, followupeventcatexpiredtco_categoryid,
                                    eventpermitissued_categoryid, eventinspectionpassedperformed_categoryid, eventinspectionfailedperformed_categoryid, 
                                    dispatchfieldinspectionbydefault, defaultcecaseoriginationevent_catid
                            	FROM public.muniprofile WHERE profileid IS NOT NULL AND profileid = ?; 
                        """);
                                
        ResultSet rs = null;
 
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query.toString());
            stmt.setInt(1, profileID);
            rs = stmt.executeQuery();
            while(rs.next()){
                mp = generateMuniProfile(rs);
            }
            
        } catch (SQLException | BObStatusException | IntegrationException ex) {
            System.out.println("MunicipalityIntegrator.fetchMuniprofile | " + ex.toString());
            throw new DatabaseFetchRuntimeException("Exception in MunicipalityIntegrator.fetchMuniProfile", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return mp;
    }
    
    /**
     * Internal generator to 
     * @param rs
     * @return
     * @throws SQLException
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    private MuniProfile generateMuniProfile(ResultSet rs) 
            throws SQLException, IntegrationException, BObStatusException{
        
        MuniProfile mp = new MuniProfile();
        
        EventCoordinator ec = getEventCoordinator();
        SystemIntegrator si = getSystemIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        
        mp.setProfileID(rs.getInt("profileid"));
        mp.setTitle(rs.getString("title"));
        mp.setDescription(rs.getString("description"));
        
        mp.setNotes(rs.getString("notes"));
        mp.setContinuousoccupancybufferdays(rs.getInt("continuousoccupancybufferdays"));
        mp.setNovDefaultDaysForFollowup(rs.getInt("novfollowupdefaultdays"));
        
        // search
        if(rs.getInt("parcelmadlinkrolefilter_lorid") != 0){
            mp.setPropSearchFilter(sc.getLinkedObjectRole(rs.getInt("parcelmadlinkrolefilter_lorid")));
        }
        
        // random switches
        mp.setDispatchInspectionsByDefault(rs.getBoolean("dispatchfieldinspectionbydefault"));
        if(rs.getInt("defaultcecaseoriginationevent_catid") != 0){
            mp.setDefaultCECaseOriginationCategory(ec.getEventCategory(rs.getInt("defaultcecaseoriginationevent_catid")));
        }
        
        
        // session init
        mp.setAutoloadOpenCECases(rs.getBoolean("autoloadopencases"));
        mp.setAutoloadOpenOccPeriods(rs.getBoolean("autoloadopenoccperiods"));
        
        
        // priority params
        mp.setPriorityParamDeadlineAdministrativeBufferDays(rs.getInt("priorityparamdeadlineadminbufferdays"));
        mp.setPriorityParamLetterSendBufferDays(rs.getInt("priorityparamnoletterbufferdays"));
        mp.setPrioritizeLetterFollowUpBuffer(rs.getBoolean("priorityparamprioritizeletterfollowupbuffer"));
        mp.setPriorityAllowEventCategoryGreenBuffers(rs.getBoolean("priorityparamalloweventcatgreenbuffers"));
        
        // CEO Permissions params
        mp.setRequireCodeOfficerTrueConductInspection(rs.getBoolean("ceoreqconductinspections"));
        mp.setRequireCodeOfficerTrueFinalizeInspection(rs.getBoolean("ceoreqfinalizeinspections"));
        mp.setRequireCodeOfficerTrueIssuePermits(rs.getBoolean("ceoreqissuepermits"));
        mp.setRequireCodeOfficerTrueAttachViolToCECase(rs.getBoolean("ceoreqattachviolationtocase"));

        mp.setRequireCodeOfficerTrueFinalizeNOV(rs.getBoolean("ceoreqfinalizenov"));
        mp.setRequireCodeOfficerTrueCloseCECase(rs.getBoolean("ceoreqclosecase"));
        mp.setRequireCodeOfficerTrueOpenCitation(rs.getBoolean("ceoreqlogcitation"));
        mp.setRequireCodeOfficerTrueUpdateCitation(rs.getBoolean("ceorequpdatecitation"));
        
        // manager permissions
        mp.setManagerRequiredAbandonment(rs.getBoolean("managerreqabandonement"));
        mp.setManagerRequiredcCloseCECase(rs.getBoolean("managerreqclosececase"));
        mp.setManagerRequiredCitationOpenClose(rs.getBoolean("managerreqcitationopenclose"));
        mp.setManagerRequiredPermitFinalize(rs.getBoolean("managerreqpermitfinalize"));
        mp.setManagerRequiredInspectionFinalize(rs.getBoolean("managerreqinspectionfinalize"));
        mp.setManagerRequiredNOVFinalize(rs.getBoolean("managerreqnovfinalize"));
        mp.setManagerRequiredViolationExtStipComp(rs.getBoolean("managerreqviolationextstipcomp"));
        mp.setManagerRequiredChecklistEdit(rs.getBoolean("managerreqchecklistedit"));
        mp.setManagerRequiredCodeSourceManage(rs.getBoolean("managerreqcodesourcemanage"));
        mp.setManagerRequiredCodeBookManage(rs.getBoolean("managerreqcodebookmanage"));
        
        // event and followup defaults
        mp.setDefaulFailedFINFollowupDays(rs.getInt("defaultinspectionfollowupwindow"));
        if(rs.getInt("followupeventcatfailedfin_categoryid") != 0){
            mp.setEventCatFailedFINFollowUp(ec.getEventCategory(rs.getInt("followupeventcatfailedfin_categoryid")));
        }
        if(rs.getInt("followupeventcatexpiredtco_categoryid") != 0){
            mp.setEventCatExpiredTCOFollowUp(ec.getEventCategory(rs.getInt("followupeventcatexpiredtco_categoryid")));
        }
        
        if(rs.getInt("eventpermitissued_categoryid") != 0){
            mp.setEventCatPermitIssued(ec.getEventCategory(rs.getInt("eventpermitissued_categoryid")));
        }
        
        if(rs.getInt("eventinspectionpassedperformed_categoryid") != 0){
            mp.setEventCatFINPassed(ec.getEventCategory(rs.getInt("eventinspectionpassedperformed_categoryid")));
        }
        
        if(rs.getInt("eventinspectionfailedperformed_categoryid") != 0){
            mp.setEventCatFINFailed(ec.getEventCategory(rs.getInt("eventinspectionfailedperformed_categoryid")));
        }
        
        
        si.populateUMAPTrackedFields(mp, rs, true);
        
        return mp;
    }
    
    
    /**
     * Builds a complete list of all municipality profiles
     * @param includeInactive
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public List<Integer> getMuniProfileList(boolean includeInactive) throws IntegrationException, BObStatusException {

        StringBuilder query = new StringBuilder();
        query.append("SELECT profileid FROM public.muniprofile WHERE profileid IS NOT NULL; ") ;
        
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ArrayList<Integer> muniProfileList = new ArrayList<>();

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query.toString());
            rs = stmt.executeQuery();
            while (rs.next()) {
                muniProfileList.add(rs.getInt("profileid"));
            }

        } catch (SQLException ex) {
            throw new IntegrationException("Exception in MunicipalityIntegrator.getMuniProfileList", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 

        return muniProfileList;
    }
    

    
     /**
     * Sets the defaultcodeset field on the muni table
     * @param set
     * @param muni
     * @throws BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void mapCodeSetAsMuniDefault(CodeSet set, Municipality muni) throws BObStatusException, IntegrationException{
        
        if(set == null || muni == null){
            throw new BObStatusException("Cannot link set and muni with null set or muni!");
            
        }
        
        String query = "UPDATE municipality SET defaultcodeset=? WHERE municode=?; ";
        Connection con = null;
        PreparedStatement stmt = null;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, set.getCodeSetID());
            stmt.setInt(2, muni.getMuniCode());
            stmt.executeUpdate();
            
        } catch (SQLException ex) { 
             System.out.println(ex);
             throw new IntegrationException("Database exception making code set active in given muni", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
    }
    
    
    
    
    /**
     * Updates a muni profile object
     * @param profile
     * @throws BObStatusException
     * @throws IntegrationException 
     */
     public int insertMuniProfile(MuniProfile profile) throws BObStatusException, IntegrationException{
        
        if(profile == null ){
            throw new BObStatusException("Cannot insert a null muniprofile object");
            
        }
        
        String query = """
                        INSERT INTO public.muniprofile(
                                        	profileid, title, description, 
                                                notes, continuousoccupancybufferdays, novfollowupdefaultdays, priorityparamdeadlineadminbufferdays, 
                                                priorityparamnoletterbufferdays, priorityparamprioritizeletterfollowupbuffer, priorityparamalloweventcatgreenbuffers, 
                                                ceoreqconductinspections, ceoreqfinalizeinspections, ceoreqissuepermits, 
                                                ceoreqattachviolationtocase, ceoreqfinalizenov, ceoreqclosecase, 
                                                ceoreqlogcitation, ceorequpdatecitation, managerreqabandonement, 
                                                managerreqclosececase, managerreqcitationopenclose, managerreqpermitfinalize, 
                                                managerreqinspectionfinalize, managerreqnovfinalize, managerreqviolationextstipcomp, 
                                                managerreqchecklistedit, managerreqcodesourcemanage, managerreqcodebookmanage, 
                                                autoloadopencases, autoloadopenoccperiods,
                                                createdts, createdby_umapid, lastupdatedts, 
                                                lastupdatedby_umapid, deactivatedts, deactivatedby_umapid, parcelmadlinkrolefilter_lorid,
                                                defaultinspectionfollowupwindow, followupeventcatfailedfin_categoryid, followupeventcatexpiredtco_categoryid,
                                                eventpermitissued_categoryid, eventinspectionpassedperformed_categoryid, eventinspectionfailedperformed_categoryid, 
                                                dispatchfieldinspectionbydefault, defaultcecaseoriginationevent_catid)
                                        	VALUES (DEFAULT, ?, ?,  
                                                        ?, ?, ?, ?, 
                                                        ?, ?, ?, 
                                                        ?, ?, ?, 
                                                        ?, ?, ?, 
                                                        ?, ?, ?, 
                                                        ?, ?, ?, 
                                                        ?, ?, ?, 
                                                        ?, ?, ?,
                                                        ?, ?,
                                                        now(), ?, now(), 
                                                        ?, NULL, NULL, ?,
                                                        ?, ?, ?,
                                                        ?, ?, ?,
                                                        ?, ?);
                       """;
        Connection con = null;
        PreparedStatement stmt = null;
        int freshID = 0;
        ResultSet rs = null;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setString(1, profile.getTitle());
            stmt.setString(2, profile.getDescription());
            
            stmt.setString(3, profile.getNotes());
            stmt.setInt(4, profile.getContinuousoccupancybufferdays());
            stmt.setInt(5, profile.getNovDefaultDaysForFollowup());
            stmt.setInt(6, profile.getPriorityParamDeadlineAdministrativeBufferDays());
            
            stmt.setInt(7, profile.getPriorityParamLetterSendBufferDays());
            stmt.setBoolean(8, profile.isPrioritizeLetterFollowUpBuffer());
            stmt.setBoolean(9, profile.isPriorityAllowEventCategoryGreenBuffers());
            
            stmt.setBoolean(10, profile.isRequireCodeOfficerTrueConductInspection());
            stmt.setBoolean(11, profile.isRequireCodeOfficerTrueFinalizeInspection());
            stmt.setBoolean(12, profile.isRequireCodeOfficerTrueIssuePermits());
            
            stmt.setBoolean(13, profile.isRequireCodeOfficerTrueAttachViolToCECase());
            stmt.setBoolean(14, profile.isRequireCodeOfficerTrueFinalizeNOV());
            stmt.setBoolean(15, profile.isRequireCodeOfficerTrueCloseCECase());
            
            stmt.setBoolean(16, profile.isRequireCodeOfficerTrueOpenCitation());
            stmt.setBoolean(17, profile.isRequireCodeOfficerTrueUpdateCitation());
            stmt.setBoolean(18, profile.isManagerRequiredAbandonment());
            
            stmt.setBoolean(19, profile.isManagerRequiredcCloseCECase());
            stmt.setBoolean(20, profile.isManagerRequiredCitationOpenClose());
            stmt.setBoolean(21, profile.isManagerRequiredPermitFinalize());
            
            stmt.setBoolean(22, profile.isManagerRequiredInspectionFinalize());
            stmt.setBoolean(23, profile.isManagerRequiredNOVFinalize());
            stmt.setBoolean(24, profile.isManagerRequiredViolationExtStipComp());
            
            stmt.setBoolean(25, profile.isManagerRequiredChecklistEdit());
            stmt.setBoolean(26, profile.isManagerRequiredCodeSourceManage());
            stmt.setBoolean(27, profile.isManagerRequiredCodeBookManage());
            
            stmt.setBoolean(28, profile.isAutoloadOpenCECases());
            stmt.setBoolean(29, profile.isAutoloadOpenOccPeriods());
            
            // createdts using pg now()
            if(profile.getCreatedby_UMAP() != null){
                stmt.setInt(30, profile.getCreatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(30, java.sql.Types.NULL);
            }
            // lastupdatedts using pg now()
            
            if(profile.getLastUpdatedby_UMAP()!= null){
                stmt.setInt(31, profile.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(31, java.sql.Types.NULL);
            }
            
            if(profile.getPropSearchFilter() != null){
                stmt.setInt(32, profile.getPropSearchFilter().getRoleID());
            } else {
                stmt.setNull(32, java.sql.Types.NULL);
            }
            
            stmt.setInt(33, profile.getDefaulFailedFINFollowupDays());
            
            // Auto-generated eventing
            if(profile.getEventCatFailedFINFollowUp() != null){
                stmt.setInt(34, profile.getEventCatFailedFINFollowUp().getCategoryID());
            } else {
                stmt.setNull(34, java.sql.Types.NULL);
            }
            if(profile.getEventCatExpiredTCOFollowUp() != null){
                stmt.setInt(35, profile.getEventCatExpiredTCOFollowUp().getCategoryID());
            } else {
                stmt.setNull(35, java.sql.Types.NULL);
            }
            
            if(profile.getEventCatPermitIssued()!= null){
                stmt.setInt(36, profile.getEventCatPermitIssued().getCategoryID());
            } else {
                stmt.setNull(36, java.sql.Types.NULL);
            }
            
            if(profile.getEventCatFINPassed()!= null){
                stmt.setInt(37, profile.getEventCatFINPassed().getCategoryID());
            } else {
                stmt.setNull(37, java.sql.Types.NULL);
            }
            
            if(profile.getEventCatFINFailed() != null){
                stmt.setInt(38, profile.getEventCatFINFailed().getCategoryID());
            } else {
                stmt.setNull(38, java.sql.Types.NULL);
            }
            
            stmt.setBoolean(39, profile.isDispatchInspectionsByDefault());
            
            if(profile.getDefaultCECaseOriginationCategory() != null){
                stmt.setInt(40, profile.getDefaultCECaseOriginationCategory().getCategoryID());
            } else {
                stmt.setNull(40, java.sql.Types.NULL);
            }
            
            
            
            // no dac alllowed on insert
            stmt.execute();

            String retrievalQuery = "SELECT currval('muni_muniprofile_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();
            while(rs.next()){
                freshID = rs.getInt(1);
            }
        } catch (SQLException ex) { 
             System.out.println(ex);
             throw new IntegrationException("Database exception inserting muni profile", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
      
        
        return freshID;
    }
    
    /**
     * Updates a muni profile object
     * @param profile
     * @throws BObStatusException
     * @throws IntegrationException 
     */
     public void updateMuniProfile(MuniProfile profile) throws BObStatusException, IntegrationException{
        
        if(profile == null ){
            throw new BObStatusException("Cannot update a null muniprofile object");
        }
        
        String query = """
                       UPDATE public.muniprofile
                       	SET title=?, description=?, 
                            notes=?, continuousoccupancybufferdays=?, novfollowupdefaultdays=?, priorityparamdeadlineadminbufferdays=?, 
                            priorityparamnoletterbufferdays=?, priorityparamprioritizeletterfollowupbuffer=?, priorityparamalloweventcatgreenbuffers=?, 
                            ceoreqconductinspections=?, ceoreqfinalizeinspections=?, ceoreqissuepermits=?, 
                            ceoreqattachviolationtocase=?, ceoreqfinalizenov=?, ceoreqclosecase=?, 
                            ceoreqlogcitation=?, ceorequpdatecitation=?, managerreqabandonement=?, 
                            managerreqclosececase=?, managerreqcitationopenclose=?, managerreqpermitfinalize=?, 
                            managerreqinspectionfinalize=?, managerreqnovfinalize=?, managerreqviolationextstipcomp=?, 
                            managerreqchecklistedit=?, managerreqcodesourcemanage=?, managerreqcodebookmanage=?, 
                            autoloadopencases=?, autoloadopenoccperiods=?,
                            lastupdatedts=now(), 
                            lastupdatedby_umapid=?, deactivatedts=NULL, deactivatedby_umapid=NULL, parcelmadlinkrolefilter_lorid=?,
                            defaultinspectionfollowupwindow=?, followupeventcatfailedfin_categoryid=?, followupeventcatexpiredtco_categoryid=?,
                            eventpermitissued_categoryid=?, eventinspectionpassedperformed_categoryid=?, eventinspectionfailedperformed_categoryid=?,
                            dispatchfieldinspectionbydefault=?, defaultcecaseoriginationevent_catid=?
                       	WHERE profileid=?;
                       """;
        Connection con = null;
        PreparedStatement stmt = null;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            
            stmt.setString(1, profile.getTitle());
            stmt.setString(2, profile.getDescription());
            
            stmt.setString(3, profile.getNotes());
            stmt.setInt(4, profile.getContinuousoccupancybufferdays());
            stmt.setInt(5, profile.getNovDefaultDaysForFollowup());
            stmt.setInt(6, profile.getPriorityParamDeadlineAdministrativeBufferDays());
            
            stmt.setInt(7, profile.getPriorityParamLetterSendBufferDays());
            stmt.setBoolean(8, profile.isPrioritizeLetterFollowUpBuffer());
            stmt.setBoolean(9, profile.isPriorityAllowEventCategoryGreenBuffers());
            
            stmt.setBoolean(10, profile.isRequireCodeOfficerTrueConductInspection());
            stmt.setBoolean(11, profile.isRequireCodeOfficerTrueFinalizeInspection());
            stmt.setBoolean(12, profile.isRequireCodeOfficerTrueIssuePermits());
            
            stmt.setBoolean(13, profile.isRequireCodeOfficerTrueAttachViolToCECase());
            stmt.setBoolean(14, profile.isRequireCodeOfficerTrueFinalizeNOV());
            stmt.setBoolean(15, profile.isRequireCodeOfficerTrueCloseCECase());
            
            stmt.setBoolean(16, profile.isRequireCodeOfficerTrueOpenCitation());
            stmt.setBoolean(17, profile.isRequireCodeOfficerTrueUpdateCitation());
            stmt.setBoolean(18, profile.isManagerRequiredAbandonment());
            
            stmt.setBoolean(19, profile.isManagerRequiredcCloseCECase());
            stmt.setBoolean(20, profile.isManagerRequiredCitationOpenClose());
            stmt.setBoolean(21, profile.isManagerRequiredPermitFinalize());
            
            stmt.setBoolean(22, profile.isManagerRequiredInspectionFinalize());
            stmt.setBoolean(23, profile.isManagerRequiredNOVFinalize());
            stmt.setBoolean(24, profile.isManagerRequiredViolationExtStipComp());
            
            stmt.setBoolean(25, profile.isManagerRequiredChecklistEdit());
            stmt.setBoolean(26, profile.isManagerRequiredCodeSourceManage());
            stmt.setBoolean(27, profile.isManagerRequiredCodeBookManage());
            
            stmt.setBoolean(28, profile.isAutoloadOpenCECases());
            stmt.setBoolean(29, profile.isAutoloadOpenOccPeriods());
           
            // lastupdatedts using pg now()
            if(profile.getLastUpdatedby_UMAP()!= null){
                stmt.setInt(30, profile.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(30, java.sql.Types.NULL);
            }
             if(profile.getPropSearchFilter() != null){
                stmt.setInt(31, profile.getPropSearchFilter().getRoleID());
            } else {
                stmt.setNull(31, java.sql.Types.NULL);
            }
             
             
            stmt.setInt(32, profile.getDefaulFailedFINFollowupDays());
            
            if(profile.getEventCatFailedFINFollowUp() != null){
                stmt.setInt(33, profile.getEventCatFailedFINFollowUp().getCategoryID());
            } else {
                stmt.setNull(33, java.sql.Types.NULL);
            }
            if(profile.getEventCatExpiredTCOFollowUp() != null){
                stmt.setInt(34, profile.getEventCatExpiredTCOFollowUp().getCategoryID());
            } else {
                stmt.setNull(34, java.sql.Types.NULL);
            }
            
             if(profile.getEventCatPermitIssued()!= null){
                stmt.setInt(35, profile.getEventCatPermitIssued().getCategoryID());
            } else {
                stmt.setNull(35, java.sql.Types.NULL);
            }
            
            if(profile.getEventCatFINPassed()!= null){
                stmt.setInt(36, profile.getEventCatFINPassed().getCategoryID());
            } else {
                stmt.setNull(36, java.sql.Types.NULL);
            }
            
            if(profile.getEventCatFINFailed() != null){
                stmt.setInt(37, profile.getEventCatFINFailed().getCategoryID());
            } else {
                stmt.setNull(37, java.sql.Types.NULL);
            }
            
            stmt.setBoolean(38, profile.isDispatchInspectionsByDefault());
            
            if(profile.getDefaultCECaseOriginationCategory() != null){
                stmt.setInt(39, profile.getDefaultCECaseOriginationCategory().getCategoryID());
            } else {
                stmt.setNull(39, java.sql.Types.NULL);
            }
            
            
            stmt.setInt(41, profile.getProfileID());
            stmt.executeUpdate();
        } catch (SQLException ex) { 
             System.out.println(ex);
             throw new IntegrationException("Database exception updating muni profile", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
      
        
    }
    
     /**
     * Deactivates a municipality
     * @param profile
     * @throws BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void deactivateMuniProfile(MuniProfile profile) throws BObStatusException, IntegrationException{
        if(profile == null || profile.getDeactivatedBy_UMAP() == null){
            throw new BObStatusException("Cannot deactivate muni profile with null muni or null deactivating UMAP!");
            
        }
        String query = "UPDATE muniprofile SET deactivatedts=now(), deactivatedby_umapid=? WHERE municode=?; ";
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            
            stmt.setInt(1, profile.getDeactivatedBy_UMAP().getUserMuniAuthPeriodID());
            stmt.setInt(2, profile.getProfileID());
            stmt.executeUpdate();
            
        } catch (SQLException ex) { 
             System.out.println(ex);
             throw new IntegrationException("Integration error deactivating muni profile", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
        
    }


}
