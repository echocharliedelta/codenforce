/*
 * Copyright (C) 2018 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.IFace_pinnable;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheClient;
import com.tcvcog.tcvce.application.interfaces.IFace_Loggable;
import com.tcvcog.tcvce.coordinators.MunicipalityCoordinator;
import com.tcvcog.tcvce.coordinators.PersonCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.coordinators.UserCoordinator;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.DatabaseFetchRuntimeException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.BOBSource;
import com.tcvcog.tcvce.entities.CECaseDataHeavy;
import com.tcvcog.tcvce.entities.IFace_noteHolder;
import com.tcvcog.tcvce.entities.IFace_trackedEntityLink;
import com.tcvcog.tcvce.entities.Icon;
import com.tcvcog.tcvce.entities.PropertyUseType;
import com.tcvcog.tcvce.entities.ImprovementSuggestion;
import com.tcvcog.tcvce.entities.IntensityClass;
import com.tcvcog.tcvce.entities.IntensitySchema;
import com.tcvcog.tcvce.entities.LinkedObjectRole;
import com.tcvcog.tcvce.entities.LinkedObjectSchemaEnum;
import com.tcvcog.tcvce.entities.ListChangeRequest;
import com.tcvcog.tcvce.entities.Manageable;
import com.tcvcog.tcvce.entities.ManagedSchemaEnum;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.Person;
import com.tcvcog.tcvce.entities.PrintStyle;
import com.tcvcog.tcvce.entities.Property;
import com.tcvcog.tcvce.entities.TrackedEntity;
import com.tcvcog.tcvce.entities.UMAPTrackedEntity;
import com.tcvcog.tcvce.entities.User;
import com.tcvcog.tcvce.entities.UserAuthorized;
import com.tcvcog.tcvce.entities.XMuniActivationRecord;
import com.tcvcog.tcvce.entities.occupancy.OccPeriod;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import com.tcvcog.tcvce.system.DatabaseUtils;
import jakarta.annotation.PostConstruct;
import jakarta.ejb.EJB;

/**
 * A catch all location for database operations against tables
 * that support Business objects across all sorts of subsystems
 * 
 * @author ellen bascomb of apt 31y
 */
public  class       SystemIntegrator 
        extends     BackingBeanUtils 
        implements  Serializable, IFaceCacheClient {

// This annotation doesn't work yet. security config problem
//    @EJB
    private DatabaseUtils dbConFactory;
        
    
    /**
     * Creates a new instance of SystemIntegrator
     */
    public SystemIntegrator() {
        System.out.println("SystemIntegrator.SystemIntegrator");
        
    }
    
    @PostConstruct
    public void initBean() {
        System.out.println("SystemIntegrator.initBean");
//        enableCaching(); called during registration
        registerClientWithManager();
    }
  
    
    
    
    @Override
    public void registerClientWithManager() {
        getSystemMuniCacheManager().registerCacheClient(this);
    }
    
   
    
    
    /* ************************************************************************
    ******************          NOTES                           ***************
    ***************************************************************************/ 
    
    
    /**
     * The Universal Note Appending method
     * @param nh with the notes ready for insertion. NO PROCESSING OF THE NOTE
     * 
     * field is done by this method. Use the Universal note appending tools
     * of this class to do so and only pass it here when the new note and meta data
     * has been appended to the original notes and that whole blob written back 
     * into notes. I just call getNotes() and stick that sucker in!
     * @throws IntegrationException 
     */
    public void writeNotes(IFace_noteHolder nh) throws IntegrationException{
         if(nh != null && nh.getDBKey() != 0){
            
            StringBuilder sb = new StringBuilder();
            sb.append("UPDATE ");
            sb.append(nh.getDBTableName());
            sb.append(" SET notes=? WHERE ");
            sb.append(nh.getPKFieldName());
            sb.append("=?;");
            Connection con = getPostgresCon();
            PreparedStatement stmt = null;

            try {
                stmt = con.prepareStatement(sb.toString());
                stmt.setString(1, nh.getNotes());
                stmt.setInt(2, nh.getDBKey());
                
                stmt.executeUpdate();

            } catch (SQLException ex) {
                System.out.println(ex.toString());
                throw new IntegrationException("Tracked Entity has been deactivated", ex);

            } finally{
                releasePostgresConnection(con, stmt);
            } 
        }
        
        
    }
    
    
    /* ************************************************************************
    ******************          PINNING                         ***************
    ***************************************************************************/ 
    
    
    /**
     * Writes an entry to the pinning tables for a given pinnable
     * @param pinnable 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void pinObject(IFace_pinnable pinnable) throws IntegrationException{
    
        if(pinnable == null || pinnable.getDBKey() == 0 || pinnable.getPinner() == null){
            throw new IntegrationException("Cannot pin object with null pinnable, user, or cecase");
        }
        
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(pinnable.getPinTableName());
        sb.append("(");
        sb.append(pinnable.getPinTableFKString());
        sb.append(", pinnedby_userid, createdts, deactivatedts) VALUES (?, ?, now(), NULL);");
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sb.toString());
            stmt.setInt(1, pinnable.getPinner().getUserID());
            stmt.setInt(2, pinnable.getDBKey());

            stmt.execute();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("SystemIntegrator.pinObject | Tracked Entity has been deactivated", ex);

        } finally{
             releasePostgresConnection(con, stmt);
        } 
    }
    
    /**
     * Deactivates the pinning entry for the given pinnable
     * @param pinnable 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void unPinObject(IFace_pinnable pinnable) throws IntegrationException{
        
        if(pinnable == null || pinnable.getDBKey() == 0 || pinnable.getPinner() == null){
            throw new IntegrationException("Cannot pin object with null pinnable, user, or cecase");
        }
        
        StringBuilder sb = new StringBuilder();
        
        sb.append("UPDATE public.cecasepin\n");
        sb.append("   SET deactivatedts=now() WHERE ");
        sb.append(pinnable.getPinTableFKString());
        sb.append("=? AND pinnedby_userid=?;");
        
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sb.toString());
            stmt.setInt(1, pinnable.getDBKey());
            stmt.setInt(2, pinnable.getPinner().getUserID());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("SystemIntegrator.unPinObject: Database error: Pin record cannot be deactivated", ex);

        } finally{
             releasePostgresConnection(con, stmt);
        } 
    }
    
    
    /**
     * Queries the appropriate pinning table to determine the 
     * current pinning status of the given pinnable
     * @param pinnable
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public boolean getPinnedStatus(IFace_pinnable pinnable) throws IntegrationException{
        boolean pinned = false;
        if(pinnable == null || pinnable.getPinner() == null){
            throw new IntegrationException("Cannot query pinned status with null pinable or null user");
        }
        

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT createdts ");
        sb.append(" FROM ");
        sb.append(pinnable.getPinTableName());
        sb.append(" WHERE ");
        sb.append(pinnable.getPinTableFKString());
        sb.append("=? AND pinnedby_userid=?;");

        try {
            stmt = con.prepareStatement(sb.toString());

            stmt.setInt(1, pinnable.getDBKey());
            stmt.setInt(2, pinnable.getPinner().getUserID());

            rs = stmt.executeQuery();

            while (rs.next()) { 
                System.out.println("SystemIntegrator.getPinnedStatus | Found record in " + pinnable.getPinTableName() + " for PK " + pinnable.getDBKey());
                pinned = true;
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("SystemIntegrator.getPinnedStatus | fatal integration error", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return pinned;
    }
    
    /* ************************************************************************
    ******************          STANDARD FIELD TRACKING        ***************
    ***************************************************************************/ 
       
    /**
     * Utility method for populating record tracking fields:
     * createdts
     * createdby_userid
     * lastupdatedts
     * lastupdatedby_userid
     * deactivatedts
     * deactivated_userid
     * @param te
     * @param rs
     * @param userIDOnly To avoid recursion, set to True to only inject UserIDs on 
     * creation, update, and deactivation 
     * @throws SQLException 
     */
    public void populateTrackedFields(TrackedEntity te, ResultSet rs, boolean userIDOnly) throws SQLException, IntegrationException, BObStatusException{
        UserCoordinator uc = getUserCoordinator();
        // temporary test: force all callers to use ID only -- MAY 2023
        
        if(rs != null){
                                
            if(rs.getTimestamp("createdts") != null){
                te.setCreatedTS(rs.getTimestamp("createdts").toLocalDateTime());                
            }
            if(rs.getInt("createdby_userid") != 0){
                if(userIDOnly){
                    te.setCreatedByUserID(rs.getInt("createdby_userid"));
                } else {
                    te.setCreatedBy(uc.user_getUser(rs.getInt("createdby_userid")));
                }
            }
            
            if(rs.getTimestamp("lastupdatedts") != null){
                te.setLastUpdatedTS(rs.getTimestamp("lastupdatedts").toLocalDateTime());
            }
            if(rs.getInt("lastupdatedby_userid") != 0){
                if(userIDOnly){
                    te.setLastUpdatedByUserID((rs.getInt("lastupdatedby_userid")));
                } else {
                    te.setLastUpdatedBy(uc.user_getUser(rs.getInt("lastupdatedby_userid")));
                }
            }
            
            if(rs.getTimestamp("deactivatedts") != null){
                 te.setDeactivatedTS(rs.getTimestamp("deactivatedts").toLocalDateTime());
            }
            if(rs.getInt("deactivatedby_userid") != 0){
                if(userIDOnly){
                    te.setDeactivatedByUserID(rs.getInt("deactivatedby_userid"));
                } else {
                    te.setDeactivatedBy(uc.user_getUser(rs.getInt("deactivatedby_userid")));
                }
            }
        }
    }
    
    /**
     * Adapter method for legacy uses of the non-boolean option version to avoid loops
     * @param ute
     * @param rs
     * @throws SQLException
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public void populateUMAPTrackedFields(UMAPTrackedEntity ute, ResultSet rs) throws SQLException, IntegrationException, BObStatusException{
        populateUMAPTrackedFields(ute, rs, false);
    }
    
    /**
     * Utility method for populating UMAP based tracked fields, 
     * the third generation of record management
     * @param ute
     * @param rs
     * @param useUMAPDIOnly flag to just write the UMAP ID to an int field rather than inject the entire UMAP object
     * @throws SQLException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void populateUMAPTrackedFields(UMAPTrackedEntity ute, ResultSet rs, boolean useUMAPDIOnly) throws SQLException, IntegrationException, BObStatusException{
        UserCoordinator uc = getUserCoordinator();
        
        if(ute != null && rs != null){
            
            if(rs.getTimestamp("createdts") != null){
                ute.setCreatedTS(rs.getTimestamp("createdts").toLocalDateTime());                
            }
            if(rs.getInt("createdby_umapid") != 0){
                if(useUMAPDIOnly){
                    ute.setCreatedBy_UMAPID(rs.getInt("createdby_umapid"));
                } else {
                    ute.setCreatedby_UMAP(uc.auth_getUMAPNotValidated(rs.getInt("createdby_umapid")));
                }
            }
            
            if(rs.getTimestamp("lastupdatedts") != null){
                ute.setLastUpdatedTS(rs.getTimestamp("lastupdatedts").toLocalDateTime());
            }
            if(rs.getInt("lastupdatedby_umapid") != 0){
                if(useUMAPDIOnly){
                    ute.setLastUpdatedBy_UMAPID(rs.getInt("lastupdatedby_umapid"));
                } else {
                    ute.setLastUpdatedby_UMAP(uc.auth_getUMAPNotValidated(rs.getInt("lastupdatedby_umapid")));
                }
            }
            
            if(rs.getTimestamp("deactivatedts") != null){
                ute.setDeactivatedTS(rs.getTimestamp("deactivatedts").toLocalDateTime());
            }
            if(rs.getInt("deactivatedby_umapid") != 0){
                if(useUMAPDIOnly){
                    ute.setDeactivatedBy_UMAPID(rs.getInt("deactivatedby_umapid"));
                } else{
                    ute.setDeactivatedBy_UMAP(uc.auth_getUMAPNotValidated(rs.getInt("deactivatedby_umapid")));
                }
            }
        }
    }
    
    /**
     * Writes the current timestamp and UMAP id of an object that extends
     * UMAPTrackedEntity 
     * 
     * @param ute to mark as having been updated
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void updateStampUMAPTrackedEntity(UMAPTrackedEntity ute) throws IntegrationException{
        if(ute == null){
            throw new IntegrationException("Cannot update NULL entity");
        }
        
        StringBuilder sb = new StringBuilder();
        
        sb.append("UPDATE ");
        sb.append(ute.getDBTableName());
        sb.append(" SET lastupdatedts=now(), lastupdatedby_umapid=? WHERE ");
        sb.append(ute.getPKFieldName());
        sb.append("=?;");
        
        Connection con = getPostgresCon();
        
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sb.toString());
            stmt.setInt(1, ute.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            stmt.setInt(2, ute.getDBKey());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("UMAP Tracked Entity could not be updaed", ex);

        } finally{
             releasePostgresConnection(con, stmt);
        } 
        
    }
    
    
    /**
     * Writes a deactivation TS and UMAP id of the given object that extends
     * UMAPTrackedEntity. Caller must write the deactivatedby_UMAP
     * 
     * @param ute MUST have a UMAP object with keycard in getDeactivatedBy_UMAP!!
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void deactivateUMAPTrackedEntity(UMAPTrackedEntity ute) throws IntegrationException{
        if(ute == null || ute.getDeactivatedBy_UMAP() == null){
            throw new IntegrationException("Cannot deactivate NULL entity");
        }
        
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ");
        sb.append(ute.getDBTableName());
        sb.append(" SET deactivatedts=now(), deactivatedby_umapid=? WHERE ");
        sb.append(ute.getPKFieldName());
        sb.append("=?;");
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sb.toString());
            stmt.setInt(1, ute.getDeactivatedBy_UMAP().getUserMuniAuthPeriodID());
            stmt.setInt(2, ute.getDBKey());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("UMAP Tracked Entity could not be deactivated", ex);

        } finally{
             if (con != null) { try { con.close(); } catch (SQLException e) { /* ignored */} }
             if (stmt != null) { try { stmt.close(); } catch (SQLException e) { /* ignored */} }
        } 
        
    }

    
    /**
     * Utility method for populating record tracking fields:
     * createdts
     * createdby_userid
     * lastupdatedts
     * lastupdatedby_userid
     * deactivatedts
     * deactivated_userid
     * @param te
     * @param rs
     * @throws SQLException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    protected void populateTrackedLinkFields(IFace_trackedEntityLink te, ResultSet rs) throws SQLException, IntegrationException, BObStatusException{
        UserIntegrator ui = getUserIntegrator();
        
        if(rs != null){
            
            if(rs.getTimestamp("createdts") != null){
                te.setLinkCreatedTS(rs.getTimestamp("createdts").toLocalDateTime());                
            }
            if(rs.getInt("createdby_userid") != 0){
                te.setLinkCreatedByUserID(rs.getInt("createdby_userid"));
            }
            
            if(rs.getTimestamp("lastupdatedts") != null){
                te.setLinkLastUpdatedTS(rs.getTimestamp("lastupdatedts").toLocalDateTime());
            }
            if(rs.getInt("lastupdatedby_userid") != 0){
                te.setLinkLastUpdatedByUserID(rs.getInt("lastupdatedby_userid"));
            }
            
            if(rs.getTimestamp("deactivatedts") != null){
                te.setLinkDeactivatedTS(rs.getTimestamp("deactivatedts").toLocalDateTime());
            }
            if(rs.getInt("deactivatedby_userid") != 0){
                te.setLinkDeactivatedByUserID(rs.getInt("deactivatedby_userid"));
            }
            if(rs.getInt("source_sourceid") != 0){
                te.setLinkSource(getBOBSource(rs.getInt("source_sourceid")));
            }
            te.setLinkNotes(rs.getString("notes"));
            
        }
    }
    
    
    /** Unified pathway for deactivating TrackedEntities
     * 
     * @param te
     * @param ua
     * @throws IntegrationException 
     */
    public void deactivateTrackedEntity(TrackedEntity te, UserAuthorized ua) throws IntegrationException{
        if(te == null || ua == null){
            throw new IntegrationException("Cannot deac null object or with null deacing user");
        }
            
            
            StringBuilder sb = new StringBuilder();
            sb.append("UPDATE ");
            sb.append(te.getDBTableName());
            sb.append(" SET deactivatedts=now(), deactivatedby_userid=? WHERE ");
            sb.append(te.getPKFieldName());
            sb.append("=?;");
            Connection con = getPostgresCon();
            PreparedStatement stmt = null;

            try {
                stmt = con.prepareStatement(sb.toString());
                stmt.setInt(1, ua.getUserID());
                stmt.setInt(2, te.getDBKey());
                
                stmt.executeUpdate();

            } catch (SQLException ex) {
                System.out.println(ex.toString());
                throw new IntegrationException("Tracked Entity could not be deactivated", ex);

            } finally{
                 releasePostgresConnection(con, stmt);
            } 
        
    }
    
    
    /**
     * Reverses a deactivation action by writing NULL to the deactivatedts and deactivatedby_userid
     * fields of a TrackedEntity
     * 
     * @param te
     * @param ua
     * @throws IntegrationException 
     */
    public void reactivateTrackedEntity(TrackedEntity te, UserAuthorized ua) throws IntegrationException{
         if(te != null && ua != null){
            
            
            StringBuilder sb = new StringBuilder();
            sb.append("UPDATE ");
            sb.append(te.getDBTableName());
            sb.append(" SET deactivatedts=NULL AND deactivatedby_userid=NULL WHERE ");
            sb.append(te.getPKFieldName());
            sb.append("=?;");
            Connection con = getPostgresCon();
            PreparedStatement stmt = null;

            try {
                stmt = con.prepareStatement(sb.toString());
                stmt.setInt(1, te.getDBKey());
                
                stmt.executeUpdate();

            } catch (SQLException ex) {
                System.out.println(ex.toString());
                throw new IntegrationException("Tracked Entity has been deactivated", ex);

            } finally{
                 if (con != null) { try { con.close(); } catch (SQLException e) { /* ignored */} }
                 if (stmt != null) { try { stmt.close(); } catch (SQLException e) { /* ignored */} }
            } 
        }
    }
    
    
    
    /* ************************************************************************
    ******************          DB UTILITIES                    ***************
    ***************************************************************************/ 
    
    
     /**
     * Utility method for checking database status
     * @return a string with all the patches and their TS
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public String getDatabasePatchRecord() throws IntegrationException{
            
            Connection con = getPostgresCon();
            ResultSet rs = null;
            PreparedStatement stmt = null;
            
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT patchnum, patchfilename, datepublished, patchauthor, notes\n" +
                        "  FROM public.dbpatch;");
            StringBuilder res = new StringBuilder();
            try {
                stmt = con.prepareStatement(sb.toString());
                
                rs = stmt.executeQuery();
                
                while (rs.next()) {
                    res.append(rs.getInt("patchnum"));
                    res.append(" | ");
                    
                }
            } catch (SQLException ex) {
                System.out.println(ex.toString());
                throw new IntegrationException("unable to get linked object role", ex);
            } finally {
                releasePostgresConnection(con, stmt, rs);
            } 
        return res.toString();
    }
    
    /* ************************************************************************
    ******************          LINKED OBJECT ROLES             ***************
    ***************************************************************************/ 
    
    
    /**
     * Cached getter for linked object roles
     * @param roleid
     * @return
     * @throws BObStatusException
     * @throws DatabaseFetchRuntimeException 
     */
     public LinkedObjectRole getLinkedObjectRole(int roleid) throws BObStatusException, DatabaseFetchRuntimeException{
         if(roleid == 0){
             throw new BObStatusException("Cannot get linked object role with ID == 0");
         }
         if(getSystemMuniCacheManager().isCachingEnabled()){
            return getSystemMuniCacheManager().getCacheLinkedObjectRole().get(roleid, k -> fetchLinkedObjectRole(roleid)); 
         } else {
             return fetchLinkedObjectRole(roleid);
         }
     }
    
  
    
    /**
     * Retrieves a linked object role record from the DB
     * @param roleid
     * @return the new object or null if roleid is 0
     */
    public LinkedObjectRole fetchLinkedObjectRole(int roleid) throws DatabaseFetchRuntimeException{
        LinkedObjectRole lor = null;
            
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT lorid, lorschema, title, description,  \n" );
        sb.append(" createdts, createdby_umapid, lastupdatedts, lastupdatedby_umapid, deactivatedts, deactivatedby_umapid, notes, sortorder \n");
        sb.append("FROM public.linkedobjectrole WHERE lorid=?;");

        try {
            stmt = con.prepareStatement(sb.toString());
            stmt.setInt(1, roleid);

            rs = stmt.executeQuery();

            while (rs.next()) {
                lor = generateLinkedObjectRole(rs);

            }
        } catch (SQLException | BObStatusException | IntegrationException ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("unable to get linked object role", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return lor;
    }
    
    
    /**
     * Update the data for given LinkedObjectRole object.
     * @param loRole
     * @throws IntegrationException 
     */
    public void updateLinkedObjectRole(LinkedObjectRole loRole) throws IntegrationException{
        String query = """
                UPDATE public.linkedObjectRole SET
                    lorschema = ?, title = ?, description = ?, notes = ?,
                    lastupdatedts = now() ,
                    lastupdatedby_umapid = ?, deactivatedts = ?, deactivatedby_umapid = ?, sortorder=? 
                    WHERE lorid = ?
                """;
        Connection con = getPostgresCon();
        PreparedStatement pstmt = null;
        try {
            pstmt = con.prepareStatement(query);
            pstmt.setObject(1, loRole.getSchema(), java.sql.Types.OTHER);
            pstmt.setString(2, loRole.getTitle());
            pstmt.setString(3, loRole.getDescription());
            pstmt.setString(4, loRole.getNotes());

            if (loRole.getLastUpdatedby_UMAP() != null) {
                pstmt.setInt(5, loRole.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                pstmt.setNull(5, java.sql.Types.NULL);
            }
            pstmt.setDate(6, loRole.getDeactivatedTS() == null ? null : Date.valueOf(loRole.getDeactivatedTS().toString()));
            if (loRole.getDeactivatedBy_UMAP()!= null) {
                pstmt.setInt(7, loRole.getDeactivatedBy_UMAP().getUserMuniAuthPeriodID());
            } else {
                pstmt.setNull(7, java.sql.Types.NULL);
            }
            pstmt.setInt(8, loRole.getSortOrder());
            pstmt.setInt(9, loRole.getRoleID());
            pstmt.executeUpdate();
        }catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot update LinkedObjectRole Entity", ex);
        } finally{
            releasePostgresConnection(con, pstmt);
        } 
        
    }
    
    /**
     * Insert the data into the LinkedObjectRole table.
     * @param loRole
     * @return int
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public int insertLinkedObjectRole(LinkedObjectRole loRole) throws IntegrationException, BObStatusException{
        if (loRole == null) {
            throw new BObStatusException("cannot insert null object into linkedObjectRole table");
        }
        String query = """
                INSERT INTO public.linkedObjectRole(
                    lorid, lorschema, title, description, notes,
                    createdts, createdby_umapid, lastupdatedts,
                    lastupdatedby_umapid, deactivatedts, deactivatedby_umapid, sortorder)
                VALUES (DEFAULT, ?, ?, ?, ?,
                    now(), ?, now(), ?, NULL, NULL, ?);
                """;
        Connection con = null;
        PreparedStatement pstmt = null;
        int generateID = 0;
        ResultSet rs = null;
        try {
            con = getPostgresCon();
            pstmt = con.prepareStatement(query);
            pstmt.setObject(1, loRole.getSchema(), java.sql.Types.OTHER);
            pstmt.setString(2, loRole.getTitle());
            pstmt.setString(3, loRole.getDescription());
            pstmt.setString(4, loRole.getNotes());
            if (loRole.getCreatedby_UMAP() != null) {
                pstmt.setInt(5, loRole.getCreatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                pstmt.setNull(5, java.sql.Types.NULL);
            }

            if (loRole.getLastUpdatedby_UMAP() != null) {
                pstmt.setInt(6, loRole.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                pstmt.setNull(6, java.sql.Types.NULL);
            }
            
            pstmt.setInt(7, loRole.getSortOrder());
            pstmt.execute();
            
            String retrievalQuery = "SELECT currval('linkedObjectRole_seq');";
            pstmt = con.prepareStatement(retrievalQuery);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                generateID = rs.getInt(1);
            }
        }catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert LinkedObjectRole Entity", ex);
        } finally{
            releasePostgresConnection(con, pstmt, rs);
        } 
        return generateID;
    }
    
    /**
     * Get the list of LinkedObjectRole based on the active flag.
     * @param isActive
     * @return List(LinkedObjectRole)
     * @throws IntegrationException 
     */
    public List<LinkedObjectRole> getLinkedObjectRoleList(boolean isActive) throws IntegrationException{
        List<LinkedObjectRole> lorList = new ArrayList<>();
        StringBuilder query = new StringBuilder();
        query.append("SELECT lorid FROM public.linkedObjectRole ");
        if (isActive) {
            query.append("WHERE deactivatedts IS NULL");
        }
        query.append(";");

        ResultSet rs = null;
        PreparedStatement stmt = null;
        Connection con = getPostgresCon();

        try {
            stmt = con.prepareStatement(query.toString());
            System.out.println("");
            rs = stmt.executeQuery();
            while (rs.next()) {
                lorList.add(getLinkedObjectRole(rs.getInt("lorid")));
            }

        } catch (SQLException | BObStatusException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("cannot generate linkedObjectRole list", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }
        return lorList;
    }
    
    
    
    /**
     * Get the list of linked object schema enum.
     * @return
     * @throws BObStatusException 
     */
    public List<LinkedObjectSchemaEnum> populateLinkedObjectSchemaEnumList() throws BObStatusException{
        return Arrays.asList(LinkedObjectSchemaEnum.values())
                .stream()
                .sorted(Comparator.comparing(Enum::toString))
                .collect(Collectors.toList());
    }
    
    
    
    /**
     * Extracts all database IDs of roles by schema enum
     * @param lose
     * @return 
     */
    public List<Integer> getLinkedObjectRoleListBySchemaFamily(LinkedObjectSchemaEnum lose) throws IntegrationException, BObStatusException{
        if(lose == null){
            throw new BObStatusException("Cannot fetch roles given null enum");
            
        }
        List<Integer> idl = new ArrayList<>();

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT lorid FROM public.linkedobjectrole WHERE lorschema = CAST ( ? AS linkedobjectroleschema);");

        try {
            stmt = con.prepareStatement(sb.toString());
            stmt.setString(1, lose.getRoleSChemaTypeString());

            rs = stmt.executeQuery();

            while (rs.next()) {
                idl.add(rs.getInt("lorid"));

            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("unable to get linked object role ID list", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return idl;
    }
    
    /**
     * Populates common fields among LinkedObjectRole family
     * 
     * @param rs containing each common field in linked objects
     * @return the superclass ready to be injected into a subtype
     * @throws java.sql.SQLException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public LinkedObjectRole generateLinkedObjectRole(ResultSet rs) throws SQLException, IntegrationException, BObStatusException{
        LinkedObjectRole lor = new LinkedObjectRole();
         if(rs != null){
            
            lor.setRoleID(rs.getInt("lorid"));
            
            lor.setTitle((rs.getString("title")));
            
            try{
                // this enum lookup makes all the code reuse possible since the
                // enum contains table identifiers that allows for dynamic generation
                // of the SQL necessary to retrieve, update, and deactivate human links to
                // any other object in the system the is a humanListHolder
                lor.setSchema(LinkedObjectSchemaEnum.valueOf(rs.getString("lorschema")));
            } catch (IllegalArgumentException ex){
                System.out.println("SystemIntgrator.generateLinkedObjectRole | Could not match Linked Object Role with Schema!");
                System.out.println(ex);
            }
            
            if(rs.getTimestamp("createdts") != null){
                lor.setCreatedTS(rs.getTimestamp("createdts").toLocalDateTime());                
            }
            lor.setDescription(rs.getString("description"));
            
          
            if(rs.getTimestamp("deactivatedts") != null){
                lor.setDeactivatedTS(rs.getTimestamp("deactivatedts").toLocalDateTime());
            }
            lor.setNotes(rs.getString("notes"));
            lor.setSortOrder(rs.getInt("sortorder"));
             populateUMAPTrackedFields(lor, rs);
        }
        return lor;
    }

    /* ************************************************************************
    ******************          PRINT STYLES                    ***************
    ***************************************************************************/ 
    
    /**
     * Cached getter for print styles
     * @param styleID
     * @return
     * @throws IntegrationException
     * @throws  DatabaseFetchRuntimeException
     */
    public PrintStyle getPrintStyle(int styleID) throws IntegrationException, DatabaseFetchRuntimeException{
          if(styleID == 0){
              throw new IntegrationException("Cannot get print style of ID == 0");
          }
          if(getSystemMuniCacheManager().isCachingEnabled()){
            return getSystemMuniCacheManager().getCachePrintStyle().get(styleID, k -> fetchPrintStyle(styleID));
          } else {
              return fetchPrintStyle(styleID);
          }
          
    }
    
    /**
     * Getter for print style objects
     * @param styleID
     * @return with field values complete
     */
    public PrintStyle fetchPrintStyle(int styleID) throws DatabaseFetchRuntimeException {
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PrintStyle style = null;
        PreparedStatement stmt = null;
        StringBuilder sb = new StringBuilder();
        sb.append("""
                  SELECT styleid, description, headerwidth, novtopmargin, 
                         novaddresseleftmargin, novaddressetopmargin, browserheadfootenabled, novtexttopmargin
                    FROM public.printstyle WHERE styleid=?;
                  """);
        Icon i = null;

        try {
            stmt = con.prepareStatement(sb.toString());
            stmt.setInt(1, styleID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                style = generatePrintStyle(rs);
            }
        } catch (SQLException  ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("unable to generate printstyle", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return style;

    }
    
    
    
    /**
     * 
     * @return
     * @throws IntegrationException 
     */
    public List<PrintStyle> getPrintStyleList() throws IntegrationException {

        String query = """
                       SELECT styleid, description, headerwidth, novtopmargin, 
                              novaddresseleftmargin, novaddressetopmargin, browserheadfootenabled, 
                              novtexttopmargin
                         FROM public.printstyle;
                       """;

        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ArrayList<PrintStyle> styleList = new ArrayList<>();
        PrintStyle style = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();
            while (rs.next()) {
                style = generatePrintStyle(rs);
                if(style != null){
                    styleList.add(style);
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("unable to generate printstyle", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return styleList;

    }
    
    /**
     * Builds a map of style descriptions to IDs
     * @return
     * @throws IntegrationException 
     */
    public Map<String, Integer> getPrintStyleMap() throws IntegrationException {
        Connection con = getPostgresCon();
        ResultSet rs = null;
        Map<String, Integer> styleMap = new HashMap<>();
        PreparedStatement stmt = null;
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT styleid, description FROM printstyle;");

        try {
            stmt = con.prepareStatement(sb.toString());
            rs = stmt.executeQuery();
            while (rs.next()) {
                styleMap.put(rs.getString("description"), rs.getInt("styleid"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("unable to generate printstyle", ex);
        } finally {
            if (con != null) { try { con.close(); } catch (SQLException e) { /* ignored */} }
             if (stmt != null) { try { stmt.close(); } catch (SQLException e) { /* ignored */} }
             if (rs != null) { try { rs.close(); } catch (SQLException ex) { /* ignored */ } }
        } 
        return styleMap;
    }

 
    
    /**
     * Generator for print styles
     * @param rs with all fields selected
     * @return with field values injected
     * @throws SQLException 
     */
    private PrintStyle generatePrintStyle(ResultSet rs) throws SQLException {
        PrintStyle style = new PrintStyle();

        style.setStyleID(rs.getInt("styleid"));

        style.setDescription(rs.getString("description"));
        style.setHeaderWidthDefault(rs.getInt("headerwidth"));

        style.setNov_page_margin_top(rs.getInt("novtopmargin"));
        style.setNov_addressee_margin_left(rs.getInt("novaddresseleftmargin"));
        style.setNov_addressee_margin_top(rs.getInt("novaddressetopmargin"));
        style.setNov_text_margin_top(rs.getInt("novtexttopmargin"));

        return style;
    }
    
     /**
     * Inserts a PrintStyle to the printstyle table
     * @param ps
     * @return with field values complete
     * @throws IntegrationException 
     */
    public int insertPrintStyle(PrintStyle ps) throws IntegrationException {
        if(ps == null){
            throw new IntegrationException("Cannot insert a null print style");
        }
        Connection con = null;
        int freshID = 0;
        ResultSet rs = null;
        PreparedStatement stmt = null;
        StringBuilder sb = new StringBuilder();
        sb.append("""
                  INSERT INTO public.printstyle(
                  	styleid, description, 
                        headerwidth, novtopmargin, novaddresseleftmargin, 
                        novaddressetopmargin, browserheadfootenabled, novtexttopmargin)
                  	VALUES (DEFAULT, ?, 
                                ?, ?, ?, 
                                ?, ?, ?);
                  """);

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(sb.toString());
            stmt.setString(1, ps.getDescription());
            
            stmt.setInt(2, ps.getHeaderWidthDefault());
            stmt.setInt(3, ps.getNov_page_margin_top());
            stmt.setInt(4, ps.getNov_addressee_margin_left());
            
            stmt.setInt(5, ps.getNov_addressee_margin_top());
            stmt.setBoolean(6, ps.isBrowserHeaderFooterEnabled());
            stmt.setInt(7, ps.getNov_text_margin_top());
            
            stmt.execute();
            
            String retrievalQuery = "SELECT currval('printstyle_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                freshID = rs.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("unable to insert print style", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return freshID;

    }
    
    
     /**
     * Updates a print style record
     * @param ps
     * @throws IntegrationException 
     */
    public void updatePrintStyle(PrintStyle ps) throws IntegrationException {
        
        if(ps == null){
            throw new IntegrationException("Cannot update a null print style");
        }
        
        Connection con = getPostgresCon();
        
        PreparedStatement stmt = null;
        StringBuilder sb = new StringBuilder();
        
        sb.append("""
                  UPDATE public.printstyle
                    	SET description=?, 
                            headerwidth=?, novtopmargin=?, novaddresseleftmargin=?, 
                            novaddressetopmargin=?, browserheadfootenabled=?, novtexttopmargin=?
                    	WHERE styleid=?;
                  """);

        try {
            stmt = con.prepareStatement(sb.toString());
          stmt.setString(1, ps.getDescription());
            
            stmt.setInt(2, ps.getHeaderWidthDefault());
            stmt.setInt(3, ps.getNov_page_margin_top());
            stmt.setInt(4, ps.getNov_addressee_margin_left());
            
            stmt.setInt(5, ps.getNov_addressee_margin_top());
            stmt.setBoolean(6, ps.isBrowserHeaderFooterEnabled());
            stmt.setInt(7, ps.getNov_text_margin_top());
            stmt.setInt(8, ps.getStyleID());
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("unable to update printstyle", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        } 
        
    }
    
   /* ************************************************************************
    ******************          RANDOM UTILITIES                ***************
    ***************************************************************************/ 
    
    
    public void insertImprovementSuggestion(ImprovementSuggestion is) throws IntegrationException {

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        String s = """
                    INSERT INTO public.improvementsuggestion(
                               improvementid, improvementtypeid, improvementsuggestiontext, 
                               improvementreply, statusid, submitterid, submissiontimestamp)
                       VALUES (DEFAULT, ?, ?, 
                               NULL, ?, ?, now());
                   """;

        try {
            stmt = con.prepareStatement(s);
            stmt.setInt(1, is.getImprovementTypeID());
            stmt.setString(2, is.getSuggestionText());
            // hard-coded status for expediency
            stmt.setInt(3, is.getStatusID());
            stmt.setInt(4, is.getSubmitter().getUserID());
            System.out.println("PersonIntegrator.getPersonListByPropertyID | sql: " + stmt.toString());
            stmt.execute();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to insert improvement suggestion, sorry", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        } 
    }

    final String impSugQuery = """
                               SELECT improvementid, improvementtypeid, improvementsuggestiontext, 
                                      improvementreply, statusid, statustitle, typetitle, submitterid, submissiontimestamp
                                 FROM public.improvementsuggestion INNER JOIN improvementstatus USING (statusid)
                                 INNER JOIN improvementtype ON improvementtypeid = typeid;
                               """;

    public ArrayList<ImprovementSuggestion> getImprovementSuggestions() throws IntegrationException, BObStatusException {
        ArrayList<ImprovementSuggestion> impList = new ArrayList<>();

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(impSugQuery);
            rs = stmt.executeQuery();
            while (rs.next()) {
                impList.add(generateImprovementSuggestion(rs));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to build improvement suggestion due to an DB integration error", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return impList;
    }

    public ResultSet getImprovementSuggestionsRS() throws IntegrationException {
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(impSugQuery);
            rs = stmt.executeQuery();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to get improvement suggestion RS due to an DB integration error", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return rs;
    }

    private ImprovementSuggestion generateImprovementSuggestion(ResultSet rs) throws SQLException, IntegrationException, BObStatusException {
        UserCoordinator uc = getUserCoordinator();
        ImprovementSuggestion is = new ImprovementSuggestion();
        is.setSuggestionID(rs.getInt("improvementid"));
        is.setSubmitter(uc.user_getUser(rs.getInt("submitterid")));
        is.setImprovementTypeID(rs.getInt("improvementtypeid"));
        is.setImprovementTypeStr(rs.getString("typetitle"));
        is.setReply(rs.getString("improvementreply"));
        is.setStatusID(rs.getInt("statusid"));
        is.setStatusStr(rs.getString("statustitle"));
        is.setSuggestionText(rs.getString("improvementsuggestiontext"));
        is.setSubmissionTimeStamp(rs.getTimestamp("submissiontimestamp").toLocalDateTime());
        return is;
    }

    public void insertListChangeRequest(ListChangeRequest lcr) throws IntegrationException {

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        String s = """
                    INSERT INTO public.listchangerequest(
                    changeid, changetext)
                    VALUES (DEFAULT, ?);
                   """;

        try {
            stmt = con.prepareStatement(s);
            stmt.setString(1, lcr.getChangeRequestText());

            stmt.execute();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("PersonIntegrator.getPerson | Unable to retrieve person", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        } 
    }

    public HashMap<String, Integer> getSuggestionTypeMap() throws IntegrationException {
        HashMap<String, Integer> hm = new HashMap<>();

        String query = """
                       SELECT typeid, typetitle, typedescription
                         FROM public.improvementtype;
                       """;

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {

            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();
            while (rs.next()) {
                hm.put(rs.getString("typetitle"), rs.getInt("typeid"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to build suggestion type map due to an DB integration error", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return hm;
    }

    /**
     * 
     * @param tableName Name of the target table to search
     * @param target    Name of the target column
     * @param targetID  Identifier of the target
     * @return int number of uses of targetID found in tableName on column target
     * @throws IntegrationException 
     */
    public int checkForUse(String tableName, String target, int targetID) throws IntegrationException{
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(target);
        sb.append(" FROM ");
        sb.append(tableName);
        sb.append(" WHERE ");
        sb.append(target);
        sb.append("=?;");
        int uses = 0;

        try {
            stmt = con.prepareStatement(sb.toString());
            stmt.setInt(1, targetID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                uses++;     
                rs.getInt(target);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("unable to locate " + target + ": " + targetID, ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return uses;
    }
    
    /**
     * Searches the DB for Foreign keys related to String search
     * @param search String to search for foreign keys
     * @return List<String> of tables with foreign keys to search
     * @throws IntegrationException 
     */
    public List<String> findForeignUseTables(String search) throws IntegrationException {
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        StringBuilder sb = new StringBuilder();
        List<String> results = new ArrayList();
        sb.append("SELECT TABLE_NAME ");
        //sb.append("CONSTRAINT_NAME, ");
        //sb.append("CONSTRAINT_TYPE ");
        sb.append("FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS ");
        sb.append("WHERE CONSTRAINT_NAME LIKE '%").append(search).append("_fk'; ");
        
        try {
            stmt = con.prepareStatement(sb.toString());
            rs = stmt.executeQuery();
            while (rs.next()) {
                results.add(rs.getString("TABLE_NAME"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to generate table_name", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return results;
    }
    
    /**
     * Checks a Manageable for use in the DB
     * @param m
     * @return int times used in the DB
     * @throws IntegrationException 
     */
    public int checkManagableForUse(Manageable m) throws IntegrationException { 
        ManagedSchemaEnum e = m.getMANAGEABLE_SCHEMA();
        
        int uses = 0;
        List<String> useTables = findForeignUseTables(e.getTARGET_TABLE_ID_FIELD());
        for(int x = 0; x < useTables.size(); x++){
            uses =+ checkForUse("public." + useTables.get(x), e.getFK_ID_FIELD(), m.getID());
            //System.out.println("Checked public." + useTables.get(x) + " for icon_iconid:" + i.getIconID());
        }
        return uses;
    }
    
    
   /* ************************************************************************
    ******************          ICONS                           ***************
    ***************************************************************************/ 
    
    
    /**
     * Cached getter for icons
     * @param iconID
     * @return the icon
     */
    public Icon getIcon(int iconID){
        // just ship back an empty ICON
    
         if(iconID == 0){
//            throw new IntegrationException("SystemIntegrator.getIcon: Cannot fetch Icon with ID eq 0");
//            System.out.println("SystemIntegrator.getIcon | Icon ID eq 0 requested, returning generic icon");
            return new Icon();
            
        }
        if(getSystemMuniCacheManager().isCachingEnabled()){
            return getSystemMuniCacheManager().getCacheIcon().get(iconID, k -> fetchIcon(iconID));
        } else {
            return fetchIcon(iconID);
        }
    }
    
    
    /**
     *
     * @param iconID
     * @return Icon of the iconID
     * @throws DatabaseFetchRuntimeException
     */
    public Icon fetchIcon(int iconID) throws DatabaseFetchRuntimeException {
      
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        String query = """
                       SELECT iconid, name, styleclass, materialicons, deactivatedts FROM public.icon WHERE iconid=?;
                       """;
        Icon i = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, iconID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                i = new Icon();
                i.setID(rs.getInt("iconid"));
                i.setName(rs.getString("name"));
                i.setStyleClass(rs.getString("styleclass"));
                i.setMaterialIcon(rs.getString("materialicons"));
                if (rs.getTimestamp("deactivatedts") == null) {
                    i.setDeactivatedts(null);
                } else {
                    i.setDeactivatedts(rs.getTimestamp("deactivatedts").toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("SystemIntegrator.getIcon: unable to generate icon", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return i;

    }
    
    /**
     * Sets the deactivatedTS on the given Icon's record
     * @param i
     * @throws IntegrationException 
     */
    public void deactivateIcon(Icon i) throws IntegrationException {
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        String query = """
                      UPDATE public.icon SET deactivatedts=now() WHERE iconid=?;
                      """;
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, i.getID());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("unable to deactivate icon", ex);
        } finally {
            releasePostgresConnection(con, stmt);

        }      
    }

    /**
     * Updates the fields on the given Icon
     * @param i
     * @throws IntegrationException 
     */
    public void updateIcon(Icon i) throws IntegrationException {
        String query = """
                       UPDATE public.icon SET name=?, materialicons=?, styleClass=?
                       WHERE iconid = ?;
                       """;
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setString(1, i.getName());
            stmt.setString(2, i.getMaterialIcon());
            stmt.setString(3, i.getStyleClass());
            stmt.setInt(4, i.getID());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("unable to update icon", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        } 
    }

    public int insertIcon(Icon i) throws IntegrationException {
        String query = """
                       INSERT INTO public.icon(
                       	name, materialicons, deactivatedts, styleClass)
                       	VALUES (?, ?, NULL, ?);
                       """;
        Connection con = null;
        PreparedStatement stmt = null;
        int freshID = 0;
        ResultSet rs = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setString(1, i.getName());
            stmt.setString(2, i.getMaterialIcon());
            stmt.setString(3, i.getStyleClass());
            stmt.execute();

            String retrievalQuery = "SELECT currval('iconid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();

            while (rs.next()) {
                freshID = rs.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("unable to insert icon", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);

        } 
        return freshID;
    }

    /**
     * 
     * @return the Icon List active only
     * @throws IntegrationException 
     */
    public List<Icon> getIconList() throws IntegrationException{
        return getIconList(false);
    }
    
    /**
     * 
     * @param includeDeactivated
     * @return the Icon List deactivated Included base on includeDeactivated boolean
     * @throws IntegrationException 
     */
    public List<Icon> getIconList(boolean includeDeactivated) throws IntegrationException {
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        StringBuilder sb = new StringBuilder();
        List<Icon> iList = new ArrayList<>();

        try {
            if(includeDeactivated){
                sb.append("SELECT iconid FROM public.icon;");
            }else{
                sb.append("SELECT iconid FROM public.icon WHERE deactivatedts IS NULL;");
            }
            stmt = con.prepareStatement(sb.toString());
            rs = stmt.executeQuery();
            while (rs.next()) {
                iList.add(getIcon(rs.getInt("iconid")));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to get icon list", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return iList;
    }
    
   
   /* ************************************************************************
    ******************          PROPERTY USE TYPES                ***************
    ***************************************************************************/ 
    
    public int putCheckForUse(PropertyUseType p) throws IntegrationException {
        int uses = 0;
        List<String> useTables = findForeignUseTables("propertyusetypeid");
        for(int x = 0; x < useTables.size(); x++){
            uses =+ checkForUse("public." + useTables.get(x), "usetype_typeid", p.getTypeID());
            System.out.println("Checked public." + useTables.get(x) + " for  usetype_typeid" + p.getTypeID());
        }
        return uses;
    }
    
    public PropertyUseType getPut(int putID) throws IntegrationException {
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        String query = """
                       SELECT propertyusetypeid, name, description, icon_iconid, zoneclass,
                       deactivatedts, createdts, createdby_umapid, deactivatedby_umapid, lastupdatedts, lastupdatedby_umapid 
                       FROM public.propertyusetype WHERE propertyusetypeid=?;
                       """;
        PropertyUseType p = null;
        SystemIntegrator si = getSystemIntegrator();

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, putID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                p = new PropertyUseType();
                p.setTypeID(rs.getInt("propertyusetypeid"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));
                p.setIcon(getIcon(rs.getInt("icon_iconid")));
                p.setZoneClass(rs.getString("zoneclass"));
                si.populateUMAPTrackedFields(p, rs, true);
            }
        } catch (SQLException | BObStatusException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("unable to generate put", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return p;
    }
    
    public List<PropertyUseType> getPutList() throws IntegrationException {
        return getPutList(false);
    }
    
    public List<PropertyUseType> getPutList(boolean includeDeactivated) throws IntegrationException {
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        StringBuilder sb = new StringBuilder();
        if(includeDeactivated){
            sb.append("SELECT propertyusetypeid FROM public.propertyusetype;");
        }else{
            sb.append("SELECT propertyusetypeid FROM public.propertyusetype WHERE deactivatedts IS NULL;");
        }
        List<PropertyUseType> putList = new ArrayList<>();
        try {
            stmt = con.prepareStatement(sb.toString());
            rs = stmt.executeQuery();
            while (rs.next()) {
                putList.add(getPut(rs.getInt("propertyusetypeid")));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to generate propertyUseType(List)", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return putList;
    }
    
       
   /* ************************************************************************
    ******************          BOB SOURCES                     ***************
    ***************************************************************************/ 

     /**
      * Cache-enabled getter for BObSource Objects
      * @param sourceID
      * @return
      * @throws BObStatusException
      * @throws DatabaseFetchRuntimeException 
      */
    public BOBSource getBOBSource(int sourceID) throws BObStatusException, DatabaseFetchRuntimeException {
        if (sourceID == 0) {
            throw new BObStatusException("SystemIntegrator.getBObSource | cannot get bob source with ID = 0;");
        }
        if(getSystemMuniCacheManager().isCachingEnabled()){
            return getSystemMuniCacheManager().getCacheBObSource().get(sourceID, k -> fetchBOBSource(sourceID));
        } else {
            return fetchBOBSource(sourceID);
        }
    }

    
     
    /**
     * Returns all Source IDs in the bobsource table
     * @return
     * @throws IntegrationException 
     */
     public List<Integer> getBobSourceListComplete() throws IntegrationException{
        List<Integer> sidl = new ArrayList<>();
        BOBSource bs = null;
          
        String query =  " SELECT sourceid FROM public.bobsource WHERE active = TRUE;";
        
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {

            stmt = con.prepareStatement(query);
            
            rs = stmt.executeQuery();
            while (rs.next()) {
                sidl.add(rs.getInt("sourceid"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to build bob source list complete due to an DB integration error", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return sidl;
     }
    
     
     /**
      * Getter for BOB source objects by id;
      * TEST of injected Datasource
      * @param sourceID
      * @return
      */
      public BOBSource fetchBOBSource(int sourceID) throws DatabaseFetchRuntimeException{
        System.out.println("SystemIntegrator.fetchBOBSource | sourceID: " + sourceID);
         
        BOBSource bs = null;

        System.out.println("SystemIntegrator.getBOBSource | NOT in cache");
        String query =    """
                             SELECT sourceid, title, description, creator, muni_municode, muniname, userattributable, 
                                     bobsource.active, bobsource.notes
                                     FROM public.bobsource 
                                  LEFT OUTER JOIN municipality ON (bobsource.muni_municode = municipality.municode)                    
                                  WHERE sourceid = ?;
                          """;

        // new
//        Connection con = dbConFactory.getConnection();
        // OLD
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {

          stmt = con.prepareStatement(query);
          stmt.setInt(1, sourceID);
          rs = stmt.executeQuery();
          while (rs.next()) {
              bs = generateBOBSource(rs);
          }
        
        } catch (SQLException | IntegrationException ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("Unable to get bob source due to an DB integration error", ex);
        } finally {
          releasePostgresConnection(con, stmt, rs);
        }
        return bs;
    }

      /**
       * Factory for and populator of fields on BOBSource objects
       * @param rs
       * @return
       * @throws SQLException
       * @throws IntegrationException 
       */
    private BOBSource generateBOBSource(ResultSet rs) throws SQLException, IntegrationException {
        BOBSource bs = new BOBSource();
        bs.setSourceid(rs.getInt("sourceid"));
        bs.setTitle(rs.getString("title"));
        bs.setUserattributable(rs.getBoolean("userattributable"));
        bs.setDescription(rs.getString("description"));
        // beware of inifinte loops
        bs.setCreatorUserID(rs.getInt("creator"));
        bs.setMuniCodeFlattened(rs.getInt("muni_municode"));
        bs.setMuniNameFlattened(rs.getString("muniname"));
        
        bs.setActive(rs.getBoolean("active"));;
        bs.setNotes(rs.getString("notes"));
        return bs;

    }
       
   /* ************************************************************************
    ******************          INTENSITY CLASSES              ***************
    ***************************************************************************/ 

    /**
     * Cached getter for intensity classes
     * @param intensityClassID
     * @return
     * @throws IntegrationException 
     */
    public IntensityClass getIntensityClass(int intensityClassID) throws IntegrationException{
        if(intensityClassID == 0){
            throw new IntegrationException("Cannot fetch intensity class by ID");
        }
    
        if(getSystemMuniCacheManager().isCachingEnabled()){
            return getSystemMuniCacheManager().getCacheIntensityClass().get(intensityClassID, k -> fetchIntensityClass(intensityClassID));
        } else {
            return fetchIntensityClass(intensityClassID);
        }
        
    }
    
    
    
    /**
     * Fetches intensity class by ID
     * @param intensityClassID
     * @return
     */
    public IntensityClass fetchIntensityClass(int intensityClassID) throws DatabaseFetchRuntimeException{
        
        IntensityClass in = null;

        String query =  """
                        SELECT classid, title, muni_municode, numericrating, schemaname, active, 
                               icon_iconid, lastupdatedts
                          FROM public.intensityclass WHERE classid=?;
                        """;

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, intensityClassID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                in = generateIntensityClass(rs);
            }
        } catch (SQLException | IntegrationException ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("SystemIntegrator.fetchIntensityClass | Unable to get Intensity List", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 

        return in;
    }
    
 
    /**
     * populates members on intensity class
     * @param rs
     * @return
     * @throws IntegrationException 
     */
    private IntensityClass generateIntensityClass(ResultSet rs) throws IntegrationException {

        IntensityClass intsty = new IntensityClass();
        MunicipalityCoordinator mc = getMuniCoordinator();

        try {
            intsty.setClassID(rs.getInt("classid"));
            intsty.setTitle(rs.getString("title"));
            if(rs.getInt("muni_municode") != 0){
                intsty.setMuni(mc.getMuni(rs.getInt("muni_municode")));
            }
            intsty.setNumericRating(rs.getInt("numericrating"));
            intsty.setSchema(new IntensitySchema(rs.getString("schemaName")));
            intsty.setActive(rs.getBoolean("active"));
            if(rs.getInt("icon_iconid") != 0){
                intsty.setIcon(getIcon(rs.getInt("icon_iconid")));
            }
            if(rs.getTimestamp("lastupdatedts") != null){
                intsty.setLastUpdatedTS(rs.getTimestamp("lastupdatedts").toLocalDateTime());
            }
        } catch (SQLException | BObStatusException ex) {
            System.out.println(ex);
            throw new IntegrationException("Error generating Intensity from ResultSet", ex);
        }

        return intsty;
    }
    /**
     * A search-like method for intensity classes with any schema name like X
     * @param schemaLabel if null, all intensities will be returned
     * @param muni returns all munis if null
     * @return
     * @throws IntegrationException 
     */
    public List<IntensityClass> getIntensityClassList(String schemaLabel, Municipality muni) throws IntegrationException {

        List<IntensityClass> inList = new ArrayList<>();

        StringBuilder sb = new StringBuilder("SELECT classid, title, muni_municode, numericrating, schemaname, active, icon_iconid, lastupdatedts FROM intensityclass WHERE classid IS NOT NULL ");
        if(schemaLabel != null){
            sb.append("AND schemaname ILIKE ? ");
        }
        if(muni != null){
            sb.append("AND muni_municode=? ");
        }
        sb.append(";");
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sb.toString());
            if(schemaLabel != null){
                stmt.setString(1, schemaLabel);
            }
            if(muni != null){
                stmt.setInt(2, muni.getMuniCode());
            }
            rs = stmt.executeQuery();
            while (rs.next()) {
                inList.add(generateIntensityClass(rs));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("SystemIntegrator.getIntensityClassList | Unable to get Intensity List", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return inList;
    }

    /**
     * Updates a record in the intensityclass table, including setting active to false
     * @param intsty
     * @throws IntegrationException 
     */
    public void updateIntensityClass(IntensityClass intsty) throws IntegrationException {

        String query = """
                       UPDATE public.intensityclass
                       SET title=?, muni_municode=?, numericrating=?,
                       schemaname=?, active=?, icon_iconid=?, lastupdatedts=now()
                       WHERE classid=?;
                       """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setString(1, intsty.getTitle());
             if(intsty.getMuni() != null){
                stmt.setInt(2, intsty.getMuni().getMuniCode());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            stmt.setInt(3, intsty.getNumericRating());
            stmt.setString(4, intsty.getSchema().getLabel());
            stmt.setBoolean(5, intsty.isActive());
            if(intsty.getIcon() != null){
                stmt.setInt(6, intsty.getIcon().getID());                
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            stmt.setInt(7, intsty.getClassID());
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update Intensity", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        }
        

    }

    /**
     * Adds a record to the intensityclass list
     * @param intsty
     * @throws IntegrationException 
     */
    public int insertIntensityClass(IntensityClass intsty) throws IntegrationException {
        String query = """
                       INSERT INTO public.intensityclass(classid, title, 
                       muni_municode, numericrating, schemaname, 
                       active, icon_iconid, lastupdatedts)
                       VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, now());
                       """;
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int freshID = 0;
        try {
            stmt = con.prepareStatement(query);
            stmt.setString(1, intsty.getTitle());
            if(intsty.getMuni() != null){
                stmt.setInt(2, intsty.getMuni().getMuniCode());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            stmt.setInt(3, intsty.getNumericRating());
            stmt.setString(4, intsty.getSchema().getLabel());
            stmt.setBoolean(5, intsty.isActive());
             if(intsty.getIcon() != null){
                stmt.setInt(6, intsty.getIcon().getID());                
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            stmt.execute();
            
            String retrievalQuery = "SELECT currval('codeviolationseverityclass_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();
            while (rs.next()) {
                freshID = rs.getInt(1);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert Intensity Class", ex);

        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return freshID;

    }

    /**
     * Assembles a list of all unique intensity schemas, which are a hacky
     * quick and dirty way of organizing intenisity classes. This was created
     * experimentally to see if using a single table with a free-form schema text
     * field is okay instead of a truly normalized intensity schema table that is keyed
     * in intensity class, as is done with a bunch of other status objects
     * @return
     * @throws IntegrationException 
     */
    public List<IntensitySchema> getIntensitySchemaList() throws IntegrationException {

        List<IntensitySchema> inList = new ArrayList<>();

        String query = "SELECT DISTINCT schemaname FROM intensityclass;";

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();
            while (rs.next()) {
                inList.add(generateIntensitySchema(rs));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("SystemIntegrator.getIntensitySchemaList | Unable to get Intensity Schema List", ex);
        } finally {
            if (con != null) { try { con.close(); } catch (SQLException e) { /* ignored */} }
             if (stmt != null) { try { stmt.close(); } catch (SQLException e) { /* ignored */} }
             if (rs != null) { try { rs.close(); } catch (SQLException ex) { /* ignored */ } }
        } 

        return inList;

    }

    /**
     * Builds a schema, which is just a object wrapper around a String
     * @param rs
     * @return
     * @throws IntegrationException 
     */
    public IntensitySchema generateIntensitySchema(ResultSet rs) throws IntegrationException {
        
        String schemaName = "";

        try {
            schemaName = rs.getString("schemaname");
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Error generating IntensitySchema from ResultSet", ex);
        }

        return new IntensitySchema(schemaName);

    }
    
    
        
       
   /* ************************************************************************
    ******************          LOGGING                         ***************
    ***************************************************************************/ 

    
    /**
     * Writes in a history record when a User accesses that object.
     * The Object's type will be checked against existing history
     * recording opportunities and create an appropriate entry in the
     * loginobjecthistory table.
     *
     * 
     *
     * @param u the User who viewed the object
     * @param ob any Object that's displayed in a data table or list in the system
     * @throws IntegrationException
     */
    public void logObjectView(User u, IFace_Loggable ob) throws IntegrationException {
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        StringBuilder insertSB = new StringBuilder();
        insertSB.append("INSERT INTO loginobjecthistory ");
        try {
            if (ob instanceof Person) {
                Person p = (Person) ob;
                
                insertSB.append("(login_userid, person_personid, entrytimestamp) VALUES (?, ?, DEFAULT); ");
                stmt = con.prepareStatement(insertSB.toString());
                
                stmt.setInt(1, u.getUserID());
                stmt.setInt(2, p.getHumanID());
                
                stmt.execute();
                
                System.out.println("SystemIntegrator.logObjectView: Person view logged id = " + p.getHumanID());
                
            } else if (ob instanceof Property) {
                Property p = (Property) ob;
                
                insertSB.append("(login_userid, property_propertyid, entrytimestamp) VALUES (?, ?, DEFAULT); ");
                stmt = con.prepareStatement(insertSB.toString());
                
                stmt.setInt(1, u.getUserID());
                stmt.setInt(2, p.getParcelKey());

                stmt.execute();

                System.out.println("SystemIntegrator.logObjectView: Property view logged id = " + p.getParcelKey());
            } else if (ob instanceof CECaseDataHeavy) {
                CECaseDataHeavy c = (CECaseDataHeavy) ob;
                
                insertSB.append("(login_userid, cecase_caseid, entrytimestamp) VALUES (?, ?, DEFAULT); ");
                stmt = con.prepareStatement(insertSB.toString());
                
                stmt.setInt(1, u.getUserID());
                stmt.setInt(2, c.getCaseID());

                stmt.execute();

                System.out.println("SystemIntegrator.logObjectView: Case view logged id = " + c.getCaseID());
            } else if (ob instanceof OccPeriod) {
                OccPeriod op = (OccPeriod) ob;

                insertSB.append("(login_userid, occperiod_periodid, entrytimestamp) VALUES (?, ?, DEFAULT); ");
                stmt = con.prepareStatement(insertSB.toString());

                stmt.setInt(1, u.getUserID());
                stmt.setInt(2, op.getPeriodID());

                stmt.execute();
                System.out.println("SystemIntegrator.logObjectView: Occ Period logged id = " + op.getPeriodID() );
            }
            
            
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Error writign object history: persons, properties, or cecases", ex);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    /* ignored */
                }
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    /* ignored */
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    /* ignored */
                }
            }
        } 
    }

    /**
     * Writes in a history record when a User accesses that object.
     * The Object's type will be checked against existing history
     * recording opportunities and create an appropriate entry in the
     * loginobjecthistory table.
     *
     * Checks for duplicates in the table before inserting.
     * If duplicate object ID exists, update existing entry with the current
     * time stamp only.
     *
     * @Deprecated 
     * @param u the User who viewed the object
     * @param ob any Object that's displayed in a data table or list in the system
     * @throws IntegrationException
     */
    public void logObjectView_OverwriteDate(User u, Object ob) throws IntegrationException {
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        StringBuilder selectSB = new StringBuilder();
        selectSB.append("SELECT historyentryid FROM loginobjecthistory " + "WHERE login_userid = ? ");
        StringBuilder insertSB = new StringBuilder();
        insertSB.append("INSERT INTO loginobjecthistory ");
        StringBuilder updateSB = new StringBuilder();
        updateSB.append("UPDATE loginobjecthistory SET entrytimestamp = now() " + "WHERE login_userid = ? ");
        try {
            if (ob instanceof Person) {
                Person p = (Person) ob;
                // prepare SELECT statement
                selectSB.append("AND person_personid = ? ");
                stmt = con.prepareStatement(selectSB.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
                stmt.setInt(1, u.getUserID());
                stmt.setInt(2, p.getHumanID());
                rs = stmt.executeQuery();
                if (rs.first()) {
                    // history entry with this user and person already exists
                    updateSB.append("AND person_personid = ? ");
                    stmt = con.prepareStatement(updateSB.toString());
                } else {
                    // pair not in history, do fresh insert
                    insertSB.append("(login_userid, person_personid, entrytimestamp) VALUES (?, ?, DEFAULT); ");
                    stmt = con.prepareStatement(insertSB.toString());
                }
                // each UPDATE and INSERT SQL structures take the params in this order
                stmt.setInt(1, u.getUserID());
                stmt.setInt(2, p.getHumanID());
                stmt.execute();
                System.out.println("SystemIntegrator.logObjectView: Person view logged id = " + p.getHumanID());
            } else if (ob instanceof Property) {
                Property p = (Property) ob;
                // prepare SELECT statement
                selectSB.append("AND property_propertyid = ? ");
                stmt = con.prepareStatement(selectSB.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
                stmt.setInt(1, u.getUserID());
                stmt.setInt(2, p.getParcelKey());
                rs = stmt.executeQuery();
                if (rs.first()) {
                    // history entry with this user and person already exists
                    updateSB.append("AND property_propertyid = ? ");
                    stmt = con.prepareStatement(updateSB.toString());
                } else {
                    // pair not in history, do fresh insert
                    insertSB.append("(login_userid, property_propertyid, entrytimestamp) VALUES (?, ?, DEFAULT); ");
                    stmt = con.prepareStatement(insertSB.toString());
                }
                // each UPDATE and INSERT SQL structures take the params in this order
                stmt.setInt(1, u.getUserID());
                stmt.setInt(2, p.getParcelKey());
                stmt.execute();
                System.out.println("SystemIntegrator.logObjectView: Property view logged id = " + p.getParcelKey());
            } else if (ob instanceof CECaseDataHeavy) {
                CECaseDataHeavy c = (CECaseDataHeavy) ob;
                // prepare SELECT statement
                selectSB.append("AND cecase_caseid = ? ");
                stmt = con.prepareStatement(selectSB.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
                stmt.setInt(1, u.getUserID());
                stmt.setInt(2, c.getCaseID());
                rs = stmt.executeQuery();
                if (rs.first()) {
                    // history entry with this user and person already exists
                    updateSB.append("AND cecase_caseid = ? ");
                    stmt = con.prepareStatement(updateSB.toString());
                } else {
                    // pair not in history, do fresh insert
                    insertSB.append("(login_userid, cecase_caseid, entrytimestamp) VALUES (?, ?, DEFAULT); ");
                    stmt = con.prepareStatement(insertSB.toString());
                }
                // each UPDATE and INSERT SQL structures take the params in this order
                stmt.setInt(1, u.getUserID());
                stmt.setInt(2, c.getCaseID());
                stmt.execute();
                System.out.println("SystemIntegrator.logObjectView: Case view logged id = " + c.getCaseID());
            } else if (ob instanceof OccPeriod) {
                OccPeriod op = (OccPeriod) ob;
                // prepare SELECT statement
                selectSB.append("AND occperiod_periodid = ? ");
                stmt = con.prepareStatement(selectSB.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
                stmt.setInt(1, u.getUserID());
                stmt.setInt(2, op.getPeriodID());
                rs = stmt.executeQuery();
                if (rs.first()) {
                    // history entry with this user and person already exists
                    updateSB.append("AND cecase_caseid = ? ");
                    stmt = con.prepareStatement(updateSB.toString());
                } else {
                    // pair not in history, do fresh insert
                    insertSB.append("(login_userid, cecase_caseid, entrytimestamp) VALUES (?, ?, DEFAULT); ");
                    stmt = con.prepareStatement(insertSB.toString());
                }
                // each UPDATE and INSERT SQL structures take the params in this order
                stmt.setInt(1, u.getUserID());
                stmt.setInt(2, op.getPeriodID());
                stmt.execute();
                System.out.println("SystemIntegrator.logObjectView: Occ Period logged id = " + op.getPeriodID() );
            }
            
            
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Error writign object history: persons, properties, or cecases", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
    }
    
   
    
    
    // *************************************************************************
    // ****************** XMUNI FACILITY INFRASTRUCTURE ************************
    // *************************************************************************
    
    
    /**
     * Extracts a single xmr from the DB by ID
     * 
     * @param xmrid
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public XMuniActivationRecord getXMuniActivationRecord(int xmrid) throws BObStatusException, IntegrationException{
        XMuniActivationRecord xmr = null;
        if(xmrid == 0){
            throw new IntegrationException("Cannot fetch xmrID == 0");
        }

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

       String q = """
                  SELECT    activationid, xmuni_municode, humanconduit_humanid, 
                            linkrole_roleid, parentkey, activationts, 
                            activationby_umapid, exitts
                    FROM public.xmuniactivation WHERE activationid=?;
                  """;

        try {
            stmt = con.prepareStatement(q);
            stmt.setInt(1, xmrid);

            rs = stmt.executeQuery();

            while (rs.next()) {
                xmr = generateXMuniActivationRecord(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("unable to get linked object role", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }    
        return xmr;
    }
    
    
    /**
     * Internal factory for xmuni activation records
     * 
     * @param rs
     * @return
     * @throws IntegrationException 
     */
    private XMuniActivationRecord generateXMuniActivationRecord(ResultSet rs) throws IntegrationException, SQLException, BObStatusException{
        if(rs == null){
            throw new IntegrationException("Cannot generate xmuni Record with null TS");
        }
        
        MunicipalityCoordinator mc = getMuniCoordinator();
        PersonCoordinator pc = getPersonCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        UserCoordinator uc = getUserCoordinator();
        
        XMuniActivationRecord xmr = new XMuniActivationRecord();
        
        xmr.setActivationID(rs.getInt("activationid")); 
        xmr.setxMuni(mc.getMuni(rs.getInt("xmuni_municode"))); 
        xmr.setHumanConduit(pc.getHuman(rs.getInt("humanconduit_humanid")));
        
        xmr.setLinkConduitRole(sc.getLinkedObjectRole(rs.getInt("linkrole_roleid")));
        xmr.setParentKey(rs.getInt("parentkey"));
        
        if(rs.getTimestamp("activationts") != null){
            xmr.setActivationTS(rs.getTimestamp("activationts").toLocalDateTime());
        }
        
        if(rs.getTimestamp("exitts") != null){
            xmr.setExitTS(rs.getTimestamp("exitts").toLocalDateTime());
        }
        xmr.setActivatorUMAP(uc.auth_getUMAPNotValidated(rs.getInt("activationby_umapid")));
        
        return xmr;
        
    }
    
    /**
     * Writes a new record to xmuniactivation table
     * @param xmr
     * @return 
     */
    public int insertXMuniActivationRecord(XMuniActivationRecord xmr) throws BObStatusException, IntegrationException{
        int freshID = 0;
         if (xmr == null) {
            throw new BObStatusException("cannot insert null object into xmrtable");
        }
        String query = """
                  INSERT INTO public.xmuniactivation(
                    	activationid, xmuni_municode, humanconduit_humanid, 
                       linkrole_roleid, parentkey, activationts, 
                       activationby_umapid, exitts)
                    	VALUES (DEFAULT, ?, ?, 
                                ?, ?, now(), 
                                ?, NULL);
                """;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection con = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            if(xmr.getxMuni() != null){
                stmt.setInt(1, xmr.getxMuni().getMuniCode());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            
            if(xmr.getHumanConduit()!= null){
                stmt.setInt(2, xmr.getHumanConduit().getHumanID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            
            if(xmr.getLinkConduitRole() != null && xmr.getLinkConduitRole().getRoleID() != 0){
                stmt.setInt(3, xmr.getLinkConduitRole().getRoleID());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            
            stmt.setInt(4, xmr.getParentKey());
                               
            if (xmr.getActivatorUMAP() != null) {
                stmt.setInt(5, xmr.getActivatorUMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            stmt.execute();
            
            String retrievalQuery = "SELECT currval('xmuniactivationid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();
            while (rs.next()) {
                freshID = rs.getInt(1);
            }
        }catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert xmr Entity", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        return freshID;
        
    }
    
    /**
     * Writes an exit TS to the xmuniactivation record given using the given TS or 
     * pg now() if xmr's exit TS is null
     * @param xmr 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void recordXMuniExit(XMuniActivationRecord xmr) throws IntegrationException{
        if(xmr == null || xmr.getActivationID() == 0){
            throw new IntegrationException("Cannot record exit TS of null xmr or xmr with ID == 0");
        } 
        String query = """
                UPDATE public.xmuniactivation
                	SET exitts=now()
                	WHERE activationid=?;
                """;
        Connection con = getPostgresCon();
        PreparedStatement pstmt = null;
        try {
            pstmt = con.prepareStatement(query);
           
            pstmt.setInt(1, xmr.getActivationID());
            pstmt.executeUpdate();
        }catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("SQL error recording XMR exit", ex);
        } finally{
            releasePostgresConnection(con, pstmt);
        } 
    }

  
}
