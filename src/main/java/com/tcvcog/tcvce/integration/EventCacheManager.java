/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheClient;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import com.tcvcog.tcvce.entities.EventCategory;
import com.tcvcog.tcvce.entities.EventCnF;
import com.tcvcog.tcvce.entities.occupancy.OccChecklistTemplate;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Holds and manages a family of object caches
 * @author Ellen Bascom of Apartment31Y
 */
@Named("eventCacheManager")
@ApplicationScoped
public class EventCacheManager implements IFaceCacheManager{
    
    @Inject
    private CECaseCacheManager ceCaseCacheManager;
    @Inject
    private OccupancyCacheManager occupancyCacheManager;
    @Inject
    private PropertyCacheManager propertyCacheManager;
    
    private boolean cachingEnabled;
    
    private Cache<Integer, EventCnF> cacheEvent;
    private Cache<Integer, EventCategory> cacheEventCategory;
    
    private final List<IFaceCacheClient> cacheClientList;
    
    public EventCacheManager(){
        cacheClientList = new ArrayList<>();
    }
    
    @PostConstruct
    public void initBean() {
        System.out.println("EventCacheManager.initBean");
        initCaching();
    }
    
    /**
     * Instantiates all our caches
     */
    @Override
    public void initCaching(){
        cacheEvent = Caffeine.newBuilder().maximumSize(20000).softValues().build();
        cacheEventCategory = Caffeine.newBuilder().maximumSize(300).softValues().build();
        
    }
    
     /**
     * Writes states
     */
    @Override
    public void writeCacheStats(){
        
        if(cacheEvent != null){
            System.out.println("cacheEvent size: " + cacheEvent.estimatedSize());
        }
        if(cacheEventCategory != null){
            System.out.println("cacheEventCategory size: " + cacheEventCategory.estimatedSize());
        }
        
    }
    
      @Override
    public void registerCacheClient(IFaceCacheClient client) {
        if(getCacheClientList() != null && !cacheClientList.contains(client)){
            getCacheClientList().add(client);
        }
    }

    @Override
    public void removeCacheClient(IFaceCacheClient client) {
        if(getCacheClientList() != null && getCacheClientList().contains(client)){
            getCacheClientList().remove(client);
        }
        
    }


    
    /**************************************************************************
    ******************              FLUSHING                *******************
    ***************************************************************************/
    
    /**
     * 
     * Dumps all my caches
     */
    @Override
    public void flushAllCaches(){
        System.out.println("EventCacheManager.flushAllCaches");
        if(cacheEvent != null){
            cacheEvent.invalidateAll();
        }
        if(cacheEventCategory != null){
            cacheEventCategory.invalidateAll();
        }
        flushUpstreamEventObjects();
        
    }
    
    /**
     * Flushes objects that are event holders
     */
    public void flushUpstreamEventObjects(){
        if(ceCaseCacheManager != null){
            ceCaseCacheManager.flushAllCaches();
        } else {
            System.out.println("EventCacheManager.flushUpstreamEventObjects | No CDI ceCaseCacheManager");
        }
        if(occupancyCacheManager != null){
            occupancyCacheManager.getCacheOccPeriod().invalidateAll();
            occupancyCacheManager.getCacheOccPermit().invalidateAll();
        } else {
            System.out.println("EventCacheManager.flushUpstreamEventObjects | No CDI occupancyCacheManager");
        }
        
    }
    
    
   
    @Override
    public void flushObjectFromCache(IFaceCachable cable) {
        if(cable != null){
            flushKey(cable.getCacheKey());
        }
    }
    
    @Override
    public void flushObjectFromCache(int cacheKey) {
        flushKey(cacheKey);
    }
    
    private void flushKey(int key){
        if(cacheEvent != null){
            cacheEvent.invalidate(key);
        }
        if(cacheEventCategory != null){
            cacheEventCategory.invalidate(key);
        }
        flushUpstreamEventObjects();
        
    }
    
    /**
     * Primary pathway for flusing event caches and caches of objects with events inside them
     * @param ev 
     */
    public void flush(EventCnF ev){
         // flush our caches
        cacheEvent.invalidate(ev.getCacheKey());
        // figure out which cache to dump
        switch(ev.getDomain()){
            case CODE_ENFORCEMENT -> ceCaseCacheManager.flushObjectFromCache(ev.getParentID());
            case OCCUPANCY -> occupancyCacheManager.flushObjectFromCache(ev.getParentID());
            case PARCEL -> propertyCacheManager.flushObjectFromCache(ev.getParentID());
        }
    }

    /**
     * Object specific flusher
     * @param cat 
     */
    public void flush(EventCategory cat){
        cacheEvent.invalidateAll();
        cacheEventCategory.invalidate(cat.getCacheKey());
        flushUpstreamEventObjects();
        
    }
  
    
    
    
    /**************************************************************************
    ******************              GETTERS AND SETTERS     *******************
    ***************************************************************************/
    
    @Override
    public boolean isCachingEnabled() {
        return cachingEnabled;
    }

    @Override
    public void disableClientCaching() {
        cachingEnabled = false;
        
    }
    

    @Override
    public void enableClientCaching() {
        cachingEnabled = true;
    }

    /**
     * @return the cacheClientList
     */
    public List<IFaceCacheClient> getCacheClientList() {
        return cacheClientList;
    }

   
    /**
     * @return the cacheEvent
     */
    public Cache<Integer, EventCnF> getCacheEvent() {
        return cacheEvent;
    }

    /**
     * @param cacheEvent the cacheEvent to set
     */
    public void setCacheEvent(Cache<Integer, EventCnF> cacheEvent) {
        this.cacheEvent = cacheEvent;
    }

    /**
     * @return the cacheEventCategory
     */
    public Cache<Integer, EventCategory> getCacheEventCategory() {
        return cacheEventCategory;
    }

    /**
     * @param cacheEventCategory the cacheEventCategory to set
     */
    public void setCacheEventCategory(Cache<Integer, EventCategory> cacheEventCategory) {
        this.cacheEventCategory = cacheEventCategory;
    }

    

    
    
}
