/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheClient;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import com.tcvcog.tcvce.entities.CodeElement;
import com.tcvcog.tcvce.entities.CodeElementGuideEntry;
import com.tcvcog.tcvce.entities.CodeSet;
import com.tcvcog.tcvce.entities.CodeSource;
import com.tcvcog.tcvce.entities.EnforceableCodeElement;
import com.tcvcog.tcvce.entities.occupancy.OccChecklistTemplate;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import jakarta.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Holds and manages a family of object caches
 * @author Ellen Bascom of Apartment31Y
 */
@Named("codeCacheManager")
@ApplicationScoped
public class CodeCacheManager  implements IFaceCacheManager{
    
    @Inject
    private CECaseCacheManager ceCaseCacheManager;
    @Inject
    private OccInspectionCacheManager occInspectionCacheManager;
    
    private boolean cachingEnabled;
    
    private Cache<Integer, CodeElement> cacheCodeElement;
    private Cache<Integer, EnforceableCodeElement> cacheEnforcableCodeElement;
    private Cache<Integer, CodeSource> cacheCodeSource;
    
    private Cache<Integer, CodeSet> cacheCodeSet;
    private Cache<Integer, CodeElementGuideEntry> cacheCodeElementGuideEntry;
    
    private final List<IFaceCacheClient> cacheClientList;
    
    public CodeCacheManager(){
        cacheClientList = new ArrayList<>();
    }
    
    @PostConstruct
    public void initBean() {
        System.out.println("CodeCacheManager.initBean");
        initCaching();
    }
    
    /**
     * Instantiates all our caches
     */
    @Override
    public void initCaching(){
        // checklists
        cacheCodeElement = Caffeine.newBuilder().maximumSize(3200).softValues().build();
        cacheEnforcableCodeElement = Caffeine.newBuilder().maximumSize(3200).softValues().build();
        cacheCodeSource = Caffeine.newBuilder().maximumSize(400).softValues().build();
        cacheCodeSet = Caffeine.newBuilder().maximumSize(1200).softValues().build();
        cacheCodeElementGuideEntry = Caffeine.newBuilder().maximumSize(300).softValues().build();
    }
    
    /**
     * Writes stats
     */
    @Override
    public void writeCacheStats(){
        System.out.println("cacheCodeElement size: " + cacheCodeElement.estimatedSize());
        System.out.println("cacheEnforcableCodeElement size: " + cacheEnforcableCodeElement.estimatedSize());
        System.out.println("cacheCodeSource size: " + cacheCodeSource.estimatedSize());
        System.out.println("cacheCodeSet size: " + cacheCodeSet.estimatedSize());
        System.out.println("cacheCodeElementGuideEntry size: " + cacheCodeElementGuideEntry.estimatedSize());

    }
    
    
    
    @Override
    public void registerCacheClient(IFaceCacheClient client) {
        if(getCacheClientList() != null && !cacheClientList.contains(client)){
            getCacheClientList().add(client);
        }
    }

    @Override
    public void removeCacheClient(IFaceCacheClient client) {
        if(getCacheClientList() != null && getCacheClientList().contains(client)){
            getCacheClientList().remove(client);
        }
        
    }
    
    /**************************************************************************
    ******************              FLUSHING                *******************
    ***************************************************************************/
    
    /**
     * 
     * Dumps all my caches
     */
    @Override
    public void flushAllCaches(){
        System.out.println("CodeCacheManager.flushAllCaches");

        if(cacheCodeElement != null) {
                cacheCodeElement.invalidateAll();
        }
        if(cacheEnforcableCodeElement != null) {
                cacheEnforcableCodeElement.invalidateAll();
        }
        if(cacheCodeSource != null) {
                cacheCodeSource.invalidateAll();
        }
        if(cacheCodeSet != null) {
                cacheCodeSet.invalidateAll();
        }
        if(cacheCodeElementGuideEntry != null) {
                cacheCodeElementGuideEntry.invalidateAll();
        }
    }
    

    @Override
    public void flushObjectFromCache(IFaceCachable cable) {
        if(cable != null){
            flushKey(cable.getCacheKey());
        }
    }
    
    @Override
    public void flushObjectFromCache(int cacheKey) {
        flushKey(cacheKey);
    }
    
    private void flushKey(int key){
        if(cacheCodeElement != null) {
                cacheCodeElement.invalidate(key);
        }
        if(cacheEnforcableCodeElement != null) {
                cacheEnforcableCodeElement.invalidate(key);
        }
        if(cacheCodeSource != null) {
                cacheCodeSource.invalidate(key);
        }
        if(cacheCodeSet != null) {
                cacheCodeSet.invalidate(key);
        }
        if(cacheCodeElementGuideEntry != null) {
                cacheCodeElementGuideEntry.invalidate(key);
        }
    }
    
     /**
     * Code elements are used system wide. This method will dump all relevant caches
     */
    public void flushCodeElementConnectedCaches(){
        // deal with caches
        // lots of folks use code elements AND they get updated somewhat frequently so 
        // let's not dump everything system wide
        
        // dump all of this class's code caches
        flushAllCaches();
        
        // only light inspections are cached, so when we get a full inspection
        // we'll ask for all these agin afresh
        occInspectionCacheManager.getCacheInspectionChecklist().invalidateAll();
        occInspectionCacheManager.getCacheOccInspectedSpace().invalidateAll();
        
        // finish this when cecas cache manager is done
        ceCaseCacheManager.flushAllCaches();
    }
    
    /**
     * Object specific flush
     * @param ele 
     */
    public void flush(CodeElement ele){
        flushCodeElementConnectedCaches();
    }
    
    /**
     * Object specific
     * @param ece 
     */
    public void flush(EnforceableCodeElement ece){
        flushCodeElementConnectedCaches();
        
    }
    
    
    /**
     * Object specific cache flush
     * @param source 
     */
    public void flush(CodeSource source){
        
        flushCodeElementConnectedCaches();
    }
    
    /**
     * Object specific
     * @param set
     */
    public void flush(CodeSet set){
        flushCodeElementConnectedCaches();
        
    }
    
    
    /**
     * Object specific
     * @param cege
     */
    public void flush(CodeElementGuideEntry cege){
        flushCodeElementConnectedCaches();
        
    }
    
    

    
    
    /**************************************************************************
    ******************          GETTERS AND SETTERS         *******************
    ***************************************************************************/
    

    @Override
    public boolean isCachingEnabled() {
        return cachingEnabled;
    }

    @Override
    public void disableClientCaching() {
        cachingEnabled = false;
    }


    @Override
    public void enableClientCaching() {
        cachingEnabled = true;
    }


    /**
     * @return the cacheClientList
     */
    public List<IFaceCacheClient> getCacheClientList() {
        return cacheClientList;
    }

   
    /**
     * @return the cacheCodeElement
     */
    public Cache<Integer, CodeElement> getCacheCodeElement() {
        return cacheCodeElement;
    }

    /**
     * @return the cacheEnforcableCodeElement
     */
    public Cache<Integer, EnforceableCodeElement> getCacheEnforcableCodeElement() {
        return cacheEnforcableCodeElement;
    }

    /**
     * @return the cacheCodeSource
     */
    public Cache<Integer, CodeSource> getCacheCodeSource() {
        return cacheCodeSource;
    }

    /**
     * @return the cacheCodeSet
     */
    public Cache<Integer, CodeSet> getCacheCodeSet() {
        return cacheCodeSet;
    }

    /**
     * @return the cacheCodeElementGuideEntry
     */
    public Cache<Integer, CodeElementGuideEntry> getCacheCodeElementGuideEntry() {
        return cacheCodeElementGuideEntry;
    }

    

    
    
}
