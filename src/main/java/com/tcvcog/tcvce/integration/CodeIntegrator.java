/*
 * Copyright (C) 2017 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheClient;
import com.tcvcog.tcvce.coordinators.CodeCoordinator;
import com.tcvcog.tcvce.coordinators.MunicipalityCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.DatabaseFetchRuntimeException;
import com.tcvcog.tcvce.domain.IntegrationException;
import java.io.Serializable;
import com.tcvcog.tcvce.entities.CodeSource;
import com.tcvcog.tcvce.entities.CodeElement;
import com.tcvcog.tcvce.entities.CodeElementGuideEntry;
import com.tcvcog.tcvce.entities.CodeSet;
import com.tcvcog.tcvce.entities.EnforceableCodeElement;
import com.tcvcog.tcvce.entities.Municipality;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import com.tcvcog.tcvce.entities.TextBlock;
import jakarta.annotation.PostConstruct;

/**
 * Database to business object layer with Caching
 * @author ellen bascomb of apt 31y
 */
public class        CodeIntegrator 
        extends     BackingBeanUtils 
        implements  Serializable, IFaceCacheClient {

    
    
      /**
     * Creates a new instance of CodeIntegrator
     */
    public CodeIntegrator() {
        
        
    }
    
    @PostConstruct
    public void initBean() {
        registerClientWithManager();
        
    }
    
    
     @Override
    public void registerClientWithManager() {
        getCodeCacheManager().registerCacheClient(this);
    }
    
    
    // *************************************************************
    // *********************CODE SOURCES****************************
    // *************************************************************
    
    

    /**
     * Cache-backed getter for CodeSource objects
     * @param sourceID
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public CodeSource getCodeSource(int sourceID) throws IntegrationException, BObStatusException{
        if(sourceID == 0){
            throw new IntegrationException("cannot get code source of ID == 0");
        }
        if(getCodeCacheManager().isCachingEnabled()){
            return getCodeCacheManager().getCacheCodeSource().get(sourceID, k -> fetchCodeSource(sourceID));
        } else {
            return fetchCodeSource(sourceID);
        }
        
    }
    
  
    /**
     * Retrieves code source information given a sourceID
     * @param sourceID the unique ID number of the code source in the DB
     * @return the CodeSource object built from the DB read
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public CodeSource fetchCodeSource(int sourceID) throws DatabaseFetchRuntimeException{
        CodeSource source = null;

        String query = """
                       SELECT   sourceid, name, year, 
                                description, url, 
                                codesource.notes as csnotes, muni_municode, crossmuni, 
                                codesource.createdts, codesource.createdby_umapid, codesource.lastupdatedts, 
                                codesource.lastupdatedby_umapid, codesource.deactivatedts, codesource.deactivatedby_umapid,
                                municipality.muniname as mname
                        	FROM public.codesource LEFT OUTER JOIN public.municipality ON (codesource.muni_municode = municipality.municode)
                                WHERE sourceid=?;
                       """;
        
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, sourceID);
            rs = stmt.executeQuery();
            while(rs.next()){
                source = populateCodeSourceMetatdataFromRS(rs);

            }
            
        } catch (SQLException | BObStatusException | IntegrationException ex) { 
             System.out.println(ex.toString());
             throw new DatabaseFetchRuntimeException("Exception in CodeIntegrator.getCodeSource", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
        
        return source;
    }
    
    
    

    /**
     * Utility method for use by methods in this Integrator which allows 
     * easy filling of code-related objects with fully-baked CodeSource objects
     * Note that client methods must manage cursor positions on the ResultSet 
     * passed in.
     *     
     * @param rs
     * 
     * @param source Clients are responsible for instantiating an emtpy CodeSource
     * and passing a reference into this method
     * 
     * @return a fully-baked CodeSource from the database
     * 
     * @throws SQLException 
     */
    private CodeSource populateCodeSourceMetatdataFromRS(ResultSet rs) throws SQLException, IntegrationException, BObStatusException{
        
        SystemIntegrator si = getSystemIntegrator();
        CodeSource source = new CodeSource();
        
        source.setSourceID(rs.getInt("sourceid"));
        source.setSourceName(rs.getString("name"));
        source.setSourceYear(rs.getInt("year"));
        source.setSourceDescription(rs.getString("description"));
        source.setUrl(rs.getString("url"));
        source.setSourceNotes(rs.getString("csnotes"));
        // flattened fields used during cycle hunting
        source.setMuniCodeFlattened(rs.getInt("muni_municode"));
        source.setMuniNameFlattened(rs.getString("mname"));
        
        source.setCrossMuni(rs.getBoolean("crossmuni"));
        
        si.populateUMAPTrackedFields(source, rs, true);
        return source;
        
    }
    /**
     * Takes in a CodeSource object and inserts into the DB
     * @param sourceToInsert
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public int insertCodeSource(CodeSource sourceToInsert) throws IntegrationException{
        String query = """
                       INSERT INTO public.codesource(
                       	sourceid, name, year, 
                       description, 
                       url, notes, muni_municode, 
                       crossmuni, createdts, createdby_umapid, 
                       lastupdatedts, lastupdatedby_umapid)
                       	VALUES (DEFAULT, ?, ?, 
                       ?, 
                       ?, ?, ?, 
                       ?, now(), ?, 
                       now(), ?);
                       """;
        int freshID= 0;
        // create sql statement up here
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

         try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setString(1, sourceToInsert.getSourceName());
            stmt.setInt(2, sourceToInsert.getSourceYear());
            
            stmt.setString(3, sourceToInsert.getSourceDescription());
            
            stmt.setString(4, sourceToInsert.getUrl());
            stmt.setString(5, sourceToInsert.getSourceNotes());
            // flattened fields used during cycle hunting
            if(sourceToInsert.getMuniCodeFlattened() != 0){
                stmt.setInt(6, sourceToInsert.getMuniCodeFlattened());
            } else {
                stmt.setInt(6, java.sql.Types.NULL);
            }
            
            stmt.setBoolean(7, sourceToInsert.isCrossMuni());
            if(sourceToInsert.getCreatedby_UMAP() != null){
                stmt.setInt(8, sourceToInsert.getCreatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setInt(8, java.sql.Types.NULL);
            }
            if(sourceToInsert.getLastUpdatedby_UMAP()!= null){
                stmt.setInt(9, sourceToInsert.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setInt(9, java.sql.Types.NULL);
            }
            
            stmt.execute();
            
            String retrievalQuery = "SELECT currval('codesource_sourceid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                freshID = rs.getInt(1);
            }
            
        } catch (SQLException ex) { 
             System.out.println(ex.toString());
             throw new IntegrationException("Cannot insert code source");
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
         
         return freshID;
    } 
    
    /**
     * Updates fields in the DB for a given CodeSource objects
     * @param sourceToUpdate
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void updateCodeSourceMetadata(CodeSource sourceToUpdate) throws IntegrationException{
         String query = """
                        UPDATE public.codesource
                         	SET name=?, year=?, description=?, 
                                url=?, notes=?, muni_municode=?, 
                                crossmuni=?, lastupdatedts=now(), lastupdatedby_umapid=?
                         	WHERE sourceid=?;
                        """;

        // create sql statement up here
        Connection con = null;
        PreparedStatement stmt = null;

         try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setString(1, sourceToUpdate.getSourceName());
            stmt.setInt(2, sourceToUpdate.getSourceYear());
            stmt.setString(3, sourceToUpdate.getSourceDescription());
            
            stmt.setString(4, sourceToUpdate.getUrl());
            stmt.setString(5, sourceToUpdate.getSourceNotes());
            // flattened fields used during cycle hunting
            if(sourceToUpdate.getMuniCodeFlattened() != 0){
                stmt.setInt(6, sourceToUpdate.getMuniCodeFlattened());
            } else {
                stmt.setInt(6, java.sql.Types.NULL);
            }
            
            stmt.setBoolean(7, sourceToUpdate.isCrossMuni());
            if(sourceToUpdate.getLastUpdatedby_UMAP()!= null){
                stmt.setInt(8, sourceToUpdate.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setInt(8, java.sql.Types.NULL);
            }
            stmt.setInt(9, sourceToUpdate.getSourceID());

            stmt.execute();
        } catch (SQLException ex) { 
             System.out.println(ex.toString());
             throw new IntegrationException("cannot update code source");
        } finally{
             releasePostgresConnection(con, stmt);
        } 
        
        
    }
    
    /**
     * Toggles the active isactive flag on the passed code source
     * @param source the CodeSource to remove from the DB
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void deactivateCodeSource(CodeSource source) throws IntegrationException{
        if(source == null) return;
        
        String query = """
                      UPDATE public.codesource
                          	SET deactivatedts=now(), deactivatedby_umapid=?
                          	WHERE sourceid=?;
                       """;

        // create sql statement up here
        Connection con = null;
        PreparedStatement stmt = null;

         try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
           
            if(source.getDeactivatedBy_UMAP() != null){
                stmt.setInt(1, source.getDeactivatedBy_UMAP().getUserMuniAuthPeriodID());
            } else {
                throw new IntegrationException("Cannot deac code source with null deactivating UMAP");
            }
            stmt.setInt(2, source.getSourceID());
            
            stmt.executeUpdate();
             
        } catch (SQLException ex) { 
             System.out.println(ex.toString());
             throw new IntegrationException("Cannot deactivate code source");
        } finally{
             releasePostgresConnection(con, stmt);
        } 
         
    }
    
  
    /**
     * Retrieves all existing code sources from the DB and builds CodeSource
     * objects for each one, populating all the fields
     * @param includeDeac
     * @return all code sources in the DB as CodeSource objects
     * @throws IntegrationException 
     */
    public List<Integer> getCompleteCodeSourceIDList(boolean includeDeac) throws IntegrationException{
        StringBuilder sb = new StringBuilder();
        sb.append("""
                       SELECT sourceid FROM public.codesource WHERE sourceid IS NOT NULL
                   """);
        
        if(!includeDeac){
            sb.append("AND deactivatedts IS NULL");
        }
        
        sb.append(";");
        
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ArrayList<Integer> codeSources = new ArrayList();
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(sb.toString());
            rs = stmt.executeQuery();
            while(rs.next()){
                codeSources.add(rs.getInt("sourceid"));
            }
            
        } catch (SQLException ex) { 
             System.out.println(ex.toString());
             throw new IntegrationException("Exception in CodeIntegrator.getCompleteCodeSourceList", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        return codeSources;
    }
    
    
    
    
    
    // *************************************************************
    // **************CODE ELEMENTS (ORDINANCES)*********************
    // *************************************************************
   
    
    /**
     * Cache-backed getter for CodeElement
     * @param elementID
     * @return
     * @throws IntegrationException
     * @throws DatabaseFetchRuntimeException
     */
    public CodeElement getCodeElement(int elementID) throws IntegrationException, DatabaseFetchRuntimeException{
        if(elementID == 0){
            throw new IntegrationException("cannot get code element of ID == 0");
        }
        if(getCodeCacheManager().isCachingEnabled()){
            return getCodeCacheManager().getCacheCodeElement().get(elementID, k -> fetchCodeElement(elementID));
        } else {
            return fetchCodeElement(elementID);
        }
        
    }
    
   
    
        /**
     * Core method which grabs data related to a single CodeElement and populates
     * a CodeElement object which is returned to the client.
     * 
     * @param elementID
     * @return the loaded up CodeElement with database data
     * @throws DatabaseFetchRuntimeException
     */
    public CodeElement fetchCodeElement(int elementID) throws DatabaseFetchRuntimeException{
        CodeElement newCodeElement = null;
        PreparedStatement stmt = null;
        Connection con = getPostgresCon();
        // note that muniCode is not returned in this query since it is specified in the WHERE
        String query = """
                       SELECT elementid, codesource_sourceid, ordchapterno, ordchaptertitle, 
                              ordsecnum, ordsectitle, ordsubsecnum, ordsubsectitle, ordtechnicaltext, 
                              ordhumanfriendlytext, resourceurl, guideentryid, notes, legacyid, 
                              ordsubsubsecnum, subsubsectitle, useinjectedvalues, lastupdatedts, 
                              createdby_userid, lastupdatedby_userid, deactivatedts, deactivatedby_userid, 
                              createdts, headerstringstatic
                         FROM public.codeelement WHERE elementid=?;
                       """;
        ResultSet rs = null;
 
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, elementID);
            rs = stmt.executeQuery();
            
            // this query will only return 1 row since the WHERE clause selects from an PK column
            while(rs.next()){
                newCodeElement = generateCodeElement(rs);
                 
            }
        } catch (SQLException | BObStatusException | IntegrationException ex) {
            throw new DatabaseFetchRuntimeException("Exception in CodeIntegrator.getCodeElementByElementID", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        return newCodeElement;
        
    }
    
    
    /**
     * Internal Utility method for loading up a CodeElement object given
     * a result set. Used by all sorts of CodeElement related methods
     * that are getting data for many code elements back in a single result set.
     * Note that the client method is responsible for manging ResultSet cursor
     * position 
     * @param rs With cursor positioned at the row to extract and populate from
     * @param e an empty CodeElement. Client class is responsible for instantiation
     * @return a populated CodeElement extracted from that row in the ResultSet
     */
    private CodeElement generateCodeElement(ResultSet rs) throws SQLException, IntegrationException, BObStatusException{
        if(rs == null) return null;
        CodeElement e = new CodeElement();
        SystemIntegrator si = getSystemIntegrator();
        // to ease the eyes, line spacing corresponds to the field spacing in CodeElement

        e.setElementID(rs.getInt("elementid"));

        e.setGuideEntryID(rs.getInt("guideentryid"));
        if(rs.getInt("guideentryid") != 0){
            e.setGuideEntry(getCodeElementGuideEntry(rs.getInt("guideentryid")));
        } else {
            e.setGuideEntry(null);
        }

        e.setSource(getCodeSource(rs.getInt("codesource_sourceid")));

        e.setOrdchapterNo(rs.getInt("ordchapterno"));

        e.setOrdchapterTitle(rs.getString("ordchaptertitle"));
        e.setOrdSecNum(rs.getString("ordsecnum"));
        e.setOrdSecTitle(rs.getString("ordsectitle"));

        e.setOrdSubSecNum(rs.getString("ordsubsecnum"));
        e.setOrdSubSecTitle(rs.getString("ordsubsectitle"));
        e.setOrdSubSecNum(rs.getString("ordsubsecnum"));

        e.setOrdSubSubSecNum(rs.getString("ordsubsubsecnum"));
        e.setOrdSubSubSecTitle(rs.getString("subsubsectitle"));

        e.setOrdTechnicalText(rs.getString("ordtechnicaltext"));

        e.setOrdHumanFriendlyText(rs.getString("ordhumanfriendlytext"));
        e.setUsingInjectedValues(rs.getBoolean("useinjectedvalues"));

        e.setResourceURL(rs.getString("resourceurl"));
        e.setNotes(rs.getString("notes"));
        
        e.setHeaderStringStatic(rs.getString("headerstringstatic"));

        si.populateTrackedFields(e, rs, true);
        return e;
    }
    
  
    
    /**
     * Key method for returning a fully-assembled link of code elements by source ID.
     * Each code element returned by a single call to this method contains its own 
 instance of a CodeSource object whose field values are identical.
 
 NOTE these are CodeElement that can become EnforcableCodeElement when they are
 added to a codset (which is, in turn, associated with a single muni)
     * 
     * @param sourceID the CodeSource id used in the WHERE clause of the embedded SQL statment
     * @return Fully-baked CodeElement objects in a ArrayList
     * @throws IntegrationException Caught by backing beans and converted into
     * user messages
     */
    public List<CodeElement> getCodeElements(int sourceID) throws IntegrationException, BObStatusException{
        String query = "SELECT elementid from codeelement where codesource_sourceID = ? AND deactivatedts IS NULL;";
        CodeCoordinator cc = getCodeCoordinator();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<CodeElement> elementList = new ArrayList();
         try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            
            stmt.setInt(1, sourceID);
            
            rs = stmt.executeQuery();
            
            while(rs.next()){
                elementList.add(cc.getCodeElement(rs.getInt("elementid")));
            }
             
        } catch (SQLException ex) { 
             System.out.println(ex.toString());
             throw new IntegrationException("Error Retrieving code element list", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        
         return elementList;
    } // close getCodeElements
    
    
    
    public int insertCodeElement(CodeElement element) throws IntegrationException{
         String query = "INSERT INTO public.codeelement(\n" +
                        "            elementid, codesource_sourceid, ordchapterno, ordchaptertitle, \n" +
                        "            ordsecnum, ordsectitle, ordsubsecnum, ordsubsectitle, ordtechnicaltext, \n" +
                        "            ordhumanfriendlytext, resourceurl, guideentryid, notes, legacyid, \n" +
                        "            ordsubsubsecnum, subsubsectitle, useinjectedvalues, lastupdatedts, \n" +
                        "            createdby_userid, lastupdatedby_userid, \n" +
                        "            createdts)\n" +
                        "    VALUES (DEFAULT, ?, ?, ?, \n" +
                        "            ?, ?, ?, ?, ?, \n" +
                        "            ?, ?, ?, ?, ?, \n" +
                        "            ?, ?, ?, now(), \n" +
                        "            ?, ?, \n" +
                        "            now());";

        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int freshID = 0;

         try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            
            if(element.getSource() != null){
                stmt.setInt(1, element.getSource().getSourceID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            stmt.setInt(2, element.getOrdchapterNo());
            stmt.setString(3, element.getOrdchapterTitle());
            
            stmt.setString(4, element.getOrdSecNum());
            stmt.setString(5, element.getOrdSecTitle());
            stmt.setString(6, element.getOrdSubSecNum());
            stmt.setString(7, element.getOrdSubSecTitle());
            stmt.setString(8, element.getOrdTechnicalText());
            
            stmt.setString(9, element.getOrdHumanFriendlyText());
            stmt.setString(10, element.getResourceURL());
            if(element.getGuideEntryID() != 0){
                stmt.setInt(11, element.getGuideEntryID());
            } else {
                stmt.setNull(11, java.sql.Types.NULL);
            }
            stmt.setString(12, element.getNotes());
            stmt.setInt(13, element.getLegacyID());
            
            stmt.setString(14, element.getOrdSubSubSecNum());
            stmt.setString(15, element.getOrdSubSubSecTitle());
            stmt.setBoolean(16, element.isUsingInjectedValues());
            
            if(element.getCreatedBy() != null){
                stmt.setInt(17, element.getCreatedBy().getUserID());
            } else{
                stmt.setNull(17, java.sql.Types.NULL);
            }
            
            if(element.getLastUpdatedBy() != null){
                stmt.setInt(18, element.getLastUpdatedBy().getUserID());
            } else{
                stmt.setNull(18, java.sql.Types.NULL);
            }
            
            stmt.execute();
            
            String retrievalQuery = "SELECT currval('codeelement_elementid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                freshID = rs.getInt(1);
            }
            
        } catch (SQLException ex) { 
             System.out.println(ex.toString());
             throw new IntegrationException("Error inserting code element", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
         
         return freshID;
    }
    
    /**
     * Updates a record in the codeelement table
     * @param element
     * @throws IntegrationException 
     */
    public void updateCodeElement(CodeElement element) throws IntegrationException{
          String query = """
                         UPDATE public.codeelement
                            SET codesource_sourceid=?, ordchapterno=?, ordchaptertitle=?, 
                                ordsecnum=?, ordsectitle=?, ordsubsecnum=?, ordsubsectitle=?, 
                                ordtechnicaltext=?, ordhumanfriendlytext=?, resourceurl=?, guideentryid=?, 
                                notes=?, legacyid=?, ordsubsubsecnum=?, subsubsectitle=?, useinjectedvalues=?, 
                                lastupdatedts=now(), lastupdatedby_userid=? 
                          WHERE elementid=?;
                         """;

        Connection con = null;
        PreparedStatement stmt = null;

         try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
           
            
            // no source changes on element update
            //stmt.setInt(2, element.getSource().getSourceID());
            
            if(element.getSource() != null){
                stmt.setInt(1, element.getSource().getSourceID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            stmt.setInt(2, element.getOrdchapterNo());
            stmt.setString(3, element.getOrdchapterTitle());
            
            stmt.setString(4, element.getOrdSecNum());
            stmt.setString(5, element.getOrdSecTitle());
            stmt.setString(6, element.getOrdSubSecNum());
            stmt.setString(7, element.getOrdSubSecTitle());
            
            stmt.setString(8, element.getOrdTechnicalText());
            stmt.setString(9, element.getOrdHumanFriendlyText());
            stmt.setString(10, element.getResourceURL());
            if(element.getGuideEntryID() != 0){
                stmt.setInt(11, element.getGuideEntryID());
            } else {
                stmt.setNull(11, java.sql.Types.NULL);
            }
            stmt.setString(12, element.getNotes());
            stmt.setInt(13, element.getLegacyID());
            stmt.setString(14, element.getOrdSubSubSecNum());
            stmt.setString(15, element.getOrdSubSubSecTitle());
            stmt.setBoolean(16, element.isUsingInjectedValues());
                        
            if(element.getLastUpdatedBy() != null){
                stmt.setInt(17, element.getLastUpdatedBy().getUserID());
            } else{
                stmt.setNull(17, java.sql.Types.NULL);
            }
            
            stmt.setInt(18, element.getElementID());
            stmt.executeUpdate();
        } catch (SQLException ex) { 
             System.out.println(ex.toString());
             throw new IntegrationException("Error updating code element", ex);
        } finally{
             releasePostgresConnection(con, stmt);
        } 
       
    }
    
    /**
     * Updates a record in the codeelement table
     * @param element
     * @throws IntegrationException 
     */
    public void updateCodeElementTechnicalTextOnly(CodeElement element) throws IntegrationException{
          String query = """
                         UPDATE public.codeelement
                            SET ordtechnicaltext=?, lastupdatedts=now(), lastupdatedby_userid=? 
                          WHERE elementid=?;
                         """;

        Connection con = null;
        PreparedStatement stmt = null;

         try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            
            stmt.setString(1, element.getOrdTechnicalText());
                        
            if(element.getLastUpdatedBy() != null){
                stmt.setInt(2, element.getLastUpdatedBy().getUserID());
            } else{
                stmt.setNull(2, java.sql.Types.NULL);
            }
            
            stmt.setInt(3, element.getElementID());
            stmt.executeUpdate();
        } catch (SQLException ex) { 
             System.out.println(ex.toString());
             throw new IntegrationException("Error updating code element technical text only", ex);
        } finally{
             releasePostgresConnection(con, stmt);
        } 
         
    }
    
    /**
     * Updates a record in the codeelement table
     * @param element
     * @throws IntegrationException 
     */
    public void updateCodeElementHeaderStringStatic(CodeElement element) throws IntegrationException{
          String query = """
                         UPDATE public.codeelement
                            SET headerstringstatic=?, lastupdatedts=now()
                          WHERE elementid=?;
                         """;

        Connection con = null;
        PreparedStatement stmt = null;

         try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
           
            
            // no source changes on element update
            //stmt.setInt(2, element.getSource().getSourceID());
            
            stmt.setString(1, element.getHeaderStringCandidate());
            
            
            stmt.setInt(2, element.getElementID());
            stmt.executeUpdate();
        } catch (SQLException ex) { 
             System.out.println(ex.toString());
             throw new IntegrationException("Error updating code element static header", ex);
        } finally{
             releasePostgresConnection(con, stmt);
        } 
         
    }
    
    /**
     * Sets the deactivationts in the codeelement table to signal deletion
     * 
     * @param element
     * @throws IntegrationException 
     */
    public void deactivateCodeElement(CodeElement element) throws IntegrationException{
        String query =  "UPDATE codeelement SET deactivatedts = now(), deactivatedby_userid=? WHERE elementid=?;";
        Connection con = null;
        PreparedStatement stmt = null;

         try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            if(element.getDeactivatedBy() != null){
                stmt.setInt(1, element.getDeactivatedBy().getUserID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            stmt.setInt(2, element.getElementID());
            stmt.execute();
             
        } catch (SQLException ex) { 
             System.out.println(ex.toString());
             throw new IntegrationException("Unable to deactivate code element--"
                     + "probably because it has been used somewhere in the system. It's here to stay.", ex);
        } finally{
             releasePostgresConnection(con, stmt);
        } 
         
    }
    
    
    // *************************************************************
    // ******CODE SETS and CODE SET ELEMENTS  (Code Books)**********
    // *************************************************************
   
     
    /**
     * Updates a record in the codeset table, which declares a schema
     * for organizing enforcable code elements
     * @param set
     * @throws IntegrationException 
     */
    public void updateCodeSetMetadata(CodeSet set) throws IntegrationException{
        if(set == null) throw  new IntegrationException("Cannot update a null code set");
        
        String query = "UPDATE public.codeset\n" +
            "SET name=?, description=?, municipality_municode=? WHERE codeSetid=?;";
        Connection con = null;
        PreparedStatement stmt = null;

         try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setString(1, set.getCodeSetName());
            stmt.setString(2, set.getCodeSetDescription());
            if(set.getMuniCodeFlattened()!= 0){
                stmt.setInt(3, set.getMuniCodeFlattened());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            stmt.setInt(4, set.getCodeSetID());
            stmt.executeUpdate();
             
        } catch (SQLException ex) { 
             System.out.println(ex.toString());
             throw new IntegrationException("Error updating code set", ex);
        } finally{
             releasePostgresConnection(con, stmt);
        } 
         
    }
    
    /**
     * Cache-backed getter for code sets
     * @param setID
     * @return
     * @throws DatabaseFetchRuntimeException
     * @throws IntegrationException 
     */
    public CodeSet getCodeSetBySetID(int setID) throws DatabaseFetchRuntimeException, IntegrationException{
        if(setID == 0){
            throw new IntegrationException("cannot get code element of ID == 0");
        }
        if(getCodeCacheManager().isCachingEnabled()){
            return getCodeCacheManager().getCacheCodeSet().get(setID, k -> fetchCodeSetBySetID(setID));
        } else {
            return fetchCodeSetBySetID(setID);
        }
        
    }
 

    
    /**
     * Retrieves a code set from the DB
     * @param setID
     * @return
     * @throws IntegrationException 
     */
    public CodeSet fetchCodeSetBySetID(int setID) throws DatabaseFetchRuntimeException{
         String query = """
                        SELECT codesetid, name, description, municipality_municode, active, municipality.muniname as mname  
                        FROM public.codeset LEFT OUTER JOIN public.municipality ON (codeset.municipality_municode = municipality.municode)  WHERE codesetid = ?
                        """;
        
         //System.out.println("CodeIntegrator.getCodeSets | MuniCode: "+ muniCode);
        
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        CodeSet cs = null;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, setID);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                cs = populateCodeSetFromRS(rs);
            }
            
        } catch (SQLException | BObStatusException | IntegrationException ex) { 
             System.out.println("CodeIntegrator.getCodeSetBySetID | " + ex.toString());
             throw new DatabaseFetchRuntimeException("Exception in CodeSetIntegrator", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        return cs;
        
    }
    
    /**
     * Builds a mapping of municipalities to code sets
     * @return
     * @throws IntegrationException 
     */
    public HashMap<Municipality, CodeSet> getMuniDefaultCodeSetMap() throws IntegrationException, BObStatusException{
        HashMap<Municipality, CodeSet> muniSetMap = new HashMap<>();
        MunicipalityCoordinator mc = getMuniCoordinator();
        
        String query = "SELECT municode, defaultcodeset FROM public.municipality;";
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                muniSetMap.put(mc.getMuni(rs.getInt("municode")), getCodeSetBySetID(rs.getInt("defaultcodeset")));
            }
            
        } catch (SQLException ex) { 
             System.out.println("CodeIntegrator.getCodeSetBySetID | " + ex.toString());
             throw new IntegrationException("Exception in CodeSetIntegrator", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        return muniSetMap;
    }
    
   
    /**
     * Genenerator of CodeSet objects from a codeset record
     * @param rs
     * @return
     * @throws SQLException
     * @throws IntegrationException 
     */
    private CodeSet populateCodeSetFromRS(ResultSet rs) throws SQLException, IntegrationException, BObStatusException{
        CodeSet set = new CodeSet();
        
        set.setCodeSetID(rs.getInt("codesetid"));
        set.setCodeSetName(rs.getString("name"));
        set.setCodeSetDescription(rs.getString("description"));
        // the key call: grab a list of all enforcable code elements in this set (large)
        set.setEnfCodeElementList(getEnforcableCodeElementList(rs.getInt("codesetid")));
        // removed muni object in a CodeSet belly during cycle hunt!
        set.setMuniCodeFlattened(rs.getInt("municipality_municode"));
        set.setMuniNameFlattened(rs.getString("mname"));
        set.setActive(rs.getBoolean("active"));

        return set;
        
    }
    
    
    
    /**
     * Extracts all code sets from the codeset table
     * @return
     * @throws IntegrationException 
     */
    public ArrayList getCodeSets() throws IntegrationException, BObStatusException {
        String query = "SELECT codesetid \n"
                + "  FROM public.codeset WHERE active=TRUE;";

        //System.out.println("CodeIntegrator.getCodeSets | MuniCode: "+ muniCode);
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ArrayList<CodeSet> codeSetList = new ArrayList();

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();

            while (rs.next()) {
                codeSetList.add(getCodeSetBySetID(rs.getInt("codesetid")));
            }

        } catch (SQLException ex) {
            System.out.println("CodeIntegrator.getCodeSetByMuniCode | " + ex.toString());
            throw new IntegrationException("Exception in CodeSetIntegrator", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return codeSetList;
    }
    
    /**
     * Gets all codesets (codebooks) for a given municipality
     * @param muniCode
     * @return
     * @throws IntegrationException 
     */
    public List getCodeSets(int muniCode) throws IntegrationException, BObStatusException{
         String query = "SELECT codesetid \n" +
                        "FROM public.codeset WHERE municipality_municode = ?";
        
         //System.out.println("CodeIntegrator.getCodeSets | MuniCode: "+ muniCode);
        
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ArrayList<CodeSet> codeSetList = new ArrayList();
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, muniCode);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                codeSetList.add(getCodeSetBySetID(rs.getInt("codesetid")));
            }
            
        } catch (SQLException ex) { 
             System.out.println("CodeIntegrator.getCodeSetByMuniCode | " + ex.toString());
             throw new IntegrationException("Exception in CodeSetIntegrator", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        return codeSetList;
    }
    
    /**
     * Cache-backed getting for CSEs
     * @param codeSetElementID
     * @return
     * @throws DatabaseFetchRuntimeException
     * @throws IntegrationException 
     */
      public EnforceableCodeElement getEnforcableCodeElement(int codeSetElementID) throws DatabaseFetchRuntimeException, IntegrationException{
        if(codeSetElementID == 0){
            throw new IntegrationException("cannot get code set element of ID == 0");
        }
        if(getCodeCacheManager().isCachingEnabled()){
            return getCodeCacheManager().getCacheEnforcableCodeElement().get(codeSetElementID, k -> fetchEnforcableCodeElement(codeSetElementID));
        } else {
            return fetchEnforcableCodeElement(codeSetElementID);
        }
        
    }
    
    
    
     /**
     * Creates and populates single EnforceableCodeElement object based on giving ID number. 
     * 
     * @param codeSetElementID
     * @return the fully-baked EnforceableCodeElement
     * @throws DatabaseFetchRuntimeException
     */
    public EnforceableCodeElement fetchEnforcableCodeElement(int codeSetElementID) throws DatabaseFetchRuntimeException{
        EnforceableCodeElement newEce = null;
        PreparedStatement stmt = null;
        Connection con = null;
        String query = """
                SELECT codesetelementid, codeset_codesetid, codelement_elementid, elementmaxpenalty,
                elementminpenalty, elementnormpenalty, penaltynotes, normdaystocomply,
                daystocomplynotes, munispecificnotes, defaultviolationdescription, defaultseverityclass_classid, 
                createdts, createdby_userid,
                lastupdatedts, lastupdatedby_userid, deactivatedts, deactivatedby_userid
                FROM public.codesetelement WHERE codesetelementid=?;
                """;
        ResultSet rs = null;
 
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, codeSetElementID);
            rs = stmt.executeQuery();
            while(rs.next()){
                newEce = generateEnforcableCodeElement(rs);
            }
            
        } catch (SQLException | BObStatusException | IntegrationException  ex) {
            System.out.println("CodeIntegrator.getEnforcableCodeElement | " + ex.toString());
            throw new DatabaseFetchRuntimeException("CodeIntegrator.getEnforcableCodeElement", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        return newEce;
    }
    
  /**
   * Returns a ArrayList of fully-baked EnforcableCodeElement objects based on a search with
   * setID. Handy for then populating data tables of codes, such as in creating
   * a codeviolation. This involves a rather complicated object composition process
   * that draws on several other methods in this class for retrieving from the database
   * 
   * @param setID
   * @return all CodeElement objects kicked out by postgres with that setID
   * @throws com.tcvcog.tcvce.domain.IntegrationException
   */
    public ArrayList getEnforcableCodeElementList(int setID) throws IntegrationException, BObStatusException{
        PreparedStatement stmt = null;
        Connection con = null;
        String query = "SELECT codesetelementid " +
                " FROM public.codesetelement WHERE codeset_codesetid=? AND deactivatedts IS NULL;";
        ResultSet rs = null;
        ArrayList<EnforceableCodeElement> eceList = new ArrayList();
 
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, setID);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                
                eceList.add(getEnforcableCodeElement(rs.getInt("codesetelementid")));
                
            }
        } catch (SQLException ex) {
            System.out.println("codeIntegrator.getEnforcableCodeElementList| " + ex.toString());
            throw new IntegrationException("codeIntegrator.getEnforcableCodeElementList", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return eceList;
    }
    
    /**
     * Generator of EnforceableCodeElement objects given a RS
     * @param rs with all fields SELECTed
     * @return the populated object
     * @throws SQLException
     * @throws IntegrationException 
     */
    private EnforceableCodeElement generateEnforcableCodeElement(ResultSet rs) throws SQLException, IntegrationException, BObStatusException{
        
        CodeCoordinator cc = getCodeCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        CodeElement ele = cc.getCodeElement(rs.getInt("codelement_elementid"));
        
        EnforceableCodeElement newEce = new EnforceableCodeElement(ele);
        
        newEce.setCodeSetID(rs.getInt("codeset_codesetid"));
        newEce.setCodeSetElementID(rs.getInt("codesetelementid"));
        
        newEce.setMaxPenalty(rs.getInt("elementmaxpenalty"));
        newEce.setMinPenalty(rs.getInt("elementminpenalty"));
        newEce.setNormPenalty(rs.getInt("elementnormpenalty"));
        newEce.setPenaltyNotes(rs.getString("penaltynotes"));
        newEce.setNormDaysToComply(rs.getInt("normdaystocomply"));
        newEce.setDaysToComplyNotes(rs.getString("daystocomplynotes"));
        newEce.setMuniSpecificNotes(rs.getString("munispecificnotes"));
        
        // DEAC DURING CHARGE UPGRADE
//        newEce.setFeeList(pi.getFeeList(newEce));
        newEce.setDefaultViolationDescription(rs.getString("defaultviolationdescription"));

        int classId = rs.getInt("defaultseverityclass_classid");
        if(classId != 0){
            newEce.setDefaultViolationSeverity(sc.getIntensityClass(classId));
        }

        if(rs.getTimestamp("createdts") != null){
            newEce.setEceCreatedTS(rs.getTimestamp("createdts").toLocalDateTime());                
        }
        newEce.setEceCreatedByUserID(rs.getInt("createdby_userid"));

        if(rs.getTimestamp("lastupdatedts") != null){
            newEce.setEceLastUpdatedTS(rs.getTimestamp("lastupdatedts").toLocalDateTime());
        }
        newEce.setEceLastUpdatedByUserID(rs.getInt("lastupdatedby_userid"));

        if(rs.getTimestamp("deactivatedts") != null){
            newEce.setEceDeactivatedTS(rs.getTimestamp("deactivatedts").toLocalDateTime());
        }
        newEce.setDeactivatedByUserID(rs.getInt("deactivatedby_userid"));

        return newEce;
    }
    
    /**
     * Creates what we call an EnforceableCodeElement, which means
 we find an existing code element and add muni-specific 
 enforcement data to that element and store it in the DB
 
 This operation adds an entry to table codesetelement
 and uses the ID of the codeSet and CodeElement to make
 the many-to-many links in the database.
     * 
     * @param ece
     * @return  
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public int insertEnforcableCodeElementToCodeSet(EnforceableCodeElement ece) throws IntegrationException{
        PreparedStatement stmt = null;
        Connection con = null;
        String query = "INSERT INTO public.codesetelement(\n" +
                    "codesetelementid, codeset_codesetid, codelement_elementid, elementmaxpenalty, \n" +
                    "elementminpenalty, elementnormpenalty, penaltynotes, normdaystocomply, \n" +
                    "daystocomplynotes, munispecificnotes, defaultseverityclass_classid, fee_feeid, defaultviolationdescription,"
                    + "createdts, createdby_userid, lastupdatedts, lastupdatedby_userid)\n" +
                    " VALUES (DEFAULT, ?, ?, ?, \n" +
                    "?, ?, ?, ?, \n" +
                    "?, ?, ?, ?, ?, \n"
                    + "now(), ?, now(), ?);";
        ResultSet rs = null;
        int freshECEID = 0;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, ece.getCodeSetID());
            stmt.setInt(2, ece.getElementID() );
            stmt.setDouble(3, ece.getMaxPenalty());
            
            stmt.setDouble(4, ece.getMinPenalty());
            stmt.setDouble(5, ece.getNormPenalty());
            stmt.setString(6, ece.getPenaltyNotes());
            stmt.setInt(7, ece.getNormDaysToComply());
            
            stmt.setString(8, ece.getDaysToComplyNotes());
            stmt.setString(9, ece.getMuniSpecificNotes());
            if(ece.getDefaultViolationSeverity() != null){
                stmt.setInt(10, ece.getDefaultViolationSeverity().getClassID());
            } else {
                stmt.setNull(10, java.sql.Types.NULL);
            }
            
            //DEPRECATED 08.23.22 by wwalk for later usage
//            if(ece.getFeeList() != null){
//                stmt.setInt(11, ece.getFeeList()):
//            } else {
//                stmt.setNull(11, java.sql.Types.NULL);
//            }

            //wwalk, 08.23.22: feeList entry currently set to NULL
            stmt.setInt(11, java.sql.Types.NULL);
            
            stmt.setString(12, ece.getDefaultViolationDescription());
            
            if(ece.getEceCreatedByUserID() != 0){
                stmt.setInt(13, ece.getEceCreatedByUserID());
            } else {
                stmt.setNull(13, java.sql.Types.NULL);
            }

            if(ece.getEceLastUpdatedByUserID() != 0){
                stmt.setInt(14, ece.getEceLastUpdatedByUserID());
            } else {
                stmt.setNull(14, java.sql.Types.NULL);
            }
            
            stmt.execute();

            String retrievalQuery = "SELECT currval('codesetelement_elementid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                freshECEID = rs.getInt(1);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Exception in CodeIntegrator.addEnforcableCodeElementToCodeSet", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        
        return freshECEID;
    }
    
    /**
     * Updates a record in the codesetelement table
     * 
     * @param ece
     * @throws IntegrationException 
     */
    public void updateEnforcableCodeElement(EnforceableCodeElement ece) throws IntegrationException{
        PreparedStatement stmt = null;
        Connection con = null;
        String query = """
                UPDATE public.codesetelement
                    SET elementmaxpenalty=?, elementminpenalty=?, elementnormpenalty=?,
                    penaltynotes=?, normdaystocomply=?, daystocomplynotes=?, munispecificnotes=?, 
                    defaultviolationdescription=?, defaultseverityclass_classid = ?,
                    lastupdatedts=now(), lastupdatedby_userid=?
                WHERE codesetelementid=?;
                """;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setDouble(1, ece.getMaxPenalty());
            stmt.setDouble(2, ece.getMinPenalty());
            stmt.setDouble(3, ece.getNormPenalty());
            
            stmt.setString(4, ece.getPenaltyNotes());
            stmt.setInt(5, ece.getNormDaysToComply());
            stmt.setString(6, ece.getDaysToComplyNotes());
            stmt.setString(7, ece.getMuniSpecificNotes());

            stmt.setString(8, ece.getDefaultViolationDescription());
            if (Objects.nonNull(ece.getDefaultViolationSeverity())) {
                stmt.setInt(9, ece.getDefaultViolationSeverity().getClassID());
            } else {
                stmt.setNull(9, java.sql.Types.NULL);
            }

            if (Objects.nonNull(ece.getLastUpdatedBy())) {
                stmt.setInt(10, ece.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(10, java.sql.Types.NULL);
            }
            
            stmt.setInt(11, ece.getCodeSetElementID());
            System.out.println("CodeIntegrator.updateEnforcableCodeElement | ece update: " + stmt.toString());
            
            stmt.execute();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update enforcable code element data", ex);
        } finally{
           releasePostgresConnection(con, stmt);
        } 
        
        
    }
    
    /**
     * Deactivates a codesetelement by setting its deactivatedts field
     * @param ece
     * @throws IntegrationException 
     */
    public void deactivateEnforcableCodeElement(EnforceableCodeElement ece ) throws IntegrationException{
        if(ece == null){
            throw new IntegrationException("Cannot nuke a null ECE!");
        } 
        PreparedStatement stmt = null;
        Connection con = null;
        String query =  "UPDATE public.codesetelement SET deactivatedts = now(), deactivatedby_userid=? \n" +
                        " WHERE codesetelementid = ?;";
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            
            if(ece.getEceDeactivatedByUserID() != 0){
                stmt.setInt(1, ece.getEceDeactivatedByUserID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            
            stmt.setInt(2, ece.getCodeSetElementID());
            
            System.out.println("CodeIntegratator.deacECE: eceid: " + ece.getCodeSetElementID() );
            System.out.println("CodeIntegratator.deacECE: stmt: " + stmt.getParameterMetaData());
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to delete enforcable code element: "
                    + "it is probably used somewhere in the system and is here to stay!", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
        
    }
    
    /*************************************************************************/
    /*****************    DEFAULT FINDINGS        ****************************/
    /*************************************************************************/
    
    /**
     * Extracts all text blocks linked to a given ece as a default finding container
     * @param ece
     * @return
     * @throws IntegrationException 
     */
    public List<TextBlock> getDefaultFindingsByEnforceableCodeElement(EnforceableCodeElement ece) throws IntegrationException, BObStatusException{
        if(ece == null){
            throw new IntegrationException("Unable to get text blocks for null ECE");
        }
        
        SystemCoordinator sc = getSystemCoordinator();
        
        PreparedStatement stmt = null;
        Connection con = null;
        String query = "SELECT textblock_blockid FROM public.codesetelementtextblock WHERE codesetelement_cseid=?;";
        
        ResultSet rs = null;
        List<TextBlock> blockList = new ArrayList();
 
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, ece.getCodeSetElementID());
            rs = stmt.executeQuery();
            
            while(rs.next()){
                
                blockList.add(sc.getTextBlock(rs.getInt("textblock_blockid")));
                
            }
        } catch (SQLException ex) {
            System.out.println("codeIntegrator.getDefaultFindingsByEnforceableCodeElement| " + ex.toString());
            throw new IntegrationException("codeIntegrator.getDefaultFindingsByEnforceableCodeElement", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return blockList;
        
    }
    
    /**
     * Links an ECE to a text block
     * @param ece
     * @param block
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public void linkEnforceableCodeElementToTextBlock(EnforceableCodeElement ece, TextBlock block) throws IntegrationException, BObStatusException{
        if(ece == null || block == null){
            throw new IntegrationException("Unable to link ece and block with null inputs");
        }
        
        PreparedStatement stmt = null;
        Connection con = null;
        String sql = """
                     INSERT INTO public.codesetelementtextblock(
                     codesetelement_cseid, textblock_blockid)
                     VALUES (?, ?);""";
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, ece.getCodeSetElementID());
            stmt.setInt(2, block.getBlockID());
            stmt.execute();
            
        } catch (SQLException ex) {
            System.out.println("codeIntegrator.linkEnforceableCodeElementToTextBlock| " + ex.toString());
            throw new IntegrationException("codeIntegrator.linkEnforceableCodeElementToTextBlock", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
        
    }
        

    /**
     * Removes link between ECE and text block
     * @param ece
     * @param block
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public void deleteEnforceableCodeElementLinkToTextBlock(EnforceableCodeElement ece, TextBlock block) throws IntegrationException, BObStatusException{
        if(ece == null || block == null){
            throw new IntegrationException("Unable to delete link between ece and block with null inputs");
        }
        
        PreparedStatement stmt = null;
        Connection con = null;
        String sql = """
                     DELETE FROM public.codesetelementtextblock
                     WHERE codesetelement_cseid=? AND textblock_blockid=?;""";
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, ece.getCodeSetElementID());
            stmt.setInt(2, block.getBlockID());
            stmt.execute();
            
        } catch (SQLException ex) {
            System.out.println("codeIntegrator.deleteEnforceableCodeElementLinkToTextBlock| " + ex.toString());
            throw new IntegrationException("codeIntegrator.deleteEnforceableCodeElementLinkToTextBlock", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
    }
    
    
    
    
    
    
    
    
    /*----------------- end findings -----------------------------------------*/
    
    /**
     * Adds a code set listing which is then a container for enforcable code set elements
     * @param codeSetToInsert
     * @return the code set list (each code set does not have the list of enfcodeelements)
     * @throws IntegrationException 
     */
    public int insertCodeSetMetadata(CodeSet codeSetToInsert) throws IntegrationException{
        if(codeSetToInsert ==  null){
            throw new IntegrationException("cannot insert a null set");
            
        }
        PreparedStatement stmt = null;
        Connection con = null;
        // note that muniCode is not returned in this query since it is specified in the WHERE
        String query = "INSERT INTO public.codeset(\n" +
                "codesetid, name, description, municipality_municode, active)\n" +
                "VALUES (DEFAULT, ?, ?, ?, TRUE);";
        
        ResultSet rs = null;
        int freshID = 0;
 
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setString(1, codeSetToInsert.getCodeSetName());
            stmt.setString(2, codeSetToInsert.getCodeSetDescription());
            if(codeSetToInsert.getMuniCodeFlattened() != 0){
                stmt.setInt(3, codeSetToInsert.getMuniCodeFlattened());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            stmt.execute();
            
            String retrievalQuery = "SELECT currval('codeset_codesetid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                freshID = rs.getInt(1);
            }
            
        } catch (SQLException ex) {
            System.out.println("CodeIntegrator.insertCodeSetMetadata | " + ex.toString());
            throw new IntegrationException("CodeIntegrator.insertCodeSetMetadata", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return freshID;
    }
    
    /**
     * Deactivates a code set record
     * @param set
     * @throws BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
   public void deactivateCodeSet(CodeSet set) throws BObStatusException, IntegrationException{
        if(set == null) throw new BObStatusException("Cannot deactivate null code set");
        
        PreparedStatement stmt = null;
        Connection con = null;
        // note that muniCode is not returned in this query since it is specified in the WHERE
        String query = "UPDATE codeset SET active=FALSE WHERE codesetid=?";
        
 
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, set.getCodeSetID());
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("CodeIntegrator.deactivateCodeSet", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
        
   }
    
    // *************************************************************
    // ********************* CODE GUIDE ****************************
    // *************************************************************
    
    /**
     * Creates a new record in the codeelementguide table
     * @param cege
     * @throws IntegrationException 
     */
    public int insertCodeElementGuideEntry(CodeElementGuideEntry cege) throws IntegrationException{
        String query =  """
                        INSERT INTO public.codeelementguide(
                                    guideentryid, category, subcategory, description, enforcementguidelines, 
                                    inspectionguidelines, priority, createdts, createdby_umapid, lastupdatedts, lastupdatedby_umapid)
                            VALUES (DEFAULT, ?, ?, ?, ?, 
                                    ?, ?, now(), ?, now(), ?);""";
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int freshID  = 0;

         try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setString(1, cege.getCategory());
            stmt.setString(2, cege.getSubCategory());
            stmt.setString(3, cege.getDescription());
            stmt.setString(4, cege.getEnforcementGuidelines());
            stmt.setString(5, cege.getInspectionGuidelines());
            stmt.setBoolean(6, cege.isPriority());
            if(cege.getCreatedBy_UMAPID() != 0){
                stmt.setInt(7, cege.getCreatedBy_UMAPID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            if(cege.getLastUpdatedBy_UMAPID() != 0){
                stmt.setInt(8, cege.getLastUpdatedBy_UMAPID());
            } else {
                stmt.setNull(8, java.sql.Types.NULL);
            }
            
            System.out.println("CodeElementGuideBB.insertCodeElementGuideEntry | stmt: " + stmt.toString());
            
            stmt.execute();
            
            String retrievalQuery = "SELECT currval('codeelementguide_id_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                freshID = rs.getInt(1);
            }
        } catch (SQLException ex) { 
             System.out.println(ex.toString());
             throw new IntegrationException("Error inserting code element type", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
         return freshID;
    }
    
    /**
     * Updates a record in the guide table
     * @param cege
     * @throws IntegrationException 
     */
    public void updateCodeElementGuideEntry(CodeElementGuideEntry cege) throws IntegrationException{
        String query =  """
                        UPDATE public.codeelementguide
                           SET category=?, subcategory=?, description=?, enforcementguidelines=?, 
                               inspectionguidelines=?, priority=?
                         WHERE guideentryid=?;""";
        Connection con = null;
        PreparedStatement stmt = null;
        SystemIntegrator si = getSystemIntegrator();

         try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setString(1, cege.getCategory());
            stmt.setString(2, cege.getSubCategory());
            stmt.setString(3, cege.getDescription());
            stmt.setString(4, cege.getEnforcementGuidelines());
            stmt.setString(5, cege.getInspectionGuidelines());
            stmt.setBoolean(6, cege.isPriority());
            stmt.setInt(7, cege.getGuideEntryID());
             System.out.println("CodeIntegrator.updateCodeElementGuideEntry | stmt: " + stmt.toString());
            stmt.execute();
            si.updateStampUMAPTrackedEntity(cege);
             
        } catch (SQLException ex) { 
             System.out.println(ex.toString());
             throw new IntegrationException("Error updating code element guide entry", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
    }
    
    /**
     * Cache-backed getter for guide entries
     * @param entryid
     * @return
     * @throws DatabaseFetchRuntimeException
     * @throws IntegrationException 
     */
     public CodeElementGuideEntry getCodeElementGuideEntry(int entryid) throws DatabaseFetchRuntimeException, IntegrationException{
        if(entryid == 0){
            throw new IntegrationException("cannot get code guide with ID == 0");
        }
        if(getCodeCacheManager().isCachingEnabled()){
            return getCodeCacheManager().getCacheCodeElementGuideEntry().get(entryid, k -> fetchCodeElementGuideEntry(entryid));
        } else {
            return fetchCodeElementGuideEntry(entryid);
        }
        
    }
    
        
    
    
    /**
     * Extracts a single code guide from the DB
     * @param entryid
     * @return
     */
    public CodeElementGuideEntry fetchCodeElementGuideEntry(int entryid) throws DatabaseFetchRuntimeException{
        String query =  """
                        SELECT  guideentryid, category, subcategory, 
                                description, enforcementguidelines, inspectionguidelines, 
                                priority, icon_iconid, createdts, 
                                createdby_umapid, lastupdatedts, lastupdatedby_umapid, 
                                deactivatedts, deactivatedby_umapid
                         	FROM public.codeelementguide WHERE guideentryid = ?;
                        """;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        CodeElementGuideEntry cege = null;
         try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, entryid);
            rs = stmt.executeQuery();
            while(rs.next()){
                cege = generateCodeElementGuideEntry(rs);
            }
             
        } catch (SQLException | BObStatusException | IntegrationException ex) { 
             System.out.println(ex.toString());
             throw new DatabaseFetchRuntimeException("Error retrieving code element guide entry by ID", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
         
         return cege;
    }
    
    /**
     * Retrieves the entire code guide!
     * @return the full code guide for browsing
     * @throws IntegrationException 
     */
    public List<CodeElementGuideEntry> getCodeElementGuideEntries() throws IntegrationException, BObStatusException{
        String query =  """
                        SELECT guideentryid
                          FROM public.codeelementguide WHERE deactivatedts IS NULL;
                        """;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ArrayList<CodeElementGuideEntry> cegelist = new ArrayList();

         try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();
            while(rs.next()){
                cegelist.add(getCodeElementGuideEntry(rs.getInt("guideentryid")));
                System.out.println("CodeIntegrator.getCodeElementGuideEntries | retrieved Entry");
                
            }
             
        } catch (SQLException ex) { 
             System.out.println(ex.toString());
             throw new IntegrationException("Error generating code element guide entry list", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        return cegelist;
    }
    
    /**
     * Generator for guide entries
     * @param rs with all fields on codeelementugide table
     * @return fully baked guide entry
     * @throws SQLException 
     */
    private CodeElementGuideEntry generateCodeElementGuideEntry(ResultSet rs) throws SQLException, IntegrationException, BObStatusException{
        SystemIntegrator si = getSystemIntegrator();
        
        CodeElementGuideEntry cege = new CodeElementGuideEntry();
        cege.setGuideEntryID(rs.getInt("guideentryid"));
        cege.setCategory(rs.getString("category"));
        cege.setSubCategory(rs.getString("subcategory"));
        cege.setDescription(rs.getString("description"));
        cege.setEnforcementGuidelines(rs.getString("enforcementguidelines"));
        cege.setInspectionGuidelines(rs.getString("inspectionguidelines"));
        cege.setPriority(rs.getBoolean("priority"));
        si.populateUMAPTrackedFields(cege, rs);
        return cege;
    }
    
    /**
     * Connects a code element with a code guide entry
     
     * @param element cannot be null. If the CodeGuideEntry in its belly
     * is null, I'll write null to the DB, removing any existing link
     * 
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void linkElementToCodeGuideEntry(CodeElement element) throws IntegrationException, BObStatusException{
        if(element == null){
            throw new BObStatusException("Cannot link element to guide entry with null element");
        }
        
        String query =  "UPDATE codeelement SET guideentryid = ?, lastupdatedts=now(), lastupdatedby_userid=? WHERE elementid = ?;";
        Connection con = null;
        PreparedStatement stmt = null;

         try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            if(element.getGuideEntry() != null){
                stmt.setInt(1, element.getGuideEntry().getGuideEntryID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            if(element.getLastUpdatedBy() != null){
                stmt.setInt(2, element.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            
            stmt.setInt(3, element.getElementID());
            stmt.execute();
             
        } catch (SQLException ex) { 
             System.out.println(ex.toString());
             throw new IntegrationException("Unable to link element id " + element.getElementID() 
                     + " to guide entry. "
                     + "Make sure your guide entry ID exists in the CodeGuide.", ex);
        } finally{
             releasePostgresConnection(con, stmt);
        } 
         
    }

   
} // close class
