/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheClient;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import com.tcvcog.tcvce.entities.BlobLight;
import com.tcvcog.tcvce.entities.BlobType;
import com.tcvcog.tcvce.entities.occupancy.OccChecklistTemplate;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Holds and manages a family of object caches
 * @author Ellen Bascom of Apartment31Y
 */
@Named("blobCacheManager")
@ApplicationScoped
public class BlobCacheManager implements IFaceCacheManager{
    
    @Inject
    private CECaseCacheManager ceCaseCacheManager;
    @Inject
    private OccInspectionCacheManager occInspectionCacheManager;
    
    private boolean cachingEnabled;
    
    private Cache<Integer, BlobLight> cacheBlobLight;
    private Cache<Integer, BlobType> cacheBlobType;
    
    private final List<IFaceCacheClient> cacheClientList;
    
    public BlobCacheManager(){
        cacheClientList = new ArrayList<>();
    }
    
    @PostConstruct
    public void initBean() {
        System.out.println("BlobCacheManager.initBean");
        initCaching();
    }
    
    /**
     * Instantiates all our caches
     */
    @Override
    public void initCaching(){
        // checklists
        cacheBlobLight = Caffeine.newBuilder().maximumSize(2000).softValues().build();
        cacheBlobType = Caffeine.newBuilder().maximumSize(50).softValues().build();
        
    }
    
     @Override
    public void writeCacheStats(){
        
        if(cacheBlobLight != null){
            System.out.println("cacheBlobLight size: " + cacheBlobLight.estimatedSize());
            
        }
        if(cacheBlobType != null){
            System.out.println("cacheBlobType size: " + cacheBlobType.estimatedSize());
        }
    }
    
     @Override
    public void registerCacheClient(IFaceCacheClient client) {
        if(getCacheClientList() != null && !cacheClientList.contains(client)){
            getCacheClientList().add(client);
        }
    }

    @Override
    public void removeCacheClient(IFaceCacheClient client) {
        if(getCacheClientList() != null && getCacheClientList().contains(client)){
            getCacheClientList().remove(client);
        }
        
    }
    
    
    
    /**************************************************************************
    ******************              FLUSHING                *******************
    ***************************************************************************/
    /**
     * 
     * Dumps all my caches
     */
    @Override
    public void flushAllCaches(){
        System.out.println("BlobCacheManager.flushAllCaches");
        if(cacheBlobLight != null){
            cacheBlobLight.invalidateAll();
        }
        if(cacheBlobType != null){
            cacheBlobType.invalidateAll();
        }
        flushUpstreamBlobLightObjects();
    }
    
    /**
     * Flushes all folks who hold blobs since
     * BlobLights don't know about their own parents
     */
    public void flushUpstreamBlobLightObjects(){
        ceCaseCacheManager.flushAllCaches();
        // FIN DH aren't cached
        // but the OISes are
        occInspectionCacheManager.getCacheOccInspectedSpace().invalidateAll();
        // muni DH not cached
        // occ period DHs aren't cached
        // prop DHs aren't cached
        
    }

    @Override
    public void flushObjectFromCache(IFaceCachable cable) {
        if(cable != null){
            flushKey(cable.getCacheKey());
        }
    }
    
    @Override
    public void flushObjectFromCache(int cacheKey) {
        flushKey(cacheKey);
    }
    
    private void flushKey(int key){
        if(cacheBlobLight != null){
            cacheBlobLight.invalidate(key);
        }
        if(cacheBlobType != null){
            cacheBlobType.invalidate(key);
        }
        flushUpstreamBlobLightObjects();
    }
    
    /**
     * object specific flusher
     * @param blight 
     */
    public void flush(BlobLight blight){
        if(blight != null){
            cacheBlobLight.invalidate(blight.getCacheKey());
            
        }
        flushUpstreamBlobLightObjects();
    }
    
    /**
     * Object specific caching
     * @param btype 
     */
    public void flush(BlobType btype){
        if(btype != null){
            cacheBlobType.invalidate(btype.getCacheKey());
        }
        cacheBlobLight.invalidateAll();
        flushUpstreamBlobLightObjects();
    }
    

    /**************************************************************************
    ******************              GETTERS / SETTERS       *******************
    ***************************************************************************/
   
    @Override
    public boolean isCachingEnabled() {
        return cachingEnabled;
    }

    @Override
    public void disableClientCaching() {
        cachingEnabled = false;
        
    }
    
    @Override
    public void enableClientCaching() {
        cachingEnabled = true;
    }

    /**
     * @return the cacheClientList
     */
    public List<IFaceCacheClient> getCacheClientList() {
        return cacheClientList;
    }

    /**
     * @return the cacheBlobLight
     */
    public Cache<Integer, BlobLight> getCacheBlobLight() {
        return cacheBlobLight;
    }

    /**
     * @param cacheBlobLight the cacheBlobLight to set
     */
    public void setCacheBlobLight(Cache<Integer, BlobLight> cacheBlobLight) {
        this.cacheBlobLight = cacheBlobLight;
    }

    /**
     * @return the cacheBlobType
     */
    public Cache<Integer, BlobType> getCacheBlobType() {
        return cacheBlobType;
    }

    /**
     * @param cacheBlobType the cacheBlobType to set
     */
    public void setCacheBlobType(Cache<Integer, BlobType> cacheBlobType) {
        this.cacheBlobType = cacheBlobType;
    }

    

    
    
}
