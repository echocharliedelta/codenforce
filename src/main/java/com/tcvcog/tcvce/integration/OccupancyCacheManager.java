/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheClient;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import com.tcvcog.tcvce.entities.occupancy.OccPeriod;
import com.tcvcog.tcvce.entities.occupancy.OccPeriodType;
import com.tcvcog.tcvce.entities.occupancy.OccPermit;
import com.tcvcog.tcvce.entities.occupancy.OccPermitType;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Holds and manages a family of object caches
 * @author Ellen Bascom of Apartment31Y
 */
@Named("occupancyCacheManager")
@ApplicationScoped
public class OccupancyCacheManager implements IFaceCacheManager{
     
    private boolean cachingEnabled;
    
    private Cache<Integer, OccPeriod> cacheOccPeriod;
    private Cache<Integer, OccPeriodType> cacheOccPeriodType;
    private Cache<Integer, OccPermit> cacheOccPermit;
    private Cache<Integer, OccPermitType> cacheOccPermitType;
      
    
    private final List<IFaceCacheClient> cacheClientList;
    
    public OccupancyCacheManager(){
        cacheClientList = new ArrayList<>();
    }
    
    @PostConstruct
    public void initBean() {
        System.out.println("OccupancyCacheManager.initBean");
        initCaching();
    }
    
    /**
     * Instantiates all our caches
     */
    @Override
    public void initCaching(){
        
        cacheOccPeriod = Caffeine.newBuilder().maximumSize(2000).softValues().build();
        cacheOccPeriodType = Caffeine.newBuilder().maximumSize(200).softValues().build();
        
        cacheOccPermit = Caffeine.newBuilder().maximumSize(3500).recordStats().softValues().build();
        cacheOccPermitType = Caffeine.newBuilder().maximumSize(200).softValues().build();
    
    }   
     /**
     * Asks each cache for its estimated size and writes to SOUT
     */
    @Override
    public void writeCacheStats(){
        if(cacheOccPeriod != null){
            System.out.println("cacheOccPeriod Size: " + cacheOccPeriod.estimatedSize());
//            System.out.println("cacheOccPeriod Size: " + cacheOccPeriod.stats());
        }
        if(cacheOccPeriodType != null){
            System.out.println("cacheOccPeriodType Size: " + cacheOccPeriodType.estimatedSize());
            
        }
        if(cacheOccPermit != null){
            System.out.println("cacheOccPermit Size: " + cacheOccPermit.estimatedSize());
        }
        if(cacheOccPermitType != null){
            System.out.println("cacheOccPermitType Size: " + cacheOccPermitType.estimatedSize());
        }
    }
    
     @Override
    public void registerCacheClient(IFaceCacheClient client) {
        if(getCacheClientList() != null && !cacheClientList.contains(client)){
            getCacheClientList().add(client);
        }
    }

    @Override
    public void removeCacheClient(IFaceCacheClient client) {
        if(getCacheClientList() != null && getCacheClientList().contains(client)){
            getCacheClientList().remove(client);
        }
    }
    
    /**************************************************************************
    ******************              FLUSHING                *******************
    ***************************************************************************/
    
    /**
     * 
     * Dumps all my caches
     */
    @Override
    public void flushAllCaches(){
        System.out.println("OccupancyCacheManager.flushAllCaches");
        if(cacheOccPeriod != null){
            cacheOccPeriod.invalidateAll();
        }
        if(cacheOccPeriodType != null){
            cacheOccPeriodType.invalidateAll();
        }
        if(cacheOccPermit != null){
            cacheOccPermit.invalidateAll();
        }
        if(cacheOccPermitType != null){
            cacheOccPermitType.invalidateAll();
        }
        flushUpstreamOccupancyRelatedObjects();
    }
    
    /**
     * Internal logic for clearing any upstream caches that might become
     * stale after a dump of any complete caches in this class.
     * As of july 2024 implementation this is empty
     */
    private void flushUpstreamOccupancyRelatedObjects(){
        
    }

    @Override
    public void flushObjectFromCache(IFaceCachable cable) {
        if(cable != null){
            flushKey(cable.getCacheKey());
        }
    }
    
    @Override
    public void flushObjectFromCache(int cacheKey) {
        flushKey(cacheKey);
    }
    
    private void flushKey(int key){
        if(cacheOccPeriod != null){
            cacheOccPeriod.invalidate(key);
        }
        if(cacheOccPeriodType != null){
            cacheOccPeriodType.invalidate(key);
        }
        if(cacheOccPermit != null){
            cacheOccPermit.invalidate(key);
        }
        if(cacheOccPermitType != null){
            cacheOccPermitType.invalidateAll();
        }
    }
    
    /**
     * Object specific flusher
     * @param period 
     */
    public void flush(OccPeriod period){
        if(period != null && period.getPeriodID() != 0){
            cacheOccPeriod.invalidate(period.getPeriodID());
        }
    }
    
    /**
     * Object specific flusher
     * @param opt 
     */
    public void flush(OccPeriodType opt){
        if(opt != null){
            cacheOccPeriodType.invalidate(opt.getOccPeriodTypeID());
            cacheOccPeriod.invalidateAll();
        }
    }
    
    /**
     * Object specific flusher
     * @param permit 
     */
    public void flush(OccPermit permit){
        if(permit != null){
            cacheOccPermit.invalidate(permit.getPermitID());
            cacheOccPeriod.invalidate(permit.getPeriodID());
            
        } 
    }
    
    /**
     * Object specific flusher
     * @param permitType 
     */
    public void flush(OccPermitType permitType){
        flushAllCaches();
    }
    
   // GETTERS AMD SETTERS
    @Override
    public boolean isCachingEnabled() {
        return cachingEnabled;
    }

    @Override
    public void disableClientCaching() {
        cachingEnabled = false;
    }

    @Override
    public void enableClientCaching() {
        cachingEnabled = true;
        
    }


    /**
     * @return the cacheClientList
     */
    public List<IFaceCacheClient> getCacheClientList() {
        return cacheClientList;
    }

   

    /**
     * @return the cacheOccPeriod
     */
    public Cache<Integer, OccPeriod> getCacheOccPeriod() {
        return cacheOccPeriod;
    }

    /**
     * @param cacheOccPeriod the cacheOccPeriod to set
     */
    public void setCacheOccPeriod(Cache<Integer, OccPeriod> cacheOccPeriod) {
        this.cacheOccPeriod = cacheOccPeriod;
    }

    /**
     * @return the cacheOccPeriodType
     */
    public Cache<Integer, OccPeriodType> getCacheOccPeriodType() {
        return cacheOccPeriodType;
    }

    /**
     * @param cacheOccPeriodType the cacheOccPeriodType to set
     */
    public void setCacheOccPeriodType(Cache<Integer, OccPeriodType> cacheOccPeriodType) {
        this.cacheOccPeriodType = cacheOccPeriodType;
    }

    /**
     * @return the cacheOccPermit
     */
    public Cache<Integer, OccPermit> getCacheOccPermit() {
        // DEBUGGING
//        System.out.println("OccupancyCacheManager.getCacheOccPermit() | stats: " + cacheOccPermit.stats());
        return cacheOccPermit;
    }

    /**
     * @param cacheOccPermit the cacheOccPermit to set
     */
    public void setCacheOccPermit(Cache<Integer, OccPermit> cacheOccPermit) {
        this.cacheOccPermit = cacheOccPermit;
    }

    /**
     * @return the cacheOccPermitType
     */
    public Cache<Integer, OccPermitType> getCacheOccPermitType() {
        return cacheOccPermitType;
    }

    /**
     * @param cacheOccPermitType the cacheOccPermitType to set
     */
    public void setCacheOccPermitType(Cache<Integer, OccPermitType> cacheOccPermitType) {
        this.cacheOccPermitType = cacheOccPermitType;
    }

    

    
    
}
