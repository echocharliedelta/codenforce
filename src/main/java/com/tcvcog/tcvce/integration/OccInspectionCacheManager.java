/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheClient;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import com.tcvcog.tcvce.entities.occupancy.FieldInspectionLight;
import com.tcvcog.tcvce.entities.occupancy.OccChecklistTemplate;
import com.tcvcog.tcvce.entities.occupancy.OccInspectedSpace;
import com.tcvcog.tcvce.entities.occupancy.OccInspectionCause;
import com.tcvcog.tcvce.entities.occupancy.OccInspectionDetermination;
import com.tcvcog.tcvce.entities.occupancy.OccInspectionDispatch;
import com.tcvcog.tcvce.entities.occupancy.OccLocationDescriptor;
import com.tcvcog.tcvce.entities.occupancy.OccSpaceTypeChecklistified;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Holds and coordinates work with the OccInspection cache subfamily
 * @author pierre15
 */
@Named("occInspectionCacheManager")  // if needed for JSF EL references
@ApplicationScoped
public class OccInspectionCacheManager implements IFaceCacheManager{
    
    private boolean cachingEnabled;
    
    @Inject
    private CECaseCacheManager ceCaseCacheManager;  
    
    /**
     * Handheld related caching switch
     */
    private boolean cachingEnabledHandheld;

    private final static boolean CACHE_HANDHELD_INITIAL_STATE = false;
    
    // checklsits
    private Cache<Integer, OccChecklistTemplate> cacheInspectionChecklist;
    private Cache<Integer, OccSpaceTypeChecklistified> cacheOccSpaceTypeChecklistified;
    
    // inspections primary
    private Cache<Integer, FieldInspectionLight> cacheInspectionLight;  // done
    private Cache<Integer, OccInspectedSpace> cacheOccInspectedSpace;  // done
    private Cache<Integer, OccLocationDescriptor> cacheLocationDescriptor;  // done
    
    // inspection utility
    private Cache<Integer, OccInspectionCause> cacheCause; // done
    private Cache<Integer, OccInspectionDetermination> cacheDetermination; // done
    private Cache<Integer, OccInspectionDispatch> cacheDispatch;  // done
     
    private final List<IFaceCacheClient> cacheClientList;

    @PostConstruct
    public void initBean() {
        System.out.println("OccInspectionCacheManager.initBean");
        initCaching();
    }
    
    /**
     * Instantiates all our caches
     */
    @Override
    public void initCaching(){
        // checklists
        cacheInspectionChecklist = Caffeine.newBuilder().maximumSize(200).softValues().build();
        cacheOccSpaceTypeChecklistified = Caffeine.newBuilder().maximumSize(2400).softValues().build();
        
        // inspections primary
        cacheInspectionLight = Caffeine.newBuilder().maximumSize(3000).softValues().build();
        cacheOccInspectedSpace = Caffeine.newBuilder().maximumSize(12000).softValues().build();
        cacheLocationDescriptor = Caffeine.newBuilder().maximumSize(1000).softValues().build();
        
        // inspections utility
        cacheCause = Caffeine.newBuilder().maximumSize(200).softValues().build();
        cacheDetermination = Caffeine.newBuilder().maximumSize(200).softValues().build();
        cacheDispatch = Caffeine.newBuilder().maximumSize(1000).softValues().build();
        cachingEnabledHandheld = CACHE_HANDHELD_INITIAL_STATE;
        System.out.println("OccInspectionCacheManager.initCaching | handheld caching: " + cachingEnabledHandheld);
    }
    
     
    @Override
    public boolean isCachingEnabled() {
        return cachingEnabled;
    }

    @Override
    public void disableClientCaching() {
        cachingEnabled = false;
        
    }
   

    @Override
    public void enableClientCaching() {
        cachingEnabled = true;
    }

    /**
     * No arg constructor
     */
    public OccInspectionCacheManager(){
        cacheClientList = new ArrayList<>();
    }
    
    
    /**
     * 
     * Display stats
     */
    @Override
    public void writeCacheStats(){
        System.out.println("cacheInspectionChecklist size: " + cacheInspectionChecklist.estimatedSize());
        System.out.println("cacheOccSpaceTypeChecklistified size: " + cacheOccSpaceTypeChecklistified.estimatedSize());
        System.out.println("cacheInspectionLight size: " + cacheInspectionLight.estimatedSize());
        System.out.println("cacheOccInspectedSpace size: " + cacheOccInspectedSpace.estimatedSize());
        System.out.println("cacheLocationDescriptor size: " + cacheLocationDescriptor.estimatedSize());
        System.out.println("cacheCause size: " + cacheCause.estimatedSize());
        System.out.println("cacheDetermination size: " + cacheDetermination.estimatedSize());
        System.out.println("cacheDispatch size: " + cacheDispatch.estimatedSize());
        
    }
    
    
    @Override
    public void registerCacheClient(IFaceCacheClient client) {
        if(getCacheClientList() != null && !cacheClientList.contains(client)){
            getCacheClientList().add(client);
        }
    }

    @Override
    public void removeCacheClient(IFaceCacheClient client) {
        if(getCacheClientList() != null && getCacheClientList().contains(client)){
            getCacheClientList().remove(client);
        }
    }

    
    /**************************************************************************
    ******************              FLUSHING                *******************
    ***************************************************************************/
    /**
     * 
     * Dumps all my caches
     */
    @Override
    public void flushAllCaches(){
        System.out.println("OccInspectionCacheManager.flushAllCaches");
        cacheInspectionChecklist.invalidateAll();
        cacheOccSpaceTypeChecklistified.invalidateAll();
        
        getCacheInspectionLight().invalidateAll();
        getCacheOccInspectedSpace().invalidateAll();
        getCacheLocationDescriptor().invalidateAll();
        
        getCacheCause().invalidateAll();
        getCacheDetermination().invalidateAll();
        getCacheDispatch().invalidateAll();
        
        flushUpstreamCachesFinRelated();
    }
    
    /**
     * Internal organ for flushing any upstream objects which may become stale
     * after a call to this manager's flushAllCaches or similar methods.
     */
    private void flushUpstreamCachesFinRelated(){
        if(ceCaseCacheManager != null){
            System.out.println("OccInspectionCacheManager.flushUpstreamCachesFinRelated | CDI worked to get ceCaseCacheManager");
            // no need to flush any upstreams since they'll ask the 
            // FIN Cache manager which we'll have already flushed
        }
    }
    
    
    @Override
    public void flushObjectFromCache(IFaceCachable cable) {
        if(cable != null){
            flushKey(cable.getCacheKey());
        }
        flushUpstreamCachesFinRelated();
    }
    
     @Override
    public void flushObjectFromCache(int cacheKey) {
         flushKey(cacheKey);
    }
    
    public void flushKey(int key){
        System.out.println("OccInspectionCacheManager.flushKey | key: " + key);
        if(getCacheInspectionChecklist() != null) getCacheInspectionChecklist().invalidate(key);
        if(getCacheOccSpaceTypeChecklistified() != null) getCacheOccSpaceTypeChecklistified().invalidate(key);
        
        if(getCacheInspectionLight() != null) getCacheInspectionLight().invalidate(key);
        if(getCacheOccInspectedSpace() != null) getCacheOccInspectedSpace().invalidate(key);
        if(getCacheLocationDescriptor() != null) getCacheLocationDescriptor().invalidate(key);
        
        if(getCacheCause() != null) getCacheCause().invalidate(key);
        if(getCacheDetermination() != null) getCacheDetermination().invalidate(key);
        if(getCacheDispatch() != null) getCacheDispatch().invalidate(key);
        
    }
    
    /**
     * Object-specific flush
     * @param oct
     */
    public void flush(OccChecklistTemplate oct){
        if(oct != null){
            cacheInspectionChecklist.invalidate(oct.getCacheKey());
        } else {
            cacheInspectionChecklist.invalidateAll();
        }
        cacheOccSpaceTypeChecklistified.invalidateAll();
        
    }
    
    
    /**
     * Object-specific flush
     * @param ostc
     */
    public void flush(OccSpaceTypeChecklistified ostc){
        if(ostc != null){
            cacheOccSpaceTypeChecklistified.invalidate(ostc.getCacheKey());
            cacheInspectionChecklist.invalidate(ostc.getChecklistParentID());
        }
    }
    
    
    /**
     * Object-specific flush
     * @param finLight
     */
    public void flush(FieldInspectionLight finLight){
        if(finLight != null) {
            cacheInspectionLight.invalidate(finLight.getCacheKey());
            // only CECase data heavies are cached, not OccPeriodDHs which hold the FINs
            if(finLight.getCecaseID() != 0){
                ceCaseCacheManager.flushObjectFromCache(finLight.getCecaseID());
            }
        }
    }
    
    
    /**
     * Object-specific flush
     * @param ois
     */
    public void flush(OccInspectedSpace ois){
        if(ois != null) {
            cacheOccInspectedSpace.invalidate(ois.getCacheKey());
        }
        
    }
    
    
    /**
     * Object specific flusher 
     * @param loc 
     */
    public void flush(OccLocationDescriptor loc){
        // anybody that has me in their belly has to go
        if(loc != null) cacheLocationDescriptor.invalidate(loc.getLocationID());
        cacheOccInspectedSpace.invalidateAll();
        cacheInspectionLight.invalidateAll();
    }
    
    
    /**
     * Object-specific flush
     * @param cause
     */
    public void flush(OccInspectionCause cause){
        cacheInspectionLight.invalidateAll();
        if(cause != null) cacheCause.invalidate(cause.getCacheKey());
        
    }
    
    
    /**
     * Object-specific flush
     * @param det
     */
    public void flush(OccInspectionDetermination det){
        cacheInspectionLight.invalidateAll();
        
        if(det != null) cacheDetermination.invalidate(det.getCacheKey());
        
    }
    
    
    /**
     * Object-specific flush
     * @param dispatch
     */
    public void flush(OccInspectionDispatch dispatch){
        if(dispatch != null){
            cacheDispatch.invalidate(dispatch.getCacheKey());
            cacheInspectionLight.invalidate(dispatch.getInspectionID());
        }
    }


    
    
    /**************************************************************************
    ******************              GETTERS AND SETTERS    *******************
    ***************************************************************************/
    
   
    /**
     * @return the cacheInspectionLight
     */
    public Cache<Integer, FieldInspectionLight> getCacheInspectionLight() {
        return cacheInspectionLight;
    }

    /**
     * @return the cacheOccInspectedSpace
     */
    public Cache<Integer, OccInspectedSpace> getCacheOccInspectedSpace() {
        return cacheOccInspectedSpace;
    }

    /**
     * @return the cacheLocationDescriptor
     */
    public Cache<Integer, OccLocationDescriptor> getCacheLocationDescriptor() {
        return cacheLocationDescriptor;
    }

    /**
     * @return the cacheCause
     */
    public Cache<Integer, OccInspectionCause> getCacheCause() {
        return cacheCause;
    }

    /**
     * @return the cacheDetermination
     */
    public Cache<Integer, OccInspectionDetermination> getCacheDetermination() {
        return cacheDetermination;
    }

    /**
     * @return the cacheDispatch
     */
    public Cache<Integer, OccInspectionDispatch> getCacheDispatch() {
        return cacheDispatch;
    }

    /**
     * @return the cacheClientList
     */
    public List<IFaceCacheClient> getCacheClientList() {
        return cacheClientList;
    }

    /**
     * @return the cacheInspectionChecklist
     */
    public Cache<Integer, OccChecklistTemplate> getCacheInspectionChecklist() {
        return cacheInspectionChecklist;
    }

    /**
     * @param cacheInspectionChecklist the cacheInspectionChecklist to set
     */
    public void setCacheInspectionChecklist(Cache<Integer, OccChecklistTemplate> cacheInspectionChecklist) {
        this.cacheInspectionChecklist = cacheInspectionChecklist;
    }

    /**
     * @return the cacheOccSpaceTypeChecklistified
     */
    public Cache<Integer, OccSpaceTypeChecklistified> getCacheOccSpaceTypeChecklistified() {
        return cacheOccSpaceTypeChecklistified;
    }

    /**
     * @param cacheOccSpaceTypeChecklistified the cacheOccSpaceTypeChecklistified to set
     */
    public void setCacheOccSpaceTypeChecklistified(Cache<Integer, OccSpaceTypeChecklistified> cacheOccSpaceTypeChecklistified) {
        this.cacheOccSpaceTypeChecklistified = cacheOccSpaceTypeChecklistified;
    }

    /**
     * @return the cachingEnabledHandheld
     */
    public boolean isCachingEnabledHandheld() {
        return cachingEnabledHandheld;
    }

    /**
     * @param cachingEnabledHandheld the cachingEnabledHandheld to set
     */
    public void setCachingEnabledHandheld(boolean cachingEnabledHandheld) {
        this.cachingEnabledHandheld = cachingEnabledHandheld;
    }

    /**
     * @return the ceCaseCacheManager
     */
    public CECaseCacheManager getCeCaseCacheManager() {
        return ceCaseCacheManager;
    }

    /**
     * @param ceCaseCacheManager the ceCaseCacheManager to set
     */
    public void setCeCaseCacheManager(CECaseCacheManager ceCaseCacheManager) {
        this.ceCaseCacheManager = ceCaseCacheManager;
    }

 
   
    
}
