/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheClient;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import com.tcvcog.tcvce.entities.ContactEmail;
import com.tcvcog.tcvce.entities.ContactPhone;
import com.tcvcog.tcvce.entities.ContactPhoneType;
import com.tcvcog.tcvce.entities.Human;
import com.tcvcog.tcvce.entities.HumanAlias;
import com.tcvcog.tcvce.entities.HumanLink;
import com.tcvcog.tcvce.entities.Person;
import com.tcvcog.tcvce.entities.occupancy.OccChecklistTemplate;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Holds and manages a family of object caches
 * @author Ellen Bascom of Apartment31Y
 */
@Named("personCacheManager")
@ApplicationScoped
public class PersonCacheManager implements IFaceCacheManager{
    
       
    @Inject
    private CECaseCacheManager ceCaseCacheManager;
    
    @Inject
    private OccupancyCacheManager occupancyCacheManager;
    
    @Inject
    private EventCacheManager eventCacheManager;
    
    private boolean cachingEnabled;
    
    private Cache<Integer, Human> cacheHuman;
    
    /**
     * Maybe we just cache persons? All these other objects are in the person
     */
    private Cache<Integer, Person> cachePerson;
    
    private Cache<Integer, HumanAlias> cacheAlias;
    private Cache<Integer, ContactEmail> cacheEmail;
    
    private Cache<Integer, ContactPhone> cachePhone;
    private Cache<Integer, ContactPhoneType> cachePhoneType;
    private Cache<Integer, HumanLink> cacheHumanLink;
    
    private final List<IFaceCacheClient> cacheClientList;
    
    public PersonCacheManager(){
        cacheClientList = new ArrayList<>();
    }
    
    @PostConstruct
    public void initBean() {
        System.out.println("personCacheManager.initBean");
        initCaching();
    }
    
    /**
     * Instantiates all our caches
     */
    @Override
    public void initCaching(){
        
        cacheHuman = Caffeine.newBuilder().maximumSize(2500).softValues().build();
        cachePerson = Caffeine.newBuilder().maximumSize(2500).softValues().build();
        cacheAlias = Caffeine.newBuilder().maximumSize(4500).softValues().build();
        cacheEmail = Caffeine.newBuilder().maximumSize(2000).softValues().build();
        
        cachePhone = Caffeine.newBuilder().maximumSize(2000).softValues().build();
        cachePhoneType = Caffeine.newBuilder().maximumSize(20).softValues().build();
        cacheHumanLink = Caffeine.newBuilder().maximumSize(8000).softValues().build();
   
    }
    
    
    

    @Override
    public void registerCacheClient(IFaceCacheClient client) {
        if(getCacheClientList() != null && !cacheClientList.contains(client)){
            getCacheClientList().add(client);
        }
    }

    @Override
    public void removeCacheClient(IFaceCacheClient client) {
        if(getCacheClientList() != null && getCacheClientList().contains(client)){
            getCacheClientList().remove(client);
        }
    }

    
    /**************************************************************************
    ******************              FLUSHING                *******************
    ***************************************************************************/
    
    
    /**
     * 
     * Dumps all my caches
     */
    @Override
    public void flushAllCaches(){
        System.out.println("personCacheManager.flushAllCaches");
       
        if(cacheHuman != null){
            cacheHuman.invalidateAll();
        }
        if(cachePerson != null){
            cachePerson.invalidateAll();
        }
        if(cacheAlias != null){
            cacheAlias.invalidateAll();
        }
        if(cacheEmail != null){
            cacheEmail.invalidateAll();
        }
        if(cachePhone != null){
            cachePhone.invalidateAll();
        }
        if(cachePhoneType != null){
            cachePhoneType.invalidateAll();
        }
        if(cacheHumanLink != null){
            cacheHumanLink.invalidateAll();
        }
        
    }

    /**
     * Stats printing
     */
    @Override
    public void writeCacheStats(){
       
        if(cacheHuman != null){
            System.out.println("cacheHuman size: " + cacheHuman.estimatedSize());
        }
        if(cachePerson != null){
            System.out.println("cacheperson size: " + cachePerson.estimatedSize());
        }
        if(cacheAlias != null){
            System.out.println("cacheAlias size: " + cacheAlias.estimatedSize());
        }
        if(cacheEmail != null){
            System.out.println("cacheEmail size: " + cacheEmail.estimatedSize());
        }
        if(cachePhone != null){
            System.out.println("cachePhone size: " + cachePhone.estimatedSize());
        }
        if(cachePhoneType != null){
            System.out.println("cachePhoneType size: " + cachePhoneType.estimatedSize());
        }
        if(cacheHumanLink != null){
            System.out.println("cacheHumanLink size: " + cacheHumanLink.estimatedSize());
        }
    }

    @Override
    public void flushObjectFromCache(IFaceCachable cable) {
        if(cable != null){
            flushKey(cable.getCacheKey());
        }
    }
    
    @Override
    public void flushObjectFromCache(int cacheKey) {
        flushKey(cacheKey);
    }
    
    private void flushKey(int key){

        if(cacheHuman != null){
            cacheHuman.invalidate(key);
        }
        if(cachePerson != null){
            cachePerson.invalidate(key);
        }
        if(cacheAlias != null){
            cacheAlias.invalidate(key);
        }
        if(cacheEmail != null){
            cacheEmail.invalidate(key);
        }
        if(cachePhone != null){
            cachePhone.invalidate(key);
        }
        if(cachePhoneType != null){
            cachePhoneType.invalidate(key);
        }
        if(cacheHumanLink != null){
            cacheHumanLink.invalidate(key);
        }
    }
    
    /**
     * Internal logic block for flushing any objects
     * which have human links inside them, which are really
     * PersonLinks
     */
    private void flushHumanHoldingObjects(){
          cacheHumanLink.invalidateAll();
          
          // and now anybody with a human link
          // only property data heavies have Human Links, and 
          // we don't cache those heavy manifestations
         
          // cases and case bits are very humany
          ceCaseCacheManager.flushAllCaches();
          // events hold human links
          eventCacheManager.getCacheEvent().invalidateAll();
          // all the occ stuff is humany 
          occupancyCacheManager.flushAllCaches();
    
    }
    
    /**
     * Internally I'll flush individual cache records in 
     * HumanCache and PersonCache and then I'll call flushHumanHoldingObjects
     * which will dump widely
     * @param humanid 
     */
    private void flushHumanPersonCachesByHumanID(int humanid){
        if(humanid != 0){
            cacheHuman.invalidate(humanid);
            cachePerson.invalidate(humanid);
        }
        flushHumanHoldingObjects();
    }
    
    /**
     * Flush human dependent caches
     * @param hum 
     */
    public void flush(Human hum){
        if(hum != null){
            flushHumanPersonCachesByHumanID(hum.getHumanID());
        }
        
    }
    
    /**
     * Object specific caching
     * @param p 
     */
    public void flush(Person p){
        flush((Human) p);
    }
    

     /**
     * Object specific flush logic
     * @param alias 
     */
    public void flush(HumanAlias alias){
        cacheAlias.invalidate(alias.getCacheKey());
        flushHumanPersonCachesByHumanID(alias.getHumanID());
        
    }
    
    /**
     * Object specific flush logic 
     * @param em 
     */
    public void flush(ContactEmail em){
        if(em != null && em.getEmailID() != 0 && em.getHumanID() != 0){
            cacheEmail.invalidate(em.getEmailID());
            flushHumanPersonCachesByHumanID(em.getHumanID());
        }
        
    }
    
    /**
     * Object specific cache flush
     * @param ph 
     */
    public void flush(ContactPhone ph){
        if(ph != null && ph.getPhoneID() != 0 && ph.getHumanID() != 0){
            cachePhone.invalidate(ph.getCacheKey());
            flushHumanPersonCachesByHumanID(ph.getHumanID());
        }
    }
    
    /**
     * OBject specific cache flush
     * as of July 2024 there's no UI for flushing contact types
     * @param cpt 
     */
    public void flush(ContactPhoneType cpt){
        if(cpt != null){
            cachePhoneType.invalidate(cpt.getPhoneTypeID());
        }
        
    }
    
    /**
     * Object specific cache flush
     * @param hlink 
     */
    public void flush(HumanLink hlink){
        cacheHumanLink.invalidate(hlink.getCacheKey());
        flushHumanPersonCachesByHumanID(hlink.getHumanID());
        
    }
        
     
    /**************************************************************************
     **************        GETTERS & SETTERS                *******************
    ***************************************************************************/
    
    
    @Override
    public boolean isCachingEnabled() {
        return cachingEnabled;
    }

    @Override
    public void disableClientCaching() {
        cachingEnabled = false;
        
    }
    
    @Override
    public void enableClientCaching() {
        cachingEnabled = true;
    }



    /**
     * @return the cacheClientList
     */
    public List<IFaceCacheClient> getCacheClientList() {
        return cacheClientList;
    }

  

    /**
     * @return the cacheHuman
     */
    public Cache<Integer, Human> getCacheHuman() {
        return cacheHuman;
    }

    /**
     * @return the cacheAlias
     */
    public Cache<Integer, HumanAlias> getCacheAlias() {
        return cacheAlias;
    }

    /**
     * @return the cacheEmail
     */
    public Cache<Integer, ContactEmail> getCacheEmail() {
        return cacheEmail;
    }

    /**
     * @return the cachePhone
     */
    public Cache<Integer, ContactPhone> getCachePhone() {
        return cachePhone;
    }

    /**
     * @return the cachePhoneType
     */
    public Cache<Integer, ContactPhoneType> getCachePhoneType() {
        return cachePhoneType;
    }

    /**
     * @return the cacheHumanLink
     */
    public Cache<Integer, HumanLink> getCacheHumanLink() {
        return cacheHumanLink;
    }

    /**
     * @return the cachePerson
     */
    public Cache<Integer, Person> getCachePerson() {
        return cachePerson;
    }

    /**
     * @param cachePerson the cachePerson to set
     */
    public void setCachePerson(Cache<Integer, Person> cachePerson) {
        this.cachePerson = cachePerson;
    }

    

    
    
}
