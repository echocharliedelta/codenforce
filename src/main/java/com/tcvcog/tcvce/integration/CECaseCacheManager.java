/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheClient;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import com.tcvcog.tcvce.entities.CEActionRequest;
import com.tcvcog.tcvce.entities.CECase;
import com.tcvcog.tcvce.entities.CECaseDataHeavy;
import com.tcvcog.tcvce.entities.Citation;
import com.tcvcog.tcvce.entities.CodeViolation;
import com.tcvcog.tcvce.entities.NoticeOfViolation;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Holds and manages a family of object caches
 * @author Ellen Bascom of Apartment31Y
 */
@Named("ceCaseCacheManager")
@ApplicationScoped
public class CECaseCacheManager implements IFaceCacheManager{
    
    private boolean cachingEnabled;
    
    private Cache<Integer, CECase> cacheCECase;
    
    // sub object caching
    private Cache<Integer, NoticeOfViolation> cacheNOV;
    private Cache<Integer, CodeViolation> cacheViolation;
    private Cache<Integer, Citation> cacheCitation;
    private Cache<Integer, CEActionRequest> cacheCEAR;
    
    
    private final List<IFaceCacheClient> cacheClientList;
    
    public CECaseCacheManager(){
        cacheClientList = new ArrayList<>();
    }
    
    @PostConstruct
    public void initBean() {
        System.out.println("CECaseCacheManager.initBean");
        initCaching();
    }
    
    /**
     * Instantiates all our caches
     */
    @Override
    public void initCaching(){
        // checklists
        cacheCECase = Caffeine.newBuilder().maximumSize(800).softValues().build();
     
        setCacheNOV(Caffeine.newBuilder().maximumSize(1800).softValues().build());
        setCacheViolation(Caffeine.newBuilder().maximumSize(1800).softValues().build());
        setCacheCitation(Caffeine.newBuilder().maximumSize(1800).softValues().build());
        setCacheCEAR(Caffeine.newBuilder().maximumSize(1800).softValues().build());
    }
    
    /**
     * Dumps all my caches. I can be called by CaseCoordinator
     * methods which don't necessarily have a handle on a case ID
     * such as a docket entry. So the CC will call me and I'll dump 
     * everybody I have and any related upstream holders of CECases
     */
    @Override
    public void flushAllCaches(){
        System.out.println("CEcaseCacheManager.flushAllCaches");
        if(cacheCECase != null){
            cacheCECase.invalidateAll();
        }
        if(getCacheNOV() != null){
            getCacheNOV().invalidateAll();
        }
        if(getCacheViolation() != null){
            getCacheViolation().invalidateAll();
        }
        if(getCacheCitation() != null){
            getCacheCitation().invalidateAll();
        }
        if(getCacheCEAR() != null){
            getCacheCEAR().invalidateAll();
        }
        flushUpstreamCaseHoldersDuringCaseIDBasedFlush();
    }
    
    /**
     * Writes stats out
     */
    @Override
    public void writeCacheStats(){
        if(cacheCECase != null){
            System.out.println("cacheCECase size: " + cacheCECase.estimatedSize());
            cacheCECase.invalidateAll();
        }
    }
    

    @Override
    public void registerCacheClient(IFaceCacheClient client) {
        if(getCacheClientList() != null && !cacheClientList.contains(client)){
            getCacheClientList().add(client);
        }
    }

    @Override
    public void removeCacheClient(IFaceCacheClient client) {
        if(getCacheClientList() != null && getCacheClientList().contains(client)){
            getCacheClientList().remove(client);
        }
        
    }
    
    /**************************************************************************
    ******************              FLUSHING                *******************
    ***************************************************************************/
    
    
    @Override
    public void flushObjectFromCache(IFaceCachable cable) {
        if(cable != null){
            flushKey(cable.getCacheKey());
        }
    }
    
    @Override
    public void flushObjectFromCache(int cacheKey) {
        flushKey(cacheKey);
    }
    
    private void flushKey(int key){
        if(cacheCECase != null){
            cacheCECase.invalidate(key);
        }
         if(getCacheNOV() != null){
            getCacheNOV().invalidate(key);
        }
        if(getCacheViolation() != null){
            getCacheViolation().invalidate(key);
        }
        if(getCacheCitation() != null){
            getCacheCitation().invalidate(key);
        }
        if(getCacheCEAR() != null){
            getCacheCEAR().invalidate(key);
        }
        flushUpstreamCaseHoldersDuringCaseIDBasedFlush();
    }
    
    /**
     * CaseCordinator is allowed to call a flush by caseID due to
     * complex object composition setups. This method is 
     * the home for any related cache flushes that must happen on a case
     * specific flush. In July 2024 first implementation cases were not composite in
     * any other object caches since PropertyDataHeavy is not cached.
     * 
     */
    private void flushUpstreamCaseHoldersDuringCaseIDBasedFlush(){
        // nothing to do here yet
    }
    
    /**
     * Flush trigger for all CECases
     * @param cse 
     */
    public void flush(CECase cse){
        if(cse != null) flushKey(cse.getCaseID());
    }
    
    /**
     * Object flushing for cecase data heavies
     * @param csedh 
     */
    public void flush(CECaseDataHeavy csedh){
        if(csedh != null) flushKey(csedh.getCaseID());
        
    }
    /**
     * Object specific flush.
     * @param nov 
     */
    public void flush(NoticeOfViolation nov){
        if(nov != null) flushKey(nov.getNoticeID());
    }
   
    /**
     * Object specific flush.
     * @param nov 
     */
    public void flush(CodeViolation cv){
        if(cv != null) flushKey(cv.getViolationID());
    }
    
    /**
     * Object specific flush.
     * @param nov 
     */
    public void flush(Citation cit){
        if(cit != null) flushKey(cit.getCitationID());
    }
    
    /**
     * Object specific flush.
     * @param nov 
     */
    public void flush(CEActionRequest cear){
        if(cear != null) flushKey(cear.getRequestID());
    }
    
    // now I need to hunt down all the locations where we insert, update, or deac
    // ANY object inside a cecase or case data heavy!!
    
    /**************************************************************************
    ******************        GETTERS AND SETTERS           *******************
    ***************************************************************************/
    
   
    @Override
    public boolean isCachingEnabled() {
        return cachingEnabled;
    }

    @Override
    public void disableClientCaching() {
        cachingEnabled = false;
    }
    
   

    @Override
    public void enableClientCaching() {
        cachingEnabled = true;
        
    }



    /**
     * @return the cacheClientList
     */
    public List<IFaceCacheClient> getCacheClientList() {
        return cacheClientList;
    }

   

    /**
     * @return the cacheCECase
     */
    public Cache<Integer, CECase> getCacheCECase() {
        return cacheCECase;
    }

    /**
     * @param cacheCECase the cacheCECase to set
     */
    public void setCacheCECase(Cache<Integer, CECase> cacheCECase) {
        this.cacheCECase = cacheCECase;
    }

    

    /**
     * @return the cacheNOV
     */
    public Cache<Integer, NoticeOfViolation> getCacheNOV() {
        return cacheNOV;
    }

    /**
     * @param cacheNOV the cacheNOV to set
     */
    public void setCacheNOV(Cache<Integer, NoticeOfViolation> cacheNOV) {
        this.cacheNOV = cacheNOV;
    }

    /**
     * @return the cacheViolation
     */
    public Cache<Integer, CodeViolation> getCacheViolation() {
        return cacheViolation;
    }

    /**
     * @param cacheViolation the cacheViolation to set
     */
    public void setCacheViolation(Cache<Integer, CodeViolation> cacheViolation) {
        this.cacheViolation = cacheViolation;
    }

    /**
     * @return the cacheCitation
     */
    public Cache<Integer, Citation> getCacheCitation() {
        return cacheCitation;
    }

    /**
     * @param cacheCitation the cacheCitation to set
     */
    public void setCacheCitation(Cache<Integer, Citation> cacheCitation) {
        this.cacheCitation = cacheCitation;
    }

    /**
     * @return the cacheCEAR
     */
    public Cache<Integer, CEActionRequest> getCacheCEAR() {
        return cacheCEAR;
    }

    /**
     * @param cacheCEAR the cacheCEAR to set
     */
    public void setCacheCEAR(Cache<Integer, CEActionRequest> cacheCEAR) {
        this.cacheCEAR = cacheCEAR;
    }

    

    
    
}
