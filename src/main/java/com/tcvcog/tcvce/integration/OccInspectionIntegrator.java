/*
 * Copyright (C) 2018 Turtle Creek Valley
 * Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheClient;
import com.tcvcog.tcvce.coordinators.BlobCoordinator;
import com.tcvcog.tcvce.coordinators.EventCoordinator;
import com.tcvcog.tcvce.coordinators.MunicipalityCoordinator;
import com.tcvcog.tcvce.coordinators.OccInspectionCoordinator;
import com.tcvcog.tcvce.coordinators.OccupancyCoordinator;
import com.tcvcog.tcvce.coordinators.PersonCoordinator;
import com.tcvcog.tcvce.coordinators.SearchCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.coordinators.UserCoordinator;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.DatabaseFetchRuntimeException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.IFace_inspectable;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.occupancy.*;
import com.tcvcog.tcvce.entities.search.SearchParamsCodeViolation;
import com.tcvcog.tcvce.entities.search.SearchParamsFieldInspection;
import com.tcvcog.tcvce.integration.*;
import com.tcvcog.tcvce.util.Constants;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import jakarta.annotation.PostConstruct;

/**
 * The master Inspection integrator for methods 
 * used during the actual inspection process.
 * See the ChecklistIntegrator for methods used to configure a checklist
 * 
 * @author ellen bascomb of apt 31y
 */
public class        OccInspectionIntegrator 
        extends     BackingBeanUtils 
        implements  Serializable, IFaceCacheClient {

    
    /**
     * Creates a new instance of ChecklistIntegrator
     */
    public OccInspectionIntegrator() {
        
    }
    
     @PostConstruct
    public void initBean(){
        registerClientWithManager();
    
    }
    
    
    
    @Override
    public void registerClientWithManager() {
        getOccInspectionCacheManager().registerCacheClient(this);
    }
    


    /**
     * Called by the OccupancyIntegrator during the construction of an
 FieldInspection object. This method, in turn, calls the private
     * getInspectedSpaceList method in this class to populate the actual
     * inspection data.
     *
     * @param inspectionID
     * @return a fully-baked ImplementedChecklist
     * @throws IntegrationException
     */
    public List<OccInspectedSpace> getInspectedSpaceList(int inspectionID) throws IntegrationException {
        List<OccInspectedSpace> inspSpaceList = new ArrayList<>();
        String query = """
                SELECT inspectedspaceid FROM public.occinspectedspace 
                WHERE deactivatedts IS NULL
                AND occinspection_inspectionid=?;
                """;

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {

            // this gets us a list of all of the spaces that have been inspected for this
            // occupancy inspection
            stmt = con.prepareStatement(query);
            stmt.setInt(1, inspectionID);
            rs = stmt.executeQuery();

            // chugg down the list of spaceids and fetch an Inspected space for each
            while (rs.next()) {
                inspSpaceList.add(getOccInspectedSpace(rs.getInt("inspectedspaceid")));
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to generate inspected space list, sorry!", ex);

        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 

        // temp to close
        return inspSpaceList;
    }
   

    /**
     * Extracts an inspected space from DB and injects elements
     * @param inspectedspaceID
     * @return     
     */
    public OccInspectedSpace getOccInspectedSpace(int inspectedspaceID) throws DatabaseFetchRuntimeException {
        if(getOccInspectionCacheManager().isCachingEnabled()){
            return getOccInspectionCacheManager().getCacheOccInspectedSpace().get(inspectedspaceID, k -> fetchOccInspectedSpace(inspectedspaceID));
        } else {
            return fetchOccInspectedSpace(inspectedspaceID);
        }    
        
    }
    
      /**
     * Extracts an inspected space from DB and injects elements
     * @param inspectedspaceID
     * @return
     */
    public OccInspectedSpace fetchOccInspectedSpace(int inspectedspaceID) throws DatabaseFetchRuntimeException {
        OccInspectedSpace inspectedSpace = null;
        if (inspectedspaceID == 0) {
            return inspectedSpace;
        }
        String querySpace = """
                            SELECT  inspectedspaceid, occinspection_inspectionid, occlocationdescription_descid, 
                                    addedtochecklistby_userid, addedtochecklistts, occchecklistspacetype_chklstspctypid,
                                    deactivatedby_userid, deactivatedts, 
                                    lengthin, widthin,  
                                    lengthin2, widthin2, 
                                    ceilingheightin, maxspaceoccupancypersons, 
                                    isbedroom, islivingroom, iskitchen, isbathroom, 
                                    egresscount, notes, maxsleepingpersons, 
                                    isfinishedspace, floorno, spacelocation, lengthft, widthft, lengthft2, widthft2
                              FROM public.occinspectedspace WHERE inspectedspaceid=?;
                            """;
        
        Connection con = null;
        
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(querySpace);
            stmt.setInt(1, inspectedspaceID);
            rs = stmt.executeQuery();

            // create our subclass OccInspectedSpace by passing in the superclass
            // OccSpace, which we make right away
            while (rs.next()) {
                inspectedSpace = generateOccInspectedSpace(rs);
            }
        } catch (SQLException | IntegrationException ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("Unable to generate an inspected space, sorry!", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return inspectedSpace;
    }
    
 
    
  
    /**
     * Extracts elements by ois
     * @param ois
     * @return
     * @throws IntegrationException 
     */
    public List<OccInspectedSpaceElement> getInspectedSpaceElements(OccInspectedSpace ois) throws IntegrationException {
        if (ois == null || ois.getInspectedSpaceID() == 0) {
            throw new IntegrationException("Cannot get occ inspected space element list with null space or OISid of zero");
        }
        OccInspectionCoordinator oic  = getOccInspectionCoordinator();
        String queryElements
                = """
                  SELECT occinspectedspaceelement.inspectedspaceelementid
                       FROM occinspectedspaceelement INNER JOIN occinspectedspace ON (occinspectedspaceelement.inspectedspace_inspectedspaceid = occinspectedspace.inspectedspaceid)
                       WHERE occinspectedspace.inspectedspaceid=?;
                  """;
        Connection con = null;
        
        ResultSet rs = null;
        PreparedStatement stmt = null;
        List<OccInspectedSpaceElement> inspectedEleList = new ArrayList<>();
        try{
            con = getPostgresCon();
            stmt = con.prepareStatement(queryElements);
            
                stmt.setInt(1, ois.getInspectedSpaceID());
                rs = stmt.executeQuery();
                while (rs.next()) {
                    inspectedEleList.add(oic.getOccInspectedSpaceElement(rs.getInt("inspectedspaceelementid")));
                }
        } catch (SQLException | IntegrationException | BObStatusException  ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("Unable to generate an inspected space, sorry!", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return inspectedEleList;
    }
    
    
    

    /**
     * GEnerator for OccInspectedSpace
     * @param rs
     * @return
     * @throws SQLException
     * @throws IntegrationException 
     */
    private OccInspectedSpace generateOccInspectedSpace(ResultSet rs) throws SQLException, IntegrationException {
        UserCoordinator uc = getUserCoordinator();
        OccChecklistIntegrator oci = getOccChecklistIntegrator();
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        OccInspectedSpace inSpace = null;
        try {
            
            inSpace = new OccInspectedSpace();
            inSpace.setInspectedSpaceID(rs.getInt("inspectedspaceid"));
            if(rs.getInt("occlocationdescription_descid") != 0){
                inSpace.setLocation(oic.getOccLocationDescriptor(rs.getInt("occlocationdescription_descid")));
            }
            
            inSpace.setType(oci.getOccSpaceTypeChecklistified(rs.getInt("occchecklistspacetype_chklstspctypid")));
            
            inSpace.setAddedToChecklistBy(uc.user_getUser(rs.getInt("addedtochecklistby_userid")));
            inSpace.setInspectionID(rs.getInt("occinspection_inspectionid"));
            
            if (rs.getTimestamp("addedtochecklistts") != null) {
                inSpace.setAddedToChecklistTS(rs.getTimestamp("addedtochecklistts").toLocalDateTime());
            }
            
            if(rs.getTimestamp("deactivatedts") != null){
                 inSpace.setDeactivatedTS(rs.getTimestamp("deactivatedts").toLocalDateTime());
            }
            if(rs.getInt("deactivatedby_userid") != 0){
                inSpace.setDeactivatedBy(uc.user_getUser(rs.getInt("deactivatedby_userid")));
            }
            
            // now for space attributes
           
            inSpace.setFloorNumber(rs.getInt("floorno"));
            inSpace.setLocationString(rs.getString("spacelocation"));
            
            inSpace.setDim1LengthFeet((int) rs.getFloat("lengthft"));
            inSpace.setDim1WidthFeet((int) rs.getFloat("widthft"));
            
            inSpace.setDim2LengthFeet((int) rs.getFloat("lengthft2"));
            inSpace.setDim2WidthFeet((int) rs.getFloat("widthft2"));
            
            inSpace.setDim1LengthInches((int) rs.getFloat("lengthin"));
            inSpace.setDim1WidthInches((int) rs.getFloat("widthin"));
            
            inSpace.setDim2LengthInches((int) rs.getFloat("lengthin2"));
            inSpace.setDim2WidthInches((int) rs.getFloat("widthin2"));
            
            inSpace.setDimCeilingHeightInches((int) rs.getFloat("ceilingheightin"));
            
            inSpace.setMaxOccPersons(rs.getInt("maxspaceoccupancypersons"));
            inSpace.setMaxSleepingPersons(rs.getInt("maxsleepingpersons"));
            inSpace.setEgressCount(rs.getInt("egresscount"));
            
            inSpace.setBedroom(rs.getBoolean("isbedroom"));
            inSpace.setLiving(rs.getBoolean("islivingroom"));
            inSpace.setKitchen(rs.getBoolean("iskitchen"));
            inSpace.setBathroom(rs.getBoolean("isbathroom"));
            inSpace.setFinishedSpace(rs.getBoolean("isfinishedspace"));
            
            inSpace.setNotes(rs.getString("notes"));
            
            // now populate the space elements
            inSpace.setInspectedElementList(getInspectedSpaceElements(inSpace));
            
        } catch (BObStatusException ex) {
            System.out.println(ex);
        }
        return inSpace;

    }

    /**
     * Extracts an inspected element and all its meta data from the DB
     * @param eleID
     * @return
     * @throws IntegrationException 
     */
    public OccInspectedSpaceElement getInspectedSpaceElement(int eleID) throws IntegrationException {
        String query_spaceIDs = """
                                SELECT inspectedspaceelementid, occinspectedspaceelement.notes AS oisenotes, locationdescription_id, lastinspectedby_userid, 
                                    lastinspectedts, compliancegrantedby_userid, compliancegrantedts, 
                                    inspectedspace_inspectedspaceid, overriderequiredflagnotinspected_userid, 
                                    occchecklistspacetypeelement_elementid, failureseverity_intensityclassid, migratetocecaseonfail, occinspection_inspectionid, occchecklistspacetypeelement.codesetelement_seteleid, transferredts, transferredby_userid, transferredtocecase_caseid  
                                FROM public.occinspectedspaceelement 
                                    INNER JOIN public.occchecklistspacetypeelement ON (occchecklistspacetypeelement_elementid = spaceelementid) 
                                    INNER JOIN public.occinspectedspace on (occinspectedspace.inspectedspaceid = occinspectedspaceelement.inspectedspace_inspectedspaceid) 
                                WHERE inspectedspaceelementid=?;
                                """;


        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        OccInspectedSpaceElement ele = null;
        try {
            // this gets us a list of all of the spaces that have been inspected for this
            // occupancy inspection
            stmt = con.prepareStatement(query_spaceIDs);
            stmt.setInt(1, eleID);
            rs = stmt.executeQuery();
            // chugg down the list of spaceids and fetch an Inspected space for each
            while (rs.next()) {
                ele = generateInspectedSpaceElement(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("getInspectedSpaceElement | Unable to get inspected space, sorry!", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return ele;
    }

    /**
     * GEnerator for inspected space elements
     * @param rs
     * @return
     * @throws SQLException
     * @throws IntegrationException 
     */
    private OccInspectedSpaceElement generateInspectedSpaceElement(ResultSet rs) throws SQLException, IntegrationException {
            CodeIntegrator ci = getCodeIntegrator();
            UserCoordinator uc = getUserCoordinator();
            CaseIntegrator cseint = getCaseIntegrator();
            BlobCoordinator bc = getBlobCoordinator();
            OccInspectionCoordinator oic = getOccInspectionCoordinator();
            SystemCoordinator sc = getSystemCoordinator();
            OccInspectedSpaceElement inspectedEle = null;
        try {
            
            inspectedEle = new OccInspectedSpaceElement(ci.getEnforcableCodeElement(rs.getInt("codesetelement_seteleid")));
            
            inspectedEle.setInspectedSpaceElementID(rs.getInt("inspectedspaceelementid"));
            inspectedEle.setOccChecklistSpaceTypeElementID(rs.getInt("occchecklistspacetypeelement_elementid"));
            
            inspectedEle.setInspectionNotes(rs.getString("oisenotes"));
            inspectedEle.setLocation(oic.getOccLocationDescriptor(rs.getInt("locationdescription_id")));
            inspectedEle.setLastInspectedBy(uc.user_getUser(rs.getInt("lastinspectedby_userid")));
            
            if (rs.getTimestamp("lastinspectedts") != null) {
                inspectedEle.setLastInspectedTS(rs.getTimestamp("lastinspectedts").toLocalDateTime());
            }
            inspectedEle.setComplianceGrantedBy(uc.user_getUser(rs.getInt("compliancegrantedby_userid")));
            if (rs.getTimestamp("compliancegrantedts") != null) {
                inspectedEle.setComplianceGrantedTS(rs.getTimestamp("compliancegrantedts").toLocalDateTime());
            }
            inspectedEle.setInspectedSpaceID(rs.getInt("inspectedspace_inspectedspaceid"));
            inspectedEle.setOverrideRequiredFlag_thisElementNotInspectedBy(uc.user_getUser(rs.getInt("overriderequiredflagnotinspected_userid")));
            
            inspectedEle.setFaillureSeverity(sc.getIntensityClass(rs.getInt("failureseverity_intensityclassid")));
            
            inspectedEle.setMigrateToCaseOnFail(rs.getBoolean("migratetocecaseonfail"));
            
            inspectedEle.setBlobList(bc.getBlobLightList(inspectedEle));
            inspectedEle.setOccInspectionID(rs.getInt("occinspection_inspectionid"));
            cseint.populateTransferrableFields(inspectedEle, rs);
            
        } catch(BObStatusException | BlobException ex){
            System.out.println(ex);
        }
            return inspectedEle;
    }

    /**
     * Creates a new record in the occlocationdescriptor table
     * @param locDesc
     * @return
     * @throws IntegrationException 
     */
    public int insertLocationDescriptor(OccLocationDescriptor locDesc) throws IntegrationException {
        String sql = """
                     INSERT INTO public.occlocationdescriptor(
                                 locationdescriptionid, description, muni_municode)
                         VALUES (DEFAULT, ?, ?);""";

        Connection con = null;
        ResultSet rs = null;
        PreparedStatement stmt = null;
        int insertedLocDescID = 0;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(sql);
            stmt.setString(1, locDesc.getLocationDescription());
            if(locDesc.getMuni() != null){
                stmt.setInt(2, locDesc.getMuni().getMuniCode());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            
            stmt.executeUpdate();

            String retrievalQuery = "SELECT currval('locationdescription_id_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();

            while (rs.next()) {
                insertedLocDescID = rs.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to generate location description, sorry!", ex);

        } finally {
            releasePostgresConnection(con, stmt, rs);
        }
        return insertedLocDescID;
    }

    /**
     * TODO: Finish me
     * @param inspection
     * @return 
     */
    public List<OccLocationDescriptor> getLocationDescriptorsByInspection(FieldInspection inspection) {
        //

        return new ArrayList();
    }
    
    /**
     * Cache backed getter for location descriptors
     * @param descriptorID
     * @return
     * @throws IntegrationException 
     */
    public OccLocationDescriptor getLocationDescriptor(int descriptorID) throws IntegrationException {
        if(descriptorID == 0){
            throw new IntegrationException("Cannot get loation descriptor of ID == 0");
        }
        if(getOccInspectionCacheManager().isCachingEnabled()){
            return getOccInspectionCacheManager().getCacheLocationDescriptor().get(descriptorID, k -> fetchLocationDescriptor(descriptorID));
        } else {
            return fetchLocationDescriptor(descriptorID);
        }
        
    }

    /**
     * Extracts a location descriptor from the DB
     * @param descriptorID
     * @return     
     */
    public OccLocationDescriptor fetchLocationDescriptor(int descriptorID) throws DatabaseFetchRuntimeException {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        MunicipalityCoordinator mc = getMuniCoordinator();
        
        String queryLocation = """
                                SELECT locationdescriptionid, description, muni_municode
                                  FROM public.occlocationdescriptor WHERE locationdescriptionid = ?;
                                """;

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        OccLocationDescriptor loc = null;
        try {
            stmt = con.prepareStatement(queryLocation);
            stmt.setInt(1, descriptorID);
            rs = stmt.executeQuery();

            while (rs.next()) {
                loc = oic.getOccLocationDescriptorSkeleton();
                loc.setLocationID(rs.getInt("locationdescriptionid"));
                loc.setLocationDescription(rs.getString("description"));
                if(rs.getInt("muni_municode") != 0){
                    loc.setMuni(mc.getMuni(rs.getInt("muni_municode")));
                }
            }
        } catch (SQLException | BObStatusException | IntegrationException ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("Unable to get location description, sorry!", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        

        return loc;
    }
    
    /**
     * Extracts all active loc descriptors by muni
     * @param muni
     * @return
     * @throws IntegrationException 
     */
    public List<OccLocationDescriptor> getLocationDescriptorsByMuni(Municipality muni) throws IntegrationException {
        if(muni == null){
            throw new IntegrationException("cannot get loc descriptors by muni with null muni");
        }
        String query_spaceIDs = """
                                SELECT locationdescriptionid
                                  FROM public.occlocationdescriptor WHERE muni_municode=? AND deactivatedts IS NULL?;
                                """;

        
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        List<OccLocationDescriptor> locList = new ArrayList<>();
        try {
        
            stmt = con.prepareStatement(query_spaceIDs);
            stmt.setInt(1, muni.getMuniCode());
            rs = stmt.executeQuery();

            // chugg down the list of spaceids and fetch an Inspected space for each
            while (rs.next()) {
                locList.add(getLocationDescriptor(rs.getInt("locationdescriptionid")));
            }
        } catch (SQLException  ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to get location description, sorry!", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 

        return locList;
    }

    /**
     * Updates a record in the occlocationdescriptor table
     * @param locDesc
     * @throws IntegrationException 
     */
    public void updateLocationDescriptor(OccLocationDescriptor locDesc) throws IntegrationException {
        String sql = """
                     UPDATE public.occlocationdescriptor
                        SET description=?, muni_municode=?
                      WHERE locationdescriptionid=?;""";

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {

            stmt = con.prepareStatement(sql);

            stmt.setString(1, locDesc.getLocationDescription());
             if(locDesc.getMuni() != null){
                stmt.setInt(2, locDesc.getMuni().getMuniCode());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            stmt.setInt(3, locDesc.getLocationID());
             
            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to generate location description, sorry!", ex);

        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        
    }

    /**
     * Creates a record in the occinspectedspace table which connects a space type 
     * with an inspeection and records who added it and when. NOTE that I don't
     * write individual element data; use my siblings for that instead
     * 
     * @param spc
     * @param inspection
     * @return
     * @throws IntegrationException 
     */
    public OccInspectedSpace recordCommencementOfSpaceInspection(OccInspectedSpace spc, FieldInspection inspection)
            throws IntegrationException {

        String sql =    """
                        INSERT INTO public.occinspectedspace(
                                    inspectedspaceid, occinspection_inspectionid, occlocationdescription_descid, 
                                    addedtochecklistby_userid, addedtochecklistts, occchecklistspacetype_chklstspctypid)
                            VALUES (DEFAULT, ?, ?, 
                                    ?, now(), ?);
                        """;

        Connection con = null;
        ResultSet rs = null;
        PreparedStatement stmt = null;
        int insertedInspSpaceID = 0;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(sql);
            
            stmt.setInt(1, inspection.getInspectionID());
            
            if (spc.getLocation() != null) {
                stmt.setInt(2, spc.getLocation().getLocationID());
            } else {
                stmt.setInt(2, Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                        .getString("locationdescriptor_implyfromspacename")));
            }
            
            if(spc.getAddedToChecklistBy() != null){
                stmt.setInt(3, spc.getAddedToChecklistBy().getUserID());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            
            if(spc.getType() != null){
                stmt.setInt(4, spc.getType().getChecklistSpaceTypeID()); 
            } else {
                throw new IntegrationException("Cannot inspect space with null type!");
            }
            
            stmt.execute();

            String retrievalQuery = "SELECT currval('occinspectedspace_pk_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();

            while (rs.next()) {
                insertedInspSpaceID = rs.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to insert newly inspected space, sorry!", ex);

        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        spc.setInspectedSpaceID(insertedInspSpaceID);

        return spc;

    }

    /**
     * Inserts an OccInspectedSpace to the inspectedchecklistspaceelement table
     * which has not previously been inspected. The OccInspectedSpace object and
     * its composed elements do not have an ID number generated from the DB yet,
     * and these will come from the DB sequences ;
     *
     *
     * @param inspection the current FieldInspection
     * @param inspSpace an OccInspectedSpace that was NOT retrieved from the DB
     * @return the number of newly inserted space elements (mostly for info value only)
 passed as the second input parameter having been written to DB and added
 to the FieldInspection's internal List of inspected elements.
     * @throws IntegrationException
     */
    public int recordInspectionOfSpaceElements(OccInspectedSpace inspSpace, FieldInspection inspection) throws IntegrationException {
        int spaceElementInserts = 0;
        Iterator<OccInspectedSpaceElement> inspectedElementListIterator = inspSpace.getInspectedElementList().iterator();
        while (inspectedElementListIterator.hasNext()) {
            OccInspectedSpaceElement oie = inspectedElementListIterator.next();
            if (oie.getInspectedSpaceElementID() != 0) {
                getOccInspectionCoordinator().updateInspectedSpaceElement(oie);
            } else {
                spaceElementInserts++;
                insertInspectedSpaceElement(oie, inspSpace);
            }
        }
        return spaceElementInserts;
    }
    
    /**
     * Massive creator of records in the all-important occinspectedspaceelement
     * table 
     * @param inspElement
     * @param inSpace
     * @throws IntegrationException 
     */
    private int insertInspectedSpaceElement(OccInspectedSpaceElement inspElement, OccInspectedSpace inSpace) throws IntegrationException{
        
        
        String sql =     "INSERT INTO public.occinspectedspaceelement(\n" +
                        "            inspectedspaceelementid, notes, locationdescription_id, lastinspectedby_userid, \n" +
                        "            lastinspectedts, compliancegrantedby_userid, compliancegrantedts, \n" +
                        "            inspectedspace_inspectedspaceid, overriderequiredflagnotinspected_userid, \n" +
                        "            occchecklistspacetypeelement_elementid, failureseverity_intensityclassid, \n" +
                        "            migratetocecaseonfail)\n" +
                        "    VALUES (DEFAULT, ?, ?, ?, \n" +
                        "            ?, ?, ?, \n" +
                        "            ?, ?, \n" +
                        "            ?, ?, \n" +
                        "            ?);";
        
        // single row formatting of query_icse
        // SELECT DISTINCT space_id FROM checklistspaceelement INNER JOIN spaceelement ON (spaceelement_id = spaceelementid) WHERE checklist_id = 1;
        
        if(inspElement == null || inspElement.getInspectedSpaceElementID() != 0){
            throw new IntegrationException("Cannot insert inspected element with nonzero ID or null");
        }
        
        int newlyInspectedSpaceElement = 0;
        Connection con = null;
        
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            con = getPostgresCon();
            // we have the parts we need for inserting into the inspectedchecklistspaceelement
            // for each inspected element, build and execute and insert
            stmt = con.prepareStatement(sql);

            stmt.setString(1, inspElement.getInspectionNotes());
            int locID;
            if (inSpace.getLocation() != null) {
                // apparently we're allowing the spaces to march in here with a
                // location descriptor that hasn't been written to DB yet...
                if (inSpace.getLocation().getLocationID() == 0) {
                    locID = insertLocationDescriptor(inSpace.getLocation());
                } else {
                    locID = inSpace.getLocation().getLocationID();
                }
            } else {
                locID = Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                        .getString("locationdescriptor_implyfromspacename"));
            }
            stmt.setInt(2, locID);
            if(inspElement.getLastInspectedBy() != null
                    && inspElement.getLastInspectedTS() != null){
                stmt.setInt(3, inspElement.getLastInspectedBy().getUserID());
                stmt.setTimestamp(4, java.sql.Timestamp.valueOf(inspElement.getLastInspectedTS()));
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
                stmt.setNull(4, java.sql.Types.NULL);
            }

            if(inspElement.getComplianceGrantedBy() != null 
                    && inspElement.getComplianceGrantedTS() != null){
                stmt.setInt(5, inspElement.getComplianceGrantedBy().getUserID());
                stmt.setTimestamp(6, java.sql.Timestamp.valueOf(inspElement.getComplianceGrantedTS()));
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
                stmt.setNull(6, java.sql.Types.NULL);
            }

            stmt.setInt(7, inSpace.getInspectedSpaceID());

            if(inspElement.getOverrideRequiredFlag_thisElementNotInspectedBy() != null){
                stmt.setInt(8, inspElement.getOverrideRequiredFlag_thisElementNotInspectedBy().getUserID());
            } else {
                stmt.setNull(8, java.sql.Types.NULL);
            }
            
            stmt.setInt(9, inspElement.getOccChecklistSpaceTypeElementID());

            if(inspElement.getFaillureSeverity()!= null){
                stmt.setInt(10, inspElement.getFaillureSeverity().getClassID());
            } else {
                stmt.setNull(10,java.sql.Types.NULL);
            }
            stmt.setBoolean(11, inspElement.isMigrateToCaseOnFail());

            stmt.execute();
            
            String retrievalQuery = "SELECT currval('inspectedspacetypeelement_inspectedstelid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();

            while (rs.next()) {
                newlyInspectedSpaceElement = rs.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to insert newly inspected space into DB, sorry!", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return newlyInspectedSpaceElement;
    }

    /**
     * Updates a single record in the occinspectedspaceelement table
     * @param inspElement with members ready for extraction and insertion into DB
     * @throws IntegrationException 
     */
    public void updateInspectedSpaceElement(OccInspectedSpaceElement inspElement) throws IntegrationException {
        String sql = """
                     UPDATE public.occinspectedspaceelement
                        SET notes=?, lastinspectedby_userid=?, lastinspectedts=?, compliancegrantedby_userid=?, 
                            compliancegrantedts=?, failureseverity_intensityclassid=?, migratetocecaseonfail=? 
                      WHERE inspectedspaceelementid=?;
                     """;

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);

            stmt.setString(1, inspElement.getInspectionNotes());

            if (inspElement.getLastInspectedBy() != null) {
                stmt.setInt(2, inspElement.getLastInspectedBy().getUserID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }

            if (inspElement.getLastInspectedTS() != null) {
                stmt.setTimestamp(3, java.sql.Timestamp.valueOf(inspElement.getLastInspectedTS()));
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            if (inspElement.getComplianceGrantedBy() != null) {
                stmt.setInt(4, inspElement.getComplianceGrantedBy().getUserID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }

            if (inspElement.getComplianceGrantedTS() != null) {
                stmt.setTimestamp(5, java.sql.Timestamp.valueOf(inspElement.getComplianceGrantedTS()));
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            
            if(inspElement.getFaillureSeverity()!= null){
                stmt.setInt(6, inspElement.getFaillureSeverity().getClassID());
            } else {
                stmt.setNull(6,java.sql.Types.NULL);
            }
            
            stmt.setBoolean(7, inspElement.isMigrateToCaseOnFail());

            stmt.setInt(8, inspElement.getInspectedSpaceElementID());

            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update inspected space element in the database, sorry!", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        
    }

    /**
     * For updating values on InspectedSpaces which have previously been
     * committed with recordInspectionOfSpaceElements.
     * Greatly expanded in July 2024 to include a bunch of space attributes.
     *
     * @param inspection
     * @param ois
     * @throws IntegrationException thrown for standard SQL errors and this
     * method being given an OccInspectedSpace object whose constituent members
     * lack ID numbers for the updates.
     */
    public void updateInspectedSpace(FieldInspection inspection, OccInspectedSpace ois) throws IntegrationException {

        String query = """
                UPDATE public.occinspectedspace
                    SET occchecklistspacetype_chklstspctypid=?,
                        floorno=?,
                        lengthin=?, widthin=?, 
                        lengthin2=?, widthin2=?, 
                        ceilingheightin=?, 
                        maxspaceoccupancypersons=?, maxsleepingpersons=?,
                        isbedroom=?, islivingroom=?, iskitchen=?, isbathroom=?, isfinishedspace=?, 
                        egresscount=?, notes=?, spacelocation=?,
                        lengthft=?, widthft=?, lengthft2=?, widthft2=?
                       
                WHERE inspectedspaceid=?;
                """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);

            if (Objects.nonNull(ois.getType())) {
                stmt.setInt(1, ois.getType().getChecklistSpaceTypeID());
            } else {
                throw new IntegrationException("Cannot update inspected space with null type!");
            }

            stmt.setInt(2, ois.getFloorNumber());
            
            stmt.setDouble(3, ois.getDim1LengthInches());
            stmt.setDouble(4, ois.getDim1WidthInches());
                       
            stmt.setDouble(5, ois.getDim2LengthInches());
            stmt.setDouble(6, ois.getDim2WidthInches());
            
            stmt.setDouble(7, ois.getDimCeilingHeightInches());
            
            stmt.setInt(8, ois.getMaxOccPersons());
            stmt.setInt(9, ois.getMaxSleepingPersons());
            
            stmt.setBoolean(10, ois.isBedroom());
            stmt.setBoolean(11, ois.isLiving());
            stmt.setBoolean(12, ois.isKitchen());
            stmt.setBoolean(13, ois.isBathroom());
            stmt.setBoolean(14, ois.isFinishedSpace());
            
            stmt.setInt(15, ois.getEgressCount());
            stmt.setString(16, ois.getNotes());
            
            stmt.setString(17, ois.getLocationString());
            
            stmt.setDouble(18, ois.getDim1LengthFeet());
            stmt.setDouble(19, ois.getDim1WidthFeet());
            stmt.setDouble(20, ois.getDim2LengthFeet());
            stmt.setDouble(21, ois.getDim2WidthFeet());
            
            stmt.setInt(22, ois.getInspectedSpaceID());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Could not update inspected space metadata", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        } 
    }
    
    /**
     * Builds an ID list of all inspections by OccPeriod or CECase
     * which have not been deactivated.
     * @param inspectable
     * @return
     * @throws IntegrationException 
     */
    public List<Integer> getOccInspectionList(IFace_inspectable inspectable) throws IntegrationException, BObStatusException {

        List<Integer> inspecIDList = new ArrayList<>();
        if(inspectable == null){
            throw new BObStatusException("OccInspectionIntegrator.getOccInspectionList | Cannot retrieve inspections from null inspectable");
        }
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT inspectionid FROM occinspection WHERE ");
        sb.append(inspectable.getDomainEnum().getDbField());
        sb.append("=? AND deactivatedts IS NULL;");
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(sb.toString());
            stmt.setInt(1, inspectable.getHostPK());
            rs = stmt.executeQuery();
            while (rs.next()) {
                inspecIDList.add(rs.getInt("inspectionid"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot retrieve occinspectionlist", ex);
        } finally {
              releasePostgresConnection(con, stmt, rs);
        } 
        return inspecIDList;
    }

    /**
     * Extracts IDs of inspections by pacc
     * @param pacc
     * @return
     * @throws IntegrationException 
     */
    public List<Integer> getOccInspectionListByPACC(int pacc) throws IntegrationException {

        List<Integer> inspecList = new ArrayList<>();

        String query = "SELECT inspectionid FROM occinspection WHERE publicaccesscc=? ";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, pacc);
            rs = stmt.executeQuery();
            while (rs.next()) {
                inspecList.add(rs.getInt("inspectionid"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot retrieve occinspectionlist", ex);

        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return inspecList;
    }
    
    /**
     * Cache-backed getter
     * @param inspectionID
     * @return
     * @throws IntegrationException 
     */
    public FieldInspectionLight getFieldInspectionLight(int inspectionID) throws IntegrationException {
         if(inspectionID == 0){
            throw new IntegrationException("Cannot get inspection with ID == 0");
        }
        
        if(getOccInspectionCacheManager().isCachingEnabled()){
            return getOccInspectionCacheManager().getCacheInspectionLight().get(inspectionID, k -> fetchFieldInspectionLight(inspectionID));
        } else {
            return fetchFieldInspectionLight(inspectionID);
        }
        
        
    }
    
    /**
     * Extracts a single occ inspection light from the DB which contains only the meta data about the
     * inspection: inspector, cause, determination, and date
     * 
     * Note the Coordinator injects it with
     * all sorts of lists, most notably the occinspectedspace list which contains the elements
     * and their inspection status
     * @param inspectionID
     * @return
     * @throws IntegrationException 
     */
    public FieldInspectionLight fetchFieldInspectionLight(int inspectionID) throws DatabaseFetchRuntimeException {

        FieldInspectionLight inspection = null;

        String query = """
                        SELECT inspectionid, occperiod_periodid, inspector_userid, publicaccesscc, 
                              enablepacc, notespreinspection, thirdpartyinspector_personid, thirdpartyinspectorapprovalts, 
                              thirdpartyinspectorapprovalby, maxoccupantsallowed, numbedrooms, numbathrooms, 
                              occchecklist_checklistlistid, effectivedate, occinspection.createdts, followupto_inspectionid, 
                              occinspection.deactivatedts, occinspection.deactivatedby_userid, timestart, timeend, 
                              occinspection.createdby_userid, occinspection.lastupdatedts, occinspection.lastupdatedby_userid, determination_detid, 
                              determinationby_userid, determinationts, remarks, generalcomments, 
                              cause_causeid, cecase_caseid, cnf_buildparceladdressstring(cnf_getparcelofinspectionid(inspectionid), TRUE, TRUE) AS addressstring, 
                                cnf_buildparceladdressstring(cnf_getparcelofinspectionid(inspectionid), FALSE, FALSE) AS addressstring1line,
                              parcelunit.unitnumber, cecase.casename, mobilerawpropertystring, mobilerawpropertyunitstring, fieldinitroutingts, fieldinitroutingby_userid
                            FROM public.occinspection 
                                LEFT OUTER JOIN public.occperiod ON (occinspection.occperiod_periodid = occperiod.periodid)
                                LEFT OUTER JOIN public.parcelunit ON (occperiod.parcelunit_unitid = parcelunit.unitid)
                                LEFT OUTER JOIN public.cecase ON (occinspection.cecase_caseid = cecase.caseid)
                            WHERE inspectionid=?;
                       """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {

            stmt = con.prepareStatement(query);
            stmt.setInt(1, inspectionID);
            rs = stmt.executeQuery();

            while (rs.next()) {
                inspection = generateFieldInspectionLight(rs);
            }

        } catch (SQLException | IntegrationException  ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("Cannot get inspection", ex);

        } finally {
              releasePostgresConnection(con, stmt, rs);
        } 

        return inspection;
    }
    
   
    /**
     * Generator of FieldInspection objects
     * @param rs
     * @return
     * @throws IntegrationException
     * @throws SQLException 
     */
    private FieldInspectionLight generateFieldInspectionLight(ResultSet rs) throws IntegrationException, SQLException {
        FieldInspectionLight ins = new FieldInspectionLight();
        UserCoordinator uc = getUserCoordinator();
        PersonCoordinator pc = getPersonCoordinator();
        
        SystemIntegrator si = getSystemIntegrator();
        
        try {
            
            ins.setInspectionID(rs.getInt("inspectionid"));
            ins.setOccPeriodID(rs.getInt("occperiod_periodid"));
            ins.setCecaseID(rs.getInt("cecase_caseid"));
            ins.setInspector(uc.user_getUser(rs.getInt("inspector_userid")));
            ins.setPacc(rs.getInt("publicaccesscc"));
            
            // META DATA strings
            
            ins.setParentPropertyAddress1Line(rs.getString("addressstring1line"));
            ins.setParentPropertyAddress2LineEscapeFalse(rs.getString("addressstring"));
            ins.setCeCaseName(rs.getString("casename"));
            ins.setOccPeriodUnitNo(rs.getString("unitnumber"));
            
            // Other fields 
            ins.setEnablePacc(rs.getBoolean("enablepacc"));
            ins.setNotesPreInspection(rs.getString("notespreinspection"));
            if (rs.getInt("thirdpartyinspector_personid") != 0) {
                ins.setThirdPartyInspector(pc.getPerson(pc.getHuman(rs.getInt("thirdpartyinspector_personid"))));
            }
            if (rs.getTimestamp("thirdpartyinspectorapprovalts") != null) {
                ins.setThirdPartyInspectorApprovalTS(rs.getTimestamp("thirdpartyinspectorapprovalts").toLocalDateTime());
            }
            
            if (rs.getInt("thirdpartyinspectorapprovalby") != 0) {
                ins.setThirdPartyApprovalBy(uc.user_getUser(rs.getInt("thirdpartyinspectorapprovalby")));
            }
            ins.setMaxOccupantsAllowed(rs.getInt("maxoccupantsallowed"));
            ins.setNumBedrooms(rs.getInt("numbedrooms"));
            ins.setNumBathrooms(rs.getInt("numbathrooms"));
            
            ins.setChecklistTemplateID(rs.getInt("occchecklist_checklistlistid"));
            
            // INSPECTION DATE and its times. Our database uses TIMESTAMP WITH TIME ZONE for all 
            // of these three fields but in Java land and UI land we want LocalDate for the
            // effective date and LocalTime for the time
            if (rs.getTimestamp("effectivedate") != null) {
                ins.setInspectionDate(rs.getTimestamp("effectivedate").toLocalDateTime().toLocalDate());
            }
            if (rs.getTimestamp("timestart") != null) {
                ins.setInspectionTimeStart(rs.getTimestamp("timestart").toLocalDateTime().toLocalTime());
            }
            if (rs.getTimestamp("timeend") != null) {
                ins.setInspectionTimeEnd(rs.getTimestamp("timeend").toLocalDateTime().toLocalTime());
            }
            
            // END DATE MESS
            
            if (rs.getInt("determination_detid") != 0) {
                ins.setDetermination(getOccInspectionDetermination(rs.getInt("determination_detid")));
            }
            
            ins.setFollowUpToInspectionID(rs.getInt("followupto_inspectionid"));
            
            if (rs.getInt("determinationby_userid") != 0) {
                ins.setDeterminationBy(uc.user_getUser(rs.getInt("determinationby_userid")));
            }
            if (rs.getTimestamp("determinationts") != null) {
                ins.setDeterminationTS(rs.getTimestamp("determinationts").toLocalDateTime());
            }
            ins.setRemarks(rs.getString("remarks"));
            ins.setGeneralComments(rs.getString("generalcomments"));
            
            if (rs.getInt("cause_causeid") != 0) {
                ins.setCause(getOccInspectionCause(rs.getInt("cause_causeid")));
            }
            
            // field init fields
            // mobilerawpropertystring, mobilerawpropertyunitstring, fieldinitroutingts, fieldinitroutingby_userid
            ins.setRawPropertyAddress(rs.getString("mobilerawpropertystring"));
            ins.setRawPropertyUnit(rs.getString("mobilerawpropertyunitstring"));  
            
            if(rs.getInt("fieldinitroutingby_userid") != 0){
                ins.setRoutingByUser(uc.user_getUser(rs.getInt("fieldinitroutingby_userid")));
            }
            if(rs.getTimestamp("fieldinitroutingts") != null){
                ins.setRoutingTS(rs.getTimestamp("fieldinitroutingts").toLocalDateTime());
            }
            
            si.populateTrackedFields(ins, rs, false);
            
        } catch (BObStatusException ex) {
            System.out.println(ex);
        }
        return ins;
    }

    /**
     * Updates a single record in the occinspection table but NOT the determination fields
     * @param occInsp
     * @throws IntegrationException 
     */
    public void updateOccInspection(FieldInspection occInsp) throws IntegrationException {
        String sql = """
                        UPDATE public.occinspection
                           SET inspector_userid=?, publicaccesscc=?, 
                               enablepacc=?, notespreinspection=?, thirdpartyinspector_personid=?, thirdpartyinspectorapprovalts=?, 
                               thirdpartyinspectorapprovalby=?, maxoccupantsallowed=?, numbedrooms=?, numbathrooms=?, 
                               occchecklist_checklistlistid=?, effectivedate=?, followupto_inspectionid=?, 
                               deactivatedts=?, deactivatedby_userid=?, timestart=?, timeend=?, 
                               lastupdatedts=now(), lastupdatedby_userid=?, remarks=?, generalcomments=?, cause_causeid=?
                         WHERE inspectionid=?;
                     """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(sql);
            if(occInsp.getInspector() != null){
                stmt.setInt(1, occInsp.getInspector().getUserID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            stmt.setInt(2, occInsp.getPacc());

            stmt.setBoolean(3, occInsp.isEnablePacc());
            stmt.setString(4, occInsp.getNotesPreInspection());
            if (occInsp.getThirdPartyInspector() != null) {
                stmt.setInt(5, occInsp.getThirdPartyInspector().getPersonID());
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            if (occInsp.getThirdPartyInspectorApprovalTS() != null) {
                stmt.setTimestamp(6, java.sql.Timestamp.valueOf(occInsp.getThirdPartyInspectorApprovalTS()));
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }

            if (occInsp.getThirdPartyApprovalBy() != null) {
                stmt.setInt(7, occInsp.getThirdPartyApprovalBy().getUserID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            stmt.setInt(8, occInsp.getMaxOccupantsAllowed());
            stmt.setInt(9, occInsp.getNumBedrooms());
            stmt.setInt(10, occInsp.getNumBathrooms());

            if (occInsp.getChecklistTemplate() != null) {
                stmt.setInt(11, occInsp.getChecklistTemplate().getInspectionChecklistID());
            } else {
                stmt.setNull(11, java.sql.Types.NULL);
            }
            if (occInsp.getEffectiveDateOfRecord() != null) {
                stmt.setTimestamp(12, java.sql.Timestamp.valueOf(occInsp.getEffectiveDateOfRecord()));
            } else {
                stmt.setNull(12, java.sql.Types.NULL);
            }
            if (occInsp.getFollowUpToInspectionID() != 0) {
                stmt.setInt(13, occInsp.getFollowUpToInspectionID());
            } else {
                stmt.setNull(13, java.sql.Types.NULL);
            }

            if (occInsp.getDeactivatedTS() != null) {
                stmt.setTimestamp(14, java.sql.Timestamp.valueOf(occInsp.getDeactivatedTS()));
            } else {
                stmt.setNull(14, java.sql.Types.NULL);
            }
            if (occInsp.getDeactivatedBy() != null) {
                stmt.setInt(15, occInsp.getDeactivatedBy().getUserID());
            } else {
                stmt.setNull(15, java.sql.Types.NULL);
            }
            if (occInsp.getTimeStart() != null) {
                stmt.setTimestamp(16, java.sql.Timestamp.valueOf(occInsp.getTimeStart()));
            } else {
                stmt.setNull(16, java.sql.Types.NULL);
            }
            if (occInsp.getTimeEnd() != null) {
                stmt.setTimestamp(17, java.sql.Timestamp.valueOf(occInsp.getTimeEnd()));
            } else {
                stmt.setNull(17, java.sql.Types.NULL);
            }
            if (occInsp.getLastUpdatedBy() != null) {
                stmt.setInt(18, occInsp.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(18, java.sql.Types.NULL);
            }
            stmt.setString(19, occInsp.getRemarks());
            stmt.setString(20, occInsp.getGeneralComments());
            if (occInsp.getCause() != null) {
                stmt.setInt(21, occInsp.getCause().getCauseID());
            } else {
                stmt.setNull(21, java.sql.Types.NULL);
            }

            stmt.setInt(22, occInsp.getInspectionID());

            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update occupancy inspection record", ex);
        } finally {
              releasePostgresConnection(con, stmt);
        }
        
    }
    
      /**
     * Updates a single record in the occinspection table but NOT the determination fields
     * @param occInsp
     * @throws IntegrationException 
     */
    public void updateOccInspectionFieldInitiatedRouting(FieldInspectionLight occInsp) throws IntegrationException {
        String sql = """
                        UPDATE public.occinspection
                         	SET occperiod_periodid=?,  cecase_caseid=?, lastupdatedts=now(), 
                                    lastupdatedby_userid=?, fieldinitroutingts=now(), fieldinitroutingby_userid=?
                         	WHERE inspectionid=?;
                     """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(sql);
            
            if(occInsp.getOccPeriodID() != 0){
                stmt.setInt(1, occInsp.getOccPeriodID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            
            if(occInsp.getCecaseID() != 0){
                stmt.setInt(2, occInsp.getCecaseID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            
            if (occInsp.getLastUpdatedBy() != null) {
                stmt.setInt(3, occInsp.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            
            if(occInsp.getRoutingByUser() != null){
                stmt.setInt(4, occInsp.getRoutingByUser().getUserID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }

            stmt.setInt(5, occInsp.getInspectionID());

            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update occupancy inspectino routing fields", ex);
        } finally {
              releasePostgresConnection(con, stmt);
        }
    }
    
    /**
     * Pathway for dumping a FIN back into the unassigned pool, wiping out any previous links to cases, or periods, 
     * and nulling out field routing stamping
     * 
     * @param occInsp
     * @throws IntegrationException 
     */
    public void updateOccInspectionRevertToUnassignedPool(FieldInspectionLight occInsp) throws IntegrationException {
        String sql = """
                        UPDATE public.occinspection
                         	SET occperiod_periodid=?,  cecase_caseid=NULL, lastupdatedts=now(), 
                                    lastupdatedby_userid=?, mobilerawpropertystring=?, mobilerawpropertyunitstring=?, fieldinitroutingts=NULL, fieldinitroutingby_userid=NULL, remarks=?
                         	WHERE inspectionid=?;
                     """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(sql);
            
            if(occInsp.getOccPeriodID() != 0){
                stmt.setInt(1, occInsp.getOccPeriodID());
            } else {
                throw new IntegrationException("Cannot revert inspection to occ period ID 0");
            }
            
            if (occInsp.getLastUpdatedBy() != null) {
                stmt.setInt(2, occInsp.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            
            stmt.setString(3, occInsp.getRawPropertyAddress());
            stmt.setString(4, occInsp.getRawPropertyUnit());
            stmt.setString(5, occInsp.getRemarks());
            stmt.setInt(6, occInsp.getInspectionID());

            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update occupancy reversion fields", ex);
        } finally {
              releasePostgresConnection(con, stmt);
        }
    }
    
    /**
     * Sets the determination of the given FieldInspection or to remove finalization
     * @param occInsp
     * @throws IntegrationException 
     */
     public void updateOccInspectionFinalizationStatus(FieldInspection occInsp) throws IntegrationException { 
        String sql = """
                     UPDATE public.occinspection
                        SET lastupdatedts=now(), lastupdatedby_userid=?, determination_detid=?, determinationby_userid=?, 
                            determinationts=?
                      WHERE inspectionid=?;
                     """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(sql);
           
           
            if (occInsp.getLastUpdatedBy() != null) {
                stmt.setInt(1, occInsp.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            if (occInsp.getDetermination() != null) {
                stmt.setInt(2, occInsp.getDetermination().getDeterminationID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            if (occInsp.getDeterminationBy() != null) {
                stmt.setInt(3, occInsp.getDeterminationBy().getUserID());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }

            if (occInsp.getDeterminationTS() != null) {
                stmt.setTimestamp(4, java.sql.Timestamp.valueOf(occInsp.getDeterminationTS()));
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            stmt.setInt(5, occInsp.getInspectionID());

            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to write field inspection determination to occ inspection record", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        }
    }

    /**
     * Sets the deactivation fields on the given FieldInspection
     * @param ins
     * @throws IntegrationException 
     */
    public void deactivateOccInspection(FieldInspectionLight ins) throws IntegrationException {

        String query = "UPDATE occinspection SET deactivatedts=now(), deactivatedby_userid=? WHERE inspectionid=?;";
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);
            if(ins.getDeactivatedBy() != null){
                stmt.setInt(1, ins.getDeactivatedBy().getUserID());
            } else {
                throw new IntegrationException("Cannot deactivate occ inspection with null deac by User");
            }
            stmt.setInt(2, ins.getInspectionID());
                    
            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot mark inspection as deactivated, sorries!", ex);
        } finally {
              releasePostgresConnection(con, stmt);
        } 
    }

   
    /**
     * Creates a new record in the occinspection table
     * @param occInsp
     * @return
     * @throws IntegrationException 
     */
    public int insertOccInspection(FieldInspection occInsp) throws IntegrationException {
        String query = """
                       INSERT INTO public.occinspection(
                                   inspectionid, occperiod_periodid, inspector_userid, publicaccesscc, 
                                   enablepacc, notespreinspection, thirdpartyinspector_personid, thirdpartyinspectorapprovalts, 
                                   thirdpartyinspectorapprovalby, maxoccupantsallowed, numbedrooms, numbathrooms, 
                                   occchecklist_checklistlistid, effectivedate, createdts, followupto_inspectionid, 
                                   deactivatedts, deactivatedby_userid, timestart, timeend, 
                                   createdby_userid, lastupdatedts, lastupdatedby_userid, determination_detid, 
                                   determinationby_userid, determinationts, remarks, generalcomments, 
                                   cause_causeid, cecase_caseid)
                           VALUES (DEFAULT, ?, ?, ?, 
                                   ?, ?, ?, ?, 
                                   ?, ?, ?, ?, 
                                   ?, ?, now(), ?, 
                                   ?, ?, ?, ?, 
                                   ?, ?, ?, ?, 
                                   ?, ?, ?, ?, 
                                   ?, ?);""";

        Connection con = null;
        ResultSet rs = null;
        PreparedStatement stmt = null;
        int newInspectionID = 0;
        try {
            con = getPostgresCon();

            stmt = con.prepareStatement(query);
            if(occInsp.getDomainEnum() == null){
                throw new IntegrationException("cannot write inspection with null domain");
            }
            switch(occInsp.getDomainEnum()){
                case OCCUPANCY:
                    stmt.setInt(1, occInsp.getOccPeriodID());
                    stmt.setNull(28, java.sql.Types.NULL);
                    break;
                case CODE_ENFORCEMENT:
                    stmt.setInt(28, occInsp.getCecaseID());
                    stmt.setNull(1, java.sql.Types.NULL);
                    break;
            }
            stmt.setInt(2, occInsp.getInspector().getUserID());
            stmt.setInt(3, occInsp.getPacc());

            stmt.setBoolean(4, occInsp.isEnablePacc());
            stmt.setString(5, occInsp.getNotesPreInspection());
            if (occInsp.getThirdPartyInspector() != null) {
                stmt.setInt(6, occInsp.getThirdPartyInspector().getPersonID());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            if (occInsp.getThirdPartyInspectorApprovalTS() != null) {
                stmt.setTimestamp(7, java.sql.Timestamp.valueOf(occInsp.getThirdPartyInspectorApprovalTS()));
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }

            if (occInsp.getThirdPartyApprovalBy() != null) {
                stmt.setInt(8, occInsp.getThirdPartyApprovalBy().getUserID());
            } else {
                stmt.setNull(8, java.sql.Types.NULL);
            }
            stmt.setInt(9, occInsp.getMaxOccupantsAllowed());
            stmt.setInt(10, occInsp.getNumBedrooms());
            stmt.setInt(11, occInsp.getNumBathrooms());
            
            if (occInsp.getChecklistTemplate() != null) {
                stmt.setInt(12, occInsp.getChecklistTemplate().getInspectionChecklistID());
            } else {
                stmt.setNull(12, java.sql.Types.NULL);
            }
            if (occInsp.getEffectiveDateOfRecord() != null) {
                stmt.setTimestamp(13, java.sql.Timestamp.valueOf(occInsp.getEffectiveDateOfRecord()));
            } else {
                stmt.setNull(13, java.sql.Types.NULL);
            }
            if (occInsp.getFollowUpToInspectionID() != 0) {
                stmt.setInt(14, occInsp.getFollowUpToInspectionID());
            } else {
                stmt.setNull(14, java.sql.Types.NULL);
            }
            if (occInsp.getDeactivatedTS() != null) {
                stmt.setTimestamp(15, java.sql.Timestamp.valueOf(occInsp.getDeactivatedTS()));
            } else {
                stmt.setNull(15, java.sql.Types.NULL);
            }
            if (occInsp.getDeactivatedBy() != null) {
                stmt.setInt(16, occInsp.getDeactivatedBy().getUserID());
            } else {
                stmt.setNull(16, java.sql.Types.NULL);
            }
            if (occInsp.getTimeStart() != null) {
                stmt.setTimestamp(17, java.sql.Timestamp.valueOf(occInsp.getTimeStart()));
            } else {
                stmt.setNull(17, java.sql.Types.NULL);
            }
            if (occInsp.getTimeEnd() != null) {
                stmt.setTimestamp(18, java.sql.Timestamp.valueOf(occInsp.getTimeEnd()));
            } else {
                stmt.setNull(18, java.sql.Types.NULL);
            }

            if (occInsp.getCreatedBy() != null) {
                stmt.setInt(19, occInsp.getCreatedBy().getUserID());
            } else {
                stmt.setNull(19, java.sql.Types.NULL);
            }
            if (occInsp.getLastUpdatedTS() != null) {
                stmt.setTimestamp(20, java.sql.Timestamp.valueOf(occInsp.getLastUpdatedTS()));
            } else {
                stmt.setNull(20, java.sql.Types.NULL);
            }
            if (occInsp.getLastUpdatedBy() != null) {
                stmt.setInt(21, occInsp.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(21, java.sql.Types.NULL);
            }
            if (occInsp.getDetermination() != null) {
                stmt.setInt(22, occInsp.getDetermination().getDeterminationID());
            } else {
                stmt.setNull(22, java.sql.Types.NULL);
            }

            if (occInsp.getDeterminationBy() != null) {
                stmt.setInt(23, occInsp.getDeterminationBy().getUserID());
            } else {
                stmt.setNull(23, java.sql.Types.NULL);
            }
            if (occInsp.getDeterminationTS() != null) {
                stmt.setTimestamp(24, java.sql.Timestamp.valueOf(occInsp.getDeterminationTS()));
            } else {
                stmt.setNull(24, java.sql.Types.NULL);
            }
            stmt.setString(25, occInsp.getRemarks());
            stmt.setString(26, occInsp.getGeneralComments());

            if (occInsp.getCause() != null) {
                stmt.setInt(27, occInsp.getCause().getCauseID());
            } else {
                stmt.setNull(27, java.sql.Types.NULL);
            }
            stmt.execute();

            String retrievalQuery = "SELECT currval('occupancyinspectionid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();

            while (rs.next()) {
                newInspectionID = rs.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert OccupancyInspection", ex);
        } finally {
             releasePostgresConnection(con, stmt, rs);
        }
        
        return newInspectionID;
    }
    
    /**
     * Looks for any dispatches by inspection. This tool will retrieve deactivated dispatches
     * so no new dispatches can be made
     * @param fin
     * @return the ID of at maximum one dipsatch; 0 for no dispatches
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public int getOccInspectionDispatchByInspection(FieldInspectionLight fin) throws IntegrationException{
        if(fin == null){
            throw new IntegrationException("Cannot fetch dispatches by null field inspection");
        }
        
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        StringBuilder sb = new StringBuilder();
        
        sb.append("SELECT dispatchid FROM occinspectiondispatch WHERE inspection_inspectionid=?;");
        int did = 0;

        try {
            stmt = con.prepareStatement(sb.toString());
            stmt.setInt(1, fin.getInspectionID());

            rs = stmt.executeQuery();
            while (rs.next()) {
                did = rs.getInt("dispatchid");
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("unable find dispatches by inspection", ex);
        } finally {
           releasePostgresConnection(con, stmt, rs);
        } 
        return did;
    }
    
    /**
     * Cache backed getter
     * @param dispatchID
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public OccInspectionDispatch getOccInspectionDispatch(int dispatchID) throws IntegrationException, BObStatusException{
        if(dispatchID == 0){
            throw new IntegrationException("cannot get dispatch of ID == 0");
        }
        if(getOccInspectionCacheManager().isCachingEnabledHandheld()){
            return getOccInspectionCacheManager().getCacheDispatch().get(dispatchID, k -> fetchOccInspectionDispatch(dispatchID));
        } else {
            return fetchOccInspectionDispatch(dispatchID);
        }
    }
    
    /**
     * Extracts a dispatch from the DB by ID
     * @param dispatchID
     * @return the fully-baked inspection dispatch
     */
    public OccInspectionDispatch fetchOccInspectionDispatch(int dispatchID) throws DatabaseFetchRuntimeException{
        
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT dispatchid, createdby_userid, createdts, dispatchnotes, "
                + "inspection_inspectionid, retrievalts, retrievedby_userid, synchronizationts, synchronizationnotes, "
                + "deactivatedts, deactivatedby_userid, lastupdatedts, lastupdatedby_userid\n" 
                + "	FROM public.occinspectiondispatch WHERE dispatchid=?;");
        OccInspectionDispatch dispatch = null;

        try {
            stmt = con.prepareStatement(sb.toString());
            stmt.setInt(1, dispatchID);

            rs = stmt.executeQuery();
            while (rs.next()) {
                dispatch = generateOccInspectionDispatch(rs);
            }
        } catch (SQLException | BObStatusException | IntegrationException  ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("unable to generate dispatch", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 

        return dispatch;
        
    }
    
    /**
     * Internal generator for dispatches
     * @param rs
     * @return 
     */
    private OccInspectionDispatch generateOccInspectionDispatch(ResultSet rs) throws SQLException, IntegrationException, BObStatusException{
        if(rs == null){
            return null;
        }
        
        UserCoordinator uc = getUserCoordinator();
        SystemIntegrator si = getSystemIntegrator();
        
        OccInspectionDispatch dispatch = new OccInspectionDispatch();
        dispatch.setDispatchID(rs.getInt("dispatchid"));
        dispatch.setDispatchNotes(rs.getString("dispatchnotes"));
        dispatch.setInspectionID(rs.getInt("inspection_inspectionid"));
        if(rs.getTimestamp("retrievalts") != null){
            dispatch.setRetrievalTS(rs.getTimestamp("retrievalts").toLocalDateTime());
        }
        if(rs.getInt("retrievedby_userid") != 0){
            dispatch.setRetrievedBy(uc.user_getUser(rs.getInt("retrievedby_userid")));   
        }
        if(rs.getTimestamp("synchronizationts") != null){
            dispatch.setSynchronizationTS(rs.getTimestamp("synchronizationts").toLocalDateTime());
        }
        dispatch.setSynchronizationNotes(rs.getString("synchronizationnotes"));
        si.populateTrackedFields(dispatch, rs, false);
        return dispatch;
        
    }

    
    /**
     * Writes a given dispatch to the DB
     * @param oid
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public int insertOccInspectionDispatch(OccInspectionDispatch oid) throws IntegrationException{
        if(oid == null){
            throw new IntegrationException("Cannot insert a null dispatch");
        }
        
        String query = "INSERT INTO public.occinspectiondispatch(\n" +
                        "	dispatchid, createdby_userid, createdts, dispatchnotes, \n" +
                        "	inspection_inspectionid, retrievalts, retrievedby_userid, \n" +
                        "	synchronizationts, synchronizationnotes, deactivatedts, deactivatedby_userid, \n" +
                        "	lastupdatedts, lastupdatedby_userid)\n" +
                        "      VALUES (DEFAULT, ?, now(), ?, "
                                    + "?, ?, ?, "
                                    + "?, ?, NULL, NULL, "
                                    + "now(), ?);";
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement stmt = null;
        int freshDispatchID = 0;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
      
            if(oid.getCreatedBy() != null && oid.getCreatedBy().getUserID() != 0){
                System.out.println("OccInspectionIntegrator.insertOccInspectionDispatch : user " + oid.getCreatedBy().getUserID());
                stmt.setInt(1, oid.getCreatedBy().getUserID());
            } else {
                System.out.println("OccInspectionIntegrator.insertOccInspectionDispatch : NULL CR USER ");
                stmt.setNull(1, java.sql.Types.NULL);
            }
            stmt.setString(2, oid.getDispatchNotes());

            stmt.setInt(3, oid.getInspectionID());
            if(oid.getRetrievalTS() != null){
                stmt.setTimestamp(4, java.sql.Timestamp.valueOf(oid.getRetrievalTS()));
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            if(oid.getRetrievedBy()!= null){
                stmt.setInt(5, oid.getRetrievedBy().getUserID());
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }

            if(oid.getSynchronizationTS() != null){
                stmt.setTimestamp(6, java.sql.Timestamp.valueOf(oid.getSynchronizationTS()));
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            stmt.setString(7, oid.getSynchronizationNotes());
            if(oid.getLastUpdatedBy() != null){
                stmt.setInt(8, oid.getLastUpdatedByUserID());
            } else {
                stmt.setNull(8, java.sql.Types.NULL);
            }
            
            stmt.execute();

            String retrievalQuery = "SELECT currval('occinspectiondispatch_dispatchid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();

            while (rs.next()) {
                freshDispatchID = rs.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert OccInspectionDispatch", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }
        
        return freshDispatchID;
        
    }
    
    /**
     * Updates a record of the occinspectiondispatch table
     * @param oid
     * @throws IntegrationException 
     */
    public void updateOccInspectionDispatch(OccInspectionDispatch oid) throws IntegrationException{
        if(oid == null){
            throw new IntegrationException("Cannot update a null dispatch");
        }
        
        String query = """
                       UPDATE public.occinspectiondispatch 
                       SET dispatchnotes=?, inspection_inspectionid=?,  
                       retrievalts=?, retrievedby_userid=?, synchronizationts=?, 
                       synchronizationnotes=?, 
                       deactivatedts=?, deactivatedby_userid=?, lastupdatedts=now(), 
                       lastupdatedby_userid=? 
                       WHERE dispatchid=?;
                       """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);
      
            stmt.setString(1, oid.getDispatchNotes());

            stmt.setInt(2, oid.getInspectionID());
            if(oid.getRetrievalTS() != null){
                stmt.setTimestamp(3, java.sql.Timestamp.valueOf(oid.getRetrievalTS()));
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            if(oid.getRetrievedBy()!= null){
                stmt.setInt(4, oid.getRetrievedBy().getUserID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }

            if(oid.getSynchronizationTS() != null){
                stmt.setTimestamp(5, java.sql.Timestamp.valueOf(oid.getSynchronizationTS()));
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            stmt.setString(6, oid.getSynchronizationNotes());

            if(oid.getDeactivatedTS() != null){
                stmt.setTimestamp(7, java.sql.Timestamp.valueOf(oid.getDeactivatedTS()));
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            
            if(oid.getDeactivatedBy() != null){
                stmt.setInt(8, oid.getDeactivatedByUserID());
            } else {
                stmt.setNull(8, java.sql.Types.NULL);
            }
            
            if(oid.getLastUpdatedBy() != null){
                stmt.setInt(9, oid.getLastUpdatedByUserID());
            } else {
                stmt.setNull(9, java.sql.Types.NULL);
            }
            stmt.setInt(10, oid.getDispatchID());
            
            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot update OccInspectionDispatch", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }
        
        
        
    }
  
    /**
     * See if a determination has been used in the DB and if not, it can be deleted
     * @param d
     * @return
     * @throws IntegrationException 
     */
    public int determinationCheckForUse(OccInspectionDetermination d) throws IntegrationException {
        SystemIntegrator si = getSystemIntegrator();
        int uses = 0;
        List<String> useTables = si.findForeignUseTables("determination");
        for(int x = 0; x < useTables.size(); x++){
            uses =+ si.checkForUse("public." + useTables.get(x), "determination_detid", d.getDeterminationID());
            System.out.println("Checked public." + useTables.get(x) + " for  determination_detid" + d.getDeterminationID());
        }
        return uses;
    }
    
    /**
     * cache backed getter
     * @param determinationID
     * @return
     * @throws IntegrationException 
     */
    public OccInspectionDetermination getOccInspectionDetermination(int determinationID) throws IntegrationException {
        if(determinationID == 0){
            throw new IntegrationException("Cannot get determination with ID of 0");
        }
        if(getOccInspectionCacheManager().isCachingEnabled()){
            return getOccInspectionCacheManager().getCacheDetermination().get(determinationID, k -> fetchOccInspectionDetermination(determinationID));
        } else {
            return fetchOccInspectionDetermination(determinationID);
            
        }
        
    }
    
    /**
     * Create a determination object from the db
     * @param determinationID
     * @return
     */
    public OccInspectionDetermination fetchOccInspectionDetermination(int determinationID) throws DatabaseFetchRuntimeException {
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        String query = """
                       SELECT determinationid, title, description, notes, eventcat_catid,qualifiesaspassed, documentationonly,
                       createdts, createdby_umapid, deactivatedby_umapid, lastupdatedts, lastupdatedby_umapid, deactivatedts, muni_municode, icon_iconid
                       FROM public.occinspectiondetermination WHERE determinationid=?;
                       """;
        OccInspectionDetermination d = null;
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, determinationID);

            rs = stmt.executeQuery();
            while (rs.next()) {
                d = generateOccInspectionDetermination(rs);
            }
        } catch (SQLException | BObStatusException | IntegrationException ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("unable to generate determination", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 

        return d;
    }
    
    /**
     * Builds an occ inspection determination from a ResultSet
     * @param rs
     * @return
     * @throws IntegrationException
     * @throws SQLException
     * @throws BObStatusException 
     */
    private OccInspectionDetermination generateOccInspectionDetermination(ResultSet rs) throws IntegrationException, SQLException, BObStatusException{
        if(rs == null){
            throw new IntegrationException("Cannot generate determination");
        }
        
        EventCoordinator ec = getEventCoordinator();
        SystemIntegrator si = getSystemIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        MunicipalityCoordinator mc = getMuniCoordinator();

        OccInspectionDetermination d = new OccInspectionDetermination();
        d.setDeterminationID(rs.getInt("determinationid"));
        d.setTitle(rs.getString("title"));
        d.setDescription(rs.getString("description"));
        d.setNotes(rs.getString("notes"));
        if (rs.getInt("eventcat_catid") != 0) {
            d.setEventCategory(ec.getEventCategory(rs.getInt("eventcat_catid")));
        }
        
        d.setQualifiesAsPassed(rs.getBoolean("qualifiesaspassed"));
        
        d.setDocumentationOnly(rs.getBoolean("documentationonly"));
        
        if(rs.getInt("muni_municode") != 0){
            d.setMuni(mc.getMuni(rs.getInt("muni_municode")));
        }
        
        if(rs.getInt("icon_iconid") != 0){
            d.setIcon(sc.getIcon(rs.getInt("icon_iconid")));
        }
        
        si.populateUMAPTrackedFields(d, rs, true);

        return d;
    }

    /**
     * Deactivates an occ inspection determination 
     * @param d
     * @throws IntegrationException 
     */
    public void deactivateDetermination(OccInspectionDetermination d) throws IntegrationException {
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE public.occinspectiondetermination SET active=false ");
        sb.append("WHERE determinationid=?");
        
        try{
            stmt = con.prepareStatement(sb.toString());
            stmt.setInt(1, d.getDeterminationID());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("unable to deactivate determination", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        } 
    }

    /**
     * Update an OccInspection determination 
     * @param determination
     * @throws IntegrationException 
     */
    public void updateOccInspectionDetermination(OccInspectionDetermination determination) throws IntegrationException {
        String query = """
                UPDATE public.occinspectiondetermination
                	SET title=?, description=?, notes=?, 
                            eventcat_catid=?, qualifiesaspassed=?, lastupdatedts=now(), 
                            lastupdatedby_umapid=?, muni_municode=?, documentationonly=?, icon_iconid=?
                	WHERE determinationid=?;
                """;
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setString(1, determination.getTitle());
            stmt.setString(2, determination.getDescription());
            stmt.setString(3, determination.getNotes());
            if (Objects.nonNull(determination.getEventCategory())) {
                stmt.setInt(4, determination.getEventCategory().getCategoryID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }

            stmt.setBoolean(5, determination.isQualifiesAsPassed());
            if (Objects.nonNull(determination.getLastUpdatedby_UMAP())) {
                stmt.setInt(6, determination.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            if (Objects.nonNull(determination.getMuni())){
                stmt.setInt(7, determination.getMuni().getMuniCode());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            stmt.setBoolean(8, determination.isDocumentationOnly());
            
            if(determination.getIcon() != null){
                stmt.setInt(9, determination.getIcon().getID());
            } else {
                stmt.setNull(9, java.sql.Types.NULL);
            }
            
            stmt.setInt(10, determination.getDeterminationID());
            
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("unable to update determination", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        } 
        
    }

    /**
     * Insert new OccInspection determination 
     * @param determination
     * @return freshID - id of newly inserted record
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public int insertOccInspectionDetermination(OccInspectionDetermination determination) throws IntegrationException, BObStatusException {
        if (Objects.isNull(determination)) {
            throw new BObStatusException("cannot insert null object into occinspection determination table");
        }
        String query = """
                INSERT INTO public.occinspectiondetermination(
                	title, description, notes, 
                       eventcat_catid, qualifiesaspassed, createdts, 
                       createdby_umapid, deactivatedby_umapid, lastupdatedts, 
                       lastupdatedby_umapid, deactivatedts, muni_municode,
                       documentationonly, icon_iconid)
                	VALUES (?, ?, ?, 
                                ?, ?, now(), 
                                ?, NULL, now(), 
                                ?, NULL, ?,
                                ?, ?);""";

        Connection con = null;
        
        PreparedStatement stmt = null;
        int freshID = 0;
        ResultSet rs = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setString(1, determination.getTitle());
            stmt.setString(2, determination.getDescription());
            stmt.setString(3, determination.getNotes());

            if (Objects.nonNull(determination.getEventCategory())) {
                stmt.setInt(4, determination.getEventCategory().getCategoryID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }

            stmt.setBoolean(5, determination.isQualifiesAsPassed());
            if (Objects.nonNull(determination.getCreatedby_UMAP())) {
                stmt.setInt(6, determination.getCreatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }

            if (Objects.nonNull(determination.getLastUpdatedby_UMAP())) {
                stmt.setInt(7, determination.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            
            if (Objects.nonNull(determination.getMuni())){
                stmt.setInt(8, determination.getMuni().getMuniCode());
            } else {
                stmt.setNull(8, java.sql.Types.NULL);
            }
            
            stmt.setBoolean(9, determination.isDocumentationOnly());
            
             if(determination.getIcon() != null){
                stmt.setInt(10, determination.getIcon().getID());
            } else {
                stmt.setNull(10, java.sql.Types.NULL);
            }

            stmt.execute();

            String retrievalQuery = "SELECT currval('occinspection_determination_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();

            while (rs.next()) {
                freshID = rs.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("unable to insert determination", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return freshID;
    }
    
     /**
     * returns an OccInspection determination list
     * @param requireActive
     * @param muni
     * @return detList
     * @throws IntegrationException 
     */
     public List<OccInspectionDetermination> getOccInspectionDeterminationListComplete(boolean requireActive, Municipality muni) throws IntegrationException {
         List<OccInspectionDetermination> detList = new ArrayList<>();
         StringBuilder sb = new StringBuilder();
         sb.append("SELECT determinationid FROM public.occinspectiondetermination WHERE determinationid IS NOT NULL ");
         if(muni != null){
             sb.append("AND muni_municode=? ");
         }
         if (requireActive) {
             sb.append("AND deactivatedts is null");
         }
         sb.append(";");
         Connection con = getPostgresCon();
         ResultSet rs = null;
         PreparedStatement stmt = null;

         try {
             stmt = con.prepareStatement(sb.toString());
             if(muni != null){
                 stmt.setInt(1, muni.getMuniCode());
             }
             rs = stmt.executeQuery();
             while (rs.next()) {
                 detList.add(getOccInspectionDetermination(rs.getInt("determinationid")));
             }
         } catch (SQLException ex) {
             System.out.println(ex.toString());
             throw new IntegrationException("Unable to generate OccInspectionDetermination(List)", ex);
         } finally {
             releasePostgresConnection(con, stmt, rs);
         } 
         return detList;
     }
        
    public int requirementCheckForUse(OccInspectionRequirement r) throws IntegrationException {
        SystemIntegrator si = getSystemIntegrator();
        int uses = 0;
        List<String> useTables = si.findForeignUseTables("requirementid");
        for(int x = 0; x < useTables.size(); x++){
            uses =+ si.checkForUse("public." + useTables.get(x), "requirement_requirementid", r.getRequirementID());
            System.out.println("Checked public." + useTables.get(x) + " for  requirement_requirementid" + r.getRequirementID());
        };
        return uses;
    }
    
    public OccInspectionRequirement getRequirement(int requirementID) throws IntegrationException {
        EventIntegrator ei = new EventIntegrator();
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT requirementid, title, description, active ");
        sb.append("FROM public.occinspectionrequirement WHERE requirementid=?;");
        OccInspectionRequirement r = null;

        try {
            stmt = con.prepareStatement(sb.toString());
            stmt.setInt(1, requirementID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                r = new OccInspectionRequirement();
                r.setRequirementID(rs.getInt("requirementid"));
                r.setTitle(rs.getString("title"));
                r.setDescription(rs.getString("description"));
                r.setActive(rs.getBoolean("active"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("unable to generate requirement", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return r;
    }

    public void deactivateRequirement(OccInspectionRequirement r) throws IntegrationException {
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE public.occinspectionrequirement SET active=false ");
        sb.append("WHERE requirementid=?");
        
        try{
            stmt = con.prepareStatement(sb.toString());
            stmt.setInt(1, r.getRequirementID());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("unable to deactivate requirement", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        } 
    }

    public void updateRequirement(OccInspectionRequirement r) throws IntegrationException {
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE public.occinspectionrequirement SET title=?, description=?, active=? ");
        sb.append("WHERE requirementid = ?;");

        try {
            stmt = con.prepareStatement(sb.toString());
            stmt.setString(1, r.getTitle());
            stmt.setString(2, r.getDescription());
            stmt.setBoolean(3, r.isActive());
            stmt.setInt(4, r.getRequirementID());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("unable to update requirement", ex);
        } finally {
             releasePostgresConnection(con, stmt);
             
        } 
    }


    public void insertRequirement(OccInspectionRequirement r) throws IntegrationException {

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO public.occinspectionrequirement(");
        sb.append("requirementid, title, description, active) ");
        sb.append("VALUES (DEFAULT, ?, ?, ?);");

        try {
            stmt = con.prepareStatement(sb.toString());
            stmt.setString(1, r.getTitle());
            stmt.setString(2, r.getDescription());
            stmt.setBoolean(3, true);
            stmt.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("unable to insert requirement", ex);
        } finally {
            releasePostgresConnection(con, stmt);
             
        } 
    }

   
    public List<OccInspectionRequirement> getRequirementList() throws IntegrationException {

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT requirementid FROM public.occinspectionrequirement WHERE active=true;");
        List<OccInspectionRequirement> reqList = new ArrayList<>();
        try {
            stmt = con.prepareStatement(sb.toString());
            rs = stmt.executeQuery();
            while (rs.next()) {
                reqList.add(getRequirement(rs.getInt("requirementid")));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to generate OccInspectionRequirement(List)", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return reqList;
    }
    
    public List<OccInspectionRequirementAssigned> getOccRequirementAssignedList(FieldInspection inspection) 
            throws IntegrationException, BObStatusException{
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT occrequirement_requirementid FROM public.occinspectionrequirementassigned ");
        sb.append("WHERE occinspection_inspectionid = ?;");
        List<OccInspectionRequirementAssigned> assignedList = new ArrayList<>();
        try {
            stmt = con.prepareStatement(sb.toString());
            stmt.setInt(1, inspection.getInspectionID());
            rs = stmt.executeQuery();
            while(rs.next()){
                assignedList.add(getOccRequirementAssigned(inspection.getInspectionID(), rs.getInt("occrequirement_requirementid")));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot get OccRequiremetnAssignedList");
            
        } finally {
            releasePostgresConnection(con, stmt);
        } 
        return assignedList;
    }
    
    public OccInspectionRequirementAssigned getOccRequirementAssigned(int inspectionID, int requirementID) 
            throws IntegrationException, BObStatusException{
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM public.occinspectionrequirementassigned ");
        sb.append("WHERE occinspection_inspectionid = ? ");
        sb.append("AND occrequirement_requirementid = ?;");
        OccInspectionRequirementAssigned reqAssigned = null;
        
        try {
            stmt = con.prepareStatement(sb.toString());
            stmt.setInt(1, inspectionID);
            rs = stmt.executeQuery();
            reqAssigned = generateOccRequirementAssigned(rs);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("cannot get occ requirement assigned");
            
        
        } finally {
            releasePostgresConnection(con, stmt);
        } 
        return reqAssigned;
    }
    
    private OccInspectionRequirementAssigned generateOccRequirementAssigned(ResultSet rs) throws BObStatusException, IntegrationException{
        OccInspectionRequirementAssigned reqAssigned = null;
        if(rs == null){
            return reqAssigned;
        }
        UserCoordinator uc = getUserCoordinator();
        try {            
            //check for null rs
            //if not null make a new instance of OccInspectionRequirementAssigned
            //in constructor call get requirement and pass in requirementid
            reqAssigned = new OccInspectionRequirementAssigned(getRequirement(rs.getInt("occrequirement_requirementid")));
            reqAssigned.setInspectionID(rs.getInt("occinspection_inspectionid"));
            
            reqAssigned.setAssignedBy(uc.user_getUser(rs.getInt("assignedby")));
            reqAssigned.setAssignedDate(rs.getTimestamp("assigneddate").toLocalDateTime());
            reqAssigned.setAssignedNotes(rs.getString("assignednotes"));
            
            if(rs.getTimestamp("fulfilleddate") != null){
                reqAssigned.setFulfilledBy(uc.user_getUser(rs.getInt("fulfilledby")));
                reqAssigned.setFulfilledDate(rs.getTimestamp("fulfilleddate").toLocalDateTime());
                reqAssigned.setFulfilledNotes(rs.getString("fulfillednotes"));
            }
            
            reqAssigned.setNotes(rs.getString("notes"));
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("cannot generate occrequirementassigned");
        }
        return reqAssigned;
    }
    
    
    /**
     * cache backed getter for occ inspection causes
     * @param causeID
     * @return
     * @throws IntegrationException 
     */
    public OccInspectionCause getOccInspectionCause(int causeID) throws IntegrationException {
        if(causeID == 0){
            throw new IntegrationException("cannot get cause with ID 0");
        }
        if(getOccInspectionCacheManager().isCachingEnabled()){
            return getOccInspectionCacheManager().getCacheCause().get(causeID, k -> fetchOccInspectionCause(causeID));
        } else {
            return fetchOccInspectionCause(causeID);
        }
        
    }

    /**
     * Extracts and builds an OccInspectionCause object from the DB
     * @param causeID
     * @return
     * @throws IntegrationException 
     */
    public OccInspectionCause fetchOccInspectionCause(int causeID) throws DatabaseFetchRuntimeException {
        OccInspectionCause cause = null;

        String query = """
                        SELECT causeid, title, description, notes, createdts, createdby_umapid, 
                       deactivatedby_umapid, lastupdatedts, deactivatedts, lastupdatedby_umapid, muni_municode
                         FROM public.occinspectioncause WHERE causeid=?;""";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {

            stmt = con.prepareStatement(query);
            stmt.setInt(1, causeID);
            rs = stmt.executeQuery();

            while (rs.next()) {
                cause = generateOccInspectionCause(rs);
            }

        } catch (SQLException | IntegrationException ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("Cannot get OccInspectionCause", ex);

        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 

        return cause;
    }
    
     /**
     * returns an OccInspection causes list
     * @param requireActive
     * @param muni
     * @return causeList
     * @throws IntegrationException 
     */
     public List<OccInspectionCause> getOccInspectionCauseListComplete(boolean requireActive, Municipality muni) throws IntegrationException {
         List<OccInspectionCause> causeList = new ArrayList<>();
         StringBuilder sb = new StringBuilder();
         sb.append("SELECT causeid FROM public.occinspectioncause WHERE causeid IS NOT NULL ");
         if(muni != null){
             sb.append("AND muni_municode=? ");
         }
         if (requireActive) {
             sb.append("AND deactivatedts is null ");
         }
         sb.append(";");
         Connection con = getPostgresCon();
         ResultSet rs = null;
         PreparedStatement stmt = null;

         try {
             stmt = con.prepareStatement(sb.toString());
             
             if(muni != null){
                 stmt.setInt(1, muni.getMuniCode());
             }
             
             rs = stmt.executeQuery();
             while (rs.next()) {
                 causeList.add(getOccInspectionCause(rs.getInt("causeid")));
             }
         } catch (SQLException ex) {
             System.out.println(ex.toString());
             throw new IntegrationException("Unable to generate OccInspectionCause(List)", ex);
         } finally {
             releasePostgresConnection(con, stmt, rs);
         } 
         return causeList;
     }

    /**
     * Generator of OccInspectionCause objects
     * @param rs
     * @return
     * @throws SQLException 
     */
    private OccInspectionCause generateOccInspectionCause(ResultSet rs) throws SQLException, IntegrationException {
        OccInspectionCause cause = new OccInspectionCause();
        SystemIntegrator si = getSystemIntegrator();
        MunicipalityCoordinator mc = getMuniCoordinator();
        try {
            cause.setCauseID(rs.getInt("causeid"));

            cause.setTitle(rs.getString("title"));
            cause.setDescription(rs.getString("description"));

            cause.setNotes(rs.getString("notes"));
            if(rs.getInt("muni_municode") != 0){
                cause.setMuni(mc.getMuni(rs.getInt("muni_municode")));
            }
            si.populateUMAPTrackedFields(cause, rs, true);
        } catch (SQLException | BObStatusException ex) {
            System.out.println("Cannot generate occ inspction cause" + ex.toString());
        }
        return cause;
    }

    /**
     * Updates a record in the OccInspectioncause table
     * @param cause
     * @throws IntegrationException 
     */
    public void updateOccInspectionCause(OccInspectionCause cause) throws IntegrationException {
        String query = """
                UPDATE public.occinspectioncause
                        	SET title=?, description=?, notes=?, lastupdatedts=now(), lastupdatedby_umapid=?, muni_municode=?
                        	WHERE causeid=?;
                """;
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);
            stmt.setString(1, cause.getTitle());
            stmt.setString(2, cause.getDescription());
            stmt.setString(3, cause.getNotes());

            if (Objects.nonNull(cause.getLastUpdatedby_UMAP())) {
                stmt.setInt(4, cause.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            
            if (Objects.nonNull(cause.getMuni())){
                stmt.setInt(5, cause.getMuni().getMuniCode());
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }

            stmt.setInt(6, cause.getCauseID());

            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update occinspectioncause record", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        }
    }

    /**
     * Removes a record from the occinspectioncause table
     * @param cause
     * @throws IntegrationException 
     */
    public void deleteCause(OccInspectionCause cause) throws IntegrationException {
        String query = "DELETE FROM public.occinspectioncause\n" + " WHERE causeid=?;";
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, cause.getCauseID());
            stmt.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot delete occ inspection cause--probably because another" + "part of the database has a reference item.", ex);
        } finally {
            releasePostgresConnection(con, stmt);

        } 
    }

    /**
     * Creates a new record in the occinspectioncause table
     * @param cause
     * @return
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public int insertOccInspectionCause(OccInspectionCause cause) throws IntegrationException, BObStatusException {
        if (Objects.isNull(cause)) {
            throw new BObStatusException("cannot insert null object into occinspection cause table");
        }
        String query = """
                INSERT INTO public.occinspectioncause(
                        	title, description, notes, 
                                createdts, createdby_umapid, deactivatedby_umapid, 
                                lastupdatedts, deactivatedts, lastupdatedby_umapid,
                                muni_municode)
                        	VALUES (?, ?, ?, 
                                        now(), ?, NULL, 
                                        now(), NULL, ?,
                                        ?);""";

        Connection con = null;
        ResultSet rs = null;
        PreparedStatement stmt = null;
        int newCauseID = 0;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);

            stmt.setString(1, cause.getTitle());
            stmt.setString(2, cause.getDescription());
            stmt.setString(3, cause.getNotes());
            if (Objects.nonNull(cause.getCreatedby_UMAP())) {
                stmt.setInt(4, cause.getCreatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }

            if (Objects.nonNull(cause.getLastUpdatedby_UMAP())) {
                stmt.setInt(5, cause.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            
            if (Objects.nonNull(cause.getMuni())){
                stmt.setInt(6, cause.getMuni().getMuniCode());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            stmt.execute();

            String retrievalQuery = "SELECT currval('occinspectioncause_causeid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();

            while (rs.next()) {
                newCauseID = rs.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert OccInspectionCause", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }
        cause.setCauseID(newCauseID);
        return newCauseID;
    }

    /**
     * Deactivate the records from occinspectedspaceelement, occinspectedspace tables.
     * @param spc
     * @throws IntegrationException 
     */
    public void deactivateInspectedSpaceData(OccInspectedSpace spc) throws IntegrationException {
        updateInspectedSpaceElementDeactivate(spc);
        updateInspectedspaceDeactivate(spc);
        
    }

    /**
     * Updates records in the occinspectedspaceelement table
     * @param spc
     * @throws IntegrationException 
     */
    public void updateInspectedSpaceElementDeactivate(OccInspectedSpace spc) throws IntegrationException {
        String sql = """
                     UPDATE public.occinspectedspaceelement
                        SET deactivatedts = now(), deactivatedby_userid = ?
                     WHERE inspectedspace_inspectedspaceid = ?;
                     """;
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);

            if(Objects.nonNull(spc.getDeactivatedBy())) {
                stmt.setInt(1, spc.getDeactivatedBy().getUserID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }

            stmt.setInt(2, spc.getInspectedSpaceID());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update inspected space element in the database, sorry!", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        }
        
    }

    /**
     * Updates records in the occinspectedspace table
     * @param spc
     * @throws IntegrationException
     */
    public void updateInspectedspaceDeactivate(OccInspectedSpace spc) throws IntegrationException {
        String sql = """
                     UPDATE public.occinspectedspace
                     SET deactivatedts = now(), deactivatedby_userid = ?
                     WHERE inspectedspaceid = ?;
                     """;
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);

            if(Objects.nonNull(spc.getDeactivatedBy())) {
                stmt.setInt(1, spc.getDeactivatedBy().getUserID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            stmt.setInt(2, spc.getInspectedSpaceID());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update inspected space element in the database, sorry!", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        }
    }
    
    /**
     * Single focal point of search method for field inspections using a
     * SearchParam subclass. Outsiders will use runQueryFieldInspection on the SearchCoordinator
     *
     * @param params
     * @return a list of FieldInspectionPropertyCasePeriodInfoHeavy
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public List<FieldInspectionLight> searchForFieldInspections(SearchParamsFieldInspection params) throws IntegrationException {
        SearchCoordinator sc = getSearchCoordinator();
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        List<FieldInspectionLight> finlight = new ArrayList<>();
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        params.appendSQL("""
                         SELECT DISTINCT occinspection.inspectionid FROM occinspection 
                         LEFT OUTER JOIN occinspectiondetermination ON (occinspection.determination_detid = occinspectiondetermination.determinationid) 
                         LEFT OUTER JOIN public.cecase ON (cecase.caseid = occinspection.cecase_caseid)
                         LEFT OUTER JOIN public.occperiod ON (occperiod.periodid = occinspection.occperiod_periodid)
                         LEFT OUTER JOIN public.parcelunit ON (parcelunit.unitid= occperiod.parcelunit_unitid)
                         LEFT OUTER JOIN public.parcel ON (cecase.parcel_parcelkey = parcel.parcelkey OR parcelunit.parcel_parcelkey = parcel.parcelkey) 
                         LEFT OUTER JOIN public.occinspectiondispatch ON (occinspection.inspectionid = occinspectiondispatch.inspection_inspectionid) 
                         WHERE occinspection.inspectionid IS NOT NULL  
                         """);

        // *******************************
        // **         BOb ID            **
        // *******************************
        if (!params.isBobID_ctl())
        {

            //*******************************
            // **   MUNI,DATES,USER,ACTIVE  **
            // *******************************
            params = (SearchParamsFieldInspection) sc.assembleBObSearchSQL_muniDatesUserActive(params,
                    SearchParamsFieldInspection.DBFIELD_MUNI,
                    SearchParamsFieldInspection.DBFIELD_ACTIVE);

            // *******************************
            // **     1. Determination      **
            // *******************************
            if (params.isDetermination_ctl())
            {
                if (params.isDetermination_val())
                {
                    params.appendSQL(" AND occinspection.determinationts IS NOT NULL ");
                } else
                {
                    params.appendSQL(" AND occinspection.determinationts IS NULL ");
                }
            }

            // *******************************
            // **     2.Re-inspection      **
            // *******************************
            if (params.isReinspection_ctl())
            {
                if (params.isReinspection_val())
                {
                    params.appendSQL(" AND occinspection.followupto_inspectionid IS NOT NULL ");
                } else
                {
                    params.appendSQL(" AND occinspection.followupto_inspectionid IS NULL ");

                }
            }

            // *******************************
            // **     3.Pass/Fail            **
            // *******************************
            if (params.isDt_qualifies_ctl())
            {
                if (params.isDt_qualifies_val())
                {
                    params.appendSQL(" AND occinspectiondetermination.qualifiesaspassed=true ");
                } else
                {
                    params.appendSQL(" AND occinspectiondetermination.qualifiesaspassed=false ");
                }
            }
            
            // *******************************
            // **     4.DOMAIN              **
            // *******************************
            if (params.isDomain_ctl())
            {
                if(params.getDomain_val() != null){
                    switch(params.getDomain_val()){
                        case CODE_ENFORCEMENT -> {
                            params.appendSQL(" AND cecase_caseid IS NOT NULL ");
                        }
                        case OCCUPANCY -> {
                            params.appendSQL(" AND occperiod_periodid IS NOT NULL ");
                        }
                        case PARCEL, UNIVERSAL -> {
                            params.setDomain_ctl(false);
                            params.appendToParamLog("Domain PARCEL and UNIVERSAL are invalid for Field Inspections; turning off domain ctl.");
                        }
                    
                    }
                } else {
                    // mis configured domain; turning off and logging
                    params.setDomain_ctl(false);
                    params.appendToParamLog("MISCONF: Found domain ctl without domain value; turning off filter!");
                }
            }
            
            
            // *******************************************
            // **     5. Field initiation (handheld app) **
            // *******************************************
            if(params.isFieldInit_ctl()){
                if(params.isFieldInitMuniOccPeriod_val()){
                    params.appendSQL(" AND occperiod_periodid=? ");
                } else { 
                    // I only want inspections NOT attached to the default muni occ period
                    params.appendSQL(" AND occperiod_periodid <> ? ");
                }
                if(params.isFieldInitNonNullLocDescr_val()){
                    params.appendSQL(" AND (mobilerawpropertystring IS NOT NULL OR mobilerawpropertyunitstring IS NOT NULL) ");
                } else {
                    // require null loc descriptors
                    params.appendSQL(" AND (mobilerawpropertystring NOT NULL AND mobilerawpropertyunitstring IS NOT NULL) ");
                }
            }
            
            
            // *******************************************
            // **     6. Field initiated routing status **
            // *******************************************
            
            if(params.isFieldRoutingStatus_ctl()){
                if(params.isFieldRoutingStatusRouted_val()){
                    // only routing complete
                    params.appendSQL(" AND fieldinitroutingts IS NOT NULL ");
                } else{
                    // routing is INCOMPLETE
                    params.appendSQL(" AND fieldinitroutingts IS NULL ");
                }
            }
            
            // *******************************************
            // **     7. Dispatch and sync status ********
            // *******************************************
            if(params.isFieldDispatch_ctl()){
                params.appendSQL(" AND dispatchid IS NOT NULL AND occinspectiondispatch.deactivatedts IS NULL ");
                if(params.isFieldDispatchRetrievedByMobile_ctl()){
                    if(params.isFieldDispatchRetrievedByMobile_val()){
                        // dispatched AND retrieved
                        params.appendSQL(" AND retrievalts IS NOT NULL ");
                    } else {
                        // dispatched but NOT YET RETRIVED
                        params.appendSQL(" AND retrievalts IS NULL ");
                    }
                }
                if(params.isFieldDispatchCycleComplete_ctl()){
                    if(params.isFieldDispatchCycleComplete_val()){
                        // these are dispatched FINS that have indeed been synchronized
                        params.appendSQL(" AND synchronizationts IS NOT NULL ");
                    } else {
                        // these are dispatched FINS that have not been synchronized
                        params.appendSQL(" AND synchronizationts IS NULL ");
                    }
                }
            }
            
        } else
        {
            params.appendSQL("inspectionid = ? "); // will be param 1 with ID search
        }

        int paramCounter = 0;

        try
        {
            stmt = con.prepareStatement(params.extractRawSQL());

            if (!params.isBobID_ctl()) {
                if (params.isMuni_ctl()) {
                    stmt.setInt(++paramCounter, params.getMuni_val().getMuniCode());
                }

                if (params.isDate_startEnd_ctl()) {
                    stmt.setTimestamp(++paramCounter, params.getDateStart_val_sql());
                    stmt.setTimestamp(++paramCounter, params.getDateEnd_val_sql());
                }

                if (params.isUser_ctl()) {
                    stmt.setInt(++paramCounter, params.getUser_val().getUserID());
                }
                
                if(params.isFieldInit_ctl() && params.isFieldInitMuniOccPeriod_val()){
                    stmt.setInt(++paramCounter, params.getFieldInitMuniOccPeriodID());
                }

            } else {
                stmt.setInt(++paramCounter, params.getBobID_val());
            }
            
            rs = stmt.executeQuery();

            int counter = 0;
            int maxResults;
            if (params.isLimitResultCount_ctl())
            {
                maxResults = params.getLimitResultCount_val();
            } else
            {
                maxResults = Integer.MAX_VALUE;
            }
            while (rs.next() && counter < maxResults)
            {
                FieldInspectionLight fi = oic.getOccInspectionLight(rs.getInt("inspectionid"));

                finlight.add(fi);

                counter++;
            }

        } catch (SQLException | IntegrationException | BObStatusException | BlobException ex)
        {
            throw new IntegrationException(ex.getMessage());

        } finally
        {
            releasePostgresConnection(con, stmt, rs);
        }

        return finlight;
    }

}