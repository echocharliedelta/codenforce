/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheClient;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.entities.BOBSource;
import com.tcvcog.tcvce.entities.County;
import com.tcvcog.tcvce.entities.Icon;
import com.tcvcog.tcvce.entities.IntensityClass;
import com.tcvcog.tcvce.entities.LinkedObjectRole;
import com.tcvcog.tcvce.entities.MuniProfile;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.PrintStyle;
import com.tcvcog.tcvce.entities.User;
import com.tcvcog.tcvce.entities.UserMuniAuthPeriod;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handles caching of municipality objects and a slew of random system objects
 * @author pierre15
 */
@Named("systemMuniUserCacheManager")
@ApplicationScoped
public class        SystemMuniUserCacheManager 
        implements  IFaceCacheManager {
    
    @Inject 
    private SystemCoordinator systemCoordinator;
    
    @Inject
    private PropertyCacheManager propertyCacheManager;
    
    // start with caching on
    private boolean cachingEnabled;

    // migrated from SystemIntegrator
    private Cache<Integer, BOBSource> cacheBObSource;
    private Cache<Integer, IntensityClass> cacheIntensityClass;
    private Cache<Integer, Icon> cacheIcon;
    private Cache<Integer, PrintStyle> cachePrintStyle;
    private Cache<Integer, LinkedObjectRole> cacheLinkedObjectRole;
    
    // migrated from MuniIntegrator
    private Cache<Integer, Municipality> cacheMuni;
    private Cache<Integer, MuniProfile> cacheMuniProfile;
    private Cache<Integer, County> cacheCounty;
    
    // from UserIntegrator
    private Cache<Integer, User> cacheUser;
    private Cache<Integer, UserMuniAuthPeriod> cacheUMAP;
    
    private final List<IFaceCacheClient> cacheClientList;
    
    public SystemMuniUserCacheManager(){
        cacheClientList = new ArrayList<>();
    }
    
    @PostConstruct
    public void initBean() {
        System.out.println("SystemMuniUserCacheManager.initBean");
        initCaching();
    }
    
    /**
     * Instantiates all our caches
     */
    @Override
    public void initCaching(){
        
        cacheBObSource = Caffeine.newBuilder().maximumSize(50).softValues().build();
        cacheIntensityClass = Caffeine.newBuilder().maximumSize(50).softValues().build();
        cacheIcon = Caffeine.newBuilder().maximumSize(250).softValues().build();
        cachePrintStyle = Caffeine.newBuilder().maximumSize(50).softValues().build();
        cacheLinkedObjectRole = Caffeine.newBuilder().maximumSize(250).softValues().build();
        
        cacheMuni = Caffeine.newBuilder().maximumSize(30).softValues().build();
        cacheMuniProfile = Caffeine.newBuilder().maximumSize(30).softValues().build();
        cacheCounty = Caffeine.newBuilder().maximumSize(10).softValues().build();
        
        cacheUser = Caffeine.newBuilder().maximumSize(250).softValues().build();
        cacheUMAP = Caffeine.newBuilder().maximumSize(750).softValues().build();
    }
    
    
    
    @Override
    public void registerCacheClient(IFaceCacheClient client) {
        if(getCacheClientList() != null && !cacheClientList.contains(client) && client != null){
            systemCoordinator.preRegistrationconfigureCacheClient(client);
            getCacheClientList().add(client);
        }
    }

    @Override
    public void removeCacheClient(IFaceCacheClient client) {
        if(getCacheClientList() != null && getCacheClientList().contains(client)){
            getCacheClientList().remove(client);
        }
        
    }
    
    
    /**
     * Prints stats
     */
    @Override
    public void writeCacheStats(){
    
        if(getCacheBObSource() != null) {
            System.out.println("cacheBobSource size: " + cacheBObSource.estimatedSize());
        }
        if(getCacheIntensityClass() != null) {
            System.out.println("cacheIntensityClass size: " + cacheIntensityClass.estimatedSize());
        }
        if(getCacheIcon() != null) {
            System.out.println("cacheIcon size: " + cacheIcon.estimatedSize());
        }
        if(getCachePrintStyle() != null) {
            System.out.println("cachePrintStyle size: " + cachePrintStyle.estimatedSize());
        }
        if(getCacheLinkedObjectRole() != null) {
            System.out.println("cacheLinkedObjectRole size: " + cacheLinkedObjectRole.estimatedSize());
        }
        if(getCacheMuni() != null) {
            System.out.println("cacheMuni size: " + cacheMuni.estimatedSize());
        }
        if(getCacheMuniProfile() != null) {
            System.out.println("cacheMuniProfile size: " + cacheMuniProfile.estimatedSize());
        }
        if(getCacheCounty() != null) {
            System.out.println("cacheCounty size: " + cacheCounty.estimatedSize());
        }
        if(getCacheUser() != null) {
            System.out.println("cacheUser size: " + cacheUser.estimatedSize());
        }
        if(getCacheUMAP() != null) {
            System.out.println("cacheUmap size: " + cacheUMAP.estimatedSize());
        }
        
    }
    
    /**************************************************************************
    ******************              FLUSHING                *******************
    ***************************************************************************/
    
    /**
     * Class specific flushing
     * @param source 
     */
    public void flush(BOBSource source){
        systemCoordinator.flushAllSystemCaches();
    }
    
    /**
     * Class specific flushing
     * @param clz
     */
    public void flush(IntensityClass clz){
        systemCoordinator.flushAllSystemCaches();
        
    }
    
    /**
     * Class specific flushing
     * @param icon
     */
    public void flush(Icon icon){
        systemCoordinator.flushAllSystemCaches();
        
    }
    
    /**
     * Class specific flushing
     * @param style
     */
    public void flush(PrintStyle style){
        systemCoordinator.flushAllSystemCaches();
        
    }
    
    /**
     * Class specific flushing
     * @param muni
     */
    public void flush(LinkedObjectRole muni){
        systemCoordinator.flushAllSystemCaches();
        
    }
    
    /**
     * Class specific flushing
     * @param muni
     */
    public void flush(Municipality muni){
        systemCoordinator.flushAllSystemCaches();
        
    }
    
    /**
     * Class specific flushing
     * @param profile
     */
    public void flush(MuniProfile profile){
        systemCoordinator.flushAllSystemCaches();
        
    }
    
    /**
     * Class specific flushing
     * @param cnty
     */
    public void flush(County cnty){
        propertyCacheManager.flushAllCaches();
    }
    
    /**
     * Class specific flushing
     * @param usr
     */
    public void flush(User usr){
        systemCoordinator.flushAllSystemCaches();
    }
    
    /**
     * Class specific flushing
     * @param umap
     */
    public void flush(UserMuniAuthPeriod umap){
        systemCoordinator.flushAllSystemCaches();
    }
    
    
    /**
     * 
     * Dumps all my caches
     */
    @Override
    public void flushAllCaches(){
        System.out.println("SystemMuniUserCacheManager.flushAllCaches");
        if(getCacheBObSource() != null) {
            getCacheBObSource().invalidateAll();
        }
        if(getCacheIntensityClass() != null) {
            getCacheIntensityClass().invalidateAll();
        }
        if(getCacheIcon() != null) {
            getCacheIcon().invalidateAll();
        }
        if(getCachePrintStyle() != null) {
            getCachePrintStyle().invalidateAll();
        }
        if(getCacheLinkedObjectRole() != null) {
            getCacheLinkedObjectRole().invalidateAll();
        }
        if(getCacheMuni() != null) {
            getCacheMuni().invalidateAll();
        }
        if(getCacheMuniProfile() != null) {
            getCacheMuniProfile().invalidateAll();
        }
        if(getCacheCounty() != null) {
            getCacheCounty().invalidateAll();
        }
        if(getCacheUser() != null) {
            getCacheUser().invalidateAll();
        }
        if(getCacheUMAP() != null) {
            getCacheUMAP().invalidateAll();
        }
    }
    

    @Override
    public void flushObjectFromCache(IFaceCachable cable) {
        if(cable != null){
            flushKey(cable.getCacheKey());
        }
    }
    
    @Override
    public void flushObjectFromCache(int cacheKey) {
        flushKey(cacheKey);
    }
    
    private void flushKey(int key){
       if(getCacheBObSource() != null) {
            getCacheBObSource().invalidate(key);
        }
        if(getCacheIntensityClass() != null) {
            getCacheIntensityClass().invalidate(key);
        }
        if(getCacheIcon() != null) {
            getCacheIcon().invalidate(key);
        }
        if(getCachePrintStyle() != null) {
            getCachePrintStyle().invalidate(key);
        }
        if(getCacheLinkedObjectRole() != null) {
            getCacheLinkedObjectRole().invalidate(key);
        }
        if(getCacheMuni() != null) {
            getCacheMuni().invalidate(key);
        }
        if(getCacheMuniProfile() != null) {
            getCacheMuniProfile().invalidate(key);
        }
        if(getCacheCounty() != null) {
            getCacheCounty().invalidate(key);
        }
        if(getCacheUser() != null) {
            getCacheUser().invalidate(key);
        }
        if(getCacheUMAP() != null) {
            getCacheUMAP().invalidate(key);
        }
    }

    
    
    /**************************************************************************
    ******************           GETTERS/SETTERS            *******************
    ***************************************************************************/

    @Override
    public boolean isCachingEnabled() {
        return cachingEnabled;
    }

    @Override
    public void disableClientCaching() {
        cachingEnabled = false;
    }

    @Override
    public void enableClientCaching() {
        cachingEnabled = true;
    }


    /**
     * @return the cacheClientList
     */
    public List<IFaceCacheClient> getCacheClientList() {
        return cacheClientList;
    }

    /**
     * @return the cacheBObSource
     */
    public Cache<Integer, BOBSource> getCacheBObSource() {
        return cacheBObSource;
    }

    /**
     * @return the cacheIntensityClass
     */
    public Cache<Integer, IntensityClass> getCacheIntensityClass() {
        return cacheIntensityClass;
    }

    /**
     * @return the cacheIcon
     */
    public Cache<Integer, Icon> getCacheIcon() {
        return cacheIcon;
    }

    /**
     * @return the cachePrintStyle
     */
    public Cache<Integer, PrintStyle> getCachePrintStyle() {
        return cachePrintStyle;
    }

    /**
     * @return the cacheLinkedObjectRole
     */
    public Cache<Integer, LinkedObjectRole> getCacheLinkedObjectRole() {
        return cacheLinkedObjectRole;
    }

    /**
     * @return the cacheMuni
     */
    public Cache<Integer, Municipality> getCacheMuni() {
        return cacheMuni;
    }

    /**
     * @return the cacheMuniProfile
     */
    public Cache<Integer, MuniProfile> getCacheMuniProfile() {
        return cacheMuniProfile;
    }

    /**
     * @return the cacheCounty
     */
    public Cache<Integer, County> getCacheCounty() {
        return cacheCounty;
    }

    /**
     * @return the cacheUser
     */
    public Cache<Integer, User> getCacheUser() {
        return cacheUser;
    }

    /**
     * @return the cacheUMAP
     */
    public Cache<Integer, UserMuniAuthPeriod> getCacheUMAP() {
        return cacheUMAP;
    }
    
    
}
