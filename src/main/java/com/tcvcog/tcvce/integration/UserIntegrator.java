/*
 * Copyright (C) 2017 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.coordinators.BlobCoordinator;
import com.tcvcog.tcvce.coordinators.MunicipalityCoordinator;
import com.tcvcog.tcvce.coordinators.PersonCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.coordinators.UserCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.domain.MetadataException;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.Person;
import com.tcvcog.tcvce.entities.RoleType;
import com.tcvcog.tcvce.entities.User;
import com.tcvcog.tcvce.entities.UserMuniAuthPeriodLogEntry;
import com.tcvcog.tcvce.entities.UserMuniAuthPeriod;
import com.tcvcog.tcvce.entities.UserAuthorized;
import com.tcvcog.tcvce.entities.UserMuniAuthPeriodInsertion;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheClient;
import com.tcvcog.tcvce.domain.DatabaseFetchRuntimeException;
import com.tcvcog.tcvce.entities.Human;
import jakarta.annotation.PostConstruct;


/**
 * The link between user related tables, most notably login and loginmuniauthperiod
 * and the object world. ONLY called by UserCoordinator classes (in ideal form)
 * 
 * 
 * @author ellen bascomb of apt 31y
 */
public class        UserIntegrator 
        extends     BackingBeanUtils 
        implements  Serializable, IFaceCacheClient {

  
    
    
      @PostConstruct
    public void initBean() {
        
        registerClientWithManager();
        
    }
    
    @Override
    public void registerClientWithManager() {
        getSystemMuniCacheManager().registerCacheClient(this);
    }
    
    
  
    /**
     * Creates a new instance of UserIntegrator
     */
    public UserIntegrator() {
        
    }
    
    
    
    /**
     * Cache-backed getter for users!!! Hurrah!
     * @param userID
     * @return
     * @throws IntegrationException
     * @throws DatabaseFetchRuntimeException 
     */
    public User getUser(int userID) throws IntegrationException, DatabaseFetchRuntimeException{
        if(userID == 0){
            throw new IntegrationException("Cannot fetch user with ID == 0");
        }
        if(getSystemMuniCacheManager().isCachingEnabled()){
            return getSystemMuniCacheManager().getCacheUser().get(userID, k -> fetchUser(userID));
        } else {
            return fetchUser(userID);
        }
    }
   
   
    
    /**
     *
     * Builds a user object given an ID
     * @param userID
     * @return
     * @throws IntegrationException 
     */
    public User fetchUser(int userID) throws DatabaseFetchRuntimeException{
        Connection con = getPostgresCon();
        ResultSet rs = null;
        User newUser = null;
        
        String query =  """
                         SELECT userid, username, password, notes, pswdlastupdated, 
                               forcepasswordreset, createdby_userid, createdts, nologinvirtualonly, 
                               deactivatedts, deactivatedby_userid, lastupdatedts, userrole, 
                               homemuni, humanlink_humanid, lastupdatedby_userid, forcepasswordresetby_userid, signature_photodocid
                          FROM public.login WHERE userid = ?;
                        """;   
        
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, userID);
            rs = stmt.executeQuery();
             while(rs.next()){
                newUser = generateUser(rs);
            }
            
        } catch (SQLException | IntegrationException | BObStatusException ex) {
            System.out.println(ex);
            throw new DatabaseFetchRuntimeException("Error getting user", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        
        return newUser;
    }
    
    
      /**
     * Extracts complete table dump of the login table
     * 
     * @return IDs of all users in the login table
     * @throws IntegrationException 
     */
    public List<Integer> getUserListComplete() throws IntegrationException{
        
        
        Connection con = getPostgresCon();
        ResultSet rs = null;
        // broken query
        String query =  "SELECT userid FROM public.login;";
        
        List<Integer> idl = new ArrayList<>();
        
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();
            while(rs.next()){
                idl.add(rs.getInt("userid"));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Error getting user", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        System.out.println("UserIntegrator.getUserListComplete: Returning list size "+ idl.toString());
        return idl;
    }
    
    /**
     * Data integrity utility that will return a non-null list of UserIDs
     * who are linked to the given human.I check the humanlink_humanid field
     * of the login table.
     * 
     * @param h who you would like to search for linking users
     * @return if not null and therefore not empty, the given human is linked to these users; If
     * null is returned, there are no current human links to the given human in the login table
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public List<Integer> getUserIDsOfUsersLinkedToHuman(Human h) throws IntegrationException{
        if(h == null){
            throw new IntegrationException("cannot look for linked humans with null incoming human");
        }
        Connection con = getPostgresCon();
        ResultSet rs = null;
        // broken query
        String query =  "SELECT userid FROM public.login WHERE humanlink_humanid=?;";
        
        List<Integer> idl = new ArrayList<>();
        
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, h.getHumanID());
            rs = stmt.executeQuery();
            while(rs.next()){
                idl.add(rs.getInt("userid"));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Error getting user", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        if(idl.isEmpty()){
            return null;
        }
        return idl;
    }
    
    
    
     /**
     * Note that the client method is responsible for moving the cursor on the 
     * result set object before passing it into this method     * 
     * @param rs
     * @return
     * @throws IntegrationException 
     */
    private User generateUser(ResultSet rs) throws IntegrationException, SQLException, BObStatusException{
        User user = new User();
        PersonCoordinator pc = getPersonCoordinator();
        SystemIntegrator si = getSystemIntegrator();
        
            // line 1 of SELECT
            user.setUserID(rs.getInt("userid"));
            user.setUsername(rs.getString("username"));
            user.setNotes(rs.getString("notes"));
         
            // line 2 of SELECT
            user.setCreatedByUserId(rs.getInt("createdby_userid"));
            if(rs.getTimestamp("createdts") != null){
                user.setCreatedTS(rs.getTimestamp("createdts").toLocalDateTime());
            }
            user.setLastUpdatedByUserID(rs.getInt("lastupdatedby_userid"));
            
            user.setNoLoginVirtualUser(rs.getBoolean("nologinvirtualonly"));
            
            // line 3 of SELECT
             if(rs.getTimestamp("deactivatedts") != null){
                user.setDeactivatedTS(rs.getTimestamp("deactivatedts").toLocalDateTime());   
            }
              
            user.setDeactivatedByUserID(rs.getInt("deactivatedby_userid"));
            if(rs.getTimestamp("lastupdatedts") != null){
                user.setLastUpdatedTS(rs.getTimestamp("lastupdatedts").toLocalDateTime());
            }
            
             if(rs.getInt("homemuni") != 0){
                user.setHomeMuniID(rs.getInt("homemuni"));
            }
            user.setHumanID(rs.getInt("humanlink_humanid"));
            user.setSignatureBlobID(rs.getInt("signature_photodocid"));
            
            if(rs.getTimestamp("forcepasswordreset") != null){
                user.setForcePasswordResetTS(rs.getTimestamp("forcepasswordreset").toLocalDateTime());
            }
            if(rs.getInt("forcepasswordresetby_userid") != 0){
                user.setForcedPasswordResetByUserID(rs.getInt("forcepasswordresetby_userid"));
            }
            
         return user;
    }
    
    /**
     *
     * Creates sekeletonized UserAuthorized
     * 
     * @param usr
     * @return null returned with null input
     * @throws IntegrationException 
     */
    public UserAuthorized getUserAuthorizedNoAuthPeriods(User usr) throws IntegrationException, BObStatusException{
        if(usr == null){
            System.out.println("UserIntegrator.getUANoUMAPs: null User passed in");
            return null;
        }
        
        UserCoordinator uc = getUserCoordinator();
        
        Connection con = getPostgresCon();
        ResultSet rs = null;
        // broken query
        String query =  " SELECT userid, pswdlastupdated, forcepasswordreset, forcepasswordresetby_userid, createdby_userid, createdts, signature_photodocid " +
                        " FROM public.login WHERE userid = ?;";
        
        UserAuthorized ua = null;
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, usr.getUserID());
            rs = stmt.executeQuery();
            while(rs.next()){
                ua = new UserAuthorized(uc.user_getUser(usr.getUserID()));
                ua = generateUserAuthorized(ua, rs);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Error generating UserAuthorized", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        
        return ua;
    }
    
    /**
     * Special adaptor method for getUserAuthorizedNoAuthPeriods which requires a User object.Used to generate a robot UserAuthorized for Sylvia
     * 
     * @param umapID
     * @return the UserAuthorized without UMAPs
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public UserAuthorized getUserAuthorizedByUMAPID(int umapID) throws IntegrationException, BObStatusException{
        
        UserCoordinator uc = getUserCoordinator();
        Connection con = getPostgresCon();
        ResultSet rs = null;
        // broken query
        String query =  " SELECT authuser_userid FROM public.loginmuniauthperiod WHERE muniauthperiodid= ?;";
        
        UserAuthorized ua = null;
        PreparedStatement stmt = null;
        int userID = 0;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, umapID);
            rs = stmt.executeQuery();
            while(rs.next()){
                userID = rs.getInt("authuser_userid");
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Error retrieving userID by UMAP", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        return getUserAuthorizedNoAuthPeriods(uc.user_getUser(userID));
        
    }
    
    /**
     * Generator method for UserAuthorized objects
     * @param ua
     * @param rs
     * @return
     * @throws SQLException 
     */
    private UserAuthorized generateUserAuthorized(UserAuthorized ua, ResultSet rs) throws SQLException{
        
            if(rs.getTimestamp("pswdlastupdated") != null){
                ua.setPswdLastUpdated(rs.getTimestamp("pswdlastupdated").toLocalDateTime());
            }
            if(rs.getTimestamp("forcepasswordreset") != null){
                ua.setForcePasswordResetTS(rs.getTimestamp("forcepasswordreset").toLocalDateTime());
            }
            if(rs.getInt("forcepasswordresetby_userid") != 0){
                ua.setForcePasswordResetByUserID(rs.getInt("forcepasswordresetby_userid"));
            }
            ua.setCreatedByUserId(rs.getInt("createdby_userid"));
            if(rs.getTimestamp("createdts") != null){
                ua.setCreatedTS(rs.getTimestamp("createdts").toLocalDateTime());
            }
            ua.setSignatureBlobID(rs.getInt("signature_photodocid"));
            
            BlobCoordinator bc = getBlobCoordinator();
        try {
            ua.setBlobList(bc.getBlobLightList(ua));
            if(ua.getSignatureBlobID() != 0){
                ua.setSignatureBlob(bc.getBlobLight(ua.getSignatureBlobID()));
            }
            
        } catch (BlobException | BObStatusException | IntegrationException ex) {
            throw new SQLException(ex.getMessage());
        }
        
        return ua;
    }
    
    /**
     * Provides a complete list of records by User in the table loginmuniauthperiod.
     * Client is responsible for validating the UMAP object by the UserCoordinator
     * @param userID
     * @return
     * @throws IntegrationException 
     */
    public List<UserMuniAuthPeriod> getUserMuniAuthPeriodsRaw(int userID) throws IntegrationException {
        Connection con = getPostgresCon();
        ResultSet rs = null;
        List<UserMuniAuthPeriod> perList = null;
        // broken query
        String query = "SELECT muniauthperiodid FROM public.loginmuniauthperiod WHERE authuser_userid=?;";
        
        PreparedStatement stmt = null;
        if(userID != 0){
            perList = new ArrayList<>();
            try {
                stmt = con.prepareStatement(query);
                stmt.setInt(1, userID);
                rs = stmt.executeQuery();
                while(rs.next()){
                    perList.add(getUserMuniAuthPeriod(rs.getInt("muniauthperiodid")));
                }

            } catch (SQLException ex) {
                System.out.println(ex);
                throw new IntegrationException("UserIntegrator.getUserMuniAuthPeriodsRaw: Error getting user access record", ex);
            } finally{
                 if (stmt != null){ try { stmt.close(); } catch (SQLException ex) {/* ignored */ } }
                 if (rs != null) { try { rs.close(); } catch (SQLException ex) { /* ignored */ } }
                 if (con != null) { try { con.close(); } catch (SQLException e) { /* ignored */} }
            } 
        }
        return perList;
    }
    
    /**
     * For use by system administrators to manage user data. Raw means that even 
     * expired or invalidated periods are STILL included. Clients can 
     * use methods on the UserCoordinator to clean periods.
     * @param m
     * @return
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public List<UserMuniAuthPeriod> getUserMuniAuthPeriodsRaw(Municipality m) throws IntegrationException, AuthorizationException{
        Connection con = getPostgresCon();
        ResultSet rs = null;
        List<UserMuniAuthPeriod> umapList = new ArrayList<>();
        
       String query =  "SELECT muniauthperiodid " +
                        "FROM public.loginmuniauthperiod WHERE muni_municode=?;";
        
        PreparedStatement stmt = null;
        
        try {
                 
            stmt = con.prepareStatement(query);
            stmt.setInt(1, m.getMuniCode());
            rs = stmt.executeQuery();
            while(rs.next()){
                umapList.add(getUserMuniAuthPeriod(rs.getInt("muniauthperiodid")));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Error fetching user list", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        
        return umapList;
        
    }
    
    /**
     * Cache-backed getter for UMAPS
     * @param periodID
     * @return
     * @throws IntegrationException 
     */
    public UserMuniAuthPeriod getUserMuniAuthPeriod(int periodID) throws IntegrationException {
        if(periodID == 0){
            throw new IntegrationException("Cannot fetch a UMAP of ID 0");
        }
        if(getSystemMuniCacheManager().isCachingEnabled()){
            return getSystemMuniCacheManager().getCacheUMAP().get(periodID, k -> fetchUserMuniAuthPeriod(periodID));
        } else {
            return fetchUserMuniAuthPeriod(periodID);
        }
    
    }
    
    /**
     * Extracts a single UMAP from the DB
     * @param periodID to retrive
     * @return the UMAP
     * @throws DatabaseFetchRuntimeException
     */
    public UserMuniAuthPeriod fetchUserMuniAuthPeriod(int periodID) throws DatabaseFetchRuntimeException {
        
        
        Connection con = getPostgresCon();
        ResultSet rs = null;
        UserMuniAuthPeriod per = null;
        // broken query
        String query = """
                       SELECT muniauthperiodid, muni_municode, authuser_userid, accessgranteddatestart, 
                            accessgranteddatestop, recorddeactivatedts, authorizedrole, loginmuniauthperiod.createdts, 
                            loginmuniauthperiod.createdby_userid, loginmuniauthperiod.notes, supportassignedby, 
                            assignmentrank, oathts, oathcourtentity_entityid, oathdoc_blobid, codeofficer, 
                            municipality.profile_profileid  
                        FROM public.loginmuniauthperiod 
                            INNER JOIN public.municipality ON loginmuniauthperiod.muni_municode = municipality.municode 
                        WHERE muniauthperiodid=?
                       """;
        
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, periodID);
            rs = stmt.executeQuery();
            while(rs.next()){
                per = generateUserMuniAuthPeriod(rs);
            }
            
        } catch (SQLException | BlobException | IntegrationException | BObStatusException ex) {
            System.out.println(ex);
            throw new DatabaseFetchRuntimeException("UserIntegrator.getUserMuniAuthPeriod | Error getting user access record", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        
        return per;
    }
    
    /**
     * SECURITY CRITICAL:
     * Internal generator for UMAPS
     * @param rs of all cols of the loginmuniauthperiod table
     * @return a fully baked object
     * @throws SQLException
     * @throws IntegrationException, perhaps due to getBlob throwing a MetaData exception
     */
    private UserMuniAuthPeriod generateUserMuniAuthPeriod(ResultSet rs) throws SQLException, IntegrationException, BlobException, BObStatusException {
        MunicipalityCoordinator mc = getMuniCoordinator();
        
        
        // our fields of muni, rank, and officer status are final therefore must be injected here at instantiation
        // for protection against accidental or malicious modification downstream
        UserMuniAuthPeriod per = new UserMuniAuthPeriod(mc.getMuni(rs.getInt("muni_municode")), 
                                                        RoleType.valueOf(rs.getString("authorizedrole")), 
                                                        rs.getBoolean("codeofficer"));
        
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        BlobCoordinator bc = getBlobCoordinator();
        
        per.setUserMuniAuthPeriodID(rs.getInt("muniauthperiodid"));
        
        // I guess we're going to inject the muni profile here since these UMAPs aren't 
        // constructed and configured in the standard way: the coordinator should be doing this
        // but several coordinator methods call this getter, and they're on the other side of the 
        // cache, and I'd like this to be cached.
        per.setMuniProfile(mc.getMuniProfile(rs.getInt("profile_profileid")));
        
        
        per.setUserID(rs.getInt("authuser_userid"));
        
        if(rs.getTimestamp("accessgranteddatestart") != null){
            per.setStartDate(rs.getTimestamp("accessgranteddatestart").toLocalDateTime());
        }
        if(rs.getTimestamp("accessgranteddatestop") != null){
            per.setStopDate(rs.getTimestamp("accessgranteddatestop").toLocalDateTime());
        }
        if(rs.getTimestamp("recorddeactivatedts") != null){
            per.setRecorddeactivatedTS(rs.getTimestamp("recorddeactivatedts").toLocalDateTime());
        }
        
        if(rs.getTimestamp("createdts") != null){
            per.setCreatedTS(rs.getTimestamp("createdts").toLocalDateTime());
        }
        per.setCreatedByUserID(rs.getInt("createdby_userid"));
        
        per.setNotes(rs.getString("notes"));
        // do support stuff later
        per.setAssignmentRelativeOrder(rs.getInt("assignmentrank"));
        
        if(rs.getTimestamp("oathts") != null){
            per.setOathTS(rs.getTimestamp("oathts").toLocalDateTime());
        }
        if(rs.getInt("oathcourtentity_entityid") != 0){
            per.setOathCourtEntityID(rs.getInt("oathcourtentity_entityid"));
        }
        
        if(rs.getInt("oathdoc_blobid") != 0){
            try {
                per.setOathBlob(bc.getBlob(rs.getInt("oathdoc_blobid")));
            } catch (MetadataException ex) {
                throw new IntegrationException(ex.getMessage());
            }
        }
        
        return per;
    }
    
    
    public void insertUserMuniAuthPeriodLogEntry(UserMuniAuthPeriodLogEntry entry) throws IntegrationException, AuthorizationException{
        Connection con = getPostgresCon();
        ResultSet rs = null;
        UserMuniAuthPeriod per = null;
        // broken query
        String query =  "INSERT INTO public.loginmuniauthperiodlog(\n" +
                        "            authperiodlogentryid, authperiod_periodid, category, entryts, \n" +
                        "            entrydateofrecord, disputedby_userid, disputedts, notes, cookie_jsessionid, \n" +
                        "            header_remoteaddr, header_useragent, header_dateraw, header_date, \n" +
                        "            header_cachectl, audit_usersession_userid, audit_usercredential_userid, \n" +
                        "            audit_muni_municode)\n" +
                        "    VALUES (DEFAULT, ?, ?, now(), \n" +
                        "            now(), ?, ?, ?, ?, \n" +
                        "            ?, ?, ?, NULL, \n" +
                        "            ?, ?, ?, \n" +
                        "            ?);";
        
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            
            stmt.setInt(1, entry.getUserMuniAuthPeriodID());
            stmt.setString(2, entry.getCategory());
            // entry ts and dateofrecord set by PG's now()
            
            if(entry.getDisputedByUserID()== 0 && entry.getDisputedts() == null){
                stmt.setNull(3, java.sql.Types.NULL);
                stmt.setNull(4, java.sql.Types.NULL);
            } else {
                throw new AuthorizationException("Cannot insert a CredentialLogEntry with not null disputed fields!");
            }
            stmt.setString(5, entry.getNotes());
            stmt.setString(6, entry.getCookie_jsessionid());
            
            stmt.setString(7, entry.getHeader_remoteaddr());
            stmt.setString(8, entry.getHeader_useragent());
            stmt.setString(9,  entry.getHeader_dateraw());
            
            //header date java type set to NULL in SQL
            stmt.setString(10, entry.getHeader_cachectl());
            stmt.setInt(11, entry.getAudit_usersession_userid());
            stmt.setInt(12, entry.getAudit_usercredential_userid());
            
            stmt.setInt(13, entry.getAudit_muni_municode());
            
            stmt.execute();
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("UserIntegrator.insertUserMuniAuthPeriodLogEntry| Error getting user access record", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
    }
    
    /**
     * Extracts all UMAP logs by UMAP for individual user record keeping
     * @param uap
     * @return
     * @throws IntegrationException 
     */
    public List<UserMuniAuthPeriodLogEntry> getMuniAuthPeriodLogEntryList(UserMuniAuthPeriod uap) throws IntegrationException{
        Connection con = getPostgresCon();
        ResultSet rs = null;
        List<UserMuniAuthPeriodLogEntry> uacleList = new ArrayList<>();
        // broken query
        String query = "SELECT authperiodlogentryid" +
                        "  FROM public.loginmuniauthperiodlog WHERE authperiod_periodid=?;";
        
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, uap.getUserMuniAuthPeriodID());
            rs = stmt.executeQuery();
            while(rs.next()){
                uacleList.add(getUserMuniAuthPeriodLogEntry(rs.getInt("authperiodlogentryid")));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("UserIntegrator.getMuniAuthPeriodLogEntryList | Error getting user access record", ex);
        } finally{
             if (stmt != null){ try { stmt.close(); } catch (SQLException ex) {/* ignored */ } }
             if (rs != null) { try { rs.close(); } catch (SQLException ex) { /* ignored */ } }
             if (con != null) { try { con.close(); } catch (SQLException e) { /* ignored */} }
        } 

        return uacleList;
    }
    
    /**
     * Extracts all UMAP logs from the system from a start date until an end date
     * @param start
     * @param end
     * @return
     * @throws IntegrationException 
     */
    public List<UserMuniAuthPeriodLogEntry> getMuniAuthPeriodLogEntryList(LocalDateTime start, LocalDateTime end) throws IntegrationException{
        if(start == null || end == null ){
            throw new IntegrationException("Start and end date required for extracting UMAP list");
            
        }
        Connection con = getPostgresCon();
        ResultSet rs = null;
        List<UserMuniAuthPeriodLogEntry> uacleList = new ArrayList<>();
        // broken query
        String query = """
                       SELECT authperiodlogentryid FROM public.loginmuniauthperiodlog 
                       WHERE entrydateofrecord >= ? and entrydateofrecord <= ?;""";
        
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setTimestamp(1, java.sql.Timestamp.valueOf(start));
            stmt.setTimestamp(2, java.sql.Timestamp.valueOf(end));
            rs = stmt.executeQuery();
            while(rs.next()){
                uacleList.add(getUserMuniAuthPeriodLogEntry(rs.getInt("authperiodlogentryid")));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("UserIntegrator.getMuniAuthPeriodLogEntryList from start to end date | Error getting user access records", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 

        return uacleList;
    }
    
    
    public UserMuniAuthPeriodLogEntry getUserMuniAuthPeriodLogEntry(int logEntryID) throws IntegrationException{
        Connection con = getPostgresCon();
        ResultSet rs = null;
        UserMuniAuthPeriodLogEntry uacle = null;
        // broken query
        String query = """
                            SELECT authperiodlogentryid, authperiod_periodid, category, entryts, 
                         	  entrydateofrecord, disputedby_userid, disputedts, loginmuniauthperiodlog.notes, cookie_jsessionid, 
                         	  header_remoteaddr, header_useragent, header_dateraw, header_date, 
                         	  header_cachectl, audit_usersession_userid, audit_usercredential_userid, 
                         	  audit_muni_municode,
                         	  login.userid,
                         	  login.username,
                         	  municipality.muniname,
                         	  loginmuniauthperiod.authorizedrole,
                         	  loginmuniauthperiod.codeofficer
                             FROM public.loginmuniauthperiodlog
                          	INNER JOIN public.loginmuniauthperiod ON (loginmuniauthperiodlog.authperiod_periodid = loginmuniauthperiod.muniauthperiodid)
                          	INNER JOIN public.municipality ON (loginmuniauthperiod.muni_municode = municipality.municode)
                          	INNER JOIN public.login ON (loginmuniauthperiod.authuser_userid = login.userid)
                            WHERE authperiodlogentryid=?;
                       """;
        
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, logEntryID);
            rs = stmt.executeQuery();
            while(rs.next()){
                uacle = generateMuniAuthPeriodLogEntry(rs);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("UserIntegrator.getUserMuniAuthPeriodLogEntry | Error getting user access record", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 

        return uacle;
        
    }
    
    /**
     * Generator method for objects representing an entry in the user auth log
     * @param rs
     * @return
     * @throws SQLException
     * @throws IntegrationException 
     */
    private UserMuniAuthPeriodLogEntry generateMuniAuthPeriodLogEntry(ResultSet rs) throws SQLException, IntegrationException{
        UserMuniAuthPeriodLogEntry uacle = new UserMuniAuthPeriodLogEntry();
        
        uacle.setLogBookEntryID(rs.getInt("authperiodlogentryid"));
        uacle.setUserMuniAuthPeriodID(rs.getInt("authperiod_periodid"));
        uacle.setCategory(rs.getString("category"));
        
        if(rs.getTimestamp("entrydateofrecord") != null){
            uacle.setEntryTS(rs.getTimestamp("entrydateofrecord").toLocalDateTime());
        }
        uacle.setDisputedByUserID(rs.getInt("disputedby_userid"));
        if(rs.getTimestamp("disputedts") != null){
            uacle.setDisputedts(rs.getTimestamp("disputedts").toLocalDateTime());
        }
        uacle.setNotes(rs.getString("notes"));
        uacle.setCookie_jsessionid(rs.getString("cookie_jsessionid"));
        
        uacle.setHeader_remoteaddr(rs.getString("header_remoteaddr"));
        uacle.setHeader_useragent(rs.getString("header_useragent"));
        uacle.setHeader_dateraw(rs.getString("header_dateraw"));
        if(rs.getTimestamp("header_date") != null){
            uacle.setHeader_date(rs.getTimestamp("header_date").toLocalDateTime());
        } else {
            uacle.setHeader_date(null);
        }
        
        uacle.setHeader_cachectl(rs.getString("header_cachectl"));
        uacle.setAudit_usersession_userid(rs.getInt("audit_usersession_userid"));
        uacle.setAudit_usercredential_userid(rs.getInt("audit_usercredential_userid"));
        
        uacle.setAudit_muni_municode(rs.getInt("audit_muni_municode"));
        
        // new fields for system-wide log viewing with username and other goodies
        uacle.setUserID(rs.getInt("userid"));
        uacle.setUserNameFlattened(rs.getString("username"));
        uacle.setMuniNameFlattened(rs.getString("muniname"));
        uacle.setUmapRank(RoleType.valueOf(rs.getString("authorizedrole")));
        uacle.setCeo(rs.getBoolean("codeofficer"));
        
        return uacle;
    }
    
    /**
     * Remember that the notion of a Credential only exists in Java land, since a User 
     * is never "in" the database, doing stuff.
     * @param uacle
     * @throws IntegrationException 
     */
    public void updateUserMuniAuthPeriodLogEntry(UserMuniAuthPeriodLogEntry uacle) throws IntegrationException{
        Connection con = getPostgresCon();
        ResultSet rs = null;
        UserMuniAuthPeriod per = null;
        // broken query
        String query = """
                       UPDATE public.loginmuniauthperiodlog
                          SET category=?, entrydateofrecord=?, disputedby_userid=?, disputedts=?, notes=?, 
                        WHERE authperiodlogentryid=?;
                       """;
        
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setString(1, uacle.getCategory());
            stmt.setTimestamp(2, java.sql.Timestamp.valueOf(uacle.getEntryDateOfRecord()));
            stmt.setInt(3, uacle.getDisputedByUserID());
            if(uacle.getDisputedts() != null){
                stmt.setTimestamp(4,  java.sql.Timestamp.valueOf(uacle.getDisputedts()));
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            stmt.setString(5, uacle.getNotes());
            
            stmt.setInt(6, uacle.getUserMuniAuthPeriodID());
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Error getting user access record", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
    }
    

    /**
     * Insertion point for records in the loginmuniauthperiod table
     * @param uap
     * @throws IntegrationException 
     */
    public void insertUserMuniAuthorizationPeriod(UserMuniAuthPeriodInsertion uap) throws IntegrationException{
        
        Connection con = getPostgresCon();
        ResultSet rs = null;
        
        String query = """
                       INSERT INTO public.loginmuniauthperiod(
                                   muniauthperiodid, muni_municode, authuser_userid, accessgranteddatestart, 
                                   accessgranteddatestop, recorddeactivatedts, authorizedrole, createdts, 
                                   createdby_userid, notes, supportassignedby, assignmentrank, oathts, oathcourtentity_entityid, oathdoc_blobid, codeofficer)
                           VALUES (DEFAULT, ?, ?, ?, 
                                   ?, NULL, CAST (? AS role), now(), 
                                   ?, ?, ?, ?, ?, ?, ?, ?);
                       """;
        
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, uap.getInsertMuni().getMuniCode());
            stmt.setInt(2, uap.getUserID());
            stmt.setTimestamp(3, java.sql.Timestamp.valueOf(uap.getStartDate()));
            
            stmt.setTimestamp(4, java.sql.Timestamp.valueOf(uap.getStopDate()));
            
            stmt.setString(5, uap.getInsertRole().toString());
            // created TS by postgres now()
            
            stmt.setInt(6, uap.getCreatedByUserID());
            stmt.setString(7, uap.getNotes());
            // set support assigned to null until functionality implemented
            stmt.setNull(8, java.sql.Types.NULL);
            stmt.setInt(9, uap.getAssignmentRelativeOrder());
            if(uap.getOathTS() != null){
                stmt.setTimestamp(10, java.sql.Timestamp.valueOf(uap.getOathTS()));
            } else {
                stmt.setNull(10, java.sql.Types.NULL);
            }
            
            if(uap.getOathCourtEntityID() != 0){
                stmt.setInt(11, uap.getOathCourtEntityID());
            } else {
                stmt.setNull(11, java.sql.Types.NULL);
            }
            
            if(uap.getOathBlob() != null){
                System.out.println("UserIntegrator.insertUserMuniAuthorizationPeriod | Oath Blob PHID: " + uap.getOathBlob().getPhotoDocID());
                stmt.setInt(12, uap.getOathBlob().getPhotoDocID());
            } else {
                System.out.println("UserIntegrator.insertUserMuniAuthorizationPeriod | NULL OATH BLOB" );
                stmt.setNull(12, java.sql.Types.NULL);
            }
            
            stmt.setBoolean(13, uap.isInsertOfficer());
            
            stmt.execute();
            
            updateUserLastUpdatedTS(uap.getUserID());
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Error inserting new authorization period", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
    }
    
    /**
     * Insertion point for records in the login table; NOTE that this is a rather 
     * sparse isnert since several login fields like password, etc., are managed 
     * by separate methods in this class and the UserCoordinator.
     * 
     * NOTE security system HARD CODED role of 'User' which is mapped
     * by Wildfly onto an authorized user via Elytron. 
     * LTTODO: Rework to not hard code
     * here!
     * 
     * @param userToInsert
     * @return
     * @throws IntegrationException 
     */
    public int insertUser(User userToInsert) throws IntegrationException{
        ResultSet rs = null;
        String query =  """
                        INSERT INTO public.login(
                                    userid, username, password, notes, humanlink_humanid, pswdlastupdated, 
                                    forcepasswordreset, createdby_userid, createdts, nologinvirtualonly, 
                                    deactivatedts, deactivatedby_userid, lastupdatedts, userrole, homemuni,           lastupdatedby_userid)
                            VALUES (DEFAULT, ?, NULL, ?, ?, NULL, 
                                    NULL, ?, now(), ?, 
                                    NULL, NULL, now(), 'User'::role, ?,           ?);
                        """;
        
        Connection con = null;
        PreparedStatement stmt = null;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setString(1, userToInsert.getUsername());
            stmt.setString(2, userToInsert.getNotes());
            
            if(userToInsert.getHuman() == null){
                stmt.setInt(3, userToInsert.getHumanID());
            } else {
                stmt.setInt(3, userToInsert.getHuman().getHumanID());
            }
            
            if(userToInsert.getCreatedByUserId() != 0){
                stmt.setInt(4, userToInsert.getCreatedByUserId());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            // created ts from db's now()
            stmt.setBoolean(5, userToInsert.isNoLoginVirtualUser());
            
            if(userToInsert.getHomeMuniID() != 0){
                stmt.setInt(6, userToInsert.getHomeMuniID());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            
            if(userToInsert.getLastUpdatedByUserID() != 0){
                stmt.setInt(7, userToInsert.getLastUpdatedByUserID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            
            stmt.execute();
            
            String idNumQuery = "SELECT currval('login_userid_seq');";
            Statement s = con.createStatement();
            rs = s.executeQuery(idNumQuery);
            rs.next();
            int newID = rs.getInt("currval");
            return newID;
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Error inserting new user", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
    }
    
    
    /**
     * Insertion point for records in the login table; NOTE that this is a rather 
     * sparse isnert since several login fields like password, etc., are managed 
     * by separate methods in this class and the UserCoordinator
     * @param muni, if NULL, all system users will be returned
     * @return the user list
     * @throws IntegrationException 
     */
    public List<User> getUsersByHomeMuni(Municipality muni) throws IntegrationException, BObStatusException{
        UserCoordinator uc = getUserCoordinator();
        Connection con = getPostgresCon();
        ResultSet rs = null;
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT userid FROM public.login ");
        if(muni != null){
            sb.append("WHERE homemuni=?");
        }
        sb.append(";");
        
        List<User> usrList = new ArrayList<>();

        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(sb.toString());
            if(muni != null){
                stmt.setInt(1, muni.getMuniCode());
            }
            stmt.execute();
            
            rs = stmt.executeQuery();
            while(rs.next()){
                usrList.add(uc.user_getUser(rs.getInt("userid")));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Error retrieving new user", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return usrList;
    }
    
    /**
     * Invalidating an Authorization period is effectively deleting that record. This action
     * cannot be undone, not even by developers.
     * 
     * @param uap
     * @throws IntegrationException 
     */
    public void invalidateUserAuthRecord(UserMuniAuthPeriod uap) throws IntegrationException{
         Connection con = getPostgresCon();
        
         String query =     "UPDATE loginmuniauthperiod SET recorddeactivatedts = now(), notes=? "
                        +   "WHERE muniauthperiodid=?;";
        
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setString(1, uap.getNotes());
            stmt.setInt(2, uap.getUserMuniAuthPeriodID());
            
            stmt.executeUpdate();
            
            // any update to a User's UMAP is also an update to that user, so stamp it!
            updateUserLastUpdatedTS(uap.getUserID());
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Error updating password", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
        
    }
    
    
    /**
     * As of September 2019, this method will hash incoming passwords with MD5 before writing to the 
     * table. Wildfly's config file specifies that it will digest any submitted password with MD5
     * and then compare. I wanted to use the crypto() library in posgres but that fucntion returns 
     * a true/false and knows how to do its own comparison--but that doesn't work with the current postgres setup
     * 
     * @param user
     * @param psswd
     * @throws IntegrationException 
     */
    public void setUserPassword_SECURITYCRITICAL(User user, String psswd) throws IntegrationException{
        
        String query =          """
                               UPDATE public.login
                                  SET password = encode(digest(?, 'md5'), 'base64') WHERE userid = ?
                               """;
        
         String updateQuery = """
                              UPDATE public.login
                                 SET pswdlastupdated = now() WHERE userid = ?
                              """;
        
        Connection con = null;
        PreparedStatement stmt = null;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setString(1, psswd);
            stmt.setInt(2, user.getUserID());
            
            // Hash and write new password
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Error updating password", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(updateQuery);
            stmt.setInt(1, user.getUserID());
            // Log that the passoword was just updated
            stmt.executeUpdate();
            //update the record itself
            updateUserLastUpdatedTS(user.getUserID());
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Error updating password", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
    }
    
    /**
     * Utility method for marking a User as last updated now() since
     * there are a number of methods that change a single field, like username,
     * or forcing a password reset
     * @param userID
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void updateUserLastUpdatedTS(int userID) throws IntegrationException{
         Connection con = getPostgresCon();
        
         String query = """
                        UPDATE public.login 
                           SET lastupdatedts = now() WHERE userid = ?
                        """;
        
        PreparedStatement stmt = null;
        if(userID != 0){
            try {
                stmt = con.prepareStatement(query);
                stmt.setInt(1, userID);

                stmt.executeUpdate();

            } catch (SQLException ex) {
                System.out.println(ex);
                throw new IntegrationException("Error updating user's lastupdated timestamp", ex);
            } finally{
                 releasePostgresConnection(con, stmt);
            } 
        }
        
    }
    
    /**
     * Utility method for marking a User as last updated now() since
     * there are a number of methods that change a single field, like username,
     * or forcing a password reset
     * @param usr
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void updateUserWipeForcePasswordReset(User usr) throws IntegrationException{
        if(usr == null){
            throw new IntegrationException("Cannot wipe password force TS and forcer ID to 0 with null inputted user");
        }
        
        Connection con = getPostgresCon();
        
         String query = """
                        UPDATE public.login 
                           SET forcepasswordreset = NULL, forcepasswordresetby_userid = NULL WHERE userid = ?
                        """;
        
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, usr.getUserID());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Error updating user's lastupdated timestamp", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
    }
    
    
    
    /**
     * Updates fields on login table record; most login fields are managed by independent
     * methods for security thoroughness
     * @param usr
     * @throws IntegrationException 
     */
    public void updateUser_deactivate(User usr) throws IntegrationException{
        if(usr == null || usr.getUserID() == 0){
            throw new IntegrationException("Cannot update user with null input or user with ID 0");
        }
        
        Connection con = getPostgresCon();
        System.out.println("UserIntegrator.updateUser_deactivate");
        String query =  """
                        UPDATE public.login
                           SET deactivatedts=now(), deactivatedby_userid=?, lastupdatedts=now() 
                         WHERE userid=?;
                        """;
        
        PreparedStatement stmt = null;
        	
        try {
            
            stmt = con.prepareStatement(query);
            if(usr.getDeactivatedByUserID() != 0){
                stmt.setInt(1, usr.getDeactivatedByUserID());
            } else {
                throw new IntegrationException("A user with ID 0 cannot deactivatea usr");
            }
            stmt.setInt(2, usr.getUserID());
            
            stmt.executeUpdate();
            
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("UserIntegrator.updateUser:Error updating User", ex);
        } finally{
             releasePostgresConnection(con, stmt);
        } 
        
    }
    
    
    /**
     * Updates fields on login table record; most login fields are managed by independent
     * methods for security thoroughness
     * I also stamp the login record lastupdatedts to now()
     * 
     * @param usr
     * @throws IntegrationException 
     */
    public void updateUser_notes(User usr) throws IntegrationException{
        if(usr == null || usr.getUserID() == 0){
            throw new IntegrationException("Cannot update user with null input or user with ID 0");
        }
        
        Connection con = getPostgresCon();
        System.out.println("UserIntegrator.updateUser_notes");
        String query =  """
                        UPDATE public.login
                           SET notes=?, lastupdatedts=now()
                         WHERE userid=?;
                        """;
        
        PreparedStatement stmt = null;
        	
        try {
            
            stmt = con.prepareStatement(query);
            stmt.setString(1, usr.getNotes());
            
            stmt.setInt(2, usr.getUserID());
            
            stmt.executeUpdate();
            
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("UserIntegrator.updateUser_notes:Error updating User", ex);
        } finally{
             releasePostgresConnection(con, stmt);
        } 
        
    }
    
    
    /**
     * Updates fields on login table record; most login fields are managed by independent
     * methods for security thoroughness
     * @param usr
     * @throws IntegrationException 
     */
    public void updateUser_homeMuni(User usr) throws IntegrationException{
        if(usr == null || usr.getUserID() == 0){
            throw new IntegrationException("Cannot update user with null input or user with ID 0");
        }
        
        Connection con = getPostgresCon();
        System.out.println("UserIntegrator.updateUser_notes");
        String query =  """
                        UPDATE public.login
                           SET homemuni=?, lastupdatedts=now()
                         WHERE userid=?;
                        """;
        
        PreparedStatement stmt = null;
        	
        try {
            
            stmt = con.prepareStatement(query);
            stmt.setInt(1, usr.getHomeMuniID());
            
            stmt.setInt(2, usr.getUserID());
            
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("UserIntegrator.updateUser_notes:Error updating User", ex);
        } finally{
             releasePostgresConnection(con, stmt);
        } 
        
    }
    
    /**
     * Updates fields on login table record; most login fields are managed by independent
     * methods for security thoroughness
     * @param usr
     * @throws IntegrationException 
     */
    public void updateUser_humanLink(User usr) throws IntegrationException{
        if(usr == null || usr.getUserID() == 0 || usr.getHumanID() == 0){
            throw new IntegrationException("Cannot update user with null input or user with ID 0 or humanlink ID 0");
        }
        
        Connection con = getPostgresCon();
        System.out.println("UserIntegrator.updateUser_notes");
        String query =  """
                        UPDATE public.login
                           SET humanlink_humanid=?, lastupdatedts=now()
                         WHERE userid=?;
                        """;
        
        PreparedStatement stmt = null;
        	
        try {
            
            stmt = con.prepareStatement(query);
            stmt.setInt(1, usr.getHumanID());
            
            stmt.setInt(2, usr.getUserID());
            
            stmt.executeUpdate();
            
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("UserIntegrator.updateUser_notes:Error updating User", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
        
    }
    
    
    /**
     * Updates fields on login table record; most login fields are managed by independent
     * methods for security thoroughness
     * @param ua
     * @throws IntegrationException 
     */
    public void updateUserSignatureBlob(UserAuthorized ua) throws IntegrationException{
        
        Connection con = getPostgresCon();
        System.out.println("UserIntegrator.updateUser");
        String query =  """
                        UPDATE public.login
                           SET signature_photodocid=? 
                         WHERE userid=?;
                        """;
        
        PreparedStatement stmt = null;
        	
        try {
            
            stmt = con.prepareStatement(query);
            if(ua.getSignatureBlob() != null){
                stmt.setInt(1, ua.getSignatureBlob().getPhotoDocID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
                        
            stmt.setInt(2, ua.getUserID());
            
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("UserIntegrator.updateUser:Error updating User signature", ex);
        } finally{
             releasePostgresConnection(con, stmt);
        } 
       
    }
    
   
    /**
     * Forces the passed in user to reset their password on next login
     * @param usr
     * @throws IntegrationException 
     */
    public void updateUser_forcePasswordReset(User usr) throws IntegrationException{
        if(usr == null || usr.getUserID() == 0){
            throw new IntegrationException("Cannot updateUser_forcePasswordReset with null input or user with ID 0");
        }
        Connection con = getPostgresCon();
        String query =  """
                        UPDATE public.login
                           SET forcepasswordreset=now(), forcepasswordresetby_userid=?  WHERE userid=?;
                        """;
        PreparedStatement stmt = null;
        
        try {
            
            stmt = con.prepareStatement(query);
            stmt.setInt(1, usr.getForcedPasswordResetByUserID());
            stmt.setInt(2, usr.getUserID());
            
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Error forcing password reset TS", ex);
        } finally{
             releasePostgresConnection(con, stmt);
        } 
    }
    
    /**
     * Primary access point for the entire User system: Called during SessionInitializer actions
     * to create a new session
     * @param userName
     * @return the Fully-baked user object ready to be passed to and fro
     * @throws IntegrationException 
     */   
    public int getUserID(String userName) throws IntegrationException{
        
        System.out.println("UserIntegrator.getUserID: ALPHA");
        Connection con = getPostgresCon();
        ResultSet rs = null;
        int userID = 0;
        String query = "SELECT userid FROM login WHERE username = ?;";
        
        PreparedStatement stmt = null;
        
        try {
            
            stmt = con.prepareStatement(query);
            stmt.setString(1, userName);
            rs = stmt.executeQuery();
            while(rs.next()){
                userID = rs.getInt("userid");
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("userint.getuser", ex);
        } finally{
             releasePostgresConnection(con, stmt);
        } 
        
        return userID;
    }
    
    /**
     * Utility method for extracting USER ids of all users who aren't deactivated
     * @return
     * @throws IntegrationException 
     */
    public List<Integer> getSystemUserIDList() throws IntegrationException{

        List<Integer> idlst = new ArrayList<>();
        Connection con = getPostgresCon();
        ResultSet rs = null;
        int userID = 0;
        String query = "SELECT userid FROM login WHERE deactivatedts IS NULL ;";
        
        PreparedStatement stmt = null;
        
        try {
            
            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();
            while(rs.next()){
                idlst.add(rs.getInt("userid"));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Userintegrator.getSystemUserIDList", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
        
        return idlst;
    }

    
}
