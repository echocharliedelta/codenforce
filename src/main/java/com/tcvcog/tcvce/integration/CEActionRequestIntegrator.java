/*
 * Copyright (C) 2017 ellen bascomb of apt 31y
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.coordinators.MunicipalityCoordinator;
import com.tcvcog.tcvce.coordinators.PersonCoordinator;
import com.tcvcog.tcvce.coordinators.PropertyCoordinator;
import com.tcvcog.tcvce.coordinators.SearchCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.coordinators.UserCoordinator;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.domain.SearchException;
import com.tcvcog.tcvce.entities.CEActionRequest;
import com.tcvcog.tcvce.entities.CEActionRequestIssueType;
import com.tcvcog.tcvce.entities.CEActionRequestStatus;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.PersonType;
import com.tcvcog.tcvce.entities.search.SearchParamsCEActionRequests;

import java.sql.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.domain.DatabaseFetchRuntimeException;
import com.tcvcog.tcvce.entities.CitationStatus;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;

/**
 * The one and only DB integration method for Code Enforcement Action Requests
 * @author ellen bascomb of apt 31y
 */
@Named("ceActionRequestIntegrator")
@ApplicationScoped
public class CEActionRequestIntegrator extends BackingBeanUtils implements Serializable {
    
    final String CEAR_ACTIVE_FIELD = "ceactionrequest.deactivatedts";
    
    @Inject
    private CECaseCacheManager ceCaseCacheManager;
    
    
    @PostConstruct
    public void initBean(){
        System.out.println("CEActionRequestIntegrator | Initiated");
    }
    
    //Connection integratorConn;
    /**
     * Creates a new instance of CEActionRequestIntegrator
     */
    public CEActionRequestIntegrator() {
    }
    
    
   
    
    /**
     * Updates only the notes field a CEAR
     * @param request
     * @throws IntegrationException 
     */
    public void updateActionRequestInternalFields(CEActionRequest request) throws IntegrationException {
        String q =  """
                     UPDATE public.ceactionrequest
                      	SET requestpubliccc=?, muni_municode=?, cecase_caseid=?, 
                            dateofrecord=?, muniinternalnotes=?, 
                            publicexternalnotes=?, status_id=?, caseattachmenttimestamp=?, 
                            paccenabled=?, caseattachment_userid=?, parcel_parcelkey=?, 
                            lastupdatedts=now(), lastupdatedby_userid=?
                      	WHERE requestid=?; 
                    """;

        // for degugging
        // System.out.println("Select Statement: ");
        // System.out.println(sb.toString());
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(q);
            
            stmt.setInt(1, request.getRequestPublicCC());
            if(request.getMuni() != null){
                stmt.setInt(2, request.getMuni().getMuniCode());  
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            if(request.getCaseID() != 0){
                stmt.setInt(3, request.getCaseID());                
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            
            if(request.getDateOfRecord() != null){
                stmt.setTimestamp(4, java.sql.Timestamp.valueOf(request.getDateOfRecord()));
            }else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            stmt.setString(5, request.getMuniNotes()); 
            
            stmt.setString(6, request.getPublicExternalNotes());
            if(request.getRequestStatus() != null){
                stmt.setInt(7, request.getRequestStatus().getStatusID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            if(request.getCaseAttachmentTimeStamp() != null){
                stmt.setTimestamp(8, java.sql.Timestamp.valueOf(request.getCaseAttachmentTimeStamp()));
            } else {
                stmt.setNull(8, java.sql.Types.NULL);
            }
            
            stmt.setBoolean(9, request.isPaccEnabled());
            if(request.getCaseAttachmentUser() != null){
                stmt.setInt(10, request.getCaseAttachmentUser().getUserID());
            } else {
                stmt.setNull(10, java.sql.Types.NULL);
            }
            if(request.getParcelKey() != 0){
                stmt.setInt(11, request.getParcelKey());
            } else {
                stmt.setNull(11, java.sql.Types.NULL);
            }
            
            if(request.getLastUpdatedTS() != null){
                stmt.setInt(12, request.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(12, java.sql.Types.NULL);
            }
            
            stmt.setInt(13, request.getRequestID());
            
            System.out.println("CEActionRequestorIntegrator.updateActionRequestInternalFields | statement: " + stmt.toString());
            
            stmt.execute();
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("CEActionRequestorIntegrator.updateActionRequestInternalFields | Integration Error: Unable to retrieve action request", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
    }

    /**
     * 
     * @param controlCode
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public List<CEActionRequest> getCEActionRequestByControlCode(int controlCode) throws IntegrationException, BObStatusException {
        List<CEActionRequest> requestList = new ArrayList<>();
        String q = """
                   SELECT requestid FROM public.ceactionrequest 
                    WHERE requestpubliccc= ?;""";

        // for degugging
        // System.out.println("Select Statement: ");
        // System.out.println(sb.toString());
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(q);
            stmt.setInt(1, controlCode);
            System.out.println("CEActionRequestorIntegrator.getCEActionRequestByControlCode | SQL: " + stmt.toString());
            // Retrieve action data from postgres
            rs = stmt.executeQuery();

            // loop through the result set and reat an action request from each
            while (rs.next()) {
                requestList.add(getActionRequestByRequestID(rs.getInt("requestid")));

            }
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("CEActionRequestorIntegrator.getCEActionRequestByControlCode | Integration Error: Unable to retrieve action request", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return requestList;
    } // close getActionRequest

    /**
     * Insertion method for CE action requests
     * @param cear
     * @return
     * @throws IntegrationException 
     */
    public int insertCEActionRequest(CEActionRequest cear) throws IntegrationException {
        int controlCode;

        StringBuilder qbuilder = new StringBuilder();
        qbuilder.append("""
                        INSERT INTO public.ceactionrequest(
                            requestid, requestpubliccc, muni_municode, 
                            issuetype_issuetypeid, cecase_caseid, 
                            createdts, dateofrecord, notataddress, 
                            
                            addressofconcern, requestdescription, isurgent, 
                            anonymityrequested, muniinternalnotes, 
                            publicexternalnotes, status_id, caseattachmenttimestamp, 
                            
                            paccenabled, caseattachment_userid, createdby_userid, 
                            parcel_parcelkey, requestorname, 
                            requestorphone, requestorphone_typeid, requestoremail, 
                            
                            lastupdatedts, lastupdatedby_userid, 
                            deactivatedts, deactivatedby_userid, requestorpersontype)
                        VALUES (DEFAULT, ?, ?, 
                                ?, NULL, 
                                now(), ?, ?, 
                                
                                ?, ?, ?, 
                                ?, NULL, 
                                NULL, ?, NULL, 
                                
                                FALSE, NULL, ?, 
                                ?, NULL, 
                                NULL, NULL, NULL,
                                
                                now(), ?,
                                NULL, NULL, CAST(? AS persontype));
                        """);

        Connection con = null;
        PreparedStatement stmt = null;
        Statement s = null;
        ResultSet rs = null;


        try {
            // start by inserting a person and getting his/her/their new ID
            con = getPostgresCon();
            stmt = con.prepareStatement(qbuilder.toString());

            controlCode = cear.getRequestPublicCC();
            stmt.setInt(1, controlCode);
            stmt.setInt(2, cear.getMuni().getMuniCode());

            stmt.setInt(3, cear.getIssue().getIssueTypeID());
            
            stmt.setTimestamp(4, java.sql.Timestamp.valueOf(cear.getDateOfRecord()));
            stmt.setBoolean(5, cear.isNotAtKnownAddress() );
            
            if(cear.isNotAtKnownAddress() && cear.getNonaddressableDescription() != null){
                stmt.setString(6, cear.getNonaddressableDescription());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            stmt.setString(7, cear.getRequestDescription());
            stmt.setBoolean(8, cear.isUrgent());
            
            stmt.setBoolean(9, cear.isAnonymitiyRequested());

            if(cear.getRequestStatus() != null){
                stmt.setInt(10, cear.getRequestStatus().getStatusID()); 
            } else {
                stmt.setNull(10, java.sql.Types.NULL);
            }
            
            if(cear.getCreatedBy()!= null){
                stmt.setInt(11, cear.getCreatedBy().getUserID());
            } else {
                stmt.setNull(11, java.sql.Types.NULL);
            }
            
            if(!cear.isNotAtKnownAddress() && cear.getParcelKey() != 0 ){
                stmt.setInt(12, cear.getParcelKey());
            } else {
                stmt.setNull(12, java.sql.Types.NULL);
            }
            
            if(cear.getLastUpdatedBy() != null){
                stmt.setInt(13, cear.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(13, java.sql.Types.NULL);
            }
            
            if(cear.getRequestorPersonType() != null){
                stmt.setString(14, cear.getRequestorPersonType().getLabel());
            } else {
                stmt.setNull(14, java.sql.Types.NULL);
            }
            
            stmt.execute();
            // grab the ID of the most recently inserted CEaction request to send
            // back to the caller, who uses it to look up that request again and
            // display the control code. We only want to display control codes
            // of requests that actually made it into the DB to avoid 
            // the user trying to look up a code for a request that doesn't exist
            String idNumQuery = "SELECT currval('ceactionrequest_requestid_seq');";
            stmt = con.prepareStatement(idNumQuery);
            int lastID;
            rs = stmt.executeQuery();
            rs.next();
            lastID = rs.getInt(1);
            return lastID;

        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Integration Error: Problem inserting new Code Enforcement Action Request", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 

    }

    /**
     * Populates fields on a CEAR from a ResultSet
     * @param rs
     * @return
     * @throws SQLException
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    private CEActionRequest generateActionRequestFromRS(ResultSet rs) throws SQLException, IntegrationException, BObStatusException {

        // create the action request object
        CEActionRequest cear = new CEActionRequest();
        CaseCoordinator cc = getCaseCoordinator();
        MunicipalityCoordinator mc = getMuniCoordinator();
        PersonIntegrator pi = getPersonIntegrator();
        UserCoordinator uc = getUserCoordinator();
        SystemIntegrator si = getSystemIntegrator();
        
        cear.setRequestID(rs.getInt("requestid"));
        cear.setRequestPublicCC(rs.getInt("requestPubliccc"));
        cear.setPaccEnabled(rs.getBoolean("paccenabled"));
        
        if(rs.getInt("parcel_parcelkey") != 0){
            cear.setParcelKey(rs.getInt("parcel_parcelkey"));
        }
        if(rs.getInt("issuetype_issuetypeid") != 0){
            cear.setIssue(cc.cear_getCEARIssueType(rs.getInt("issuetype_issuetypeid")));
        }
        cear.setRequestDescription(rs.getString("requestDescription"));
        
        cear.setRequestStatus(getRequestStatus(rs.getInt("status_id")));
        cear.setNotAtKnownAddress(rs.getBoolean("notataddress"));
        cear.setNonaddressableDescription(rs.getString("addressofconcern"));

        cear.setMuni(mc.getMuni(rs.getInt("muni_municode")));

        cear.setRequestorName(rs.getString("requestorname"));
        cear.setRequestorPhone(rs.getString("requestorphone"));
        
        if(rs.getInt("requestorphone_typeid") != 0){
            cear.setRequestorPhoneType(pi.getContactPhoneType(rs.getInt("requestorphone_typeid")));
        }
        cear.setRequestorEmail(rs.getString("requestoremail"));

        if(rs.getString("requestorpersontype") != null){
            cear.setRequestorPersonType(PersonType.valueOf(rs.getString("requestorpersontype")));
        }
       
        cear.setCaseID(rs.getInt("cecase_caseid"));
        java.sql.Timestamp ts = rs.getTimestamp("caseattachmenttimestamp");
        if(ts != null){
            cear.setCaseAttachmentTimeStamp(ts.toLocalDateTime());
            cear.setCaseAttachmentUser(uc.user_getUser(rs.getInt("caseattachment_userid")));
        }
        
        if(rs.getTimestamp("dateofrecord") != null){
            cear.setDateOfRecord(rs.getTimestamp("dateofrecord").toLocalDateTime());
        }

        cear.setUrgent(rs.getBoolean("isurgent"));
        cear.setAnonymitiyRequested(rs.getBoolean("anonymityRequested"));

        cear.setMuniNotes(rs.getString("muniinternalnotes"));
        cear.setPublicExternalNotes(rs.getString("publicexternalnotes"));
        cear.setCreatedTS(rs.getTimestamp("createdts").toLocalDateTime());
        
        si.populateTrackedFields(cear, rs, false);
        
        return cear;
    }

   
    /**
     * Cached backed getter for a single CEAR 
     * @param requestID the PK
     * @return the fully baked object
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public CEActionRequest getActionRequestByRequestID(int requestID) throws IntegrationException, BObStatusException {
      if(requestID == 0){
          throw new IntegrationException("Cannot fetch CEAR with and ID == 0");
      }
      if(ceCaseCacheManager.isCachingEnabled()){
          return ceCaseCacheManager.getCacheCEAR().get(requestID, k -> fetchActionRequestByRequestID(requestID));
      }  else {
          return fetchActionRequestByRequestID(requestID);
      }
    } 
    
  
    /**
     * Actual DB backed getter
     * @param requestID the PK
     * @return the fully baked object
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public CEActionRequest fetchActionRequestByRequestID(int requestID) throws DatabaseFetchRuntimeException {
        CEActionRequest cear = null;
        String q = """
                    SELECT requestid, requestpubliccc, ceactionrequest.muni_municode, 
                            issuetype_issuetypeid, cecase_caseid, 
                            createdts, dateofrecord, notataddress, 
                            addressofconcern, requestdescription, isurgent, 
                            anonymityrequested, muniinternalnotes, 
                            publicexternalnotes, status_id, caseattachmenttimestamp, 
                            paccenabled, caseattachment_userid, createdby_userid, 
                            parcel_parcelkey, requestorname, requestorphone, 
                            requestorphone_typeid, requestoremail, 
                            lastupdatedts, lastupdatedby_userid, deactivatedts, 
                            deactivatedby_userid,requestorpersontype
                    FROM public.ceactionrequest 
                    INNER JOIN ceactionrequestissuetype ON ceactionrequest.issuetype_issuetypeid = ceactionrequestissuetype.issuetypeid 
                    WHERE requestid = ?;
                   """;

        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(q);
            stmt.setInt(1, requestID);
            // Retrieve action data from postgres
            rs = stmt.executeQuery();

            // loop through the result set and reat an action request from each
            while (rs.next()) {
                cear = generateActionRequestFromRS(rs);
            }
        } catch (SQLException | BObStatusException | IntegrationException ex) {
            System.out.println(ex);
            throw new DatabaseFetchRuntimeException("CEActionRequestorIntegrator.getActionRequest | Integration Error: Unable to retrieve action request", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return cear;
    } // close getActionRequest
    
  
    
    /**
     * Used during the final stages of the CEAR submission process
     * once we already have an ID in our CEAR we're updating just the requestor fields
     * @param cear
     * @throws IntegrationException 
     */
    public void updateCEARPublicFields(CEActionRequest cear) throws IntegrationException {

        String q = """
                   UPDATE public.ceactionrequest
                   	SET muni_municode=?, 
                            issuetype_issuetypeid=?, dateofrecord=?, notataddress=?, 
                            addressofconcern=?, requestdescription=?, isurgent=?, 
                            anonymityrequested=?, parcel_parcelkey=?, requestorname=?, 
                            requestorphone=?, requestorphone_typeid=?, requestoremail=?, 
                            lastupdatedts=now(), lastupdatedby_userid=?, 
                            requestorpersontype=CAST(? AS persontype)
                   	WHERE requestid = ?;
                   """;

        Connection con = null;
        PreparedStatement stmt = null;
        try {
            
            con = getPostgresCon();
            stmt = con.prepareStatement(q);
          
            stmt.setInt(1, cear.getMuni().getMuniCode());

            stmt.setInt(2, cear.getIssue().getIssueTypeID());
            stmt.setTimestamp(3, java.sql.Timestamp.valueOf(cear.getDateOfRecord()));
            stmt.setBoolean(4, cear.isNotAtKnownAddress() );
            
            if(cear.isNotAtKnownAddress() && cear.getNonaddressableDescription() != null){
                stmt.setString(5, cear.getNonaddressableDescription());
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            stmt.setString(6, cear.getRequestDescription());
            stmt.setBoolean(7, cear.isUrgent());
            
            stmt.setBoolean(8, cear.isAnonymitiyRequested());
            if(cear.getParcelKey() != 0){
                stmt.setInt(9, cear.getParcelKey());
            } else {
                stmt.setNull(9, java.sql.Types.NULL);
            }
            stmt.setString(10, cear.getRequestorName());
            
            stmt.setString(11, cear.getRequestorPhone());
            if(cear.getRequestorPhoneType() != null){
                stmt.setInt(12, cear.getRequestorPhoneType().getPhoneTypeID());
            } else {
                stmt.setNull(12, java.sql.Types.NULL);
            }
            stmt.setString(13, cear.getRequestorEmail());
            
            if(cear.getLastUpdatedBy() != null){
                stmt.setInt(14, cear.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(14, java.sql.Types.NULL);
            }
            if(cear.getRequestorPersonType() != null){
                stmt.setString(15, cear.getRequestorPersonType().toString());
            } else {
                stmt.setNull(15, java.sql.Types.NULL);
            }
            
            stmt.setInt(16, cear.getRequestID());
            
            System.out.println("CEActionRequestorIntegrator.updatePublicFields| statement: " + stmt.toString());
            // Retrieve action data from postgres
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Integration Error: Unable to update action request", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        } 

    }
    
    
     public void updateCEARNotes(CEActionRequest cear) throws IntegrationException {

        String q = """
                   UPDATE public.ceactionrequest
                   	SET muniinternalnotes=?, publicexternalnotes=?, lastupdatedts=now(), lastupdatedby_userid=?
                   	WHERE requestid = ?;
                   """;

        Connection con = null;
        PreparedStatement stmt = null;
        try {
            
            con = getPostgresCon();
            stmt = con.prepareStatement(q);
            
            
            stmt.setString(1, cear.getMuniNotes());
            stmt.setString(2, cear.getPublicExternalNotes());
            
            if(cear.getLastUpdatedByUserID() != 0){
                stmt.setInt(3, cear.getLastUpdatedByUserID());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            
            stmt.setInt(4, cear.getRequestID());
            
            System.out.println("CEActionRequestorIntegrator.updateCEARNotes| statement: " + stmt.toString());
            // Retrieve action data from postgres
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Integration Error: Unable to update action request notes", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        } 

    }

    /**
     * Extracts a single CEAR request status from the DB
     * @param statusID
     * @return
     * @throws IntegrationException 
     */
    public CEActionRequestStatus getRequestStatus(int statusID) throws IntegrationException, BObStatusException {
        if(statusID == 0){
            throw new BObStatusException("Cannot feetch status with ID == 0");
        }
        CEActionRequestStatus status = null;
        String query = """
                       SELECT statusid, title, description, icon_iconid
                         FROM public.ceactionrequeststatus WHERE statusid = ?;""";
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1,statusID);
            rs = stmt.executeQuery();

            // loop through the result set and reat an action request from each
            while (rs.next()) {
                status = generateCEActionRequestStatus(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("CEActionRequestorIntegrator.getRequestStatus | Integration Error: Unable to retrieve action request status", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return status;
    }

    /**
     * Internal organ for populating a CEAR status from a ResultSet
     * @param rs
     * @return
     * @throws IntegrationException 
     */
    private CEActionRequestStatus generateCEActionRequestStatus(ResultSet rs) throws IntegrationException {
        CEActionRequestStatus arqs = new CEActionRequestStatus();
        SystemCoordinator sc = getSystemCoordinator();
        try {
            arqs.setStatusID(rs.getInt("statusid"));
            arqs.setStatusTitle(rs.getString("title"));
            arqs.setDescription(rs.getString("description"));
            if(rs.getInt("icon_iconid") != 0){
                arqs.setIcon(sc.getIcon(rs.getInt("icon_iconid")));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Cannot Generate CEActionRequestStatus object", ex);
        }
        return arqs;
    }
    
    /**
     * Extracts a list of all available statuses from the DB
     * @return
     * @throws IntegrationException 
     */
    public List<CEActionRequestStatus> getRequestStatusList() throws IntegrationException {

        List<CEActionRequestStatus> statusList = new ArrayList();
        CEActionRequestStatus status;
        String query = """
                       SELECT statusid, title, description, icon_iconid
                         FROM public.ceactionrequeststatus WHERE active = TRUE;
                       """;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();

            // loop through the result set and reat an action request from each
            while (rs.next()) {
                status = generateCEActionRequestStatus(rs);
                statusList.add(status);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("CEActionRequestorIntegrator.getRequestStatusList | Integration Error: Unable to retrieve action request status list", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return statusList;
    }
    
    /**
     * Extracts a record from the ceactionrequestissuetype table
     * @param issueID
     * @return
     * @throws IntegrationException 
     */
    public CEActionRequestIssueType getRequestIssueType(int issueID) throws IntegrationException {

        CEActionRequestIssueType tpe = null;
        String query = """
                       SELECT issuetypeid, typename, typedescription, muni_municode, notes, 
                              intensity_classid, active
                         FROM public.ceactionrequestissuetype
                        WHERE issuetypeid = ?;
                       """;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, issueID);
            rs = stmt.executeQuery();

            // loop through the result set and reat an action request from each
            while (rs.next()) {
                tpe = generateCEActionRequestIssueType(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("CEActionRequestorIntegrator.getRequestIssueType | Integration Error: Unable to retrieve action request issue type", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return tpe;
    }

    /**
     *  Internal populator of action request issue types
     * @param rs
     * @return
     * @throws IntegrationException 
     */
    private CEActionRequestIssueType generateCEActionRequestIssueType(ResultSet rs) throws IntegrationException {
        CEActionRequestIssueType tpe = new CEActionRequestIssueType();
        MunicipalityCoordinator mc = getMuniCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        
        try {
            tpe.setIssueTypeID(rs.getInt("issuetypeid"));
            tpe.setName(rs.getString("typename"));
            tpe.setDescription((rs.getString("typedescription")));
            if(rs.getInt("muni_municode") != 0){
                tpe.setMuni(mc.getMuni(rs.getInt("muni_municode")));
            }
            tpe.setNotes(rs.getString("notes"));
            tpe.setIntensityClass(sc.getIntensityClass(rs.getInt("intensity_classid")));
            tpe.setActive(rs.getBoolean("active"));
            
        } catch (SQLException | BObStatusException ex) {
            System.out.println(ex);
            throw new IntegrationException("Cannot Generate CEActionRequestIssueType object", ex);
        }
        return tpe;
    }
    
    /**
     * Generates a list of CEActionRequestIssueType objects
     * @param muni restrict results to given muni; when null, all issue types are returned
     * @param includeInactive
     * @return the list of sub-BObs
     * @throws IntegrationException 
     */
    public List<CEActionRequestIssueType> getRequestIssueTypeList(Municipality muni, boolean includeInactive) throws IntegrationException {

        List<CEActionRequestIssueType> typeList = new ArrayList();
        
        StringBuilder sb = new StringBuilder();
        sb.append( "SELECT issuetypeid, typename, typedescription, muni_municode, notes, intensity_classid, active \n");
        sb.append(" FROM public.ceactionrequestissuetype WHERE issuetypeid IS NOT NULL ");
        if(muni != null){
            sb.append(" AND muni_municode=? ");
        } 
        if(includeInactive){
            sb.append("");
        } else {
            sb.append(" AND active=TRUE");
        }
        sb.append(";");
        
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(sb.toString());
            if(muni != null){
                stmt.setInt(1, muni.getMuniCode());
            } 

            rs = stmt.executeQuery();

            // loop through the result set and reat an action request from each
            while (rs.next()) {
                typeList.add(generateCEActionRequestIssueType(rs));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("CEActionRequestorIntegrator.getRequestIssueTypeList | Integration Error: Unable to retrieve action request issue type list", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return typeList;
    }
    
   
    
    /**
     * Called by the SearchCoordinator's queryCEAR method
     * implements
     * a multi-stage SQL statement building process based on the settings on the
     * SearchParams object passed into this method.
     *
     * @param params A SearchParamsCEActionRequests object with all of the
     * levers nicely setup for searching
     * 
     * @return a List of all the action requests that meet the search criteria.
     * NOTE that no user will be allowed to search outside of its municipality,
     * including when using a straight ID search.
     * 
     * @throws IntegrationException
     */
    public List<Integer> searchForCEActionRequests(SearchParamsCEActionRequests params) throws IntegrationException {
        SearchCoordinator sc = getSearchCoordinator();
        List<Integer> cearidlst = new ArrayList();
        
        params.appendSQL("SELECT DISTINCT requestid FROM public.ceactionrequest \n");
        params.appendSQL("LEFT OUTER JOIN public.parcel ON (ceactionrequest.parcel_parcelkey = parcel.parcelkey) \n");
        params.appendSQL("WHERE requestid IS NOT NULL \n"); 

        // ****************************
        // **         OBJECT ID      **
        // ****************************
         if (!params.isBobID_ctl()) {
             
            
            // ***********************************
            // **     MUNI,DATES,USER,ACTIVE    **
            // *********************************** 
           
            params = (SearchParamsCEActionRequests) sc.assembleBObSearchSQL_muniDatesUserActive(params, 
                                                                            SearchParamsCEActionRequests.MUNIFIELD,
                                                                            CEAR_ACTIVE_FIELD);
            
            // ****************************
            // **   1.REQUEST STATUS     **
            // **************************** 
            if(params.isRequestStatus_ctl()){
                if(params.getRequestStatus_val() != null){
                    params.appendSQL("AND status_id = ? "); // param 4 without ID search
                } else {
                    params.setRequestStatus_ctl(false);
                    params.appendToParamLog("REQUEST STATUS: status filter on but found null CEActionRequestStatus; status filter turned off; | ");
                }
            }
            
            // ****************************
            // **   2/ISSUE TYPE         **
            // **************************** 
            if(params.isIssueType_ctl()){
                if(params.getIssueType_val() != null){
                    params.appendSQL("AND issuetype_issuetypeid=? "); // param 4 without ID search
                } else {
                    params.setIssueType_ctl(false);
                    params.appendToParamLog("ISSUE TYPE: Issue type filter on but found null CEActionRequestIssueType; issue type filter turned off; | ");
                }
            }
            
            // ****************************
            // **   3/ADDRESSABILITY     **
            // **************************** 
            if(params.isNonaddressable_ctl()){
                if(params.isNonaddressable_val()){
                    params.appendSQL("AND notataddress = TRUE ");
                } else {
                    params.appendSQL("AND notataddress = FALSE ");
                }
            } 
            
            // ****************************
            // **       4.URGENCY        **
            // **************************** 
            if(params.isUrgent_ctl()){
                if(params.isUrgent_val()){
                    params.appendSQL("AND isurgent = TRUE ");
                } else {
                    params.appendSQL("AND isurgent = FALSE ");
                }
            } 

            // ****************************
            // **     5/CASE CNXN        **
            // **************************** 
            if (params.isCaseAttachment_ctl()) {
                if (params.isCaseAttachment_val()) {
                    params.appendSQL("AND cecase_caseid IS NOT NULL ");
                } else {
                    params.appendSQL("AND cecase_caseid IS NULL ");
                }
            }
            
            // ****************************
            // **    6/CECASE            **
            // **************************** 
            
            if (params.isCecase_ctl()) {
                if(params.getCecase_val() != null){
                    params.appendSQL("AND cecase_caseid=? ");
                } else {
                    params.setCecase_ctl(false);
                    params.appendToParamLog("CECASE ID: no CECase found; case id filter turned off; | ");
                }
            }
            
            // ****************************
            // **    7/PUBLIC ACCESS     **
            // **************************** 
            if (params.isPacc_ctl()) {
                if(params.isPacc_val()){
                    params.appendSQL("AND paccenabled = TRUE ");
                } else {
                    params.appendSQL("AND paccenabled = FALSE");
                }
            }
                
            // *******************************
            // **   8: REQUESTING PERSON    **
            // ******************************* 
            
            // as of May 2023 implementation, person links are managed by the system-wide
            // linking infrastructure; person/human linking occurs in the ceactionrequesthuman table
            // not with a single FK on ceactionrequest
                
            // *******************************
            // **   9: Property             **
            // ******************************* 
            
            if (params.isProperty_ctl()) {
                if(params.getProperty_val() != null){
                    params.appendSQL("AND parcel_parcelkey=? ");
                } else {
                    params.setProperty_ctl(false);
                    params.appendToParamLog("Parcel: no parcel object found; filter turned off; | ");
                }
            }
            // *******************************
            // **   10: PACC Value           **
            // ******************************* 
            
            if (params.isPaccnum_ctl()) {
                if(params.getPaccnum_val() != 0){
                    params.appendSQL("AND requestpubliccc=? ");
                } else {
                    params.setPaccnum_ctl(false);
                    params.appendToParamLog("PACC num: I shall not search for PACC equal to 0 | ");
                }
            }

        // ****************************
        // **      OBJECT ID         **
        // **************************** 
        } else {
            params.appendSQL("AND requestid=? "); // will be param 2 with ID search
        }
        params.appendSQL(";");

        ResultSet rs = null;
        PreparedStatement stmt = null;
        Connection con = getPostgresCon();
        int paramCounter = 0;

        try {
            stmt = con.prepareStatement(params.extractRawSQL());
            
            // as long as we're not searching by ID only
             if (!params.isBobID_ctl()) {
                 if(params.isMuni_ctl()){
                    if(params.getMuni_val() != null){
                        stmt.setInt(++paramCounter, params.getMuni_val().getMuniCode());
                    } else {
                        throw new IntegrationException("Cannot query CEAR with null muni search value object");
                    }
                 } 
                
                 if(params.isDate_startEnd_ctl()){
                    stmt.setTimestamp(++paramCounter, params.getDateStart_val_sql());
                    stmt.setTimestamp(++paramCounter, params.getDateEnd_val_sql());
                 }
                 
                if (params.isUser_ctl()) {
                    if(params.getUser_val() != null){
                        stmt.setInt(++paramCounter, params.getUser_val().getUserID());
                    }else {
                        throw new IntegrationException("Cannot query CEAR with missing user value object");
                    }
                } 
                
                if(params.isRequestStatus_ctl()){
                    if(params.getRequestStatus_val()!= null){
                        stmt.setInt(++paramCounter, params.getRequestStatus_val().getStatusID());
                    } else {
                        throw new IntegrationException("Cannot query CEAR with missin status object in status value");
                    }
                } 
                
                if(params.isIssueType_ctl()){
                    if(params.getIssueType_val() != null){
                        stmt.setInt(++paramCounter, params.getIssueType_val().getIssueTypeID());
                    } else {
                        throw new IntegrationException("Query CEAR missing issue type value object");
                    }
                } 
                
                if(params.isCecase_ctl()){
                    stmt.setInt(++paramCounter, params.getCecase_val().getCaseID());
                }
                
                if(params.isProperty_ctl()){
                    if(params.getProperty_val() != null){
                        stmt.setInt(++paramCounter, params.getProperty_val().getParcelKey());
                    } else {
                        throw new IntegrationException("Query CEAR missing property value");
                    }
                } 
                
                if(params.isPaccnum_ctl()){
                    stmt.setInt(++paramCounter, params.getPaccnum_val());
                } 

            } else {
                stmt.setInt(++paramCounter, params.getBobID_val());
            }

            params.appendToParamLog("CEActionRequestIntegrator SQL before execution: ");
            params.appendToParamLog(stmt.toString());
            
            rs = stmt.executeQuery();
            
            int counter = 0;
            int maxResults;
            if (params.isLimitResultCount_ctl()) {
                maxResults = params.getLimitResultCount_val();
            } else {
                maxResults = Integer.MAX_VALUE;
            }
            while (rs.next() && counter < maxResults) {
                cearidlst.add(rs.getInt("requestid"));
                counter++;
            }

        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Integration Error: Problem retrieving and generating action request list", ex);
        } finally {
            
            releasePostgresConnection(con, stmt, rs);

        }// close try/catch
        return cearidlst;
    } // close method
    
    
     public int insertCEARIssueType(CEActionRequestIssueType cearIssueType) throws IntegrationException{
        
        if(cearIssueType == null || cearIssueType.getIssueTypeID() != 0){
            throw new IntegrationException("Cannot insert issue type with null input or input with ID of not 0");
        }
        
        String query =  """
                       INSERT INTO public.ceactionrequestissuetype(
                            	issuetypeid, typename, typedescription, muni_municode, notes, intensity_classid, active)
                            	VALUES (DEFAULT, ?, ?, ?, ?, ?, TRUE); """;
                
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        int freshStatusid = 0;

        try {
            stmt = con.prepareStatement(query);
           
            stmt.setString(1, cearIssueType.getName());
            stmt.setString(2, cearIssueType.getDescription());
            if(cearIssueType.getMuni() != null){
                stmt.setInt(3, cearIssueType.getMuni().getMuniCode());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }             
            stmt.setString(4, cearIssueType.getNotes());
            if(cearIssueType.getIntensityClass() != null){
                stmt.setInt(5, cearIssueType.getIntensityClass().getClassID());
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
                    
            stmt.execute();
            
            String retrievalQuery = "SELECT currval('actionrqstissuetype_issuetypeid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next()){
                 freshStatusid = rs.getInt(1);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to insert cear issue type into database, sorry.", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
        
        return freshStatusid;
        
    }
    
    /**
     * Nukes a record from the citationstatus table
     * @param cearIssueType
     * @throws IntegrationException 
     */
    public void deactivateCEARIssueType(CEActionRequestIssueType cearIssueType) throws IntegrationException{
        
        if(cearIssueType == null || cearIssueType.getIssueTypeID() == 0){
            throw new IntegrationException("Cannot deac cear issue type with null input or input with ID of 0");
        }
        
        String query = """
                        UPDATE public.ceactionrequestissuetype
                            SET active=FALSE
                          WHERE issuetypeid=?;
                       """;
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, cearIssueType.getIssueTypeID());
            
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to deactivate cear issue type, sorry.", ex);
            
        } finally{
            releasePostgresConnection(con, stmt);
        } 
    }
    
    /**
     * Updates a single record in the citationstatus table
     * @param cearIssueType
     * @throws IntegrationException 
     */
    public void updateCEARIssueType(CEActionRequestIssueType cearIssueType) throws IntegrationException{
        if(cearIssueType == null || cearIssueType.getIssueTypeID() == 0){
            throw new IntegrationException("Cannot deac cear issue type with null input or input with ID of 0");
        }
        
        String query =  """
                            UPDATE public.ceactionrequestissuetype
                            	SET typename=?, typedescription=?, muni_municode=?, notes=?, intensity_classid=?
                            	WHERE issuetypeid=? ;
                        """;
        
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            
            stmt.setString(1, cearIssueType.getName());
            stmt.setString(2, cearIssueType.getDescription());
            if(cearIssueType.getMuni() != null){
                stmt.setInt(3, cearIssueType.getMuni().getMuniCode());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }             
            stmt.setString(4, cearIssueType.getNotes());
            if(cearIssueType.getIntensityClass() != null){
                stmt.setInt(5, cearIssueType.getIntensityClass().getClassID());
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            stmt.setInt(6, cearIssueType.getIssueTypeID());
            
            
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update cear issue type in database, sorry.", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
    } // close method
    
} // close class
