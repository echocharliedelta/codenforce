/*
 * Copyright (C) 2018 Adam Gutonski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.coordinators.EventCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.coordinators.UserCoordinator;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.DatabaseFetchRuntimeException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.CECase;
import com.tcvcog.tcvce.entities.Citation;
import com.tcvcog.tcvce.entities.CitationCodeViolationLink;
import com.tcvcog.tcvce.entities.CitationDocketRecord;
import com.tcvcog.tcvce.entities.CitationFilingType;
import com.tcvcog.tcvce.entities.CitationPenalty;
import com.tcvcog.tcvce.entities.CitationPenaltyTypeEnum;
import com.tcvcog.tcvce.entities.CitationStatus;
import com.tcvcog.tcvce.entities.CitationStatusLogEntry;
import com.tcvcog.tcvce.entities.CitationViolationStatusEnum;
import com.tcvcog.tcvce.entities.CourtEntity;
import com.tcvcog.tcvce.entities.Property;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import jakarta.faces.application.FacesMessage;
import com.tcvcog.tcvce.entities.IfaceCitationPenaltyHolder;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;

/**
 *  Database integration methods for court related objects, the
 * two major families of which are CourtEntity objects and the entire
 * (happy) Citation family.
 * 
 * @author Adam Gutonski & Ellen Bascomb of Apartment 31Y
 */
@Named("courtEntityIntegrator")
@ApplicationScoped
public class CourtEntityIntegrator extends BackingBeanUtils implements Serializable {
    
    @Inject
    private CECaseCacheManager ceCaseCacheManager;
    
    
    public CourtEntityIntegrator() {
        
    }
    
    public void updateCourtEntity(CourtEntity courtEntity) throws IntegrationException {
        String query = """
                UPDATE public.courtentity
                SET 
                    entityofficialnum=?, jurisdictionlevel=?, name=?,
                    address_street=?, address_city=?, address_zip=?, address_state=?,
                    county=?, phone=?, url=?, notes=?, judgename=?,
                    lastupdatedts=now(), lastupdatedby_umapid=?
                WHERE entityid=?;
                """;
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        System.out.println("CourtEntityIntegrator.updateCourtEntity | updated name: " + courtEntity.getCourtEntityName());

        try {
            stmt = con.prepareStatement(query);
            stmt.setString(1, courtEntity.getCourtEntityOfficialNum());
            stmt.setString(2, courtEntity.getJurisdictionLevel());
            stmt.setString(3, courtEntity.getCourtEntityName());

            stmt.setString(4, courtEntity.getAddressStreet());
            stmt.setString(5, courtEntity.getAddressCity());
            stmt.setString(6, courtEntity.getAddressZip());
            stmt.setString(7, courtEntity.getAddressState());

            stmt.setString(8, courtEntity.getAddressCounty());
            stmt.setString(9, courtEntity.getPhone());
            stmt.setString(10, courtEntity.getUrl());
            stmt.setString(11, courtEntity.getNotes());
            stmt.setString(12, courtEntity.getJudgeName());

            if (courtEntity.getLastUpdatedby_UMAP() != null) {
                stmt.setInt(13, courtEntity.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(13, java.sql.Types.NULL);
            }

            stmt.setInt(14, courtEntity.getCourtEntityID());
            System.out.println("CourtEntityIntegrator.updateCourtEntity | sql: " + stmt.toString());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update Court Entity", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        }
    }
    
    /**
     * Retrieves all court entities
     * @return
     * @throws IntegrationException 
     */
    public List<CourtEntity> getCourtEntityList(boolean requireActive) throws IntegrationException {
        ArrayList<CourtEntity> ceList = new ArrayList();
        StringBuilder query = new StringBuilder();
        query.append("SELECT entityid FROM public.courtentity ");
        if (requireActive) {
            query.append("WHERE deactivatedts IS NULL");
        }
        query.append(";");

        ResultSet rs = null;
        PreparedStatement stmt = null;
        Connection con = getPostgresCon();

        try {
            stmt = con.prepareStatement(query.toString());
            System.out.println("");
            rs = stmt.executeQuery();
            while (rs.next()) {
                ceList.add(getCourtEntity(rs.getInt("entityid")));
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("cannot generate court entity list", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }
        return ceList;
    }
    
    /**
     * Retrieves court entities by municode
     * @param muniCode
     * @return
     * @throws IntegrationException 
     */
    public List<CourtEntity> getCourtEntityList(int muniCode) throws IntegrationException {
        String query = "SELECT courtentity_entityid FROM municourtentity WHERE muni_municode=?;";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        List<CourtEntity> ceList = new ArrayList();

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, muniCode);
            rs = stmt.executeQuery();
            while(rs.next()){
                ceList.add(getCourtEntity(rs.getInt("courtentity_entityid")));
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot generate court entity list", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }
        return ceList;
    }



    public int insertCourtEntity(CourtEntity courtEntity) throws IntegrationException, BObStatusException {
        if (courtEntity == null) {
            throw new BObStatusException("cannot insert null object into court entity table");
        }
        String query = """
                INSERT INTO public.courtentity(
                    entityid, entityofficialnum, jurisdictionlevel, name, address_street,
                    address_city, address_zip, address_state, county, phone, url,
                    notes, judgename,
                    createdts, createdby_umapid, lastupdatedts,
                    lastupdatedby_umapid, deactivatedts, deactivatedby_umapid)
                VALUES (DEFAULT, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?, ?,
                    ?, ?,
                    now(), ?, now(),
                    ?, NULL, NULL);
                """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        int freshID = 0;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(query);
            stmt.setString(1, courtEntity.getCourtEntityOfficialNum());
            stmt.setString(2, courtEntity.getJurisdictionLevel());
            stmt.setString(3, courtEntity.getCourtEntityName());
            stmt.setString(4, courtEntity.getAddressStreet());

            stmt.setString(5, courtEntity.getAddressCity());
            stmt.setString(6, courtEntity.getAddressZip());
            stmt.setString(7, courtEntity.getAddressState());
            stmt.setString(8, courtEntity.getAddressCounty());
            stmt.setString(9, courtEntity.getPhone());
            stmt.setString(10, courtEntity.getUrl());

            stmt.setString(11, courtEntity.getNotes());
            stmt.setString(12, courtEntity.getJudgeName());

            if (courtEntity.getCreatedby_UMAP() != null) {
                stmt.setInt(13, courtEntity.getCreatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(13, java.sql.Types.NULL);
            }

            if (courtEntity.getLastUpdatedby_UMAP() != null) {
                stmt.setInt(14, courtEntity.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(14, java.sql.Types.NULL);
            }

            stmt.execute();

            stmt = con.prepareStatement(query);
            String retrievalQuery = "SELECT currval('courtentity_entityid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();

            while (rs.next()) {
                freshID = rs.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert Court Entity", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 

        return freshID;
    }

    public CourtEntity getCourtEntity(int entityID) throws IntegrationException {

        String query = """
                SELECT entityid, entityofficialnum, jurisdictionlevel, name, address_street,
                address_city, address_zip, address_state, county, phone, url,
                notes, judgename,
                createdts, createdby_umapid, lastupdatedts,
                lastupdatedby_umapid, deactivatedts, deactivatedby_umapid
                FROM public.courtentity WHERE entityID=?;
                """;

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        CourtEntity ce = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, entityID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                ce = generateCourtEntity(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot generate court entity due to SQL error", ex);
        } catch (IntegrationException ex) {
            throw new IntegrationException("Cannot generate court entity", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }
        return ce;
    }

    private CourtEntity generateCourtEntity(ResultSet rs) throws IntegrationException {
        CourtEntity newCourtEntity = new CourtEntity();
        SystemIntegrator si = getSystemIntegrator();

        try {
            newCourtEntity.setCourtEntityID(rs.getInt("entityid"));
            newCourtEntity.setCourtEntityOfficialNum(rs.getString("entityofficialnum"));
            newCourtEntity.setJurisdictionLevel(rs.getString("jurisdictionlevel"));
            newCourtEntity.setCourtEntityName(rs.getString("name"));
            newCourtEntity.setAddressStreet(rs.getString("address_street"));

            newCourtEntity.setAddressCity(rs.getString("address_city"));
            newCourtEntity.setAddressZip(rs.getString("address_zip"));
            newCourtEntity.setAddressState(rs.getString("address_state"));
            newCourtEntity.setAddressCounty(rs.getString("county"));
            newCourtEntity.setPhone(rs.getString("phone"));
            newCourtEntity.setUrl(rs.getString("url"));

            newCourtEntity.setNotes(rs.getString("notes"));
            newCourtEntity.setJudgeName(rs.getString("judgename"));

            si.populateUMAPTrackedFields(newCourtEntity, rs, true);
        } catch (SQLException | BObStatusException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Error generating Court Entity from result set", ex);
        }

        return newCourtEntity;
    }
    
    
     
     // ***********************************************************************
     // **                  CITATIONS                                        **
     // ***********************************************************************
     
     /**
      * Creates a new record in the citation table and connects applicable
      * code violations; TODO: the violation connection should probably be separated out
      * 
      * @param citation
     * @return the new citation's record database primary key
      * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
      */
    public int insertCitation(Citation citation) throws IntegrationException, BObStatusException{

        // Updated for citation overhaul as aprt of humanization
        String insert =  """
                         INSERT INTO public.citation(
                                     citationid, citationno, origin_courtentity_entityid, 
                                     login_userid, dateofrecord, isactive, notes, 
                                     officialtext, createdts, createdby_userid, lastupdatedts, lastupdatedby_userid, 
                                     deactivatedts, deactivatedby_userid, filingtype_typeid)
                             VALUES (DEFAULT, ?, ?, 
                                     ?, ?, TRUE, ?, 
                                     ?, now(), ?, now(), ?, 
                                     NULL, NULL, ?);
                         """ ;
        
        Connection con = getPostgresCon();
        PreparedStatement stmt1 = null;
        ResultSet rs = null;
        
        int lastCID = 0;
        
        try {
            
            stmt1 = con.prepareStatement(insert);
          
            
            stmt1.setString(1, citation.getCitationNo());
            
            if(citation.getOrigin_courtentity() != null){
                stmt1.setInt(2, citation.getOrigin_courtentity().getCourtEntityID());
            } else {
                stmt1.setNull(2, java.sql.Types.NULL);
            }
            
            if(citation.getFilingOfficer() != null){
                stmt1.setInt(3, citation.getFilingOfficer().getUserID());
            } else {
                stmt1.setNull(3, java.sql.Types.NULL);
            }
            
            if(citation.getDateOfRecord() != null){
                stmt1.setTimestamp(4, java.sql.Timestamp.valueOf(citation.getDateOfRecord()));
            } else {
                stmt1.setNull(4, java.sql.Types.NULL);
            }
            
            stmt1.setString(5, citation.getNotes());
            stmt1.setString(6, citation.getOfficialText());
            
            if(citation.getCreatedBy() != null){
                stmt1.setInt(7, citation.getCreatedBy().getUserID());
            } else {
                stmt1.setNull(7, java.sql.Types.NULL);
            }
            
            if(citation.getLastUpdatedBy()!= null){
                stmt1.setInt(8, citation.getLastUpdatedBy().getUserID());
            } else {
                stmt1.setNull(8, java.sql.Types.NULL);
            }
            
            if(citation.getFilingType() != null){
                stmt1.setInt(9, citation.getFilingType().getTypeID());
            } else {
                stmt1.setNull(9, java.sql.Types.NULL);
            }
            
            stmt1.execute();
           
            stmt1 = con.prepareStatement("SELECT currval('citation_citationid_seq');");
            rs = stmt1.executeQuery();
            
            while(rs.next()){
                 lastCID= rs.getInt(1);
            }
           
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to insert citation into database, sorry.", ex);
            
        } finally{
             releasePostgresConnection(con, stmt1, rs);
        } 
        
        return lastCID;
    }
    
    
    /**
     * Second half of the citation insert process: linking violations to their
     * parent citation
     * @param cit 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void insertCitationCodeViolationLink(Citation cit) throws IntegrationException{
        
        if(cit != null){
            String queryCitationViolationTable = """
                                                 INSERT INTO public.citationviolation(
                                                             citationviolationid, citation_citationid, codeviolation_violationid, 
                                                             createdts, lastupdatedts, status, createdby_userid, 
                                                             lastupdatedby_userid, notes, source_sourceid)
                                                     VALUES (DEFAULT, ?, ?, 
                                                             now(), now(), CAST(? AS citationviolationstatus), ?, 
                                                             ?, ?, ?);
                                                 """;

            Connection con = getPostgresCon();
            PreparedStatement stmt = null;

            try {

                stmt = con.prepareStatement(queryCitationViolationTable);
                
                ListIterator<CitationCodeViolationLink> li = cit.getViolationList().listIterator();

                while(li.hasNext()){
                    CitationCodeViolationLink ccvl = li.next();
                    stmt.setInt(1, cit.getCitationID());
                    stmt.setInt(2, ccvl.getViolationID());
                    if(ccvl.getCitVStatus() != null){
                        stmt.setString(3, ccvl.getCitVStatus().toString());
                    } else {
                        stmt.setNull(3, java.sql.Types.NULL);
                    }
                    
                    if(ccvl.getLinkCreatedByUserID() != 0){
                        stmt.setInt(4, ccvl.getLinkCreatedByUserID());
                    } else {
                        stmt.setNull(4, java.sql.Types.NULL);
                    }
                    if(ccvl.getLinkLastUpdatedByUserID() != 0){
                        stmt.setInt(5, ccvl.getLinkLastUpdatedByUserID());
                    } else {
                        stmt.setNull(5, java.sql.Types.NULL);
                    }
                    stmt.setString(6, ccvl.getLinkNotes());
                    if(ccvl.getLinkSource() != null){
                        stmt.setInt(7, ccvl.getLinkSource().getSourceid());
                    } else {
                        stmt.setNull(7, java.sql.Types.NULL);
                    }
                    System.out.println("CourtEntityIntegrator.linkcitation | about to write link violationID : " + ccvl.getViolationID() + " to citation " + cit.getCitationID() );
                    stmt.execute();
                } // close while over citaiton violations

            } catch (SQLException ex) {
                System.out.println(ex.toString());
                throw new IntegrationException("CourtEntityIntegrator.insertCitationCodeViolationLink | Unable to insert citation link into database, sorry.", ex);

            } finally{
                releasePostgresConnection(con, stmt);
            } 
        } else {
            throw new IntegrationException("CourtEntityIntegrator.insertCitationCodeViolationLink | Cannot link violations to a citation NULL citation");
        }
    }
    
    /**
     * Updates a record in the citationviolation table
     * @param ccvl
     * @throws IntegrationException 
     */
     public void updateCitationCodeViolationLink(CitationCodeViolationLink ccvl) throws IntegrationException, BObStatusException{
        
        if(ccvl == null){
           throw new BObStatusException("Cannot update citation violation with null input");
        }
        
        String queryCitationViolationTable = "UPDATE public.citationviolation\n" +
        "   SET lastupdatedts=now(), status=CAST(? AS citationviolationstatus), lastupdatedby_userid=?, notes=?, source_sourceid=? \n" +
        " WHERE citationviolationid=?;";

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {

            stmt = con.prepareStatement(queryCitationViolationTable);

            if(ccvl.getCitVStatus() != null){
                stmt.setString(1, ccvl.getCitVStatus().toString());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            
            if(ccvl.getLinkLastUpdatedByUserID() != 0){
                stmt.setInt(2, ccvl.getLinkLastUpdatedByUserID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            stmt.setString(3, ccvl.getLinkNotes());
            
            if(ccvl.getLinkSource() != null){
                stmt.setInt(4, ccvl.getLinkSource().getSourceid());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            
            stmt.setInt(5, ccvl.getCitationViolationID());
            
            
            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update citation code violation record, sorry.", ex);

        } finally{
             releasePostgresConnection(con, stmt);
        } 

    }
    /**
     * Deactivates a record in the citationviolation table; I only check for a non-zero 
     * deactivator and I'll set deacTS to now()
     * 
     * @param ccvl
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
     public void deactivateCitationCodeViolationLink(CitationCodeViolationLink ccvl) throws IntegrationException, BObStatusException{
        
        if(ccvl == null){
           throw new BObStatusException("Cannot update citation violation with null input");
        }
        
        String queryCitationViolationTable = "UPDATE public.citationviolation\n" +
        "   SET lastupdatedts=now(), lastupdatedby_userid=?, deactivatedts=now(), deactivatedby_userid=? \n" +
        " WHERE citationviolationid=?;";

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {

            stmt = con.prepareStatement(queryCitationViolationTable);

            
            if(ccvl.getLinkLastUpdatedByUserID()!= 0){
                stmt.setInt(1, ccvl.getLinkLastUpdatedByUserID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            
            if(ccvl.getLinkDeactivatedByUserID() != 0){
                stmt.setInt(2, ccvl.getLinkDeactivatedByUserID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            
            stmt.setInt(3, ccvl.getCitationViolationID());
            
            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update citation code violation record, sorry.", ex);

        } finally{
             releasePostgresConnection(con, stmt);
        } 

    }

    
    /**
     * Extracts a list of citation ID values given a CodeViolation ID
     * @param violationID
     * @return
     * @throws IntegrationException 
     */
    public List<Integer> getCitationIDsByViolation(int violationID) throws IntegrationException{
        List<Integer> cList = new ArrayList<>();
        String query = "SELECT citation_citationid FROM public.citationviolation WHERE codeviolation_violationid = ?;";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, violationID);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                cList.add(rs.getInt("citation_citationid"));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot generate citation list", ex);
            
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        
        return cList;
    }
    
    /**
     * Cache-backed getter for a single citation from the DB given a citation ID
     * @param id
     * @return
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public Citation getCitation(int id) throws IntegrationException, BObStatusException{
        if(id == 0){
            throw new BObStatusException("Cannot get citation with ID == 0");
        }
        if(ceCaseCacheManager.isCachingEnabled()){
            return ceCaseCacheManager.getCacheCitation().get(id, k -> fetchCitation(id));
        } else {
            return fetchCitation(id);
        }
       
    }
    
    /**
     * DB extraction for a citation
     * @param id
     * @return
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public Citation fetchCitation(int id) throws DatabaseFetchRuntimeException{

        String query = """
                        SELECT  citationid, citationno, origin_courtentity_entityid, 
                                login_userid, dateofrecord, isactive, notes, 
                                officialtext, createdts, createdby_userid, lastupdatedts, lastupdatedby_userid, 
                                deactivatedts, deactivatedby_userid, filingtype_typeid
                         FROM public.citation WHERE citationid=?;
                       """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        Citation c = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                c = generateCitationFromRS(rs);
            }
            
        } catch (SQLException | BObStatusException | IntegrationException ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("CourtEntityIntegrator.getCitation(): cannot fetch citation, sorry.", ex);
            
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        
        return c;
    }
    
    /**
     * Retrieves a DocketNumber record from the DB
     * @param id
     * @return the fully-backed DocketREcord
     * @throws IntegrationException 
     */
    public CitationDocketRecord getCitationDocketRecord(int id) throws IntegrationException{

        String query =  """
                        SELECT docketid, docketno, dateofrecord, courtentity_entityid, createdts, 
                               createdby_userid, lastupdatedts, lastupdatedby_userid, deactivatedts, 
                               deactivatedby_userid, notes, citation_citationid   FROM public.citationdocketno WHERE docketid = ?;
                        """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        CitationDocketRecord cdr = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                cdr = generateCitationDocketRecord(rs);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("CourtEntityIntegrator.getCitationDocketRecord: cannot fetch citationdocketrecord, sorry.", ex);
            
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        
        return cdr;
    }
    
    /**
     * Injects member values from a result set for a CitationDocketRecord
     * @param rs
     * @return 
     */
    private CitationDocketRecord generateCitationDocketRecord(ResultSet rs) throws SQLException, IntegrationException{
        SystemIntegrator si = getSystemIntegrator();
        
        CitationDocketRecord cdr = null;
        if(rs != null){
            try {
                cdr = new CitationDocketRecord();
                
                cdr.setDocketID(rs.getInt("docketid"));
                cdr.setDocketNumber(rs.getString("docketno"));
                if(rs.getTimestamp("dateofrecord") != null){
                    cdr.setDateOfRecord(rs.getTimestamp("dateofrecord").toLocalDateTime());
                }
                
                cdr.setCourtEntity(getCourtEntity(rs.getInt("courtentity_entityid")));
                cdr.setCitationID(rs.getInt("citation_citationid"));
                cdr.setNotes(rs.getString("notes"));
                
                si.populateTrackedFields(cdr, rs, false);
            } catch (BObStatusException ex) {
                throw new IntegrationException(ex.getMessage());
            }
        }
        
        return cdr;
    }
    
    /**
     * Extracts citation docket records by citation
     * @param cit
     * @return
     * @throws BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public List<Integer> getCitationDocketRecords(Citation cit) 
            throws BObStatusException, IntegrationException, IntegrationException{
        if(cit == null){
            throw new BObStatusException("Cannot get citation docket nos for null citation");
        }
        
        List<Integer> cdridl = new ArrayList<>();
        
        String query = "SELECT docketid FROM citationdocketno WHERE citation_citationid=? AND deactivatedts IS NULL ;";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, cit.getCitationID());
            rs = stmt.executeQuery();
            
            while(rs.next()){
                cdridl.add(rs.getInt("docketid"));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot fetch dockets by citation, Sorry!", ex);
            
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        return cdridl;
        
    }
    
    /**
     * Creates a new record in the citationdocketno table
     * @param cdr
     * @return the ID of the freshly inserted docket
     * @throws IntegrationException 
     */
    public int insertCitationDocket(CitationDocketRecord cdr) throws IntegrationException{
        if(cdr == null || cdr.getCitationID() == 0){
            throw new IntegrationException("Cannot insert citation docket with null docket record or citation ID of zero!");
        }
        
        String query =  """
                        INSERT INTO public.citationdocketno(
                                    docketid, docketno, dateofrecord, courtentity_entityid, createdts, 
                                    createdby_userid, lastupdatedts, lastupdatedby_userid, 
                                    citation_citationid)
                            VALUES (DEFAULT, ?, ?, ?, now(), 
                                    ?, now(), ?, 
                                    ?);
                        """;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int freshDocketID = 0;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            
            stmt.setString(1, cdr.getDocketNumber());
            
            if(cdr.getDateOfRecord() != null){
                stmt.setTimestamp(2, java.sql.Timestamp.valueOf(cdr.getDateOfRecord()));
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            
            if(cdr.getCourtEntity() != null){
                stmt.setInt(3, cdr.getCourtEntity().getCourtEntityID());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            
            if(cdr.getCreatedBy() != null){
                stmt.setInt(4, cdr.getCreatedBy().getUserID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            
            if(cdr.getLastUpdatedBy() != null){
                stmt.setInt(5, cdr.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            
            stmt.setInt(6, cdr.getCitationID());
            
            stmt.execute();
            
            String retrievalQuery = "SELECT currval('citationdocketno_docketid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                 freshDocketID = rs.getInt(1);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to insert citation Docket into database, sorry.", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        return freshDocketID;
    }
    
    /**
     * Updates a docket record
     * @param cdr
     * @throws IntegrationException 
     */
    public void updateCitationDocket(CitationDocketRecord cdr) throws IntegrationException{
        if(cdr == null || cdr.getCitationID() == 0){
            throw new IntegrationException("Cannot udate citation docket with null docket record or citation ID of zero!");
        }
        
        String query =  """
                        UPDATE public.citationdocketno
                           SET docketno=?, dateofrecord=?, courtentity_entityid=?, 
                               lastupdatedts=now(), lastupdatedby_userid=?, 
                               citation_citationid=? 
                         WHERE docketid=?;
                        """;
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = con.prepareStatement(query);
            
            stmt.setString(1, cdr.getDocketNumber());
            
            if(cdr.getDateOfRecord() != null){
                stmt.setTimestamp(2, java.sql.Timestamp.valueOf(cdr.getDateOfRecord()));
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            
            if(cdr.getCourtEntity() != null){
                stmt.setInt(3, cdr.getCourtEntity().getCourtEntityID());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            
            if(cdr.getLastUpdatedBy() != null){
                stmt.setInt(4, cdr.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            
            stmt.setInt(5, cdr.getCitationID());
            stmt.setInt(6, cdr.getDocketID());
            
            stmt.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to insert citation Docket into database, sorry.", ex);
            
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
    }
    
    /**
     * Sets the deactivation TS and deactivation UserID on a citation docket
     * 
     * @param cdr
     * @throws IntegrationException 
     */
    public void deactivateCitationDocket(CitationDocketRecord cdr) throws IntegrationException{
        if(cdr == null || cdr.getCitationID() == 0){
            throw new IntegrationException("Cannot insert citation docket with null docket record or citation ID of zero!");
        }
        
        String query =  "UPDATE public.citationdocketno\n" +
                        "   SET deactivatedts=now(), deactivatedby_userid=?, lastupdatedts=now(), lastupdatedby_userid=? \n" +
                        " WHERE docketid=?;";
        
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = con.prepareStatement(query);
            
            if(cdr.getDeactivatedBy()!= null){
                stmt.setInt(1, cdr.getDeactivatedBy().getUserID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            
            if(cdr.getLastUpdatedBy() != null){
                stmt.setInt(2, cdr.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            
            stmt.setInt(3, cdr.getDocketID());
            
            stmt.execute();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to deactivate citation Docket in database, sorry.", ex);
            
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
    }
    
    
    /**
     * Utility method for retrieving a list of Citation objects
     * @param citIDList a list of citation IDs
     * @return list of Citations, will always return an instantiated List
     * @throws IntegrationException 
     */
    public List<Citation> getCitations(List<Integer> citIDList) throws IntegrationException, BObStatusException{
        
        List<Citation> citL = new ArrayList<>();
        if(citIDList != null && !citIDList.isEmpty()){
            for(Integer i: citIDList){
                citL.add(getCitation(i));
            }
        }
        return citL;
        
    }
    
    /**
     * Extracts a list of Citation IDs from the DB given a Property
     * @param prop
     * @return
     * @throws IntegrationException 
     */
    public List<Citation> getCitations(Property prop) throws IntegrationException, BObStatusException{
        //this doesn't work anymore since citations don't know about cases, we have to go through citationViolation
        // codeviolations know about cases
        
        String query =  "SELECT citationid FROM public.citation 	"
                        + "INNER JOIN public.cecase ON cecase.caseid = citation.caseid \n" +
                        "INNER JOIN public.parcel ON cecase.parcel_parcelkey = parcel.parcelkey \n" +
                        "WHERE parcelkey=? AND citation.isactive = TRUE; ";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        List<Citation> citationList = new ArrayList();
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, prop.getParcelKey());
            rs = stmt.executeQuery();
            
            while(rs.next()){
                citationList.add(getCitation(rs.getInt("citationid")));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("cannot fetch citations by property, sorries!", ex);
            
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        return citationList;
    }
    
    /**
     * Extracts citations from the DB given a CECase
     * @param ceCase
     * @return list of Citations IDs of citations that are active only by cecase
     * @throws IntegrationException 
     */
    public List<Integer> getCitations(CECase ceCase) throws IntegrationException{
        CaseCoordinator cc = getCaseCoordinator();
        String query =  "SELECT DISTINCT ON (citationID) citation.citationid, codeviolation.cecase_caseID FROM public.citationviolation 	\n" +
                        "	INNER JOIN public.citation ON citation.citationid = citationviolation.citation_citationid\n" +
                        "	INNER JOIN public.codeviolation on codeviolation.violationid = citationviolation.codeviolation_violationid\n" +
                        "	INNER JOIN public.cecase ON cecase.caseid = codeviolation.cecase_caseID\n" +
                        "	WHERE codeviolation.cecase_caseID=? AND citation.deactivatedts IS NULL;";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        List<Integer> citationIDList = new ArrayList();
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, ceCase.getCaseID());
            rs = stmt.executeQuery();
            
            while(rs.next()){
                citationIDList.add(rs.getInt("citationid"));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("CourtEntityIntegrator.getCitations(CECase): cannot fetch citations by CECase, sorry.", ex);
            
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        return citationIDList;
    }
    
    /**
     * Extracts a list of CodeViolation IDs from DB given a citation ID
     * @param citvid
     * @return
     * @throws IntegrationException 
     */
    public CitationCodeViolationLink getCitationCodeViolation(int citvid) throws IntegrationException{
        
        String query =  """
                            SELECT  citationviolationid, citation_citationid, codeviolation_violationid, 
                                    citationviolation.createdts, citationviolation.lastupdatedts, citationviolation.deactivatedts, status, citationviolation.createdby_userid, 
                                    citationviolation.lastupdatedby_userid, citationviolation.deactivatedby_userid, citationviolation.notes, source_sourceid, codeviolation.cecase_caseid FROM public.citationviolation
                            INNER JOIN public.codeviolation ON (citationviolation.codeviolation_violationid = codeviolation.violationid) 
                            WHERE citationviolationid=?;
                        """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        CitationCodeViolationLink cvl = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, citvid);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                cvl = generateCitationViolationLink(rs);
                
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("getCitationCodeViolation cannot fetch list of CitationViolationLinks by citvid, sorry.", ex);
            
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        return cvl;
    }
    
   
   
    /**
     * Generator of a subclass of CodeViolation: a code violation attached to a citation
     * @param rs
     * @return
     * @throws SQLException
     * @throws IntegrationException 
     */
    private CitationCodeViolationLink generateCitationViolationLink(ResultSet rs) throws SQLException, IntegrationException{
        try {
            CaseCoordinator cc = getCaseCoordinator();
            SystemIntegrator si = getSystemIntegrator();
            SystemCoordinator sc = getSystemCoordinator();
            CitationCodeViolationLink cvl = new CitationCodeViolationLink(cc.violation_getCodeViolation(rs.getInt("codeviolation_violationid")));
            cvl.setCitationViolationID(rs.getInt("citationviolationid"));
            
            cvl.setCitationID(rs.getInt("citation_citationid"));
            cvl.setViolationID(rs.getInt("codeviolation_violationid"));
            
            cvl.setLinkNotes(rs.getString("notes"));
            if(rs.getInt("source_sourceid") != 0){
                cvl.setLinkSource(sc.getBObSource(rs.getInt("source_sourceid")));
            }
            if(rs.getString("status") != null){
                cvl.setCitVStatus(CitationViolationStatusEnum.valueOf(rs.getString("status")));
            }
            cvl.setCeCaseID(rs.getInt("cecase_caseid"));
            
            si.populateTrackedLinkFields(cvl, rs);
            
            return cvl;
        } catch (BObStatusException ex) {
            throw new IntegrationException(ex.getMessage());
        }
    }
    
    
     /**
     * Extracts a list of CodeViolation IDs from DB given a citation ID
     * @param cid
     * @return
     * @throws IntegrationException 
     */
    public List<CitationCodeViolationLink> getCodeViolationsByCitation(Citation cid) throws IntegrationException{
        
        String query =  """
                        SELECT citationviolationid, citation_citationid, codeviolation_violationid, 
                                citationviolation.createdts, citationviolation.lastupdatedts, citationviolation.deactivatedts, status, citationviolation.createdby_userid, 
                                citationviolation.lastupdatedby_userid, citationviolation.deactivatedby_userid, citationviolation.notes, citationviolation.source_sourceid, codeviolation.cecase_caseid AS cecase_caseid 
                        FROM citationviolation 
                         INNER JOIN  public.codeviolation ON (citationviolation.codeviolation_violationid = codeviolation.violationid)
                         WHERE citation_citationid=?;
                        """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        List<CitationCodeViolationLink> cvll = new ArrayList<>();
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, cid.getCitationID());
            rs = stmt.executeQuery();
            
            while(rs.next()){
                cvll.add(generateCitationViolationLink(rs));
                
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("getCodeViolationsByCitation: cannot fetch list of CitationViolationLinks by Citation ID, sorry.", ex);
            
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        return cvll;
    }

    
    /**
     * Internal generator method for creating a Citation from a ResultSet with 
     * all DB fields present
     * @param rs
     * @return
     * @throws IntegrationException 
     */
    private Citation generateCitationFromRS(ResultSet rs) throws IntegrationException, BObStatusException{
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        SystemIntegrator si = getSystemIntegrator();
        UserCoordinator uc = getUserCoordinator();
        Citation cit = new Citation();
        try {
            cit.setCitationID(rs.getInt("citationid"));
            cit.setCitationNo(rs.getString("citationno"));
            
            
            cit.setOrigin_courtentity(cei.getCourtEntity(rs.getInt("origin_courtentity_entityID")));
            
            if(rs.getTimestamp("dateOfRecord") != null){
                cit.setDateOfRecord(rs.getTimestamp("dateOfRecord").toLocalDateTime());
            }
            cit.setFilingOfficer(uc.user_getUser(rs.getInt("login_userid")));
            cit.setFilingType(getCitationFilingType(rs.getInt("filingtype_typeid")));
            cit.setNotes(rs.getString("notes"));
            cit.setOfficialText(rs.getString("officialtext"));
            si.populateTrackedFields(cit, rs, false);
            
        } catch (SQLException | IntegrationException ex) {
            System.out.println(ex);
            throw new IntegrationException("Unable to build citation from RS", ex);
        }
        return cit;
    }
    
    
    /**
     * Writes a new record into the citationcitationstatus table to track
     * the movement a given citation through the court process
     * 
     * @param cit to which the log entry should be attached
     * @param csle the populated lot entry
     * @return the fresh record ID
     */
    public int insertCitationStatusLogEntry(Citation cit, CitationStatusLogEntry csle) throws IntegrationException{
        
        if(cit == null || csle == null || csle.getStatus() == null){
            throw new IntegrationException("cannot insert a log entry with null citation or log entry!");
        }
        
        String query =  """
                        INSERT INTO public.citationcitationstatus(
                                    citationstatusid, citation_citationid, citationstatus_statusid, 
                                    dateofrecord, createdts, createdby_userid, lastupdatedts, lastupdatedby_userid, courtentity_entityid )
                            VALUES (DEFAULT, ?, ?, 
                                    ?, now(), ?, now(), ?, ?);
                        """;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int freshLogID = 0;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            
            stmt.setInt(1, cit.getCitationID());
            stmt.setInt(2, csle.getStatus().getStatusID());
            
            if(csle.getDateOfRecord() != null){
                stmt.setTimestamp(3, java.sql.Timestamp.valueOf(csle.getDateOfRecord()));
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            
            if(csle.getCreatedBy() != null){
                stmt.setInt(4, csle.getCreatedBy().getUserID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            
            
            if(csle.getLastUpdatedBy() != null){
                stmt.setInt(5, csle.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            
            if(csle.getCourtEntity() != null){
                stmt.setInt(6, csle.getCourtEntity().getCourtEntityID());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            
            stmt.execute();

            String retrievalQuery = "SELECT currval('citationcitationstatus_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                 freshLogID = rs.getInt(1);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to insert citation log entry into database, sorry.", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        return freshLogID;
        
    }
    
    
    /**
     * Writes a new record into the citationcitationstatus table to track
     * the movement a given citation through the court process
     * 
     * @param csle the populated lot entry
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void updateStatusLogEntry(CitationStatusLogEntry csle) throws IntegrationException{
        
        if(csle == null || csle.getStatus() == null){
            throw new IntegrationException("cannot insert a log entry with null citation or log entry!");
        }
        
        String query =  """
                        UPDATE public.citationcitationstatus
                           SET citationstatus_statusid=?, 
                               dateofrecord=?, lastupdatedts=now(), 
                               lastupdatedby_userid=?, courtentity_entityid=?
                         WHERE citationstatusid=?;""";
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            
            stmt.setInt(1, csle.getStatus().getStatusID());
            
            if(csle.getDateOfRecord() != null){
                stmt.setTimestamp(2, java.sql.Timestamp.valueOf(csle.getDateOfRecord()));
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            
            if(csle.getLastUpdatedBy() != null){
                stmt.setInt(3, csle.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            
            if(csle.getCourtEntity() != null){
                stmt.setInt(4, csle.getCourtEntity().getCourtEntityID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            
            stmt.setInt(5, csle.getLogEntryID());
                
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update status log entry, sorry.", ex);
        } finally{
             releasePostgresConnection(con, stmt);
        } 
    }
    
    /**
     * Deactivates a citation status log entry
     * @param csle
     * @throws IntegrationException 
     */
    public void deactivateCitationStatusLogEntry(CitationStatusLogEntry csle) throws IntegrationException{
          
        if(csle == null ){
            throw new IntegrationException("cannot deactuvate a null log entry!");
        }
        
        String query =  """
                        UPDATE public.citationcitationstatus
                           SET  lastupdatedts=now(), lastupdatedby_userid=?,         
                        deactivatedts=now(), deactivatedby_userid=?  WHERE citationstatusid=?;
                        """;
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            
            if(csle.getLastUpdatedBy() != null){
                stmt.setInt(1, csle.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            
            if(csle.getDeactivatedBy() != null){
                stmt.setInt(2, csle.getDeactivatedBy().getUserID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            
            stmt.setInt(3, csle.getLogEntryID());
                
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to deactivate citation log, sorry.", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
    }
    
    
    /**
     * Creates a log of citation statuses and their dates of progression
     * through the court system; 
     * @param cit
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public List<Integer> citation_getStatusLog(Citation cit) throws IntegrationException{
           String query =  """
                           SELECT citationstatusid 
                             FROM public.citationcitationstatus JOIN public.citationstatus ON citationcitationstatus.citationstatus_statusid = statusid
                             WHERE citation_citationid=? AND citationcitationstatus.deactivatedts IS NULL ORDER BY dateofrecord ASC;
                           """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        List<Integer> statusLogIDList = new ArrayList<>();
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, cit.getCitationID());
            rs = stmt.executeQuery();
            
            while(rs.next()){
                statusLogIDList.add(rs.getInt("citationstatusid"));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot generate citation status log, sorry", ex);
            
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        return statusLogIDList;           
    }
    
    /**
     * Extracts a single citation status log entry from the DB
     * @param logRecID
     * @return
     * @throws IntegrationException 
     */
    public CitationStatusLogEntry getCitationStatusLogEntry(int logRecID) throws IntegrationException{
        String query =  """
                        SELECT citationstatusid, citation_citationid, citationstatus_statusid, 
                               dateofrecord, createdts, createdby_userid, lastupdatedts, lastupdatedby_userid, 
                               deactivatedts, deactivatedby_userid, notes, courtentity_entityid
                          FROM public.citationcitationstatus WHERE citationstatusid = ?;
                        """;
                
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        CitationStatusLogEntry statusLog = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, logRecID);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                statusLog = generateCitationStatusLogEntry(rs);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot generate citation status log entry, sorry", ex);
            
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        return statusLog;   
        
        
   }
    
    /**
     * Generator method for CitationStatusLog Entries
     * @param rs
     * @return a single log entry
     */
    private CitationStatusLogEntry generateCitationStatusLogEntry(ResultSet rs) throws SQLException, IntegrationException{
        try {
            SystemIntegrator si = getSystemIntegrator();
            CaseCoordinator cc = getCaseCoordinator();
            CitationStatusLogEntry csle = new CitationStatusLogEntry();
            
            csle.setLogEntryID(rs.getInt("citationstatusid"));
            csle.setCitationID(rs.getInt("citation_citationid"));
            csle.setStatus(cc.citation_getCitationStatus(rs.getInt("citationstatus_statusid")));
            
            if(rs.getTimestamp("dateofrecord") != null){
                csle.setDateOfRecord(rs.getTimestamp("dateofrecord").toLocalDateTime());
            }
            
            csle.setNotes(rs.getString("notes"));
            
            csle.setCourtEntity(getCourtEntity(rs.getInt("courtentity_entityid")));
            
            si.populateTrackedFields(csle, rs, false);
            
            return csle;
        } catch (BObStatusException ex) {
            throw new IntegrationException(ex.getMessage());
        }
        
    }
    
    
    
    /**
     * Updates a single record in the citation table
     * @param citation
     * @throws IntegrationException 
     */
    public void updateCitation(Citation citation) throws IntegrationException{
        String query =  """
                        UPDATE public.citation
                           SET  origin_courtentity_entityid=?, 
                               login_userid=?, dateofrecord=?, 
                               officialtext=?, lastupdatedts=now(), 
                               lastupdatedby_userid=?, 
                               filingtype_typeid=?, citationno = ?
                         WHERE citationid=?;
                        """;
        Connection con = getPostgresCon();
        PreparedStatement stmt1 = null;
        
        try {
            stmt1 = con.prepareStatement(query);
            
            
            if(citation.getOrigin_courtentity() != null){
                stmt1.setInt(1, citation.getOrigin_courtentity().getCourtEntityID());
            } else {
                stmt1.setNull(1, java.sql.Types.NULL);
            }
            
            if(citation.getFilingOfficer() != null){
                stmt1.setInt(2, citation.getFilingOfficer().getUserID());
            } else {
                stmt1.setNull(2, java.sql.Types.NULL);
            }
            
            if(citation.getDateOfRecord() != null){
                stmt1.setTimestamp(3, java.sql.Timestamp.valueOf(citation.getDateOfRecord()));
            } else {
                stmt1.setNull(3, java.sql.Types.NULL);
            }
            
            stmt1.setString(4, citation.getOfficialText());
            
            if(citation.getLastUpdatedBy()!= null){
                stmt1.setInt(5, citation.getLastUpdatedBy().getUserID());
            } else {
                stmt1.setNull(5, java.sql.Types.NULL);
            }
            
            if(citation.getFilingType() != null){
                stmt1.setInt(6, citation.getFilingType().getTypeID());
            } else {
                stmt1.setNull(6, java.sql.Types.NULL);
            }
            
            stmt1.setString(7, citation.getCitationNo());
            
            stmt1.setInt(8, citation.getCitationID());
            
            stmt1.execute();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update citation in the database, sorry.", ex);
        } finally{
            releasePostgresConnection(con, stmt1);
        } 
    }
    
    /**
     * Nukes a Citation from the DB! Dangerous!
     * @param citation
     * @throws IntegrationException 
     */
    public void deactivateCitation(Citation citation) throws IntegrationException{
        String query =  """
                        UPDATE public.citation
                           SET  deactivatedts=now(), deactivatedby_userid=?, lastupdatedts=now(), 
                               lastupdatedby_userid=? 
                         WHERE citationid=?;
                        """;
        
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            if(citation.getDeactivatedBy() != null){
                stmt.setInt(1, citation.getDeactivatedBy().getUserID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            
            if(citation.getLastUpdatedBy() != null){
                stmt.setInt(2, citation.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            
            stmt.setInt(3, citation.getCitationID());
            
            stmt.execute();
            
            getFacesContext().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO, 
                    "Citation has been deactivated!", ""));
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to delete this citation in the database, sorry. "
                    + "Most likely reason: some other record in the system references this citation somehow, "
                    + "like a court case. As a result, this citation cannot be deleted.", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
    }
    
    
     /** *************************************************** */
     /** ******************* FILING TYPES  ***************** */
     /** *************************************************** */
    
    /**
     * Extracts a record from citationfilingtype
     * @param typeID
     * @return the fully baked type object
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public CitationFilingType getCitationFilingType(int typeID) 
            throws IntegrationException, BObStatusException{
        String query =  """
                        SELECT typeid, title, description, muni_municode, active
                          FROM public.citationfilingtype WHERE typeid=?; """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        CitationFilingType cft = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, typeID);
            
            rs = stmt.executeQuery();
            
            while(rs.next()){
                cft = generateCitationFilingType(rs);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("CEI.getCitationFilingType(typeid): cannot fetch CitationFiliingType, sorry.", ex);
            
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
             
        return cft;
        
    }
    
    /**
     * Generates a CitationFilingType object from a RS
     * @param rs
     * @return
     * @throws BObStatusException 
     */
    private CitationFilingType generateCitationFilingType(ResultSet rs) throws BObStatusException, SQLException{
        if(rs == null){
            throw new BObStatusException("Cannot generate filing type with null RS");
        }
        CitationFilingType cft = new CitationFilingType();
        
        cft.setTypeID(rs.getInt("typeid"));
        cft.setTitle(rs.getString("title"));
        cft.setDescription(rs.getString("description"));
        return cft;
    }
    
    /**
     * Builds a complete list of filing types
     * @return
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public List<CitationFilingType> getCitationFilingTypes() 
            throws IntegrationException, BObStatusException{
        String query =  "SELECT typeid FROM public.citationfilingtype WHERE active = TRUE; ";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        List<CitationFilingType> cftl = new ArrayList<>();
        
        try {
            stmt = con.prepareStatement(query);
            
            rs = stmt.executeQuery();
            
            while(rs.next()){
                cftl.add(getCitationFilingType(rs.getInt("typeid")));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("cannot fetch citation filing type, sorry.", ex);
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        return cftl;
    }   
    
     /** *************************************************** */
     /** ******************** PENALTIES ******************** */
     /** *************************************************** */
    
    /**
     * Creates a CitationStatusLogEntry given a statusid
     * @param statusID
     * @return
     * @throws IntegrationException 
     */
    public CitationStatus getCitationStatus(int statusID) throws IntegrationException{
            
        String query =  """
                        SELECT statusid, statusname, description, icon_iconid, editsforbidden, 
                               appellate, displayorder, terminalstatus, eventcategory_categoryid, batchupdatecitationviolations, deactivatedts FROM citationStatus WHERE statusid=?""";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        CitationStatus cs = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, statusID);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                cs = generateCitationStatus(rs);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("CEI.getCiationStatus(statusID): cannot fetch CitationStatus, sorry.", ex);
            
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        return cs;
        
        
    }
    
    /**
     * Builds a complete list of possible citation statuses
     * @param includeInactive
     * @return
     * @throws IntegrationException 
     */
    public List<CitationStatus> getCitationStatusList(boolean includeInactive) throws IntegrationException, BObStatusException{
        CaseCoordinator cc = getCaseCoordinator();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT statusid FROM citationStatus ");
        if(includeInactive){
            sb.append(";");
        } else {
            sb.append(" WHERE deactivatedts IS NULL");
        }
        
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        List<CitationStatus> csList = new ArrayList<>();
        
        try {
            stmt = con.prepareStatement(sb.toString());
            
            rs = stmt.executeQuery();
            
            while(rs.next()){
                csList.add(cc.citation_getCitationStatus(rs.getInt("statusid")));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("CEI.getCitationStatusList(): cannot fetch complete CitationStatus List, sorry.", ex);
            
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        return csList;
        
        
        
    }
    
    /**
     * Internal generator method for CitationStatusLogEntry objects
     * @param rs
     * @return
     * @throws IntegrationException 
     */
    private CitationStatus generateCitationStatus(ResultSet rs) throws IntegrationException{
        CitationStatus cs = new CitationStatus();
        
        SystemIntegrator si = getSystemIntegrator();
        WorkflowIntegrator wi = getWorkflowIntegrator();
        EventCoordinator ec = getEventCoordinator();
        
        try {
        
            cs.setStatusID(rs.getInt("statusid"));
            cs.setStatusTitle(rs.getString("statusname"));
            cs.setDescription(rs.getString("description"));
            cs.setIcon(si.getIcon(rs.getInt("icon_iconid")));
            cs.setEditsForbidden(rs.getBoolean("editsforbidden"));
            cs.setDisplayOrder(rs.getInt("displayorder"));
            
            cs.setTerminalStatus(rs.getBoolean("terminalstatus"));
            cs.setAppellate(rs.getBoolean("appellate"));
            
            if(rs.getInt("eventcategory_categoryid") != 0){
                cs.setEventCategoryTrigger(ec.getEventCategory(rs.getInt("eventcategory_categoryid")));
            }
            
            if(rs.getString("batchupdatecitationviolations") != null){
                cs.setBatchApplyCitationViolationStatus(CitationViolationStatusEnum.valueOf(rs.getString("batchupdatecitationviolations")));
            }
            if(rs.getTimestamp("deactivatedts") != null){
                cs.setDeactivatedTS(rs.getTimestamp("deactivatedts").toLocalDateTime());
            }
            
        
        } catch (SQLException ex) {
            
            System.out.println(ex);
            throw new IntegrationException("Cannot Generate citation status object, sorry", ex);
        }
        return cs;
    }
    
    /**
     * Creates a new record in the citationstatus table
     * NOTE that this is a utility method for creating a new citation status
     * that can be applied to any citation, NOT for logging a new status of
     * a particular citation. 
     * 
     * @param cs
     * @return  id of the new status
     * @throws IntegrationException 
     */
    public int insertCitationStatus(CitationStatus cs) throws IntegrationException{
        
        if(cs == null || cs.getStatusID() != 0){
            throw new IntegrationException("Cannot insert citation status with null input or input with ID of not 0");
        }
        
        String query =  """
                        INSERT INTO public.citationstatus(
                                    statusid, statusname, description, 
                                    icon_iconid, editsforbidden, appellate, 
                                    terminalstatus, batchupdatecitationviolations, displayorder)
                            VALUES (DEFAULT, ?, ?, 
                                    ?, ?, ?, 
                                    ?, ?::citationviolationstatus, ?);""";
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        int freshStatusid = 0;

        try {
            stmt = con.prepareStatement(query);
            stmt.setString(1, cs.getStatusTitle());
            stmt.setString(2, cs.getDescription());
            if(cs.getIcon() != null){
                stmt.setInt(3, cs.getIcon().getID());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            stmt.setBoolean(4, cs.isEditsForbidden());
            stmt.setBoolean(5, cs.isAppellate());
            stmt.setBoolean(6, cs.isTerminalStatus());
            if(cs.getBatchApplyCitationViolationStatus() != null){
                stmt.setString(7, cs.getBatchApplyCitationViolationStatus().name());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            stmt.setInt(8, cs.getDisplayOrder());
            
            stmt.execute();
            
            String retrievalQuery = "SELECT currval('citationstatus_statusid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next()){
                 freshStatusid = rs.getInt(1);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to insert citation status into database, sorry.", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
        
        return freshStatusid;
        
    }
    
    /**
     * Nukes a record from the citationstatus table
     * @param cs
     * @throws IntegrationException 
     */
    public void deactivateCitationStatus(CitationStatus cs) throws IntegrationException{
        
        if(cs == null || cs.getStatusID() == 0){
            throw new IntegrationException("Cannot deac citation status with null input or input with ID of 0");
        }
        
        String query = """
                        UPDATE public.citationstatus
                            SET deactivatedts=now()
                          WHERE statusid=?;
                       """;
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, cs.getStatusID());
            
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to deactivate citation into database, sorry.", ex);
            
        } finally{
            releasePostgresConnection(con, stmt);
        } 
    }
    
    /**
     * Updates a single record in the citationstatus table
     * @param cs
     * @throws IntegrationException 
     */
    public void updateCitationStatus(CitationStatus cs) throws IntegrationException{
        if(cs == null || cs.getStatusID() == 0){
            throw new IntegrationException("Cannot update citation status with null input or input with ID of 0");
        }
        
        String query =  """
                        UPDATE public.citationstatus
                           SET statusname=?, description=?, icon_iconid=?, editsforbidden=?, appellate=?, batchupdatecitationviolations=?::citationviolationstatus, displayorder=?, terminalstatus=?
                         WHERE statusid=?;
                        """;
        
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setString(1, cs.getStatusTitle());
            stmt.setString(2, cs.getDescription());
            stmt.setInt(3, cs.getIcon().getID());
            stmt.setBoolean(4, cs.isEditsForbidden());
            stmt.setBoolean(5, cs.isAppellate());
           
            if(cs.getBatchApplyCitationViolationStatus() != null){
                stmt.setString(6, cs.getBatchApplyCitationViolationStatus().name());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            stmt.setInt(7, cs.getDisplayOrder());
            stmt.setBoolean(8, cs.isTerminalStatus());
            stmt.setInt(9, cs.getStatusID());
            
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update citation in database, sorry.", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
    } // close method
    
 
    
    /** ******************** PENALTIES ******************** */
     
    
    /**
     * insertion point for citation penalties
     * @param penalty
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public int insertCitationPenalty(CitationPenalty penalty) throws IntegrationException{
        if(penalty == null){
            throw new IntegrationException("Cannot insert null penalty");
        }
        
        int freshPenaltyID = 0;
        
        String query =  """
                        INSERT INTO public.citationpenalty(
                            	penaltyid, penaltytype, penaltyamount, 
                                jaildays, notes, dateofrecord, 
                                citation_citationid, citationviolation_citvid, 
                                createdts, createdby_userid, 
                                lastupdatedts, lastupdatedby_userid, 
                                deactivatedts, deactivatedby_userid)
                            	VALUES (DEFAULT, ?::penaltytype, ?, 
                                        ?, ?, ?, 
                                        ?, ?, 
                                        now(), ?, 
                                        now(), ?, 
                                        NULL, NULL);""";
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setString(1, penalty.getPenaltyType().name());
            stmt.setBigDecimal(2, penalty.getFineAmount());
            
            stmt.setInt(3, penalty.getJailDays());
            stmt.setString(4, penalty.getNotes());
            stmt.setTimestamp(5, java.sql.Timestamp.valueOf(penalty.getDateOfRecord().atStartOfDay()));
            
            if(penalty.getCitationID() != 0){
                stmt.setInt(6, penalty.getCitationID());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            if(penalty.getCitationViolationID() != 0){
                stmt.setInt(7, penalty.getCitationViolationID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            if(penalty.getCreatedBy() != null){
                stmt.setInt(8, penalty.getCreatedBy().getUserID());
            } else {
                stmt.setNull(8, java.sql.Types.NULL);
            }
            if(penalty.getLastUpdatedBy() != null){
                stmt.setInt(9, penalty.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(9, java.sql.Types.NULL);
            }
            
            stmt.execute();

            String retrievalQuery = "SELECT currval('citationpenalty_penaltyid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next()){
                 freshPenaltyID = rs.getInt(1);
            }
            
            // stmt.setTimestamp(12, java.sql.Timestamp.valueOf(permit.getStaticreinspectiondate().atStartOfDay()));
            // LocalDate.from(rs.getTimestamp("dynamicdateofapplication").toLocalDateTime())
            stmt.execute();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to insert citationpenalty into database, sorry.", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
        return freshPenaltyID;
        
    }
    
    
    /**
     * Update point for citation penalty objects
     * @param penalty 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void updateCitationPenalty(CitationPenalty penalty) throws IntegrationException{
        if(penalty == null){
            throw new IntegrationException("Cannot insert null penalty");
        }
        String query =  """
                            UPDATE public.citationpenalty
                            	SET penaltytype=?::penaltytype, penaltyamount=?, jaildays=?, 
                                    notes=?, dateofrecord=?, citation_citationid=?, 
                                    citationviolation_citvid=?, 
                                    lastupdatedts=now(), lastupdatedby_userid=?, 
                                    deactivatedts=?, deactivatedby_userid=?
                            	WHERE penaltyid=?;
                        """;
        
        
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            
            stmt.setString(1, penalty.getPenaltyType().name());
            stmt.setBigDecimal(2, penalty.getFineAmount());
            
            stmt.setInt(3, penalty.getJailDays());
            stmt.setString(4, penalty.getNotes());
            stmt.setTimestamp(5, java.sql.Timestamp.valueOf(penalty.getDateOfRecord().atStartOfDay()));
            
            if(penalty.getCitationID() != 0){
                stmt.setInt(6, penalty.getCitationID());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            if(penalty.getCitationViolationID() != 0){
                stmt.setInt(7, penalty.getCitationViolationID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            
            if(penalty.getLastUpdatedBy() != null){
                stmt.setInt(8, penalty.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(8, java.sql.Types.NULL);
            }

            if(penalty.getDeactivatedTS() != null){
                stmt.setTimestamp(9, java.sql.Timestamp.valueOf(penalty.getDeactivatedTS()));
            } else {
                stmt.setNull(9, java.sql.Types.NULL);
            }
            
            if(penalty.getDeactivatedBy() != null){
                stmt.setInt(10, penalty.getDeactivatedBy().getUserID());
            } else {
                stmt.setNull(10, java.sql.Types.NULL);
            }
            
            stmt.setInt(11, penalty.getPenaltyID());
            
            stmt.executeUpdate();
            System.out.println("CourtEntityIntegrator.updateCitationPenalty | penaltyID: " + penalty.getPenaltyID());
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update citationpenalty into database, sorry.", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
    }
            
    
    
    /**
     * Gets a single penalty by ID
     * @param penaltyID
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */    
    public CitationPenalty getCitationPenalty(int penaltyID) 
            throws IntegrationException, BObStatusException{
        
        if(penaltyID == 0){
            throw new IntegrationException("Cannot get penalytID of 0");
        }
        
        String query =  """
                        SELECT  penaltyid, penaltytype, penaltyamount, 
                                jaildays, notes, dateofrecord, 
                                citation_citationid, citationviolation_citvid, createdts, 
                                createdby_userid, lastupdatedts, lastupdatedby_userid, 
                                deactivatedts, deactivatedby_userid
                          	FROM public.citationpenalty WHERE penaltyid=?;""";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        CitationPenalty pen = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, penaltyID);
            
            rs = stmt.executeQuery();
            
            while(rs.next()){
                pen = generateCitationPenalty(rs);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("CourtEntityIntegrator.getCitationPenalty | error retrieving penalty by ID", ex);
            
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        return pen;
    }
    
    
    public List<Integer> getCitationPenaltyList(IfaceCitationPenaltyHolder holder) 
            throws IntegrationException, BObStatusException{
        if(holder == null){
            throw new IntegrationException("Cannot get penalty list with null holder");
        }
        
        String query =  """
                        SELECT  penaltyid FROM public.citationpenalty WHERE deactivatedts IS NULL AND""";
        StringBuilder qb = new StringBuilder(query);
        qb.append(" ").append(holder.getFKField()).append("=?;");
        
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        List<Integer> idl = new ArrayList<>();
        
        try {
            stmt = con.prepareStatement(qb.toString());
            stmt.setInt(1, holder.getHolderPK());
            
            rs = stmt.executeQuery();
            
            while(rs.next()){
                idl.add(rs.getInt("penaltyid"));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("CourtEntityIntegrator.getCitationPenaltyList | error retrieving penalties by ID", ex);
            
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        return idl;
    }
    
    /**
     * Generator of penalty types
     * @param rs
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws java.sql.SQLException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public CitationPenalty generateCitationPenalty(ResultSet rs) throws IntegrationException, SQLException, BObStatusException{
        if(rs == null){
            throw new IntegrationException("cannot generate from null RS");
        }
        SystemIntegrator si = getSystemIntegrator();
        
        CitationPenalty pen = new CitationPenalty();
        
        pen.setPenaltyID(rs.getInt("penaltyid"));
        pen.setCitationID(rs.getInt("citation_citationid"));
        pen.setCitationViolationID(rs.getInt("citationviolation_citvid"));
        
        pen.setPenaltyType(CitationPenaltyTypeEnum.valueOf(rs.getString("penaltytype")));
        pen.setFineAmount(rs.getBigDecimal("penaltyamount"));
        pen.setJailDays(rs.getInt("jaildays"));
        
        if(rs.getTimestamp("dateofrecord") != null){
            pen.setDateOfRecord(rs.getTimestamp("dateofrecord").toLocalDateTime().toLocalDate());
        }
        pen.setNotes(rs.getString("notes"));
        si.populateTrackedFields(pen, rs, false);
        
        return pen;
    }
}
