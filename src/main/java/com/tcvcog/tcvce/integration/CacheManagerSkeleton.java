/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheClient;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import com.tcvcog.tcvce.entities.occupancy.OccChecklistTemplate;
import jakarta.annotation.PostConstruct;
import jakarta.faces.bean.ApplicationScoped;
import jakarta.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Holds and manages a family of object caches
 * @author Ellen Bascom of Apartment31Y
 */

public class CacheManagerSkeleton extends BackingBeanUtils implements IFaceCacheManager{
    
    private boolean cachingEnabled;
    
    private Cache<Integer, OccChecklistTemplate> cacheInspectionChecklist;
    
    private final List<IFaceCacheClient> cacheClientList;
    
    public CacheManagerSkeleton(){
        cacheClientList = new ArrayList<>();
    }
    
    @PostConstruct
    public void initBean() {
        System.out.println("XXXXXXXXXXCacheManager.initBean");
        initCaching();
    }
    
    /**
     * Instantiates all our caches
     */
    @Override
    public void initCaching(){
        // checklists
        cacheInspectionChecklist = Caffeine.newBuilder().maximumSize(200).softValues().build();
        
    }
    
    /**
     * Writes out
     */
    @Override
    public void writeCacheStats(){
        /// write estimatedSize()
    }
    
    /**
     * 
     * Dumps all my caches
     */
    @Override
    public void flushAllCaches(){
        System.out.println("XXXXXXXXXXCacheManager.flushAllCaches");
        if(cacheInspectionChecklist != null){
            cacheInspectionChecklist.invalidateAll();
        }
        
    }

    @Override
    public void flushObjectFromCache(IFaceCachable cable) {
        if(cable != null){
            flushKey(cable.getCacheKey());
        }
    }
    
    @Override
    public void flushObjectFromCache(int cacheKey) {
        flushKey(cacheKey);
    }
    
    private void flushKey(int key){
        if(cacheInspectionChecklist != null){
            cacheInspectionChecklist.invalidate(key);
        }
        
    }



    @Override
    public void registerCacheClient(IFaceCacheClient client) {
        if(getCacheClientList() != null && !cacheClientList.contains(client)){
            getCacheClientList().add(client);
        }
    }

    @Override
    public void removeCacheClient(IFaceCacheClient client) {
        if(getCacheClientList() != null && getCacheClientList().contains(client)){
            getCacheClientList().remove(client);
        }
        
    }


    @Override
    public boolean isCachingEnabled() {
        return cachingEnabled;
    }

    @Override
    public void disableClientCaching() {
        cachingEnabled = false;
    }


    @Override
    public void enableClientCaching() {
        cachingEnabled = true;
    }


    /**
     * @return the cacheClientList
     */
    public List<IFaceCacheClient> getCacheClientList() {
        return cacheClientList;
    }

    /**
     * @return the cacheInspectionChecklist
     */
    public Cache<Integer, OccChecklistTemplate> getCacheInspectionChecklist() {
        return cacheInspectionChecklist;
    }

    /**
     * @param cacheInspectionChecklist the cacheInspectionChecklist to set
     */
    public void setCacheInspectionChecklist(Cache<Integer, OccChecklistTemplate> cacheInspectionChecklist) {
        this.cacheInspectionChecklist = cacheInspectionChecklist;
    }

    

    
    
}
