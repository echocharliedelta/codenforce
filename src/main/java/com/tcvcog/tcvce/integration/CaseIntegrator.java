/*
 * Copyright (C) 2017 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.coordinators.BlobCoordinator;
import com.tcvcog.tcvce.coordinators.CaseCoordinator;
import com.tcvcog.tcvce.coordinators.CodeCoordinator;
import com.tcvcog.tcvce.coordinators.EventCoordinator;
import com.tcvcog.tcvce.coordinators.MunicipalityCoordinator;
import com.tcvcog.tcvce.coordinators.PersonCoordinator;
import com.tcvcog.tcvce.coordinators.PropertyCoordinator;
import com.tcvcog.tcvce.coordinators.SearchCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.coordinators.UserCoordinator;
import com.tcvcog.tcvce.coordinators.WorkflowCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.DatabaseFetchRuntimeException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.CECaseDataHeavy;
import com.tcvcog.tcvce.entities.CECase;
import com.tcvcog.tcvce.entities.CasePhaseEnum;
import com.tcvcog.tcvce.entities.Citation;
import com.tcvcog.tcvce.entities.CodeViolation;
import com.tcvcog.tcvce.entities.CodeViolationDisplayable;
import com.tcvcog.tcvce.entities.Icon;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.NoticeOfViolation;
import com.tcvcog.tcvce.entities.TextBlock;
import com.tcvcog.tcvce.entities.search.SearchParamsCECase;
import com.tcvcog.tcvce.entities.Blob;
import com.tcvcog.tcvce.entities.CodeViolationPropCECaseHeavy;
import com.tcvcog.tcvce.entities.search.SearchParamsCodeViolation;
import com.tcvcog.tcvce.entities.BlobLight;
import com.tcvcog.tcvce.entities.CECaseOneClickTemplate;
import com.tcvcog.tcvce.entities.CitationCECasePropertyHeavy;
import com.tcvcog.tcvce.entities.IFace_transferrable;
import com.tcvcog.tcvce.entities.NoticeOfViolationTemplate;
import com.tcvcog.tcvce.entities.NoticeOfViolationType;
import com.tcvcog.tcvce.entities.Property;
import com.tcvcog.tcvce.entities.TextBlockCategory;
import com.tcvcog.tcvce.entities.search.SearchParamsCitation;
import com.tcvcog.tcvce.entities.search.SearchParamsDateRule;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import com.tcvcog.tcvce.entities.CaseActionDueBySourceEnum;
import com.tcvcog.tcvce.entities.CaseActionDueByUrgencyEnum;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.time.LocalDateTime;

/**
 *
 * @author ellen bascomb of apt 31y
 */
@Named("caseIntegrator")
@ApplicationScoped 
public class CaseIntegrator extends BackingBeanUtils implements Serializable{

    
    @Inject
    private CECaseCacheManager ceCaseCacheManager;
    
    /**
     * Creates a new instance of CaseIntegrator
     */
    public CaseIntegrator() {
    }
   
    /**
     * Single focal point of serach method for Code Enforcement case using a SearchParam
     * subclass. Outsiders will use runQueryCECase or runQueryCECase
     * @param params
     * @return a list of CECase IDs
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public List<Integer> searchForCECases(SearchParamsCECase params) throws IntegrationException, BObStatusException{
        SearchCoordinator sc = getSearchCoordinator();
        List<Integer> cseidlst = new ArrayList<>();
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        
        params.appendSQL("SELECT DISTINCT caseid \n");
        params.appendSQL("FROM public.cecase \n");
        params.appendSQL("INNER JOIN public.parcel ON (cecase.parcel_parcelkey = parcel.parcelkey) \n");
        // These clauses are to deprecate the property info case structure and person info case structure
        // (These used to be containers basically for storing events about parcels and persons using CE cases)
        params.appendSQL("WHERE caseid IS NOT NULL ");
        
        // *******************************
        // **         BOb ID            **
        // *******************************
         if (!params.isBobID_ctl()) {

            //*******************************
            // **   MUNI,DATES,USER,ACTIVE  **
            // *******************************
            params = (SearchParamsCECase) sc.assembleBObSearchSQL_muniDatesUserActive(params, 
                                                                SearchParamsCECase.MUNI_DBFIELD,
                                                                SearchParamsCECase.CECASE_ACTIVE_FIELD);
            
            // *******************************
            // **       1:OPEN/CLOSED       **
            // *******************************
            if (params.isCaseOpen_ctl()) {
                if(params.isCaseOpen_val()){
                    params.appendSQL("AND closingdate IS NULL ");
                } else {
                    params.appendSQL("AND closingdate IS NOT NULL ");
                }
            }
            
            // *******************************
            // **      2:PROPERTY           **
            // *******************************
             if (params.isProperty_ctl()) {
                if(params.getProperty_val()!= null){
                    params.appendSQL("AND parcel_parcelkey=? ");
                } else {
                    params.setProperty_ctl(false);
                    params.appendToParamLog("PARCEL: no parcel object; prop filter disabled");
                }
            }
            
            // *******************************
            // **     3:PROPERTY UNIT       **
            // *******************************
             if (params.isPropertyUnit_ctl()) {
                if(params.getPropertyUnit_val()!= null){
                    params.appendSQL("AND parcelunit_unitid=? ");
                } else {
                    params.setPropertyUnit_ctl(false);
                    params.appendToParamLog("PARCEL UNIT: no parcelunit object; propunit filter disabled");
                }
            }
            
          
            // *******************************
            // **     4.BOb SOURCE          **
            // *******************************
             if (params.isSource_ctl()) {
                if(params.getSource_val() != null){
                    params.appendSQL("AND bobsource_sourceid=? ");
                } else {
                    params.setSource_ctl(false);
                    params.appendToParamLog("SOURCE: no BOb source object; source filter disabled");
                }
            }
           
            // *******************************
            // **        5. PACC            **
            // *******************************
             if (params.isPacc_ctl()) {
                if(params.isPacc_val()){
                    params.appendSQL("AND paccenabled = TRUE ");
                } else {
                    params.appendSQL("AND paccenabled = TRUE ");
                }
            }
            
            
        } else {
            params.appendSQL(" AND caseid = ? "); // will be param 1 with ID search
        }

        int paramCounter = 0;
            
        try {
            stmt = con.prepareStatement(params.extractRawSQL());

            if (!params.isBobID_ctl()) {
                if (params.isMuni_ctl()) {
                     stmt.setInt(++paramCounter, params.getMuni_val().getMuniCode());
                }
                
                // adapted to possible use of date rule list
                if(params.isDate_startEnd_ctl()){
                    if(params.getDateRuleList() != null && !params.getDateRuleList().isEmpty()){
                        for(SearchParamsDateRule dr: params.getDateRuleList()){
                            if(dr.isDate_null_ctl()){
                                // no injection needed
                                System.out.println("CaseIntegrator.searchForCases: Found null date rule; no injection: " + params.getFilterName());
                            } else { // inject dates
                                if(dr.getDate_start_val() != null && dr.getDate_end_val() != null){
                                    stmt.setTimestamp(++paramCounter, java.sql.Timestamp.valueOf(dr.getDate_start_val()));
                                    stmt.setTimestamp(++paramCounter, java.sql.Timestamp.valueOf(dr.getDate_end_val()));
                                } else {
                                    System.out.println("CaseIntegrator.searchForCases: Found null start or end date vals: " + params.getFilterName());
                                }
                            }
                        }
                    } else { // legacy no date rule list
                        stmt.setTimestamp(++paramCounter, params.getDateStart_val_sql());
                        stmt.setTimestamp(++paramCounter, params.getDateEnd_val_sql());
                    }
                 }
                
                if (params.isUser_ctl()) {
                   stmt.setInt(++paramCounter, params.getUser_val().getUserID());
                }
                
                if (params.isProperty_ctl()) {
                    stmt.setInt(++paramCounter, params.getProperty_val().getParcelKey());
                }
                
                if (params.isPropertyUnit_ctl()) {
                    stmt.setInt(++paramCounter, params.getPropertyUnit_val().getUnitID());
                }
                
                 
                if(params.isSource_ctl()){
                    stmt.setInt(++paramCounter, params.getSource_val().getSourceid());
                }

            } else {
                stmt.setInt(++paramCounter, params.getBobID_val());
            }
            
            rs = stmt.executeQuery();

            int counter = 0;
            int maxResults;
            if (params.isLimitResultCount_ctl()) {
                maxResults = params.getLimitResultCount_val();
            } else {
                maxResults = Integer.MAX_VALUE;
            }
            while (rs.next() && counter < maxResults) {
                cseidlst.add(rs.getInt("caseid"));
                counter++;
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot search for code enf cases, sorry!", ex);
            
        } finally{
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return cseidlst;
    }
    
    
    /**
     * Single focal point of search method for code violations using a SearchParam
     * subclass. Outsiders will use runQueryCodeViolation
     * @param params
     * @return a list of CodeViolationPropCECaseHeavy
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public List<CodeViolationPropCECaseHeavy> searchForCodeViolations(SearchParamsCodeViolation params) throws IntegrationException, BObStatusException{
        SearchCoordinator sc = getSearchCoordinator();
        CaseCoordinator cc = getCaseCoordinator();
        List<CodeViolationPropCECaseHeavy> cvpcehl = new ArrayList<>();
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        params.appendSQL("SELECT DISTINCT codeviolation.violationid, cecase.caseid, cecase.casename, parcel.parcelkey, municipality.municode, municipality.muniname ");
        params.appendSQL("FROM public.codeviolation  ");
        params.appendSQL("INNER JOIN public.cecase ON (cecase.caseid = codeviolation.cecase_caseid) ");
        params.appendSQL("INNER JOIN public.parcel ON (cecase.parcel_parcelkey = parcel.parcelkey) ");
        params.appendSQL("INNER JOIN public.municipality ON (parcel.muni_municode = municipality.municode) ");
        params.appendSQL("LEFT OUTER JOIN  ");
        params.appendSQL("(	SELECT codeviolation_violationid, citation.citationid, citation.dateofrecord ");
        params.appendSQL("FROM public.citationviolation  ");
        params.appendSQL("INNER JOIN public.citation ON (citationviolation.citation_citationid = citation.citationid) ");
        // commented out to fix Alex's bug: zero cited violations on his report
//        params.appendSQL("INNER JOIN public.citationstatus on (citationstatus.statusid = citation.status_statusid) ");
//        params.appendSQL("WHERE citationstatus.editsforbidden = TRUE	 ");
        params.appendSQL("WHERE citation.deactivatedts IS NULL AND citationviolation.deactivatedts IS NULL ");
        params.appendSQL(") AS citv ON (codeviolation.violationid = citv.codeviolation_violationid) ");
        params.appendSQL("LEFT OUTER JOIN  ");
        params.appendSQL("( ");
        params.appendSQL("SELECT codeviolation_violationid, sentdate ");
        params.appendSQL("FROM noticeofviolationcodeviolation ");
        params.appendSQL("INNER JOIN public.noticeofviolation ON (noticeofviolationcodeviolation.noticeofviolation_noticeid = noticeofviolation.noticeid) ");
        params.appendSQL("WHERE noticeofviolation.sentdate IS NOT NULL AND noticeofviolation.active = TRUE");
        params.appendSQL(") AS novcv ON (codeviolation.violationid = novcv.codeviolation_violationid) ");
        params.appendSQL("WHERE violationid IS NOT NULL ");
        
        // *******************************
        // **         BOb ID            **
        // *******************************
         if (!params.isBobID_ctl()) {

            //*******************************
            // **   MUNI,DATES,USER,ACTIVE  **
            // *******************************
            params = (SearchParamsCodeViolation) sc.assembleBObSearchSQL_muniDatesUserActive(params, 
                                                                SearchParamsCodeViolation.DBFIELD_MUNI,
                                                                SearchParamsCodeViolation.DBFIELD_ACTIVE);
            
            
            // *******************************
            // **     1.PROPERTY          **
            // *******************************
             if (params.isProperty_ctl()) {
                if(Objects.nonNull(getSessionBean().getSessProperty())){
                    params.appendSQL("AND parcel.parcelkey=? ");
                } else {
                    params.setProperty_ctl(false);
                    params.appendToParamLog("PROPERTY PARAM: no property given; filter disabled");
                }
            }
            
            
            // *******************************
            // **     2. CITED              **
            // *******************************
             if (params.isCited_ctl()) {
                if(params.isCited_val()){
                    params.appendSQL("AND citv.citationid IS NOT NULL ");
                } else {
                    params.appendSQL("AND citv.citationid IS NULL ");
                }
            }
            
            // *******************************
            // **     3.LEGACY IMPORT       **
            // *******************************
             if (params.isLegacyImport_ctl()) {
                if(params.isLegacyImport_val()){
                    params.appendSQL("AND legacyimport=TRUE ");
                } else {
                    params.appendSQL("AND legacyimport=FALSE ");
                   
                }
            }
            
            // *******************************
            // **     4.SEVERITY            **
            // *******************************
             if (params.isSeverity_ctl()) {
                if(params.getSeverity_val() != null){
                    params.appendSQL("AND severity_classid=? ");
                } else {
                     params.setSeverity_ctl(false);
                    params.appendToParamLog("SEVERITY: no severity/intensity source object; severity filter disabled");
                }
            }
            
            // *******************************
            // **     5.BOb SOURCE          **
            // *******************************
             if (params.isSource_ctl()) {
                if(params.getSource_val() != null){
                    params.appendSQL("AND cecase.bobsource_sourceid=? ");
                } else {
                    params.setSource_ctl(false);
                    params.appendToParamLog("SOURCE: no BOb source object; source filter disabled");
                }
            }
             
            // *******************************
            // **     6.notice mailed       **
            // *******************************
             if (params.isNoticeMailed_ctl()) {
                if(params.isNoticeMailed_val()){
                    params.appendSQL("AND novcv.sentdate IS NOT NULL ");
                } else {
                    params.appendSQL("AND novcv.sentdate IS NULL ");
                }
            }
             
            // *******************************
            // **     7. transfer           **
            // *******************************
             if (params.isTransferred_ctl()) {
                if(params.isTransferred_val()){
                    params.appendSQL("AND codeviolation.transferredts IS NOT NULL ");
                } else {
                    params.appendSQL("AND codeviolation.transferredts IS NULL ");
                }
            }
             
            // *******************************
            // **     8. compliance         **
            // *******************************
             if (params.isCompliance_ctl()) {
                if(params.isCompliance_val()){
                    params.appendSQL("AND codeviolation.compliancetimestamp IS NOT NULL ");
                } else {
                    params.appendSQL("AND codeviolation.compliancetimestamp IS NULL ");
                }
            }
             
             
            // *******************************
            // **     9. nullified         **
            // *******************************
             if (params.isNullified_ctl()) {
                if(params.isNullified_val()){
                    params.appendSQL("AND codeviolation.nullifiedts IS NOT NULL ");
                } else {
                    params.appendSQL("AND codeviolation.nullifiedts IS NULL ");
                }
            }
             
             
           
            
            
        } else {
            params.appendSQL("violationid = ? "); // will be param 1 with ID search
        }

        int paramCounter = 0;
            
        try {
            stmt = con.prepareStatement(params.extractRawSQL());

            if (!params.isBobID_ctl()) {
                if (params.isMuni_ctl()) {
                     stmt.setInt(++paramCounter, params.getMuni_val().getMuniCode());
                }
                
                if(params.isDate_startEnd_ctl()){
                    stmt.setTimestamp(++paramCounter, params.getDateStart_val_sql());
                    stmt.setTimestamp(++paramCounter, params.getDateEnd_val_sql());
                 }
                
                if (params.isUser_ctl()) {
                   stmt.setInt(++paramCounter, params.getUser_val().getUserID());
                }
                // Violation set 1
                if (params.isProperty_ctl()) {
                    stmt.setInt(++paramCounter, getSessionBean().getSessProperty().getPropertyID());
                }
                
                // violation set 4
                if(params.isSeverity_ctl()){
                    stmt.setInt(++paramCounter, params.getSeverity_val().getClassID());
                }
                
                // violation set 5
                if(params.isSource_ctl()){
                    stmt.setInt(++paramCounter, params.getSource_val().getSourceid());
                }

            } else {
                stmt.setInt(++paramCounter, params.getBobID_val());
            }
            
            rs = stmt.executeQuery();

            int counter = 0;
            int maxResults;
            if (params.isLimitResultCount_ctl()) {
                maxResults = params.getLimitResultCount_val();
            } else {
                maxResults = Integer.MAX_VALUE;
            }
            
            while (rs.next() && counter < maxResults) {
                cvpcehl.add(generateCodeViolationPropCECaseHeavy(rs));
                counter++;
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot search for violations, sorry!", ex);
            
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        
        return cvpcehl;
        
    }
    
    /**
     * Internal generator for property heavy Code Violations
     * @param rs
     * @return 
     */
    private CodeViolationPropCECaseHeavy generateCodeViolationPropCECaseHeavy(ResultSet rs) throws IntegrationException, SQLException, BObStatusException{
        if(rs == null){
            throw new IntegrationException("Cannot generate CV with null RS");
        }

        CaseCoordinator cc = getCaseCoordinator();
        PropertyCoordinator pc = getPropertyCoordinator();
        
        CodeViolationPropCECaseHeavy cvpch = new CodeViolationPropCECaseHeavy(cc.violation_getCodeViolation(rs.getInt("violationid")));
        cvpch.setCeCaseName(rs.getString("casename"));
        Property prop;
        try {
            prop = pc.getProperty(rs.getInt("parcelkey"));
        } catch (AuthorizationException ex) {
            throw new BObStatusException(ex.getMessage());
        }
        if(prop != null && prop.getAddress() != null){
            cvpch.setPropertyAddress(prop.getAddress().getAddressPretty2LineEscapeFalse());
        } else {
            System.out.println("CaseIntegrator.generateCodeViolationPropCECaseHeavy | could not build address for parcel id: " + rs.getInt("parcelkey"));
        }
        cvpch.setPropertyID(rs.getInt("parcelkey"));
        cvpch.setMuniCode(rs.getInt("municode"));
        cvpch.setMuniName(rs.getString("muniname"));
        
        return cvpch;
        
    }
    
    /**
     * Single focal point of search method for citations using a SearchParam
     * subclass.
     *
     * @param params
     * @return a list of citationCECasePropertyHeavys
     */
    public List<CitationCECasePropertyHeavy> searchForCitations(SearchParamsCitation params) {
        CaseCoordinator cc = getCaseCoordinator();
        SearchCoordinator sc = getSearchCoordinator();
        List<CitationCECasePropertyHeavy> citationCECasePropertyHeavys = new ArrayList<>();
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        params.appendSQL("SELECT DISTINCT ON(ct.citationid) ct.citationid ");
        params.appendSQL("FROM citation ct ");
        params.appendSQL("LEFT JOIN citationdocketno cd ON cd.citation_citationid = ct.citationid ");
        params.appendSQL("LEFT JOIN citationviolation citv ON citv.citation_citationid = ct.citationid ");
        params.appendSQL("LEFT JOIN codeviolation cv ON cv.violationid = citv.codeviolation_violationid ");
        params.appendSQL("LEFT JOIN cecase cec ON cec.caseid = cv.cecase_caseid ");
        params.appendSQL("LEFT JOIN parcel p ON p.parcelkey = cec.parcel_parcelkey ");
        params.appendSQL("LEFT JOIN codesetelement codsv ON codsv.codesetelementid = cv.codesetelement_elementid ");
        params.appendSQL("LEFT JOIN codeelement codele ON codele.elementid = codsv.codelement_elementid ");
        params.appendSQL("LEFT JOIN citationcitationstatus ccs ON ccs.citation_citationid = ct.citationid ");
        params.appendSQL("LEFT JOIN citationstatus cs ON cs.statusid = ccs.citationstatus_statusid ");
        params.appendSQL("WHERE ct.citationid IS NOT NULL \n");
        params.appendSQL("AND ct.deactivatedts IS NULL \n");

        // *******************************
        // **         BOb ID            **
        // *******************************
        if (!params.isBobID_ctl())
        {

            //*******************************
            // **   MUNI,DATES,USER,ACTIVE  **
            // *******************************
            params = (SearchParamsCitation) sc.assembleBObSearchSQL_muniDatesUserActive(params,
                    SearchParamsCitation.DBFIELD_MUNI,
                    SearchParamsCitation.DBFIELD_ACTIVE);

            //*******************************
            // **      1.CITATION ID     **
            // *******************************
            if (params.isCitationId_ctl())
            {
                if (Objects.nonNull(params.getCitationId_val()))
                {
                    params.appendSQL("AND ct.citationid=? ");
                } else
                {
                    params.setCitationId_ctl(false);
                    params.appendToParamLog("CITATION ID: no ID specified; citation id filter disabled; |");
                }
            }

            //*******************************
            // **      2.DOCKET NO    **
            // *******************************
            if (params.isDocketNo_ctl())
            {
                if (Objects.nonNull(params.getDocketNo_val()) && !params.getDocketNo_val().isEmpty())
                {
                    params.appendSQL("AND cd.docketno=? AND cd.deactivatedts IS NULL ");
                } else
                {
                    params.setDocketNo_ctl(false);
                    params.appendToParamLog("DOCKET NO: no docket number specified; docket no filter disabled; |");
                }
            }

            //********************************
            // **3.CITATION VIOLATION STATUS**
            // *******************************
            if (params.isLinkStatus_ctl())
            {
                if (Objects.nonNull(params.getLinkStatus_val()))
                {
                    params.appendSQL("AND citv.status=? ::citationviolationstatus AND citv.deactivatedts IS NULL ");
                } else
                {
                    params.setLinkStatus_ctl(false);
                    params.appendToParamLog("VIOLATION STATUS: no violationstatus specified; violation status filter disabled; |");
                }
            }

            //********************************
            // **4.CITATION LOG ENTRY STATUS**
            // *******************************
            if (params.isLogEntryStatus_ctl())
            {
                if (Objects.nonNull(params.getLogEntryStatus_val()))
                {
                    params.appendSQL("AND cs.statusname=? AND ccs.deactivatedts IS NULL ");
                } else
                {
                    params.setLogEntryStatus_ctl(false);
                    params.appendToParamLog("LOG ENTRY STATUS: no log entry status specified; log entry status filter disabled; |");
                }
            }

            //********************************
            // **5.Code Element**
            // *******************************
            if (params.isCodeElement_ctl())
            {
                if (Objects.nonNull(params.getCodeElement_val()))
                {
                    params.appendSQL("AND codele.elementid=? ");
                } else
                {
                    params.setCodeElement_ctl(false);
                    params.appendToParamLog("ORDINANCE: no ordinance specified; ordinance filter disabled; |");
                }
            }
            
            if(params.isCitationNumber_ctl()){
                params.appendSQL(" AND citationno ILIKE ? ");
            }

        } else
        {
            params.appendSQL("ct.citationid = ? "); // will be param 1 with ID search
        }
        int paramCounter = 0;
        params.appendSQL(";");

        try
        {
            stmt = con.prepareStatement(params.extractRawSQL());
            if (!params.isBobID_ctl())
            {
                if (params.isMuni_ctl())
                {
                    stmt.setInt(++paramCounter, params.getMuni_val().getMuniCode());
                }

                if (params.isDate_startEnd_ctl())
                {
                    stmt.setTimestamp(++paramCounter, params.getDateStart_val_sql());
                    stmt.setTimestamp(++paramCounter, params.getDateEnd_val_sql());
                }

                if (params.isUser_ctl())
                {
                    stmt.setInt(++paramCounter, params.getUser_val().getUserID());
                }

                if (params.isCitationId_ctl())
                {
                    stmt.setInt(++paramCounter, params.getCitationId_val());
                }
                if (params.isDocketNo_ctl())
                {
                    stmt.setString(++paramCounter, params.getDocketNo_val());
                }
                if (params.isLinkStatus_ctl())
                {
                    stmt.setString(++paramCounter, params.getLinkStatus_val().name());
                }
                if (params.isLogEntryStatus_ctl())
                {
                    stmt.setString(++paramCounter, params.getLogEntryStatus_val().getStatusTitle());
                }
                if (params.isCodeElement_ctl())
                {
                    stmt.setInt(++paramCounter, params.getCodeElement_val().getElementID());
                }
                if(params.isCitationNumber_ctl()){
                    stmt.setString(++paramCounter, params.getCitationNumber_val());
                }
            } else
            {
                stmt.setInt(++paramCounter, params.getBobID_val());
            }
            rs = stmt.executeQuery();
            int counter = 0;
            int maxResults;
            if (params.isLimitResultCount_ctl())
            {
                maxResults = params.getLimitResultCount_val();
            } else
            {
                maxResults = Integer.MAX_VALUE;
            }
            while (rs.next() && counter < maxResults)
            {
                CitationCECasePropertyHeavy cECasePropertyHeavy = new CitationCECasePropertyHeavy(cc.citation_getCitation(rs.getInt("citationid")));

                citationCECasePropertyHeavys.add(cECasePropertyHeavy);

                counter++;
            }
            System.out.println("Case Integrator: total citations: " + citationCECasePropertyHeavys.size());
        } catch (SQLException | IntegrationException | BObStatusException | BlobException ex)
        {
            System.out.println(ex.toString());
        } finally
        {
            releasePostgresConnection(con, stmt, rs);
        }

        return citationCECasePropertyHeavys;
    }

    
    /**
     * Generates a CECaseDataHeavy without the big, fat lists
     * @param ceCaseID
     * @return
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public CECase getCECase(int ceCaseID) throws IntegrationException, BObStatusException{
        String query = """
                       SELECT   caseid, cecasepubliccc, login_userid, 
                                casename, originationdate, closingdate, 
                                cecase.createdts, cecase.notes, paccenabled, 
                                allowuplinkaccess, cecase.bobsource_sourceid, cecase.lastupdatedts, 
                                parcelunit_unitid, cecase.parcel_parcelkey, cecase.createdby_umapid, 
                                cecase.lastupdatedby_umapid, cecase.deactivatedts, cecase.deactivatedby_umapid, oneclick_clickid,
                                actionduebydate, actionduebysource, actionduebyurgency, actionduebydescription, actionduebyrelatedsubobject, actionduebylog, actionduebylastupdatets, casestatus,
                                cnf_buildparceladdressstring(cecase.parcel_parcelkey, TRUE, TRUE) AS addressstring2linebreak,
                                parcelunit.unitnumber AS unitnumberstring, municipality.municode AS municode, parcel.parcelidcnty AS countyparcelid
                            FROM public.cecase 
                            	LEFT OUTER JOIN public.parcelunit ON (cecase.parcelunit_unitid = parcelunit.unitid)
                            	LEFT OUTER JOIN public.parcel ON (cecase.parcel_parcelkey = parcel.parcelkey)
                            	LEFT OUTER JOIN public.municipality ON (parcel.muni_municode = municipality.municode) 
                            WHERE caseid=?;
                       """;
        ResultSet rs = null;
        PreparedStatement stmt = null;
        Connection con = null;
        CECase c = null;
        
        try {
            
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, ceCaseID);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                c = generateCECase(rs);
            }
            
        } catch (SQLException | BlobException  ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot get cecase by id", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        
        return c;
    }
    
    /**
     * Internal generator for CECase objects
     * @param rs with all DB fields included
     * @return
     * @throws SQLException
     * @throws IntegrationException 
     */
     private CECase generateCECase(ResultSet rs) throws SQLException, IntegrationException, BlobException{
        try {
            UserCoordinator uc = getUserCoordinator();
            SystemIntegrator si = getSystemIntegrator();
            CaseCoordinator cc = getCaseCoordinator();
            MunicipalityCoordinator mc = getMuniCoordinator();
            
            CECase cse = new CECase();
            
            cse.setCaseID(rs.getInt("caseid"));
            cse.setPublicControlCode(rs.getInt("cecasepubliccc"));
            
            cse.setParcelKey(rs.getInt("parcel_parcelkey"));
            cse.setPropertyUnitID(rs.getInt("parcelunit_unitid"));
            cse.setPropertyAddress2LineFlat(rs.getString("addressstring2linebreak"));
            cse.setPropertyUnitNumberFlat(rs.getString("unitnumberstring"));
            cse.setMuni(mc.getMuni(rs.getInt("municode")));
            cse.setPropertyParcelIDCountyFlat(rs.getString("countyparcelid"));
            
            cse.setCaseManager(uc.user_getUser(rs.getInt("login_userid")));
            
            cse.setCaseName(rs.getString("casename"));
            
            if(rs.getTimestamp("originationdate") != null){
                cse.setOriginationDate(rs.getTimestamp("originationdate")
                        .toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
            }
            
            if(rs.getTimestamp("closingdate") != null){
                cse.setClosingDate(rs.getTimestamp("closingdate")
                        .toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
            }
            
            cse.setNotes(rs.getString("notes"));
            cse.setPaccEnabled(rs.getBoolean("paccenabled"));
            cse.setAllowForwardLinkedPublicAccess(rs.getBoolean("allowuplinkaccess"));
            
            if(rs.getInt("bobsource_sourceid") != 0){
                cse.setSource(si.getBOBSource(rs.getInt("bobsource_sourceid")));
            }
            if(rs.getInt("oneclick_clickid") != 0){
                cse.setOneClickTemplate(cc.cecase_getCECaseOneClickTemplate(rs.getInt("oneclick_clickid")));
            }
            
            // v3.1.x priority/status field
            if(rs.getTimestamp("actionduebydate") != null){
                cse.setActionDueBy(rs.getTimestamp("actionduebydate").toLocalDateTime());
            }
            if(rs.getString("actionduebysource") != null){
                cse.setActionDueSource(CaseActionDueBySourceEnum.valueOf(rs.getString("actionduebysource")));
            }
            if(rs.getString("actionduebyurgency") != null){
                cse.setActionDueUrgency(CaseActionDueByUrgencyEnum.valueOf(rs.getString("actionduebyurgency")));
            }
            cse.setActionDueByDescription(rs.getString("actionduebydescription"));
            if(rs.getString("actionduebylog") != null){
                StringBuilder sb = new StringBuilder(rs.getString("actionduebylog"));
                cse.setActionDueByLog(sb);
            }
            cse.setActionDueBySubObjectID(rs.getInt("actionduebyrelatedsubobject"));
            if(rs.getTimestamp("actionduebylastupdatets") != null){
                cse.setActionDueBy(rs.getTimestamp("actionduebylastupdatets").toLocalDateTime());
            }
            cse.setStatusXML(rs.getString("casestatus"));
            
            // end priority
            
            si.populateUMAPTrackedFields(cse, rs);
            
            return cse;
        } catch (BObStatusException | DatabaseFetchRuntimeException ex) {
            throw new IntegrationException(ex.toString());
        }
    }
    
     /**
      * First gen search method to be deprecated in Beta
      * @param pacc
      * @return
      * @throws IntegrationException
      * @throws BObStatusException 
      */
    public List<CECase> getCECasesByPACC(int pacc) throws IntegrationException, BObStatusException{
        CaseCoordinator cc = getCaseCoordinator();
        ArrayList<CECase> caseList = new ArrayList();
        String query = "SELECT caseid FROM public.cecase WHERE cecasepubliccc = ?;";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        
        try {
            
            stmt = con.prepareStatement(query);
            stmt.setInt(1, pacc);
            System.out.println("CaseIntegrator.getCECasesByPacc | sql: " + stmt.toString());
            rs = stmt.executeQuery();
            
            while(rs.next()){
                // TODO: fix me - I need to get cases from the coordinator
                caseList.add(getCECase(rs.getInt("caseid")));
                
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot search for cases by PACC, sorry", ex);
            
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        
        return caseList;
    }
    
 
    /**
     * Insertion point for CECaseDataHeavy objects; must be called by Coordinator who checks 
     * logic before sending to the DB. This method only copies from the passed in CECaseDataHeavy
     * into the SQL INSERT
     * 
     * @param ceCase
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public int insertNewCECase(CECase ceCase) throws IntegrationException, BObStatusException{
        String query = """
                      INSERT INTO public.cecase(
                                   	caseid, cecasepubliccc, 
                                        login_userid, casename, originationdate, 
                                        closingdate, createdts, notes, 
                                        paccenabled, allowuplinkaccess, 
                                        bobsource_sourceid, 
                                        lastupdatedts, parcelunit_unitid, parcel_parcelkey, 
                                        createdby_umapid, lastupdatedby_umapid, oneclick_clickid )
                                   	VALUES (DEFAULT, ?,  
                                                ?, ?, ?, 
                                                ?, now(), ?, 
                                                ?, ?,
                                                ?, 
                                                now(), ?, ?, 
                                                ?, ?, ?);
                                            """;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int insertedCaseID = 0;
        Connection con = null;
        
        try {
            
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, ceCase.getPublicControlCode());
            
            if(ceCase.getCaseManager() != null){
                stmt.setInt(2, ceCase.getCaseManager().getUserID());  
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            stmt.setString(3, ceCase.getCaseName());
            if(ceCase.getOriginationDate() != null){
                stmt.setTimestamp(4, java.sql.Timestamp.valueOf(ceCase.getOriginationDate()));
            } else {
                stmt.setNull(4, java.sql.Types.NULL);                
            }
            // closing date is by definition null at insertion
            stmt.setNull(5, java.sql.Types.NULL); 
            // createdts by now()
            stmt.setString(6, ceCase.getNotes());
            
            stmt.setBoolean(7, ceCase.isPaccEnabled());
            stmt.setBoolean(8, ceCase.isAllowForwardLinkedPublicAccess());
            
            if(ceCase.getSource() != null){
                stmt.setInt(9, ceCase.getSource().getSourceid());
            } else {
                stmt.setNull(9, java.sql.Types.NULL); 
            }
            
            if(ceCase.getPropertyUnitID() != 0) {
                stmt.setInt(10, ceCase.getPropertyUnitID());
            } else { 
                stmt.setNull(10, java.sql.Types.NULL); 
            }
            stmt.setInt(11, ceCase.getParcelKey());
            
            if(ceCase.getCreatedby_UMAP()!= null){
                stmt.setInt(12, ceCase.getCreatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(12, java.sql.Types.NULL); 
            }
            if(ceCase.getLastUpdatedby_UMAP()!= null){
                stmt.setInt(13, ceCase.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(13, java.sql.Types.NULL); 
            }
            if(ceCase.getOneClickTemplate() != null){
                stmt.setInt(14, ceCase.getOneClickTemplate().getOneClickID());
            } else {
                stmt.setNull(14, java.sql.Types.NULL);
            }
            
            stmt.execute();
            
            String retrievalQuery = "SELECT currval('cecase_caseID_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                insertedCaseID = rs.getInt(1);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Case Integrator: FATAL INSERT error; apologies.", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        
        return insertedCaseID;
        
    }
    
    /**
     * Updates the values in the CECaseDataHeavy in the DB but does NOT
     * edit the data in connected tables, namely CodeViolation, EventCnF, and Person
     * Use calls to other add methods in this class for adding additional
     * violations, events, and people to a CE case.
     * 
     * Method will update the case's last udpated time stamp and user, if supplied
     * by the case object's fields
     * 
     * @param ceCase the case to updated, with updated member variables
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void updateCECaseMetadata(CECase ceCase) throws IntegrationException{
        String query =  """
                        UPDATE public.cecase
                           SET cecasepubliccc=?, parcel_parcelkey=?, parcelunit_unitid=?, 
                               casename=?, originationdate=?, closingdate=?, 
                               notes=?, paccenabled=?, allowuplinkaccess=?, 
                               bobsource_sourceid=?, 
                               lastupdatedts=now(), lastupdatedby_umapid=?
                          WHERE caseid=?;""" ;
        PreparedStatement stmt = null;
        Connection con = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, ceCase.getPublicControlCode());
            stmt.setInt(2, ceCase.getParcelKey());
            
            if(ceCase.getPropertyUnitID() != 0){
                stmt.setInt(3, ceCase.getPropertyUnitID());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
          
            if(ceCase.getCaseName() != null){
                stmt.setString(4, ceCase.getCaseName());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            if(ceCase.getOriginationDate() != null){
                stmt.setTimestamp(5, java.sql.Timestamp.valueOf(ceCase.getOriginationDate()));
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            if(ceCase.getClosingDate() != null){
                stmt.setTimestamp(6, java.sql.Timestamp.valueOf(ceCase.getClosingDate())); 
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            
            stmt.setString(7, ceCase.getNotes());
            stmt.setBoolean(8, ceCase.isPaccEnabled());
            stmt.setBoolean(9, ceCase.isAllowForwardLinkedPublicAccess());
            
            if(ceCase.getSource() != null){
                stmt.setInt(10, ceCase.getSource().getSourceid());
            } else {
                stmt.setNull(10, java.sql.Types.NULL);
            }
            
            if(ceCase.getLastUpdatedby_UMAP()!= null){
                stmt.setInt(11, ceCase.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(11, java.sql.Types.NULL);
            }
            
            stmt.setInt(12, ceCase.getCaseID());
            
            stmt.executeUpdate();
            
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot udpate case due to a database storage issue", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
        
    }
    
    /**
     * Single field updater method for a new case manager
     * @param ceCase containing the updated manger injected into case
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void updateCECaseManager(CECase ceCase) throws IntegrationException{
         String query =  """
                        UPDATE public.cecase
                           SET login_userid=?, lastupdatedby_umapid=?, lastupdatedts=now() 
                          WHERE caseid=?;""" ;
        PreparedStatement stmt = null;
        Connection con = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
                      
            if(ceCase.getCaseManager() != null){
                stmt.setInt(1, ceCase.getCaseManager().getUserID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
             if(ceCase.getLastUpdatedby_UMAP()!= null){
                stmt.setInt(2, ceCase.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            stmt.setInt(3, ceCase.getCaseID());
            
            stmt.executeUpdate();
            
              
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot udpate case manager", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
        
    }
    
    
    /**
     * Updates the values in the generation 2 cecase action date fields
     * @param ceCase
     * @throws IntegrationException 
     */
    public void updateCECaseActionDateFields(CECase ceCase) throws IntegrationException{
        if(ceCase == null || ceCase.getCaseID() == 0){
            throw new IntegrationException("Cannot update a null cecase or a cecase with id 0");
        }
         String query =  """
                        UPDATE public.cecase
                           SET  actionduebydate=?, actionduebysource=?, actionduebyurgency=?, 
                                actionduebydescription=?, actionduebyrelatedsubobject=?, actionduebylog=?, 
                                actionduebylastupdatets=now()
                          WHERE caseid=?;""" ;
        PreparedStatement stmt = null;
        Connection con = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            if(ceCase.getActionDueBy() != null){
                stmt.setTimestamp(1, java.sql.Timestamp.valueOf(ceCase.getActionDueBy()));
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }           
            if(ceCase.getActionDueSource() != null){
                stmt.setString(2, ceCase.getActionDueSource().name());
            }else {
                stmt.setNull(2, java.sql.Types.NULL);
            }   
            if(ceCase.getActionDueUrgency() != null){
                stmt.setString(3, ceCase.getActionDueUrgency().name());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }   
            stmt.setString(4, ceCase.getActionDueByDescription());
            if(ceCase.getActionDueBySubObjectID() != 0){
                stmt.setInt(5, ceCase.getActionDueBySubObjectID());
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }   
            stmt.setString(6, ceCase.getActionDueByLog().toString());
          
            stmt.setInt(7, ceCase.getCaseID());
            
            stmt.executeUpdate();
            
              
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot udpate case manager", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
    }
    
    
    /**
     * Updates the values in the generation 2 cecase action date fields
     * @param ceCase
     * @throws IntegrationException 
     */
    public void updateCECaseStatusXML(CECase ceCase) throws IntegrationException{
        if(ceCase == null || ceCase.getCaseID() == 0){
            throw new IntegrationException("Cannot update a null cecase or a cecase with id 0");
        }
         String query =  """
                            UPDATE public.cecase
                               SET  casestatus=?::xml
                              WHERE caseid=?; 
                         """ ;
        PreparedStatement stmt = null;
        Connection con = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
          
            stmt.setString(1, ceCase.getStatusXML());
          
            stmt.setInt(2, ceCase.getCaseID());
            
            stmt.executeUpdate();
              
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot udpate case manager", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
    }
    
   
    /**
     * Updates only the notes field on cecase table
     * @param cse with the Notes field as you want it inserted 
     * 
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public void updateCECaseNotes(CECase cse) 
            throws IntegrationException, BObStatusException{
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        if(cse == null){
            throw new BObStatusException("cannot update notes on a null case");
        }
        
        try {
            String s = "UPDATE public.cecase SET notes=?, lastupdatedts=now(), lastupdatedby_umapid=? WHERE caseid=?";
            stmt = con.prepareStatement(s);
            stmt.setString(1, cse.getNotes());
            if(cse.getLastUpdatedby_UMAP() != null){
                stmt.setInt(2, cse.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            stmt.setInt(3, cse.getCaseID());

            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update cecase notes", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        } 
        
    }
    
    /*  ***********************************************************************
     *  ***************         ONE CLICK CASE JUNK             ***************
     *  ***********************************************************************
     */
        
     
    /**
     * Builds a one-touch case template object
     * 
     * @param templateID
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public CECaseOneClickTemplate getCECaseOneClickTempalte(int templateID) throws IntegrationException, BObStatusException, BlobException{
        String query = """
                      SELECT    oneclickid, muni_municode, title, 
                                ordinance_codeseteleid, defaultcasename, orginationevent_catid, 
                                novtemplate_templateid, icon_iconid, deactivatedts, 
                                notes, displayorder, autofinalizenov
                      	FROM public.municipalityoneclickcase WHERE oneclickid=?;
                       """;
        ResultSet rs = null;
        PreparedStatement stmt = null;
        Connection con = null;
        CECaseOneClickTemplate template = null;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, templateID);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                template = generateCECaseOneClickTemplate(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot get cecase template by id", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        return template;
    }
    
    /**
     * Populates fields of the one-click template including its inner objects
     * @param rs
     * @return 
     */
    private CECaseOneClickTemplate generateCECaseOneClickTemplate(ResultSet rs) throws IntegrationException, SQLException, BObStatusException, BlobException{
        if(rs == null){
            throw new IntegrationException("Cannot populate from null RS");
        }
        MunicipalityCoordinator mc = getMuniCoordinator();
        CodeCoordinator cc =getCodeCoordinator();
        EventCoordinator ec = getEventCoordinator();
        CaseCoordinator caseCoor = getCaseCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        
        CECaseOneClickTemplate template = new CECaseOneClickTemplate();
        
        template.setOneClickID(rs.getInt("oneclickid"));
        template.setMuni(mc.getMuni(rs.getInt("muni_municode")));
        
        template.setTitle(rs.getString("title"));
        template.setDefaultCaseName(rs.getString("defaultcasename"));
        
        template.setViolatedOrdinance(cc.getEnforcableCodeElement(rs.getInt("ordinance_codeseteleid")));
        template.setOriginationEventCategory(ec.getEventCategory(rs.getInt("orginationevent_catid")));
        template.setNovTemplate(caseCoor.nov_getNoticeOfViolationTemplate(rs.getInt("novtemplate_templateid")));
        
        template.setIcon(sc.getIcon(rs.getInt("icon_iconid")));
        template.setNotes(rs.getString("notes"));
        
        if(rs.getTimestamp("deactivatedts") != null){
            template.setDeactivatedTS(rs.getTimestamp("deactivatedts").toLocalDateTime());
        }
        
        template.setDisplayOrder(rs.getInt("displayorder"));
        template.setAutoFinalizeNOV(rs.getBoolean("autofinalizenov"));
        
        return template;
    }
    
     
    /**
     * Builds a one-touch case template object
     * 
     * @param muni if null, all objects returned
     * @param includeDeactivated when true even records with non-null deacTS are returned
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public List<Integer> getCECaseOneClickTempalteListByMuni(Municipality muni, boolean includeDeactivated) throws IntegrationException, BObStatusException{
        StringBuilder sb = new StringBuilder("""
                                                SELECT    oneclickid
                                                FROM      public.municipalityoneclickcase WHERE oneclickid IS NOT NULL """);
        if(muni != null){
            sb.append(" AND muni_municode=? ");
        }
        if(includeDeactivated){
            sb.append(" AND deactivatedts IS NOT NULL ");
        } else {
            sb.append(" AND deactivatedts IS NULL ");
        }
        sb.append(";");
        
        ResultSet rs = null;
        PreparedStatement stmt = null;
        Connection con = null;
        List<Integer> templateIDList = new ArrayList<>();
        
        try {
            
            con = getPostgresCon();
            stmt = con.prepareStatement(sb.toString());
            if(muni != null){
                stmt.setInt(1, muni.getMuniCode());
            }
            rs = stmt.executeQuery();
            
            while(rs.next()){
                templateIDList.add(rs.getInt("oneclickid"));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot get cecase template by id", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        
        return templateIDList;
    }
    
    /**
     * Updates a single record of a caseTemplate
     * @param template belly objects cannot be null or you'll get a null pointer; use the audit method
     * on the case coordinator
     * @throws IntegrationException 
     */
    public void updateCECasOneClickTemplate(CECaseOneClickTemplate template) throws IntegrationException{
        if(template == null ){
            throw new IntegrationException("Cannot update null case template or template with null inner bits");
        }
         String query =  """
                         UPDATE public.municipalityoneclickcase
                         	SET muni_municode=?, title=?,
                                    ordinance_codeseteleid=?, defaultcasename=?, orginationevent_catid=?, 
                                    novtemplate_templateid=?, icon_iconid=?, deactivatedts=?, 
                                    notes=?, displayorder=?, autofinalizenov=?
                         	WHERE oneclickid=?;
                        """ ;
         
        PreparedStatement stmt = null;
        Connection con = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
        
            stmt.setInt(1, template.getMuni().getMuniCode());
            stmt.setString(2, template.getTitle());
            
            stmt.setInt(3, template.getViolatedOrdinance().getCodeSetElementID());
            stmt.setString(4, template.getDefaultCaseName());
            stmt.setInt(5, template.getOriginationEventCategory().getCategoryID());
            
            stmt.setInt(6, template.getNovTemplate().getTemplateID());
            stmt.setInt(7, template.getIcon().getID());
            
            if(template.getDeactivatedTS() != null){
                stmt.setTimestamp(8, java.sql.Timestamp.valueOf(template.getDeactivatedTS()));
            } else {
                stmt.setNull(8, java.sql.Types.NULL);
            }
            
            stmt.setString(9, template.getNotes());
 
            stmt.setInt(10, template.getDisplayOrder());
            stmt.setBoolean(11, template.isAutoFinalizeNOV());
            
            stmt.setInt(12, template.getOneClickID());
            
            
            
            stmt.executeUpdate();
            
              
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot udpate case template", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
        
    }
    
    /**
     * Creates a fresh record in the cecaseonetouchtemplate table
     * @param template
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
     public int insertCECaseOneClickTemplate(CECaseOneClickTemplate template) throws IntegrationException, BObStatusException{
        String query = """
                       INSERT INTO public.municipalityoneclickcase(
                                        oneclickid, muni_municode, title, 
                                        ordinance_codeseteleid, defaultcasename, orginationevent_catid, 
                                        novtemplate_templateid, icon_iconid, deactivatedts, 
                                        notes, displayorder, autofinalizenov)
                            VALUES (DEFAULT, ?, ?, 
                                    ?, ?, ?, 
                                    ?, ?, NULL, 
                                    ?, ?, ?);
                        """;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int freshTemplateID = 0;
        Connection con = null;
        
        try {
            
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
           
            stmt.setInt(1, template.getMuni().getMuniCode());
            stmt.setString(2, template.getTitle());
            
            stmt.setInt(3, template.getViolatedOrdinance().getCodeSetElementID());
            stmt.setString(4, template.getDefaultCaseName());
            stmt.setInt(5, template.getOriginationEventCategory().getCategoryID());
            
            stmt.setInt(6, template.getNovTemplate().getTemplateID());
            stmt.setInt(7, template.getIcon().getID());
            
            stmt.setString(8, template.getNotes());
            
            stmt.setInt(9, template.getDisplayOrder());
            stmt.setBoolean(10, template.isAutoFinalizeNOV());
            
            stmt.execute();
            
            String retrievalQuery = "SELECT currval('municipalityoneclickcase_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                freshTemplateID = rs.getInt(1);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Case Integrator: FATAL INSERT error on case templates; apologies.", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        
        return freshTemplateID;
        
    }
    
    
    /** ***********************************************************************
     *  ***************         UNSORTED JUNK                   ***************
     *  ***********************************************************************
     */
    
   
    /**
     * Updates only the notes field on citation table
     
     * 
     * @param cit
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public void updateCitationNotes(Citation cit) 
            throws IntegrationException, BObStatusException{
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        if(cit == null){
            throw new BObStatusException("cannot update notes on a null citation");
        }
        
        try {
            String s = "UPDATE public.citation SET notes=? WHERE citationid=?";
            stmt = con.prepareStatement(s);
            stmt.setString(1, cit.getNotes());
            stmt.setInt(2, cit.getCitationID());

            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update citation notes", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        } 
        
    }
   
    /**
     * Updates only the notes field on noticeofviolation table
     
     * 
     * @param nov
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public void novUpdateNotes(NoticeOfViolation nov) 
            throws IntegrationException, BObStatusException{
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        if(nov == null){
            throw new BObStatusException("cannot update notes on a null notice");
        }
        
        try {
            String s = "UPDATE public.noticeofviolation SET notes=? WHERE noticeid=?";
            stmt = con.prepareStatement(s);
            stmt.setString(1, nov.getNotes());
            stmt.setInt(2, nov.getNoticeID());

            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update NOV notes", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        } 
        
    }
    
   
 
    
   
    /**
     * Grabs caseids from the loginobjecthistory table
     * @param userID
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public List<Integer> getCECaseHistoryList(int userID) 
            throws IntegrationException, BObStatusException{
        List<Integer> cseidl = new ArrayList<>();
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            String s = "SELECT cecase_caseid, entrytimestamp FROM loginobjecthistory "
                    + "WHERE login_userid = ? "
                    + "AND cecase_caseid IS NOT NULL "
                    + "ORDER BY entrytimestamp DESC;";
            stmt = con.prepareStatement(s);
            stmt.setInt(1, userID);

            rs = stmt.executeQuery();
            
            while (rs.next()) {
                cseidl.add(rs.getInt("cecase_caseid"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to generate case history list", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return cseidl;
    }
      /**
     *
     * @param casephase
     * @return
     * @throws IntegrationException
     */
    public Icon getIcon(CasePhaseEnum casephase) throws IntegrationException {
        Connection con = getPostgresCon();
        SystemIntegrator si = getSystemIntegrator();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT iconid ");
        sb.append("FROM public.cecasestatusicon WHERE status=?::casephase;");
        Icon icon = null;

        try {
            stmt = con.prepareStatement(sb.toString());
            stmt.setString(1, casephase.toString());
            rs = stmt.executeQuery();
            while (rs.next()) {
                icon = si.getIcon(rs.getInt("iconid"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to generate icon from cecase phase enum", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        
        return icon;

    }
    
    
    
    // *************************************************************************
    // *            CODE VIOLATIONS                                            *
    // *************************************************************************
    

    /**
     * Creates a new record in the codeviolation table from a CodeViolation object
     * @param v
     * @return
     * @throws IntegrationException 
     */
    public int insertCodeViolation(CodeViolation v) throws IntegrationException {
        int lastID = 0;

        String query =  """
                        INSERT INTO public.codeviolation(
                                    violationid, codesetelement_elementid, cecase_caseid, 
                                    dateofrecord, stipulatedcompliancedate, penalty, 
                                    description, notes, legacyimport, 
                                    severity_classid,
                                    lastupdatedts, lastupdatedby_userid,
                                    createdts, createdby_userid)
                            VALUES (DEFAULT, ?, ?, 
                                    ?, ?, ?, 
                                    ?, ?, ?, 
                                    ?, 
                                    now(), ?, 
                                    now(), ?);
                        
                        """;
        Connection con = null;
        PreparedStatement stmt = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);

            stmt.setInt(1, v.getViolatedEnfElement().getCodeSetElementID());
            stmt.setInt(2, v.getCeCaseID());

            if(v.getDateOfRecord() != null){
                stmt.setTimestamp(3, java.sql.Timestamp.valueOf(v.getDateOfRecord()));
            } else {
                stmt.setTimestamp(3, java.sql.Timestamp.valueOf(LocalDateTime.now()));
            }
            stmt.setTimestamp(4, java.sql.Timestamp.valueOf(v.getStipulatedComplianceDate()));
            stmt.setDouble(5, v.getPenalty());

            stmt.setString(6, v.getDescription());
            stmt.setString(7, v.getNotes());
            stmt.setBoolean(8, v.isLeagacyImport());
            
            if(v.getSeverityIntensity() != null){
                stmt.setInt(9, v.getSeverityIntensity().getClassID());
            } else {
                stmt.setNull(9, java.sql.Types.NULL);
            }
            
            if(v.getCreatedBy() != null){
                stmt.setInt(10, v.getCreatedBy().getUserID());
            } else {
                stmt.setNull(10, java.sql.Types.NULL);
            }
            if(v.getLastUpdatedBy() != null){
                stmt.setInt(11, v.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(11, java.sql.Types.NULL);
            }
            
            stmt.execute();

            String idNumQuery = "SELECT currval('codeviolation_violationid_seq');";
            Statement s = con.createStatement();
            ResultSet rs;
            rs = s.executeQuery(idNumQuery);
            rs.next();
            lastID = rs.getInt(1);

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("cannot insert code violation, sorry.", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        } 
        
        return lastID;

    }
    
        /**
     * Pathway for updating CodeViolatio objects
     * @param v
     * @throws IntegrationException 
     */
    public void updateCodeViolation(CodeViolation v) throws IntegrationException {
        String query =  """
                         UPDATE public.codeviolation
                           SET codesetelement_elementid=?, cecase_caseid=?, dateofrecord=?, 
                               stipulatedcompliancedate=?, 
                               penalty=?, description=?, legacyimport=?, 
                               severity_classid=?, 
                               lastupdatedts=now(), lastupdatedby_userid=?, nullifiedts=?, nullifiedby=? 
                         WHERE violationid = ?;""";
        
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, v.getViolatedEnfElement().getCodeSetElementID());
            stmt.setInt(2, v.getCeCaseID());
            if(v.getDateOfRecord() != null){
                stmt.setTimestamp(3, java.sql.Timestamp.valueOf(v.getDateOfRecord()));
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            
            if(v.getStipulatedComplianceDate()!= null){
                stmt.setTimestamp(4, java.sql.Timestamp.valueOf(v.getStipulatedComplianceDate()));
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            
            stmt.setDouble(5, v.getPenalty());
            stmt.setString(6, v.getDescription());
            stmt.setBoolean(7, v.isLeagacyImport());
            
            if(v.getSeverityIntensity() != null){
                stmt.setInt(8, v.getSeverityIntensity().getClassID());
            } else {
                stmt.setNull(8, java.sql.Types.NULL);
            }
            
            if(v.getLastUpdatedBy() != null){
                stmt.setInt(9, v.getLastUpdatedBy().getUserID());
            }else {
                stmt.setNull(9, java.sql.Types.NULL);
            }
            
            if(v.getNullifiedTS() != null){
                stmt.setTimestamp(10, java.sql.Timestamp.valueOf(v.getNullifiedTS()));
            } else {
                stmt.setNull(10, java.sql.Types.NULL);
            }
            
            if(v.getNullifiedUser() != null){
                stmt.setInt(11, v.getNullifiedUser().getUserID());
            } else {
                stmt.setNull(11, java.sql.Types.NULL);
            }
            
            stmt.setInt(12, v.getViolationID());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("cannot update code violation, sorry.", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        } 
    }
    
    
    /**
     * Pathway for recording compliance data on a single code violation record
     * @param v
     * @throws IntegrationException 
     */
    public void updateCodeViolationCompliance(CodeViolation v) throws IntegrationException {
        String query =  """
                         UPDATE public.codeviolation
                           SET actualcompliancedate=?, compliancenote=?, 
                               complianceuser=?, compliancetimestamp=now(), 
                               lastupdatedts=now(), lastupdatedby_userid=? 
                         WHERE violationid = ?;""" ;
        
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            if(v.getActualComplianceDate() != null){
                stmt.setTimestamp(1, java.sql.Timestamp.valueOf(v.getActualComplianceDate()));
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            
            stmt.setString(2, v.getComplianceNote());
            
            if(v.getComplianceUser() != null){
                stmt.setInt(3, v.getComplianceUser().getUserID());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            
            if(v.getLastUpdatedBy() != null){
                stmt.setInt(4, v.getLastUpdatedBy().getUserID());
            }else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            
            stmt.setInt(5, v.getViolationID());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Integration error: cannot record violation compliance, sorry.", ex);

        } finally {
            releasePostgresConnection(con, stmt);
        } 
    }
    
    /**
     * Generator method for CodeViolation objects
     * @param rs
     * @return
     * @throws SQLException
     * @throws IntegrationException
     */
    private CodeViolation generateCodeViolationFromRS(ResultSet rs) 
            throws SQLException, 
            IntegrationException,
            BObStatusException
            {

        CodeViolation v = new CodeViolation();
        CodeIntegrator ci = getCodeIntegrator();
        UserCoordinator uc = getUserCoordinator();
        WorkflowCoordinator wc = getWorkflowCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        SystemIntegrator si = getSystemIntegrator();
        
        v.setViolationID(rs.getInt("violationid"));
        v.setViolatedEnfElement(ci.getEnforcableCodeElement(rs.getInt("codesetelement_elementid")));
        
        v.setCeCaseID(rs.getInt("cecase_caseid"));
        if(rs.getTimestamp("dateofrecord") != null){
            v.setDateOfRecord(rs.getTimestamp("dateofrecord").toInstant()
                    .atZone(ZoneId.systemDefault()).toLocalDateTime());
        }

        if(rs.getTimestamp("stipulatedcompliancedate") != null){
            
        v.setStipulatedComplianceDate(rs.getTimestamp("stipulatedcompliancedate").toInstant()
                .atZone(ZoneId.systemDefault()).toLocalDateTime());
        }
                
        if (!(rs.getTimestamp("actualcompliancedate") == null)) {
            v.setActualComplianceDate(rs.getTimestamp("actualcompliancedate").toInstant()
                    .atZone(ZoneId.systemDefault()).toLocalDateTime());
        }

        v.setPenalty(rs.getDouble("penalty"));
        v.setDescription(rs.getString("description"));
        v.setNotes(rs.getString("notes"));
        v.setLeagacyImport(rs.getBoolean("legacyimport"));

        if(rs.getTimestamp("compliancetimestamp") != null){
            v.setComplianceTimeStamp(rs.getTimestamp("compliancetimestamp").toLocalDateTime());
            v.setComplianceUser(uc.user_getUser(rs.getInt("complianceUser")));
        }
        
        v.setComplianceNote(rs.getString("compliancenote"));
        
        if(rs.getTimestamp("nullifiedts") != null){
            v.setNullifiedTS(rs.getTimestamp("nullifiedts").toLocalDateTime());
        } else {
            v.setNullifiedTS(null);
        }

        if(rs.getInt("nullifiedby") != 0){
            v.setNullifiedUser(uc.user_getUser(rs.getInt("nullifiedby")));
        } 
        
        if(rs.getInt("severity_classid") != 0){
            v.setSeverityIntensity(sc.getIntensityClass(rs.getInt("severity_classid")));
        }
        
        List<BlobLight> blobList = new ArrayList<>();

        v.setBlobList(blobList);
        
        si.populateTrackedFields(v, rs, false);
        populateTransferrableFields(v, rs);
        
        return v;
    }

    /**
     * Cached-backed getter method for CodeViolation objects
     * @param violationID
     * @return
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public CodeViolation getCodeViolation(int violationID) 
            throws IntegrationException, BObStatusException {
        if(violationID == 0){
            throw new IntegrationException("CaseIntegrator.getCodeViolation | Cannot getCodeViolation with id = 0");
        }
        if(ceCaseCacheManager.isCachingEnabled()){
            return ceCaseCacheManager.getCacheViolation().get(violationID, k -> fetchCodeViolation(violationID));
        } else {
            return fetchCodeViolation(violationID);
        }
    }

    /**
     * DB extraction method for CodeViolation objects
     * @param violationID
     * @return
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public CodeViolation fetchCodeViolation(int violationID) 
            throws DatabaseFetchRuntimeException {

        String query = """
                       SELECT   violationid, codesetelement_elementid, cecase_caseid, 
                                dateofrecord, createdts, stipulatedcompliancedate, 
                                actualcompliancedate, penalty, description, 
                                notes, legacyimport, compliancetimestamp, 
                                complianceuser, severity_classid, createdby_userid, 
                                compliancetfexpiry_proposalid, lastupdatedts, lastupdatedby_userid, 
                                compliancenote, nullifiedts, 
                                nullifiedby, bobsource_sourceid, transferredts, 
                                transferredby_userid, transferredtocecase_caseid, 
                                deactivatedts, deactivatedby_userid
                       	FROM public.codeviolation WHERE violationid = ?
                       """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        CodeViolation cv = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, violationID);
            rs = stmt.executeQuery();

            while (rs.next()) {
                cv = generateCodeViolationFromRS(rs);
            }

        } catch (SQLException | BObStatusException | IntegrationException ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("cannot fetch code violation by ID, sorry.", ex);

        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return cv;
    }

    /**
     * Queries for all violation attached to a specific CECase
     * @param cse
     * @return
     * @throws IntegrationException 
     */
    public List<Integer> getCodeViolations(CECase cse) throws IntegrationException, BObStatusException {
        if(cse == null){
            throw new BObStatusException("Cannot get violations by case with null case!");
            
        }
        
        String query = "SELECT violationid FROM codeviolation WHERE cecase_caseid = ? AND deactivatedts IS NULL;";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        List<Integer> idl = new ArrayList();

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, cse.getCaseID());
            rs = stmt.executeQuery();

            while (rs.next()) {
                idl.add(rs.getInt("violationid"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("CaseInt.getCodeViolations(CECase): Cannot fetch code violations by CECase, sorry.", ex);

        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return idl;
    }
    
    /**
     * Utility adaptor method for retrieving multiple code violations given a CECaseDataHeavy
     * @param c
     * @return
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public List<Integer> getCodeViolationIDList(CECaseDataHeavy c) throws IntegrationException, BObStatusException {
        return getCodeViolations(c);
    }
    
    /**
     * TODO: NADGIT Fix the BlobList References
     * @param cv
     * @return
     * @throws IntegrationException 
     */
    public List<Blob> loadViolationPhotoList(CodeViolation cv) throws IntegrationException{
        
        List<Blob> vBlobList = new ArrayList<>();
        BlobCoordinator bc = getBlobCoordinator();
        
        String query = "SELECT photodoc_photodocid FROM public.codeviolationphotodoc WHERE codeviolation_violationid = ?";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, cv.getViolationID());
            rs = stmt.executeQuery();

//            while (rs.next()) {
//                vBlobList.add(bc.getBlob(rs.getInt("photodoc_photodocid")));
//            }
            
//            cv.setBlobIDList(blobList);

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot load photos on violation.", ex);

        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        
        return vBlobList;
    }
       /**
     * Updates only the notes field on codeviolation table
     * 
     * @param viol
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public void updateCodeViolationNotes(CodeViolation viol) 
            throws IntegrationException, BObStatusException{
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        if(viol == null){
            throw new BObStatusException("cannot update notes on a null violation");
        }
        
        try {
            String s = "UPDATE public.codeviolation SET notes=?,lastupdatedts=now(), lastupdatedby_userid=?  WHERE violationid=?";
            
            stmt = con.prepareStatement(s);
            stmt.setString(1, viol.getNotes());
            if(viol.getLastUpdatedBy() != null){
                stmt.setInt(2, viol.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            stmt.setInt(3, viol.getViolationID());

            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update code violation notes", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        } 
        
    }
    
    
    /**
     * Writes the three fields specified by the IFace_transferrable interface
     * @param trable 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void updateTransferrable(IFace_transferrable trable) throws IntegrationException{
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        
        if(trable == null){
            throw new IntegrationException("cannot update notes a null transferrable");
        }
        
        try {

            StringBuilder sb = new StringBuilder();
            sb.append("UPDATE ");
            sb.append(trable.getTransferEnum().getTargetTableID());
            sb.append(" SET transferredts=?, transferredby_userid=?, transferredtocecase_caseid=? ");
            sb.append(" WHERE ");
            sb.append(trable.getTransferEnum().getTargetPKField());
            sb.append("=?;");
                    
            stmt = con.prepareStatement(sb.toString());
            stmt.setTimestamp(1, java.sql.Timestamp.valueOf(trable.getTransferredTS()));
            if(trable.getTransferredBy() != null && trable.getTransferredBy().getUserID() != 0){
                stmt.setInt(2, trable.getTransferredBy().getUserID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            if(trable.getTransferredToCECaseID() != 0){
                stmt.setInt(3, trable.getTransferredToCECaseID());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            
            stmt.setInt(4, trable.getDBKey());

            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update transferrable status", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        } 
    }
    
    
    /**
     * Populates the three fields on a transferrable object:
     * 
     * @param trable
     * @param rs
     * @throws IntegrationException
     * @throws SQLException
     * @throws BObStatusException 
     */
    public void populateTransferrableFields(IFace_transferrable trable, ResultSet rs) throws IntegrationException, SQLException, BObStatusException{
        
        if(trable == null || rs == null){
            throw new IntegrationException("Cannot populate transferrable with null transferrable or RS");
        }
        UserCoordinator uc = getUserCoordinator();
        
        if(rs.getTimestamp("transferredts") != null){
            trable.setTransferredTS(rs.getTimestamp("transferredts").toLocalDateTime());
        }
        if(rs.getInt("transferredby_userid") != 0){
            trable.setTransferredBy(uc.user_getUser(rs.getInt("transferredby_userid")));
        }
        trable.setTransferredToCECaseID(rs.getInt("transferredtocecase_caseid"));
        
    }
    
    
    // *************************************************************************
    // *            NOTICES OF VIOLATION (NOV)                                 *
    // *************************************************************************
    
    
    
    
    /**
     * Writes a new Notice of Violation to the DB
     * @param c associated CECaseDataHeavy
     * @param notice the Notice to write
     * @return the id assigned the new notice by the database
     * @throws IntegrationException 
     */
    public int novInsert(CECaseDataHeavy c, NoticeOfViolation notice) throws IntegrationException, BObStatusException {

        String query =  """
                        INSERT INTO public.noticeofviolation(
                                    noticeid, caseid, lettertextbeforeviolations, creationtimestamp, 
                                    dateofrecord, recipient_humanid, recipient_mailing, lettertextafterviolations, 
                                    notes, creationby, notifyingofficer_userid, lettertemplate_templateid, includestipcompdate)
                            VALUES (DEFAULT, ?, ?, now(), 
                                    ?, ?, ?, ?, 
                                    ?, ?, ?, ?, ?);
                        """;

        Connection con = null;
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int insertedNOVId = 0;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            
            stmt.setInt(1, c.getCaseID());
            stmt.setString(2, notice.getNoticeTextBeforeViolations());
            stmt.setTimestamp(3, java.sql.Timestamp.valueOf(notice.getDateOfRecord()));
            
            if(notice.getRecipient() != null){
                stmt.setInt(4, notice.getRecipient().getHumanID());
            } else {
                throw new BObStatusException("Cannot write notice without a person recipient");
            }
            if(notice.getRecipientMailingAddress() != null){
                stmt.setInt(5, notice.getRecipientMailingAddress().getAddressID());
            } else {
                throw new BObStatusException("Cannot write notice without an address");
            }
            stmt.setString(6, notice.getNoticeTextAfterViolations());
            stmt.setString(7, notice.getNotes());
            stmt.setInt(8, notice.getCreationBy().getUserID());
            
             if(notice.getNotifyingOfficer() != null){
                stmt.setInt(9, notice.getNotifyingOfficer().getUserID());
            } else {
                throw new BObStatusException("Cannot write notice without a notifying officer");
            }
            
            if(notice.getTemplate() != null){
                stmt.setInt(10, notice.getTemplate().getTemplateID());
            } else {
                stmt.setNull(10, java.sql.Types.NULL);
            }
             
            stmt.setBoolean(11, notice.isIncludeStipulatedCompDate());
                    
            stmt.execute();
            
            String retrievalQuery = "SELECT currval('noticeofviolation_noticeid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();
            while(rs.next()){
                insertedNOVId = rs.getInt(1);
            }
            
            notice.setNoticeID(insertedNOVId);
            System.out.println("CaseIntetgrator.novInsert | noticeid " + notice.getNoticeID());
            System.out.println("CaseIntegrator.novInsert | retrievedid " + insertedNOVId);
            
            List<CodeViolationDisplayable> cvList = notice.getViolationList();
            Iterator<CodeViolationDisplayable> iter = cvList.iterator();
            while(iter.hasNext()){
                CodeViolationDisplayable cvd = iter.next();
                novConnectNoticeToCodeViolation(notice, cvd);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert notice of violation letter at this time, sorry.", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return insertedNOVId;

    } // close method
    
    /**
     * Creates link between a NOV and a CECase
     * @param nov
     * @param cv
     * @throws IntegrationException 
     */
    private void novConnectNoticeToCodeViolation(NoticeOfViolation nov, CodeViolationDisplayable cv) throws IntegrationException{
        String query =  """
                        INSERT INTO public.noticeofviolationcodeviolation(
                                    noticeofviolation_noticeid, codeviolation_violationid, includeordtext, 
                                    includehumanfriendlyordtext, includeviolationphoto)
                            VALUES (?, ?, ?, 
                                    ?, ?);""";
        // note that original time stamp is not altered on an update

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            System.out.println("ViolationIntegrator.novConnectNoticeTOCodeViolation | noticeid " + nov.getNoticeID());
            
            stmt.setInt(1, nov.getNoticeID());
            stmt.setInt(2, cv.getViolationID());
            stmt.setBoolean(3, cv.isIncludeOrdinanceText());
            stmt.setBoolean(4, cv.isIncludeHumanFriendlyText());
            stmt.setBoolean(5, cv.isIncludeViolationPhotos());

            stmt.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to connect notice and violation in database", ex);
        } finally {
             releasePostgresConnection(con, stmt);
        } 
    }
    
    /**
     * Utility method for calling novUpdateNoticeCodeViolation on each violation
     * in a NOV
     * @param nov
     * @throws IntegrationException 
     */
    public void novUpdateAllNoticeCodeViolations(NoticeOfViolation nov) throws IntegrationException{
            Iterator<CodeViolationDisplayable> iter = nov.getViolationList().iterator();
            CodeViolationDisplayable cvd = null;
            while(iter.hasNext()){
                novUpdateNoticeCodeViolation(iter.next(), nov);
            }
    }
    
    /**
     * Updates the connection between a code violation and a NOV
     * @param cv
     * @param nov
     * @throws IntegrationException 
     */
    private void novUpdateNoticeCodeViolation(CodeViolationDisplayable cv, NoticeOfViolation nov) throws IntegrationException{
        String query =  """
                        UPDATE public.noticeofviolationcodeviolation
                           SET includeordtext=?, includehumanfriendlyordtext=?, includeviolationphoto=? 
                         WHERE noticeofviolation_noticeid=? AND codeviolation_violationid=?;
                        """;
        // note that original time stamp is not altered on an update

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            stmt.setBoolean(1, cv.isIncludeOrdinanceText());
            stmt.setBoolean(2, cv.isIncludeHumanFriendlyText());
            stmt.setBoolean(3, cv.isIncludeViolationPhotos());
            stmt.setInt(4, nov.getNoticeID());
            stmt.setInt(5, cv.getViolationID());

            stmt.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update notice violation links", ex);
        } finally {
             releasePostgresConnection(con, stmt);
        } 
    }
    
    /**
     * Removes linkages between a NOV and its CodeViolations. We don't lose any data beyond
     * display params for the CV in the NOV
     * @param nov
     * @throws IntegrationException 
     */
    private void novClearNoticeCodeViolationConnections(NoticeOfViolation nov) throws IntegrationException{
         String query =     """
                            DELETE FROM public.noticeofviolationcodeviolation
                             WHERE noticeofviolation_noticeid=?;
                            """;
        // note that original time stamp is not altered on an update

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, nov.getNoticeID());
            stmt.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to clear connections between NOVs and code violations", ex);
        } finally {
             releasePostgresConnection(con, stmt);
        } 
    }
    
    /**
     * When a NOV is locked, it cannot be edited because it's ready for mailing
     * @param nov
     * @throws IntegrationException 
     */
    public void novLockAndQueueForMailing(NoticeOfViolation nov) throws IntegrationException{
        if (nov == null){
            throw new IntegrationException("novLockAndQueueForMailing(...): Cannot lock null NOV");
        }
        
        String query =  """
                        UPDATE public.noticeofviolation
                           SET lockedandqueuedformailingdate=?, lockedandqueuedformailingby=?,        recipient_humanid=?, recipient_mailing=?, 
                               fixedrecipientxferts=now(), fixedrecipientname=?, fixedrecipientbldgno=?, 
                               fixedrecipientstreet=?, fixedrecipientcity=?, fixedrecipientstate=?, 
                               fixedrecipientzip=?,  fixednotifyingofficername=?, fixednotifyingofficertitle=?, 
                               fixednotifyingofficerphone=?, fixednotifyingofficeremail=?, fixedissuingofficersig_photodocid=?, fixedrecipientmailingaddresscombined=?    
                        WHERE noticeid=?;
                        """; 
        // note that original time stamp is not altered on an update

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            stmt.setTimestamp(1, java.sql.Timestamp.valueOf(nov.getLockedAndqueuedTS()));
            stmt.setInt(2, nov.getLockedAndQueuedBy().getUserID());
            
            stmt.setInt(3, nov.getRecipient().getHumanID());
            stmt.setInt(4, nov.getRecipientMailingAddress().getAddressID());
            
            stmt.setString(5, nov.getFixedRecipientName());
            stmt.setString(6, nov.getFixedRecipientBldgNo());
            stmt.setString(7, nov.getFixedRecipientStreet());
            stmt.setString(8, nov.getFixedRecipientCity());
            stmt.setString(9, nov.getFixedRecipientState());
            stmt.setString(10, nov.getFixedRecipientZip());
            
            stmt.setString(11, nov.getFixedNotifyingOfficerName());
            stmt.setString(12, nov.getFixedNotifyingOfficerTitle());
            stmt.setString(13, nov.getFixedNotifyingOfficerPhone());
            stmt.setString(14, nov.getFixedNotifyingOfficerEmail());
            
            if(nov.getFixedNotifyingOfficerSignaturePhotoDocID() != 0){
                stmt.setInt(15, nov.getFixedNotifyingOfficerSignaturePhotoDocID());
            } else {
                stmt.setNull(15, java.sql.Types.NULL);
            }
            
            if(nov.getFixedRecipientMailingAddressCombinedEscapeFalse() != null){
                stmt.setString(16, nov.getFixedRecipientMailingAddressCombinedEscapeFalse());
            } else {
                stmt.setNull(16, java.sql.Types.NULL);
            }
            stmt.setInt(17, nov.getNoticeID());

            stmt.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("novLockAndQueueForMailing(...): Unable to write NOV lock to database", ex);
        } finally {
             releasePostgresConnection(con, stmt);
        } 
    }
    
    /**
     * Updates the NOV record with sender info
     * @param nov
     * @throws IntegrationException 
     */
    public void novRecordMailing(NoticeOfViolation nov) throws IntegrationException{
        String query =  "UPDATE public.noticeofviolation\n" +
                        "   SET sentdate=?, sentby=? " +
                        "  WHERE noticeid=?;";
        // note that original time stamp is not altered on an update

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            stmt.setTimestamp(1, java.sql.Timestamp.valueOf(nov.getSentTS()));
            stmt.setInt(2, nov.getSentBy().getUserID());
            stmt.setInt(3, nov.getNoticeID());

            stmt.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to record NOV mailing in database", ex);
        } finally {
             releasePostgresConnection(con, stmt);
        } 
        
    }
    
    /**
     * Updates an NOV record to reflect the NOV being returned by the USPS
     * @param nov
     * @throws IntegrationException 
     */
    public void novRecordReturnedNotice(NoticeOfViolation nov) throws IntegrationException{
        String query =  """
                        UPDATE public.noticeofviolation
                           SET returneddate=?, returnedby=?   WHERE noticeid=?;
                        """;
        // note that original time stamp is not altered on an update

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            stmt.setTimestamp(1, java.sql.Timestamp.valueOf(nov.getReturnedTS()));
            stmt.setInt(2, nov.getReturnedBy().getUserID());
            stmt.setInt(3, nov.getNoticeID());

            stmt.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot record notice as returned in database", ex);
        } finally {
             releasePostgresConnection(con, stmt);
        } 
        
    }
    
    /**
     * Updates the notes field only on a NOV object
     * @param nov
     * @throws IntegrationException 
     */
    public void novUpdateNoticeNotes(NoticeOfViolation nov) throws IntegrationException{
        String query =  """
                        UPDATE public.noticeofviolation
                           SET notes=?   WHERE noticeid=?;
                        """;
        // note that original time stamp is not altered on an update

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            stmt.setString(1, nov.getNotes());
            stmt.setInt(2, nov.getNoticeID());

            stmt.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot record notice as returned in database", ex);
        } finally {
             releasePostgresConnection(con, stmt);
        } 
    }
    
    /**
     * Updates a subset of NOV fields
     * @param notice
     * @throws IntegrationException 
     */
    public void novUpdateNotice(NoticeOfViolation notice) throws IntegrationException {
        String query = """
                       UPDATE public.noticeofviolation
                          SET   lettertextbeforeviolations=?, 
                                dateofrecord=?, lettertextafterviolations=?, recipient_humanid=?, recipient_mailing=?, 
                                notifyingofficer_userid=?, notifyingofficer_humanid=?, 
                                includestipcompdate=?, lettertemplate_templateid=?, actiondueby=?   
                       WHERE noticeid=?;
                       """;
        // note that original time stamp is not altered on an update

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            stmt.setString(1, notice.getNoticeTextBeforeViolations());
            stmt.setTimestamp(2, java.sql.Timestamp.valueOf(notice.getDateOfRecord()));
            stmt.setString(3, notice.getNoticeTextAfterViolations());
            if(notice.getRecipient() != null){
                stmt.setInt(4, notice.getRecipient().getHumanID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            if(notice.getRecipientMailingAddress() != null){
                stmt.setInt(5, notice.getRecipientMailingAddress().getAddressID());
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            if(notice.getNotifyingOfficer() != null){
                stmt.setInt(6, notice.getNotifyingOfficer().getUserID());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            
            if(notice.getNotifyingOfficerPerson()!= null){
                stmt.setInt(7, notice.getNotifyingOfficerPerson().getHumanID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            
            stmt.setBoolean(8, notice.isIncludeStipulatedCompDate());
            
            if(notice.getTemplate() != null){
                stmt.setInt(9, notice.getTemplate().getTemplateID());
            } else {
                stmt.setNull(9, java.sql.Types.NULL);
            }
            
            if(notice.getActionDueBy() != null){
                stmt.setTimestamp(10, java.sql.Timestamp.valueOf(notice.getActionDueBy()));
            } else {
                stmt.setNull(10, java.sql.Types.NULL);
            }
            
            stmt.setInt(11, notice.getNoticeID());

            stmt.executeUpdate();
            
            novClearNoticeCodeViolationConnections(notice);
            
            List<CodeViolationDisplayable> cvList = notice.getViolationList();
            
            for (CodeViolationDisplayable cvd : cvList) {
                novConnectNoticeToCodeViolation(notice, cvd);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("cannot update notice of violation letter at this time, sorry.", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        } 
    }

    /**
     * Actually deletes an NOV from the DB! Very rare indeed. The CaseCoordinator
     * will only allow this if the NOV has not been locked or printed
     * @param notice
     * @throws IntegrationException 
     */
    public void novDeactivate(NoticeOfViolation notice) throws IntegrationException {
        String query = "UPDATE noticeofviolation SET active=FALSE "
                + "  WHERE noticeid=?;";
        // note that original time stamp is not altered on an update

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, notice.getNoticeID());

            stmt.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("cannot update notice of violation letter at this time, sorry.", ex);
        } finally {
             releasePostgresConnection(con, stmt);
        } 

    }
    
    /**
     * Updates an NOV record to reflect that no mailing actions have occurred
     * @param notice
     * @throws IntegrationException 
     */
     public void novResetMailingFieldsToNull(NoticeOfViolation notice) throws IntegrationException {
        String query = """
                       UPDATE public.noticeofviolation
                          SET sentdate=NULL, sentby=NULL, returneddate=NULL, returnedby=NULL,
                              lockedandqueuedformailingdate=NULL, lockedandqueuedformailingby=NULL  WHERE noticeid=?;
                       """;
        // note that original time stamp is not altered on an update

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, notice.getNoticeID());
            stmt.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("cannot clear nov of mailing data.", ex);
        } finally {
             releasePostgresConnection(con, stmt);
        } 

    }
    
    /**
     * Cached-bached  getter for the NOV object
     * @param noticeID
     * @return
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.BlobException
     */
    public NoticeOfViolation novGet(int noticeID) throws IntegrationException, BObStatusException, BlobException{
        if(noticeID == 0){
            throw new BObStatusException("Cannot fetch an NOV with ID == 0");
        }
        if(ceCaseCacheManager.isCachingEnabled()){
            return ceCaseCacheManager.getCacheNOV().get(noticeID, k -> novFetch(noticeID));
        } else {
            return novFetch(noticeID);
        }
    }
    
    /**
     * Actual DB extraction for NOVs
     * @param noticeID
     * @return
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.BlobException
     */
    public NoticeOfViolation novFetch(int noticeID) throws DatabaseFetchRuntimeException{
        String query =  """
                        SELECT noticeid, caseid, lettertextbeforeviolations, creationtimestamp, 
                               dateofrecord, sentdate, returneddate, personid_recipient, lettertextafterviolations, 
                               lockedandqueuedformailingdate, lockedandqueuedformailingby, sentby, 
                               returnedby, notes, creationby, active, followupevent_eventid, notifyingofficer_userid,        recipient_humanid, recipient_mailing, 
                               fixedrecipientxferts, fixedrecipientname, fixedrecipientbldgno, 
                               fixedrecipientstreet, fixedrecipientcity, fixedrecipientstate, 
                               fixedrecipientzip, fixednotifyingofficername, fixednotifyingofficertitle, 
                               fixednotifyingofficerphone, fixednotifyingofficeremail, lettertemplate_templateid, letter_typeid, fixedissuingofficersig_photodocid, 
                                includestipcompdate, fixedrecipientmailingaddresscombined, actiondueby
                          FROM public.noticeofviolation WHERE noticeid = ?;
                        """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        NoticeOfViolation notice = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, noticeID);
            rs = stmt.executeQuery();

            while (rs.next()) {
                notice = novGenerate(rs);
            }

        } catch (SQLException | BObStatusException | BlobException | IntegrationException  ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("CaseIntegrator.netGet(): cannot fetch NoticeOfViolation by NoticeID, sorry.", ex);

        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return notice;
    }

    /**
     * Grabs all the NOVs for a given CECase
     * @param ceCase
     * @return
     * @throws IntegrationException 
     */
    public List<Integer> novGetList(CECase ceCase) throws IntegrationException {
        String query = "SELECT noticeid FROM public.noticeofviolation WHERE caseid=? AND active=TRUE;";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        List<Integer> idl = new ArrayList();

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, ceCase.getCaseID());
            rs = stmt.executeQuery();

            while (rs.next()) {
                idl.add(rs.getInt("noticeid"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("cannot fetch NOV List by CECAse, sorry.", ex);

        } finally {
             if (con != null) { try { con.close(); } catch (SQLException e) { /* ignored */} }
             if (stmt != null) { try { stmt.close(); } catch (SQLException e) { /* ignored */} }
             if (rs != null) { try { rs.close(); } catch (SQLException ex) { /* ignored */ } }
        } 
        return idl;
    }
    
    /**
     * Extracts the IDs of all NOV types based on muni
     * @return the IDs of the types
     * @throws IntegrationException 
     */
    public List<Integer> novGetTypeList() throws IntegrationException {
        
        String sb  = "SELECT novtypeid FROM public.noticeofviolationtype WHERE deactivatedts IS NULL; ";

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        List<Integer> idl = new ArrayList();

        try {
            stmt = con.prepareStatement(sb);
            rs = stmt.executeQuery();

            while (rs.next()) {
                idl.add(rs.getInt("novtypeid"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("cannot fetch NOV Type List by Muni, sorry.", ex);

        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 

        return idl;
    }
    
    /**
     * Insertion point for NOV Types
     * @param novt
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public int novInsertNOVType(NoticeOfViolationType novt) throws IntegrationException, BObStatusException {

        if(novt == null || novt.getTypeID() != 0){
            throw new IntegrationException("Cannot insert new NOV type with null type or nonzero ID");
        }
        
        String query =  """
                        INSERT INTO public.noticeofviolationtype(
                                    novtypeid, title, description, eventcatsent_catid, eventcatfollowup_catid, 
                                    eventcatreturned_catid, followupwindowdays, headerimage_photodocid, 
                                    courtdocument, injectviolations, muni_municode,
                                    deactivatedts, printstyle_styleid, includestipcompdate)
                            VALUES (DEFAULT, ?, ?, ?, ?, 
                                    ?, ?, ?, 
                                    ?, ?, ?,
                                    NULL, ?, ?);""";

        Connection con = null;
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int freshID = 0;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            
            stmt.setString(1, novt.getTitle());
            stmt.setString(2, novt.getDescription());
            if(novt.getEventCatSent() != null){
                stmt.setInt(3, novt.getEventCatSent().getCategoryID());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            if(novt.getEventCatFollowUp() != null){
                stmt.setInt(4, novt.getEventCatFollowUp().getCategoryID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            if(novt.getEventCatReturned() != null){
                stmt.setInt(5, novt.getEventCatReturned().getCategoryID());
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            
            stmt.setInt(6, novt.getFollowUpWindowDays());
            
            if(novt.getNovHeaderBlob() != null){
                stmt.setInt(7, novt.getNovHeaderBlob().getPhotoDocID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            
            stmt.setBoolean(8, novt.isCourtDocument());
            stmt.setBoolean(9, novt.isInjectViolations());
            stmt.setInt(10, novt.getMuni().getMuniCode());
            
            if(novt.getPrintStyle() != null){
                stmt.setInt(11, novt.getPrintStyle().getStyleID());
            } else{
                stmt.setNull(11, java.sql.Types.NULL);
            }
            
            stmt.setBoolean(12, novt.isIncludeStipCompDate());
        
            stmt.execute();

            String retrievalQuery = "SELECT currval('noticeofviolationtype_novtypeid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                freshID = rs.getInt(1);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert notice of violation type due to an integration error, sorry.", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return freshID;

    } // close method
    
    /**
     * Update point for NOV Types
     * @param novt
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public void novUpdateNOVType(NoticeOfViolationType novt) throws IntegrationException, BObStatusException {

        if(novt == null || novt.getTypeID() == 0){
            throw new IntegrationException("Cannot update NOV type with null type or ID == 0");
        }
        
        String query =  """
                        UPDATE public.noticeofviolationtype
                           SET title=?, description=?, eventcatsent_catid=?, eventcatfollowup_catid=?, 
                               eventcatreturned_catid=?, followupwindowdays=?, headerimage_photodocid=?, 
                               courtdocument=?, muni_municode=?,
                               injectviolations=?, deactivatedts=?, printstyle_styleid=?, includestipcompdate=? 
                         WHERE novtypeid=?;
                        """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int freshID = 0;

        try {
            stmt = con.prepareStatement(query);
            
            stmt.setString(1, novt.getTitle());
            stmt.setString(2, novt.getDescription());
            if(novt.getEventCatSent() != null){
                stmt.setInt(3, novt.getEventCatSent().getCategoryID());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            if(novt.getEventCatFollowUp() != null){
                stmt.setInt(4, novt.getEventCatFollowUp().getCategoryID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            if(novt.getEventCatReturned() != null){
                stmt.setInt(5, novt.getEventCatReturned().getCategoryID());
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            
            stmt.setInt(6, novt.getFollowUpWindowDays());
            
            if(novt.getNovHeaderBlob() != null){
                stmt.setInt(7, novt.getNovHeaderBlob().getPhotoDocID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
                      
            stmt.setBoolean(8, novt.isCourtDocument());
            
            stmt.setInt(9, novt.getMuni().getMuniCode());
            
            stmt.setBoolean(10, novt.isInjectViolations());
            if(novt.getDeactivatedTS() != null){
                stmt.setTimestamp(11, java.sql.Timestamp.valueOf(novt.getDeactivatedTS()));
            } else {
                stmt.setNull(11, java.sql.Types.NULL);
            }
            
            if(novt.getPrintStyle() != null){
                stmt.setInt(12, novt.getPrintStyle().getStyleID());
            } else{
                stmt.setNull(12, java.sql.Types.NULL);
            }
            
            stmt.setBoolean(13, novt.isIncludeStipCompDate());
            stmt.setInt(14, novt.getTypeID());
            
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert notice of violation type due to an integration error, sorry.", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 

    } // close method
    
    
    /**
     * Extracts and builds an NOV type object from the DB.
     * @param tpe
     * @return
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BlobException 
     */
    public NoticeOfViolationType novGetType(int tpe) throws IntegrationException, BlobException {
        if(tpe == 0){
            throw new IntegrationException("Cannot get NOV type with ID ==0");
        }
        
        String query  = """
                        SELECT novtypeid, title, description, eventcatsent_catid, eventcatfollowup_catid, 
                               eventcatreturned_catid, followupwindowdays, headerimage_photodocid, 
                               courtdocument, injectviolations, muni_municode,
                               noticeofviolationtype.deactivatedts, printstyle_styleid, includestipcompdate 
                            FROM public.noticeofviolationtype WHERE novtypeid=?;
                        """;
        
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        NoticeOfViolationType novt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, tpe);
            rs = stmt.executeQuery();

            while (rs.next()) {
                novt = novGenerateType(rs);
            }

        } catch (SQLException | BObStatusException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("cannot fetch NOV Type by id, sorry.", ex);

        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return novt;
    }
    
    
   /**
    * Internal generator for creating NOV type objects
    */    
    private NoticeOfViolationType novGenerateType(ResultSet rs) throws SQLException, IntegrationException, BlobException, BObStatusException{
        NoticeOfViolationType novt = new NoticeOfViolationType();
        EventCoordinator ec = getEventCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        BlobCoordinator bc = getBlobCoordinator();
        MunicipalityCoordinator mc =getMuniCoordinator();
        
        novt.setTypeID(rs.getInt("novtypeid"));
        novt.setTitle(rs.getString("title"));
        novt.setDescription(rs.getString("description"));
        
        if(rs.getInt("muni_municode") != 0){
            novt.setMuni(mc.getMuni(rs.getInt("muni_municode")));
        }
            
        
        if(rs.getInt("eventcatsent_catid") != 0){
            novt.setEventCatSent(ec.getEventCategory(rs.getInt("eventcatsent_catid")));
        }
        if(rs.getInt("eventcatfollowup_catid") != 0){
            novt.setEventCatFollowUp(ec.getEventCategory(rs.getInt("eventcatfollowup_catid")));
        }
        if(rs.getInt("eventcatreturned_catid") != 0){
            novt.setEventCatReturned(ec.getEventCategory(rs.getInt("eventcatreturned_catid")));
        }
        
        novt.setFollowUpWindowDays(rs.getInt("followupwindowdays"));
        if(rs.getInt("headerimage_photodocid") != 0){
            novt.setNovHeaderBlob(bc.getBlobLight(rs.getInt("headerimage_photodocid")));
        } 
        
        novt.setCourtDocument(rs.getBoolean("courtdocument"));
        novt.setInjectViolations(rs.getBoolean("injectviolations"));
        if(rs.getTimestamp("deactivatedts") != null){
            novt.setDeactivatedTS(rs.getTimestamp("deactivatedts").toLocalDateTime());
        }
        if(rs.getInt("printstyle_styleid") != 0){
            novt.setPrintStyle(sc.getPrintStyle(rs.getInt("printstyle_styleid")));
        }
        
        novt.setIncludeStipCompDate(rs.getBoolean("includestipcompdate"));
        return novt;
    }
    
    /**
     * Grabs all the NOVs for a given CECase
     * @param cv
     * @return of Notice letter IDs
     * @throws IntegrationException 
     */
    public List<Integer> novGetNOVIDList(CodeViolation cv) throws IntegrationException {
        String query = "SELECT noticeofviolation_noticeid FROM public.noticeofviolationcodeviolation "
                + "WHERE codeviolation_violationid=?;";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        List<Integer> idl = new ArrayList<>();
        
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, cv.getViolationID());
            rs = stmt.executeQuery();

            while (rs.next()) {
                idl.add(rs.getInt("noticeofviolation_noticeid"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("cannot fetch NOV LIst by CodeViolation, sorry!", ex);

        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return idl;
    }

    /**
     * Internal generator method for NOVs
     * @param rs
     * @return
     * @throws SQLException
     * @throws IntegrationException
     */
    private NoticeOfViolation novGenerate(ResultSet rs) 
            throws SQLException, 
            IntegrationException,
            BObStatusException,
            BlobException {

        UserCoordinator uc = getUserCoordinator();
        PersonCoordinator pc = getPersonCoordinator();
        PropertyCoordinator propc = getPropertyCoordinator();
        CaseCoordinator cc = getCaseCoordinator();
        
        // the magical moment of notice instantiation
        NoticeOfViolation notice = new NoticeOfViolation();

        notice.setNoticeID(rs.getInt("noticeid"));
        notice.setCaseID(rs.getInt("caseid"));
        
        notice.setDateOfRecord(rs.getTimestamp("dateofrecord").toLocalDateTime());

        notice.setNoticeTextBeforeViolations(rs.getString("lettertextbeforeviolations"));
        populateCodeViolations(notice);
        notice.setNoticeTextAfterViolations(rs.getString("lettertextafterviolations"));

        notice.setCreationTS(rs.getTimestamp("creationtimestamp").toLocalDateTime());
        notice.setCreationBy(uc.user_getUser(rs.getInt("creationby")));
        
        if (rs.getTimestamp("lockedandqueuedformailingdate") != null) {
            notice.setLockedAndqueuedTS(rs.getTimestamp("lockedandqueuedformailingdate").toLocalDateTime());
            notice.setLockedAndQueuedBy(uc.user_getUser(rs.getInt("lockedandqueuedformailingby")));
        }
        
        if (rs.getTimestamp("sentdate") != null) {
            notice.setSentTS(rs.getTimestamp("sentdate").toLocalDateTime());
            notice.setSentBy(uc.user_getUser(rs.getInt("sentby")));
        } 
        
        if (rs.getTimestamp("returneddate") != null) {
            notice.setReturnedTS(rs.getTimestamp("returneddate").toLocalDateTime());
            notice.setReturnedBy(uc.user_getUser(rs.getInt("returnedby")));
        } 
        
        notice.setNotes(rs.getString("notes"));
        
        notice.setActive(rs.getBoolean("active"));
        
        if(rs.getInt("notifyingofficer_userid") != 0){
            notice.setNotifyingOfficer(uc.user_getUser(rs.getInt("notifyingofficer_userid")));
        }
        if(notice.getNotifyingOfficer() != null && notice.getNotifyingOfficer().getHumanID() != 0){
            notice.setNotifyingOfficerPerson(pc.getPersonByHumanID(notice.getNotifyingOfficer().getHumanID()));
        }
        
        if(rs.getInt("recipient_humanid") != 0){
            notice.setRecipient(pc.getPersonByHumanID(rs.getInt("recipient_humanid")));
        }
        if(rs.getInt("recipient_mailing") != 0){
            notice.setRecipientMailingAddress(propc.getMailingAddress(rs.getInt("recipient_mailing")));
        }
        if(rs.getTimestamp("fixedrecipientxferts") != null){
            notice.setFixedAddrXferTS(rs.getTimestamp("fixedrecipientxferts").toLocalDateTime());
        }
        
        notice.setFixedRecipientName(rs.getString("fixedrecipientname"));
        notice.setFixedRecipientBldgNo(rs.getString("fixedrecipientbldgno"));
        notice.setFixedRecipientStreet(rs.getString("fixedrecipientstreet"));
        notice.setFixedRecipientCity(rs.getString("fixedrecipientcity"));
        notice.setFixedRecipientState(rs.getString("fixedrecipientstate"));
        notice.setFixedRecipientZip(rs.getString("fixedrecipientzip"));
        
        notice.setFixedRecipientMailingAddressCombinedEscapeFalse(rs.getString("fixedrecipientmailingaddresscombined"));
        
        notice.setFixedNotifyingOfficerName(rs.getString("fixednotifyingofficername"));
        notice.setFixedNotifyingOfficerTitle(rs.getString("fixednotifyingofficertitle"));
        notice.setFixedNotifyingOfficerPhone(rs.getString("fixednotifyingofficerphone"));
        notice.setFixedNotifyingOfficerEmail(rs.getString("fixednotifyingofficeremail"));

        // for legacy compability. Post Oct 2023, we use the template ID and that
        // template is keyed to a type
        notice.setNoticeTypeLegacyID(rs.getInt("letter_typeid"));
        
        if(rs.getInt("lettertemplate_templateid") != 0){
            notice.setTemplate(cc.nov_getNoticeOfViolationTemplate(rs.getInt("lettertemplate_templateid")));
        }
        notice.setFixedNotifyingOfficerSignaturePhotoDocID(rs.getInt("fixedissuingofficersig_photodocid"));
        notice.setIncludeStipulatedCompDate(rs.getBoolean("includestipcompdate"));
        if(rs.getTimestamp("actiondueby") != null){
            notice.setActionDueBy(rs.getTimestamp("actiondueby").toLocalDateTime());
        }
        
        return notice;
    }
    
    /**
     * Looks up all of the CodeViolations associated with a given NOV
     * and extracts their display properties stored in the linking DB's linking table
     * @param nov
     * @return
     * @throws IntegrationException
     */
    private NoticeOfViolation populateCodeViolations(NoticeOfViolation nov) 
            throws IntegrationException, BObStatusException{
        String query =  """
                          SELECT noticeofviolation_noticeid, codeviolation_violationid, includeordtext, 
                               includehumanfriendlyordtext, includeviolationphoto
                          FROM public.noticeofviolationcodeviolation WHERE noticeofviolation_noticeid = ?;
                        """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        CodeViolationDisplayable viol;
        List<CodeViolationDisplayable> codeViolationList = new ArrayList<>();
        CaseCoordinator cc = getCaseCoordinator();

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, nov.getNoticeID() );
            rs = stmt.executeQuery();

            while (rs.next()) {
                viol = new CodeViolationDisplayable(cc.violation_getCodeViolation(rs.getInt("codeviolation_violationid")));
                viol.setIncludeOrdinanceText(rs.getBoolean("includeordtext"));
                viol.setIncludeHumanFriendlyText(rs.getBoolean("includehumanfriendlyordtext"));
                viol.setIncludeViolationPhotos(rs.getBoolean("includeviolationphoto"));
                codeViolationList.add(viol);
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot populate notice of violation, sorry!", ex);

        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        nov.setViolationList(codeViolationList);
        return nov;
    }
    
    /**
     * ******************************************
     * *********   TEMPLATE CENTRAL  ************
     * ********* REVAMP October 2023 ************
     * ******************************************
     */
    
   /**
    * Retrieves all text block categories from the DB
     * @param muni
    * @return if null, all active cats are returned
    * @throws IntegrationException 
    */
    public List<Integer> getNOVTemplates(Municipality muni) throws IntegrationException{
        String query =  "SELECT templateid FROM public.noticeofviolationtemplate WHERE deactivatedts IS NULL";
        if(muni != null){
            query = query + " AND muni_municode=?;";
        } else {
            query = query + ";";
        }
        List<Integer> idl = new ArrayList<>();
        
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
             if(muni != null){
                stmt.setInt(1, muni.getMuniCode());
             }
            rs = stmt.executeQuery();

            while (rs.next()) {
                idl.add(rs.getInt("templateid"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot fetch templates, sorries!", ex);

        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return idl;
    }
    
    
    
    /**
    * Retrieves a single notice of violation template
    * @param id
    * @return
    * @throws IntegrationException 
    */
    public NoticeOfViolationTemplate getNoticeOfViolationTemplate(int id) throws IntegrationException, BlobException, BObStatusException{
        if(id == 0){
            throw new IntegrationException("Canot get a template if id == 0");
        }
        String query =  """
                            SELECT templateid, templatetype_typeid, title, muni_municode, templatetext, deactivatedts
                                FROM public.noticeofviolationtemplate WHERE templateid=?
                        """;

        NoticeOfViolationTemplate plate = null;
        
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, id);
            
            rs = stmt.executeQuery();

            while (rs.next()) {
                plate = generateNOVTemplate(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot fetch Template, sorries!", ex);
        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return plate;
    }
    
    /**
     * Builds a NoticeOfViolationTemplate object from an RS
     * @param rs
     * @return
     * @throws SQLException
     * @throws IntegrationException 
     */
    private NoticeOfViolationTemplate generateNOVTemplate(ResultSet rs) throws SQLException, IntegrationException, BlobException, BObStatusException{
        NoticeOfViolationTemplate plate = new NoticeOfViolationTemplate();
        MunicipalityCoordinator mc = getMuniCoordinator();
        CaseCoordinator cc = getCaseCoordinator();
        
        plate.setTemplateID(rs.getInt("templateid"));
        plate.setTemplateType(cc.nov_getNOVType(rs.getInt("templatetype_typeid")));
        plate.setTitle(rs.getString("title"));
        if(rs.getInt("muni_municode") != 0){
            plate.setMuni(mc.getMuni(rs.getInt("muni_municode")));
        }
        plate.setTemplateTextEscapeFalse(rs.getString("templatetext"));
        if(rs.getTimestamp("deactivatedts") != null){
            plate.setDeactivatedTS(rs.getTimestamp("deactivatedts").toLocalDateTime());
        }

        return plate;
    }
    
      
  
     /**
      * Creates a new entry in the noticeofviolationtemplate
     * @param plate
     * @return  ID of fresh category
      * @throws IntegrationException 
      */
     public int insertNoticeOfViolationTemplate(NoticeOfViolationTemplate plate) throws IntegrationException, BObStatusException{
        if(plate == null || plate.getTemplateType() == null){
             throw new BObStatusException("Cannot update template with null plate or type");
        }
        String query =  """
                        INSERT INTO public.noticeofviolationtemplate(
                            templateid, templatetype_typeid, title, muni_municode, templatetext, deactivatedts)
                            VALUES (DEFAULT, ?, ?, ?, ?, NULL);
                        """;
        PreparedStatement stmt = null;
        Connection con = null;
        ResultSet rs = null;
        int freshPlateID = 0;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
          
            stmt.setInt(1, plate.getTemplateType().getTypeID());
            stmt.setString(2, plate.getTitle());
            if(plate.getMuni() != null){
                stmt.setInt(3, plate.getMuni().getMuniCode());
            }
            stmt.setString(4, plate.getTemplateTextEscapeFalse());
            
            stmt.execute(); 
            
            String retrievalQuery = "SELECT currval('noticeofviolationtemplate_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                freshPlateID = rs.getInt(1);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Fatal error: Cannot insert new template into db, sorry; Complain to Eric.", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
         return freshPlateID;
     }
    
    
    
     /**
      * Updates a record in the noticeofviolationtemplate table
      *  @param plate to update
      * @throws IntegrationException 
      */
     public void updateNoticeOfViolationTemplate(NoticeOfViolationTemplate plate) throws IntegrationException, BObStatusException{
        if(plate == null || plate.getTemplateType() == null){
            throw new BObStatusException("Cannot update template with null plate or type");
        }
         String query =  """
                        UPDATE public.noticeofviolationtemplate
                            SET templatetype_typeid=?, title=?, muni_municode=?, templatetext=?, deactivatedts=?
                            WHERE templateid=?;
                        """;
        PreparedStatement stmt = null;
        Connection con = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
          
            
            stmt.setInt(1, plate.getTemplateType().getTypeID());
            stmt.setString(2, plate.getTitle());
            if(plate.getMuni() != null){
                stmt.setInt(3, plate.getMuni().getMuniCode());
            }
            stmt.setString(4, plate.getTemplateTextEscapeFalse());
            if(plate.getDeactivatedTS() != null){
                stmt.setTimestamp(5, java.sql.Timestamp.valueOf(plate.getDeactivatedTS()));
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            stmt.setInt(6, plate.getTemplateID());
            
            stmt.executeUpdate(); 

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Text Block Cat Integration Module: cannot insert text block cat into DB, sorry", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
     }
    
    
    
    
    
     // ***********************************************************************
     // **                  TEXT BLOCKS 
     // ***********************************************************************
     
 
   /**
    * Retrieves all text block categories from the DB
     * @param muni
    * @return if null, all active cats are returned
    * @throws IntegrationException 
    */
    public List<Integer> getTextBlockCategoryList(Municipality muni) throws IntegrationException{
        String query =  "SELECT categoryid FROM public.textblockcategory WHERE deactivatedts IS NULL ";
        if(muni != null){
            query = query + " AND muni_municode=?;";
        } else {
            query = query + ";";
        }
        List<Integer> idl = new ArrayList<>();
        
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
             if(muni != null){
                stmt.setInt(1, muni.getMuniCode());
             }
            rs = stmt.executeQuery();

            while (rs.next()) {
                idl.add(rs.getInt("categoryid"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot fetch TextBlockMap, sorries!", ex);

        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return idl;
    }
    
    
     /**
    * Retrieves a single text block category
     * @param id
    * @return
    * @throws IntegrationException 
    */
    public TextBlockCategory getTextBlockCategory(int id) throws IntegrationException{
        if(id == 0){
            throw new IntegrationException("Canot get a text block category if id == 0");
        }
        String query =  "SELECT categoryid, categorytitle, icon_iconid, muni_municode, \n" +
                        "       deactivatedts\n" +
                        "  FROM public.textblockcategory WHERE categoryid=?";

        TextBlockCategory tbc = null;
        
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, id);
            
            rs = stmt.executeQuery();

            while (rs.next()) {
                tbc = generateTextBlockCategory(rs);
            }
        } catch (SQLException | BObStatusException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot fetch TextBlockCategory, sorries!", ex);
        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return tbc;
    }
    
   /**
    * Retrieves all text blocks from DB
    * @return
    * @throws IntegrationException 
    * @Deprecated in favor of standard coordinator model--if it wants to build a MAP, 
    *  then it can do so by itself
    */
    public HashMap<String, Integer> getTextBlockCategoryMap() throws IntegrationException{
        String query =  "SELECT categoryid, categorytitle\n" +
                        "  FROM public.textblockcategory;";
        HashMap<String, Integer> categoryMap = new HashMap<>();
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();

            while (rs.next()) {
                categoryMap.put(rs.getString("categorytitle"), rs.getInt("categoryid"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot fetch TextBlockMap, sorries!", ex);

        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return categoryMap;
    }
    
    
    /**
     * Builds a text block
     * @param rs
     * @return
     * @throws SQLException
     * @throws IntegrationException 
     */
    private TextBlock generateTextBlock(ResultSet rs) throws SQLException, IntegrationException, BObStatusException{
        TextBlock tb = new TextBlock();
        MunicipalityCoordinator mc = getMuniCoordinator();
        
        tb.setBlockID(rs.getInt("blockid"));
        tb.setCategory(getTextBlockCategory(rs.getInt("blockcategory_catid")));
        if(rs.getInt("muni_municode") != 0){
            tb.setMuni(mc.getMuni(rs.getInt("muni_municode")));
        }
        tb.setTextBlockName(rs.getString("blockname"));
        tb.setTextBlockText(rs.getString("blocktext"));
        tb.setPlacementOrder(rs.getInt("placementorderdefault"));
        tb.setInjectableTemplate(rs.getBoolean("injectabletemplate"));
        if(rs.getTimestamp("deactivatedts") != null){
            tb.setDeactivatedTS(rs.getTimestamp("deactivatedts").toLocalDateTime());
        }
        return tb;
    }
    
    
    /**
     * Generator for text block categories;
     * @param rs
     * @return 
     */
    private TextBlockCategory generateTextBlockCategory(ResultSet rs) throws SQLException, IntegrationException, BObStatusException{
        TextBlockCategory tbc = new TextBlockCategory();
        MunicipalityCoordinator mc = getMuniCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        
        tbc.setCategoryID(rs.getInt("categoryid"));
        tbc.setTitle(rs.getString("categorytitle"));
        if(rs.getInt("icon_iconid") != 0){
            tbc.setIcon(sc.getIcon(rs.getInt("icon_iconid")));
        }
        tbc.setMuni(mc.getMuni(rs.getInt("muni_municode")));
        if(rs.getTimestamp("deactivatedts") != null){
            tbc.setDeactivatedTS(rs.getTimestamp("deactivatedts").toLocalDateTime());
        }
        return tbc;
    }
    
    /**
     * Extracts a single text block from the DB
     * @param blockID
     * @return
     * @throws IntegrationException 
     */
     public TextBlock getTextBlock(int blockID) throws IntegrationException{
         
        String query =  "SELECT blockid, blockcategory_catid, muni_municode, blockname, blocktext, \n" +
                        "       placementorderdefault, injectabletemplate, deactivatedts\n" +
                        "  FROM public.textblock  WHERE blockid=?;";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection con = null;
        TextBlock tb = null;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, blockID);
            
            rs = stmt.executeQuery(); 
            
            while(rs.next()){
                tb = generateTextBlock(rs);
            }
            
        } catch (SQLException | BObStatusException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Code Violation Integrator: cannot retrive text block by ID", ex);
            
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
          
         return tb;
     }
     
   
     
     /**
      * Extracts all text blocks associated with a given Muni
     * @param tbc if null, blocks are returned without respect to category
      * @param m if Null, all blocks returned
      * @return
      * @throws IntegrationException 
      */
     public List<Integer> getTextBlockIDList(TextBlockCategory tbc, Municipality m) throws IntegrationException{
        StringBuilder sb = new StringBuilder(" SELECT blockid " );
                            sb.append("  FROM public.textblock WHERE deactivatedts IS NULL ");
        
        if(m != null){
            sb.append(" AND textblock.muni_municode=?");
        } 
        if(tbc != null){
            sb.append(" AND blockcategory_catid=?");
        }
        sb.append(";");
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection con = null;
        List<Integer> idl = new ArrayList();
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(sb.toString());
            int paramCount = 0;
            if(m != null){
                stmt.setInt(++paramCount, m.getMuniCode());
            }
            if(tbc != null){
                stmt.setInt(++paramCount, tbc.getCategoryID());
            }
            rs = stmt.executeQuery(); 
            while(rs.next()){
                idl.add(rs.getInt("blockid"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("CaseIntegrator: cannot retrieve text blocks by cat and/or muni", ex);
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
         return idl;
     }
   
     
     /**
      * Extracts a master list of all text blocks regardless of Muni
      * @return
      * @throws IntegrationException 
      */
     public List<Integer> getAllTextBlocks() throws IntegrationException{
        String query =    "  SELECT blockid \n" +
                          "  FROM public.textblock;";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection con = null;
        List<Integer> idl = new ArrayList<>();
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery(); 
            while(rs.next()){
                idl.add((rs.getInt("blockid")));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Code Violation Integrator: cannot retrive all textblocks", ex);
            
        } finally{
             releasePostgresConnection(con, stmt, rs);
        } 
        return idl;
     }
     
     /**
      * Creates a new entry in the textblock table
      * @param tb
     * @return 
      * @throws IntegrationException 
      */
     public int insertTextBlock(TextBlock tb) throws IntegrationException{
        String query =  "INSERT INTO public.textblock(\n" +
                        " blockid, blockcategory_catid, muni_municode, blockname, "
                        + "blocktext, placementorderdefault, injectabletemplate)\n" +
                        " VALUES (DEFAULT, ?, ?, ?, ?, ?, ?);";
        PreparedStatement stmt = null;
        Connection con = null;
        ResultSet rs;
        int insertedBlockID = 0;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
           if(tb.getCategory() != null){
                stmt.setInt(1, tb.getCategory().getCategoryID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
           if(tb.getMuni() != null){
                stmt.setInt(2, tb.getMuni().getMuniCode());
           } else{
                stmt.setNull(2, java.sql.Types.NULL);
           }
            stmt.setString(3, tb.getTextBlockName());
            stmt.setString(4, tb.getTextBlockText());
            stmt.setInt(5, tb.getPlacementOrder());
            stmt.setBoolean(6, tb.isInjectableTemplate());
            
            stmt.execute(); 

            String retrievalQuery = "SELECT currval('textblock_blockid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();
            while(rs.next()){
                insertedBlockID = rs.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Text Block Integration Module: cannot insert text block into DB, sorry", ex);
        } finally{
             releasePostgresConnection(con, stmt);
        } 
          
         return insertedBlockID;
         
     }
     
     /**
      * Updates key fields on text blocks
      * @param tb
      * @throws IntegrationException 
      */
     public void updateTextBlock(TextBlock tb) throws IntegrationException{
        String query = "UPDATE public.textblock\n" +
                        "   SET blockcategory_catid=?, muni_municode=?, blockname=?, \n" +
                        "       blocktext=?,placementorderdefault=?, injectabletemplate=?, deactivatedts=? \n" +
                        " WHERE blockid=?;";
        PreparedStatement stmt = null;
        Connection con = null;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            if(tb.getCategory() != null){
                stmt.setInt(1, tb.getCategory().getCategoryID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            if(tb.getMuni() != null){
                stmt.setInt(2, tb.getMuni().getMuniCode());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            stmt.setString(3, tb.getTextBlockName());
            stmt.setString(4, tb.getTextBlockText());
            stmt.setInt(5, tb.getPlacementOrder());
            stmt.setBoolean(6, tb.isInjectableTemplate());
            if(tb.getDeactivatedTS() != null){
                stmt.setTimestamp(7, java.sql.Timestamp.valueOf(tb.getDeactivatedTS()));
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            stmt.setInt(8, tb.getBlockID());  
            
            stmt.execute(); 
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Text Block Integration Module: cannot insert text block into DB", ex);
            
        } finally{
             releasePostgresConnection(con, stmt);
        } 
     }
     
     /**
      * Creates a new entry in the textblock table
     * @param tbc
     * @return  ID of fresh category
      * @throws IntegrationException 
      */
     public int insertTextBlockCategory(TextBlockCategory tbc) throws IntegrationException{
        String query =  "INSERT INTO public.textblockcategory(\n" +
                        "            categoryid, categorytitle, icon_iconid, muni_municode, deactivatedts)\n" +
                        "    VALUES (DEFAULT, ?, ?, ?, ?);";
        PreparedStatement stmt = null;
        Connection con = null;
        ResultSet rs = null;
        int insertedBlockCatID = 0;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
          
            stmt.setString(1, tbc.getTitle());
            if(tbc.getIcon() != null){
                stmt.setInt(2, tbc.getIcon().getID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            if(tbc.getMuni() != null){
                stmt.setInt(3, tbc.getMuni().getMuniCode());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            if(tbc.getDeactivatedTS() != null){
                stmt.setTimestamp(4, java.sql.Timestamp.valueOf(tbc.getDeactivatedTS()));
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            
            stmt.execute(); 
            
            String retrievalQuery = "SELECT currval('blockcategory_categoryid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();
            
            while(rs.next()){
                insertedBlockCatID = rs.getInt(1);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Text Block Cat Integration Module: cannot insert text block cat into DB, sorry", ex);
        } finally{
            releasePostgresConnection(con, stmt);
        } 
         return insertedBlockCatID;
     }
     
     /**
      * Updates key fields on text blocks
     * @param tbc
      * @throws IntegrationException 
      */
     public void updateTextBlockCategory(TextBlockCategory tbc) throws IntegrationException{
        String query = """
                       UPDATE public.textblockcategory
                          SET categorytitle=?, icon_iconid=?, muni_municode=?, 
                              deactivatedts=?
                        WHERE categoryid=?;
                       """;
        PreparedStatement stmt = null;
        Connection con = null;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            
            stmt.setString(1, tbc.getTitle());
            if(tbc.getIcon() != null){
                stmt.setInt(2, tbc.getIcon().getID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            if(tbc.getMuni() != null){
                stmt.setInt(3, tbc.getMuni().getMuniCode());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            if(tbc.getDeactivatedTS() != null){
                stmt.setTimestamp(4, java.sql.Timestamp.valueOf(tbc.getDeactivatedTS()));
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            stmt.setInt(5, tbc.getCategoryID());
            
            stmt.executeUpdate(); 
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Text Block Category Integration Module: cannot update text block category in DB", ex);
        } finally{
             releasePostgresConnection(con, stmt);
        } 
     }
     
} // close CaseIntegrator