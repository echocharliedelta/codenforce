 /*
 * Copyright (C) 2017 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheClient;
import com.tcvcog.tcvce.coordinators.EventCoordinator;
import com.tcvcog.tcvce.coordinators.PersonCoordinator;
import com.tcvcog.tcvce.coordinators.SearchCoordinator;
import com.tcvcog.tcvce.coordinators.UserCoordinator;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.DatabaseFetchRuntimeException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.CECase;
import com.tcvcog.tcvce.entities.EventCnF;
import com.tcvcog.tcvce.entities.EventCategory;
import com.tcvcog.tcvce.entities.EventRealm;
import com.tcvcog.tcvce.entities.EventType;
import com.tcvcog.tcvce.entities.IFace_EventHolder;
import com.tcvcog.tcvce.entities.IFace_EventLinked;
import com.tcvcog.tcvce.entities.PropertyDataHeavy;
import com.tcvcog.tcvce.entities.RoleType;
import com.tcvcog.tcvce.entities.search.SearchParamsEvent;
import com.tcvcog.tcvce.entities.occupancy.OccPeriod;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import jakarta.annotation.PostConstruct;
import javax.imageio.ImageIO;

/**
 *
 * @author ellen bascomb of apt 31y
 */
public class    EventIntegrator 
        extends BackingBeanUtils
        implements Serializable, IFaceCacheClient {
    
    
    
    
    /**
     * Creates a new instance of EventIntegrator
     */
    public EventIntegrator() {
        
    }
    
    @PostConstruct
    public void initBean() {
        registerClientWithManager();
        
    }
    

    @Override
    public void registerClientWithManager() {
        getEventCacheManager().registerCacheClient(this);
    }

    
    final String ACTIVE_FIELD = "event.deactivatedts";
    
    
//    --------------------------------------------------------------------------
//    ******************************** EVENTS **********************************
//    --------------------------------------------------------------------------
    
    /**
     * Cache-backed getter for events
     * @param evid
     * @return
     * @throws IntegrationException
     * @throws DatabaseFetchRuntimeException 
     */
    public EventCnF getEvent(int evid) throws IntegrationException, DatabaseFetchRuntimeException{
        if(evid == 0){
            throw new IntegrationException("Cannot get Event with ID of 0");
        }
        
        if(getEventCacheManager().isCachingEnabled()){
            return getEventCacheManager().getCacheEvent().get(evid, k -> fetchEvent(evid) );
        } else {
            return fetchEvent(evid);
        }
    
    }
    
    
    
    /**
     * Base object creation method under the Grand Unified EventCnF GUER Model
     * 
     * @param evid
     * @return a fully-baked event, not configured for any application in 
     * a given authcontext
     * @throws DatabaseFetchRuntimeException
     */
    public EventCnF fetchEvent(int evid) throws DatabaseFetchRuntimeException{
        
        String query = """
                       SELECT 	eventid, category_catid, cecase_caseid, event.createdts, 
                         		eventdescription, event.createdby_userid, event.active,  
                         		event.notes, occperiod_periodid, 
                         		timestart, timeend, event.lastupdatedby_userid, event.lastupdatedts, 
                         		event.deactivatedts, event.deactivatedby_userid, event.parcel_parcelkey,
                         		cecase.casename, parcelunit.unitnumber, 
                                        cnf_buildparceladdressstring(cnf_getparcelofeventid(eventid), TRUE, TRUE) AS addressstring,
                                        cnf_buildparceladdressstring(cnf_getparcelofeventid(eventid), FALSE, FALSE) AS addressstring1line
                         	FROM public.event
                         	LEFT OUTER JOIN public.occperiod ON (event.occperiod_periodid = occperiod.periodid)
                         	LEFT OUTER JOIN public.parcelunit ON (occperiod.parcelunit_unitid = parcelunit.unitid)
                         	LEFT OUTER JOIN public.cecase ON (event.cecase_caseid = cecase.caseid)
                         	WHERE eventid=?;
                       """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        EventCnF ev = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, evid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                ev = generateEventFromRS(rs);
            }
        } catch (SQLException | IntegrationException ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("Cannot generate event", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 

        return ev;
    }
    
    
    /**
     * Calls the slick function for getting the parcel key of this event
     * @param ev
     * @return the parcelkey of the containing parcel, applies to events of all domains
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public int getEventParcelKey(EventCnF ev) throws IntegrationException{
        
        if(ev == null || ev.getEventID() == 0){
            throw new IntegrationException("cannot get parcelkey for null event or event with ID of 0");
            
        }
        String query = """
                       SELECT 	cnf_getparcelofeventid(eventid) AS parcelkey
                         	FROM public.event
                         	WHERE eventid=?;
                       """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        int parcelKey = 0;
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, ev.getEventID());
            rs = stmt.executeQuery();
            while (rs.next()) {
                parcelKey = rs.getInt("parcelkey");
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("Cannot generate event", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 

        return parcelKey;
    }
    
      /**
       * 
       * Generator method for EventCnF objects
     * Legacy note: [Zanda was trippin when he wrote this!]
     * ....And when he revised it for occbeta! 
     *
     * @param rs containing all fields in the event table
     * @param premadeEvent used by event creatino pathways that involve instantiation 
     * at other locations -- somewhat hacky and consider unifying
     * @return
     * @throws SQLException
     * @throws IntegrationException
     */
    private EventCnF generateEventFromRS(ResultSet rs) throws SQLException, IntegrationException {
         
        try {
            PersonCoordinator pc = getPersonCoordinator();
            UserCoordinator uc = getUserCoordinator();
            SystemIntegrator si = getSystemIntegrator();
            
            EventCnF ev = new EventCnF();
            
            ev.setEventID(rs.getInt("eventid"));
            if(rs.getInt("category_catid") != 0){
                ev.setCategory(getEventCategory(rs.getInt("category_catid")));
            }
            
            ev.appendToDescription(rs.getString("eventDescription"));
            
            // these values will be used by the configure method to set the domain
            ev.setCeCaseID(rs.getInt("cecase_caseid"));
            ev.setOccPeriodID(rs.getInt("occperiod_periodid"));
            ev.setParcelKey(rs.getInt("parcel_parcelkey"));
            
            // Parent strings; coordinator will make them pretty
            ev.setParcelAddressTwoLineEscapeFalse(rs.getString("addressstring"));
            ev.setParcelAddressOneLine(rs.getString("addressstring1line"));
            ev.setPropertyUnitNumber(rs.getString("unitnumber"));
            ev.setCaseName(rs.getString("casename"));
            
            if (rs.getTimestamp("timestart") != null) {
                LocalDateTime dt = rs.getTimestamp("timestart").toInstant()
                        .atZone(ZoneId.systemDefault()).toLocalDateTime();
                ev.setTimeStart(dt);
            }
            
            if (rs.getTimestamp("timeend") != null) {
                LocalDateTime dt = rs.getTimestamp("timeend").toInstant()
                        .atZone(ZoneId.systemDefault()).toLocalDateTime();
                ev.setTimeEnd(dt);
            }
            
            ev.setNotes(rs.getString("notes"));
            si.populateTrackedFields(ev, rs, false);
            
            return ev;
        } catch (BObStatusException ex) {
            throw new IntegrationException(ex.getMessage());
        }
    }
    

    /**
     * Extracts all records of event views by userid
     * @param userID
     * @return
     * @throws IntegrationException 
     */
    public List<Integer> getEventHistory(int userID) throws IntegrationException {
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Integer> al = new ArrayList<>();

        try {
            String s = "SELECT event_eventid, entrytimestamp FROM loginobjecthistory "
                    + "WHERE login_userid = ? "
                    + "AND event_eventid IS NOT NULL "
                    + "ORDER BY entrytimestamp DESC;";
            stmt = con.prepareStatement(s);
            stmt.setInt(1, userID);

            rs = stmt.executeQuery();
            while (rs.next()) {
                al.add(rs.getInt("event_eventid"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("PersonIntegrator.getPerson | Unable to retrieve person", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 

        System.out.println("PersonIntegrator Retrieved history of size: " + al.size());
        return al;

    }

   
    
    
    /**
     * Builds a List of EventCnF objects given an ERG, which in June 2020 were
     * only CECase and OccPeriod objects
     * @param evHolder
     * @return
     * @throws IntegrationException 
     */
     public List<Integer> getEventList(IFace_EventHolder evHolder) throws IntegrationException{
        
     StringBuilder queryStub = new StringBuilder("SELECT eventid FROM public.event WHERE eventid IS NOT NULL AND deactivatedts IS NULL AND ");
            
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        List<Integer> evidl = new ArrayList<>();
        
        if(evHolder == null){
            return evidl;
        }
        
        if(evHolder instanceof OccPeriod){
            queryStub.append("occperiod_periodid=?;");
        } else if(evHolder instanceof CECase){
            queryStub.append("cecase_caseid=?;");
        } else if(evHolder instanceof PropertyDataHeavy){
            queryStub.append("parcel_parcelkey=?;");
        }

        try {
        
            stmt = con.prepareStatement(queryStub.toString());
            
            if(evHolder instanceof OccPeriod){
                OccPeriod op = (OccPeriod) evHolder;
                stmt.setInt(1, op.getPeriodID());
            } else if(evHolder instanceof CECase){
                CECase cec = (CECase) evHolder;
                stmt.setInt(1, cec.getCaseID());
            } else if(evHolder instanceof PropertyDataHeavy){
                PropertyDataHeavy pdh = (PropertyDataHeavy) evHolder;
                stmt.setInt(1, pdh.getParcelKey());
            }
            
            rs = stmt.executeQuery();
            
            while (rs.next()) {
                evidl.add(rs.getInt("eventid"));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot generate list of events", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 

        return evidl;
    }
     
     
     /**
      * Extracts events linked to the given implementing class
      * @param evLinked 
      * @return 
      */
     public List<Integer> getLinkedEventIDList(IFace_EventLinked evLinked) throws IntegrationException{
        if(evLinked == null || evLinked.getEventLinkEnum() == null){
             throw new IntegrationException("Cannot get linked events with null eventLinked or null enum");
        }
             
        StringBuilder queryStub = new StringBuilder("SELECT event_eventid FROM public.eventlinkage WHERE linkid IS NOT NULL AND deactivatedts IS NULL AND ");
            
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        List<Integer> evidl = new ArrayList<>();
        
        queryStub.append(evLinked.getEventLinkEnum().getTargetTableFKFieldString());
        queryStub.append("=?;");
        
        try {
            stmt = con.prepareStatement(queryStub.toString());
            
            stmt.setInt(1, evLinked.getLinkTargetPrimaryKey());
            
            rs = stmt.executeQuery();
            
            while (rs.next()) {
                evidl.add(rs.getInt("event_eventid"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot generate list of linked event IDs", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return evidl;
     }
     
     /**
      * Writes a new record to the eventlinkage table which specifies
      * connections between non-parent-of-event objects such as citations 
      * and notices of violation
      * 
      * @param event
      * @param evLinked 
      */
     public void linkObjectToEvent(EventCnF event, IFace_EventLinked evLinked) throws IntegrationException{
           if(event == null || evLinked == null || evLinked.getEventLinkEnum() == null) {
             throw new IntegrationException("Cannot deac event links with null event or event linked or its field enum holder");
         }
        
        StringBuilder sb = new StringBuilder();
        
        sb.append("INSERT INTO public.eventlinkage(linkid, event_eventid, ");
        sb.append(evLinked.getEventLinkEnum().getTargetTableFKFieldString());
        sb.append(") VALUES (DEFAULT, ?, ?);");
        
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sb.toString());
            
            stmt.setInt(1, event.getEventID());
            stmt.setInt(2, evLinked.getLinkTargetPrimaryKey());
            
            stmt.execute();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot write event links", ex);

        } finally {
            releasePostgresConnection(con, stmt);
        } 
         
     }
     
     /**
      * Writes a deac timestamp to ALL RECORDS in the eventlinkage table
      * which are keyed to the given event AND the given implementing class of
      * IFace_EventLinked. Since the eventLink process is simplified, the linked 
      * events aren't carying around their specific linkID so I can't deactivate 
      * specific links within the event-object connection subrealm.
      * 
      * 
      * @param event
      * @param evLinked 
      */
     public void deactivateEventLinks(EventCnF event, IFace_EventLinked evLinked) throws IntegrationException{
         if(event == null || evLinked == null || evLinked.getEventLinkEnum() == null) {
             throw new IntegrationException("Cannot deac event links with null event or event linked or its field enum holder");
         }
        
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE public.eventlinkage ");
        sb.append("   SET  deactivatedts=now() " );
        sb.append(" WHERE event_eventid=? AND ");
        sb.append(evLinked.getEventLinkEnum().getTargetTableFKFieldString());
        sb.append("=?;");
        
        
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sb.toString());
            
            stmt.setInt(1, event.getEventID());
            stmt.setInt(2, evLinked.getLinkTargetPrimaryKey());
            
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot update event", ex);

        } finally {
            releasePostgresConnection(con, stmt);
        } 
     }
     
    
    /**
     * Attaches an EventCnF to a code enforcement case. No checking of logic occurs
     * in this integration method, so the caller should always be a coordiantor
     * who has vetted the event and the associated case.
     *
     * @param event
     * @return the id of the event just inserted
     * @throws IntegrationException when the system is unable to store event in
     * DB
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public int insertEvent(EventCnF event) throws IntegrationException, BObStatusException {
        if(event == null) return 0;
        int insertedEventID = 0;

        String query = "INSERT INTO public.event(\n" +
                        "            eventid, category_catid, cecase_caseid, createdts, eventdescription, \n" +
                        "            createdby_userid, notes, occperiod_periodid, timestart, \n" +
                        "            timeend, lastupdatedby_userid, lastupdatedts, parcel_parcelkey)\n" +
                        "    VALUES (DEFAULT, ?, ?, now(), ?, \n" +
                        "            ?, ?, ?, ?, \n" +
                        "            ?, ?, now(), ?);"; 
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            
            if(event.getCategory() != null){
                stmt.setInt(1, event.getCategory().getCategoryID());
            } else {
                throw new BObStatusException("EventIntegrator.insertEvent | Cannot insert without a non-null category");
            }
            
            if(event.getDomain() == EventRealm.CODE_ENFORCEMENT && event.getCeCaseID() != 0){
                stmt.setInt(2, event.getCeCaseID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }

            // note that the timestamp is set by a call to postgres's now()
   
            stmt.setString(3, event.getDescription());
            
            if(event.getCreatedBy()!= null){
                stmt.setInt(4, event.getCreatedBy().getUserID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            
            stmt.setString(5, event.getNotes());
            
            if(event.getDomain() == EventRealm.OCCUPANCY && event.getOccPeriodID() != 0){
                stmt.setInt(6, event.getOccPeriodID());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            
            if (event.getTimeStart() != null) {
                stmt.setTimestamp(7, java.sql.Timestamp.valueOf(event.getTimeStart()));
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            
            
            if (event.getTimeEnd() != null) {
                stmt.setTimestamp(8, java.sql.Timestamp.valueOf(event.getTimeEnd()));
            } else {
                stmt.setNull(8, java.sql.Types.NULL);
            }
            
            if(event.getLastUpdatedBy() != null){
                stmt.setInt(9, event.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(9, java.sql.Types.NULL);
            }
            
            if(event.getDomain() == EventRealm.PARCEL && event.getParcelKey() != 0){
                stmt.setInt(10, event.getParcelKey());
            } else {
                stmt.setNull(10, java.sql.Types.NULL);
            }
            
            stmt.execute();

            String retrievalQuery = "SELECT currval('ceevent_eventID_seq');";
            stmt = con.prepareStatement(retrievalQuery);

            rs = stmt.executeQuery();
            while (rs.next()) {
                insertedEventID = rs.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert Event into system", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 

        // now connect people to event that has already been logged
      
        
        return insertedEventID;

    } // close method
    
    
    /**
     * Updates a record in the event table
     * @param event
     * @throws IntegrationException 
     */
    public void updateEventCategoryMaintainType(EventCnF event) throws IntegrationException {
        if(event == null) return;
        
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE public.event\n");
        sb.append("   SET  category_catid=?\n" );
        sb.append(" WHERE eventid=?;");

        // TO DO: finish clearing view confirmation
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sb.toString());
            
            
            if(event.getCategory() != null){
                stmt.setInt(1, event.getCategory().getCategoryID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            stmt.setInt(2, event.getEventID());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot update event", ex);

        } finally {
            releasePostgresConnection(con, stmt);
        } 
    }
    


    /**
     * Updates a record in the event table
     * @param event
     * @throws IntegrationException 
     */
    public void updateEvent(EventCnF event) throws IntegrationException {
        if(event == null) return;
        PersonIntegrator pi = getPersonIntegrator();
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE public.event\n");
        sb.append("""
                     SET cecase_caseid=?, eventdescription=?, 
                         occperiod_periodid=?, 
                         timestart=?, timeend=?, lastupdatedby_userid=?, lastupdatedts=now(), 
                         deactivatedts=?, deactivatedby_userid=?, parcel_parcelkey=? 
                  """);
        sb.append(" WHERE eventid=?;");

        // TO DO: finish clearing view confirmation
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sb.toString());
            
            
            if(event.getDomain() == EventRealm.CODE_ENFORCEMENT && event.getCeCaseID() != 0){
                stmt.setInt(1, event.getCeCaseID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }

            // note that the timestamp is set by a call to postgres's now()
   
            stmt.setString(2, event.getDescription());
            
            if(event.getDomain() == EventRealm.OCCUPANCY && event.getOccPeriodID() != 0){
                stmt.setInt(3, event.getOccPeriodID());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            
            if (event.getTimeStart() != null) {
                stmt.setTimestamp(4, java.sql.Timestamp.valueOf(event.getTimeStart()));
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
                      
            
            if (event.getTimeEnd() != null) {
                stmt.setTimestamp(5, java.sql.Timestamp.valueOf(event.getTimeEnd()));
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            
            if(event.getLastUpdatedBy() != null){
                stmt.setInt(6, event.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            
            if (event.getDeactivatedTS() != null) {
                stmt.setTimestamp(7, java.sql.Timestamp.valueOf(event.getDeactivatedTS()));
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            
            if (event.getDeactivatedBy() != null) {
                stmt.setInt(8, event.getDeactivatedBy().getUserID());
            } else {
                stmt.setNull(8, java.sql.Types.NULL);
            }
            
            if(event.getDomain() == EventRealm.PARCEL && event.getParcelKey() != 0){
                stmt.setInt(9, event.getParcelKey());
            } else {
                stmt.setNull(9, java.sql.Types.NULL);
            }
            
            stmt.setInt(10, event.getEventID());
            
            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot update  event", ex);

        } finally {
            releasePostgresConnection(con, stmt);
        } 
        
    }
    

    /**
     * Updates a record in the event table
     * @param event
     * @throws IntegrationException 
     */
    public void updateEventEndTime(EventCnF event) throws IntegrationException {
        if(event == null) return;
        PersonIntegrator pi = getPersonIntegrator();
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE public.event\n");
        sb.append("""
                     SET timeend=? 
                  """);
        sb.append(" WHERE eventid=?;");

        // TO DO: finish clearing view confirmation
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sb.toString());
            
            
            
            if (event.getTimeEnd() != null) {
                stmt.setTimestamp(1, java.sql.Timestamp.valueOf(event.getTimeEnd()));
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
       
            stmt.setInt(2, event.getEventID());
            
            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot update  event end time", ex);

        } finally {
            releasePostgresConnection(con, stmt);
        } 
    }
    
 
    

    /**
     * Updates only the notes field and lastupdatedby field
     * @param event
     * @throws IntegrationException 
     */
    public void updateEventNotes(EventCnF event) throws IntegrationException {
        if(event == null) return;
        PersonIntegrator pi = getPersonIntegrator();
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE public.event\n");
        sb.append("   SET notes=?, lastupdatedby_userid=?, lastupdatedts=now() \n" );
        sb.append(" WHERE eventid=?;");

        // TO DO: finish clearing view confirmation
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sb.toString());
            stmt.setString(1, event.getNotes());
            if(event.getLastUpdatedBy() != null){
                stmt.setInt(2, event.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            stmt.setInt(3, event.getEventID());
            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot retrive event", ex);

        } finally {
            releasePostgresConnection(con, stmt);
        } 
    }
    
    /**
     * Primary search method for EventCnF objects system wide!
     * @param params
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
     public List<Integer> searchForEvents(SearchParamsEvent params) 
            throws IntegrationException, BObStatusException {
        if(params == null){
            throw new IntegrationException("cannot search for events with null SearchParamsEvent");
            
        }        
        SearchCoordinator sc = getSearchCoordinator();
        List<Integer> evidlst = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        Connection con = getPostgresCon();

        // we need an EventDomain for the BOBID, too, so set it arbitrarily if it's null
        if(params.isEventDomain_ctl() && params.getEventDomain_val() == null){
            params.setEventDomain_val(EventRealm.UNIVERSAL);
            params.appendToParamLog("DOMAIN CONTROL: no object specified - UNIVERSAL chosen as default; | ");
        }
        
        params.appendSQL("SELECT DISTINCT eventid \n");
        params.appendSQL("FROM public.event INNER JOIN public.eventcategory ON (category_catid = categoryid) \n");
        params.appendSQL("LEFT OUTER JOIN public.eventhuman ON (eventhuman.event_eventid = event.eventid) \n");
        // to get to property and hence municipality, we must traverse different key pathways
        // through the database for CE versus Occ. This is all backflippy crazy shit because
        // of the decision to maintain only one event table for both Occ events and CE events and property events.
//        if(params.getEventDomain_val().equals(EventRealm.CODE_ENFORCEMENT)){
            params.appendSQL("LEFT OUTER JOIN public.cecase ON (cecase.caseid = event.cecase_caseid) \n");
            params.appendSQL("LEFT OUTER JOIN public.occperiod ON (occperiod.periodid = event.occperiod_periodid) \n");
            params.appendSQL("LEFT OUTER JOIN public.parcelunit ON (parcelunit.unitid= occperiod.parcelunit_unitid) \n");
            params.appendSQL("LEFT OUTER JOIN public.parcel ON (cecase.parcel_parcelkey = parcel.parcelkey OR parcelunit.parcel_parcelkey = parcel.parcelkey OR event.parcel_parcelkey = parcel.parcelkey)  \n");
            params.appendSQL("LEFT OUTER JOIN public.eventlinkage ON (event.eventid = eventlinkage.eventlink_eventid) ");
            
//        } else {
            // with only two enum values now, we either have Code enf or occ
//            params.appendSQL("LEFT OUTER JOIN public.parcel ON (parcel.parcelkey = parcelunit.parcel_parcelkey) \n");
//        }
        params.appendSQL("WHERE eventid IS NOT NULL \n");
        
        // as long as this isn't an ID only search, do the normal SQL building process
        if (!params.isBobID_ctl()) {
            
            //*******************************
           // **   MUNI,DATES,USER,ACTIVE  **
           // *******************************
            params = (SearchParamsEvent) sc.assembleBObSearchSQL_muniDatesUserActive(
                                                                        params, 
                                                                        SearchParamsEvent.MUNI_DBFIELD,
                                                                        ACTIVE_FIELD);

            //*******************************
           // **      1.EVENT CATEGORY     **
           // *******************************
            if (params.isEventCat_ctl() ) {
                if(params.getEventCat_val() != null){
                    params.appendSQL("AND eventcategory.categoryid=? ");
                } else {
                    params.setEventCat_ctl(false);
                    params.appendToParamLog("EVENT CATEGORY: no object specified; event cat filter disabled; |"); 
                }
            }

           // *******************************
           // **      2.EVENT TYPE         **
           // *******************************
            if (params.isEventType_ctl() ) {
                if(params.getEventType_val()!= null){
                    params.appendSQL("AND public.eventcategory.categorytype = CAST(? AS eventType) ");
                } else {
                    params.setEventType_ctl(false);
                    params.appendToParamLog("EVENT TYPE: no object specified; event type filter disabled; | ");
                }
            }
            
            
           // *******************************
           // **     3.EVENT DOMAIN        **
           // *******************************
            if(params.isEventDomain_ctl()){
                switch(params.getEventDomain_val()){
                    case UNIVERSAL:
                        break; // no restricting sql required
     
                    default: // catches parcel, CECase and occ periods
                        params.appendSQL("AND ");
                        params.appendSQL(params.getEventDomain_val().getDbField()); //Code enf or Occ
                        params.appendSQL(" ");
                        params.appendSQL("IS NOT NULL ");
                }
            }
            
           // *******************************
           // **   4.EVENT DOMAIN PARENT BOB ID   **
           // *******************************
            if(params.isEventDomainPK_ctl()){
                params.appendSQL("AND ");
                params.appendSQL(params.getEventDomain_val().getDbField());
                params.appendSQL("=? ");
            }
            
           // *******************************
           // **   5.EVENT PERSONS         **
           // *******************************
            if (params.isPerson_ctl()) {
                if(params.getPerson_val() != null){
                    params.appendSQL("AND eventhuman.human_humanid=? ");
                } else {
                    params.setPerson_ctl(false);
                    params.appendToParamLog("EVENT PERSONS: No human object specified; person filter disabled; | ");
                }
            }

           // *******************************
           // **    6.ROLE FLOOR TO ENACT**
           // *******************************
            if (params.isRolefloor_enact_ctl()) {
                if (params.getRoleFloor_enact_val() != null) {
                    params.appendSQL("AND eventcategory.rolefloorenact=CAST(? AS role)");
                } else {
                    params.setRolefloor_enact_ctl(false);
                    params.appendToParamLog("EVENT ROLE FLOOR ENACT: No role object included in value field; enact role floor filter disabled; | ");
                }
            }

           // *******************************
           // **    7.ROLE FLOOR TO VIEW**
           // *******************************
            if (params.isRolefloor_view_ctl()) {
                if (params.getRoleFloor_view_val() != null) {
                    params.appendSQL("AND eventcategory.rolefloorviewt=CAST(? AS role)");
                } else {
                    params.setRolefloor_view_ctl(false);
                    params.appendToParamLog("EVENT ROLE FLOOR VIEW: No role object included in value field; view role floor filter disabled; | ");
                }
            }

           // *******************************
           // **    8.ROLE FLOOR TO UPDATE**
           // *******************************
            if (params.isRolefloor_update_ctl()) {
                if (params.getRoleFloor_update_val() != null) {
                    params.appendSQL("AND eventcategory.rolefloorupdate=CAST(? AS role)");
                } else {
                    params.setRolefloor_update_ctl(false);
                    params.appendToParamLog("EVENT ROLE FLOOR UPDATE: No role object included in value field; update role floor filter disabled; | ");
                }
            }
            
           // *******************************
           // **   9.PROPERTY              **
           // *******************************
            if (params.isProperty_ctl()) {
                if(params.getProperty_val()!= null){
                    params.appendSQL("AND parcel.parcelkey=? ");
                } else {
                    params.setProperty_ctl(false);
                    params.appendToParamLog("PROPERTY: No PROPERTY object specified; filter disabled; | ");
                }
            }
            
           // *******************************
           // **   10. NOTIFY        **
           // *******************************
            if (params.isNotify_ctl()) {
                params.appendSQL("AND eventcategory.notifymonitors=");
                if (params.isNotify_val()) {
                    params.appendSQL("TRUE ");
                } else {
                    params.appendSQL("FALSE ");
                }
            }
            
           // *******************************
           // **   11. FOLLOW UP           **
           // *******************************
            if (params.isFollowup_ctl()) {
                params.appendSQL(" AND eventcategory.followuprequired=");
                if (params.isFollowup_val()) {
                    params.appendSQL("TRUE ");
                } else {
                    params.appendSQL("FALSE ");
                }
            }
           
           // *******************************
           // **   10. Follow up complete  **
           // *******************************
            if (params.isFollowupComplete_ctl()) {
                params.appendSQL("AND eventlinkage.eventlink_eventid IS ");
                if (params.isFollowupComplete_val()) {
                    params.appendSQL("NOT NULL ");
                } else {
                    params.appendSQL("NULL ");
                }
            }
           
            
        } else {
            params.appendSQL("AND event.eventid=? "); // will be param 1 with ID search
        }
        int paramCounter = 0;
        params.appendSQL(";");
            
        try {
            stmt = con.prepareStatement(params.extractRawSQL());

            if (!params.isBobID_ctl()) {
                
                if (params.isMuni_ctl()) {
                     stmt.setInt(++paramCounter, params.getMuni_val().getMuniCode());
                }
                
                if(params.isDate_startEnd_ctl()){
                    stmt.setTimestamp(++paramCounter, params.getDateStart_val_sql());
                    stmt.setTimestamp(++paramCounter, params.getDateEnd_val_sql());
                 }
                
                if (params.isUser_ctl()) {
                   stmt.setInt(++paramCounter, params.getUser_val().getUserID());
                }

                if (params.isEventCat_ctl()) {
                    stmt.setInt(++paramCounter, params.getEventCat_val().getCategoryID());
                }
                
                if (params.isEventType_ctl()) {
                    stmt.setString(++paramCounter, params.getEventType_val().name());
                }

                if(params.isEventDomainPK_ctl()){
                    stmt.setInt(++paramCounter, params.getEventDomainPK_val());
                }
                
                if (params.isPerson_ctl()) {
                    stmt.setInt(++paramCounter, params.getPerson_val().getHumanID());
                }
                
                if(params.isRolefloor_enact_ctl()) {
                    stmt.setString(++paramCounter, params.getRoleFloor_enact_val().name());
                }
                if(params.isRolefloor_view_ctl()) {
                    stmt.setString(++paramCounter, params.getRoleFloor_view_val().name());
                }
                if(params.isRolefloor_update_ctl()) {
                    stmt.setString(++paramCounter, params.getRoleFloor_update_val().name());
                }
                
                if (params.isProperty_ctl()) {
                    stmt.setInt(++paramCounter, params.getProperty_val().getParcelKey());
                }
                
                
                
            } else {
                stmt.setInt(++paramCounter, params.getBobID_val());
            }

            rs = stmt.executeQuery();

            int counter = 0;
            int maxResults;
            if (params.isLimitResultCount_ctl()) {
                maxResults = params.getLimitResultCount_val();
            } else {
                maxResults = Integer.MAX_VALUE;
            }
            while (rs.next() && counter < maxResults) {
                evidlst.add(rs.getInt("eventid"));
                counter++;
            }

        } catch (SQLException ex) {
            System.out.println(ex);
//            throw new IntegrationException("Integration Error: Problem retrieving and generating action request list", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }
        return evidlst;
    }
    
    
//    --------------------------------------------------------------------------
//    ************************** EVENT CATEGORIES ****************************** 
//    --------------------------------------------------------------------------
    
    
    /**
     * Cache-Backed getter for event categories
     * @param catID
     * @return
     * @throws IntegrationException
     * @throws DatabaseFetchRuntimeException 
     */
    public EventCategory getEventCategory(int catID) throws IntegrationException, DatabaseFetchRuntimeException {
        if(catID == 0){
            throw new IntegrationException("Cannot get event category with an ID of 0");
        }
        if(getEventCacheManager().isCachingEnabled()){
            return getEventCacheManager().getCacheEventCategory().get(catID, k -> fetchEventCategory(catID));
        } else {
            return fetchEventCategory(catID);
        }
        
    }
    
    
    
     /**
     * Base record retrieval method for EventCategory objects
     * @param catID
     * @return fully baked
     * @throws DatabaseFetchRuntimeException
     */
    public EventCategory fetchEventCategory(int catID) throws DatabaseFetchRuntimeException {

        String query =  """
                         SELECT categoryid, categorytype, title, description, notifymonitors, 
                                hidable, icon_iconid, relativeorderwithintype, relativeorderglobal, 
                                hosteventdescriptionsuggtext, directive_directiveid, defaultdurationmins, 
                                active, rolefloorenact, rolefloorview, rolefloorupdate, 
                                alertevent, alertevent_stopcategoryid, followuprequired, adminevent 
                          FROM public.eventcategory WHERE categoryid=?;""";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        EventCategory ec = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, catID);
            rs = stmt.executeQuery();

            while (rs.next()) {
                ec = generateEventCategoryFromRS(rs);
            }

        } catch (SQLException | IntegrationException ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("Cannot get event categry", ex);

        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 

        return ec;
    }

    /**
     * Extracts values from ResultSet and populates object as appropriate
     * @param rs
     * @return
     * @throws SQLException
     * @throws IntegrationException 
     */
    private EventCategory generateEventCategoryFromRS(ResultSet rs) throws SQLException, IntegrationException {
        SystemIntegrator si = getSystemIntegrator();
        EventCoordinator evCoor = getEventCoordinator();
        WorkflowIntegrator choiceInt = getWorkflowIntegrator();
        
        EventCategory ec = new EventCategory();
        
        ec.setCategoryID(rs.getInt("categoryid"));
        
        if(!(rs.getString("categoryType") == null) && !(rs.getString("categoryType").equals(""))){
            ec.setEventType(EventType.valueOf(rs.getString("categoryType")));
        }
        ec.setEventCategoryTitle(rs.getString("title"));
        ec.setEventCategoryDesc(rs.getString("description"));

        ec.setNotifymonitors(rs.getBoolean("notifymonitors"));
        ec.setHidable(rs.getBoolean("hidable"));
        
        if(rs.getInt("icon_iconid") != 0){
            ec.setIcon(si.getIcon(rs.getInt("icon_iconid")));
        }

        ec.setRelativeOrderWithinType(rs.getInt("relativeorderwithintype"));
        ec.setRelativeOrderGlobal(rs.getInt("relativeorderglobal"));
        ec.setHostEventDescriptionSuggestedText(rs.getString("hosteventdescriptionsuggtext"));
        
        if(rs.getInt("directive_directiveid") != 0){
            try {
                ec.setDirective(choiceInt.getDirective(rs.getInt("directive_directiveid")));
            } catch (BObStatusException ex) {
                throw new IntegrationException(ex.getMessage());
            }
        }
        ec.setActive(rs.getBoolean("active"));
        ec.setDefaultDurationMins(rs.getInt("defaultdurationmins"));
        if(rs.getString("rolefloorenact") != null && !rs.getString("rolefloorenact").equals("")){
            ec.setRoleFloorEventEnact(RoleType.valueOf(rs.getString("rolefloorenact")));
        }
        
        if(rs.getString("rolefloorview") != null && !rs.getString("rolefloorview").equals("")){
            ec.setRoleFloorEventView(RoleType.valueOf(rs.getString("rolefloorview")));
        }
        
        if(rs.getString("rolefloorupdate") != null && !rs.getString("rolefloorupdate").equals("")){
            ec.setRoleFloorEventUpdate(RoleType.valueOf(rs.getString("rolefloorupdate")));
        }
        
        ec.setAlertEvent(rs.getBoolean("alertevent"));
        if(rs.getInt("alertevent_stopcategoryid") != 0){
            ec.setAlertStopCategory(evCoor.getEventCategory(rs.getInt("alertevent_stopcategoryid")));
        }
        
        ec.setFollowUpEvent(rs.getBoolean("followuprequired"));
        ec.setAdminEvent(rs.getBoolean("adminevent"));
        
        return ec;
    }
   
    
    
    /**
     * Fetches a complete list of eventcategory records, including inactive cats
     * @return
     * @throws IntegrationException 
     */
    public List<EventCategory> getEventCategoryList() throws IntegrationException {
        String query = "SELECT categoryid FROM public.eventcategory WHERE active=TRUE;";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        ArrayList<EventCategory> categoryList = new ArrayList();

        try {
            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();
            while (rs.next()) {
                categoryList.add(getEventCategory(rs.getInt("categoryid")));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot generate list of event categories", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 

        return categoryList;
    }
    
    
    /**
     * List produced here ultimately used for actively picking event categories by the user
     * @param et
     * @return
     * @throws IntegrationException
     */
    public List<EventCategory> getEventCategoryList(EventType et) throws IntegrationException {
        String query = "SELECT categoryid FROM public.eventcategory WHERE categorytype = CAST (? as eventtype) AND eventcategory.active=TRUE;";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        ArrayList<EventCategory> categoryList = new ArrayList();
        EventCoordinator ec = getEventCoordinator();
        
        try {

            stmt = con.prepareStatement(query);
            stmt.setString(1, et.toString());
            rs = stmt.executeQuery();
            while (rs.next()) {
                categoryList.add(ec.getEventCategory(rs.getInt("categoryid")));
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot generate list of event categories by type", ex);

        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 

        return categoryList;
    }

    /**
     * Creates a new record in the eventcategory table
     * @param ec
     * @return 
     * @throws IntegrationException 
     */
    public int insertEventCategory(EventCategory ec) throws IntegrationException {

        String query = """
                       INSERT INTO public.eventcategory(
                                   categoryid, categorytype, title, description, notifymonitors, 
                                   hidable, icon_iconid, relativeorderwithintype, relativeorderglobal, 
                                   hosteventdescriptionsuggtext, directive_directiveid, defaultdurationmins, 
                                   active, rolefloorenact, rolefloorview, rolefloorupdate, prioritygreenbufferdays,
                                    alertevent, alertevent_stopcategoryid, followuprequired, adminevent)
                           VALUES (DEFAULT, CAST(? AS EVENTTYPE), ?, ?, ?, 
                                   ?, ?, ?, ?, 
                                   ?, ?, ?, 
                                   ?, CAST(? AS role), CAST(? AS ROLE), CAST(? AS ROLE), ?,
                                    ?, ?, ?, ?);""";

        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int freshID = 0;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setString(1, ec.getEventType().name());
            stmt.setString(2, ec.getEventCategoryTitle());
            stmt.setString(3, ec.getEventCategoryDesc());
            stmt.setBoolean(4, ec.isNotifymonitors());
            
            stmt.setBoolean(5, ec.isHidable());

            if(ec.getIcon() != null){
                stmt.setInt(6, ec.getIcon().getID());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            stmt.setInt(7, ec.getRelativeOrderWithinType());
            stmt.setInt(8, ec.getRelativeOrderGlobal());
            
            stmt.setString(9, ec.getHostEventDescriptionSuggestedText());
            if(ec.getDirective() != null){
                    stmt.setInt(10, ec.getDirective().getDirectiveID());
            } else {
                stmt.setNull(10, java.sql.Types.NULL);
            }
            stmt.setInt(11, ec.getDefaultDurationMins());

            stmt.setBoolean(12, ec.isActive());
            if(ec.getRoleFloorEventEnact() != null){
                stmt.setString(13, ec.getRoleFloorEventEnact().toString());
            } else {
                stmt.setNull(13, java.sql.Types.NULL);
            }
            
            if(ec.getRoleFloorEventView() != null){
                stmt.setString(14, ec.getRoleFloorEventView().toString());
            } else {
                stmt.setNull(14, java.sql.Types.NULL);
            }
            
            if(ec.getRoleFloorEventUpdate() != null){
                stmt.setString(15, ec.getRoleFloorEventUpdate().toString());
            } else {
                stmt.setNull(15, java.sql.Types.NULL);
            }
            
            if(ec.getGreenBufferDays() != 0){
                stmt.setInt(16, ec.getGreenBufferDays());
            } else {
                stmt.setNull(16, java.sql.Types.NULL);
            }

            stmt.setBoolean(17, ec.isAlertEvent());
            if(ec.getAlertStopCategory() != null){
                stmt.setInt(18, ec.getAlertStopCategory().getCategoryID());
            } else {
                stmt.setNull(18, java.sql.Types.NULL);
            }
            
            stmt.setBoolean(19, ec.isFollowUpEvent());
            stmt.setBoolean(20, ec.isAdminEvent());
            
            stmt.execute();
            
            String retrievalQuery = "SELECT currval('ceeventcategory_categoryid_seq');";
            stmt = con.prepareStatement(retrievalQuery);

            rs = stmt.executeQuery();
            while (rs.next()) {
                freshID = rs.getInt("currval");

            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to insert event category", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return freshID;
    }

    /**
     * Updates a singel record in the eventcategory table
     * @param ec
     * @throws IntegrationException 
     */
    public void updateEventCategory(EventCategory ec) throws IntegrationException {

        String query =  """
                        UPDATE public.eventcategory
                           SET  categorytype=CAST(? AS eventtype), title=?, description=?, notifymonitors=?, 
                                hidable=?, icon_iconid=?, relativeorderwithintype=?, relativeorderglobal=?, 
                                hosteventdescriptionsuggtext=?, directive_directiveid=?, defaultdurationmins=?, 
                                active=?, rolefloorenact=CAST(? AS role), rolefloorview=CAST(? AS role), 
                                rolefloorupdate=CAST(? AS role), prioritygreenbufferdays=?, 
                                alertevent=?, alertevent_stopcategoryid=?, followuprequired=?, adminevent=?
                        WHERE categoryid = ?;
                        """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            stmt.setString(1, ec.getEventType().name());
            stmt.setString(2, ec.getEventCategoryTitle());
            stmt.setString(3, ec.getEventCategoryDesc());
            stmt.setBoolean(4, ec.isNotifymonitors());
            
            stmt.setBoolean(5, ec.isHidable());

            if(ec.getIcon() != null){
                stmt.setInt(6, ec.getIcon().getID());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            stmt.setInt(7, ec.getRelativeOrderWithinType());
            stmt.setInt(8, ec.getRelativeOrderGlobal());
            
            stmt.setString(9, ec.getHostEventDescriptionSuggestedText());
            if(ec.getDirective() != null){
                    stmt.setInt(10, ec.getDirective().getDirectiveID());
            } else {
                stmt.setNull(10, java.sql.Types.NULL);
            }
            stmt.setInt(11, ec.getDefaultDurationMins());

            stmt.setBoolean(12, ec.isActive());
            if(ec.getRoleFloorEventEnact() != null){
                stmt.setString(13, ec.getRoleFloorEventEnact().toString());
            } else {
                stmt.setNull(13, java.sql.Types.NULL);
            }
            
            if(ec.getRoleFloorEventView() != null){
                stmt.setString(14, ec.getRoleFloorEventView().toString());
            } else {
                stmt.setNull(14, java.sql.Types.NULL);
            }
            
            if(ec.getRoleFloorEventUpdate() != null){
                stmt.setString(15, ec.getRoleFloorEventUpdate().toString());
            } else {
                stmt.setNull(15, java.sql.Types.NULL);
            }
            
            if(ec.getGreenBufferDays() != 0){
                stmt.setInt(16, ec.getGreenBufferDays());
            } else {
                stmt.setNull(16, java.sql.Types.NULL);
            }
            
             stmt.setBoolean(17, ec.isAlertEvent());
            if(ec.getAlertStopCategory() != null){
                stmt.setInt(18, ec.getAlertStopCategory().getCategoryID());
            } else {
                stmt.setNull(18, java.sql.Types.NULL);
            }
            
            stmt.setBoolean(19, ec.isFollowUpEvent());
            stmt.setBoolean(20, ec.isAdminEvent());
            
            
            stmt.setInt(21, ec.getCategoryID());

            stmt.execute();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update event category", ex);

        } finally {
            releasePostgresConnection(con, stmt);
        } 
    }
} // close class