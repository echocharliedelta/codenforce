/*
 * Copyright (C) Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.integration;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheClient;
import com.tcvcog.tcvce.coordinators.*;
import com.tcvcog.tcvce.domain.*;
import com.tcvcog.tcvce.entities.*;
import com.tcvcog.tcvce.integration.*;
import com.tcvcog.tcvce.entities.occupancy.*;
import com.tcvcog.tcvce.entities.search.SearchParamsOccPeriod;
import com.tcvcog.tcvce.entities.search.SearchParamsOccPermit;
import java.io.Serializable;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import jakarta.annotation.PostConstruct;

/**
 * Integration methods that return populated business objects with all their
 composite elements all nicely situated on shelves and in Lists

 High-level object families produced here include: OccPeriod OccPermitType
 OccPermit OccPermitApplication
 *
 * @author ellen bascomb of apt 31y
 */
public class OccupancyIntegrator 
        extends BackingBeanUtils implements Serializable, IFaceCacheClient {

    
    final String ACTIVE_FIELD_OCCPERIOD = "occperiod.deactivatedts";
    final String ACTIVE_FIELD_OCCPERMIT = "occpermit.deactivatedts";
    
 
    
    
    @Override
    public void registerClientWithManager() {
        getOccupancyCacheManager().registerCacheClient(this);
    }
  
  
    
    /**
     * Extracts all active occ periods from the DB using a unitID
     * @param unitID
     * @return
     * @throws IntegrationException
     * @throws EventException
     * @throws AuthorizationException
     * @throws BObStatusException
     * @throws ViolationException 
     */
    public List<Integer> getOccPeriodIDListByUnitID(int unitID) throws IntegrationException, EventException, AuthorizationException, BObStatusException, ViolationException {
        List<Integer> opIDList = new ArrayList<>();
        String query = "SELECT periodid FROM public.occperiod WHERE parcelunit_unitid=? AND deactivatedts IS NULL;";

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, unitID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                opIDList.add(rs.getInt("periodid"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to build occ period list by unit ID", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return opIDList;
    }

    /**
     * Primary entry point for searches against the occ permit table
     * @param spop
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
  public List<Integer> searchForOccPermits(SearchParamsOccPermit spop) throws IntegrationException{
        
        if(spop == null){
            throw new IntegrationException("cannot search with null params");
            
        }
        List<Integer> permitIDList = new ArrayList<>();
        
        SearchCoordinator sc = getSearchCoordinator();
        
        spop.appendSQL("SELECT permitid FROM occpermit\n");
        spop.appendSQL("INNER JOIN occperiod ON (occperiod.periodid = occpermit.occperiod_periodid)\n");
        spop.appendSQL("INNER JOIN parcelunit ON (occperiod.parcelunit_unitid = parcelunit.unitid)\n");
        spop.appendSQL("INNER JOIN parcel ON (parcelunit.parcel_parcelkey = parcel.parcelkey)\n" );
        spop.appendSQL("INNER JOIN municipality ON (parcel.muni_municode = municipality.municode)\n" );
        spop.appendSQL("WHERE permitid IS NOT NULL ");                        
        
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            if (!spop.isBobID_ctl()) {
           
                spop = (SearchParamsOccPermit) sc.assembleBObSearchSQL_muniDatesUserActive(spop, 
                                                            SearchParamsOccPermit.MUNI_DBFIELD,
                                                            ACTIVE_FIELD_OCCPERMIT);
                if(spop.isPermitTypeList_ctl()){
                    if(spop.getPermitTypeList_val() != null && !spop.getPermitTypeList_val().isEmpty()){
                        StringBuilder sb = new StringBuilder("AND ");
                        int counter = 1;
                        // we won't use the actual type here in this for
                        for(OccPermitType opt: spop.getPermitTypeList_val()){
                            sb.append("permittype_typeid=? ");
                            counter++;
                            if(counter < spop.getPermitTypeList_val().size()){
                                sb.append("OR ");
                            }
                        }
                        spop.appendSQL(sb.toString());
                    } else {
                        // we're not configured to search for any permits by type so turn off ctl
                        spop.setPermitTypeList_ctl(false);
                    }
                }
                
                if(spop.isDraft_ctl()){
                    if(spop.isDraft_val()){
                        spop.appendSQL("AND finalizedts IS NULL ");
                    } else {
                        spop.appendSQL("AND finalizedts IS NOT NULL ");
                    }
                }
                
                if(spop.isNullified_ctl()){
                    if(spop.isNullified_val()){
                        spop.appendSQL("AND nullifiedts IS NOT NULL ");
                    } else {
                        spop.appendSQL("AND nullifiedts IS NULL ");
                    }
                }
                
                if(spop.isReferenceNo_ctl()){
                    spop.appendSQL("AND referenceno ILIKE ? ");
                }
            // this is our special ID only path    
            } else {
                spop.appendSQL("AND permitid=?");
            }
            
            spop.appendSQL(";");
            
            stmt = con.prepareStatement(spop.extractRawSQL());
            
            int paramCounter = 0;

            if (!spop.isBobID_ctl()) {
                // start standard field injections
                if (spop.isMuni_ctl()) {
                     stmt.setInt(++paramCounter, spop.getMuni_val().getMuniCode());
                }
                
                if(spop.isDate_startEnd_ctl()){
                    stmt.setTimestamp(++paramCounter, spop.getDateStart_val_sql());
                    stmt.setTimestamp(++paramCounter, spop.getDateEnd_val_sql());
                 }
                
                if (spop.isUser_ctl()) {
                   stmt.setInt(++paramCounter, spop.getUser_val().getUserID());
                }
                // start permit specific query injections
                if(spop.isPermitTypeList_ctl()){
                    for(OccPermitType opt: spop.getPermitTypeList_val()){
                        stmt.setInt(++paramCounter, opt.getTypeID());
                    }
                }
                
                if(spop.isReferenceNo_ctl()){
                    StringBuilder str = new StringBuilder();
                    str.append("%");
                    str.append(spop.getReferenceNo_val());
                    str.append("%");
                    stmt.setString(++paramCounter, str.toString());
                }
                
            } else {
                stmt.setInt(++paramCounter, spop.getBobID_val());
            }

            rs = stmt.executeQuery();

            int counter = 0;
            int maxResults;
            if (spop.isLimitResultCount_ctl()) {
                maxResults = spop.getLimitResultCount_val();
            } else {
                maxResults = Integer.MAX_VALUE;
            }
            while (rs.next() && counter < maxResults) {
                permitIDList.add(rs.getInt("permitid"));
                counter++;
            }
            
           
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to search for occ permits", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return permitIDList;
      
  }

    /**
     * Single point of entry for queries against the OccPeriod table
     * @param params
     * @return 
     */
    public List<Integer> searchForOccPeriods(SearchParamsOccPeriod params) {
        SearchCoordinator sc = getSearchCoordinator();
        
        List<Integer> periodList = new ArrayList<>();
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        params.appendSQL("SELECT DISTINCT occperiod.periodid ");
        params.appendSQL("FROM occperiod \n");
        params.appendSQL("INNER JOIN parcelunit ON (occperiod.parcelunit_unitid = parcelunit.unitid) \n");
        params.appendSQL("INNER JOIN parcel ON (parcelunit.parcel_parcelkey = parcel.parcelkey) \n ");
        params.appendSQL("LEFT OUTER JOIN occinspection ON (occinspection.occperiod_periodid = periodid) \n");
        params.appendSQL("LEFT OUTER JOIN occpermit ON (occpermit.occperiod_periodid = periodid) \n ");
        params.appendSQL("LEFT OUTER JOIN humanoccperiod ON (occperiod.periodid = humanoccperiod.occperiod_periodid) \n");
        
        if(params.isEventwindow_ctl()){
            params.appendSQL("""
                             JOIN LATERAL (
                                 SELECT ev.*
                                 FROM event ev
                                 WHERE ev.occperiod_periodid = occperiod.periodid
                                   -- The key time-bound condition:
                                   AND ev.timestart >= CURRENT_TIMESTAMP - (? * INTERVAL '1 day')
                                 ORDER BY ev.timestart DESC
                                 LIMIT 1
                             ) e ON TRUE 
                             """);
        }
        
        params.appendSQL("WHERE occperiod.periodid IS NOT NULL ");

        
        if (!params.isBobID_ctl()) {
           // *******************************
           // **   MUNI,DATES,USER,ACTIVE  **
           // *******************************
            params = (SearchParamsOccPeriod) sc.assembleBObSearchSQL_muniDatesUserActive(params, 
                                                            SearchParamsOccPeriod.MUNI_DBFIELD,
                                                            ACTIVE_FIELD_OCCPERIOD);
            
           // *******************************
            // **        PROPERTY           **
            // *******************************
             if (params.isProperty_ctl()) {
                if(params.getProperty_val()!= null){
                    params.appendSQL("AND parcel_parcelkey=? ");
                } else {
                    params.setProperty_ctl(false);
                    params.appendToParamLog("Parcel: no parcel object; prop filter disabled");
                }
            }
            
            // *******************************
            // **       PROPERTY UNIT       **
            // *******************************
             if (params.isPropertyUnit_ctl()) {
                if(params.getPropertyUnit_val()!= null){
                    params.appendSQL("AND parcelunit_unitid=? ");
                } else {
                    params.setPropertyUnit_ctl(false);
                    params.appendToParamLog("parcel UNIT: no PropertyUnit object; propunit filter disabled");
                }
            }
            
            // *******************************
            // **       PERMITS             **
            // *******************************
            if (params.isPermitIssuance_ctl()) {
                if (params.isPermitIssuance_val()) {
                    params.appendSQL("occpermit.dateissued IS NOT NULL ");
                } else {
                    params.appendSQL("occpermit.dateissued IS NULL ");
                }
            }

            // *******************************
            // **    PASSED INSPECTIONS     **
            // *******************************
            if (params.isInspectionPassed_ctl()) {
                params.appendSQL("AND occinspection.passedinspectionts ");
                if (params.isInspectionPassed_val()) {
                    params.appendSQL("IS NOT NULL ");
                } else {
                    params.appendSQL("IS NULL ");
                }
            }

            // *******************************
            // **    THIRD PARTY            **
            // *******************************
            if (params.isThirdPartyInspector_ctl()) {
                params.appendSQL("AND occinspection.thirdpartyinspector_personid ");
                if (params.isThirdPartyInspector_registered_val()) {
                    params.appendSQL("IS NOT NULL ");
                } else {
                    params.appendSQL("IS NULL ");
                }

                params.appendSQL("AND occinspection.thirdpartyinspector_personid ");
                if (params.isThirdPartyInspector_approved_val()) {
                    params.appendSQL("IS NOT NULL ");
                } else {
                    params.appendSQL("IS NULL ");
                }
            }

           // *******************************
            // **          7:PACC          **
            // *******************************
            if (params.isPacc_ctl()) {
                if(params.isPacc_val()){
                    params.appendSQL("AND paccenabled = TRUE ");
                } else {
                    params.appendSQL("AND paccenabled = FALSE ");
                }
            }
             
            // *******************************
            // **       8:PERSON            **
            // *******************************
            if (params.isPerson_ctl()) {
                if(params.getPerson_val()!= null){
                    params.appendSQL("AND occperiodperson.person_personid=? ");
                } else {
                    params.setPropertyUnit_ctl(false);
                    params.appendToParamLog("PERSON: no Person object found; person filter disabled");
                }
            }
             
        } else {
            params.appendSQL("AND occperiod.periodid=? "); // will be param 1 with ID search
        }
        
        params.appendSQL(";");

        int paramCounter = 0;

        try {
            stmt = con.prepareStatement(params.extractRawSQL());

            if (!params.isBobID_ctl()) {
                if(params.isEventwindow_ctl()){
                    stmt.setInt(++paramCounter, params.getEventwindow_daysintopast());
                }
                
                if (params.isMuni_ctl()) {
                     stmt.setInt(++paramCounter, params.getMuni_val().getMuniCode());
                }
                
                if(params.isDate_startEnd_ctl()){
                    stmt.setTimestamp(++paramCounter, params.getDateStart_val_sql());
                    stmt.setTimestamp(++paramCounter, params.getDateEnd_val_sql());
                 }
                
                if (params.isUser_ctl()) {
                   stmt.setInt(++paramCounter, params.getUser_val().getUserID());
                }
                
                if (params.isProperty_ctl()) {
                    stmt.setInt(++paramCounter, params.getProperty_val().getParcelKey());
                }

                if (params.isPropertyUnit_ctl()) {
                    stmt.setInt(++paramCounter, params.getPropertyUnit_val().getUnitID());
                }
                // filter OCC-8
                if (params.isPerson_ctl()) {
                    stmt.setInt(++paramCounter, params.getPerson_val().getHumanID());
                }

            } else {
                stmt.setInt(++paramCounter, params.getBobID_val());
            }

            rs = stmt.executeQuery();

            int counter = 0;
            int maxResults;
            if (params.isLimitResultCount_ctl()) {
                maxResults = params.getLimitResultCount_val();
            } else {
                maxResults = Integer.MAX_VALUE;
            }
            while (rs.next() && counter < maxResults) {
                periodList.add(rs.getInt("periodid"));
                counter++;
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            

        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return periodList;
    }
    
    /**
     * Cache-enabled getter for occ periods!
     * @param periodid
     * @return
     * @throws IntegrationException
     * @throws EventException
     * @throws AuthorizationException
     * @throws BObStatusException
     * @throws ViolationException 
     */
    public OccPeriod getOccPeriod(int periodid) throws IntegrationException, 
                                                                EventException, 
                                                                AuthorizationException, 
                                                                BObStatusException, 
                                                                ViolationException {
        if(periodid == 0){
            throw new IntegrationException("cannot get occ period with ID == 0");
        } 
//        if(getOccupancyCacheManager().isCachingEnabled()){
        // ECD thinks this was permanently disabled due to runaawy ran during caching testing
        if(false){
            return getOccupancyCacheManager().getCacheOccPeriod().get(periodid, k -> fetchOccPeriod(periodid));
        } else {
            return fetchOccPeriod(periodid);
        }
    }

    /**
     * Primary extraction point for an occupancy period by ID
     * @param periodid
     * @return
     
     */
    public OccPeriod fetchOccPeriod(int periodid) throws DatabaseFetchRuntimeException {
        OccPeriod op = null;
        OccupancyCoordinator oc = getOccupancyCoordinator();
        String query = """
                        SELECT  periodid, occperiod.source_sourceid, parcelunit.unitnumber, 
                                parcelunit.parcel_parcelkey, parcel.parcelidcnty, parcelunit_unitid, 
                                occperiod.createdts, startdate, startdatecertifiedby_userid, 
                                startdatecertifiedts, enddate, enddatecertifiedby_userid, 
                                enddatecterifiedts, manager_userid, authorizationts, authorizedby_userid, 
                                overrideperiodtypeconfig, occperiod.notes, periodtype_typeid, 
                                occperiod.createdby_userid, occperiod.lastupdatedby_userid, occperiod.lastupdatedts, 
                                occperiod.deactivatedts, occperiod.deactivatedby_userid 
                        FROM public.occperiod JOIN public.parcelunit ON (occperiod.parcelunit_unitid = parcelunit.unitid) 
                       JOIN public.parcel ON (parcel.parcelkey = parcelunit.parcel_parcelkey) WHERE periodid=?;
                       """;

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, periodid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                op = generateOccPeriod(rs);
            }
        } catch (SQLException | BObStatusException | IntegrationException | AbstractMethodError ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("Unable to build occ period");
        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return op;
    }
    
    
    /**
     * Returns the IDs of occ permit types that map to a given occ period type
     * @param opt
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public List<Integer> getOccPermitTypesByOccPeriodType(OccPeriodType opt) throws IntegrationException{
        
        List<Integer> idList = new ArrayList<>();
        if(opt == null){
            throw new IntegrationException("Cannot get occ permit types by period with null period type");
        }
        
        String query = """
                            SELECT permittype_typeid
                            	FROM public.occperiodtypepermittype WHERE periodtype_typeid = ?
                       """;

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, opt.getOccPeriodTypeID());
            rs = stmt.executeQuery();
            while (rs.next()) {
                idList.add(rs.getInt("permittype_typeid"));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to occ permit type by period type", ex);
        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return idList;
    }
    
    /**
     * Cached-bached getter
     * @param typeID
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public OccPeriodType getOccPeriodType(int typeID) throws IntegrationException, BObStatusException{
        if(typeID == 0){
            throw new BObStatusException("Cannot get occ period type with type == 0");
        }
        
//        if(getOccupancyCacheManager().isCachingEnabled()){
        if(false){
            return getOccupancyCacheManager().getCacheOccPeriodType().get(typeID, k -> fetchOccPeriodType(typeID));
        } else {
            return fetchOccPeriodType(typeID);
        }
        
    }
    
    /**
     * getter for occ period types from the DB
     * @param typeID
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public OccPeriodType fetchOccPeriodType(int typeID) throws DatabaseFetchRuntimeException{
        
        OccPeriodType opt = null;
        
        String query = """
                            SELECT  typeid, title, description, 
                                    taskchain_chainid, muni_municode, active, 
                                    reinspectionwindowdays, defaultoriginationeventcat_categoryid, defaultchecklist
                                     FROM public.occperiodtype WHERE typeid=?;
                       """;

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, typeID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                opt = generateOccPeriodType(rs);
            }
        } catch (SQLException | BObStatusException | IntegrationException ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("Unable to build occ period type", ex);
        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return opt;
    }
    
    /**
     * Populates an occ period type from a result set
     * 
     * @param rs
     * @return 
     */
    private OccPeriodType generateOccPeriodType(ResultSet rs) throws SQLException, IntegrationException, BObStatusException {
        MunicipalityCoordinator mc = getMuniCoordinator();
        TaskCoordinator tc = getTaskCoordinator();
        EventCoordinator ec = getEventCoordinator();
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        
        OccPeriodType opt = new OccPeriodType();
        
        opt.setOccPeriodTypeID(rs.getInt("typeid"));
        opt.setTitle(rs.getString("title"));
        opt.setDescription(rs.getString("description"));
        opt.setMuni(mc.getMuni(rs.getInt("muni_municode")));
        opt.setActive(rs.getBoolean("active"));
        if(rs.getInt("taskchain_chainid") != 0){
            opt.setChain(tc.getTaskChain(rs.getInt("taskchain_chainid")));
        }
        opt.setReinspectionWindowDays(rs.getInt("reinspectionwindowdays"));
        
        if(rs.getInt("defaultoriginationeventcat_categoryid") != 0){
            opt.setDefaultOriginationEventCategory(ec.getEventCategory(rs.getInt("defaultoriginationeventcat_categoryid")));
        }
        if(rs.getInt("defaultchecklist") != 0){
            opt.setDefaultInspectionChecklist(oic.getChecklistTemplate(rs.getInt("defaultchecklist")));
        }
        
        return opt;
        
    }
    
    /**
     * Insertion point for occ period types
     * @param opt
     * @return
     * @throws IntegrationException 
     */
    public int insertOccPeriodType(OccPeriodType opt) throws IntegrationException{
        if(opt == null || opt.getOccPeriodTypeID() != 0){
            throw new IntegrationException("Cannot insert null OPT or type with non-null ID");
        }
        
        String query = """
                       INSERT INTO public.occperiodtype(
                           	typeid, title, description, 
                                taskchain_chainid, muni_municode, active, 
                                reinspectionwindowdays, defaultoriginationeventcat_categoryid, defaultchecklist)
                           	VALUES (DEFAULT, ?, ?, 
                                        ?, ?, TRUE, 
                                        ?, ?, ?);
                       """; 
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement stmt = null;
        int freshID = 0;

        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            
            stmt.setString(1, opt.getTitle());
            stmt.setString(2, opt.getDescription());
            if(opt.getChain() != null){
                stmt.setInt(3, opt.getChain().getChainID());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            if(opt.getMuni() != null){
                stmt.setInt(4, opt.getMuni().getMuniCode());  
            }else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            stmt.setInt(5, opt.getReinspectionWindowDays());
            
            if(opt.getDefaultOriginationEventCategory() != null){
                stmt.setInt(6, opt.getDefaultOriginationEventCategory().getCategoryID());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            
            if(opt.getDefaultInspectionChecklist() != null){
                stmt.setInt(7, opt.getDefaultInspectionChecklist().getInspectionChecklistID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            stmt.execute();

            String lastIDNumSQL = "SELECT currval('occperiodtypeid_seq'::regclass)";
            stmt = con.prepareStatement(lastIDNumSQL);
            rs = stmt.executeQuery();
            while (rs.next()) {
                freshID = rs.getInt("currval");
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("OccupancyIntegrator.insertOccPeriodType"
                    + "| IntegrationError: unable to insert occupancy period type", ex);
        } finally {
           releasePostgresConnection(con, stmt, rs);
        }
        return freshID;
        
    }
    
    /**
     * Writes a linking record in the occperiodtypepermittype table
     * @param opt
     * @throws IntegrationException 
     */
    public void connectOccPeriodTypeToPermitTypes(OccPeriodType opt) throws IntegrationException{
        if(opt == null || opt.getOccPeriodTypeID() == 0 || opt.getPermitTypes() == null){
            throw new IntegrationException("Cannot insert null OPT or type with non-null ID or null permit type list");
        }
        
        String query = """
                       INSERT INTO public.occperiodtypepermittype(
                                	periodtype_typeid, permittype_typeid)
                                	VALUES (?, ?);
                       """; 
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement stmt = null;

        
        try {
            con = getPostgresCon();
            
            for(OccPermitType permitType: opt.getPermitTypes()){
                
                stmt = con.prepareStatement(query);
                stmt.setInt(1, opt.getOccPeriodTypeID());
                stmt.setInt(2, permitType.getTypeID());
                
                stmt.execute();
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("OccupancyIntegrator.connectOccPeriodTypeToPermitType"
                    + "| IntegrationError: unable to insert connection", ex);
        } finally {
           releasePostgresConnection(con, stmt, rs);
        }
        
    }
    
    /**
     * Actually deletes records in the occperiodtypepermittype table
     * @param opt
     * @throws IntegrationException 
     */
    public void clearOccPeriodTypeToOccPermitTypeLinks(OccPeriodType opt) throws IntegrationException{
        if(opt == null || opt.getOccPeriodTypeID() == 0){
            throw new IntegrationException("Cannot clear connections with occ period type with null input or ID of 0");
        }
        String query = """
                        DELETE FROM occperiodtypepermittype WHERE periodtype_typeid=?;
                       """; 
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement stmt = null;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, opt.getOccPeriodTypeID());
                        
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("OccupancyIntegrator.clearOccPeriodTypeToOccPermitTypeLinks"
                    + "| IntegrationError: unable to delete occ period type to permit type links", ex);
        } finally {
           releasePostgresConnection(con, stmt, rs);
        }
        
    }
    
    
    /**
     * Updates a record in the occperiodtype table
     * @param opt
     * @throws IntegrationException 
     */
    public void updateOccPeriodType(OccPeriodType opt) throws IntegrationException{
        if(opt == null || opt.getOccPeriodTypeID() == 0){
            throw new IntegrationException("Cannot update occ period type with null input or ID of 0");
        }
        String query = """
                        UPDATE public.occperiodtype
                        	SET title=?, description=?, taskchain_chainid=?, 
                                    muni_municode=?, active=?, reinspectionwindowdays=?,
                                    defaultoriginationeventcat_categoryid=?, defaultchecklist=?
                        	WHERE typeid=?;
                       """; 
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement stmt = null;
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            
            stmt.setString(1, opt.getTitle());
            stmt.setString(2, opt.getDescription());
            if(opt.getChain() != null){
                stmt.setInt(3, opt.getChain().getChainID());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            if(opt.getMuni() != null){
                stmt.setInt(4, opt.getMuni().getMuniCode());  
            }else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            stmt.setBoolean(5, opt.isActive());
            stmt.setInt(6, opt.getReinspectionWindowDays());
            
            if(opt.getDefaultOriginationEventCategory() != null){
                stmt.setInt(7, opt.getDefaultOriginationEventCategory().getCategoryID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            
            if(opt.getDefaultInspectionChecklist() != null){
                stmt.setInt(8, opt.getDefaultInspectionChecklist().getInspectionChecklistID());
            } else {
                stmt.setNull(8, java.sql.Types.NULL);
            }
            
            stmt.setInt(9, opt.getOccPeriodTypeID());
                        
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("OccupancyIntegrator.updateOccPeriodType"
                    + "| IntegrationError: unable to update occupancy period type", ex);
        } finally {
           releasePostgresConnection(con, stmt, rs);
        }
    
        
    }
    
    
    /**
     * Extracts all occ period types from the DB
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public List<OccPeriodType> getOccPeriodTypeListComplete() throws IntegrationException{
       List<OccPeriodType> optl = new ArrayList<>();
       OccupancyCoordinator oc = getOccupancyCoordinator();
        
        String query = """
                            SELECT typeid FROM public.occperiodtype;
                       """;

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            rs = stmt.executeQuery();
            while (rs.next()) {
                optl.add(oc.getOccPeriodType(rs.getInt("typeid")));
            }
        } catch (SQLException | BObStatusException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to build occ period type list", ex);
        } finally {
             releasePostgresConnection(con, stmt, rs);
        } 
        return optl;
    }

    /**
     * Generator for occupancy period objects
     * @param rs
     * @return
     * @throws SQLException
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    private OccPeriod generateOccPeriod(ResultSet rs) throws SQLException, IntegrationException, BObStatusException {
        SystemIntegrator si = getSystemIntegrator();
        UserCoordinator uc = getUserCoordinator();
        OccupancyCoordinator oc = getOccupancyCoordinator();

        OccPeriod op = new OccPeriod();

        op.setPeriodID(rs.getInt("periodid"));

        if(rs.getInt("source_sourceid") != 0){
            op.setSource(si.getBOBSource(rs.getInt("source_sourceid")));
        }
        
        if(rs.getInt("periodtype_typeid") != 0){
            op.setPeriodType(oc.getOccPeriodType(rs.getInt("periodtype_typeid")));
        }
        

        if(rs.getTimestamp("startdate") != null){
            op.setStartDate(rs.getTimestamp("startdate").toLocalDateTime());
        }
            
        op.setStartDateCertifiedBy(uc.user_getUser(rs.getInt("startdatecertifiedby_userid")));
        if(rs.getTimestamp("startdatecertifiedts") != null){
            op.setStartDateCertifiedTS(rs.getTimestamp("startdatecertifiedts").toLocalDateTime());
        }

        if(rs.getTimestamp("enddate") != null){
            op.setEndDate(rs.getTimestamp("enddate").toLocalDateTime());
        }
        op.setEndDateCertifiedBy(uc.user_getUser(rs.getInt("enddatecertifiedby_userid")));
        if(rs.getTimestamp("enddatecterifiedts") != null){
            op.setEndDateCertifiedTS(rs.getTimestamp("enddatecterifiedts").toLocalDateTime());
        }

        op.setManager(uc.user_getUser(rs.getInt("manager_userid")));

        if(rs.getTimestamp("authorizationts") != null){
            op.setAuthorizedTS(rs.getTimestamp("authorizationts").toLocalDateTime());
        }
        op.setAuthorizedBy(uc.user_getUser(rs.getInt("authorizedby_userid")));

        op.setOverrideTypeConfig(rs.getBoolean("overrideperiodtypeconfig"));
        op.setNotes(rs.getString("notes"));
        
        // flattened fields
        op.setPropertyUnitID(rs.getInt("parcelunit_unitid"));
        op.setParentParcelKey(rs.getInt("parcel_parcelkey"));
        op.setPropertyUnitNumber(rs.getString("unitnumber"));
        op.setParentParcelIDCounty(rs.getString("parcelidcnty"));
        si.populateTrackedFields(op, rs, false);

        return op;
    }
    
    /**
     * Grabs all occ period IDs from the loginobjecthistory table
     * @param userID
     * @return
     * @throws IntegrationException 
     */
 public List<Integer> getOccPeriodHistoryList(int userID) throws IntegrationException {
        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Integer> al = new ArrayList<>();

        try {
            String s = "SELECT occperiod_periodid, entrytimestamp FROM loginobjecthistory "
                    + "WHERE login_userid = ? "
                    + "AND occperiod_periodid IS NOT NULL "
                    + "ORDER BY entrytimestamp DESC;";
            stmt = con.prepareStatement(s);
            stmt.setInt(1, userID);

            rs = stmt.executeQuery();
            while (rs.next()) {
                al.add(rs.getInt("occperiod_periodid"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("PersonIntegrator.getPerson | Unable to retrieve person", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 

        System.out.println("OccIntegrator Retrieved period of size: " + al.size());
        return al;
    }  
 
 
 // **********************************************************
 // ******************** OCCUPANCY PERMITS *******************
 // **********************************************************
 
    /**
     * The CULPRIT in the caching bug is THIS METHOD
     * @param permitID
     * @return
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws BlobException 
     */
    public OccPermit getOccPermit(int permitID) throws IntegrationException, BObStatusException, BlobException {
        if(permitID == 0){
            throw new IntegrationException("Cannot fetch occ permit with ID == 0");
        } 
        // COMMENTED to NOT CACHE occ permits in July 2024 during first caching layer
        // implementation due to a heap overrun traced to this cache only!
//        if(getOccupancyCacheManager().isCachingEnabled()){
        if(false){
            return getOccupancyCacheManager().getCacheOccPermit().get(permitID, k -> fetchOccPermit(permitID));
        } else {
            return fetchOccPermit(permitID);
        }
        
    }

 /**
  * Extracts a single occ period by ID
  * @param permitID
  * @return
  */
    public OccPermit fetchOccPermit(int permitID) throws DatabaseFetchRuntimeException {
        OccPermit op = null;
        String query = """
                        SELECT permitid, occperiod_periodid, referenceno, staticpermitadditionaltext, 
                              occpermit.notes, finalizedts, finalizedby_userid, statictitle, staticmuniaddress, 
                              staticpropertyinfo, staticownerseller, staticcolumnlink, staticbuyertenant, 
                              staticproposeduse, staticusecode, staticpropclass, staticdateofapplication, 
                              staticinitialinspection, staticreinspectiondate, staticfinalinspection, 
                              staticdateofissue, staticofficername, staticissuedundercodesourceid, 
                              staticstipulations, staticcomments, staticmanager, statictenants, 
                              staticleaseterm, staticleasestatus, staticpaymentstatus, staticnotice, 
                              occpermit.createdts, occpermit.createdby_userid, occpermit.lastupdatedts, occpermit.lastupdatedby_userid, 
                              occpermit.deactivatedts, occpermit.deactivatedby_userid, staticconstructiontype, nullifiedts,        
                              nullifiedby_userid, staticdateexpiry, permittype_typeid, staticsignature_photodocid,
                              staticownersellerlabel, staticbuyertenantlabel, staticheader_photodocid, staticheaderwidthpx, parcel_parcelkey,
                                dynamiclastsavedflowstep, dynamiclastsavedts, dynamicparcelinfo, 
                                dynamicownersellertype, dynamicownersellerlinks, dynamicbuyertenanttype, 
                                dynamicbuyersellercolumnlink, dynamicbuyertenantlinks, dynamicmanagerlinks, 
                                dynamictenantlinks, dynamicissuingofficer, dynamiccodesources, 
                                dynamicstipulationsfreeform, dynamicstipulationblocks, dynamicnoticesfreeform, 
                                dynamicnoticesblocks, dynamiccommentsfreeform, dynamiccommentsblocks, 
                                dynamicdateofapplication, dynamicinitialinspection, dynamicreinspectiondate, 
                                dynamicfinalinspection, dynamicdateofissue, dynamicdateexpiry,
                                dynamicinitialinspection_inspectionid, dynamicreinspectiondate_inspectionid, dynamicfinalinspection_inspectionid,
                                dynamicapplication_applictionid, amendmentinitts, amendmentcommitts, amendedby_umapid, staticdisplaymuniaddress,
                                revokedts, revokedby_umapid, revokedreason, 
                                staticdisplayperslinkleftcol, staticdisplayperslinkrightcol, 
                                staticdisplayperslinkmanagers, staticdisplayperslinktenants,
                                adminoverridets, adminoverride_umapid
                        FROM public.occpermit INNER JOIN public.occperiod ON (occpermit.occperiod_periodid = occperiod.periodid)
                                              INNER JOIN public.parcelunit ON (occperiod.parcelunit_unitid = parcelunit.unitid) 
                        WHERE permitid=?;
                       """;
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, permitID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                op = generateOccPermit(rs);
            }
        } catch (SQLException | BObStatusException | BlobException | IntegrationException ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("Unable to get occ permit due to integration error", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return op;

    }

    /**
     * Generator for occ permits
     * @param rs
     * @return
     * @throws SQLException
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    private OccPermit generateOccPermit(ResultSet rs) throws SQLException, IntegrationException, BObStatusException, BlobException {
        UserCoordinator uc = getUserCoordinator();
        OccPermit permit = new OccPermit();
        SystemIntegrator si = getSystemIntegrator();

        permit.setPermitID(rs.getInt("permitid"));
        permit.setPeriodID(rs.getInt("occperiod_periodid"));
        // flattened field to avoid cycles and reduce DB calls
        permit.setParentParcelKey(rs.getInt("parcel_parcelkey"));
        
        permit.setReferenceNo(rs.getString("referenceno"));
        
        permit.setPermitType(getOccPermitType(rs.getInt("permittype_typeid")));

        permit.setNotes(rs.getString("notes"));

        if(rs.getTimestamp("finalizedts") != null){
            permit.setFinalizedts(rs.getTimestamp("finalizedts").toLocalDateTime());
        }
        
        if(rs.getTimestamp("nullifiedts") != null){
            permit.setNullifiedTS(rs.getTimestamp("nullifiedts").toLocalDateTime());
        }
        if(rs.getInt("nullifiedby_userid") != 0){
            permit.setNullifiedBy(uc.user_getUser(rs.getInt("nullifiedby_userid")));
        }
        
        if(rs.getTimestamp("adminoverridets") != null){
            permit.setAdminOverrideTS(rs.getTimestamp("adminoverridets").toLocalDateTime());
        }
        if(rs.getInt("adminoverride_umapid") != 0){
            permit.setAdminOverrideUMAP(uc.auth_getUMAPNotValidated(rs.getInt("adminoverride_umapid")));
        }
        
        populateDynamicFields(permit, rs);
        populateStaticFields(permit, rs);
        
        si.populateTrackedFields(permit, rs, true);
        
        return permit;
    }
    
    /**
     * Injects in-process fields saved during the permit building process
     * but BEFORE finalization
     * @param permit
     * @param Rs
     * @return 
     */
    private OccPermit populateDynamicFields(OccPermit permit, ResultSet rs) throws SQLException, IntegrationException, BObStatusException, BlobException, IntegrationException{
        PropertyCoordinator pc = getPropertyCoordinator();
        UserCoordinator uc = getUserCoordinator();
        CodeCoordinator cc = getCodeCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        
        // dynamic population tracking
        permit.setDynamicLastStep(rs.getInt("dynamiclastsavedflowstep"));
        if(rs.getTimestamp("dynamiclastsavedts") != null){
            permit.setDynamicLastSaveTS(rs.getTimestamp("dynamiclastsavedts").toLocalDateTime());
        }
        
        // parent parcel data
        if(rs.getInt("dynamicparcelinfo") != 0){
            permit.setParcelInfo(pc.getParcelInfo(rs.getInt("dynamicparcelinfo")));
        }
        
        // ******************** PERSONS **************************
        // the mighty person lists
        // which right now we'll set to an int[] but our coordinator will objectify these IDs
        // since it will have a UA
        if(rs.getString("dynamicownersellertype") != null){
            permit.setOwnerSellerType(OccPermitPersonListEnum.valueOf(rs.getString("dynamicownersellertype")));
        }
        if(rs.getArray("dynamicownersellerlinks") != null){
            permit.setOwnerSellerLinkIDList((Integer[]) rs.getArray("dynamicownersellerlinks").getArray());
        }
        
        if(rs.getString("dynamicbuyertenanttype") != null){
            permit.setBuyerTenantType(OccPermitPersonListEnum.valueOf(rs.getString("dynamicbuyertenanttype")));
        }
        if(rs.getArray("dynamicbuyertenantlinks") != null){
            permit.setBuyerTenantLinkIDList((Integer[]) rs.getArray("dynamicbuyertenantlinks").getArray());
        }
        if(rs.getArray("dynamicmanagerlinks") != null){
            permit.setManagerLinkIDList((Integer[]) rs.getArray("dynamicmanagerlinks").getArray());
        }
        if(rs.getArray("dynamictenantlinks") != null){
            permit.setTenantLinkIDList((Integer[]) rs.getArray("dynamictenantlinks").getArray());
        }
        
        // governing code sources & officer
        if(rs.getInt("dynamicissuingofficer") != 0){
            permit.setIssuingOfficer(uc.user_getUser(rs.getInt("dynamicissuingofficer")));
        }
        if(rs.getArray("dynamiccodesources") != null){
            // OLD: List<Integer> sourceList = Arrays.asList(Arrays.stream((int[]) rs.getArray("dynamiccodesources").getArray()).boxed().toArray(Integer[]::new));
            Integer[] intArray = (Integer[]) rs.getArray("dynamiccodesources").getArray();
            List<Integer> sourceList = Arrays.stream(intArray)
                                  .collect(Collectors.toList());
            permit.setIssuingCodeSourceList(cc.getCodeSourceList(sourceList));
        }
        
        // Our three classifications for text on the permit
        permit.setText_stipulations(rs.getString("dynamicstipulationsfreeform"));
        if(rs.getArray("dynamicstipulationblocks") != null){
            Integer[] intArray = (Integer[]) rs.getArray("dynamicstipulationblocks").getArray();
            List<Integer> stipBlockIDList = Arrays.stream(intArray)
                                  .collect(Collectors.toList());
            permit.setTextBlocks_stipulations(sc.getTextBlocksByIDList(stipBlockIDList));
        }
        
        permit.setText_notices(rs.getString("dynamicnoticesfreeform"));
        if(rs.getArray("dynamicnoticesblocks") != null){
            Integer[] intArray = (Integer[]) rs.getArray("dynamicnoticesblocks").getArray();
            List<Integer> noticeBlockIDList = Arrays.stream(intArray)
                                  .collect(Collectors.toList());
            permit.setTextBlocks_notice(sc.getTextBlocksByIDList(noticeBlockIDList));
        }
        
        permit.setText_comments(rs.getString("dynamiccommentsfreeform"));
        if(rs.getArray("dynamiccommentsblocks") != null){
            Integer[] intArray = (Integer[]) rs.getArray("dynamiccommentsblocks").getArray();
            List<Integer> commentBlockIDList = Arrays.stream(intArray)
                                  .collect(Collectors.toList());
            permit.setTextBlocks_comments(sc.getTextBlocksByIDList(commentBlockIDList));
        }
        
        // now for dates
        if(rs.getTimestamp("dynamicdateofapplication") != null){
            permit.setDynamicDateOfApplication(LocalDate.from(rs.getTimestamp("dynamicdateofapplication").toLocalDateTime()));
        }
        // activate when applications are up and going
        permit.setDynamicsDateOfApplicationAppRef(null);
        
        if(rs.getTimestamp("dynamicinitialinspection") != null){
            permit.setDynamicInitialInspection(LocalDate.from(rs.getTimestamp("dynamicinitialinspection").toLocalDateTime()));
            
        }
        if(rs.getInt("dynamicinitialinspection_inspectionid") != 0){
            permit.setDynamicFinalInspectionFINRef(oic.getOccInspectionLight(rs.getInt("dynamicinitialinspection_inspectionid")));
        }
        
        if(rs.getTimestamp("dynamicreinspectiondate") != null){
            permit.setDynamicreinspectiondate(LocalDate.from(rs.getTimestamp("dynamicreinspectiondate").toLocalDateTime()));
            
        }
        if(rs.getInt("dynamicreinspectiondate_inspectionid") != 0){
            permit.setDynamicReInspectionFINRef(oic.getOccInspectionLight(rs.getInt("dynamicreinspectiondate_inspectionid")));
        }
        
        if(rs.getTimestamp("dynamicfinalinspection") != null){
            permit.setDynamicfinalinspection(LocalDate.from(rs.getTimestamp("dynamicfinalinspection").toLocalDateTime()));
        }
        if(rs.getInt("dynamicfinalinspection_inspectionid") != 0){
            permit.setDynamicFinalInspectionFINRef(oic.getOccInspectionLight(rs.getInt("dynamicfinalinspection_inspectionid")));
        }
        
        if(rs.getTimestamp("dynamicdateofissue") != null){
            permit.setDynamicdateofissue(LocalDate.from(rs.getTimestamp("dynamicdateofissue").toLocalDateTime()));
        }
        if(rs.getTimestamp("dynamicdateexpiry") != null){
            permit.setDynamicDateExpiry(LocalDate.from(rs.getTimestamp("dynamicdateexpiry").toLocalDateTime()));
        }
        
        return permit;
    }
    
    
    /**
     * Injects fields that are "final" 
     * @param permit
     * @param Rs
     * @return 
     */
    private OccPermit populateStaticFields(OccPermit permit, ResultSet rs) throws SQLException, IntegrationException, BObStatusException{
        UserCoordinator uc = getUserCoordinator();
        
        permit.setFinalizedBy(uc.user_getUser(rs.getInt("finalizedby_userid")));
       
        permit.setStatictitle(rs.getString("statictitle"));
        permit.setStaticmuniaddress(rs.getString("staticmuniaddress"));
        permit.setStaticdisplaymuniaddress(rs.getBoolean("staticdisplaymuniaddress"));
        
        permit.setStaticpropertyinfo(rs.getString("staticpropertyinfo"));
        
        permit.setStaticdisplayperslinkleftcol(rs.getBoolean("staticdisplayperslinkleftcol"));
        permit.setStaticownersellerlabel(rs.getString("staticownersellerlabel"));
        permit.setStaticownerseller(rs.getString("staticownerseller"));
        
        permit.setStaticcolumnlink(rs.getString("staticcolumnlink"));
        
        permit.setStaticdisplayperslinkrightcol(rs.getBoolean("staticdisplayperslinkrightcol"));
        permit.setStaticbuyertenantlabel(rs.getString("staticbuyertenantlabel"));
        permit.setStaticbuyertenant(rs.getString("staticbuyertenant"));
        
        permit.setStaticproposeduse(rs.getString("staticproposeduse"));
        permit.setStaticusecode(rs.getString("staticusecode"));
        permit.setStaticpropclass(rs.getString("staticpropclass"));
        if(rs.getTimestamp("staticdateofapplication") != null){
            permit.setStaticdateofapplication(LocalDate.from(rs.getTimestamp("staticdateofapplication").toLocalDateTime()));
        }
        if(rs.getTimestamp("staticinitialinspection") != null){
            permit.setStaticinitialinspection(LocalDate.from(rs.getTimestamp("staticinitialinspection").toLocalDateTime()));
        }
        if(rs.getTimestamp("staticreinspectiondate") != null){
            permit.setStaticreinspectiondate(LocalDate.from(rs.getTimestamp("staticreinspectiondate").toLocalDateTime()));
        }
        if(rs.getTimestamp("staticfinalinspection") != null){
            permit.setStaticfinalinspection(LocalDate.from(rs.getTimestamp("staticfinalinspection").toLocalDateTime()));
        }
        if(rs.getTimestamp("staticdateofissue") != null){
            permit.setStaticdateofissue(LocalDate.from(rs.getTimestamp("staticdateofissue").toLocalDateTime()));
        }
        if(rs.getTimestamp("staticdateexpiry") != null){
            permit.setStaticdateofexpiry(LocalDate.from(rs.getTimestamp("staticdateexpiry").toLocalDateTime()));
        }
        
        permit.setStaticofficername(rs.getString("staticofficername"));
        permit.setStaticissuedundercodesourceid(rs.getString("staticissuedundercodesourceid"));
        permit.setStaticstipulations(rs.getString("staticstipulations"));
        permit.setStaticcomments(rs.getString("staticcomments"));
        
        // Managers and tenants across sale
        permit.setStaticdisplayperslinkmanagers(rs.getBoolean("staticdisplayperslinkmanagers"));
        permit.setStaticmanager(rs.getString("staticmanager"));
        permit.setStaticdisplayperslinktenants(rs.getBoolean("staticdisplayperslinktenants"));
        permit.setStatictenants(rs.getString("statictenants"));
        
        permit.setStaticleaseterm(rs.getString("staticleaseterm"));
        permit.setStaticleasestatus(rs.getString("staticleasestatus"));
        permit.setStaticpaymentstatus(rs.getString("staticpaymentstatus"));
        permit.setStaticnotice(rs.getString("staticnotice"));
        permit.setStaticconstructiontype(rs.getString("staticconstructiontype"));
        
        
        permit.setStaticOfficerSignaturePhotoDocID(rs.getInt("staticsignature_photodocid"));
        permit.setStaticheaderphotodocid(rs.getInt("staticheader_photodocid"));
        permit.setStaticheaderWidthPx(rs.getInt("staticheaderwidthpx"));
        
        // Amendment
        if(rs.getTimestamp("amendmentinitts") != null){
            permit.setAmendementInitTS(rs.getTimestamp("amendmentinitts").toLocalDateTime());
        }
        if(rs.getTimestamp("amendmentcommitts") != null){
            permit.setAmendementCommitTS(rs.getTimestamp("amendmentcommitts").toLocalDateTime());
        }
        if(rs.getInt("amendedby_umapid") != 0){
            permit.setAmendmendedByUmap(uc.auth_getUMAPNotValidated(rs.getInt("amendedby_umapid")));
        }
        // I guess ECD wanted to also have an amending user and not just the UMAP?
        if(permit.getAmendmendedByUmap() != null){
            permit.setAmendedByUser(uc.user_getUser(permit.getAmendmendedByUmap().getUserID()));
        }
        
        // Revocation
        if(rs.getTimestamp("revokedts") != null){
            permit.setRevokedTS(rs.getTimestamp("revokedts").toLocalDateTime());
        }
        if(rs.getInt("revokedby_umapid") != 0){
            permit.setRevokedByUMAP(uc.auth_getUMAPNotValidated(rs.getInt("revokedby_umapid")));
        }
        permit.setRevokedReason(rs.getString("revokedreason"));
        if(permit.getRevokedByUMAP() != null){
            permit.setRevokedByUser(uc.user_getUser(permit.getRevokedByUMAP().getUserID()));
        }
        
        
        return permit;
        
    }
    

    /**
     * Extracts permits by occ period 
     * @param period
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public List<Integer> getOccPermitIDList(OccPeriod period) throws IntegrationException, BObStatusException {
        List<Integer> permitIDList = new ArrayList<>();
        String query = "SELECT permitid FROM public.occpermit WHERE occperiod_periodid=? AND deactivatedts IS NULL;";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, period.getPeriodID());
            rs = stmt.executeQuery();
            while (rs.next()) {
                permitIDList.add(rs.getInt("permitid"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to build occ permit ID list", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return permitIDList;
    }

    /**
     * Entry point for OccPermit objects into the database. 
     * Only called by coordinator. BUT remember, 
     *  you can only write to the metadata fields here. Actual permit static fields
     *  are written specially through the occPermitPopulateStaticFieldsFromDynamicFields
     * @param permit
     * @return ID of the skeleton. 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public int insertOccPermit(OccPermit permit) throws BObStatusException, IntegrationException{
        if(permit == null){
            throw new BObStatusException("Cannot insert null occ permit");
        }
        
        String query = """
                       INSERT INTO public.occpermit(
                                   permitid, occperiod_periodid, referenceno,  notes, createdts, createdby_userid, lastupdatedts, lastupdatedby_userid, permittype_typeid)
                           VALUES (DEFAULT, ?, NULL, ?, now(), ?, now(), ?, ?);
                       """; 
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement stmt = null;
        int freshPermitID = 0;

        PaymentCoordinator pc = getPaymentCoordinator();
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, permit.getPeriodID());
            stmt.setString(2, permit.getNotes());
            if(permit.getCreatedBy() != null){
                stmt.setInt(3, permit.getCreatedBy().getUserID());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            // on creation, the last updator is the creator
            if(permit.getCreatedBy() != null){
                stmt.setInt(4, permit.getCreatedBy().getUserID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            
            if(permit.getPermitType() != null){
                stmt.setInt(5, permit.getPermitType().getTypeID());
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            
            stmt.execute();

            String lastIDNumSQL = "SELECT currval('occupancypermit_permitid_seq'::regclass)";
            stmt = con.prepareStatement(lastIDNumSQL);
            rs = stmt.executeQuery();
            while (rs.next()) {
                freshPermitID = rs.getInt("currval");
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("OccupancyIntegrator.insertOccPermit"
                    + "| IntegrationError: unable to insert occupancy permit", ex);
        } finally {
           releasePostgresConnection(con, stmt, rs);
        }
        return freshPermitID;
    }
    
    
    /**
     * Asks DB for the count of finalized permits by muni
     * @param muni
     * @return 
     */
    public int countFinalizedPermitsByMuni(Municipality muni) throws BObStatusException, IntegrationException{
        if(muni == null){
            throw new BObStatusException("Cannot get permit count by muni with null muni");
        }
        
        String query = "SELECT count(permitid) AS pcount\n" +
                        "FROM occpermit INNER JOIN occperiod ON (occperiod.periodid = occpermit.occperiod_periodid)\n" +
                        "INNER JOIN parcelunit ON (occperiod.parcelunit_unitid = parcelunit.unitid)\n" +
                        "INNER JOIN parcel ON (parcelunit.parcel_parcelkey = parcel.parcelkey)\n" +
                        "WHERE parcel.muni_municode=? AND finalizedts IS NOT NULL;"; 
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement stmt = null;
        int finalizedPermitCount = 0;

        PaymentCoordinator pc = getPaymentCoordinator();
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, muni.getMuniCode());
            
            rs = stmt.executeQuery();

            while (rs.next()) {
                finalizedPermitCount = rs.getInt("pcount");
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("OccupancyIntegrator.countFinalizePermitsByMuni");
        } finally {
           releasePostgresConnection(con, stmt, rs);
        }
        return finalizedPermitCount;
        
        
        
        
    }
    
    /**
     * Updates an occ permit's metadata, including notes and deactivationts and decativatedby_userid
     * @param permit
     * @throws IntegrationException 
     */
    public void updateOccPermit(OccPermit permit) throws IntegrationException {
        String query = """
                       UPDATE public.occpermit
                          SET referenceno=?,  
                              notes=?, finalizedts=?, finalizedby_userid=?, lastupdatedts=now(), 
                              lastupdatedby_userid=?, deactivatedts=?, deactivatedby_userid=?, occperiod_periodid=?, permittype_typeid=?
                        WHERE permitid=?;""";

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            if(permit.getReferenceNo() != null){
                stmt.setString(1, permit.getReferenceNo());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            stmt.setString(2, permit.getNotes());
            
            if(permit.getFinalizedts() != null){
                stmt.setTimestamp(3, java.sql.Timestamp.valueOf(permit.getFinalizedts()));
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            if(permit.getFinalizedBy() != null){
                stmt.setInt(4, permit.getFinalizedBy().getUserID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            if(permit.getLastUpdatedBy() != null){
                stmt.setInt(5, permit.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            
            if(permit.getDeactivatedTS() != null){
                stmt.setTimestamp(6, java.sql.Timestamp.valueOf(permit.getDeactivatedTS()));
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            
            if(permit.getDeactivatedBy() != null){
                stmt.setInt(7, permit.getDeactivatedBy().getUserID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
            
            stmt.setInt(8, permit.getPeriodID());
            
            if(permit.getPermitType() != null){
                stmt.setInt(9, permit.getPermitType().getTypeID());
            } else {
                stmt.setNull(9, java.sql.Types.NULL);
            }
            
            stmt.setInt(10, permit.getPermitID());
            
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("OccupancyIntegrator.updateOccpermit | Unable to update occupancy permit metadata", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        }
        
    }
    
    /**
     * Updates an occ permit's metadata, including notes and deactivationts and decativatedby_userid
     * @param permit
     * @throws IntegrationException 
     */
    public void updateOccPermitAmendmentInit(OccPermit permit) throws IntegrationException {
        String query = """
                       UPDATE public.occpermit
                          SET lastupdatedts=now(), lastupdatedby_userid=?, amendmentinitts=now(), amendedby_umapid=?
                        WHERE permitid=?;
                       """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            if(permit.getLastUpdatedBy() != null){
                stmt.setInt(1, permit.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            if(permit.getAmendmendedByUmap() != null){
                stmt.setInt(2, permit.getAmendmendedByUmap().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            
            stmt.setInt(3, permit.getPermitID());
            
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("OccupancyIntegrator.updateOccpermit | Unable to update occupancy permit metadata", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        }
        
        
    }
    
    /**
     * Updates an occ permit's metadata to TURN OFF amendment mode
     * @param permit
     * @throws IntegrationException 
     */
    public void updateOccPermitAmendmentAbort(OccPermit permit) throws IntegrationException {
        String query = """
                       UPDATE public.occpermit
                          SET lastupdatedts=now(), lastupdatedby_userid=?, amendmentinitts=NULL, amendedby_umapid=NULL
                        WHERE permitid=?;
                       """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            if(permit.getLastUpdatedBy() != null){
                stmt.setInt(1, permit.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            stmt.setInt(2, permit.getPermitID());
            
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("OccupancyIntegrator.updateOccpermit | Unable to quite amendment mode", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        }

    }
    
    /**
     * Updates a permit's static fields for AMENDMENT fields only, which are the person connections.
     * 
     * @param permit
     * @throws IntegrationException 
     */
    public void updateOccPermitAmendmentCommit(OccPermit permit) throws IntegrationException {
        String query = """
                       UPDATE public.occpermit
                          SET   lastupdatedts=now(), lastupdatedby_userid=?, amendmentcommitts=now(), 
                                amendedby_umapid=?, staticownerseller=?, staticbuyertenant=?, 
                                staticmanager=?, statictenants=?, notes=?, dynamiclastsavedts=now(), dynamiclastsavedflowstep=?
                        WHERE permitid=?;
                       """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            if(permit.getLastUpdatedBy() != null){
                stmt.setInt(1, permit.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            if(permit.getAmendmendedByUmap() != null){
                stmt.setInt(2, permit.getAmendmendedByUmap().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            
            stmt.setString(3, permit.getStaticownerseller());
            stmt.setString(4, permit.getStaticbuyertenant());
            stmt.setString(5, permit.getStaticmanager());
            stmt.setString(6, permit.getStatictenants());
            
            stmt.setString(7, permit.getNotes());
            stmt.setInt(8, permit.getDynamicLastStep());
            
            stmt.setInt(9, permit.getPermitID());
            
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("OccupancyIntegrator.updateOccpermit | Unable to update occupancy permit metadata", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        }

    }
    /**
     * Updates a permit's static fields for AMENDMENT fields only, which are the person connections.
     * 
     * @param permit
     * @throws IntegrationException 
     */
    public void updateOccPermitRevocation(OccPermit permit) throws IntegrationException {
        String query = """
                       UPDATE public.occpermit
                          SET   lastupdatedts=now(), lastupdatedby_userid=?, 
                                revokedts=now(), revokedby_umapid=?, revokedreason=?
                        WHERE permitid=?;
                       """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            if(permit.getLastUpdatedBy() != null){
                stmt.setInt(1, permit.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            if(permit.getRevokedByUMAP()!= null){
                stmt.setInt(2, permit.getRevokedByUMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            
            stmt.setString(3, permit.getRevokedReason());
            stmt.setInt(4, permit.getPermitID());
            
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("OccupancyIntegrator.updateOccpermit | Unable to update occupancy permit metadata", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        }
    }
    
    /**
     * Writes a timestamp and UMAP ID to a permit record to allow users to bypass any
     * OR writes nulls if the incoming permit has nulls for admin TS and admin UMAP
     * audit flags
     * @param permit
     * @throws IntegrationException 
     */
    public void updateOccPermitAdminOverride(OccPermit permit) throws IntegrationException {
        String query = """
                       UPDATE public.occpermit
                          SET   lastupdatedts=now(), lastupdatedby_userid=?, 
                                adminoverridets=?, adminoverride_umapid=?
                        WHERE permitid=?;
                       """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            if(permit.getLastUpdatedBy() != null){
                stmt.setInt(1, permit.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            if(permit.getAdminOverrideTS() != null){
                stmt.setTimestamp(2, java.sql.Timestamp.valueOf(permit.getAdminOverrideTS()));
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
                
            }
            if(permit.getAdminOverrideUMAP() != null){
                stmt.setInt(3, permit.getAdminOverrideUMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            stmt.setInt(4, permit.getPermitID());
            
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("OccupancyIntegrator.updateOccpermit | Unable to update occupancy permit metadata", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        }
    }
    
    /**
     * Updates an occ permit's dynamic fields which are used for incremental saving 
     * of data BEFORE finalization. To maintain amendment separation, this method
     * does NOT update dynamic person links. Call updateOccPermitDynamicFieldsPersonLinksOnly 
     * in this class for those fields
     * 
     * @param permit
     * @throws IntegrationException 
     */
    public void updateOccPermitDynamicFieldsExceptPersons(OccPermit permit) throws IntegrationException, BObStatusException {
        PersonCoordinator pc = getPersonCoordinator();
        CodeCoordinator cc = getCodeCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        
        String query = """
            UPDATE public.occpermit
            SET     lastupdatedts=now(), lastupdatedby_userid=?, permittype_typeid=?, 
                    dynamiclastsavedflowstep=?, dynamiclastsavedts=now(), dynamicparcelinfo=?, 
                    dynamicownersellertype=?, dynamicbuyertenanttype=?, 
                    dynamicbuyersellercolumnlink=?, 
                    dynamicissuingofficer=?, dynamiccodesources=?, 
                    dynamicstipulationsfreeform=?, dynamicstipulationblocks=?, dynamicnoticesfreeform=?, 
                    dynamicnoticesblocks=?, dynamiccommentsfreeform=?, dynamiccommentsblocks=?, 
                    dynamicdateofapplication=?, dynamicapplication_applictionid=?,
                    dynamicinitialinspection=?, dynamicinitialinspection_inspectionid=?, 
                    dynamicreinspectiondate=?, dynamicreinspectiondate_inspectionid=?, 
                    dynamicfinalinspection=?, dynamicfinalinspection_inspectionid=?,
                    dynamicdateofissue=?, dynamicdateexpiry=?
            WHERE permitid=?;""";

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            // tracking
            if(permit.getLastUpdatedBy() != null){
                stmt.setInt(1, permit.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            
            // must have a type, since this is step 1
            if(permit.getPermitType() != null){
                stmt.setInt(2, permit.getPermitType().getTypeID());
            } else {
                throw new BObStatusException("All permit drafts must have a non-null type");
            }
            // step tracking
            stmt.setInt(3, permit.getDynamicLastStep());
            
            // parcel info
            if(permit.getParcelInfo() != null){
                stmt.setInt(4, permit.getParcelInfo().getParcelInfoID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            
            // person links!! Gross
            if(permit.getOwnerSellerType() != null){
                stmt.setString(5, permit.getOwnerSellerType().name());
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            
            if(permit.getBuyerTenantType() != null){
                stmt.setString(6, permit.getBuyerTenantType().name());
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            
            if(permit.getDynamicPersonsColumnLink() != null){
                stmt.setString(7, permit.getDynamicPersonsColumnLink());
            }else {
                stmt.setNull(7, java.sql.Types.NULL);
            }
          
            // Authority 
            if(permit.getIssuingOfficer() != null){
                stmt.setInt(8, permit.getIssuingOfficer().getUserID());
            }else {
                stmt.setNull(8, java.sql.Types.NULL);
            }
            
            if(permit.getIssuingCodeSourceList() != null && !permit.getIssuingCodeSourceList().isEmpty()){
                stmt.setArray(9, con.createArrayOf("INTEGER", cc.getCodeSourceIDArrayFromSourceList(permit.getIssuingCodeSourceList())));
            }else {
                stmt.setNull(9, java.sql.Types.NULL);
            }
            
            // Text blocks
            stmt.setString(10, permit.getText_stipulations());
            if(permit.getTextBlocks_stipulations() != null && !permit.getTextBlocks_stipulations().isEmpty()){
                stmt.setArray(11, con.createArrayOf("INTEGER", sc.getTextBlockIntegerArrayFromBlocks(permit.getTextBlocks_stipulations())));
            }else {
                stmt.setNull(11, java.sql.Types.NULL);
            }
            
            stmt.setString(12, permit.getText_notices());
            if(permit.getTextBlocks_notice() != null && !permit.getTextBlocks_notice().isEmpty()){
                stmt.setArray(13, con.createArrayOf("INTEGER", sc.getTextBlockIntegerArrayFromBlocks(permit.getTextBlocks_notice())));
            }else {
                stmt.setNull(13, java.sql.Types.NULL);
            }
            
            stmt.setString(14, permit.getText_comments());
            if(permit.getTextBlocks_comments()!= null && !permit.getTextBlocks_comments().isEmpty()){
                stmt.setArray(15, con.createArrayOf("INTEGER", sc.getTextBlockIntegerArrayFromBlocks(permit.getTextBlocks_comments())));
            }else {
                stmt.setNull(15, java.sql.Types.NULL);
            }
            
            // dates
            if(permit.getDynamicDateOfApplication() != null){
                stmt.setTimestamp(16, java.sql.Timestamp.valueOf(permit.getDynamicDateOfApplication().atStartOfDay()));
            } else {
                stmt.setNull(16, java.sql.Types.NULL);
            }
            if(permit.getDynamicsDateOfApplicationAppRef() != null){
                stmt.setInt(17, permit.getDynamicsDateOfApplicationAppRef().getId());
            } else {
                stmt.setNull(17, java.sql.Types.NULL);
            }
            
            if(permit.getDynamicInitialInspection() != null){
                stmt.setTimestamp(18, java.sql.Timestamp.valueOf(permit.getDynamicInitialInspection().atStartOfDay()));
            } else {
                stmt.setNull(18, java.sql.Types.NULL);
            }
            if(permit.getDynamicInitialInspectionFINRef() != null){
                stmt.setInt(19, permit.getDynamicInitialInspectionFINRef().getInspectionID());
            } else {
                stmt.setNull(19, java.sql.Types.NULL);
            }
            if(permit.getDynamicreinspectiondate() != null){
                stmt.setTimestamp(20, java.sql.Timestamp.valueOf(permit.getDynamicreinspectiondate().atStartOfDay()));
            } else {
                stmt.setNull(20, java.sql.Types.NULL);
            }
            if(permit.getDynamicReInspectionFINRef() != null){
                stmt.setInt(21, permit.getDynamicReInspectionFINRef().getInspectionID());
            } else {
                stmt.setNull(21, java.sql.Types.NULL);
            }
            if(permit.getDynamicfinalinspection() != null){
                stmt.setTimestamp(22, java.sql.Timestamp.valueOf(permit.getDynamicfinalinspection().atStartOfDay()));
            } else {
                stmt.setNull(22, java.sql.Types.NULL);
            }
            if(permit.getDynamicFinalInspectionFINRef() != null){
                stmt.setInt(23, permit.getDynamicFinalInspectionFINRef().getInspectionID());
            } else {
                stmt.setNull(23, java.sql.Types.NULL);
            }
            if(permit.getDynamicdateofissue() != null){
                stmt.setTimestamp(24, java.sql.Timestamp.valueOf(permit.getDynamicdateofissue().atStartOfDay()));
            } else {
                stmt.setNull(24, java.sql.Types.NULL);
            }
            if(permit.getDynamicDateExpiry() != null){
                stmt.setTimestamp(25, java.sql.Timestamp.valueOf(permit.getDynamicDateExpiry().atStartOfDay()));
            } else {
                stmt.setNull(25, java.sql.Types.NULL);
            }
            
            stmt.setInt(26, permit.getPermitID());
            
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("OccupancyIntegrator.updateOccpermit | Unable to update occupancy permit metadata", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        }
       
    }
    
    /**
     * Updates an occ permit's dynamic person fields ONLY which are used for incremental saving 
     * of data BEFORE finalization
     * 
     * @param permit
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void updateOccPermitDynamicFieldsPersonLinksOnly(OccPermit permit) throws IntegrationException, BObStatusException {
        
        String query = """
            UPDATE public.occpermit
            SET     dynamicownersellerlinks=?, dynamicbuyertenantlinks=?, dynamicmanagerlinks=?, 
                    dynamictenantlinks=?
            WHERE permitid=?;""";

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
           if(permit.getOwnerSellerLinkList() != null && !permit.getOwnerSellerLinkList().isEmpty()){
                stmt.setArray(1, con.createArrayOf("INTEGER", permit.getOwnerSellerLinkIDList()));
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
          
            if(permit.getBuyerTenantLinkList() != null && !permit.getBuyerTenantLinkList().isEmpty()){
                stmt.setArray(2, con.createArrayOf("INTEGER", permit.getBuyerTenantLinkIDList()));
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
          
            if(permit.getManagerLinkList() != null && !permit.getManagerLinkList().isEmpty()){
                stmt.setArray(3, con.createArrayOf("INTEGER", permit.getManagerLinkIDList()));
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
          
            if(permit.getTenantLinkList() != null && !permit.getTenantLinkList().isEmpty()){
                stmt.setArray(4, con.createArrayOf("INTEGER", permit.getTenantLinkIDList()));
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }
            
            stmt.setInt(5, permit.getPermitID());
            
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("OccupancyIntegrator.updateOccpermitPersonLInks | Unable to update occupancy dymaic person links", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        }
     
    }
    
    
    
     
    /**
     * Nullifed an occ permit
     * @param permit
     * @throws IntegrationException 
     */
    public void nullifyOccupancyPermit(OccPermit permit) throws IntegrationException {
        String query = """
                       UPDATE public.occpermit
                          SET nullifiedts=now(), nullifiedby_userid=?, lastupdatedts=now(), 
                              lastupdatedby_userid=? 
                        WHERE permitid=?;
                       """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            if(permit.getNullifiedBy() != null){
                stmt.setInt(1, permit.getNullifiedBy().getUserID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            
            if(permit.getLastUpdatedBy() != null){
                stmt.setInt(2, permit.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            
            stmt.setInt(3, permit.getPermitID());
            
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("OccupancyIntegrator.updateOccpermit | Unable to update occupancy permit metadata", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        }
        
    }
    
    
    /**
     * I update the special permit static fields on an occ permit
     * @param permit     
     * @throws IntegrationException 
     */
    public void updateOccPermitStaticFields(OccPermit permit) throws IntegrationException {
        String query = """
                       UPDATE public.occpermit
                          SET statictitle=?, 
                              staticmuniaddress=?, staticpropertyinfo=?, staticownerseller=?, 
                              staticcolumnlink=?, staticbuyertenant=?, staticproposeduse=?, 
                              staticusecode=?, staticpropclass=?, staticdateofapplication=?, 
                              staticinitialinspection=?, staticreinspectiondate=?, staticfinalinspection=?, 
                              staticdateofissue=?, staticofficername=?, staticissuedundercodesourceid=?, 
                              staticstipulations=?, staticcomments=?, staticmanager=?, statictenants=?, 
                              staticleaseterm=?, staticleasestatus=?, staticpaymentstatus=?, 
                              staticnotice=?, lastupdatedts=now(), 
                              lastupdatedby_userid=?, staticconstructiontype=?, staticdateexpiry=?, staticsignature_photodocid=?,
                              staticownersellerlabel=?, staticbuyertenantlabel=?, staticheader_photodocid=?, staticheaderwidthpx=?,
                              staticdisplaymuniaddress=?, staticdisplayperslinkleftcol=?, staticdisplayperslinkrightcol=?, staticdisplayperslinkmanagers=?, staticdisplayperslinktenants=?
                       
                       WHERE permitid = ?;
                       """;

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            
            
            stmt.setString(1, permit.getStatictitle());
            
            stmt.setString(2, permit.getStaticmuniaddress());
            stmt.setString(3, permit.getStaticpropertyinfo());
            stmt.setString(4, permit.getStaticownerseller());
            
            stmt.setString(5, permit.getStaticcolumnlink());
            stmt.setString(6, permit.getStaticbuyertenant());
            stmt.setString(7, permit.getStaticproposeduse());
           
            stmt.setString(8, permit.getStaticusecode());
            stmt.setString(9, permit.getStaticpropclass());
            if(permit.getStaticdateofapplication() != null){
                stmt.setTimestamp(10, java.sql.Timestamp.valueOf(permit.getStaticdateofapplication().atStartOfDay()));
            } else {
                stmt.setNull(10, java.sql.Types.NULL);
            }
            
            if(permit.getStaticinitialinspection() != null){
                stmt.setTimestamp(11, java.sql.Timestamp.valueOf(permit.getStaticinitialinspection().atStartOfDay()));
            } else {
                stmt.setNull(11, java.sql.Types.NULL);
            }
            if(permit.getStaticreinspectiondate() != null){
                stmt.setTimestamp(12, java.sql.Timestamp.valueOf(permit.getStaticreinspectiondate().atStartOfDay()));
            } else {
                stmt.setNull(12, java.sql.Types.NULL);
            }
            if(permit.getStaticfinalinspection() != null){
                stmt.setTimestamp(13, java.sql.Timestamp.valueOf(permit.getStaticfinalinspection().atStartOfDay()));
            } else {
                stmt.setNull(13, java.sql.Types.NULL);
            }
            
            if(permit.getStaticdateofissue() != null){
                stmt.setTimestamp(14, java.sql.Timestamp.valueOf(permit.getStaticdateofissue().atStartOfDay()));
            } else {
                stmt.setNull(14, java.sql.Types.NULL);
            }
            stmt.setString(15, permit.getStaticofficername());
            stmt.setString(16, permit.getStaticissuedundercodesourceid());
            
            stmt.setString(17, permit.getStaticstipulations());
            stmt.setString(18, permit.getStaticcomments());
            stmt.setString(19, permit.getStaticmanager());
            stmt.setString(20, permit.getStatictenants());
            
            stmt.setString(21, permit.getStaticleaseterm());
            stmt.setString(22, permit.getStaticleasestatus());
            stmt.setString(23, permit.getStaticpaymentstatus());
            
            stmt.setString(24, permit.getStaticnotice());
            if(permit.getLastUpdatedBy() != null){
                stmt.setInt(25, permit.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(25, java.sql.Types.NULL);
            }
            stmt.setString(26, permit.getStaticconstructiontype());
            if(permit.getStaticdateofexpiry() != null){
                stmt.setTimestamp(27, java.sql.Timestamp.valueOf(permit.getStaticdateofexpiry().atStartOfDay()));
            } else {
                stmt.setNull(27, java.sql.Types.NULL);
            }
            if(permit.getStaticOfficerSignaturePhotoDocID() != 0){
                stmt.setInt(28, permit.getStaticOfficerSignaturePhotoDocID());
            } else {
                stmt.setNull(28, java.sql.Types.NULL);
            }
            if(permit.getStaticownersellerlabel() != null){
                stmt.setString(29, permit.getStaticownersellerlabel());
            } else {
                stmt.setNull(29, java.sql.Types.NULL);
            }
            if(permit.getStaticbuyertenantlabel() != null){
                stmt.setString(30, permit.getStaticbuyertenantlabel());
            } else {
                stmt.setNull(30, java.sql.Types.NULL);
            }
            if(permit.getStaticheaderphotodocid() != 0){
                stmt.setInt(31, permit.getStaticheaderphotodocid());
            } else {
                stmt.setNull(31, java.sql.Types.NULL);
            }
            if(permit.getStaticheaderWidthPx() != 0){
                stmt.setInt(32, permit.getStaticheaderWidthPx());
            } else {
                stmt.setNull(32, java.sql.Types.NULL);
            }
            
            stmt.setBoolean(33, permit.isStaticdisplaymuniaddress());
            
            stmt.setBoolean(34, permit.isStaticdisplayperslinkleftcol());
            stmt.setBoolean(35, permit.isStaticdisplayperslinkrightcol());
            stmt.setBoolean(36, permit.isStaticdisplayperslinkmanagers());
            stmt.setBoolean(37, permit.isStaticdisplayperslinktenants());
            
            stmt.setInt(38, permit.getPermitID());

            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("OccupancyIntegrator.updateOccpermitStaticFields | Unable to update occupancy permit static fields", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        }
      
    }

    //*****************************************************************************
    //*************  OCC PERIOD AND OCC PERMIT TYPE STUFF  ************************
    //*****************************************************************************
    
    /**
     * Updates a record in occ permit type
     * @param permitType
     * @throws IntegrationException 
     */
    public void updateOccPermitType(OccPermitType permitType) throws IntegrationException {
        String query = """
                UPDATE public.occpermittype
                   SET muni_municode=?, title=?, authorizeduses=?, description=?, 
                       permittable=?, requireinspectionpass=?, requireleaselink=?, 
                       allowthirdpartyinspection=?, commercial=?, defaultpermitvalidityperioddays=?, 
                       permittitle=?, expires=?, 
                       requiremanager=?, requiretenant=?, requirezerobalance=?,lastupdatedts=now(),lastupdatedby_umapid=?, 
                       personcolumnleftenum=?, personcolumnrightenum=?, personcolumseparator=?,
                       occupancyfamilypermit=?, 
                       personlinkleftcolactivate=?, personlinkleftcolrequire=?, personlinkleftcolcustomheader=?, 
                       personlinkrightcolactivate=?, personlinkrightcolrequire=?, personlinkrightcolcustomheader=?, 
                       personlinkmanageractivate=?, personlinktenantactivate=?, displaymuniaddress=?, 
                       personlinkmanagerrequire=?, personlinktenantrequire=?
                 WHERE typeid=?;""";

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, permitType.getMuni().getMuniCode());
            stmt.setString(2, permitType.getTitle());
            stmt.setString(3, permitType.getAuthorizeduses());
            stmt.setString(4, permitType.getDescription());

            stmt.setBoolean(5, permitType.isPermittable());
            stmt.setBoolean(6, permitType.isPassedInspectionRequired());

            stmt.setBoolean(7, permitType.isRequireLeaseLink());
            stmt.setBoolean(8, permitType.isAllowthirdpartyinspection());

            stmt.setBoolean(9, permitType.isCommercial());

            stmt.setInt(10, permitType.getDefaultValidityPeriodDays());
            stmt.setString(11, permitType.getPermitTitle());
            stmt.setBoolean(12, permitType.isExpires());
            stmt.setBoolean(13, permitType.isRequireManager());
            stmt.setBoolean(14, permitType.isRequireTenant());
            stmt.setBoolean(15, permitType.isRequireZeroBalance());
            if (permitType.getLastUpdatedby_UMAP() != null) {
                stmt.setInt(16, permitType.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(16, java.sql.Types.NULL);
            }
            
            if(permitType.getPersonColumnLeftLabel() != null){
                stmt.setString(17, permitType.getPersonColumnLeftLabel().name());
            } else {
                stmt.setNull(17, java.sql.Types.NULL);
            }
            if(permitType.getPersonColumnRightLabel() != null){
                stmt.setString(18, permitType.getPersonColumnRightLabel().name());
            } else {
                stmt.setNull(18, java.sql.Types.NULL);
            }
            stmt.setString(19, permitType.getPersonColumnLinkingText());
            
             stmt.setBoolean(20, permitType.isOccupancyFamilyPermit());
            
            stmt.setBoolean(21, permitType.isPersonColumnLeftActivate());
            stmt.setBoolean(22, permitType.isRequireLPersonLinkLeftCol());
            stmt.setString(23, permitType.getPersonColumnLeftCustomLabel());
            
            stmt.setBoolean(24, permitType.isPersonColumnRightActivate());
            stmt.setBoolean(25, permitType.isRequireLPersonLinkRightCol());
            stmt.setString(26, permitType.getPersonColumnRightCustomLabel());
            
            stmt.setBoolean(27, permitType.isPersonColumnManagerActivate());
            stmt.setBoolean(28, permitType.isPersonColumnTenantActivate());
            stmt.setBoolean(29, permitType.isPermitDisplayMuniAddress());
            
            stmt.setBoolean(30, permitType.isRequireManager());
            stmt.setBoolean(31, permitType.isRequireTenant());
            
            stmt.setInt(32, permitType.getTypeID());

            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update occupancy permit type", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        }
    }
    
    /**
     * Updates an occ permit type's default text blocks which are pre-populated 
     * when creating a permit
     * @param permitType
     * @throws IntegrationException 
     */
    public void updateOccPermitTypeDefaultTextBlocks(OccPermitType permitType) throws IntegrationException {
        SystemCoordinator sc = getSystemCoordinator();
        
        String query = """
                UPDATE public.occpermittype
                   SET defaulttextblocksstipulations_textblockid=?, 
                       defaulttextblocksnotices_textblockid=?, 
                       defaulttextblockscomments_textblockid=?
                 WHERE typeid=?;""";

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            if(permitType.getPreaddTextBlocksStipulations() != null && !permitType.getPreaddTextBlocksStipulations().isEmpty()){
                stmt.setArray(1, con.createArrayOf("INTEGER", sc.getTextBlockIntegerArrayFromBlocks(permitType.getPreaddTextBlocksStipulations())));
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            if(permitType.getPreaddTextBlocksNotices() != null && !permitType.getPreaddTextBlocksNotices().isEmpty()){
                stmt.setArray(2, con.createArrayOf("INTEGER", sc.getTextBlockIntegerArrayFromBlocks(permitType.getPreaddTextBlocksNotices()))); 
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }
            if(permitType.getPreaddTextBlocksComments()!= null && !permitType.getPreaddTextBlocksComments().isEmpty()){
                stmt.setArray(3, con.createArrayOf("INTEGER", sc.getTextBlockIntegerArrayFromBlocks(permitType.getPreaddTextBlocksComments())));
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            
            stmt.setInt(4, permitType.getTypeID());

            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update occupancy permit type", ex);
        } finally {
            releasePostgresConnection(con, stmt);
        }
     
        
    }


    /**
     * cached backed getter
     * @param typeid
     * @return
     * @throws IntegrationException 
     */
    public OccPermitType getOccPermitType(int typeid) throws IntegrationException {
        if(typeid == 0){
            throw new IntegrationException("Cannot get permit type with type id == 0");
        }
        if(getOccupancyCacheManager().isCachingEnabled()){
            return getOccupancyCacheManager().getCacheOccPermitType().get(typeid, k -> fetchOccPermitType(typeid));
        } else {
            return fetchOccPermitType(typeid);
            
        }
        
        
    }
    /**
     * Primary extraction point for occ period types
     * @param typeid
     * @return  null if typeid == 0
     */
    public OccPermitType fetchOccPermitType(int typeid) throws DatabaseFetchRuntimeException {
        
        OccPermitType tpe = null;
        String query = """
                       SELECT   typeid, muni_municode, title, authorizeduses, description, 
                                permittable, requireinspectionpass, requireleaselink,
                                allowthirdpartyinspection, commercial, defaultpermitvalidityperioddays, 
                                eventruleset_setid, permittitle, expires, requiremanager, 
                                requiretenant, requirezerobalance, createdts, createdby_umapid, 
                                deactivatedby_umapid, lastupdatedts, deactivatedts, lastupdatedby_umapid, 
                                personcolumnleftenum, personcolumnrightenum, personcolumseparator, 
                                defaulttextblocksstipulations_textblockid, defaulttextblocksnotices_textblockid, defaulttextblockscomments_textblockid,
                                occupancyfamilypermit, 
                                personlinkleftcolactivate, personlinkleftcolrequire, personlinkleftcolcustomheader, 
                                personlinkrightcolactivate, personlinkrightcolrequire, personlinkrightcolcustomheader, 
                                personlinkmanageractivate, personlinktenantactivate, displaymuniaddress,
                                personlinkmanagerrequire, personlinktenantrequire
                         FROM public.occpermittype WHERE typeid=?;
                       """;

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, typeid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                tpe = generateOccPermitType(rs);
            }
        } catch (SQLException | IntegrationException ex) {
            System.out.println(ex.toString());
            throw new DatabaseFetchRuntimeException("Unable to occ period type due to an DB integration error", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return tpe;
    }

    /**
     * Extracts occ permit types by muniprofile; This is a legacy method; Permit types are
     * specified by the occ period type, not a direct muni mapping retrieval
     * @param muniProfileID
     * @return
     * @throws IntegrationException 
     */
    public List<OccPermitType> getOccPermitTypeListByMuniProfileID(int muniProfileID) throws IntegrationException {
        List<OccPermitType> typeList = new ArrayList<>();
        String query = """
                            SELECT occperiodtype_typeid
                                   FROM public.muniprofileoccperiodtype WHERE muniprofile_profileid=?;
                       """;

        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, muniProfileID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                typeList.add(getOccPermitType(rs.getInt("occperiodtype_typeid")));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to build occupancy period type list", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        } 
        return typeList;
    }

    /**
     * Extracts all OccPeriodTypes in DB for configuration
     * @param requireActive
     * @return
     * @throws IntegrationException 
     */
    public List<OccPermitType> getOccPermitTypeList(boolean requireActive) throws IntegrationException {
        List<OccPermitType> occPeriodTypeList = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
       sb.append("SELECT typeid FROM public.occpermittype ");
        if (requireActive) {
            sb.append("WHERE deactivatedts is null");
        }
        sb.append(";");
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement(sb.toString());
            rs = stmt.executeQuery();
            while (rs.next()) {
                occPeriodTypeList.add(getOccPermitType(rs.getInt("typeid")));
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot get OccupancyPermitType list, sorry", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }
        return occPeriodTypeList;
    }
    
    /**
     * Extracts all OccPeriodTypes in DB for configuration
     * @param muni
     * @param requireActive
     * @return
     * @throws IntegrationException 
     */
    public List<OccPermitType> getOccPermitTypeListComplete(Municipality muni, boolean requireActive) throws IntegrationException {
        List<OccPermitType> occPeriodTypeList = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT typeid FROM public.occpermittype WHERE typeid IS NOT NULL ");
        if (Objects.nonNull(muni)) {
            sb.append("AND muni_municode=? ");
        }
        if (requireActive) {
            sb.append("AND deactivatedts is null");
        }

        sb.append(";");
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sb.toString());
            if (Objects.nonNull(muni)) {
                stmt.setInt(1, muni.getMuniCode());
            }
            rs = stmt.executeQuery();
            while (rs.next()) {
                occPeriodTypeList.add(getOccPermitType(rs.getInt("typeid")));
            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot get OccupancyPermitType list, sorry", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }
        return occPeriodTypeList;
    }

    /**
     * Writes a new record in the occ permit type table
     * @param permitType
     * @return id of newly inserted record
     * @throws IntegrationException 
     */
    public int insertOccPermitType(OccPermitType permitType) throws IntegrationException {
        String query = """ 
                       INSERT INTO public.occpermittype(
                                muni_municode, title, authorizeduses, description, permittable, 
                                requireinspectionpass, requireleaselink, allowthirdpartyinspection, commercial, 
                                defaultpermitvalidityperioddays, permittitle, expires, requiremanager, 
                                requiretenant, requirezerobalance, createdts, createdby_umapid, deactivatedby_umapid, 
                                lastupdatedts, deactivatedts, lastupdatedby_umapid, personcolumnleftenum, personcolumnrightenum, personcolumseparator,
                                occupancyfamilypermit, 
                                personlinkleftcolactivate, personlinkleftcolrequire, personlinkleftcolcustomheader, 
                                personlinkrightcolactivate, personlinkrightcolrequire, personlinkrightcolcustomheader, 
                                personlinkmanageractivate, personlinktenantactivate, displaymuniaddress,
                                personlinkmanagerrequire, personlinktenantrequire)
                       	VALUES (?, ?, ?, ?, ?, 
                                ?, ?, ?, ?, 
                                ?, ?, ?, ?, 
                                ?, ?, now(), ?, NULL, 
                                now(), NULL, ?, ?, ?, ?,
                                ?, 
                                ?, ?, ?,
                                ?, ?, ?,
                                ?, ?, ?,
                                ?, ?);
                       """;

        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int newFreshId = 0;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, permitType.getMuni().getMuniCode());
            stmt.setString(2, permitType.getTitle());
            stmt.setString(3, permitType.getAuthorizeduses());
            stmt.setString(4, permitType.getDescription());

            stmt.setBoolean(5, permitType.isPermittable());
            stmt.setBoolean(6, permitType.isPassedInspectionRequired());

            stmt.setBoolean(7, permitType.isRequireLeaseLink());
            stmt.setBoolean(8, permitType.isAllowthirdpartyinspection());
            
            stmt.setBoolean(9, permitType.isCommercial());

            stmt.setInt(10, permitType.getDefaultValidityPeriodDays());
            stmt.setString(11, permitType.getPermitTitle());
            
            stmt.setBoolean(12, permitType.isExpires());
            stmt.setBoolean(13, permitType.isRequireManager());
            stmt.setBoolean(14, permitType.isRequireTenant());
            stmt.setBoolean(15, permitType.isRequireZeroBalance());
            
            if (Objects.nonNull(permitType.getCreatedby_UMAP())) {
                stmt.setInt(16, permitType.getCreatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(16, java.sql.Types.NULL);
            }

            if (Objects.nonNull(permitType.getLastUpdatedby_UMAP())) {
                stmt.setInt(17, permitType.getLastUpdatedby_UMAP().getUserMuniAuthPeriodID());
            } else {
                stmt.setNull(17, java.sql.Types.NULL);
            }
            
            if(permitType.getPersonColumnLeftLabel() != null){
                stmt.setString(18, permitType.getPersonColumnLeftLabel().name());
            } else {
                stmt.setNull(18, java.sql.Types.NULL);
            }
            if(permitType.getPersonColumnRightLabel() != null){
                stmt.setString(19, permitType.getPersonColumnRightLabel().name());
            } else {
                stmt.setNull(19, java.sql.Types.NULL);
            }
            stmt.setString(20, permitType.getPersonColumnLinkingText());
            
            stmt.setBoolean(21, permitType.isOccupancyFamilyPermit());
            
            stmt.setBoolean(22, permitType.isPersonColumnLeftActivate());
            stmt.setBoolean(23, permitType.isRequireLPersonLinkLeftCol());
            stmt.setString(24, permitType.getPersonColumnLeftCustomLabel());
            
            stmt.setBoolean(25, permitType.isPersonColumnRightActivate());
            stmt.setBoolean(26, permitType.isRequireLPersonLinkRightCol());
            stmt.setString(27, permitType.getPersonColumnRightCustomLabel());
            
            stmt.setBoolean(28, permitType.isPersonColumnManagerActivate());
            stmt.setBoolean(29, permitType.isPersonColumnTenantActivate());
            stmt.setBoolean(30, permitType.isPermitDisplayMuniAddress());
            
            stmt.setBoolean(31, permitType.isRequireManager());
            stmt.setBoolean(32, permitType.isRequireTenant());
            
            stmt.execute();
            
            String retrievalQuery = "SELECT currval('occperiodtypeid_seq');";
            stmt = con.prepareStatement(retrievalQuery);
            rs = stmt.executeQuery();

            while (rs.next()) {
                newFreshId = rs.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Cannot insert OccupancyPermitType ", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }
        return newFreshId;
    }

    /**
     * Insertion point for occupancy period objects
     * @param period
     * @return
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public int insertOccPeriod(OccPeriod period) throws IntegrationException, BObStatusException {
        if(period == null || period.getCreatedBy() == null || period.getPropertyUnitID() == 0){
            throw new BObStatusException("cannot insert occ period with null period, type, or creator, or parcel unit with ID == 0");
        }
        
        String query = """
                        INSERT INTO public.occperiod(
                                   periodid, source_sourceid, parcelunit_unitid, createdts, 
                                   startdate, enddate, manager_userid, 
                                   notes, createdby_userid, lastupdatedby_userid, lastupdatedts, periodtype_typeid)
                           VALUES (DEFAULT, ?, ?, now(),  
                                   ?, ?, ?, ?, ?, ?, now(), ? );
                       """;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement stmt = null;
        int newPeriodId = 0;

        PaymentCoordinator pc = getPaymentCoordinator();
        
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);

            // PARAMS LINE 1
            if(period.getSource() != null){
                stmt.setInt(1, period.getSource().getSourceid());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            
            stmt.setInt(2, period.getPropertyUnitID());
            
            if (period.getStartDate() != null) {
                stmt.setTimestamp(3, java.sql.Timestamp.valueOf(period.getStartDate()));
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }

            if (period.getEndDate() != null) {
                stmt.setTimestamp(4, java.sql.Timestamp.valueOf(period.getEndDate()));
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }

            if (period.getManager() != null) {
                stmt.setInt(5, period.getManager().getUserID());
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }
            
            stmt.setString(6, period.getNotes());
            
            if (period.getCreatedBy() != null) {
                stmt.setInt(7, period.getCreatedBy().getUserID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }

            if (period.getLastUpdatedBy() != null) {
                stmt.setInt(8, period.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(8, java.sql.Types.NULL);
            }
            
            if(period.getPeriodType() != null){
                stmt.setInt(9, period.getPeriodType().getOccPeriodTypeID());
            } else {
                stmt.setNull(9, java.sql.Types.NULL);
            }

            stmt.execute();

            String lastIDNumSQL = "SELECT currval('occperiodid_seq'::regclass);";
            stmt = con.prepareStatement(lastIDNumSQL);
            rs = stmt.executeQuery();
            while (rs.next()) {
                newPeriodId = rs.getInt("currval");
            }
        } catch (SQLException ex) {
            throw new IntegrationException("OccupancyIntegrator.insertOccPeriod"
                    + "| IntegrationError: unable to insert occupancy period", ex);
        } finally {
           releasePostgresConnection(con, stmt, rs);
        }
        return newPeriodId;
    }
    
    

    /**
     * Updates an occ period 
     * @param period
     * @throws IntegrationException 
     */
    public void updateOccPeriod(OccPeriod period) throws IntegrationException {
        String query = """
                       UPDATE public.occperiod
                          SET source_sourceid=?, parcelunit_unitid=?,
                              startdate=?, 
                              startdatecertifiedby_userid=?, startdatecertifiedts=?, enddate=?, 
                              enddatecertifiedby_userid=?, enddatecterifiedts=?, manager_userid=?, 
                              authorizationts=?, authorizedby_userid=?, overrideperiodtypeconfig=?, 
                              notes=?, lastupdatedby_userid=?, lastupdatedts=now() 
                        WHERE periodid=?;
                       """;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement stmt = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);

            // PARAMS LINE 1
            if(period.getSource() != null){
                stmt.setInt(1, period.getSource().getSourceid());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
            stmt.setInt(2, period.getPropertyUnitID());
            // timestamp set to now()

            if (period.getStartDate() != null) {
                stmt.setTimestamp(3, java.sql.Timestamp.valueOf(period.getStartDate()));
            } else {
                stmt.setNull(3, java.sql.Types.NULL);
            }
            // PARAMS LINE 3
            if (period.getStartDateCertifiedBy() != null) {
                stmt.setInt(4, period.getStartDateCertifiedBy().getUserID());
            } else {
                stmt.setNull(4, java.sql.Types.NULL);
            }

            if (period.getStartDateCertifiedTS() != null) {
                stmt.setTimestamp(5, java.sql.Timestamp.valueOf(period.getStartDateCertifiedTS()));
            } else {
                stmt.setNull(5, java.sql.Types.NULL);
            }

            if (period.getEndDate() != null) {
                stmt.setTimestamp(6, java.sql.Timestamp.valueOf(period.getEndDate()));
            } else {
                stmt.setNull(6, java.sql.Types.NULL);
            }
            if (period.getEndDateCertifiedBy() != null) {
                stmt.setInt(7, period.getEndDateCertifiedBy().getUserID());
            } else {
                stmt.setNull(7, java.sql.Types.NULL);
            }

            if (period.getEndDateCertifiedTS() != null) {
                stmt.setTimestamp(8, java.sql.Timestamp.valueOf(period.getEndDateCertifiedTS()));
            } else {
                stmt.setNull(8, java.sql.Types.NULL);
            }

            // PARAMS LINE 4
            if (period.getManager() != null) {
                stmt.setInt(9, period.getManager().getUserID());
            } else {
                stmt.setNull(9, java.sql.Types.NULL);
            }
            if (period.getAuthorizedTS() != null) {
                stmt.setTimestamp(10, java.sql.Timestamp.valueOf(period.getAuthorizedTS()));
            } else {
                stmt.setNull(10, java.sql.Types.NULL);
            }
            if (period.getAuthorizedBy() != null) {
                stmt.setInt(11, period.getAuthorizedBy().getUserID());
            } else {
                stmt.setNull(11, java.sql.Types.NULL);
            }
            stmt.setBoolean(12, period.isOverrideTypeConfig());

            // PARAMS LINE 5
            stmt.setString(13, period.getNotes());
           

            if(period.getLastUpdatedBy() != null){
                stmt.setInt(14, period.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(14, java.sql.Types.NULL);
            }

            stmt.setInt(15, period.getPeriodID());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Integration error: Unable to update occ period. This is a fatal error that should be reported.", ex);
        } finally {
             releasePostgresConnection(con, stmt, rs);
        }
        
        

    }
    /**
     * Updates an occ period 
     * @param period
     * @throws IntegrationException 
     */
    public void updateGivenOccPeriodsType(OccPeriod period) throws IntegrationException {
        if(period == null || period.getPeriodID() == 0){
            throw new IntegrationException("cannot update null period or period with ID==0");
        }
        
        String query = """
                       UPDATE public.occperiod
                          SET periodtype_typeid=?
                        WHERE periodid=?;
                       """;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement stmt = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            if(period.getPeriodType() != null){
                stmt.setInt(1, period.getPeriodType().getOccPeriodTypeID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }
         
            stmt.setInt(2, period.getPeriodID());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Integration error: Unable to update occ period. This is a fatal error that should be reported.", ex);
        } finally {
             releasePostgresConnection(con, stmt, rs);
        }

        
    }
    /**
     * Updates an occ period 
     * @param period
     * @throws IntegrationException 
     */
    public void updateOccPeriodManager(OccPeriod period) throws IntegrationException {
        if(period == null || period.getManager() == null){
            throw new IntegrationException("cannot update null period or period with ID==0");
        }
        
        String query = """
                       UPDATE public.occperiod
                          SET manager_userid=?
                        WHERE periodid=?;
                       """;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement stmt = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, period.getManager().getUserID());
         
            stmt.setInt(2, period.getPeriodID());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Integration error: Unable to update occ period. This is a fatal error that should be reported.", ex);
        } finally {
             releasePostgresConnection(con, stmt, rs);
        }
        
    }
    /**
     * Deacs an occ period 
     * @param period
     * @throws IntegrationException 
     */
    public void deactivateOccPeriod(OccPeriod period) throws IntegrationException {
        String query = """
                       UPDATE public.occperiod
                          SET  deactivatedts=now(), deactivatedby_userid=?, lastupdatedby_userid=?, lastupdatedts=now() 
                        WHERE periodid=?;
                       """;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement stmt = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);

            if(period.getDeactivatedBy() != null){
                stmt.setInt(1, period.getDeactivatedBy().getUserID());
            } else {
                stmt.setNull(1, java.sql.Types.NULL);
            }

            if(period.getLastUpdatedBy() != null){
                stmt.setInt(2, period.getLastUpdatedBy().getUserID());
            } else {
                stmt.setNull(2, java.sql.Types.NULL);
            }

            stmt.setInt(3, period.getPeriodID());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("Integration error: Unable to deac occ period. This is a fatal error that should be reported.", ex);
        } finally {
             releasePostgresConnection(con, stmt, rs);
        }
        
    }
 
    /**
     * Generator for occ period types
     * @param rs
     * @return
     * @throws IntegrationException 
     */
    private OccPermitType generateOccPermitType(ResultSet rs) throws IntegrationException {
        OccPermitType opt = new OccPermitType();
        MunicipalityCoordinator mc = getMuniCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        SystemIntegrator si = getSystemIntegrator();
        
        try {
            opt.setTypeID(rs.getInt("typeid"));
            opt.setMuni(mc.getMuni(rs.getInt("muni_municode")));
            opt.setTitle(rs.getString("title"));
            opt.setAuthorizeduses(rs.getString("authorizeduses"));
            opt.setDescription(rs.getString("description"));
            
            
            opt.setPermittable(rs.getBoolean("permittable"));
            opt.setPassedInspectionRequired(rs.getBoolean("requireinspectionpass"));
            
            opt.setRequireLeaseLink(rs.getBoolean("requireleaselink"));
            opt.setAllowthirdpartyinspection(rs.getBoolean("allowthirdpartyinspection"));
            
            opt.setOccupancyFamilyPermit(rs.getBoolean("occupancyfamilypermit"));
            opt.setCommercial(rs.getBoolean("commercial"));
            
            opt.setDefaultValidityPeriodDays(rs.getInt("defaultpermitvalidityperioddays"));
            
            opt.setBaseRuleSetID(rs.getInt("eventruleset_setid"));
            opt.setPermitTitle(rs.getString("permittitle"));
            opt.setExpires(rs.getBoolean("expires"));
            
            // LEFT PERSON COLUMN
            opt.setPersonColumnLeftActivate(rs.getBoolean("personlinkleftcolactivate"));
            opt.setRequireLPersonLinkLeftCol(rs.getBoolean("personlinkleftcolrequire"));
            if(rs.getString("personcolumnleftenum") != null){
                opt.setPersonColumnLeftLabel(OccPermitPersonListEnum.valueOf(rs.getString("personcolumnleftenum")));
            } 
            opt.setPersonColumnLeftCustomLabel(rs.getString("personlinkleftcolcustomheader"));
            
            // COL CONNECTOR
            opt.setPersonColumnLinkingText(rs.getString("personcolumseparator"));
            
            // RIGHT PERSON COLUMN
            opt.setPersonColumnRightActivate(rs.getBoolean("personlinkrightcolactivate"));
            opt.setRequireLPersonLinkRightCol(rs.getBoolean("personlinkrightcolrequire"));
            if(rs.getString("personcolumnrightenum") != null){
                opt.setPersonColumnRightLabel(OccPermitPersonListEnum.valueOf(rs.getString("personcolumnrightenum")));
            } 
            opt.setPersonColumnRightCustomLabel(rs.getString("personlinkrightcolcustomheader"));
           
            // MANAGER PERSONS
            opt.setPersonColumnManagerActivate(rs.getBoolean("personlinkmanageractivate"));
            opt.setRequireManager(rs.getBoolean("personlinkmanagerrequire"));
            
            // TENANTS COLUMN
            opt.setPersonColumnTenantActivate(rs.getBoolean("personlinktenantactivate"));
            opt.setRequireTenant(rs.getBoolean("personlinktenantrequire"));
            
            // OTHER STUFF
            opt.setRequireZeroBalance(rs.getBoolean("requirezerobalance"));
            opt.setPermitDisplayMuniAddress(rs.getBoolean("displaymuniaddress"));
            
            // DEFAULT TEXT BLOCKS
            if(rs.getArray("defaulttextblocksstipulations_textblockid") != null){
                Integer[] intArray = (Integer[]) rs.getArray("defaulttextblocksstipulations_textblockid").getArray();
                List<Integer> commentBlockIDList = Arrays.stream(intArray)
                                      .collect(Collectors.toList());
                opt.setPreaddTextBlocksStipulations(sc.getTextBlocksByIDList(commentBlockIDList));
            }
            if(rs.getArray("defaulttextblocksnotices_textblockid") != null){
                Integer[] intArray = (Integer[]) rs.getArray("defaulttextblocksnotices_textblockid").getArray();
                List<Integer> commentBlockIDList = Arrays.stream(intArray)
                                      .collect(Collectors.toList());
                opt.setPreaddTextBlocksNotices(sc.getTextBlocksByIDList(commentBlockIDList));
            }
            if(rs.getArray("defaulttextblockscomments_textblockid") != null){
                Integer[] intArray = (Integer[]) rs.getArray("defaulttextblockscomments_textblockid").getArray();
                List<Integer> commentBlockIDList = Arrays.stream(intArray)
                                      .collect(Collectors.toList());
                opt.setPreaddTextBlocksComments(sc.getTextBlocksByIDList(commentBlockIDList));
            }

            si.populateUMAPTrackedFields(opt, rs, true);

            // ** Deac during CHARGES UPGRADE
//            opt.setPermittedFees(pi.getFeeList(opt));

        } catch (SQLException | BObStatusException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Error generating OccPermitType from ResultSet", ex);
        }

        return opt;
    }

    /**
     * Insertion point for occ period applications
     * @param application
     * @return
     * @throws IntegrationException 
     */
    public int insertOccPermitApplication(OccPermitApplication application) throws IntegrationException {
        String query = "INSERT INTO public.occpermitapplication(applicationid,  "
                + "reason_reasonid, submissiontimestamp, "
                + "submitternotes, internalnotes, propertyunitid, "
                + "declaredtotaladults, declaredtotalyouth, rentalintent, "
                + "occperiod_periodid, externalnotes, status, "
                + "applicationpubliccc, paccenabled, allowuplinkaccess) "
                + "VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?::occapplicationstatus, ?, ?, ?) "
                + "RETURNING applicationid;";

        Connection con = null;
        PreparedStatement stmt = null;
        int applicationId;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, application.getReason().getId());
            stmt.setTimestamp(2, java.sql.Timestamp.valueOf(application.getSubmissionDate()));
            stmt.setString(3, application.getSubmissionNotes());
            stmt.setString(4, application.getInternalNotes());
            stmt.setString(5, String.valueOf(application.getApplicationPropertyUnit().getUnitID()));
            
            int adults = 0;
            int youth = 0;
                    
                    for(Human h : application.getAttachedPersons()){
                        
                        if(h.isUnder18()){
                            youth++;
                        } else {
                            adults++;
                        }
                        
                    }
            
            stmt.setInt(6, adults);
            stmt.setInt(7, youth);
            stmt.setBoolean(8, false);
            stmt.setInt(9, application.getConnectedPeriod().getPeriodID());
            stmt.setString(10, application.getExternalPublicNotes());
            stmt.setString(11, OccApplicationStatusEnum.Waiting.name());
            stmt.setInt(12, application.getPublicControlCode());
            
            application.setPaccEnabled(true); //We want to make sure they default to visible
            
            stmt.setBoolean(13, application.isPaccEnabled());
            stmt.setBoolean(14, application.isUplinkAccess());
                    
            stmt.execute();
            ResultSet inserted_application = stmt.getResultSet();
            inserted_application.next();
            applicationId = inserted_application.getInt(1);

        } catch (SQLException ex) {
            System.out.println("OccupancyIntegrator.insertOccPermitApplication() | ERROR:" + ex);
            throw new IntegrationException("OccupancyIntegrator.insertOccPermitApplication"
                    + "| IntegrationError: unable to insert occupancy permit application ", ex);
        } finally {
            releasePostgresConnection(con, stmt);
            
        }

        return applicationId;
    }

    /**
     * Extracts all occ permit application reasons
     * @return
     * @throws IntegrationException 
     */
    public List<OccPermitApplicationReason> getOccPermitApplicationReasons() throws IntegrationException {

        OccPermitApplicationReason reason = null;
        List<OccPermitApplicationReason> reasons = new ArrayList<>();
        String query = "SELECT reasonid, reasontitle, reasondescription, activereason, humanfriendlydescription, periodtypeproposal_periodid "
                + "FROM public.occpermitapplicationreason "
                + "WHERE activereason = 'true';";

        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();
            while (rs.next()) {
                reason = generateOccPermitApplicationReason(rs);
                reasons.add(reason);
            }

        } catch (SQLException ex) {
            System.out.println("OccupancyIntegrator.getOccPermitApplicationReasons() | ERROR: " + ex);
            throw new IntegrationException("OccupancyInspectionIntegrator.getOccPermitApplicationReasons "
                    + "| IntegrationException: Unable to get occupancy permit application reasons ", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }
        return reasons;
    }

    /**
     * Extracts a single occ permit application by ID
     * @param applicationID
     * @return
     * @throws IntegrationException
     * @throws EventException
     * @throws AuthorizationException
     * @throws BObStatusException
     * @throws ViolationException
     */
    public OccPermitApplication getOccPermitApplication(int applicationID) throws IntegrationException, EventException, AuthorizationException, BObStatusException, ViolationException, ViolationException {
        OccPermitApplication occpermitapp = null;
        String query = "   SELECT applicationid, reason_reasonid, submissiontimestamp, \n"
                + "       submitternotes, internalnotes, propertyunitid, declaredtotaladults, \n"
                + "       declaredtotalyouth, occperiod_periodid, rentalintent, status, externalnotes,\n"
                + "       applicationpubliccc, paccenabled, allowuplinkaccess\n"
                + "  FROM public.occpermitapplication WHERE applicationid=?;";

        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, applicationID);
            rs = stmt.executeQuery();

            while (rs.next()) {
                occpermitapp = generateOccPermitApplication(rs);
            }
        } catch (SQLException ex) {
            throw new IntegrationException("OccupancyInspectionIntegrator.getOccPermitApplication | "
                    + "IntegrationException: Unable to retrieve occupancy permit application ", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }
        return occpermitapp;
    }

    
    /**
     * Generator for occ permit applications
     * @param rs
     * @return
     * @throws IntegrationException
     * @throws EventException
     * @throws AuthorizationException
     * @throws BObStatusException
     * @throws ViolationException 
     */
    private OccPermitApplication generateOccPermitApplication(ResultSet rs) throws IntegrationException, EventException, AuthorizationException, BObStatusException, ViolationException {
        OccPermitApplication occpermitapp = new OccPermitApplication();
        PersonIntegrator pi = getPersonIntegrator();
        PropertyIntegrator propint = getPropertyIntegrator();
        try {
            occpermitapp.setId(rs.getInt("applicationid"));
            occpermitapp.setReason(getOccPermitApplicationReason(rs.getInt("reason_reasonid")));
            occpermitapp.setSubmissionDate(rs.getTimestamp("submissiontimestamp").toLocalDateTime());
            occpermitapp.setSubmissionNotes(rs.getString("submitternotes"));
            occpermitapp.setInternalNotes(rs.getString("internalNotes"));
            // use coordinator!
            //occpermitapp.setApplicationPropertyUnit(propint.getPropertyUnit(Integer.parseInt(rs.getString("propertyunitid"))));
            occpermitapp.setConnectedPeriod(getOccPeriod(rs.getInt("occperiod_periodid")));
            occpermitapp.setStatus(OccApplicationStatusEnum.valueOf(rs.getString("status")));
            occpermitapp.setExternalPublicNotes(rs.getString("externalnotes"));
            occpermitapp.setPublicControlCode(rs.getInt("applicationpubliccc"));
            occpermitapp.setPaccEnabled(rs.getBoolean("paccenabled"));
            occpermitapp.setUplinkAccess(rs.getBoolean("allowuplinkaccess"));
            
            if(occpermitapp.getConnectedPeriod() != null)
            {
            
                occpermitapp.setAttachedPersons(new ArrayList<>());
                
                for (HumanLink skeleton : pi.getPersonOccApplicationList(occpermitapp)) {

    //  ----->  TODO: Update for Humanization/Parcelization <------
//                    if (skeleton.isApplicant()) {
//                        occpermitapp.setApplicantPerson(skeleton);
//                    }
//
//                    if(skeleton.isPreferredContact()){
//                        occpermitapp.setPreferredContact(skeleton);
//                    }

                    occpermitapp.getAttachedPersons().add(skeleton);

                }
            }
            
        } catch (SQLException ex) {
            System.out.println("OccupancyIntegrator.generateOccPermitApplication() | ERROR: " + ex);
            throw new IntegrationException("OccupancyInspectionIntegrator.generateOccPermitApplication | "
                    + "IntegrationException: Unable to generate occupancy permit application ", ex);
        }
        return occpermitapp;
    }

    /**
     * Gets all OccPermitApplications. Use with care.
     * @return
     * @throws IntegrationException
     * @throws EventException
     * @throws AuthorizationException
     * @throws BObStatusException
     * @throws ViolationException 
     */
    public List<OccPermitApplication> getOccPermitApplicationList() throws IntegrationException, EventException, AuthorizationException, BObStatusException, ViolationException {
        List<OccPermitApplication> occpermitappList = new ArrayList<>();
        String query = "SELECT applicationid \n"
                + "  FROM public.occpermitapplication;";

        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();

            while (rs.next()) {
                occpermitappList.add(getOccPermitApplication(rs.getInt("applicationid")));
            }
        } catch (SQLException ex) {
            throw new IntegrationException("OccupancyIntegrator.getOccPermitApplicationList", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }
        return occpermitappList;
    }
    
    /**
     * Gets all occ permit applications by period
     * @param op
     * @return
     * @throws IntegrationException
     * @throws EventException
     * @throws AuthorizationException
     * @throws BObStatusException
     * @throws ViolationException 
     */
    public List<OccPermitApplication> getOccPermitApplicationList(OccPeriod op) throws IntegrationException, EventException, AuthorizationException, BObStatusException, ViolationException {
        List<OccPermitApplication> occpermitappList = new ArrayList<>();
        String query = "SELECT occpermitapp_applicationid\n"
                + "  FROM public.occperiodpermitapplication WHERE occperiod_periodid = ?;   ";

        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, op.getPeriodID());
            rs = stmt.executeQuery();

            while (rs.next()) {
                occpermitappList.add(getOccPermitApplication(rs.getInt("occpermitapp_applicationid")));
            }
        } catch (SQLException ex) {
            throw new IntegrationException("OccupancyIntegrator.getOccPermitApplicationList", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }
        return occpermitappList;
    }

    /**
     * Extracts an occ period application by integer public control code
     * @param pacc
     * @return
     * @throws IntegrationException
     * @throws EventException
     * @throws AuthorizationException
     * @throws BObStatusException
     * @throws ViolationException 
     */
    public List<OccPermitApplication> getOccPermitApplicationListByControlCode(int pacc) throws IntegrationException, EventException, AuthorizationException, BObStatusException, ViolationException {
        List<OccPermitApplication> occpermitappList = new ArrayList<>();
        String query = "SELECT applicationid\n"
                + "  FROM public.occpermitapplication WHERE applicationpubliccc = ?;   ";

        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, pacc);
            rs = stmt.executeQuery();

            while (rs.next()) {
                occpermitappList.add(getOccPermitApplication(rs.getInt("applicationid")));
            }
        } catch (SQLException ex) {
            throw new IntegrationException("OccupancyIntegrator.getOccPermitApplicationListByControlCode() | ERROR: " + ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }
        return occpermitappList;
    }
    
    /**
     * Changes PACC access by permit
     * @param permit
     * @throws IntegrationException 
     */
    public void updatePACCAccess(OccPermitApplication permit) throws IntegrationException {

        String q = "UPDATE occpermitapplication SET paccenabled = ? WHERE applicationid = ?;";

        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(q);
            stmt.setBoolean(1, permit.isPaccEnabled());
            stmt.setInt(2, permit.getId());
            // Retrieve action data from postgres
            stmt.executeUpdate();

            
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new IntegrationException("OccupancyIntegrator.updatePACCAccess | Integration Error: Unable to update OccPermitApplication", ex);
        } finally {
            
            releasePostgresConnection(con, stmt);
        } 

    }
    
    /**
     * Attaches a message to the OccPermitApplication inside the PublicInfoBundle, 
     * uses PACC to find application
     * @param application
     * @param message
     * @throws IntegrationException 
     */
    public void attachMessageToOccPermitApplication(PublicInfoBundleOccPermitApplication application, String message) throws IntegrationException {
        String query = "UPDATE public.occpermitapplication\n"
                + "SET externalnotes=? WHERE occpermitapplication.applicationpubliccc = ?;";

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setString(1, message);
            stmt.setInt(2, application.getPacc());
            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("OccupancyIntegrator.attachMessageToOccPermitApplication() | ERROR: " + ex);
            throw new IntegrationException("OccupancyInspectionIntegrator.attachMessageToOccPermitApplication"
                    + " | IntegrationException: Unable to attach message to occupancy permit application ", ex);
        } finally {
            releasePostgresConnection(con, stmt);
            
        }

    }
    
    /**
     * Updates a record in the occpermitapplication table
     * @param application
     * @throws IntegrationException 
     */
    public void updateOccPermitApplication(OccPermitApplication application) throws IntegrationException {
        String query = "UPDATE public.occpermitapplication "
                + "SET reason_reasonid=?, submissiontimestamp=?, "
                + "submitternotes=?, internalnotes=?, propertyunitid=?, externalnotes=?, status=?::occapplicationstatus, "
                + "applicationpubliccc=?, paccenabled=?, allowuplinkaccess=? "
                + "WHERE occpermitapplication.applicationid = ?;";

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, application.getReason().getId());
            stmt.setTimestamp(2, java.sql.Timestamp.valueOf(application.getSubmissionDate()));
            stmt.setString(3, application.getSubmissionNotes());
            stmt.setString(4, application.getInternalNotes());
            stmt.setString(5, String.valueOf(application.getApplicationPropertyUnit().getUnitID()));
            stmt.setString(6, application.getExternalPublicNotes());
            stmt.setString(7, application.getStatus().name());
            stmt.setInt(8, application.getPublicControlCode());
            stmt.setBoolean(9, application.isPaccEnabled());
            stmt.setBoolean(10, application.isUplinkAccess());
            stmt.setInt(11, application.getId());
            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("OccupancyIntegrator.updateOccPermitApplication() | ERROR: " + ex);
            throw new IntegrationException("OccupancyInspectionIntegrator.updateOccPermitApplication"
                    + " | IntegrationException: Unable to update occupancy permit application ", ex);
        } finally {
            releasePostgresConnection(con, stmt);
            
        }

    }

    /**
     * Doesn't do anything yet.
     * @param application 
     */
    public void deleteOccPermitApplication(OccPermitApplication application) {
    }

    /**
     * Extracts a single occ permit application reason by ID
     * @param reasonId
     * @return
     * @throws IntegrationException 
     */
    public OccPermitApplicationReason getOccPermitApplicationReason(int reasonId) throws IntegrationException {
        OccPermitApplicationReason occpermitappreason = null;

        String query = "SELECT reasonid, reasontitle, reasondescription, activereason, "
                + "humanfriendlydescription, periodtypeproposal_periodid\n "
                + "FROM public.occpermitapplicationreason \n"
                + "WHERE reasonid = ?;";

        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = getPostgresCon();
            stmt = con.prepareStatement(query);
            stmt.setInt(1, reasonId);
            rs = stmt.executeQuery();

            while (rs.next()) {
                occpermitappreason = generateOccPermitApplicationReason(rs);
            }

        } catch (SQLException ex) {
            throw new IntegrationException("OccupancyInspectionIntegrator.getOccPermitApplicationReason | "
                    + "IntegrationException: Unable to get occupancy permit application reason ", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }

        return occpermitappreason;
    }

    /**
     * Generator for occpermit application reasons
     * @param rs
     * @return
     * @throws IntegrationException 
     */
    private OccPermitApplicationReason generateOccPermitApplicationReason(ResultSet rs) throws IntegrationException {
        OccPermitApplicationReason occpermitappreason = new OccPermitApplicationReason();

        try {
            occpermitappreason.setId(rs.getInt("reasonid"));
            occpermitappreason.setTitle(rs.getString("reasontitle"));
            occpermitappreason.setDescription(rs.getString("reasondescription"));
            occpermitappreason.setActive(rs.getBoolean("activereason"));
            occpermitappreason.setHumanFriendlyDescription(rs.getString("humanfriendlydescription"));
            occpermitappreason.setPersonsRequirement(getPersonsRequirement(rs.getInt("reasonid")));
            // this is wrong!!
            occpermitappreason.setProposalPeriodType(getOccPermitType(rs.getInt("periodtypeproposal_periodid")));
        } catch (SQLException ex) {
            throw new IntegrationException("OccupancyIntegrator.generateOccPermitApplicationReason | "
                    + "Integration Error: Unable to generate occupancy permit application reason ", ex);
        }

        return occpermitappreason;
    }

    /**
     * Extracts and builds an occ application person requirement
     * @Deprecated  with humanization there are no longer person types
     * @param reasonId
     * @return
     * @throws IntegrationException 
     */
    public OccAppPersonRequirement getPersonsRequirement(int reasonId) throws IntegrationException {
        OccAppPersonRequirement personsRequirement = null;
        String query = "SELECT reasonid, humanfriendlydescription FROM public.occpermitapplicationreason "
                + "WHERE reasonid = ?";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, reasonId);
            rs = stmt.executeQuery();
            while (rs.next()) {
                personsRequirement = generatePersonsRequirement(rs);
            }
        } catch (SQLException ex) {
            throw new IntegrationException("OccupancyIntegrator.getPersonsRequirement | "
                    + "IntegrationError: Unable to get PersonsRequirement ", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }

        return personsRequirement;
    }

    /**
     * Generator for Occ application person requriements
     * @Deprecated  with humanization there are no longer person types
     * @param rs
     * @return
     * @throws IntegrationException 
     */
    private OccAppPersonRequirement generatePersonsRequirement(ResultSet rs) throws IntegrationException {
        OccAppPersonRequirement personsRequirement = new OccAppPersonRequirement();

        try {
            personsRequirement.setRequirementSatisfied(false);
            personsRequirement.setRequirementExplanation(rs.getString("humanfriendlydescription"));
            personsRequirement.setRequiredPersonTypes(getRequiredPersonTypes(rs.getInt("reasonid")));
            personsRequirement.setOptionalPersonTypes(getOptionalPersonTypes(rs.getInt("reasonid")));
        } catch (SQLException ex) {
            throw new IntegrationException("OccupancyIntegrator.generatePersonsRequirement | "
                    + "IntegrationError: Unable to generate PersonsRequirement. ", ex);
        }

        return personsRequirement;
    }

    /**
     * Extracts person types
     * @Deprecated  with humanization there are no longer person types
     * @param reasonId
     * @return
     * @throws IntegrationException 
     */
    public List<PersonType> getRequiredPersonTypes(int reasonId) throws IntegrationException {
        List<PersonType> requiredPersonTypes = null;
        String query = "SELECT requiredpersontypes FROM public.occpermitapplicationreason "
                + "WHERE reasonid = ?";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, reasonId);
            rs = stmt.executeQuery();
            while (rs.next()) {
                requiredPersonTypes = generateRequiredPersonTypes(rs);
            }
        } catch (SQLException ex) {
            throw new IntegrationException("OccupancyIntegrator.getRequiredPersonTypes | "
                    + "IntegrationError: Unable to get required person types ", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }

        return requiredPersonTypes;
    }

    /**
     * Generator of required person types
     * @Deprecated  with humanization there are no longer person types
     * @param rs
     * @return
     * @throws IntegrationException 
     */
    private List<PersonType> generateRequiredPersonTypes(ResultSet rs) throws IntegrationException {
        List<PersonType> requiredPersonTypes = new ArrayList<>();
        String[] convertedPersonTypes = null;

        try {

            Array personTypes = rs.getArray("requiredpersontypes");
            convertedPersonTypes = (String[]) personTypes.getArray();

        } catch (SQLException ex) {
            throw new IntegrationException("OccupancyIntegrator.generateRequiredPersonTypes | "
                    + "IntegrationError: Unable to generate required person types ", ex);
        }
        for (String personType : convertedPersonTypes) {
            PersonType requiredPersonType = PersonType.valueOf(personType);
            requiredPersonTypes.add(requiredPersonType);
        }
        return requiredPersonTypes;
    }

    /**
     * Extracts optional person types
     * @Deprecated  with humanization there are no longer person types
     * @param reasonId
     * @return
     * @throws IntegrationException 
     */
    public List<PersonType> getOptionalPersonTypes(int reasonId) throws IntegrationException {
        List<PersonType> optionalPersonTypes = null;
        String query = "SELECT optionalpersontypes FROM public.occpermitapplicationreason "
                + "WHERE reasonid = ?";
        Connection con = getPostgresCon();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, reasonId);
            rs = stmt.executeQuery();
            while (rs.next()) {
                optionalPersonTypes = generateOptionalPersonTypes(rs);
            }
        } catch (SQLException ex) {
            throw new IntegrationException("OccupancyIntegrator.getOptionalPersonTypes | "
                    + "IntegrationError: Unable to get optional person types. ", ex);
        } finally {
            releasePostgresConnection(con, stmt, rs);
        }

        return optionalPersonTypes;
    }

    /**
     * Generator for optional person types
     * @Deprecated  with humanization there are no longer person types
     * @param rs
     * @return
     * @throws IntegrationException 
     */
    private List<PersonType> generateOptionalPersonTypes(ResultSet rs) throws IntegrationException {
        List<PersonType> optionalPersonTypes = new ArrayList<>();
        String[] convertedPersonTypes = null;

        try {
            java.sql.Array persTypesSQLArray = rs.getArray("optionalpersontypes");
            if (persTypesSQLArray != null) {
                convertedPersonTypes = (String[]) persTypesSQLArray.getArray();
            }

        } catch (SQLException ex) {
            throw new IntegrationException("OccupancyIntegrator.generateOptionalPersonTypes | "
                    + "IntegrationError: Unable to generate optional person types. ", ex);
        }
        if (convertedPersonTypes != null) {
            for (String personType : convertedPersonTypes) {
                PersonType optionalPersonType = PersonType.valueOf(personType);
                optionalPersonTypes.add(optionalPersonType);
            }
        }
        return optionalPersonTypes;
    }
    
    /**
     * Inserts a person into the occpermitapplicationperson table in the
     * database.
     * @Deprecated  with humanization there are no longer person types
     * @param person
     * @param applicationID
     * @throws IntegrationException
     */
    public void insertOccApplicationPerson(HumanLink person, int applicationID) throws IntegrationException {

        String query = "INSERT INTO public.occpermitapplicationperson(occpermitapplication_applicationid, "
                + "person_personid, applicant, preferredcontact, active, applicationpersontype)\n"
                + "VALUES (?, ?, ?, ?, true, CAST (? AS persontype));";

        Connection con = getPostgresCon();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, applicationID);
            stmt.setInt(2, person.getHumanID());

//  ----->  TODO: Update for Humanization/Parcelization <------
//            stmt.setBoolean(3, person.isApplicant());
//            stmt.setBoolean(4, person.isPreferredContact());
//            stmt.setString(5, person.getApplicationPersonType().name());
            
            stmt.execute();
            } catch (SQLException ex) {
                System.out.println("OccupancyIntegrator.insertOccApplicationPerson() | ERROR: "+ ex);
                throw new IntegrationException("OccupancyIntegrator.insertOccApplicationPerson"
                        + " | IntegrationException: Unable to insert occupancy permit application ", ex);
            } finally{
            releasePostgresConnection(con, stmt);
            }
    }
    
    public void updatePersonOccPeriod(HumanLink input, OccPermitApplication app) throws IntegrationException{
        Connection con = getPostgresCon();
        String query = "UPDATE occpermitapplicationperson "
                + "SET applicant = ?, preferredcontact = ?, applicationpersontype = ?::persontype, active = ? "
                + "WHERE person_personid = ? AND occpermitapplication_applicationid = ?;";

        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(query);

//  ----->  TODO: Update for Humanization/Parcelization <------
//            stmt.setBoolean(1, input.isApplicant());
//            stmt.setBoolean(2, input.isPreferredContact());
//            stmt.setString(3, input.getApplicationPersonType().name());
//            stmt.setBoolean(4, input.isLinkActive());
            stmt.setInt(5, input.getHumanID());
            stmt.setInt(6, app.getId());
            stmt.execute();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
            throw new IntegrationException("Unable to update person-occperiod link");
        } finally {
            releasePostgresConnection(con, stmt);
        } 
    }

   
   
}
