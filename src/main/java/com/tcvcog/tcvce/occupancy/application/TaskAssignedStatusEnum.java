/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.occupancy.application;

/**
 * Specifies possible task types
 * @author Ellen Bascomb of Apartment 31Y
 */
public enum TaskAssignedStatusEnum {
    NOT_COMPLETE_NOT_OVERDUE (  "Not complete; not overdue",
                                "Due date in the future",
                                "pending"),
    
    COMPLETE         (          "Complete",
                                "Task is done",
                                "check_circle"),
    
    OVERDUE (                   "Not complete; OVERDUE",
                                "No completion and due date has passed",
                                "warning"),
    
    NULLIFIED (                 "Nullified",
                                "Removed from task sequence",
                                "event_busy"),
    
    UNKNOWN (                   "Task status unknown",
                                "Unable to determine status",
                                "unknown_document");
    
  
    private final String title;
    private final String description;
    private final String materialIcon;
    
    private TaskAssignedStatusEnum(String t, String d, String mati){
        title = t;
        description = d;
        materialIcon = mati;
    }
    
    public String getTitle(){
        return title;
    }
    
    public String getDescription(){
        return description;
    }

    /**
     * @return the materialIcon
     */
    public String getMaterialIcon() {
        return materialIcon;
    }
    
    
}
