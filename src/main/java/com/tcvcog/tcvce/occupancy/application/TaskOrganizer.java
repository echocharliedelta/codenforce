/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.occupancy.application;

import java.io.Serializable;
import java.util.List;
import org.primefaces.model.TreeNode;

/**
 * Contains a list of TaskAssigned and their tree form incarnation.
 * Designed to be in the belly of an OccPeriod.
 * 
 * @author pierre15
 */
public class TaskOrganizer implements Serializable{
   
    private TaskChain governingChain;
    private List<TaskAssigned> taskAssignedList;
    private TreeNode<TaskAssigned> taskTree;

    
    
    /**
     * @return the governingChain
     */
    public TaskChain getGoverningChain() {
        return governingChain;
    }

    /**
     * @param governingChain the governingChain to set
     */
    public void setGoverningChain(TaskChain governingChain) {
        this.governingChain = governingChain;
    }

    /**
     * @return the taskAssignedList
     */
    public List<TaskAssigned> getTaskAssignedList() {
        return taskAssignedList;
    }

    /**
     * @param taskAssignedList the taskAssignedList to set
     */
    public void setTaskAssignedList(List<TaskAssigned> taskAssignedList) {
        this.taskAssignedList = taskAssignedList;
    }

    /**
     * @return the taskTree
     */
    public TreeNode<TaskAssigned> getTaskTree() {
        return taskTree;
    }

    /**
     * @param taskTree the taskTree to set
     */
    public void setTaskTree(TreeNode<TaskAssigned> taskTree) {
        this.taskTree = taskTree;
    }
    
    
  
    
}
