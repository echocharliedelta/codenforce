package com.tcvcog.tcvce.occupancy.application;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.coordinators.MunicipalityCoordinator;
import com.tcvcog.tcvce.coordinators.OccupancyCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.EventException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.MunicipalityDataHeavy;
import com.tcvcog.tcvce.entities.OccPermitPersonListEnum;
import com.tcvcog.tcvce.entities.occupancy.OccPermitType;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OccPermitTypeBB extends BackingBeanUtils implements Serializable {
    private List<OccPermitType> permitTypeList;
    private OccPermitType currentOccPermitType;
    private MunicipalityDataHeavy currentOccPermitMuniDataHeavy;
    
    private boolean editModeOccPermitTypeInfo;
    private boolean occPermitTypeCreateMode;

    private boolean includeInactiveOccPermitTypes;
    private boolean showAllMunis;

    private List<Municipality> muniList;
    
    private List<OccPermitPersonListEnum> personListEnumCandidates;

    public OccPermitTypeBB() {
    }

    @PostConstruct
    public void initBean()  {
        System.out.println("OccPermit Type BB: Init");
        MunicipalityCoordinator mc = getMuniCoordinator();
        try {
            includeInactiveOccPermitTypes = false;
            showAllMunis = false;
            occPermitTypeCreateMode = false;
            editModeOccPermitTypeInfo = false;
            refreshOccPermitTypeList();
            if (Objects.nonNull(permitTypeList) && !permitTypeList.isEmpty()) {
                registerCurrentOccPermitType(permitTypeList.get(0));
            }
            muniList = mc.getMuniList(true);
            configureOccPermitPersonColumnLabelEnumCandidates();
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }
    }
    
    /**
     * Creates a list of all our enum candidates for permit type config
     */
    private void configureOccPermitPersonColumnLabelEnumCandidates(){
        personListEnumCandidates = new ArrayList<>();
        personListEnumCandidates.addAll(Arrays.asList(OccPermitPersonListEnum.values()));
    }

    /**
     * Loads a fresh lists of OccPermitTypes from the db
     *
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public void refreshOccPermitTypeList() throws BObStatusException {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        Municipality muni = null;
        if (!showAllMunis) {
            muni = getSessionBean().getSessMuni();
        }
        try {
            permitTypeList = oc.getOccPermitTypeListComplete(muni, !includeInactiveOccPermitTypes);
            // refresh our list on the permit file type bb as well
            OccPeriodTypeBB optbb = (OccPeriodTypeBB) getFacesContext().getApplication().evaluateExpressionGet(getFacesContext(), "#{occPeriodTypeBB}", OccPeriodTypeBB.class);
            if(optbb != null){
                optbb.refreshOccPermitTypeListComplete();
            }
            
        } catch (IntegrationException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to load OccPermit Type List",
                            "This must be corrected by the system administrator"));
        }
    }

    public void viewOccPermitType(OccPermitType permitType) {
        registerCurrentOccPermitType(permitType);
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Viewing OccPermit Type: " + permitType.getTypeID(), ""));
    }
    
    /**
     * Injects the given OPT into current member and sets the muni data heavy
     * to match
     * @param opt 
     */
     private void registerCurrentOccPermitType(OccPermitType opt){
        MunicipalityCoordinator mc = getMuniCoordinator();
        currentOccPermitType = opt;
        if(opt != null){
            try {
                if(currentOccPermitType.getMuni() != null){
                    currentOccPermitMuniDataHeavy = mc.assembleMuniDataHeavy(currentOccPermitType.getMuni(), getSessionBean().getSessUser());
                }
            } catch (IntegrationException | AuthorizationException | BObStatusException | BlobException | EventException  ex) {
                System.out.println(ex);
                 getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to load OccPermit Type",
                            "This must be corrected by the system administrator"));
            } 
        }
     }
    

    /**
     * Listener for users to click the add or the edit button
     *
     * @param ev
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public void toggleOccPermitTypeAddEditButtonChange(ActionEvent ev) throws BObStatusException {
        System.out.println("toggle OccPermitType AddEditButtonChange");
        if (editModeOccPermitTypeInfo) {
            if (Objects.nonNull(currentOccPermitType) && currentOccPermitType.getTypeID() == 0) {
                onInsertOccPermitTypeCommit();
            } else {
                onUpdateOccPermitTypeCommit();
            }
            refreshOccPermitTypeList();
        }
        editModeOccPermitTypeInfo = !editModeOccPermitTypeInfo;
    }

    /**
     * Begins the OccPermitType creation process
     *
     * @param ev
     */
    public void onOccPermitTypeAddInit(ActionEvent ev) {
        System.out.println("OccPermiTypeBB.onOccPermitTypeAddInit");
        OccupancyCoordinator oc = getOccupancyCoordinator();
        registerCurrentOccPermitType(oc.getOccPermitTypeSkeleton(getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod()));
        editModeOccPermitTypeInfo = true;
    }

    /**
     * Listener for users to cancel their OccPermit Type add/edit operation
     *
     * @param ev
     */
    public void onOperationOccPermitTypeAddEditAbort(ActionEvent ev) {
        System.out.println("occPermitTypeBB.toggleOccPermitTypeAddEditButtonChange: ABORT");
        editModeOccPermitTypeInfo = false;
        occPermitTypeCreateMode = false;
    }

    public void onInsertOccPermitTypeCommit() {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            registerCurrentOccPermitType(oc.insertOccPermitType(currentOccPermitType, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod()));
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Successful Insert New OccPermit Type!", ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
        }
    }

    public void onUpdateOccPermitTypeCommit() {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            oc.updateOccPermitType(currentOccPermitType, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "OccPermit Type updated with Id " + currentOccPermitType.getTypeID(), ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Failed to Update Permit Type", ""));
        }
    }

    /**
     * Listener for user request to start the nuking process of a OccPermit Type
     *
     * @param permitType
     */
    public void onOccPermitTypeNukeInitButtonChange(OccPermitType permitType) {
        registerCurrentOccPermitType(permitType);
    }

    /**
     * Listener for user requests to commit a OccPermit Type nuke operation
     *
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void onOccPermitTypeNukeCommitButtonChange() throws AuthorizationException {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            oc.deactivateOccPermitType(currentOccPermitType, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
            refreshOccPermitTypeList();
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "OccPermit Type deactivated with ID: " + currentOccPermitType.getTypeID(), ""));
            if (Objects.nonNull(permitTypeList) && !permitTypeList.isEmpty()) {
                registerCurrentOccPermitType(permitTypeList.get(0));
            } else {
                registerCurrentOccPermitType(null);
            }
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
        }
    }

    public List<OccPermitType> getPermitTypeList() {
        return permitTypeList;
    }

    public void setPermitTypeList(List<OccPermitType> permitTypeList) {
        this.permitTypeList = permitTypeList;
    }

    public OccPermitType getCurrentOccPermitType() {
        return currentOccPermitType;
    }

    public void setCurrentOccPermitType(OccPermitType currentOccPermitType) {
        this.currentOccPermitType = currentOccPermitType;
    }

    public boolean isEditModeOccPermitTypeInfo() {
        return editModeOccPermitTypeInfo;
    }

    public void setEditModeOccPermitTypeInfo(boolean editModeOccPermitTypeInfo) {
        this.editModeOccPermitTypeInfo = editModeOccPermitTypeInfo;
    }

    public boolean isOccPermitTypeCreateMode() {
        return occPermitTypeCreateMode;
    }

    public void setOccPermitTypeCreateMode(boolean occPermitTypeCreateMode) {
        this.occPermitTypeCreateMode = occPermitTypeCreateMode;
    }

    public boolean isIncludeInactiveOccPermitTypes() {
        return includeInactiveOccPermitTypes;
    }

    public void setIncludeInactiveOccPermitTypes(boolean includeInactiveOccPermitTypes) {
        this.includeInactiveOccPermitTypes = includeInactiveOccPermitTypes;
    }

    public boolean isShowAllMunis() {
        return showAllMunis;
    }

    public void setShowAllMunis(boolean showAllMunis) {
        this.showAllMunis = showAllMunis;
    }


    public List<Municipality> getMuniList() {
        return muniList;
    }

    public void setMuniList(List<Municipality> muniList) {
        this.muniList = muniList;
    }

    /**
     * @return the personListEnumCandidates
     */
    public List<OccPermitPersonListEnum> getPersonListEnumCandidates() {
        return personListEnumCandidates;
    }

    /**
     * @param personListEnumCandidates the personListEnumCandidates to set
     */
    public void setPersonListEnumCandidates(List<OccPermitPersonListEnum> personListEnumCandidates) {
        this.personListEnumCandidates = personListEnumCandidates;
    }

    /**
     * @return the currentOccPermitMuniDataHeavy
     */
    public MunicipalityDataHeavy getCurrentOccPermitMuniDataHeavy() {
        return currentOccPermitMuniDataHeavy;
    }

    /**
     * @param currentOccPermitMuniDataHeavy the currentOccPermitMuniDataHeavy to set
     */
    public void setCurrentOccPermitMuniDataHeavy(MunicipalityDataHeavy currentOccPermitMuniDataHeavy) {
        this.currentOccPermitMuniDataHeavy = currentOccPermitMuniDataHeavy;
    }

  
}
