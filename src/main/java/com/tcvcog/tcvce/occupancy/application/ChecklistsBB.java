/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcvcog.tcvce.occupancy.application;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.coordinators.CodeCoordinator;
import com.tcvcog.tcvce.coordinators.OccInspectionCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.EventException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.CodeSet;
import com.tcvcog.tcvce.entities.CodeSource;
import com.tcvcog.tcvce.entities.EnforceableCodeElement;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.occupancy.OccChecklistTemplate;
import com.tcvcog.tcvce.entities.occupancy.OccInspectionCause;
import com.tcvcog.tcvce.entities.occupancy.OccSpaceElement;
import com.tcvcog.tcvce.entities.occupancy.OccSpaceType;
import com.tcvcog.tcvce.entities.occupancy.OccSpaceTypeChecklistified;
import java.util.ArrayList;
import java.util.List;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;
import jakarta.inject.Inject;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Backing bean that supports the creation and management of field inspection
 * checklists
 *
 * @author Ellen Bascomb of Apartment 31Y
 */
public class ChecklistsBB extends BackingBeanUtils {
    
  
   
    private List<OccChecklistTemplate> checklistTemplateList;
    private boolean checklistTemplateListShowAllMunis;
    private boolean checklistTemplateListIncludeDeactivated;
    private OccChecklistTemplate currentChecklistTemplate;
    
    // clones of checklists
    private OccChecklistTemplate clonedChecklistTemplate;
    private boolean cloneChecklistAddOrdsToTargetMuniCodebook;
    private boolean editModeCurrentChecklistTemplate;
    private List<CodeSource> codeSourceList;
    private List<OccInspectionCause> causeCandidateList;
    
    // space type management
    private List<OccSpaceType> spaceTypeList;
    private List<OccSpaceType> spaceTypeListSelected;
    private boolean spaceTypeBatchRequired;
    private OccSpaceType currentOccSpaceType;
    private boolean editModeCurrentOccSpaceType;
    
    // space type cloning and copying
    private String cloneCopyPathway;
    private boolean cloneSpaceTypeToOtherChecklist;
    private boolean copyOrdinancesToExistingSpaceType;
    
    private OccSpaceTypeChecklistified spaceTypeToClone;
    private OccSpaceTypeChecklistified selectedSpaceTypeForCopy;

    private OccSpaceTypeChecklistified currentOccSpaceTypeChecklistified;
    private boolean editModeCurrentOccSpaceTypeChecklistified;
    private OccChecklistTemplate targetChecklistTemplateForSpaceTypeClone;
    
    // ordinances
    private CodeSet currentCodeSet;
    private List<CodeSet> codeSetList;
    private CodeSet selectedCodeSet;
    
    private OccSpaceElement currentOccSpaceElement;
    private List<OccSpaceElement> occSpaceElementList;
    private List<OccSpaceElement> occSpaceElementListSelected;
    private List<OccSpaceElement> occSpaceElementListFiltered;
    private Set<OccSpaceElement> occSpaceElementListPreviousSelected;
    private List<OccSpaceElement> occSpaceElementSelectedFilteredList;
    private String ordinanceFilterText;
    
    // permissions
    private boolean permissionAllowAddMissingOrdsToTargetMuni;
    private boolean permissionAllowChecklistEdit;
    private boolean permissionEnableCrossCodebookAddFacility;
    
    /**
     * Creates a new instance of checklistsBB
     */
    public ChecklistsBB() {
    }

    /**
     * Sets up our master lists and option drop downs
     */
    @PostConstruct
    public void initBean() {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        CodeCoordinator cc = getCodeCoordinator();
        try {
            checklistTemplateListShowAllMunis = false;
            checklistTemplateListIncludeDeactivated = false;
            refreshCurrentChecklistTemplateList();
            codeSourceList = cc.getCodeSourceList(false);
            causeCandidateList = oic.getOccInspectionCauseListComplete(true, null);
            refreshSpaceTypeListAndClearSelected();
            currentCodeSet = getSessionBean().getSessCodeSet();
            configureOccSpaceElementList();
            configurePermissions();
            occSpaceElementListPreviousSelected = new HashSet<>();
            occSpaceElementSelectedFilteredList = new ArrayList<>();
            
            // setup cross codebook facility
            codeSetList = cc.getCodeSetListComplete();
            
            
        } catch (IntegrationException | BObStatusException  ex) {
            System.out.println(ex);
        }
    }
    
    /**
     * Internal organ for wrapping the current code set's elements in wrappers
     * for selection and attachment to an inspected space
     */
    private void configureOccSpaceElementList(){
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        if(currentCodeSet != null){
            occSpaceElementList = oic.wrapECEListInOccSpaceElementWrapper(currentCodeSet.getEnfCodeElementList());
        } else {
            System.out.println("ChecklistsBB.configureOccSpaceElementList | cannot build wrapped ord list with null codeset");
        }
    }
    
    /**
     * Asks the checklist coordinator for permissions switch settings
     */
    private void configurePermissions(){
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        
        permissionAllowChecklistEdit = oic.permissionCheckpointChecklistEditGeneral(getSessionBean().getSessUser());
        permissionAllowAddMissingOrdsToTargetMuni = oic.permissionsCheckpointChecklistCloneAddMissingOrdsToTargetCodeBook(getSessionBean().getSessUser());
        permissionEnableCrossCodebookAddFacility = oic.permissionsCheckpointChecklistCrossCodebookAdd(getSessionBean().getSessUser());
        
    }
    
    /**
     * Sets member with list of all space types--muni agnostic
     */
    private void refreshSpaceTypeListAndClearSelected() throws IntegrationException{
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        spaceTypeList = oic.getOccSpaceTypeList();
        spaceTypeListSelected = new ArrayList<>();
        
    }

    
    /**
     * Listener to start the creation process of an OccChecklist
     * @param ev 
     */
    public void onChecklistCreateInitButtonClick(ActionEvent ev){
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        currentChecklistTemplate = oic.getOccChecklistTemplateSkeleton(getSessionBean().getSessMuni());
        editModeCurrentChecklistTemplate = false; 
        toggleEditModeChecklistTemplate();
    }
    
    /**
     * Listener for user requests to view a checklist
     *
     * @param oct
     */
    public void onChecklistViewEditLinkClick(OccChecklistTemplate oct) {
        System.out.println("ChecklistsBB.onChecklistViewEditLinkClick | ChecklsitID: " + oct.getInspectionChecklistID());
        currentChecklistTemplate = oct;
        refreshCurrentChecklistTemplate();

    }
    
    
    /**
     * Pass-through listener to receive button clicks; delegates to
     * no arg method toggleEditModeChecklistTemplate
     * @param ev 
     */
    public void onToggleEditModeChecklistTemplateButtonClick(ActionEvent ev){
        toggleEditModeChecklistTemplate();
    }

    /**
     * Listener for user requests to begin the checklist clone operation
     * @param ev 
     */
    public void onCloneChecklistInit(ActionEvent ev){
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        System.out.println("ChecklistsBB.onCloneChecklistInit");
        if(currentChecklistTemplate != null){
            clonedChecklistTemplate = oic.getOccChecklistTemplateSkeleton(getSessionBean().getSessMuni());
            cloneChecklistAddOrdsToTargetMuniCodebook = false;
        } else {
            getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fatal error on checklist clone init.", ""));
        }
        
    }
    
    /**
     * Finalizes request to make a deep copy of the current checklist
     * @param ev 
     */
    public void onCloneChecklistCommit(ActionEvent ev){
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        try {
            int freshChecklistID = oic.cloneOccChecklistTemplate(currentChecklistTemplate, clonedChecklistTemplate, cloneChecklistAddOrdsToTargetMuniCodebook, getSessionBean().getSessUser());
            currentChecklistTemplate = oic.getChecklistTemplate(freshChecklistID);
            checklistTemplateListShowAllMunis = true;
            refreshCurrentChecklistTemplateList();
        } catch (BObStatusException | AuthorizationException | EventException | IntegrationException ex) {
            System.out.println(ex);
              getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            
        } 
        System.out.println("ChecklistsBB.onCloneChecklistCommit");
        
    }
    
    /**
     * Listener for user requests to abort the checklist clone operation
     * @param ev 
     */
    public void onCloneChecklistOperationAbort(ActionEvent ev){
        System.out.println("ChecklistsBB.onCloneChecklistOperationAbort");
         getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Clone operation aborted!", ""));
    }
    
    /**
     * Listener for user clicks of the "edit" button which starts the update
     * process and its re-click as the "save edits" button which either writes
     * the new template in or updates the existing one.
     */
    public void toggleEditModeChecklistTemplate() {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        try {
            System.out.println("InspectionsBB.toggleEditModeChecklistTemplate | start of method value: " + editModeCurrentChecklistTemplate);
            if (editModeCurrentChecklistTemplate) {
                if (currentChecklistTemplate == null) {
                    throw new BObStatusException("Cannot edit a null current checklist template");
                }
                if(currentChecklistTemplate.getInspectionChecklistID() == 0){
                    int freshid = oic.insertChecklistTemplateMetadata(currentChecklistTemplate, getSessionBean().getSessUser());
                    currentChecklistTemplate = oic.getChecklistTemplate(freshid);
                    System.out.println("InspectionsBB.toggleEditModeChecklistTemplate | inserted...");
                } else {
                    oic.updateChecklistTemplateMetadata(currentChecklistTemplate, getSessionBean().getSessUser());
                    System.out.println("InspectionsBB.toggleEditModeChecklistTemplate | updated...");
                }
                refreshCurrentChecklistTemplateList();
                refreshCurrentChecklistTemplate();

            } 
        } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
        // do the toggle
        editModeCurrentChecklistTemplate = !editModeCurrentChecklistTemplate;
    }
    
    /**
     * Listener for user requests to abort the edit of a checklist
     * @param ev 
     */
    public void onEditModeChecklistTemplateAbort(ActionEvent ev){
        // turn off edit mode; don't talk to DB about anything!
        editModeCurrentChecklistTemplate = !editModeCurrentChecklistTemplate;
    }
    
    /**
     * Grabs a fresh copy of the checklist list from DB 
     * and restores the current template to the current Role
     */
    public void refreshCurrentChecklistTemplateList(){
        OccInspectionCoordinator osi = getOccInspectionCoordinator();
        Municipality muni = null; 
        if(!checklistTemplateListShowAllMunis){
            muni = getSessionBean().getSessMuni();
        }
        try {
            checklistTemplateList = osi.getOccChecklistTemplateList(muni, checklistTemplateListIncludeDeactivated);
        } catch (IntegrationException ex) {
            System.out.println(ex);
        }
    }
    
    /**
     * Asks the coordinator for a fresh copy of the current checklist
     */
    public void refreshCurrentChecklistTemplate(){
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        if(currentChecklistTemplate !=  null){
            try {
                currentChecklistTemplate = oic.getChecklistTemplate(currentChecklistTemplate.getInspectionChecklistID());
            } catch (IntegrationException ex) {
                System.out.println(ex);
            }
        }
    }
    
    /**
     * Listener for user requests to start the deactivation process
     * @param oct to be deactivated in next step
     */
    public void onDeactivateChecklistInitLinkClick(ActionEvent ev){
        System.out.println("ChecklistsBB.onDeactivateChecklistInitLinkClick");
        
    }
    
    
    /**
     * Listener for user requests to deactivate a checklist
     * @param ev 
     */
    public void onDeactivateChecklistButtonPress(ActionEvent ev){
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        if(currentChecklistTemplate != null){
            try {
                oic.deactivateChecklistTemplate(getSessionBean().getSessUser(), currentChecklistTemplate);
                refreshCurrentChecklistTemplateList();
                getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_INFO, 
                               "Deactivated checklist template ID: " 
                                       + currentChecklistTemplate.getInspectionChecklistID(), ""));
                currentChecklistTemplate = null;
            } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            }
        }
    }
    
    /**
     * Listener for user requests to start the process 
     * to create a new space type
     * @param ev 
     */
    public void onCreateNewSpaceTypeInitButtonPush(ActionEvent ev){
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        currentOccSpaceType = oic.getOccSpaceTypeSkeleton();
        editModeCurrentOccSpaceType = true;
    }
    
    /**
     * Deac stamps the given space type and it will not display in the 
     * candidate list (but this won't impact existing checklists)
     * @param ost 
     */
    public void onDeactivateSpaceType(OccSpaceType ost){
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        try {
            oic.deactivateOccSpaceType(ost, getSessionBean().getSessUser());
            refreshSpaceTypeListAndClearSelected();
            System.out.println("ChecklistsBB.onDeactivateSpaceType");
            getFacesContext().addMessage(null,
                   new FacesMessage(FacesMessage.SEVERITY_INFO, 
                           "Deactivated space type!", ""));
        } catch (BObStatusException | AuthorizationException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                   new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            
        } 
    }
    
    
      /**
     * Listener for user clicks of the "edit" button which starts the update
     * process and its re-click as the "save edits" button which either writes
     * the new template in or updates the existing one.
     */
    public void toggleEditModeOccSpaceType() {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        try {
            if (editModeCurrentOccSpaceType) {
                if (currentOccSpaceType == null) {
                    throw new BObStatusException("Cannot edit a null currentOccSpaceType");
                }
                if(currentOccSpaceType.getSpaceTypeID() == 0){
                    int freshSpcID = oic.insertSpaceType(currentOccSpaceType, getSessionBean().getSessUser());
                    currentOccSpaceType = oic.getOccSpaceType(freshSpcID);
                    getFacesContext().addMessage(null,
                           new FacesMessage(FacesMessage.SEVERITY_INFO, 
                                   "Success: Added new space type: " + currentOccSpaceType.getSpaceTypeTitle(), ""));
                } else {
                    oic.updateSpaceType(currentOccSpaceType, getSessionBean().getSessUser());
                    // refresh
                    currentOccSpaceType = oic.getOccSpaceType(currentOccSpaceType.getSpaceTypeID());
                    getFacesContext().addMessage(null,
                           new FacesMessage(FacesMessage.SEVERITY_INFO, 
                                   "Success: Updated space type: " + currentOccSpaceType.getSpaceTypeTitle(), ""));
                }
                refreshSpaceTypeListAndClearSelected();
                // rebuild our spaceType list with the new values
//                onSpaceTypeLinkInit();

            } else {
                getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_INFO, 
                               "You are now editing space type ID " + currentOccSpaceType.getSpaceTypeID(), ""));

            }
        } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }

        // do the toggle
        editModeCurrentOccSpaceType = !editModeCurrentOccSpaceType;

    }
    
    /**
     * The actual listener that delegated to the toggle method
     * @param ev 
     */
    public void onEditModeOccSpaceTypeToggleButtonPush(ActionEvent ev){
        toggleEditModeOccSpaceType();
        
        
    }
    
    
    /**
     * Listener for user requests to abort the edit of a checklist
     * @param ev 
     */
    public void onEditModeOccSpaceTypeAbort(ActionEvent ev){
        // turn off edit mode; don't talk to DB about anything!
        
        editModeCurrentOccSpaceType = !editModeCurrentOccSpaceType;
        
    }
    
    /**
     * Listener for user requests to start the link process
     * between a space type and the current checklist
     * @param ev 
     */
    public void onSpaceTypeLinkInitButtonChange(ActionEvent ev){
        try {
            refreshSpaceTypeListAndClearSelected();
            
        } catch (IntegrationException ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
        }
    }
    
    
    /**
     * Builds a custom spaceTypeLIst that only includes unique values
     * for the current Checklist
     */
    private void onSpaceTypeLinkInit(){
        // Take out the space types already linked
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        List<OccSpaceType> tempTypeList = new ArrayList<>();
        if(spaceTypeList == null){
            spaceTypeList = new ArrayList<>();
        }
        if(spaceTypeListSelected == null){
            spaceTypeListSelected = new ArrayList();
        }
        if(!spaceTypeListSelected.isEmpty()){
            spaceTypeListSelected.clear();
        }

        if(     currentChecklistTemplate != null 
                && currentChecklistTemplate.getOccSpaceTypeList() != null 
                && !currentChecklistTemplate.getOccSpaceTypeList().isEmpty()){
            List<OccSpaceType> typesInChecklist = oic.downcastOccSpaceTypeChecklistified(currentChecklistTemplate.getOccSpaceTypeList());
            // Don't display space types that are already in the template
            for(OccSpaceType ost: spaceTypeList){
                if(!typesInChecklist.contains(ost)){
                    tempTypeList.add(ost);
                }
            }
            spaceTypeList = tempTypeList;
        } 
    }
    /**
     * 
     * Listener for user requests to attach all their
     * selected spaces to the current checklist
     * @param ev
     */
    public void onLinkSelectedSpaceTypesToChecklist(ActionEvent ev){
        onLinkSelectedSpaceTypesToChecklist();  // return ignored for normal UI pathway
    }
    
    /**
     * Internal organ for linking space types to a checklist
     * @return the last OccSpaceType which is now its linked subclass. A little hacky- used during cloning
     */
    private OccSpaceTypeChecklistified onLinkSelectedSpaceTypesToChecklist(){
        OccSpaceTypeChecklistified ostc = null;
        if(currentChecklistTemplate != null && spaceTypeListSelected != null && !spaceTypeListSelected.isEmpty() ){
            OccInspectionCoordinator oic = getOccInspectionCoordinator();
            for(OccSpaceType ost: spaceTypeListSelected){
                try {
                    ostc = oic.insertAndLinkSpaceTypeChecklistifiedListToTemplate(currentChecklistTemplate, ost, spaceTypeBatchRequired, getSessionBean().getSessUser());
                    getFacesContext().addMessage(null,
                           new FacesMessage(FacesMessage.SEVERITY_INFO, ost.getSpaceTypeTitle() + " linked succesfully!", ""));
                } catch (BObStatusException | IntegrationException  | AuthorizationException ex) {
                    System.out.println(ex);
                    getFacesContext().addMessage(null,
                           new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
                } 
            }
            refreshCurrentChecklistTemplate();
        }
        return ostc;
    }

    /**
     * Makes sure members ares setup for easy processing of the dialog's selections
     * of ordinances
     * @param ostchk 
     */
    public void onChooseOrdinancesToLinkToSpaceTypeLinkCLick(OccSpaceTypeChecklistified ostchk){
        currentOccSpaceTypeChecklistified = ostchk;
        resetOrdinanceSelectionFilter();
        System.out.println("ChecklistsBB.done with onChooseOrdinancesToLinkToSpaceTypeLinkCLick");
    }
    
    /**
     * Resets search backing members on this class to a fresh state
     */
    private void resetOrdinanceSelectionFilter(){
        ordinanceFilterText = "";
        occSpaceElementListFiltered = occSpaceElementList;
        occSpaceElementListSelected = new ArrayList<>();
        occSpaceElementListPreviousSelected.clear();
        occSpaceElementSelectedFilteredList.clear();
        
    }
    
    /**
     * For system admin users to add from any codebook
     * @param ev 
     */
    public void onLoadSelectedCodebookOrdinancesForSpaceAssignment(ActionEvent ev){
        if(selectedCodeSet != null){
            System.out.println("ChecklistsBB.onLoadSelectedCodebookOrdinancesForSpaceAssignment");
            currentCodeSet = selectedCodeSet;
            configureOccSpaceElementList();
            onChooseOrdinancesToLinkToSpaceTypeLinkCLick(currentOccSpaceTypeChecklistified);
        }
    }
    
    /**
     * Listener for user requests to connect their selected
     * code elements to the current SpaceType
     * 
     * @param ev 
     */
    public void onLinkSelectedECEsToSpaceType(ActionEvent ev){
        // Only do selection management when NOT cloning a space, for which this process
        // has already been implemented
        if(spaceTypeToClone == null){
            compareSelectedList();
            setSelectedFilteredList();
        }
        if(occSpaceElementListSelected != null && !occSpaceElementListSelected.isEmpty()){
            System.out.println("ChecklistsBB.onLinkSelectedECEsToSpaceType | selected list size " + occSpaceElementListSelected.size());
            OccInspectionCoordinator oic = getOccInspectionCoordinator();
            for(OccSpaceElement ose: occSpaceElementListSelected){
                try {
                    oic.insertAndLinkCodeElementsToSpaceType(currentOccSpaceTypeChecklistified, ose, currentChecklistTemplate, getSessionBean().getSessUser());
                } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
                    System.out.println(ex);
                     getFacesContext().addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error linking ordinances to space type: " + ex.getMessage(), ""));
                } 
            }
            getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_INFO,"Successfully linked code elements to space!" , ""));
            refreshCurrentChecklistTemplate();
            occSpaceElementListSelected.clear();
        } // null inputs check
    }
    
    /**
     * Listener to view the details of an enforcable ordinance in a space type.
     * Sets up the session bean to hold the choice, and lets the template be
     * displayed
     * @param ose to view
     */
    public void onViewOrdinanceDetailsLinkClick(OccSpaceElement ose){
        System.out.println("ChecklistsBB.onViewOrdinanceDetailsLinkClick | cseID: " + ose.getCodeSetElementID());
        currentOccSpaceElement = ose;
        getSessionBean().setSessEnforcableCodeElement((EnforceableCodeElement) ose);
    }
    
    /**
     * Listener to start the removal process of a space type from a checklist
     * @param ostchk 
     */
    public void onRemoveSpaceTypeFromChecklistInit(OccSpaceTypeChecklistified ostchk){
        currentOccSpaceTypeChecklistified = ostchk;
    }
    
    /**
     * Listener for user requests to remove a space type from a the current checklist
     * @param ostchk
     */
    public void onRemoveSpaceTypeFromChecklist(){
        if(currentOccSpaceTypeChecklistified != null){
            
            System.out.println("ChecklistsBB.onRemoveSpaceTypeFromChecklist | OSTID: " + currentOccSpaceTypeChecklistified.getChecklistSpaceTypeID());
            OccInspectionCoordinator oic = getOccInspectionCoordinator();
            try {
                oic.detachOccSpaceTypeChecklistifiedFromTemplate(currentOccSpaceTypeChecklistified, currentChecklistTemplate, getSessionBean().getSessUser());
                refreshCurrentChecklistTemplate();
                  getFacesContext().addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_INFO,"Successfully removed Space Type ID: " + currentOccSpaceTypeChecklistified.getSpaceTypeID(), ""));
            } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
                System.out.println(ex);
                  getFacesContext().addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_ERROR,ex.getMessage() + " | On space typeID: "+ currentOccSpaceTypeChecklistified.getSpaceTypeID(), ""));
            } 
        }
    }
    
    // CLONING SPACE TYPES AND ORDS
    
    /**
     * Starts the cloning process of Occ space types
     * 
     * @param ostc
     */
    public void onCloneSpaceTypeInit(OccSpaceTypeChecklistified ostc){
        System.out.println("ChecklistsBB.onCloneSpaceTypeInit");
        spaceTypeToClone = ostc;
        // now our current space type is a skeleton ready for a new name
        // this may or may not actually be used depending on the user's pathway selection
        onCreateNewSpaceTypeInitButtonPush(null);
        if(currentOccSpaceType != null){
            currentOccSpaceType.setSpaceTypeDescription("Created during Clone operation");;
        }
        // now simulate the selection of ordinances to link to the new space type
        occSpaceElementListSelected = new ArrayList<>();
        occSpaceElementListSelected.addAll(spaceTypeToClone.getCodeElementList());
        cloneCopyPathway = "CLONE_CURRENT";
        onClonePathwayChange();
    }
    
    /**
     * Listener for user toggling between copy pathways
     */
    public void onClonePathwayChange(){
        System.out.println("ChecklistsBB.onClonePathwayChange | Path: " + cloneCopyPathway);
        switch(cloneCopyPathway){
            case "CLONE_CURRENT" -> {
                cloneSpaceTypeToOtherChecklist = false;
                copyOrdinancesToExistingSpaceType = false;
            }
            case "COPY_CURRENT" -> {
                cloneSpaceTypeToOtherChecklist = false;
                copyOrdinancesToExistingSpaceType = true;
                
            }
            case "CLONE_OTHER" -> {
                cloneSpaceTypeToOtherChecklist = true;
                copyOrdinancesToExistingSpaceType = false;
                
            }
        }
    }
    
    /**
     * Completes the clone space type process, and depending on the boolean
     * flag settings may or may not be writing to a checklist that is not 
     * the current one
     * @param ev 
     */
    public void onCloneSpaceTypeCommit(ActionEvent ev){
        System.out.println("ChecklistsBB.onCloneSpaceTypeCommit | pathway: " + cloneCopyPathway);
        switch(cloneCopyPathway){
            case "CLONE_CURRENT" -> {
                writeNewSpaceType();
            } // close clone current
            
            case "COPY_CURRENT" -> {
                currentOccSpaceTypeChecklistified = selectedSpaceTypeForCopy;
            } // close copy current
            
            case "CLONE_OTHER" -> {
                if(targetChecklistTemplateForSpaceTypeClone != null){
                    currentChecklistTemplate = targetChecklistTemplateForSpaceTypeClone;
                } else {       
                    // FORM VALUE ERROR cleanup and abort
                      getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, "No Target checklist selected; aborting clone operation!", ""));
                      System.out.println("ChecklistsBB.doClonePathCloneOhter | FORM ERROR ON ");
                      resetSpaceTypeToClone();
                }
                writeNewSpaceType();

            } // close clone to other checklist
        } // close path-specific switch
        
        // now add all ords in our space type to clone to our new current space type checklistified
       onLinkSelectedECEsToSpaceType(null);
        
        // show success if we have it
        if(!hasErrorMessages()){
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clone/copy success", ""));
        } else {
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error cloning/coping space elements", ""));
        }
        refreshCurrentChecklistTemplate();
        resetSpaceTypeToClone();
    }
    
    /**
     * Logic snippet used during cloning operations.
     * When this method is done, the current OccSpaceTypeChecklistified is the
     * fresh one the user created in the copy/clone form
     */
    private void writeNewSpaceType(){
           
        toggleEditModeOccSpaceType();
        spaceTypeListSelected = new ArrayList<>();
        spaceTypeListSelected.add(currentOccSpaceType);

        // Our selected list is of size 1, so our returned value should be the one SpaceType now linked to 
        // our desired checklist
        currentOccSpaceTypeChecklistified = onLinkSelectedSpaceTypesToChecklist();
    }
    
    /**
     * Listener for users to trim their clone/copy list
     * @param ose 
     */
    public void onRemoveOccSpaceElementFromCloneCopyList(OccSpaceElement ose){
        if(occSpaceElementListSelected != null){
            occSpaceElementListSelected.remove(ose);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Removed Ord ID: " + ose.getElementID(), ""));
        }
    }
    
    /**
     * From ChatGPT custom 
     * @return 
     */
    public boolean hasErrorMessages() {
       
        Iterator<FacesMessage> messages = getFacesContext().getMessages();
        while (messages.hasNext()) {
            FacesMessage message = messages.next();
            if (message.getSeverity() == FacesMessage.SEVERITY_ERROR) {
                return true; // Found a SEVERITY_ERROR message
            }
        }
        return false; // No SEVERITY_ERROR messages found
    }
    
    
    /**
     * Listener for user requests to cancel space type clone
     * @param ev 
     */
    public void onCloneSpaceTypeAbort(ActionEvent ev){
        System.out.println("ChecklistsBB.onCloneSpaceTypeAbort");
        resetSpaceTypeToClone();
        
    }
    
    /**
     * sets our space Type to clone to null
     */
    private void resetSpaceTypeToClone(){
        // reset
        spaceTypeToClone = null;
        editModeCurrentOccSpaceType = false;
        selectedSpaceTypeForCopy = null;
    }
    
    
    
    // SPACE ELEMENT MANAGEMENT

    /**
     * Listener for user requests to start the removal process.
     * @param ose 
     */
    public void onRemoveOccSpaceElementFromSpaceTypeInit(OccSpaceElement ose){
        currentOccSpaceElement = ose;
    }

    /**
     * Listener for user requests to remove a space element from the type
     * @param ose 
     */
    public void onRemoveOccSpaceElementFromSpaceType(){
        if(currentOccSpaceElement != null){
            
            System.out.println("ChecklistsBB.onRemoveSpaceElementFromSpace | OSEID: " + currentOccSpaceElement.getOccChecklistSpaceTypeElementID());
            
            OccInspectionCoordinator oic = getOccInspectionCoordinator();
            try {
                oic.detachCodeElementFromSpaceType(currentOccSpaceElement, currentChecklistTemplate, getSessionBean().getSessUser());
                refreshCurrentChecklistTemplate();
                getFacesContext().addMessage(null,
                             new FacesMessage(FacesMessage.SEVERITY_INFO," Successfully removed occ space element ID: "+ currentOccSpaceElement.getOccChecklistSpaceTypeElementID(), ""));
            } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                             new FacesMessage(FacesMessage.SEVERITY_ERROR,ex.getMessage() + " | On space element ID: "+ currentOccSpaceElement.getOccChecklistSpaceTypeElementID(), ""));
            } 
        } else {
            System.out.println("ChecklistsBB.onRemoveSpaceElementFromSpace | NULL OSE INPUT!!");
        }
    }
    
    /**
     * Listener for user requests to abort the removal process
     * @param ev 
     */
    public void onRemoveOperationAbortButtonPress(ActionEvent ev){
        System.out.println("InspectionsBB.onRemoveOperationAbortButtonPress");
    }
    
   
    
    /**
     * Boolean swap tool for making required objects optional and optional ones required
     * @param oschk 
     */
    public void onToggleRequiredOccSpaceTypeChecklistified(OccSpaceTypeChecklistified oschk){
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        System.out.println("ChecklistsBB.onToggleRequiredOccSpaceTypeChecklistified | ostchkID: " + oschk.getChecklistSpaceTypeID());
        try {
            // flip!
            oschk.setRequired(!oschk.isRequired());
            oic.updateSpaceTypeChecklistified(oschk, getSessionBean().getSessUser());
            refreshCurrentChecklistTemplate();
            getFacesContext().addMessage(null,
                   new FacesMessage(FacesMessage.SEVERITY_INFO, "Toggled Required on OccSpaceType ID " + oschk.getChecklistSpaceTypeID() + " to " + oschk.isRequired(), ""));
        } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                   new FacesMessage(FacesMessage.SEVERITY_INFO, ex.getMessage(), ""));
        } 
    }
    
    /**
     * Boolean swap tool for making required objects optional and optional ones required
     * @param oschk 
     */
    public void onToggleAutoAddOccSpaceTypeChecklistified(OccSpaceTypeChecklistified oschk){
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        System.out.println("ChecklistsBB.onToggleAutoAddOccSpaceTypeChecklistified | ostchkID: " + oschk.getChecklistSpaceTypeID());
        try {
            // flip!
            oschk.setAutoAddOnInspectionInit(!oschk.isAutoAddOnInspectionInit());
            oic.updateSpaceTypeChecklistified(oschk, getSessionBean().getSessUser());
            refreshCurrentChecklistTemplate();
            getFacesContext().addMessage(null,
                   new FacesMessage(FacesMessage.SEVERITY_INFO, "Toggled auto add on OccSpaceType ID " + oschk.getChecklistSpaceTypeID() + " to " + oschk.isRequired(), ""));
        } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                   new FacesMessage(FacesMessage.SEVERITY_INFO, ex.getMessage(), ""));
        } 
    }
    
    /**
     * Boolean swap tool for making required objects optional and optional ones required
     * @param ose
     */
    public void onToggleRequiredOccSpaceElement(OccSpaceElement ose){
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        if(ose != null){

            System.out.println("ChecklistsBB.onToggleRequiredOccSpaceElement | oseID: " + ose.getOccChecklistSpaceTypeElementID());
            try {
                oic.toggleRequiredAndUpdateOccSpaceElement(ose, getSessionBean().getSessUser());
                refreshCurrentChecklistTemplate();
                getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_INFO, "Toggled Required on OccSpaceElement ID " + ose.getOccChecklistSpaceTypeElementID() + " to " + ose.isRequiredForInspection(), ""));
            } catch (IntegrationException | AuthorizationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                       new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            }
        } else {
            getFacesContext().addMessage(null,
                   new FacesMessage(FacesMessage.SEVERITY_ERROR, "No OccSpaceElement selected", ""));
        }
    }
    
    /**
     * listener for user requests to edit the space type checklistified
     * @param ostchk 
     */
    public void onSpaceTypeChecklistifiedViewEditLinkClick(OccSpaceTypeChecklistified ostchk){
        currentOccSpaceTypeChecklistified = ostchk;
    }
    
     /**
     * To build occSpaceElementListFiltered with respect to ordinance filter
     * text
     */
    private void applyOrdinanceFilter() {
        compareSelectedList();
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        occSpaceElementListFiltered = oic.inspectionAction_configureOccSpaceElementListFiltered(
                occSpaceElementList,
                ordinanceFilterText);
        setSelectedFilteredList();
    }

    /**
     * To clear applied ordinace filter
     */
    private void clearOrdinanceFilter() {
        compareSelectedList();
        setSelectedFilteredList();
        ordinanceFilterText = "";
        occSpaceElementListFiltered = occSpaceElementList;
    }
    
    private void setSelectedFilteredList() {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        occSpaceElementSelectedFilteredList.clear();
        occSpaceElementSelectedFilteredList = oic.getFilterSelectedList(occSpaceElementListFiltered, occSpaceElementListPreviousSelected);
        occSpaceElementListSelected.clear();
        occSpaceElementListSelected.addAll(occSpaceElementListPreviousSelected);
    }

    private void compareSelectedList() {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        occSpaceElementListPreviousSelected.addAll(occSpaceElementListSelected);
        if (!occSpaceElementListSelected.containsAll(occSpaceElementSelectedFilteredList)) {
            Set<OccSpaceElement> removedElements = new HashSet<>(oic.updatePreviousSelectedList(
                    occSpaceElementListPreviousSelected, occSpaceElementListSelected, occSpaceElementListFiltered));
            occSpaceElementListPreviousSelected.removeAll(removedElements);
        }
        
        if (!occSpaceElementSelectedFilteredList.containsAll( occSpaceElementListSelected)) {
            Set<OccSpaceElement> removedElement = new HashSet<>(oic.updatePreviousSelectedList(
                    occSpaceElementListPreviousSelected, occSpaceElementListSelected, occSpaceElementListFiltered));
            occSpaceElementListPreviousSelected.removeAll(removedElement);
        }
    }

    /**
     * Listener for user requests to apply ordinance filter
     */
    public void onApplyOrdinanceFilter() {
        applyOrdinanceFilter();
    }

    /**
     * Listener for user requests to clear ordinance filter
     */
    public void onClearOrdinanceFilter() {
        clearOrdinanceFilter();
    }

    // ***************************
    // *** GETTERS AND SETTERS  **
    // ***************************
    /**
     * @return the checklistTemplateList
     */
    public List<OccChecklistTemplate> getChecklistTemplateList() {
        return checklistTemplateList;
    }

    /**
     * @return the currentChecklistTemplate
     */
    public OccChecklistTemplate getCurrentChecklistTemplate() {
        return currentChecklistTemplate;
    }

    /**
     * @return the editModeCurrentChecklistTemplate
     */
    public boolean isEditModeCurrentChecklistTemplate() {
        return editModeCurrentChecklistTemplate;
    }

    /**
     * @return the currentOccSpaceType
     */
    public OccSpaceType getCurrentOccSpaceType() {
        return currentOccSpaceType;
    }

    /**
     * @return the editModeCurrentOccSpaceType
     */
    public boolean isEditModeCurrentOccSpaceType() {
        return editModeCurrentOccSpaceType;
    }

    /**
     * @return the currentOccSpaceTypeChecklistified
     */
    public OccSpaceTypeChecklistified getCurrentOccSpaceTypeChecklistified() {
        return currentOccSpaceTypeChecklistified;
    }

    /**
     * @return the editModeCurrentOccSpaceTypeChecklistified
     */
    public boolean isEditModeCurrentOccSpaceTypeChecklistified() {
        return editModeCurrentOccSpaceTypeChecklistified;
    }

    /**
     * @return the occSpaceElementList
     */
    public List<OccSpaceElement> getOccSpaceElementList() {
        return occSpaceElementList;
    }

    /**
     * @return the occSpaceElementListSelected
     */
    public List<OccSpaceElement> getOccSpaceElementListSelected() {
        return occSpaceElementListSelected;
    }

    /**
     * @param checklistTemplateList the checklistTemplateList to set
     */
    public void setChecklistTemplateList(List<OccChecklistTemplate> checklistTemplateList) {
        this.checklistTemplateList = checklistTemplateList;
    }

    /**
     * @param currentChecklistTemplate the currentChecklistTemplate to set
     */
    public void setCurrentChecklistTemplate(OccChecklistTemplate currentChecklistTemplate) {
        this.currentChecklistTemplate = currentChecklistTemplate;
    }

    /**
     * @param editModeCurrentChecklistTemplate the
     * editModeCurrentChecklistTemplate to set
     */
    public void setEditModeCurrentChecklistTemplate(boolean editModeCurrentChecklistTemplate) {
        this.editModeCurrentChecklistTemplate = editModeCurrentChecklistTemplate;
    }

    /**
     * @param currentOccSpaceType the currentOccSpaceType to set
     */
    public void setCurrentOccSpaceType(OccSpaceType currentOccSpaceType) {
        this.currentOccSpaceType = currentOccSpaceType;
    }

    /**
     * @param editModeCurrentOccSpaceType the editModeCurrentOccSpaceType to set
     */
    public void setEditModeCurrentOccSpaceType(boolean editModeCurrentOccSpaceType) {
        this.editModeCurrentOccSpaceType = editModeCurrentOccSpaceType;
    }

    /**
     * @param currentOccSpaceTypeChecklistified the
     * currentOccSpaceTypeChecklistified to set
     */
    public void setCurrentOccSpaceTypeChecklistified(OccSpaceTypeChecklistified currentOccSpaceTypeChecklistified) {
        this.currentOccSpaceTypeChecklistified = currentOccSpaceTypeChecklistified;
    }

    /**
     * @param editModeCurrentOccSpaceTypeChecklistified the
     * editModeCurrentOccSpaceTypeChecklistified to set
     */
    public void setEditModeCurrentOccSpaceTypeChecklistified(boolean editModeCurrentOccSpaceTypeChecklistified) {
        this.editModeCurrentOccSpaceTypeChecklistified = editModeCurrentOccSpaceTypeChecklistified;
    }

    /**
     * @param occSpaceElementList the occSpaceElementList to set
     */
    public void setOccSpaceElementList(List<OccSpaceElement> occSpaceElementList) {
        this.occSpaceElementList = occSpaceElementList;
    }

    /**
     * @param occSpaceElementListSelected the occSpaceElementListSelected to set
     */
    public void setOccSpaceElementListSelected(List<OccSpaceElement> occSpaceElementListSelected) {
        this.occSpaceElementListSelected = occSpaceElementListSelected;
    }

    /**
     * @return the codeSourceList
     */
    public List<CodeSource> getCodeSourceList() {
        return codeSourceList;
    }

    /**
     * @param codeSourceList the codeSourceList to set
     */
    public void setCodeSourceList(List<CodeSource> codeSourceList) {
        this.codeSourceList = codeSourceList;
    }

    /**
     * @return the spaceTypeList
     */
    public List<OccSpaceType> getSpaceTypeList() {
        return spaceTypeList;
    }

    /**
     * @param spaceTypeList the spaceTypeList to set
     */
    public void setSpaceTypeList(List<OccSpaceType> spaceTypeList) {
        this.spaceTypeList = spaceTypeList;
    }

    /**
     * @return the spaceTypeListSelected
     */
    public List<OccSpaceType> getSpaceTypeListSelected() {
        return spaceTypeListSelected;
    }

    /**
     * @param spaceTypeListSelected the spaceTypeListSelected to set
     */
    public void setSpaceTypeListSelected(List<OccSpaceType> spaceTypeListSelected) {
        this.spaceTypeListSelected = spaceTypeListSelected;
    }

    /**
     * @return the spaceTypeBatchRequired
     */
    public boolean isSpaceTypeBatchRequired() {
        return spaceTypeBatchRequired;
    }

    /**
     * @param spaceTypeBatchRequired the spaceTypeBatchRequired to set
     */
    public void setSpaceTypeBatchRequired(boolean spaceTypeBatchRequired) {
        this.spaceTypeBatchRequired = spaceTypeBatchRequired;
    }

    /**
     * @return the currentCodeSet
     */
    public CodeSet getCurrentCodeSet() {
        return currentCodeSet;
    }

    /**
     * @param currentCodeSet the currentCodeSet to set
     */
    public void setCurrentCodeSet(CodeSet currentCodeSet) {
        this.currentCodeSet = currentCodeSet;
    }

    /**
     * @return the occSpaceElementListFiltered
     */
    public List<OccSpaceElement> getOccSpaceElementListFiltered() {
        return occSpaceElementListFiltered;
    }

    /**
     * @param occSpaceElementListFiltered the occSpaceElementListFiltered to set
     */
    public void setOccSpaceElementListFiltered(List<OccSpaceElement> occSpaceElementListFiltered) {
        this.occSpaceElementListFiltered = occSpaceElementListFiltered;
    }

    /**
     * @return the currentOccSpaceElement
     */
    public OccSpaceElement getCurrentOccSpaceElement() {
        return currentOccSpaceElement;
    }

    /**
     * @param CurrentOccSpaceElement the currentOccSpaceElement to set
     */
    public void setCurrentOccSpaceElement(OccSpaceElement CurrentOccSpaceElement) {
        this.currentOccSpaceElement = CurrentOccSpaceElement;
    }

    /**
     * @return the checklistTemplateListShowAllMunis
     */
    public boolean isChecklistTemplateListShowAllMunis() {
        return checklistTemplateListShowAllMunis;
    }

    /**
     * @return the checklistTemplateListIncludeDeactivated
     */
    public boolean isChecklistTemplateListIncludeDeactivated() {
        return checklistTemplateListIncludeDeactivated;
    }

    /**
     * @param checklistTemplateListShowAllMunis the checklistTemplateListShowAllMunis to set
     */
    public void setChecklistTemplateListShowAllMunis(boolean checklistTemplateListShowAllMunis) {
        this.checklistTemplateListShowAllMunis = checklistTemplateListShowAllMunis;
    }

    /**
     * @param checklistTemplateListIncludeDeactivated the checklistTemplateListIncludeDeactivated to set
     */
    public void setChecklistTemplateListIncludeDeactivated(boolean checklistTemplateListIncludeDeactivated) {
        this.checklistTemplateListIncludeDeactivated = checklistTemplateListIncludeDeactivated;
    }

    /**
     * @return the clonedChecklistTemplate
     */
    public OccChecklistTemplate getClonedChecklistTemplate() {
        return clonedChecklistTemplate;
    }

    /**
     * @param clonedChecklistTemplate the clonedChecklistTemplate to set
     */
    public void setClonedChecklistTemplate(OccChecklistTemplate clonedChecklistTemplate) {
        this.clonedChecklistTemplate = clonedChecklistTemplate;
    }

   
    /**
     * @return the cloneChecklistAddOrdsToTargetMuniCodebook
     */
    public boolean isCloneChecklistAddOrdsToTargetMuniCodebook() {
        return cloneChecklistAddOrdsToTargetMuniCodebook;
    }

    /**
     * @param cloneChecklistAddOrdsToTargetMuniCodebook the cloneChecklistAddOrdsToTargetMuniCodebook to set
     */
    public void setCloneChecklistAddOrdsToTargetMuniCodebook(boolean cloneChecklistAddOrdsToTargetMuniCodebook) {
        this.cloneChecklistAddOrdsToTargetMuniCodebook = cloneChecklistAddOrdsToTargetMuniCodebook;
    }

    /**
     * @return the permissionAllowAddMissingOrdsToTargetMuni
     */
    public boolean isPermissionAllowAddMissingOrdsToTargetMuni() {
        return permissionAllowAddMissingOrdsToTargetMuni;
    }

    /**
     * @param permissionAllowAddMissingOrdsToTargetMuni the permissionAllowAddMissingOrdsToTargetMuni to set
     */
    public void setPermissionAllowAddMissingOrdsToTargetMuni(boolean permissionAllowAddMissingOrdsToTargetMuni) {
        this.permissionAllowAddMissingOrdsToTargetMuni = permissionAllowAddMissingOrdsToTargetMuni;
    }

    /**
     * @return the permissionAllowChecklistEdit
     */
    public boolean isPermissionAllowChecklistEdit() {
        return permissionAllowChecklistEdit;
    }

    /**
     * @param permissionAllowChecklistEdit the permissionAllowChecklistEdit to set
     */
    public void setPermissionAllowChecklistEdit(boolean permissionAllowChecklistEdit) {
        this.permissionAllowChecklistEdit = permissionAllowChecklistEdit;
    }

    public String getOrdinanceFilterText() {
        return ordinanceFilterText;
    }

    public void setOrdinanceFilterText(String ordinanceFilterText) {
        this.ordinanceFilterText = ordinanceFilterText;
    }

    /**
     * @return the causeCandidateList
     */
    public List<OccInspectionCause> getCauseCandidateList() {
        return causeCandidateList;
    }

    /**
     * @param causeCandidateList the causeCandidateList to set
     */
    public void setCauseCandidateList(List<OccInspectionCause> causeCandidateList) {
        this.causeCandidateList = causeCandidateList;
    }

    /**
     * @return the spaceTypeToClone
     */
    public OccSpaceTypeChecklistified getSpaceTypeToClone() {
        return spaceTypeToClone;
    }

    /**
     * @param spaceTypeToClone the spaceTypeToClone to set
     */
    public void setSpaceTypeToClone(OccSpaceTypeChecklistified spaceTypeToClone) {
        this.spaceTypeToClone = spaceTypeToClone;
    }

    /**
     * @return the cloneSpaceTypeToOtherChecklist
     */
    public boolean isCloneSpaceTypeToOtherChecklist() {
        return cloneSpaceTypeToOtherChecklist;
    }

    /**
     * @param cloneSpaceTypeToOtherChecklist the cloneSpaceTypeToOtherChecklist to set
     */
    public void setCloneSpaceTypeToOtherChecklist(boolean cloneSpaceTypeToOtherChecklist) {
        this.cloneSpaceTypeToOtherChecklist = cloneSpaceTypeToOtherChecklist;
    }

    /**
     * @return the targetChecklistTemplateForSpaceTypeClone
     */
    public OccChecklistTemplate getTargetChecklistTemplateForSpaceTypeClone() {
        return targetChecklistTemplateForSpaceTypeClone;
    }

    /**
     * @param targetChecklistTemplateForSpaceTypeClone the targetChecklistTemplateForSpaceTypeClone to set
     */
    public void setTargetChecklistTemplateForSpaceTypeClone(OccChecklistTemplate targetChecklistTemplateForSpaceTypeClone) {
        this.targetChecklistTemplateForSpaceTypeClone = targetChecklistTemplateForSpaceTypeClone;
    }

    /**
     * @return the copyOrdinancesToExistingSpaceType
     */
    public boolean isCopyOrdinancesToExistingSpaceType() {
        return copyOrdinancesToExistingSpaceType;
    }

    /**
     * @param copyOrdinancesToExistingSpaceType the copyOrdinancesToExistingSpaceType to set
     */
    public void setCopyOrdinancesToExistingSpaceType(boolean copyOrdinancesToExistingSpaceType) {
        this.copyOrdinancesToExistingSpaceType = copyOrdinancesToExistingSpaceType;
    }

    /**
     * @return the cloneCopyPathway
     */
    public String getCloneCopyPathway() {
        return cloneCopyPathway;
    }

    /**
     * @param cloneCopyPathway the cloneCopyPathway to set
     */
    public void setCloneCopyPathway(String cloneCopyPathway) {
        this.cloneCopyPathway = cloneCopyPathway;
    }

    /**
     * @return the selectedSpaceTypeForCopy
     */
    public OccSpaceTypeChecklistified getSelectedSpaceTypeForCopy() {
        return selectedSpaceTypeForCopy;
    }

    /**
     * @param selectedSpaceTypeForCopy the selectedSpaceTypeForCopy to set
     */
    public void setSelectedSpaceTypeForCopy(OccSpaceTypeChecklistified selectedSpaceTypeForCopy) {
        this.selectedSpaceTypeForCopy = selectedSpaceTypeForCopy;
    }

    /**
     * @return the permissionEnableCrossCodebookAddFacility
     */
    public boolean isPermissionEnableCrossCodebookAddFacility() {
        return permissionEnableCrossCodebookAddFacility;
    }

    /**
     * @param permissionEnableCrossCodebookAddFacility the permissionEnableCrossCodebookAddFacility to set
     */
    public void setPermissionEnableCrossCodebookAddFacility(boolean permissionEnableCrossCodebookAddFacility) {
        this.permissionEnableCrossCodebookAddFacility = permissionEnableCrossCodebookAddFacility;
    }

    /**
     * @return the codeSetList
     */
    public List<CodeSet> getCodeSetList() {
        return codeSetList;
    }

    /**
     * @param codeSetList the codeSetList to set
     */
    public void setCodeSetList(List<CodeSet> codeSetList) {
        this.codeSetList = codeSetList;
    }

    /**
     * @return the selectedCodeSet
     */
    public CodeSet getSelectedCodeSet() {
        return selectedCodeSet;
    }

    /**
     * @param selectedCodeSet the selectedCodeSet to set
     */
    public void setSelectedCodeSet(CodeSet selectedCodeSet) {
        this.selectedCodeSet = selectedCodeSet;
    }
  
}
