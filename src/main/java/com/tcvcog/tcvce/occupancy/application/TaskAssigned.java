/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.occupancy.application;

import com.tcvcog.tcvce.entities.UserMuniAuthPeriod;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Subclass of TaskSpecification which is attached to an OccPeriod
 * @author pierre15
 */
public  class   TaskAssigned 
        extends TaskSpecification {
    
    private int assignmentID;
    private int occPeriodID;
    
    private TaskAssignedStatusEnum statusEnum;
    
    private int displayOrderActual;
    private int predecessorBufferActual;
    
    private LocalDateTime dateStartBy;
    private LocalDateTime dateStartActual;
    private LocalDateTime dateDueBy;
    
    private LocalDateTime dateCompletionTS;
    private LocalDateTime dateCompletionDateOfRecord;
    private UserMuniAuthPeriod completionCertificationByUMAP;
    
    private TaskAssignee assignedTo;
    
    private boolean remindersEnabled;
    private String notes;
    
    private LocalDateTime nullifiedTS;
    private UserMuniAuthPeriod nullifiedByUMAP;
    
    private LocalDateTime deactivatedTS;
    private UserMuniAuthPeriod deativatedByUMAP;
    
    private List<TaskLinkage> linkList;
    
    /**
     * Constructor of the subclass given the superclass spec
     * @param spec 
     */
    public TaskAssigned(TaskSpecification spec){
        super(spec);
        // no sublcass fields to init
        
    }
    
    /**
     * Constructor written by ChatGPT 3.5   
     * @param otherTaskAssigned 
     */
    public TaskAssigned(TaskAssigned otherTaskAssigned) {
        super(otherTaskAssigned); // Call the constructor of the parent class to copy TaskSpecification fields

        if (otherTaskAssigned != null) {
            this.assignmentID = otherTaskAssigned.getAssignmentID();
            this.occPeriodID = otherTaskAssigned.getOccPeriodID();
            this.displayOrderActual = otherTaskAssigned.getDisplayOrderActual();
            
            this.statusEnum = otherTaskAssigned.getStatusEnum();
            
            this.predecessorBufferActual = otherTaskAssigned.getPredecessorBufferActual();
            this.dateStartBy = otherTaskAssigned.getDateStartBy();
            this.dateStartActual = otherTaskAssigned.getDateStartActual();
            
            this.dateDueBy = otherTaskAssigned.getDateDueBy();
            this.dateCompletionTS = otherTaskAssigned.getDateCompletionTS();
            this.dateCompletionDateOfRecord = otherTaskAssigned.getDateCompletionDateOfRecord();
            
            this.completionCertificationByUMAP = otherTaskAssigned.getCompletionCertificationByUMAP();
            this.assignedTo = otherTaskAssigned.getAssignedTo();
            this.remindersEnabled = otherTaskAssigned.isRemindersEnabled();
            
            this.notes = otherTaskAssigned.getNotes();
            this.nullifiedTS = otherTaskAssigned.getNullifiedTS();
            this.nullifiedByUMAP = otherTaskAssigned.getNullifiedByUMAP();
            
            this.deactivatedTS = otherTaskAssigned.getDeactivatedTS();
            this.deativatedByUMAP = otherTaskAssigned.getDeativatedByUMAP();
            this.linkList = new ArrayList<>(otherTaskAssigned.getLinks());
        }
    }

    /**
     * NO arg constructor
    */
    public TaskAssigned() {
        // nothing to init
    }
    
    

    /**
     * @return the assignmentID
     */
    public int getAssignmentID() {
        return assignmentID;
    }

    /**
     * @param assignmentID the assignmentID to set
     */
    public void setAssignmentID(int assignmentID) {
        this.assignmentID = assignmentID;
    }

    /**
     * @return the occPeriodID
     */
    public int getOccPeriodID() {
        return occPeriodID;
    }

    /**
     * @param occPeriodID the occPeriodID to set
     */
    public void setOccPeriodID(int occPeriodID) {
        this.occPeriodID = occPeriodID;
    }

    /**
     * @return the displayOrderActual
     */
    public int getDisplayOrderActual() {
        return displayOrderActual;
    }

    /**
     * @param displayOrderActual the displayOrderActual to set
     */
    public void setDisplayOrderActual(int displayOrderActual) {
        this.displayOrderActual = displayOrderActual;
    }

    /**
     * @return the predecessorBufferActual
     */
    public int getPredecessorBufferActual() {
        return predecessorBufferActual;
    }

    /**
     * @param predecessorBufferActual the predecessorBufferActual to set
     */
    public void setPredecessorBufferActual(int predecessorBufferActual) {
        this.predecessorBufferActual = predecessorBufferActual;
    }

    /**
     * @return the dateStartBy
     */
    public LocalDateTime getDateStartBy() {
        return dateStartBy;
    }

    /**
     * @param dateStartBy the dateStartBy to set
     */
    public void setDateStartBy(LocalDateTime dateStartBy) {
        this.dateStartBy = dateStartBy;
    }

    /**
     * @return the dateStartActual
     */
    public LocalDateTime getDateStartActual() {
        return dateStartActual;
    }

    /**
     * @param dateStartActual the dateStartActual to set
     */
    public void setDateStartActual(LocalDateTime dateStartActual) {
        this.dateStartActual = dateStartActual;
    }

    /**
     * @return the dateDueBy
     */
    public LocalDateTime getDateDueBy() {
        return dateDueBy;
    }

    /**
     * @param dateDueBy the dateDueBy to set
     */
    public void setDateDueBy(LocalDateTime dateDueBy) {
        this.dateDueBy = dateDueBy;
    }

    /**
     * @return the dateCompletionTS
     */
    public LocalDateTime getDateCompletionTS() {
        return dateCompletionTS;
    }

    /**
     * @param dateCompletionTS the dateCompletionTS to set
     */
    public void setDateCompletionTS(LocalDateTime dateCompletionTS) {
        this.dateCompletionTS = dateCompletionTS;
    }

    /**
     * @return the dateCompletionDateOfRecord
     */
    public LocalDateTime getDateCompletionDateOfRecord() {
        return dateCompletionDateOfRecord;
    }

    /**
     * @param dateCompletionDateOfRecord the dateCompletionDateOfRecord to set
     */
    public void setDateCompletionDateOfRecord(LocalDateTime dateCompletionDateOfRecord) {
        this.dateCompletionDateOfRecord = dateCompletionDateOfRecord;
    }

    /**
     * @return the completionCertificationByUMAP
     */
    public UserMuniAuthPeriod getCompletionCertificationByUMAP() {
        return completionCertificationByUMAP;
    }

    /**
     * @param completionCertificationByUMAP the completionCertificationByUMAP to set
     */
    public void setCompletionCertificationByUMAP(UserMuniAuthPeriod completionCertificationByUMAP) {
        this.completionCertificationByUMAP = completionCertificationByUMAP;
    }

    /**
     * @return the assignedTo
     */
    public TaskAssignee getAssignedTo() {
        return assignedTo;
    }

    /**
     * @param assignedTo the assignedTo to set
     */
    public void setAssignedTo(TaskAssignee assignedTo) {
        this.assignedTo = assignedTo;
    }

    /**
     * @return the remindersEnabled
     */
    public boolean isRemindersEnabled() {
        return remindersEnabled;
    }

    /**
     * @param remindersEnabled the remindersEnabled to set
     */
    public void setRemindersEnabled(boolean remindersEnabled) {
        this.remindersEnabled = remindersEnabled;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return the nullifiedTS
     */
    public LocalDateTime getNullifiedTS() {
        return nullifiedTS;
    }

    /**
     * @param nullifiedTS the nullifiedTS to set
     */
    public void setNullifiedTS(LocalDateTime nullifiedTS) {
        this.nullifiedTS = nullifiedTS;
    }

    /**
     * @return the nullifiedByUMAP
     */
    public UserMuniAuthPeriod getNullifiedByUMAP() {
        return nullifiedByUMAP;
    }

    /**
     * @param nullifiedByUMAP the nullifiedByUMAP to set
     */
    public void setNullifiedByUMAP(UserMuniAuthPeriod nullifiedByUMAP) {
        this.nullifiedByUMAP = nullifiedByUMAP;
    }

    /**
     * @return the deactivatedTS
     */
    public LocalDateTime getDeactivatedTS() {
        return deactivatedTS;
    }

    /**
     * @param deactivatedTS the deactivatedTS to set
     */
    public void setDeactivatedTS(LocalDateTime deactivatedTS) {
        this.deactivatedTS = deactivatedTS;
    }

    /**
     * @return the deativatedByUMAP
     */
    public UserMuniAuthPeriod getDeativatedByUMAP() {
        return deativatedByUMAP;
    }

    /**
     * @param deativatedByUMAP the deativatedByUMAP to set
     */
    public void setDeativatedByUMAP(UserMuniAuthPeriod deativatedByUMAP) {
        this.deativatedByUMAP = deativatedByUMAP;
    }

    /**
     * @return the links
     */
    public List<TaskLinkage> getLinks() {
        return linkList;
    }

    /**
     * @param links the links to set
     */
    public void setLinks(List<TaskLinkage> links) {
        this.linkList = links;
    }

    /**
     * @return the statusEnum
     */
    public TaskAssignedStatusEnum getStatusEnum() {
        return statusEnum;
    }

    /**
     * @param statusEnum the statusEnum to set
     */
    public void setStatusEnum(TaskAssignedStatusEnum statusEnum) {
        this.statusEnum = statusEnum;
    }
}
