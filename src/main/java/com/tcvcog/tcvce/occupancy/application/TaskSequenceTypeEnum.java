/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.occupancy.application;

/**
 * Denotes how the holder of this enum is related to the holder's 
 * predecessor task. These values mirror generally accepted project management
 * task sequence types.
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */
public enum TaskSequenceTypeEnum {
    
    FINISHTOSTART (         "Finish-to-start", 
                            "Predecessor task must be completed to start this one"),
    
    FINISHTOFINISH (        "Finish-to-finish", 
                            "Predecessor task must be completed to complete this one"),
    
    STARTTOSTART  (         "Start-to-start", 
                            "Predecessor task must be started to start this one"),
    
    STARTTOFINISH (         "Start-to-finish", 
                            "Predecessor task must be started to finish this one (uncommon)"),
    
    SEQUENTIALUNSPECIFIED ( "Unspecified", 
                            "Unknown ordering with respect to predecessor task"),
    
    CHOICEOUTCOMEBRANCHHEAD("Branch head", 
                            "The predecessor task to this one is a choice and this "
                            + "task represents the first task in potentially a series "
                            + "of tasks based on the choice made in the predecessor task");
    
    private final String title;
    private final String description;
    
    private TaskSequenceTypeEnum(String t, String descr){
        title = t;
        description = descr;
    }
    
    public String getTitle(){
        return title;
    }
       
    public String getDescription(){
        return description;
    }
}
