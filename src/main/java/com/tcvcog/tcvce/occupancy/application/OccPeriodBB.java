/*
 * Copyright (C) 2019 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.occupancy.application;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.PropertyProfileBB;
import com.tcvcog.tcvce.session.SessionBean;
import com.tcvcog.tcvce.coordinators.*;
import com.tcvcog.tcvce.domain.*;
import com.tcvcog.tcvce.entities.*;
import com.tcvcog.tcvce.entities.occupancy.*;
import com.tcvcog.tcvce.entities.reports.ReportConfigOccPermit;
import com.tcvcog.tcvce.util.ComponentIDEnum;
import com.tcvcog.tcvce.util.Constants;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.component.UIComponent;
import jakarta.faces.component.UIInput;
import jakarta.faces.context.ExternalContext;
import jakarta.faces.context.FacesContext;
import jakarta.faces.event.ActionEvent;
import jakarta.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.PrimeFaces;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

/**
 * Backer for the OccPeriod
 *
 * @author Ellen Bascomb, JURPLEL
 */
public class OccPeriodBB
        extends BackingBeanUtils {

    final static String PERMIT_BLOCKS_STIPULATIONS = "textblock_cat_permit_stipulations";
    final static String PERMIT_BLOCKS_NOTICES = "textblock_cat_permit_notices";
    final static String PERMIT_BLOCKS_COMMENTS = "textblock_cat_permit_comments";

    final static String PERMIT_BLOCK_CATEGORY_PARAM_KEY = "permit-block-cat";
    final static String PERMIT_PERSON_LIST_PARAM_KEY = "permit-person-list";

    final static int PERMIT_FLOW_STEPS_COMPLETE = -9;

    private OccPeriod lastSavedOccPeriod;
    private OccPeriodDataHeavy currentOccPeriod;
    private PropertyUnitDataHeavy currentPropertyUnit;

    private List<OccPermitType> permitTypeCandidateList;
    private List<User> occPeriodManagerCandidateList;
    private User occPeriodManagerSelected;

    private OccPermit currentOccPermit;
    private ReportConfigOccPermit currentOccPermitConfig;

    private boolean editModeOccPermit;
    private boolean editModeOccPeriodManager;
    private boolean editModeOccPeriodType;

    private PropertyDataHeavy currentPropertyDH;

    private TextBlockPermitFieldEnum currentTextBlockPermitFieldEnum;
    private List<TextBlock> permitBlocksActiveCandidateList;
    private List<TextBlock> permitBlocksSelectedList;
    private List<TextBlock> permitBlocksFilteredList;

    private TextBlock currentTextBlock;

    private List<HumanLink> occPermitCandidateHumanLinkList;
    private List<HumanLink> occPermitSelectedHumanLinkList;
    private HumanLink occPermitSelectedHumanLink;

    private OccPermitPersonListEnum occPermitCurrentPersonLinkEnum;
    private List<OccPermitPersonListEnum> occPermitOwnerSellerOptions;
    private List<OccPermitPersonListEnum> occPermitBuyerTenantOptions;

//  *******************************
//  ************ PERMISSIONS ******
//  *******************************
    // occ periods
    private boolean permissionsAllowOccPeriodOpenEdit;
    private boolean permissionsAllowOccPeriodDeactivation;
    private boolean permissionsAllowOccPeriodTypeChange;
    private boolean permissionsAllowOccPeriodManagerChange;

    // permits (aka "certificates")
    private boolean permissionsAllowOccPermitDraft;
    private boolean permissionsAllowOccpermitIssue;
    private boolean permissionsAllowCurrentOccPermitAmendment;

//  *******************************
//  ************ WORKFLOW**********
//  *******************************
    private List<OccPeriodType> occPeriodTypeCandidateList;
    private OccPeriodType selectedOccPeriodType;

    private List<PropertyUnit> propertyUnitCandidateList;
    private PropertyUnit selectedPropertyUnit;

    private List<User> managerInspectorCandidateList;
    private User selectedManager;

    private List<OccPeriodStatusEnum> statusListForHelp;

//  *******************************
//  ************ TASKS **********
//  *******************************
    private TaskAssigned currentTaskAssigned;

//  *****************************************
//  ************ PERMIT STEPS FLOW **********
//  *****************************************
    private MenuModel permitStepsModel;
    private int permitActiveStep;
    private Map<Integer, String> permitFlowStepMap;
    private OccPermitPreAmendmentFields currentOccPermitPreAmendmentFields;

    private List<HumanLink> humanLinkListComprehensive;
    private boolean permitPersonCandidateIncludePropertyLinks;
    private boolean permitPersonCandidateIncludeUnitLinks;
    private boolean permitPersonCandidateIncludeOccPeriodLinks;

    private boolean permitPersonSearchModeEnabled;
    private boolean permitPersonAddModeEnabled;

    private String personQuickAddCertRoleString;

    private String permitIssuanceMethod;
    private List<HumanLink> permitIssuancePersonCandidateList;
    private HumanLink permitIssuancePersonTarget;
    private LocalDateTime permitIssuanceTS;
    private boolean permitIssuanceCreateFollowupEvent;
    private int permitIssuanceFollowupEventDaysFromToday;

//  *****************************************************
//  ************ HACKY PERMIT VALIDATION DATES **********
//  *****************************************************
    private LocalDate dateForValidationApplication;
    private LocalDate dateForValidationFinInit;
    private LocalDate dateForValidationFinRe;
    private LocalDate dateForValidationFinFinal;
    private LocalDate dateForValidationIssuance;
    private LocalDate dateForValidationExpiry;

    /**
     * Creates a new instance of OccPeriodSearchWorkflowBB
     */
    public OccPeriodBB() {
    }

    /**
     * Assume a zen-like state and initialize this bean
     */
    @PostConstruct
    public void initBean() {
        System.out.printf("OccPeriodSearchWorkflowBB constructed");
        OccupancyCoordinator oc = getOccupancyCoordinator();
        PropertyCoordinator pc = getPropertyCoordinator();
        UserCoordinator uc = getUserCoordinator();
        SessionBean sb = getSessionBean();

        // setup event view
        getSessionEventConductor().setSessEventsPageEventDomainRequest(EventRealm.OCCUPANCY);

        getSessionBean().setSessHumanListRefreshedList(null);
        currentOccPeriod = sb.getSessOccPeriod();
        refreshCurrentOccPeriodDataHeavy(false);
        if (currentOccPeriod != null) {
            try {
                occPeriodTypeCandidateList = oc.getOccPeriodTypeList(getSessionBean().getSessMuni(), false);
                occPeriodManagerCandidateList = oc.assembleOccPeriodManagerCandidates(getSessionBean().getSessUser());
            } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
                System.out.println(ex);
            }
            

            configurePropertyAndUnitFromCurrentOccPeriodAndSyncSessionObjects();
        }

        // Setup our choices for which type of persons we have on the permit
        occPermitOwnerSellerOptions = new ArrayList<>();
        occPermitOwnerSellerOptions.add(OccPermitPersonListEnum.PROPERTY_SELLER);
        occPermitOwnerSellerOptions.add(OccPermitPersonListEnum.CURRENT_OWNER);

        occPermitBuyerTenantOptions = new ArrayList<>();
        occPermitBuyerTenantOptions.add(OccPermitPersonListEnum.NEW_OWNER);
        occPermitBuyerTenantOptions.add(OccPermitPersonListEnum.NON_SALE_TENANTS);
        initPermissions();

        statusListForHelp = Arrays.asList(OccPeriodStatusEnum.values());
    }

    /**
     * Uses our current occ period to set this bean and the session's property
     * and unit and period
     */
    private void configurePropertyAndUnitFromCurrentOccPeriodAndSyncSessionObjects() {
        PropertyCoordinator pc = getPropertyCoordinator();
        OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            currentPropertyUnit = pc.getPropertyUnitDataHeavy(pc.getPropertyUnitDataHeavy(pc.getPropertyUnit(currentOccPeriod.getPropertyUnitID()), getSessionBean().getSessUser()), getSessionBean().getSessUser());
            getSessionBean().setSessPropertyUnit(currentPropertyUnit);
            currentPropertyDH = pc.getPropertyDataHeavy(currentPropertyUnit.getParcelKey(), getSessionBean().getSessUser());
            getSessionBean().setSessProperty(currentPropertyDH);
            setPropertyUnitCandidateList(getSessionBean().getSessProperty().getUnitList());

        } catch (IntegrationException | BObStatusException | AuthorizationException | EventException | SearchException | BlobException ex) {
            System.out.println(ex);
        }

    }

    /**
     * sets permissions bits
     */
    private void initPermissions() {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        PermissionsCoordinator permCoor = getPermissionsCoordinator();

        // set our permissions bits from the coordinator
        permissionsAllowOccPeriodOpenEdit = oc.permissionsCheckpointOpenOccPeriod(getSessionBean().getSessUser());
        permissionsAllowOccPermitDraft = oc.permissionsCheckpointOccPermitDraft(getSessionBean().getSessUser());
        permissionsAllowOccpermitIssue = oc.permissionsCheckpointOccPermitIssueAmend(getSessionBean().getSessUser());
        permissionsAllowOccPeriodDeactivation = permCoor.permissionsCheckpointCreatorEditDeac(currentOccPeriod, getSessionBean().getSessUser());
        permissionsAllowOccPeriodManagerChange = oc.permissionsCheckpointOccPeriodManagerUpdate(getSessionBean().getSessUser(), lastSavedOccPeriod);
        permissionsAllowOccPeriodTypeChange = oc.permissionsCheckpointOccPeriodTypeUpdate(getSessionBean().getSessUser());
    }

    /**
     * Gets a fresh copy of this period's parent unit and property data heavy
     * subclasses; I also inject these into the session for freshness's sake
     */
    public void refreshCurrentPeriodParentObjects() {
        PropertyCoordinator pc = getPropertyCoordinator();
        try {

            if (currentPropertyDH != null) {
                currentPropertyDH = pc.assemblePropertyDataHeavy(currentPropertyDH, getSessionBean().getSessUser());
                getSessionBean().setSessProperty(currentPropertyDH);
            }
            if (currentPropertyUnit != null) {
                currentPropertyUnit = pc.getPropertyUnitDataHeavy(pc.getPropertyUnit(currentPropertyUnit.getUnitID()), getSessionBean().getSessUser());
                getSessionBean().setSessPropertyUnit(currentPropertyUnit);
            }
        } catch (AuthorizationException | BObStatusException | BlobException | EventException | IntegrationException | SearchException ex) {
            System.out.println(ex);
        }

    }

    /**
     * Gets an updated instance of the current OccPeriod
     *
     * @param triggerEventReload when true, I'll inject this occ period's events
     * into the managed event list for slurping up by the event subsystem
     */
    public void refreshCurrentOccPeriodDataHeavy(boolean triggerEventReload) {
        if (currentOccPeriod != null) {
            currentOccPeriod = getSessionOccupancyConductor().registerSessionOccPeriod(currentOccPeriod);
            if (triggerEventReload) {
                getSessionEventConductor().setSessEventListForRefreshUptake(currentOccPeriod.getEventList());
            }
        }
    }

    /**
     * Listener for user requests to certify a field on the OccPeriod
     */
    public void certifyOccPeriodField() {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        FacesContext context = getFacesContext();
        String field = context.getExternalContext().getRequestParameterMap().get("fieldtocertify");

        System.out.println("occPeriodBB.certifyOccPeriodField | field: " + field);

        UserAuthorized u = getSessionBean().getSessUser();
        LocalDateTime now = LocalDateTime.now();

        // Set actual values
        switch (field) {
            case "occperiodtype":
                if (currentOccPeriod.getPeriodTypeCertifiedBy() == null) {
                    currentOccPeriod.setPeriodTypeCertifiedBy(u);
                    currentOccPeriod.setPeriodTypeCertifiedTS(now);
                } else {
                    currentOccPeriod.setPeriodTypeCertifiedBy(null);
                    currentOccPeriod.setPeriodTypeCertifiedTS(null);
                }
                break;
            case "startdate":
                if (currentOccPeriod.getStartDateCertifiedBy() == null) {
                    currentOccPeriod.setStartDateCertifiedBy(u);
                    currentOccPeriod.setStartDateCertifiedTS(now);
                } else {
                    currentOccPeriod.setStartDateCertifiedBy(null);
                    currentOccPeriod.setStartDateCertifiedTS(null);
                }
                break;
            case "enddate":
                if (currentOccPeriod.getEndDateCertifiedBy() == null) {
                    currentOccPeriod.setEndDateCertifiedBy(u);
                    currentOccPeriod.setEndDateCertifiedTS(now);
                } else {
                    currentOccPeriod.setEndDateCertifiedBy(null);
                    currentOccPeriod.setEndDateCertifiedTS(null);
                }
                break;
            default:
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Error! Unable to certify field", ""));
        }

        saveOccPeriodChanges(null);
    }

    /**
     * Listener for the requests to authorize or unauthorize the OccPeriod
     */
    public void toggleOccPeriodAuthorization() {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            oc.toggleOccPeriodAuthorization(currentOccPeriod, getSessionBean().getSessUser());
            if (currentOccPeriod.getAuthorizedBy() != null) {
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Success! Occupancy period ID " + currentOccPeriod.getPeriodID()
                        + " is now authorized and permits can be generated.", ""));
            } else {
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Occupancy period ID " + currentOccPeriod.getPeriodID()
                        + " has been successfully deauthorized.", ""));
            }
        } catch (AuthorizationException | BObStatusException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
        }
    }

    /**
     * utility pass through method to be called when loading Occperiod advanced
     * settings; As of Feb 2024 upgrade there was no general update, only
     * field-specific ones: manager, type
     *
     */
    public void updateOccPeriodInitialize() {

    }

    /**
     * Listener for user requests to go into our out of update mode for period
     * type
     *
     * @param ev
     */
    public void onToggleOccPeriodUpdateManager(ActionEvent ev) {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        if (editModeOccPeriodManager) {
            try {
                oc.updateOccPeriodManager(currentOccPeriod, occPeriodManagerSelected, getSessionBean().getSessUser());
                refreshCurrentOccPeriodDataHeavy(true);
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Permit file mnager change success!", ""));
                getSessionEventConductor().refreshCalendarAndFollowupBacklog(null);
                getSessionEventConductor().setSessEventListForRefreshUptake(currentOccPeriod.getEventList());
            } catch (IntegrationException | AuthorizationException | BObStatusException | EventException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        ex.getMessage(), ""));
            }
        }
        editModeOccPeriodManager = !editModeOccPeriodManager;
    }

    /**
     * Listener to abort the type change operation
     *
     * @param ev
     */
    public void onOccperiodUpdateManagerAbortButtonChange(ActionEvent ev) {
        System.out.println("OccPeriodBB.onOccperiodUpdateManagerAbortButtonChange");
        editModeOccPeriodManager = false;

    }

    /**
     * Listener for user requests to go into our out of update mode for period
     * type
     *
     * @param ev
     */
    public void onToggleOccPeriodUpdateType(ActionEvent ev) {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        if (editModeOccPeriodType) {
            try {
                oc.updateAnOccPeriodsType(currentOccPeriod, selectedOccPeriodType, getSessionBean().getSessUser());
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Permit file type change success!", ""));
                refreshCurrentOccPeriodDataHeavy(false);
            } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        ex.getMessage(), ""));
            }
        }
        editModeOccPeriodType = !editModeOccPeriodType;
    }

    /**
     * Listener to abort the type change operation
     *
     * @param ev
     */
    public void onOccperiodUpdateTypeAbortButtonChange(ActionEvent ev) {
        System.out.println("OccPeriodBB.onOccperiodUpdateTypeAbortButtonChange");
        editModeOccPeriodType = false;

    }

    /**
     * This method attempts to update the database entry for the
     * currentOccPeriod. It will fail in certain conditions, in which case the
     * currentOccPeriod is returned to a backup made before any current unsaved
     * changes.
     *
     * @param ev
     */
    public void saveOccPeriodChanges(ActionEvent ev) {
        OccupancyCoordinator oc = getOccupancyCoordinator();

        try {
            oc.editOccPeriod(currentOccPeriod, getSessionBean().getSessUser());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Save successful on OccPeriod ID: " + currentOccPeriod.getPeriodID(), ""));
            System.out.println("occPeriodBB.saveOccPeriodChanges successful");
            refreshCurrentOccPeriodDataHeavy(editModeOccPermit);
            
        } catch (IntegrationException | BObStatusException ex) {
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
            System.out.println("occPeriodBB.saveOccPeriodChanges failure");

            // Restore working copy of occ period to last working one if saving to database fails.
            
        }
    }

    

    /**
     * Listener for user request to map the current occ period to a a new
     * property unit
     */
    public void updatePeriodPropUnit() {
//        OccupancyCoordinator oc = getOccupancyCoordinator();
//        try {
//            oc.updateOccPeriodPropUnit(currentOccPeriod, getSelectedPropertyUnit());
//             getFacesContext().addMessage(null,
//                new FacesMessage(FacesMessage.SEVERITY_INFO,
//                "The current occupancy period has been assigned to property unit ID " + getSelectedPropertyUnit().getUnitID(), ""));
//        } catch (IntegrationException ex) {
//            System.out.println(ex);
//             getFacesContext().addMessage(null,
//                new FacesMessage(FacesMessage.SEVERITY_ERROR,
//                ex.getMessage(), ""));
//        }
//        refreshCurrentOccPeriodDataHeavy();
    }

    /**
     * Listener for requests to go view the property profile of a property
     * associated with the given case largely copied from CECaseSearchProfileBB
     * (Maybe this should be in BackingBeanUtils?)
     *
     * @return
     */
    public String exploreProperty() {

        try {
            return getSessionBean().navigateToPageCorrespondingToObject(getSessionBean().getSessProperty());
        } catch (BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
        }
        return "";
    }

    /**
     * Listener for cancel requests on occ period deac
     *
     * @param ev
     */
    public void onOccPeriodDeacCancel(ActionEvent ev) {
        System.out.println("cancel occ period deac");
    }

    /**
     * Listener for user requests to start deac process
     *
     * @param ev
     */
    public void onOccPeriodDeactivateInit(ActionEvent ev) {
        System.out.println("OccPeriodBB. OccPeriodDeacInit");

    }

    /**
     * Listener for user requests to deactivate the occ period
     *
     * @return reloads page on logic failure, otherwise goes to prop profile of
     * the host property
     */
    public String onOccperiodDeactivateCommit() {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            oc.deactivateOccPeriod(currentOccPeriod, getSessionBean().getSessUser());
            return getSessionBean().navigateToPageCorrespondingToObject(getSessionBean().getSessProperty());
        } catch (BObStatusException | AuthorizationException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.toString(), ""));
            return "";
        }
    }

    /**
     * Listener for user requests to view or add blobs to this period
     *
     * @param ev
     */
    public void manageBlobsOnOccPeriod(ActionEvent ev) {
        try {
            getSessionBean().setAndRefreshSessionBlobHolderAndBuildUpstreamPool(currentOccPeriod);
        } catch (BObStatusException | BlobException | IntegrationException ex) {
            System.out.println(ex);

        }

    }

    /**
     * Special getter that asks the session for its refreshed list and injects
     * into the current occ period for viewing
     *
     * @return
     */
    public List<FieldInspectionLight> getManagedFieldInspectionList() {
        List<FieldInspectionLight> filist = getSessionInspectionConductor().getSessFieldInspectionListForRefresh();
        if (currentOccPeriod != null) {
            if (filist != null && !filist.isEmpty()) {
                currentOccPeriod.setInspectionList(filist);
                getSessionInspectionConductor().setSessFieldInspectionListForRefresh(null);
                refreshCurrentOccPeriodDataHeavy(false);
                return currentOccPeriod.getInspectionList();
            } else {
                return currentOccPeriod.getInspectionList();
            }
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * Special wrapper getter around the current occperiod's human link list
     * that asks the session for a new link list on table load that might occur
     * during a link edit operation
     *
     * @return the new human link list
     */
    public List<HumanLink> getManagedHumanLinkList() {
        List<HumanLink> hll = getSessionBean().getSessHumanListRefreshedList();
        if (hll != null) {
            System.out.println("OccPeriodBB.getManagedHumanLinkList | session list size: " + hll.size());
            currentOccPeriod.sethumanLinkList(hll);
            // clear our refreshed list
            getSessionBean().setSessHumanListRefreshedList(null);
        } else {
            System.out.println("OccPeriodBB.getManagedHumanLinkList | emtpy session human links list");
        }
        return currentOccPeriod.gethumanLinkList();
    }

    // *************************************************************************
    // ********************* OCC PERMITS   ************************************
    // *************************************************************************
    /**
     * Listener for user requests to start a new occ permit
     *
     * @param ev
     */
    public void onOccPermitInitButtonChange(ActionEvent ev) {
        OccupancyCoordinator oc = getOccupancyCoordinator();

        try {
            currentOccPermit = oc.getOccPermitSkeleton(getSessionBean().getSessUser(), getSessionBean().getSessMuni());
            configurePermitTypeListFromPeriodType();
        } catch (AuthorizationException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));

        }
        System.out.println("OccPeriodBB.onOccPermitInitButtonChange");
    }

    /**
     * Sets up available permit types based on this period's type
     */
    private void configurePermitTypeListFromPeriodType() {
        if (currentOccPeriod != null
                && currentOccPeriod.getPeriodType() != null
                && currentOccPeriod.getPeriodType().getPermitTypes() != null
                && !currentOccPeriod.getPeriodType().getPermitTypes().isEmpty()) {
            permitTypeCandidateList = currentOccPeriod.getPeriodType().getPermitTypes();
            System.out.println("OccPeriodBB.configurePermitTypeListFromPeriodType | loaded types: " + permitTypeCandidateList.size());
        } else {
            System.out.println("OccPeriodBB.configurePermitTypeListFromPeriodType | unable to load permit types from period ");
        }
    }

    /**
     * Convenience method for turning on help for user when no certificate types
     * appear
     *
     * @return
     */
    public boolean isRenderHelpPermitTypeCandidListingEmpty() {
        boolean render = permitTypeCandidateList == null || (permitTypeCandidateList != null && permitTypeCandidateList.isEmpty());
        return render;
    }

    /**
     * Listener for user requests to be done selecting persons. Since all the
     * other operations are AJAXED, we don't need to do anything
     *
     * @param ev
     */
    public void onOccPermitDoneWithPersonsSelection(ActionEvent ev) {
        System.out.println("OccPeriodBB.onOccPermitDoneWithPersonsSelection");
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Done with person assignments!", ""));
    }

    /**
     * Writes a skeleton occ permit into the db; This method was edited to cope
     * with an edge case of a user in the flow returning to step 1 after having
     * already written the skeleton to the DB, in which case we have an update
     * not insert operation, and all we'll update is the permit type
     *
     * @param ev
     */
    public void onOccPermitInitCommitButtonChange(ActionEvent ev) {
        OccupancyCoordinator oc = getOccupancyCoordinator();

        int freshPermitID;
        try {
            if (currentOccPermit != null && currentOccPermit.getPermitID() == 0) {
                freshPermitID = oc.insertOccPermit(currentOccPermit, currentOccPeriod, getSessionBean().getSessUser());
                currentOccPermit.setPermitID(freshPermitID);
            } else {
                // update our type only
                oc.updateOccPermit(currentOccPermit, getSessionBean().getSessUser());
            }
            refreshCurrentOccPermit(null);
            // let's wait on this until we have a type
            configureCurrentOccPermitForOfficerReview();
            System.out.println("OccPeriodBB.onOccPermitInitCommitButtonChange | current occ permit ID: " + currentOccPermit.getPermitID());
        } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }

    }

    /**
     * Grabs a new copy of the curent occ permit, if it's ID is not zero
     *
     * @param ev
     */
    public void refreshCurrentOccPermit(ActionEvent ev) {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        if (currentOccPermit != null) {
            try {
                currentOccPermit = oc.getOccPermit(currentOccPermit.getPermitID(), getSessionBean().getSessUser());
            } catch (IntegrationException | BObStatusException | BlobException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                ex.getMessage(), ""));

            }
        }
    }

    /**
     * Lister to remove a code source from list
     *
     * @param src
     */
    public void onRemoveCodeSourceFromPermit(CodeSource src) {
        if (currentOccPermit != null && currentOccPermit.getIssuingCodeSourceList() != null) {
            currentOccPermit.getIssuingCodeSourceList().remove(src);
        }
    }

    /**
     * Listener to add a code source to permit
     *
     * @param src
     */
    public void onAddCodeSourceToPermit(CodeSource src) {
        if (currentOccPermit != null && currentOccPermit.getIssuingCodeSourceList() != null) {
            currentOccPermit.getIssuingCodeSourceList().add(src);
        }
    }

    /**
     * Listener for user requests to init code source changes to the DB
     *
     * @param ev
     */
    public void onCodeSourceEditInitButtonChange(ActionEvent ev) {
        System.out.println("OccPeriodBB.onCodeSourceEditInitButtonChange");

    }

    /**
     * Listener for user requests to write the code source changes to the DB
     *
     * @param ev
     */
    public void onCodeSourceEditCommitButtonChange(ActionEvent ev) {
        System.out.println("OccPeriodBB.onCodeSourceEditCommitButtonChange");
        saveCurrentPermitDyanmicFieldsAndRefreshPermitAndOPDH();
        onOccPermitGenerateStaticFields(null);

    }

    /**
     * Listener for user requests to abort the code source edit operation
     *
     * @param ev
     */
    public void onCodeSourceEditAbortButtonChange(ActionEvent ev) {
        System.out.println("OccPeriodBB.onCOdeSourceEditAbortButtonChange");
    }

    /**
     * Listener for user requests to init edit property/parcel info
     *
     * @param ev
     */
    public void onPermitPropertyInfoEditInitButtonChange(ActionEvent ev) {
        System.out.println("OccPeriodBB.onPermitPropertyInfoEditInitButtonChange");

    }

    /**
     * Listener for user requests to commit edit property/parcel info
     *
     * @param ev
     */
    public void onPermitPropertyInfoEditCommitButtonChange(ActionEvent ev) {
        System.out.println("OccPeriodBB.onPermitPropertyInfoEditCommitButtonChange");

    }

    /**
     * Listener for user requests to abort edit property/parcel info
     *
     * @param ev
     */
    public void onPermitPropertyInfoEditAbortButtonChange(ActionEvent ev) {
        System.out.println("OccPeriodBB.onPermitPropertyInfoEditAbortButtonChange");

    }

    /**
     * Internal organ for asking the coordinator to make sensible occ permit
     * injections such as dates of various inspections and human links if they
     * can be deciphered.
     *
     */
    private void configureCurrentOccPermitForOfficerReview() {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        // SHIP all we have to the coordinator for logic check and AUDIT
        // for PERMIT ISSUANCE CLEARANCE CLARENCE, WHAT?
        try {
            currentOccPermit = oc.occPermitAssignSensibleDynamicValuesAndAudit(currentOccPermit,
                    currentOccPeriod,
                    getSessionBean().getSessUser(),
                    currentPropertyDH,
                    getSessionBean().getSessMuni());
        } catch (BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
        System.out.println("OccPeriodBB.configureCurrentOccPermitForOfficerReview | DONE with preliminary config");
    }

    /**
     * Listener for user requests to view the config fields on an occ permit
     *
     * @param permit
     */
    public void onOccPermitViewConfigLinkClick(OccPermit permit) {
        currentOccPermit = permit;
        configureCurrentOccPermitForOfficerReview();
    }

    /**
     * listener to view events on a given permit
     *
     * @param permit
     */
    public void onOccPermitViewEvents(OccPermit permit) {
        currentOccPermit = permit;
    }

    /**
     * Listener for users to start the office change process
     *
     * @param ev
     */
    public void onOccPermitChangeOfficerInit(ActionEvent ev) {
        System.out.println("onOccPermitChangeOfficerInit");
    }

    /**
     * Listener for users to confirm new officer choice
     *
     * @param ev
     */
    public void onOccPermitChangeOfficerCommit(ActionEvent ev) {
        System.out.println("onOccPermitChangeOfficerCommit | new officer: " + currentOccPermit.getIssuingOfficer().getUsername());
        // if we're anywhere in the flow but at the beginning, we're in our flow and can write our new fields
        if (permitActiveStep != 0) {
            saveCurrentPermitDyanmicFieldsAndRefreshPermitAndOPDH();
            onOccPermitGenerateStaticFields(null);
        }
    }

    /**
     * Sends the current occ period and permit with all the goodies injected to
     * the coordinator for static String extraction
     *
     * @param ev
     */
    public void onOccPermitGenerateStaticFields(ActionEvent ev) {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            oc.occPermitPopulateStaticFieldsFromDynamicFieldsAndWriteToDB(currentOccPeriod, currentOccPermit, getSessionBean().getSessUser(), getSessionBean().getSessMuni());
            refreshCurrentOccPermit(null);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Finalized Occ permit No " + currentOccPermit.getReferenceNo(), ""));
        } catch (BObStatusException | IntegrationException | BlobException | SearchException | AuthorizationException | EventException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
    }

    /**
     * Begins finalization process
     *
     * @param ev
     */
    public void onOccPermitAuditForFinalization(ActionEvent ev) {
        System.out.println("OccPeriodBB.onOccPermitAuditForFinalization");
        OccupancyCoordinator oc = getOccupancyCoordinator();
        refreshCurrentOccPermit(null);
        try {
            if (oc.occPermitAuditForFinalization(currentOccPermit, getSessionBean().getSessUser())) {
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "This permit PASSED and is ready for finalization", ""));

            } else {
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "This permit did NOT pass audit!", ""));

            }
        } catch (BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            ex.getMessage(), ""));
        }
    }

    /**
     * Empty listener to start the finalization commit process
     *
     * @param ev
     */
    public void onOccPermitFinalizeInitButtonChange(ActionEvent ev) {
        System.out.println("OccPeriodBB.onOccPermitFinalizeInitButtonChange");
    }

    /**
     * Empty listener to abort the finalization commit process
     *
     * @param ev
     */
    public void onOccPermitFinalizeAbortButtonChange(ActionEvent ev) {
        System.out.println("OccPeriodBB.onOccPermitFinalizeAbortButtonChange");
    }

    /**
     * Writes finalization fields to DB on an occ permit
     *
     * @param ev
     */
    public void onOccPermitFinalizeCommitButtonChange(ActionEvent ev) {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            if (!currentOccPermit.isCurrentPermitAmendmentMode()) {
                oc.occPermitFinalize(currentOccPermit, currentOccPeriod, getSessionBean().getSessUser(), getSessionBean().getSessMuni());
            } else {
                onOccPermitAmendCommit(null);
            }

            refreshCurrentOccPermit(null);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Finalized Occ permit ref Number: " + currentOccPermit.getReferenceNo(), ""));
            refreshCurrentOccPeriodDataHeavy(true);
            routeToPermitFlowStep(6);
        } catch (BObStatusException | IntegrationException | AuthorizationException | EventException | BlobException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
    }

    /**
     * Writes finalization fields to DB on an occ permit
     *
     * @param ev
     */
    public void onOccPermitFinalizeOverride(ActionEvent ev) {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            oc.occPermitFinalizeOverrideAudit(currentOccPermit, currentOccPeriod, getSessionBean().getSessUser(), getSessionBean().getSessMuni());
            refreshCurrentOccPermit(null);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Finalized Occ permit No " + currentOccPermit.getReferenceNo(), ""));
            refreshCurrentOccPeriodDataHeavy(true);
        } catch (BObStatusException | IntegrationException | AuthorizationException | EventException | BlobException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
    }

    /**
     * ***********************************************************************
     * *********************** AMENDMENTS *************************************
     * ************************************************************************
     */
    /**
     * Starts the amendment process which just checks to see that the given
     * OccPermit can actually be amended
     *
     * @param permit
     */
    public void onOccPermitAmendInit(OccPermit permit) {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        currentOccPermit = permit;
        permissionsAllowCurrentOccPermitAmendment = oc.allowPermitAmendment(permit, getSessionBean().getSessUser());

    }

    /**
     * Listener for users who activate the "do you want to amend?" dialog and
     * want to cancel before beginning the actual amendment process. There's no
     * action needed here.
     *
     * @param ev
     */
    public void onOccPermitAmendInitCancel(ActionEvent ev) {
        System.out.println("OccPeriodBB.onOccPermitAmendInitCancel");
    }

    /**
     * listener for second step of the amendment process--once the permit CAN be
     * amended, this method is called from the dialog which will then actually
     * timestamp the permit as being in amendment process. My coordinator call
     * will check again that this permit can be amended by the current user.
     *
     * After this method is called, the currentOccPermit cannot be refreshed
     * until after amendment is finalized
     *
     * @param ev
     */
    public void onOccPermitAmendCommence(ActionEvent ev) {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            // this method will give us our amendment-in-process permit
            currentOccPermit = oc.permitAmendInit(currentOccPermit, getSessionBean().getSessUser());
            occPermitBuildAndStorePreAmendmentFields();
            currentOccPermit.setDynamicLastStep(OccPermitFlowStepEnum.PERSONS.getStepNumber1Based());
            onOccPermitInitFlow(null);
            routeToPermitFlowStep(OccPermitFlowStepEnum.PERSONS);
            refreshCurrentOccPeriodDataHeavy(false);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "You are now amending permit reference no.:" + currentOccPermit.getReferenceNo(), ""));
        } catch (BObStatusException | BlobException | IntegrationException | AbstractMethodError ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
    }

    /**
     * Asks the coordinator to build us a pre-amendment fields which this bean
     * will save for use during the amendment process.
     */
    private void occPermitBuildAndStorePreAmendmentFields() {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        if (currentOccPermit != null) {
            currentOccPermitPreAmendmentFields = oc.occPermitExtractPreAmendmentFields(currentOccPermit);
        }
    }

    /**
     * listener for users to continue the amendment process once initiated.
     * We'll drop the user right into the person config step
     *
     * @param permit
     */
    public void onOccPermitAmendContinue(OccPermit permit) {
        System.out.println("OccPeriodBB.onOccPermitAmendContinue | continue amendment of cert ID: " + permit.getPermitID());
        currentOccPermit = permit;
        occPermitBuildAndStorePreAmendmentFields();
        onOccPermitInitFlow(null);
        // short curcuit the normal routing process to avoid refreshes
        gotoPermitFlowStep3Persons(null);
    }

    /**
     * listener for users who have started the actual amendment process, and are
     * in amendment mode, and do not wish to proceed with the amendment
     *
     * @param ev
     */
    public void onOccPermitAmendAbort(ActionEvent ev) {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        System.out.println("OccPeriodBB.onOccPermitAmendQuit");
        try {
            oc.permitAmendAbort(currentOccPermit, getSessionBean().getSessUser());
            currentOccPermitPreAmendmentFields = null;
            refreshCurrentOccPermit(null);
            refreshCurrentOccPeriodDataHeavy(false);
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }
    }

    /**
     * Listener for the user's request to complete amending permit person links
     * and go directly to step 5, review. This is a short circuit method that
     * hops the remarks step. Most importantly, I'll populate this permit's
     * static person fields BUT NOT write them to the DB as we do during
     * conventional permit config flow. SO, the permit cannot be refreshed
     * during this process.
     *
     * Then I'll set our active step to REVIEW and bypass all the usual wiring
     *
     * @param ev
     */
    public void onPermitPersonAmendmentComplete(ActionEvent ev) {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            oc.occPermitPopulateAmendableStaticFieldsWithoutDBWrite(currentOccPermit, getSessionBean().getSessUser());
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
        }

        permitActiveStep = OccPermitFlowStepEnum.REVIEW.getStepNumber1Based();

        System.out.println("OccPeriodBB.onPermitPersonAmendmentComplete");

    }

    /**
     * Writes the amended permit values to the DB amendment process. Called by
     * the finalization confirm dialog
     *
     * @param ev
     */
    public void onOccPermitAmendCommit(ActionEvent ev) {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            // inject our pre-amendment fields
            currentOccPermit.setPreAmendmentFields(currentOccPermitPreAmendmentFields);
            oc.permitAmendCommit(currentOccPermit, getSessionBean().getSessUser());
            // okay now we can actually refresh
            refreshCurrentOccPermit(ev);
            refreshCurrentOccPeriodDataHeavy(true);
            gotoPermitFlowStep6Issue(null);
            System.out.println("OccPeriodBB.onOccPermitAmendCommit");
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "You successfully amended permit reference no.:" + currentOccPermit.getReferenceNo(), ""));
        } catch (BObStatusException | AuthorizationException | IntegrationException | AbstractMethodError ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
    }

    /**
     * ***********************************************************************
     * *********************** PERMIT Admin override
     * *******************************
     * ************************************************************************
     */
    /**
     * Toggles the admin override on a given permit
     *
     * @param ev
     */
    public void onTogglePermitAdminOverride(ActionEvent ev) {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        if (currentOccPermit != null) {
            try {
                System.out.println("OccPeriodBB.onTogglePermitAdminOverride");
                oc.occPermitToggleAdminOverride(currentOccPermit, getSessionBean().getSessUser());
                refreshCurrentOccPermit(null);
                refreshCurrentOccPeriodDataHeavy(false);
            } catch (BObStatusException | AuthorizationException | IntegrationException ex) {
                System.out.println(ex);
            } 
        }

    }

    /**
     * ***********************************************************************
     * *********************** PERMIT REVOKE *******************************
     * ************************************************************************
     */
    /**
     * Listener for user to start revocation process
     *
     * @param permit
     */
    public void onOccPermitRevokeInit(OccPermit permit) {
        System.out.println("OccPeriodBB.onOccPermitRevokeInit | permitID: " + permit.getPermitID());
        currentOccPermit = permit;
    }

    public void onOccPermitRevokeAbort(ActionEvent ev) {
        System.out.println("OccPeriodBB.onOccPermitRevokeAbort");

    }

    /**
     * Listener for user requests to commit the revocation operation
     *
     * @param ev
     */
    public void onOccPermitRevokeCommit(ActionEvent ev) {
        OccupancyCoordinator oc = getOccupancyCoordinator();

        try {
            System.out.println("OccPeriodBB.onOccPermitRevokeInit");
            oc.permitRevokeCommit(currentOccPermit, currentOccPeriod, getSessionBean().getSessUser());
            refreshCurrentOccPermit(null);
            refreshCurrentOccPeriodDataHeavy(true);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Certificate successfully revoked with reference no. " + currentOccPermit.getReferenceNo(), ""));
        } catch (BObStatusException | AuthorizationException | IntegrationException | EventException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
    }

    /**
     * ***********************************************************************
     * *********************** PERMIT DEAC ETC *******************************
     * ************************************************************************
     */
    /**
     * listener for user requests to deac permit from the permit table
     *
     * @param permit
     */
    public void onOccPermitDeacInit(OccPermit permit) {
        currentOccPermit = permit;
    }

    /**
     * Starts the deactivation process
     *
     * @param ev
     */
    public void onOccPermitDeactivateInit(ActionEvent ev) {
        System.out.println("OccPeriodBB.onOccPermitDeactivateInit");

    }

    /**
     * Listener for user requests to deactivate the current non-finalized occ
     * permit
     *
     * @param ev
     */
    public void onOccPermitDeactivateCommit(ActionEvent ev) {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        if (currentOccPermit != null) {
            currentOccPermit.setDeactivatedBy(getSessionBean().getSessUser());
            currentOccPermit.setDeactivatedTS(LocalDateTime.now());
            try {
                oc.updateOccPermit(currentOccPermit, getSessionBean().getSessUser());
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Occ Permit ID " + currentOccPermit.getPermitID() + " has been deactivated! ", ""));
                refreshCurrentOccPermit(null);
                refreshCurrentOccPeriodDataHeavy(true);
            } catch (BObStatusException | IntegrationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                ex.getMessage(), ""));

            }
        }
    }

    /**
     * Redirects user to page to print occ permit
     *
     * @param op
     * @return
     */
    public String onOccPermitPrintLinkClick(OccPermit op) {
        currentOccPermit = op;
        try {
            System.out.println("OccPeriodBB.onOccPermitPrintLinkClick: request to print permit ID: " + op.getPermitID());
            configureOccPermitForPrinting();
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
            return "errorPermitPrint";
        }
        return "occPermit";
    }

    /**
     * Internal organ for prepping the current permit for printing
     *
     * @throws IntegrationException
     * @throws BObStatusException
     */
    private void configureOccPermitForPrinting() throws IntegrationException, BObStatusException {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        currentOccPermitConfig = oc.getOccPermitReportConfigDefault(currentOccPermit, currentOccPeriod, currentPropertyUnit, getSessionBean().getSessUser());
        getSessionBean().setReportConfigOccPermit(currentOccPermitConfig);

    }

    /**
     * Listener to view the current permit in a new tab usign programatic
     * execution of client side JS
     *
     * @param ev
     */
    public void onOccPermitPrintExecute(ActionEvent ev) {
        try {
            configureOccPermitForPrinting();
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }

        // with help from ChatGPT 3.5 on 23FEB24
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();

        String baseURL = request.getRequestURL().toString().replace(request.getRequestURI(), request.getContextPath());
        String permitURL = baseURL + "/restricted/reports/reportOccPermit.xhtml";
        PrimeFaces.current().executeScript("window.open('" + permitURL + "', '_blank');");
    }

    /**
     * Listener for user requests to look at an individual text block for a
     * permit
     *
     * @param tb
     */
    public void onOccPermitViewTextBlock(TextBlock tb) {
        currentTextBlock = tb;
    }

    /**
     * Listener for user requests to remove a text block from one of the three
     * fields that can hold the contents of a text bock. This method asks the
     * request param for the value of the key: permit-block-cat
     *
     * @param tb
     */
    public void onOccPermitRemoveTextBlockFromQueue(TextBlock tb) {

        String cat = getFacesContext().getExternalContext().getRequestParameterMap().get(PERMIT_BLOCK_CATEGORY_PARAM_KEY);
        currentTextBlockPermitFieldEnum = TextBlockPermitFieldEnum.valueOf(cat);
        System.out.println("OccPeriodBB.onOccPermitRemoveTextBlockFromQueue | block enum selected: " + currentTextBlockPermitFieldEnum.getLabel());
        switch (currentTextBlockPermitFieldEnum) {
            case STIPULATIONS:
                currentOccPermit.getTextBlocks_stipulations().remove(tb);
                break;
            case NOTICES:
                currentOccPermit.getTextBlocks_notice().remove(tb);
                break;
            case COMMENTS:
                currentOccPermit.getTextBlocks_comments().remove(tb);
                break;
        }

    }

    /**
     * Listener for user requests to start the block choice process This method
     * asks the request param for the value of the key: permit-block-cat which
     * it then uses to figure out which candidate block list to load into the
     * primary active list.
     *
     * @param ev
     */
    public void onOccPermitExtractBlockListEnumAndConfigureCandidatesLinkClick(ActionEvent ev) {
        SystemCoordinator sc = getSystemCoordinator();

        String cat = getFacesContext().getExternalContext().getRequestParameterMap().get(PERMIT_BLOCK_CATEGORY_PARAM_KEY);
        currentTextBlockPermitFieldEnum = TextBlockPermitFieldEnum.valueOf(cat);
        System.out.println("OccPeriodBB.onOccPermitChooseBlocksForPermitLinkClick | block enum selected: " + currentTextBlockPermitFieldEnum.getLabel());
        if (permitBlocksActiveCandidateList == null) {
            permitBlocksActiveCandidateList = new ArrayList<>();
        }
        permitBlocksActiveCandidateList.clear();
        if (currentTextBlockPermitFieldEnum != null) {
            try {
                permitBlocksActiveCandidateList.addAll(sc.getTextBlockList(
                        sc.getTextBlockCategory(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString(currentTextBlockPermitFieldEnum.getDbFixedValueLookup()))),
                        getSessionBean().getSessMuni()));
                System.out.println("OccPeriodBB.onOccPermitExtractBlockListEnumAndConfigureCandidatesLinkClick | candidate list: " + permitBlocksActiveCandidateList.size());
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
            }
        }
    }

    /**
     * Listener for user requests to add the selected blocks to the correct
     * permit field. This method uses the value of the bean's
     * currentTextBlockPermitFieldEnum to figure out which list to add the
     * selected ones to.
     *
     * This method supports the legacy, non dynamic field saving version of the
     * permit building process and should be preserved for backwards compat
     * until dynamic flow is in a happy place.
     *
     * @param ev
     */
    public void onOccPermitAddSelectedBlocksToPermit(ActionEvent ev) {
        System.out.println("OccPeriodBB.onOccPermitAddSelectedBlocksToPermit | permitBlocksSelectedList: " + permitBlocksSelectedList.size());
        if (currentOccPermit != null) {

            switch (currentTextBlockPermitFieldEnum) {
                case STIPULATIONS:
                    if (currentOccPermit.getTextBlocks_stipulations() == null) {
                        currentOccPermit.setTextBlocks_stipulations(new ArrayList<>());
                    }
                    currentOccPermit.getTextBlocks_stipulations().addAll(permitBlocksSelectedList);
                    break;
                case NOTICES:
                    if (currentOccPermit.getTextBlocks_notice() == null) {
                        currentOccPermit.setTextBlocks_notice(new ArrayList<>());
                    }
                    currentOccPermit.getTextBlocks_notice().addAll(permitBlocksSelectedList);
                    break;
                case COMMENTS:

                    if (currentOccPermit.getTextBlocks_comments() == null) {
                        currentOccPermit.setTextBlocks_comments(new ArrayList<>());
                    }
                    currentOccPermit.getTextBlocks_comments().addAll(permitBlocksSelectedList);
                    break;
            }
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Added " + permitBlocksSelectedList.size() + " text blocks to occ permit text field: " + currentTextBlockPermitFieldEnum.getLabel(), ""));
            permitBlocksSelectedList.clear();
        }
    }

    /**
     * Listener for user requests to start the permit type text blocks default
     * write operation
     *
     * @param ev
     */
    public void onOccPermitSaveCurrentTextBlocksAsDefaultInit(ActionEvent ev) {
        System.out.println("onOccPermitSaveCurrentTextBlocksAsDefaultInit");
    }

    public void onOccPermitSaveCurrentTextBlocksAsDefaultAbort(ActionEvent ev) {
        System.out.println("onOccPermitSaveCurrentTextBlocksAsDefaultAbort");
    }

    /**
     * Listener for user requests to write the current permit's text blocks as
     * the default config for that type
     *
     * @param ev
     */
    public void onOccPermitSaveCurrentTextBlocksAsTypeDefaultCommit(ActionEvent ev) {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            oc.updateOccPermitTypeAutoAddTextBlocks(currentOccPermit);
            System.out.println("onOccPermitSaveCurrentTextBlocksAsDefaultCommit");
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Default text blocks have been updated for permits of type: " + currentOccPermit.getPermitType().getPermitTitle(), ""));
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_FATAL,
                            ex.getMessage(), ""));
        }
    }

    /**
     * Listener for user requests to start the process of picking people to link
     * This method supports the legacy, non dynamic field saving version of the
     * permit building process and should be preserved for backwards compat
     * until dynamic flow is in a happy place.
     *
     * to the current permit's person fields
     *
     * @param ev
     */
    public void onOccPermitPersonLinkingInitButtonChange(ActionEvent ev) {
        String cat = getFacesContext().getExternalContext().getRequestParameterMap().get(PERMIT_PERSON_LIST_PARAM_KEY);
        occPermitCurrentPersonLinkEnum = OccPermitPersonListEnum.valueOf(cat);
        System.out.println("OccPeriodBB.onOccPermitPersonLinkingInitButtonChange | selected person link enum: " + occPermitCurrentPersonLinkEnum.getLabel());
        configureOccPermitSelectedHumanLinkList(occPermitCurrentPersonLinkEnum);

        occPermitCandidateHumanLinkList = new ArrayList<>();
        if (currentOccPeriod.gethumanLinkList() != null && !currentOccPeriod.gethumanLinkList().isEmpty()) {
            occPermitCandidateHumanLinkList.addAll(currentOccPeriod.gethumanLinkList());
        }
        if (currentPropertyDH != null && currentPropertyDH.gethumanLinkList() != null && !currentPropertyDH.gethumanLinkList().isEmpty()) {
            occPermitCandidateHumanLinkList.addAll(currentPropertyDH.gethumanLinkList());
        }

        if (currentPropertyUnit != null && currentPropertyUnit.gethumanLinkList() != null && !currentPropertyUnit.gethumanLinkList().isEmpty()) {
            occPermitCandidateHumanLinkList.addAll(currentPropertyUnit.gethumanLinkList());
        }

        System.out.println("OccPeriodBB.onOccPermitPersonLinkingInitButtonChange | person candidate size " + occPermitCandidateHumanLinkList.size());

    }

    /**
     * Utility for making the single queued list reflect the chosen occ permit
     * human list This method supports the legacy, non dynamic field saving
     * version of the permit building process and should be preserved for
     * backwards compat until dynamic flow is in a happy place.
     *
     * @param opple
     */
    private void configureOccPermitSelectedHumanLinkList(OccPermitPersonListEnum opple) {

        if (opple != null) {
            occPermitCurrentPersonLinkEnum = opple;
        }

        occPermitSelectedHumanLinkList = new ArrayList<>();
        occPermitSelectedHumanLinkList.clear();

        if (occPermitCurrentPersonLinkEnum != null) {
            switch (occPermitCurrentPersonLinkEnum) {
                case CURRENT_OWNER:
                    if (currentOccPermit.getOwnerSellerLinkList() != null && !currentOccPermit.getOwnerSellerLinkList().isEmpty()) {
                        occPermitSelectedHumanLinkList.addAll(currentOccPermit.getOwnerSellerLinkList());
                    }
                    break;
                case MANAGERS:
                    if (currentOccPermit.getManagerLinkList() != null && !currentOccPermit.getManagerLinkList().isEmpty()) {
                        occPermitSelectedHumanLinkList.addAll(currentOccPermit.getManagerLinkList());
                    }
                    break;
                case NEW_OWNER:
                    if (currentOccPermit.getBuyerTenantLinkList() != null && !currentOccPermit.getBuyerTenantLinkList().isEmpty()) {
                        occPermitSelectedHumanLinkList.addAll(currentOccPermit.getBuyerTenantLinkList());

                    }
                    break;
                case SALE_WITH_TENANTS:
                    if (currentOccPermit.getTenantLinkList() != null && !currentOccPermit.getTenantLinkList().isEmpty()) {
                        occPermitSelectedHumanLinkList.addAll(currentOccPermit.getTenantLinkList());
                    }
                    break;
            }
        }
    }

    /**
     * Listener to switch between the four person link lists on the permit This
     * method supports the legacy, non dynamic field saving version of the
     * permit building process and should be preserved for backwards compat
     * until dynamic flow is in a happy place.
     *
     * @param ev
     */
    public void onOCcPermitPersonLinkListChooseLinkClick(ActionEvent ev) {
        String cat = getFacesContext().getExternalContext().getRequestParameterMap().get(PERMIT_PERSON_LIST_PARAM_KEY);
        occPermitCurrentPersonLinkEnum = OccPermitPersonListEnum.valueOf(cat);
        System.out.println("OccPeriodBB.onOCcPermitPersonLinkListChooseLinkClick | selected person link enum: " + occPermitCurrentPersonLinkEnum.getLabel());
        configureOccPermitSelectedHumanLinkList(occPermitCurrentPersonLinkEnum);
    }

    /**
     * Adaptor listener for adding a person to a specified list which we'll pull
     * out of the request params
     *
     * @param hl
     */
    public void onOccPermitPersonLinkFromFlow(HumanLink hl) {
        String personLinkAddTargetEnumString;
        if (personQuickAddCertRoleString == null) {
            // get our string from the request param map
            personLinkAddTargetEnumString = getFacesContext().getExternalContext().getRequestParameterMap().get("personlinkenumstring");
        } else {
            // inject our list box selection for the cert role into this method's 
            // local member
            personLinkAddTargetEnumString = personQuickAddCertRoleString;
        }
        if (personLinkAddTargetEnumString != null) {
            try {
                occPermitCurrentPersonLinkEnum = OccPermitPersonListEnum.valueOf(personLinkAddTargetEnumString);
            } catch (IllegalArgumentException | NullPointerException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fatal error: Unable to add person properly to permit; dev must correct", ""));
            }
        } else {
            System.out.println("OccPeriodBB.onOccPermitPersonLinkFromFlow | no cert link role string");
        }
        onOccPermitPersonLinkQueuePerson(hl);
        saveCurrentPermitDyanmicFieldsAndRefreshPermitAndOPDH();

        // reset our quick person link role string
        personQuickAddCertRoleString = null;
    }

    /**
     * Listener for user requests to queue their chosen person to the bean's
     * currently selected person link list represented by the enum value
     *
     * @param hl
     */
    public void onOccPermitPersonLinkQueuePerson(HumanLink hl) {
        if (occPermitCurrentPersonLinkEnum != null) {
            System.out.println("OccPeriodBB.onOccPermitPersonLinkQueuePerson | currentPersonLinkEnum  " + occPermitCurrentPersonLinkEnum.getLabel());
        }
        System.out.println("OccPeriodBB.onOccPermitPersonLinkQueuePerson | queuing Person ID  " + hl.getHumanID());
        if (currentOccPermit != null) {

            switch (occPermitCurrentPersonLinkEnum) {
                case CURRENT_OWNER:
                    if (currentOccPermit.getOwnerSellerLinkList() == null) {
                        currentOccPermit.setOwnerSellerLinkList(new ArrayList<>());
                    }
                    currentOccPermit.getOwnerSellerLinkList().add(hl);
                    break;
                case MANAGERS:
                    if (currentOccPermit.getManagerLinkList() == null) {
                        currentOccPermit.setManagerLinkList(new ArrayList<>());
                    }
                    currentOccPermit.getManagerLinkList().add(hl);
                    break;
                case NEW_OWNER:
                    if (currentOccPermit.getBuyerTenantLinkList() == null) {
                        currentOccPermit.setBuyerTenantLinkList(new ArrayList<>());
                    }
                    currentOccPermit.getBuyerTenantLinkList().add(hl);
                    break;
                case SALE_WITH_TENANTS:
                    if (currentOccPermit.getTenantLinkList() == null) {
                        currentOccPermit.setTenantLinkList(new ArrayList<>());
                    }
                    currentOccPermit.getTenantLinkList().add(hl);
                    break;
            }
            configureOccPermitSelectedHumanLinkList(occPermitCurrentPersonLinkEnum);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Added person ID " + hl.getHumanID() + " to occ permit person list:  " + occPermitCurrentPersonLinkEnum.getLabel(), ""));
        } else {
            System.out.println("OccPeriodBB.onOccPermitPersonLinkQueuePerson | NULL currentOccPermit");
        }
    }

    /**
     * Adaptor listener for removing a person to a specified list which we'll
     * pull out of the request params
     *
     * @param hl
     */
    public void onOccPermitPersonLinkFromFlowRemove(HumanLink hl) {
        String personLinkAddTargetEnumString = getFacesContext().getExternalContext().getRequestParameterMap().get("personlinkenumstring");
        if (personLinkAddTargetEnumString != null) {
            try {
                occPermitCurrentPersonLinkEnum = OccPermitPersonListEnum.valueOf(personLinkAddTargetEnumString);
            } catch (IllegalArgumentException | NullPointerException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fatal error: Unable to add person properly to permit; dev must correct", ""));
            }
        }
        onOccPermitPersonLinkRemove(hl);
        saveCurrentPermitDyanmicFieldsAndRefreshPermitAndOPDH();
    }

    /**
     * Listener for user requests to remove the chosen human link from the
     * bean's currently selected person link list
     *
     * @param hl
     */
    public void onOccPermitPersonLinkRemove(HumanLink hl) {
        switch (occPermitCurrentPersonLinkEnum) {
            case CURRENT_OWNER:
                currentOccPermit.getOwnerSellerLinkList().remove(hl);
                break;
            case MANAGERS:
                currentOccPermit.getManagerLinkList().remove(hl);
                break;
            case NEW_OWNER:
                currentOccPermit.getBuyerTenantLinkList().remove(hl);
                break;
            case SALE_WITH_TENANTS:
                currentOccPermit.getTenantLinkList().remove(hl);
                break;
        }
        configureOccPermitSelectedHumanLinkList(null);
    }

    /**
     * Listener for userers to start the nullification process
     *
     * @param op
     */
    public void onOccPermitNullifyInitLinkClick(OccPermit op) {
        System.out.println("OccPeriodBB.onOccPermitNullifyInitLinkClick");
        currentOccPermit = op;

    }

    /**
     * Builds a single master list of person link candidates from optionally the
     * current permit's ancestor property, property unit, and occ period
     */
    public void configurePersonLinkComphrensiveListing() {
        humanLinkListComprehensive = new ArrayList<>();
        // compile all human links on ancestor objects of our current permit: property, unit, and period
        if (permitPersonCandidateIncludePropertyLinks
                && currentPropertyDH != null
                && currentPropertyDH.gethumanLinkList() != null
                && !currentPropertyDH.gethumanLinkList().isEmpty()) {
            humanLinkListComprehensive.addAll(currentPropertyDH.gethumanLinkList());
        }
        if (permitPersonCandidateIncludeUnitLinks
                && currentPropertyUnit != null
                && currentPropertyUnit.gethumanLinkList() != null
                && !currentPropertyUnit.gethumanLinkList().isEmpty()) {
            humanLinkListComprehensive.addAll(currentPropertyUnit.gethumanLinkList());
        }
        if (permitPersonCandidateIncludeOccPeriodLinks
                && currentOccPeriod != null
                && currentOccPeriod.gethumanLinkList() != null
                && !currentOccPeriod.gethumanLinkList().isEmpty()) {
            humanLinkListComprehensive.addAll(currentOccPeriod.gethumanLinkList());
        }
        System.out.println("OccPeriodBB.configurePersonLinkComphrensiveListing | list size: " + humanLinkListComprehensive.size());
    }

    /**
     * listener to start the person search add mode on the
     *
     * @param ev
     */
    public void onOccPermitPersonSearch(ActionEvent ev) {
        permitPersonSearchModeEnabled = true;
        System.out.println("OccPeriodBB.onOccPermitPersonSearch | enabling person search mode");
    }

    /**
     * Toggles off person search mode
     *
     * @param ev
     */
    public void onOccPermitPersonSearchClose(ActionEvent ev) {
        permitPersonAddModeEnabled = false;
    }

    /**
     * Listener for user requests to nullify the current occ permit
     *
     * @param ev
     */
    public void onOccPermitNullifyCommitLinkClick(ActionEvent ev) {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            oc.nullifyOccPermit(currentOccPermit, currentOccPeriod, getSessionBean().getSessUser());
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Occ Permit ID " + currentOccPermit.getPermitID() + " has been nullified! ", ""));
            refreshCurrentOccPermit(null);
            refreshCurrentOccPeriodDataHeavy(true);
        } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
    }

    /**
     * Listener to start a new permit with the flow
     *
     * @param ev
     */
    public void onOccPermitCreateNewWithFlow(ActionEvent ev) {
        onOccPermitInitButtonChange(null);
        // turn off for testing
//        onOccPermitInitCommitButtonChange(null);
        onOccPermitInitFlow(null);

    }

    /**
     * Listener to start the flow process which is called to begin a new
     * certificate AND to resume config of an existing certificate AND to amend
     * a finalized certificate; This method sets the active step at the first
     * step: 1 but can safely be changed by later operations after flow init by
     * calling the routeToPermitFlowStep(int step)
     *
     *
     * @param ev
     */
    public void onOccPermitInitFlow(ActionEvent ev) {
        System.out.println("OccPeriodBB.onOccPermitInitFlow");
        initPermitStepMap();
        initPermitSteps();
        permitActiveStep = 1;
    }

    /**
     * Steps up a convenient handy mapping of step number to name for happy
     * users. FOlks
     */
    private void initPermitStepMap() {
        setPermitFlowStepMap(new HashMap<>());
        for (OccPermitFlowStepEnum step : OccPermitFlowStepEnum.values()) {
            getPermitFlowStepMap().put(step.getStepNumber1Based(), step.getStepTitle());
        }

    }

    /**
     * Steps up our dynamic menu
     */
    private void initPermitSteps() {
        permitStepsModel = new DefaultMenuModel();
        for (OccPermitFlowStepEnum step : OccPermitFlowStepEnum.values()) {
            permitStepsModel.getElements().add(createMenuItem(step.getStepTitle(), null));
        }

    }

    /**
     * Builds us a nice little menu item for the permit flow
     *
     * @param value
     * @param command
     * @return
     */
    private DefaultMenuItem createMenuItem(String value, String command) {
        DefaultMenuItem item = new DefaultMenuItem();
        item.setAjax(false);
        item.setValue(value);
        item.setDisabled(true);
        item.setStyleClass("non-clickable");
//        item.setCommand(command);
//        item.setUpdate("permit-steps-panel");
        return item;
    }

    /**
     * Listener to end the permit flow at a specific step
     *
     * @param ev
     */
    public void onSavePermitFlowAndClose(ActionEvent ev) {
        saveCurrentPermitDyanmicFieldsAndRefreshPermitAndOPDH();
    }

    /**
     * Writes our current step to the current permit and then sends our in
     * process permit to the DB and refreshes both this permit and the parent
     * occ period
     */
    private void saveCurrentPermitDyanmicFieldsAndRefreshPermitAndOPDH() {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        if (currentOccPermit != null) {
            try {
                currentOccPermit.setDynamicLastStep(permitActiveStep);
                oc.updateOccPermitDynamicFields(currentOccPermit, currentOccPeriod, getSessionBean().getSessUser());
                System.out.println("OccPeriodBB.saveCurrentPermitDyanmicFields | at step: " + permitActiveStep);
                refreshCurrentOccPermit(null);
                refreshCurrentOccPeriodDataHeavy(false);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Occ Permit ID " + currentOccPermit.getPermitID() + " has been saved for later! ", ""));
            } catch (BObStatusException | IntegrationException | AuthorizationException ex) {
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            }
        }
    }

    /**
     * Listener for user requests to resume the permitting config at the given
     * permit's current dynamic step. This method is also called to resume the
     * permit amendment process at the person linking step. The permit itself
     * must have its dynamic step configured properly.
     *
     * @param permit
     */
    public void resumePermitConfigAtCurrentStep(OccPermit permit) {
        currentOccPermit = permit;
        if (currentOccPermit != null) {
            System.out.println("OccPeriodBB.resumePermitConfigAtCurrentStep | permit ID: " + currentOccPermit.getPermitID() + " | current step: " + currentOccPermit.getDynamicLastStep());
            onOccPermitInitFlow(null);
            routeToPermitFlowStep(currentOccPermit.getDynamicLastStep());
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Resuming certificate config at step " + currentOccPermit.getDynamicLastStep(), ""));
        } else {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal: Error resuming certificate config at step X!", ""));
        }
    }

    /**
     * Listener for step navigation; I'm just a switch that calls the correct
     * methods internally; Code in this bean can safety bypass this method which
     * extracts the target step through the request parameter map and instead
     * just call the routeToPermitFlowStep(int step) method in this class
     * directly.
     *
     * @param ev
     */
    public void gotoPermitFlowStepX(ActionEvent ev) {
        String flowTarget = getFacesContext().getExternalContext().getRequestParameterMap().get("flowsteptarget");
        int step = 1;
        if (flowTarget != null) {
            step = Integer.parseInt(flowTarget);
        } else {
            System.out.println("OccPeriodBB.gotoPermitFlowStepX | error processing request step mapped value! Falling back to step 1 ");
        }
        System.out.println("OccPeriodBB.gotoPermitFlowStep - currentStep" + permitActiveStep);
        System.out.println("OccPeriodBB.gotoPermitFlowStep: " + step);

        routeToPermitFlowStep(step);
    }

    /**
     * I wasn't getting my request parameter map sent to the server AFTER a
     * validation failed and was then corrected, so here's my single workround
     * method for ONLY going from Dates to persons. GRRR
     *
     * @param ev
     */
    public void onPermitFlowProceedPastDateStep(ActionEvent ev) {
        routeToPermitFlowStep(3);

    }

    /**
     * Internal routing organ that will call the appropriate step method based
     * on the passed in integer step number (1-based). For more modern
     * Enum-based flow routing, use routeToPermitFlow(Enum val)
     *
     * @param step
     */
    private void routeToPermitFlowStep(int step) {
        saveCurrentPermitDyanmicFieldsAndRefreshPermitAndOPDH();
        resetOccPermitDynamicFormFields();

        switch (step) {
            case 1 -> {
                gotoPermitFlowStep1Type(null);
            }
            case 2 -> {
                gotoPermitFlowStep2Dates(null);
            }
            case 3 -> {
                gotoPermitFlowStep3Persons(null);
            }
            case 4 -> {
                gotoPermitFlowStep4Remarks(null);
            }
            case 5 -> {
                gotoPermitFlowStep5Review(null);
            }
            case 6 -> {
                gotoPermitFlowStep6Issue(null);
            }
            default -> {
                System.out.println("Unknown Step target; doing nothing");
            }
        }
    }

    /**
     * Internal routing organ that will call the appropriate step method based
     * on the passed in step number (1-based). This is an amendment in process
     * safe method that won't bugger up the current permit instance
     *
     * @param step
     */
    private void routeToPermitFlowStep(OccPermitFlowStepEnum step) {

        if (currentOccPermit != null && !currentOccPermit.isCurrentPermitAmendmentMode()) {
            saveCurrentPermitDyanmicFieldsAndRefreshPermitAndOPDH();
            resetOccPermitDynamicFormFields();
        } else {
            System.out.println("OccPeriodBB.routeToPermitFlowStep | found amendment in process | skipping saving and refresh");
        }

        switch (step) {
            case PERMIT_TYPE -> {
                gotoPermitFlowStep1Type(null);
            }
            case DATES -> {
                gotoPermitFlowStep2Dates(null);
            }
            case PERSONS -> {
                gotoPermitFlowStep3Persons(null);
            }
            case REMARKS -> {
                gotoPermitFlowStep4Remarks(null);
            }
            case REVIEW -> {
                gotoPermitFlowStep5Review(null);
            }
            case ISSUE -> {
                gotoPermitFlowStep6Issue(null);
            }
            default -> {
                System.out.println("Unknown Step target; doing nothing");
            }
        }
    }

    /**
     * Since we've got a lot of fancy form jazz going on in our occ permit form,
     * this method resets the validation status to valid on relevant form
     * components
     */
    public void resetOccPermitDynamicFormFields() {

        UIComponent compDateOfApplication = getFacesContext().getViewRoot().findComponent(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_APPLICATION.getComponentID());
        if (compDateOfApplication != null && compDateOfApplication instanceof UIInput) {
            UIInput ldtInput = (UIInput) compDateOfApplication;
            ldtInput.setValid(true);
        }

        UIComponent compDateOfFinInit = getFacesContext().getViewRoot().findComponent(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_FIN_INIT.getComponentID());
        if (compDateOfFinInit != null && compDateOfFinInit instanceof UIInput) {
            UIInput ldtInput = (UIInput) compDateOfFinInit;
            ldtInput.setValid(true);
        }

        UIComponent compDateOfFinReinspect = getFacesContext().getViewRoot().findComponent(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_FIN_REINSPECT.getComponentID());
        if (compDateOfFinReinspect != null && compDateOfFinReinspect instanceof UIInput) {
            UIInput ldtInput = (UIInput) compDateOfFinReinspect;
            ldtInput.setValid(true);
        }

        UIComponent compDateOfFinFinal = getFacesContext().getViewRoot().findComponent(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_FIN_FINAL.getComponentID());
        if (compDateOfFinFinal != null && compDateOfFinFinal instanceof UIInput) {
            UIInput ldtInput = (UIInput) compDateOfFinFinal;
            ldtInput.setValid(true);
        }

        UIComponent compDateOfIssuance = getFacesContext().getViewRoot().findComponent(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_ISSUANCE.getComponentID());
        if (compDateOfIssuance != null && compDateOfIssuance instanceof UIInput) {
            UIInput ldtInput = (UIInput) compDateOfIssuance;
            ldtInput.setValid(true);
        }

        UIComponent compDateOfExpiry = getFacesContext().getViewRoot().findComponent(ComponentIDEnum.PERMIT_DATE_COMPONENT_ID_EXPIRY.getComponentID());
        if (compDateOfExpiry != null && compDateOfExpiry instanceof UIInput) {
            UIInput ldtInput = (UIInput) compDateOfExpiry;
            ldtInput.setValid(true);
        }

    }

    /**
     * Regular old getter
     *
     * @return the permitActiveStep
     */
    public int getPermitActiveStep() {
        return permitActiveStep;
    }

    /**
     * Listener to move to step 1: type
     *
     * @param ev
     */
    public void gotoPermitFlowStep1Type(ActionEvent ev) {
        System.out.println("OccPeriodBB.gotoPermitFlowStep1Type");
        permitActiveStep = 1;
    }

    /**
     * listener to be done with initial step of type selection. Users cannot
     * STOP at this step. They can only cancel.
     *
     * @param ev
     */
    public void onPermitFlowCompleteStep1Type(ActionEvent ev) {
        onOccPermitInitCommitButtonChange(null);
       
        routeToPermitFlowStep(2);
    }

    /**
     * Listener to end the flow at step 1
     *
     * @param ev
     */
    public void onPermitFlowCancel(ActionEvent ev) {
        System.out.println("OccPeriodBB.onPermitFlowCancel");
    }

    /**
     * Listener to move to step 2: Dates
     *
     * @param ev
     */
    public void gotoPermitFlowStep2Dates(ActionEvent ev) {
        System.out.println("OccPeriodBB.gotoPermitFlowStep2Dates");
        permitActiveStep = 2;
    }

    /**
     * Listener to move to step 3: Persons Turns on all our include switches and
     * build the comprehensive person list
     *
     * @param ev
     */
    public void gotoPermitFlowStep3Persons(ActionEvent ev) {
        System.out.println("OccPeriodBB.gotoPermitFlowStep4Persons");
        permitPersonCandidateIncludePropertyLinks = true;
        permitPersonCandidateIncludeUnitLinks = true;
        permitPersonCandidateIncludeOccPeriodLinks = true;

        configurePersonLinkComphrensiveListing();

        permitPersonSearchModeEnabled = false;

        permitActiveStep = 3;

    }

    /**
     * Step two in the person quick add commit process: The PersonBB's first
     * step method will have completed meaning our session humanlink is the one
     * to add to our permit.
     *
     */
    public void onPersonQuickAddCommitButtonChange() {
        System.out.println("OccPeriodBB.onPersonQuickAddCommitButtonChange");

        onOccPermitPersonLinkFromFlow(getSessionBean().getSessHumanLink());
        // refresh our shit
        refreshCurrentPeriodParentObjects();
        refreshCurrentOccPeriodDataHeavy(false);
        // reset our candidate listings to refleft our new person being linked
        configurePersonLinkComphrensiveListing();

    }

    /**
     * listener to refresh all current person links
     *
     * @param ev
     */
    public void onPermitPersonLinkRefresh(ActionEvent ev) {
        System.out.println("OccPeriodBB.onPermitPersonLinkRefresh");
        refreshCurrentOccPermit(null);
    }

    /**
     * Listener to move to step 4: Remarks
     *
     * @param ev
     */
    public void gotoPermitFlowStep4Remarks(ActionEvent ev) {
        System.out.println("OccPeriodBB.gotoPermitFlowStep5Remarks");
        // setup our canned text utility lists
        // we'll build candidate list by link clink
        permitBlocksSelectedList = new ArrayList<>();
        permitBlocksFilteredList = new ArrayList<>();
        permitActiveStep = 4;

    }

    /**
     * Listener to move to step 5: Finalize
     *
     * @param ev
     */
    public void gotoPermitFlowStep5Review(ActionEvent ev) {
        System.out.println("OccPeriodBB.gotoPermitFlowStep5Review");
        onOccPermitGenerateStaticFields(null);
        permitActiveStep = 5;

    }

    /**
     * Listener for users editing the parcel info on a permit; I'll grab the
     * property profile BB and call its edit commit and then i'll refresh all my
     * objects;
     *
     * @param ev
     */
    public void onEditParcelInfoCommitButtonChange(ActionEvent ev) {
        PropertyProfileBB propProfileBB = (PropertyProfileBB) getFacesContext().getApplication().evaluateExpressionGet(getFacesContext(), "#{propertyProfileBB}", PropertyProfileBB.class);
        if (propProfileBB != null) {
            propProfileBB.onParcelInfoEditModeToggleButtonChange(null);
            // rebuild our static values for preview 
            // since this operation takes place after dyamic population
            // the coordinator method will get us a fresh copy of the property
            // from which it shall extrac relevant fields 
            // updated on our form
            routeToPermitFlowStep(5);
            refreshCurrentPeriodParentObjects();

        }
    }

    /**
     * Listener to move to step 6: Issue
     *
     * @param ev
     */
    public void gotoPermitFlowStep6Issue(ActionEvent ev) {
        System.out.println("OccPeriodBB.gotoPermitFlowStep7Issue");
        OccupancyCoordinator oc = getOccupancyCoordinator();
        // setup form fields for issuance
        permitIssuanceFollowupEventDaysFromToday = 2;
        permitIssuanceCreateFollowupEvent = false;
        permitIssuancePersonCandidateList = currentOccPermit.getCompletePermitHumanLinkList();
        permitIssuancePersonCandidateList.add(oc.createMuniStaffHumanLink());
        permitActiveStep = 6;

        if (currentOccPermit.getStaticdateofissue() != null) {
            permitIssuanceTS = currentOccPermit.getStaticdateofissue().atStartOfDay();
        } else {
            permitIssuanceTS = LocalDateTime.now();
        }
        // only do a dynamic save on non-finalized amendmentment permits coming
        // through this path
        if (currentOccPermit != null && currentOccPermit.getAmendementCommitTS() == null) {
            saveCurrentPermitDyanmicFieldsAndRefreshPermitAndOPDH();
        }

    }

    /**
     * Listener for user requests to complete the flow cycle which will involve
     * registering events based on form fields submitted
     *
     * @param ev
     */
    public void onCompletePermitFlow(ActionEvent ev) {
        EventCoordinator ec = getEventCoordinator();
        OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            if (!permitIssuanceCreateFollowupEvent) {
                System.out.println("OccPeriodBB.onCompletePermitFlow | log issuance event");
                // configure description
                StringBuilder sb = new StringBuilder();
                sb.append("Certificate with reference number: ");
                sb.append(currentOccPermit.getReferenceNo());
                if (permitIssuanceMethod != null) {
                    sb.append(" was ");
                    sb.append(permitIssuanceMethod);
                    if (permitIssuancePersonTarget != null) {
                        sb.append(permitIssuancePersonTarget.getName());
                    }
                }
                oc.occPermitDeployRecordingAndFollowUpEvents(currentOccPermit,
                        currentOccPeriod,
                        sb.toString(),
                        occPermitSelectedHumanLink,
                        getSessionBean().getSessUser());
                permitActiveStep = PERMIT_FLOW_STEPS_COMPLETE;
            } else {
                // follow up event activated
                System.out.println("OccPeriodBB.onCompletePermitFlow | create reminder event instead");
                EventCnF issuanceReminderEvent = ec.initEvent(currentOccPeriod, oc.getPermitFollowupRequredEventCategory());
                issuanceReminderEvent.configureEventTimesFromStartTime(LocalDateTime.now().plusDays(permitIssuanceFollowupEventDaysFromToday), 0);
                List<EventCnF> evList = ec.addEvent(issuanceReminderEvent, currentOccPeriod, getSessionBean().getSessUser());
                if (evList != null && !evList.isEmpty()) {
                    ec.linkEventToEventLinked(evList.get(0), currentOccPermit);
                }
                permitActiveStep = 6;
            }
            // reset our event jazz
            getSessionEventConductor().setSessEventListForRefreshUptake(ec.getEventList(currentOccPeriod));
            getSessionEventConductor().refreshCalendarAndFollowupBacklog(null);
            // set our permit to know where it ended
            saveCurrentPermitDyanmicFieldsAndRefreshPermitAndOPDH();
        } catch (BObStatusException | AuthorizationException | EventException | IntegrationException | BlobException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
    }

    // *************************************************************************
    // ********************* MISC  *******************************
    // *************************************************************************
    /**
     * Special getter for the event list whose contents is managed by a shared
     * set of utility methods on EventBB
     *
     * @return the fresh list of events for this case
     */
    public List<EventCnF> getManagedEventList() {
        List<EventCnF> evlist = getSessionEventConductor().getSessEventListForRefreshUptake();
        if (currentOccPeriod != null) {
            if (evlist != null) {
                System.out.println("CECaseBB.getManagedEventList | fresh event list found on sessionbean of size: " + evlist.size());
                currentOccPeriod.setEventList(evlist);
                getSessionEventConductor().setSessEventListForRefreshUptake(null);
            }
            return currentOccPeriod.getEventList();
        }
        return new ArrayList<>();
    }

    /**
     * Special getter wrapper around the CECase blob list that checks the
     * session for a new blob list that may have been injected by the
     * BlobUtilitiesBB
     *
     * @return the CECases's updated blob list
     */
    public List<BlobLight> getManagedBlobLightList() {
        List<BlobLight> sessBlobListForUpdate = getSessionBean().getSessBlobLightListForRefreshUptake();
        if (sessBlobListForUpdate != null
                && currentOccPeriod != null
                && getSessionBean().getSessBlobLightListHolderForRefreshUptake() != null
                && getSessionBean().getSessBlobLightListHolderForRefreshUptake().getBlobLinkEnum() == BlobLinkEnum.OCC_PERIOD
                && getSessionBean().getSessBlobLightListHolderForRefreshUptake().getParentObjectID() == currentOccPeriod.getPeriodID()) {
            currentOccPeriod.setBlobList(sessBlobListForUpdate);
            // clear session since we have the new list
            getSessionBean().resetBlobRefreshUptakeFields();
        } else {
            if (currentOccPeriod.getBlobList() == null) {
                currentOccPeriod.setBlobList(new ArrayList<>());
            }
        }

        System.out.println("occPeriodBB.getManagedBlobLightList: " + currentOccPeriod.getBlobList());
        return currentOccPeriod.getBlobList();
    }

    // *************************************************************************
    // ********************* TASKS  *******************************
    // *************************************************************************
    public void onViewTaskAssigned(TaskAssigned task) {
        currentTaskAssigned = task;

    }

    public void completeTask(TaskAssigned task) {
        TaskCoordinator tc = getTaskCoordinator();
        currentTaskAssigned = task;

    }

    /**
     * listener for user requests to setup the task system for the current occ
     * period which would be needed if the occ period was created before the
     * auto-init of tasks during occ period creation. This began occurring in
     * late Sept 2023
     *
     * @param ev
     */
    public void initOccPeriodTasks(ActionEvent ev) {
        TaskCoordinator tc = getTaskCoordinator();
        try {
            tc.initializeOccPeriodTaskSubsystem(currentOccPeriod, getSessionBean().getSessUser());
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Successfully initialized task tree on this occ period", ""));
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
    }

    /**
     * Listener for user requests to reload the current task tree, which will
     * reevaluate the state of each task based on the state of the containing
     * occ period
     *
     * @param ev
     */
    public void refreshTaskTree(ActionEvent ev) {
        refreshCurrentOccPeriodDataHeavy(false);

    }

    // *************************************************************************
    // ********************* GETTERS AND SETTERS *******************************
    // *************************************************************************
    /**
     * @return the currentOccPeriod
     */
    public OccPeriodDataHeavy getCurrentOccPeriod() {
        return currentOccPeriod;
    }

    /**
     * @param currentOccPeriod the currentOccPeriod to set
     */
    public void setCurrentOccPeriod(OccPeriodDataHeavy currentOccPeriod) {
        this.currentOccPeriod = currentOccPeriod;
    }

    /**
     * @return the currentPropertyUnit
     */
    public PropertyUnitDataHeavy getCurrentPropertyUnit() {
        return currentPropertyUnit;
    }

    /**
     * @return the lastSavedOccPeriod
     */
    public OccPeriod getLastSavedOccPeriod() {
        return lastSavedOccPeriod;
    }

    /**
     * @return the occPeriodTypeList
     */
    public List<OccPeriodType> getOccPeriodTypeList() {
        return occPeriodTypeCandidateList;
    }

    /**
     * @return the selectedPropertyUnit
     */
    public PropertyUnit getSelectedPropertyUnit() {
        return selectedPropertyUnit;
    }

    /**
     * @return the managerInspectorCandidateList
     */
    public List<User> getManagerInspectorCandidateList() {
        return managerInspectorCandidateList;
    }

    /**
     * @return the selectedManager
     */
    public User getSelectedManager() {
        return selectedManager;
    }

    /**
     * @param lastSavedOccPeriod the lastSavedOccPeriod to set
     */
    public void setLastSavedOccPeriod(OccPeriod lastSavedOccPeriod) {
        this.lastSavedOccPeriod = lastSavedOccPeriod;
    }

    /**
     * @param occPeriodTypeList the occPeriodTypeList to set
     */
    public void setOccPeriodTypeList(List<OccPeriodType> occPeriodTypeList) {
        this.occPeriodTypeCandidateList = occPeriodTypeList;
    }

    /**
     * @param selectedPropertyUnit the selectedPropertyUnit to set
     */
    public void setSelectedPropertyUnit(PropertyUnit selectedPropertyUnit) {
        this.selectedPropertyUnit = selectedPropertyUnit;
    }

    /**
     * @param managerInspectorCandidateList the managerInspectorCandidateList to
     * set
     */
    public void setManagerInspectorCandidateList(List<User> managerInspectorCandidateList) {
        this.managerInspectorCandidateList = managerInspectorCandidateList;
    }

    /**
     * @param selectedManager the selectedManager to set
     */
    public void setSelectedManager(User selectedManager) {
        this.selectedManager = selectedManager;
    }

    /**
     * @return the selectedOccPeriodType
     */
    public OccPeriodType getSelectedOccPeriodType() {
        return selectedOccPeriodType;
    }

    /**
     * @return the propertyUnitCandidateList
     */
    public List<PropertyUnit> getPropertyUnitCandidateList() {
        return propertyUnitCandidateList;
    }

    /**
     * @param selectedOccPeriodType the selectedOccPeriodType to set
     */
    public void setSelectedOccPeriodType(OccPeriodType selectedOccPeriodType) {
        this.selectedOccPeriodType = selectedOccPeriodType;
    }

    /**
     * @param propertyUnitCandidateList the propertyUnitCandidateList to set
     */
    public void setPropertyUnitCandidateList(List<PropertyUnit> propertyUnitCandidateList) {
        this.propertyUnitCandidateList = propertyUnitCandidateList;
    }

    /**
     * @return the currentOccPermit
     */
    public OccPermit getCurrentOccPermit() {
        return currentOccPermit;
    }

    /**
     * @return the currentOccPermitConfig
     */
    public ReportConfigOccPermit getCurrentOccPermitConfig() {
        return currentOccPermitConfig;
    }

    /**
     * @return the editModeOccPermit
     */
    public boolean isEditModeOccPermit() {
        return editModeOccPermit;
    }

    /**
     * @param currentOccPermit the currentOccPermit to set
     */
    public void setCurrentOccPermit(OccPermit currentOccPermit) {
        this.currentOccPermit = currentOccPermit;
    }

    /**
     * @param currentOccPermitConfig the currentOccPermitConfig to set
     */
    public void setCurrentOccPermitConfig(ReportConfigOccPermit currentOccPermitConfig) {
        this.currentOccPermitConfig = currentOccPermitConfig;
    }

    /**
     * @param editModeOccPermit the editModeOccPermit to set
     */
    public void setEditModeOccPermit(boolean editModeOccPermit) {
        this.editModeOccPermit = editModeOccPermit;
    }

    /**
     * @return the currentPropertyDH
     */
    public PropertyDataHeavy getCurrentPropertyDH() {
        return currentPropertyDH;
    }

    /**
     * @param currentPropertyDH the currentPropertyDH to set
     */
    public void setCurrentPropertyDH(PropertyDataHeavy currentPropertyDH) {
        this.currentPropertyDH = currentPropertyDH;
    }

    /**
     * @return the currentTextBlock
     */
    public TextBlock getCurrentTextBlock() {
        return currentTextBlock;
    }

    /**
     * @param currentTextBlock the currentTextBlock to set
     */
    public void setCurrentTextBlock(TextBlock currentTextBlock) {
        this.currentTextBlock = currentTextBlock;
    }

    /**
     * @return the currentTextBlockPermitFieldEnum
     */
    public TextBlockPermitFieldEnum getCurrentTextBlockPermitFieldEnum() {
        return currentTextBlockPermitFieldEnum;
    }

    /**
     * @param currentTextBlockPermitFieldEnum the
     * currentTextBlockPermitFieldEnum to set
     */
    public void setCurrentTextBlockPermitFieldEnum(TextBlockPermitFieldEnum currentTextBlockPermitFieldEnum) {
        this.currentTextBlockPermitFieldEnum = currentTextBlockPermitFieldEnum;
    }

    /**
     * @return the permitBlocksActiveCandidateList
     */
    public List<TextBlock> getPermitBlocksActiveCandidateList() {
        return permitBlocksActiveCandidateList;
    }

    /**
     * @param permitBlocksActiveCandidateList the
     * permitBlocksActiveCandidateList to set
     */
    public void setPermitBlocksActiveCandidateList(List<TextBlock> permitBlocksActiveCandidateList) {
        this.permitBlocksActiveCandidateList = permitBlocksActiveCandidateList;
    }

    /**
     * @return the permitBlocksSelectedList
     */
    public List<TextBlock> getPermitBlocksSelectedList() {
        return permitBlocksSelectedList;
    }

    /**
     * @param permitBlocksSelectedList the permitBlocksSelectedList to set
     */
    public void setPermitBlocksSelectedList(List<TextBlock> permitBlocksSelectedList) {
        this.permitBlocksSelectedList = permitBlocksSelectedList;
    }

    /**
     * @return the occPermitSelectedHumanLinkList
     */
    public List<HumanLink> getOccPermitSelectedHumanLinkList() {
        return occPermitSelectedHumanLinkList;
    }

    /**
     * @return the occPermitSelectedHumanLink
     */
    public HumanLink getOccPermitSelectedHumanLink() {
        return occPermitSelectedHumanLink;
    }

    /**
     * @return the occPermitCurrentPersonLinkEnum
     */
    public OccPermitPersonListEnum getOccPermitCurrentPersonLinkEnum() {
        return occPermitCurrentPersonLinkEnum;
    }

    /**
     * @param occPermitSelectedHumanLinkList the occPermitSelectedHumanLinkList
     * to set
     */
    public void setOccPermitSelectedHumanLinkList(List<HumanLink> occPermitSelectedHumanLinkList) {
        this.occPermitSelectedHumanLinkList = occPermitSelectedHumanLinkList;
    }

    /**
     * @param occPermitSelectedHumanLink the occPermitSelectedHumanLink to set
     */
    public void setOccPermitSelectedHumanLink(HumanLink occPermitSelectedHumanLink) {
        this.occPermitSelectedHumanLink = occPermitSelectedHumanLink;
    }

    /**
     * @param occPermitCurrentPersonLinkEnum the occPermitCurrentPersonLinkEnum
     * to set
     */
    public void setOccPermitCurrentPersonLinkEnum(OccPermitPersonListEnum occPermitCurrentPersonLinkEnum) {
        this.occPermitCurrentPersonLinkEnum = occPermitCurrentPersonLinkEnum;
    }

    /**
     * @return the occPermitCandidateHumanLinkList
     */
    public List<HumanLink> getOccPermitCandidateHumanLinkList() {
        return occPermitCandidateHumanLinkList;
    }

    /**
     * @param occPermitCandidateHumanLinkList the
     * occPermitCandidateHumanLinkList to set
     */
    public void setOccPermitCandidateHumanLinkList(List<HumanLink> occPermitCandidateHumanLinkList) {
        this.occPermitCandidateHumanLinkList = occPermitCandidateHumanLinkList;
    }

    /**
     * @return the permitBlocksFilteredList
     */
    public List<TextBlock> getPermitBlocksFilteredList() {
        return permitBlocksFilteredList;
    }

    /**
     * @param permitBlocksFilteredList the permitBlocksFilteredList to set
     */
    public void setPermitBlocksFilteredList(List<TextBlock> permitBlocksFilteredList) {
        this.permitBlocksFilteredList = permitBlocksFilteredList;
    }

    /**
     * @return the permitTypeCandidateList
     */
    public List<OccPermitType> getPermitTypeCandidateList() {
        return permitTypeCandidateList;
    }

    /**
     * @param permitTypeCandidateList the permitTypeCandidateList to set
     */
    public void setPermitTypeCandidateList(List<OccPermitType> permitTypeCandidateList) {
        this.permitTypeCandidateList = permitTypeCandidateList;
    }

    /**
     * @return the occPermitOwnerSellerOptions
     */
    public List<OccPermitPersonListEnum> getOccPermitOwnerSellerOptions() {
        return occPermitOwnerSellerOptions;
    }

    /**
     * @param occPermitOwnerSellerOptions the occPermitOwnerSellerOptions to set
     */
    public void setOccPermitOwnerSellerOptions(List<OccPermitPersonListEnum> occPermitOwnerSellerOptions) {
        this.occPermitOwnerSellerOptions = occPermitOwnerSellerOptions;
    }

    /**
     * @return the occPermitBuyerTenantOptions
     */
    public List<OccPermitPersonListEnum> getOccPermitBuyerTenantOptions() {
        return occPermitBuyerTenantOptions;
    }

    /**
     * @param occPermitBuyerTenantOptions the occPermitBuyerTenantOptions to set
     */
    public void setOccPermitBuyerTenantOptions(List<OccPermitPersonListEnum> occPermitBuyerTenantOptions) {
        this.occPermitBuyerTenantOptions = occPermitBuyerTenantOptions;
    }

    /**
     * @return the permissionsAllowOccPeriodOpenEdit
     */
    public boolean isPermissionsAllowOccPeriodOpenEdit() {
        return permissionsAllowOccPeriodOpenEdit;
    }

    /**
     * @return the permissionsAllowOccPermitDraft
     */
    public boolean isPermissionsAllowOccPermitDraft() {
        return permissionsAllowOccPermitDraft;
    }

    /**
     * @return the permissionsAllowOccpermitIssue
     */
    public boolean isPermissionsAllowOccpermitIssue() {
        return permissionsAllowOccpermitIssue;
    }

    /**
     * @return the permissionsAllowOccPeriodDeactivation
     */
    public boolean isPermissionsAllowOccPeriodDeactivation() {
        return permissionsAllowOccPeriodDeactivation;
    }

    /**
     * @return the statusListForHelp
     */
    public List<OccPeriodStatusEnum> getStatusListForHelp() {
        return statusListForHelp;
    }

    /**
     * @param statusListForHelp the statusListForHelp to set
     */
    public void setStatusListForHelp(List<OccPeriodStatusEnum> statusListForHelp) {
        this.statusListForHelp = statusListForHelp;
    }

    /**
     * @return the currentTaskAssigned
     */
    public TaskAssigned getCurrentTaskAssigned() {
        return currentTaskAssigned;
    }

    /**
     * @param currentTaskAssigned the currentTaskAssigned to set
     */
    public void setCurrentTaskAssigned(TaskAssigned currentTaskAssigned) {
        this.currentTaskAssigned = currentTaskAssigned;
    }

    /**
     * @return the permitStepsModel
     */
    public MenuModel getPermitStepsModel() {
        return permitStepsModel;
    }

    /**
     * @param permitStepsModel the permitStepsModel to set
     */
    public void setPermitStepsModel(MenuModel permitStepsModel) {
        this.permitStepsModel = permitStepsModel;
    }

    /**
     * @param permitActiveStep the permitActiveStep to set
     */
    public void setPermitActiveStep(int permitActiveStep) {
        this.permitActiveStep = permitActiveStep;
    }

    /**
     * @return the permitFlowStepMap
     */
    public Map<Integer, String> getPermitFlowStepMap() {
        return permitFlowStepMap;
    }

    /**
     * @param permitFlowStepMap the permitFlowStepMap to set
     */
    public void setPermitFlowStepMap(Map<Integer, String> permitFlowStepMap) {
        this.permitFlowStepMap = permitFlowStepMap;
    }

    /**
     * @return the permitPersonCandidateIncludePropertyLinks
     */
    public boolean isPermitPersonCandidateIncludePropertyLinks() {
        return permitPersonCandidateIncludePropertyLinks;
    }

    /**
     * @param permitPersonCandidateIncludePropertyLinks the
     * permitPersonCandidateIncludePropertyLinks to set
     */
    public void setPermitPersonCandidateIncludePropertyLinks(boolean permitPersonCandidateIncludePropertyLinks) {
        this.permitPersonCandidateIncludePropertyLinks = permitPersonCandidateIncludePropertyLinks;
    }

    /**
     * @return the permitPersonCandidateIncludeUnitLinks
     */
    public boolean isPermitPersonCandidateIncludeUnitLinks() {
        return permitPersonCandidateIncludeUnitLinks;
    }

    /**
     * @param permitPersonCandidateIncludeUnitLinks the
     * permitPersonCandidateIncludeUnitLinks to set
     */
    public void setPermitPersonCandidateIncludeUnitLinks(boolean permitPersonCandidateIncludeUnitLinks) {
        this.permitPersonCandidateIncludeUnitLinks = permitPersonCandidateIncludeUnitLinks;
    }

    /**
     * @return the permitPersonCandidateIncludeOccPeriodLinks
     */
    public boolean isPermitPersonCandidateIncludeOccPeriodLinks() {
        return permitPersonCandidateIncludeOccPeriodLinks;
    }

    /**
     * @param permitPersonCandidateIncludeOccPeriodLinks the
     * permitPersonCandidateIncludeOccPeriodLinks to set
     */
    public void setPermitPersonCandidateIncludeOccPeriodLinks(boolean permitPersonCandidateIncludeOccPeriodLinks) {
        this.permitPersonCandidateIncludeOccPeriodLinks = permitPersonCandidateIncludeOccPeriodLinks;
    }

    /**
     * @return the humanLinkListComprehensive
     */
    public List<HumanLink> getHumanLinkListComprehensive() {
        return humanLinkListComprehensive;
    }

    /**
     * @param humanLinkListComprehensive the humanLinkListComprehensive to set
     */
    public void setHumanLinkListComprehensive(List<HumanLink> humanLinkListComprehensive) {
        this.humanLinkListComprehensive = humanLinkListComprehensive;
    }

    /**
     * @return the permitPersonSearchModeEnabled
     */
    public boolean isPermitPersonSearchModeEnabled() {
        return permitPersonSearchModeEnabled;
    }

    /**
     * @param permitPersonSearchModeEnabled the permitPersonSearchModeEnabled to
     * set
     */
    public void setPermitPersonSearchModeEnabled(boolean permitPersonSearchModeEnabled) {
        this.permitPersonSearchModeEnabled = permitPersonSearchModeEnabled;
    }

    /**
     * @return the permitPersonAddModeEnabled
     */
    public boolean isPermitPersonAddModeEnabled() {
        return permitPersonAddModeEnabled;
    }

    /**
     * @param permitPersonAddModeEnabled the permitPersonAddModeEnabled to set
     */
    public void setPermitPersonAddModeEnabled(boolean permitPersonAddModeEnabled) {
        this.permitPersonAddModeEnabled = permitPersonAddModeEnabled;
    }

    /**
     * @return the personQuickAddCertRoleString
     */
    public String getPersonQuickAddCertRoleString() {
        return personQuickAddCertRoleString;
    }

    /**
     * @param personQuickAddCertRoleString the personQuickAddCertRoleString to
     * set
     */
    public void setPersonQuickAddCertRoleString(String personQuickAddCertRoleString) {
        this.personQuickAddCertRoleString = personQuickAddCertRoleString;
    }

    /**
     * @return the dateForValidationExpiry
     */
    public LocalDate getDateForValidationExpiry() {
        return dateForValidationExpiry;
    }

    /**
     * @param dateForValidationExpiry the dateForValidationExpiry to set
     */
    public void setDateForValidationExpiry(LocalDate dateForValidationExpiry) {
        this.dateForValidationExpiry = dateForValidationExpiry;
    }

    /**
     * @return the dateForValidationIssuance
     */
    public LocalDate getDateForValidationIssuance() {
        return dateForValidationIssuance;
    }

    /**
     * @param dateForValidationIssuance the dateForValidationIssuance to set
     */
    public void setDateForValidationIssuance(LocalDate dateForValidationIssuance) {
        this.dateForValidationIssuance = dateForValidationIssuance;
    }

    /**
     * @return the dateForValidationFinFinal
     */
    public LocalDate getDateForValidationFinFinal() {
        return dateForValidationFinFinal;
    }

    /**
     * @param dateForValidationFinFinal the dateForValidationFinFinal to set
     */
    public void setDateForValidationFinFinal(LocalDate dateForValidationFinFinal) {
        this.dateForValidationFinFinal = dateForValidationFinFinal;
    }

    /**
     * @return the dateForValidationFinRe
     */
    public LocalDate getDateForValidationFinRe() {
        return dateForValidationFinRe;
    }

    /**
     * @param dateForValidationFinRe the dateForValidationFinRe to set
     */
    public void setDateForValidationFinRe(LocalDate dateForValidationFinRe) {
        this.dateForValidationFinRe = dateForValidationFinRe;
    }

    /**
     * @return the dateForValidationFinInit
     */
    public LocalDate getDateForValidationFinInit() {
        return dateForValidationFinInit;
    }

    /**
     * @param dateForValidationFinInit the dateForValidationFinInit to set
     */
    public void setDateForValidationFinInit(LocalDate dateForValidationFinInit) {
        this.dateForValidationFinInit = dateForValidationFinInit;
    }

    /**
     * @return the dateForValidationApplication
     */
    public LocalDate getDateForValidationApplication() {
        return dateForValidationApplication;
    }

    /**
     * @param dateForValidationApplication the dateForValidationApplication to
     * set
     */
    public void setDateForValidationApplication(LocalDate dateForValidationApplication) {
        this.dateForValidationApplication = dateForValidationApplication;
    }

    /**
     * @return the permitIssuanceFollowupEventDaysFromToday
     */
    public int getPermitIssuanceFollowupEventDaysFromToday() {
        return permitIssuanceFollowupEventDaysFromToday;
    }

    /**
     * @param permitIssuanceFollowupEventDaysFromToday the
     * permitIssuanceFollowupEventDaysFromToday to set
     */
    public void setPermitIssuanceFollowupEventDaysFromToday(int permitIssuanceFollowupEventDaysFromToday) {
        this.permitIssuanceFollowupEventDaysFromToday = permitIssuanceFollowupEventDaysFromToday;
    }

    /**
     * @return the permitIssuanceCreateFollowupEvent
     */
    public boolean isPermitIssuanceCreateFollowupEvent() {
        return permitIssuanceCreateFollowupEvent;
    }

    /**
     * @param permitIssuanceCreateFollowupEvent the
     * permitIssuanceCreateFollowupEvent to set
     */
    public void setPermitIssuanceCreateFollowupEvent(boolean permitIssuanceCreateFollowupEvent) {
        this.permitIssuanceCreateFollowupEvent = permitIssuanceCreateFollowupEvent;
    }

    /**
     * @return the permitIssuanceTS
     */
    public LocalDateTime getPermitIssuanceTS() {
        return permitIssuanceTS;
    }

    /**
     * @param permitIssuanceTS the permitIssuanceTS to set
     */
    public void setPermitIssuanceTS(LocalDateTime permitIssuanceTS) {
        this.permitIssuanceTS = permitIssuanceTS;
    }

    /**
     * @return the permitIssuancePersonTarget
     */
    public HumanLink getPermitIssuancePersonTarget() {
        return permitIssuancePersonTarget;
    }

    /**
     * @param permitIssuancePersonTarget the permitIssuancePersonTarget to set
     */
    public void setPermitIssuancePersonTarget(HumanLink permitIssuancePersonTarget) {
        this.permitIssuancePersonTarget = permitIssuancePersonTarget;
    }

    /**
     * @return the permitIssuanceMethod
     */
    public String getPermitIssuanceMethod() {
        return permitIssuanceMethod;
    }

    /**
     * @param permitIssuanceMethod the permitIssuanceMethod to set
     */
    public void setPermitIssuanceMethod(String permitIssuanceMethod) {
        this.permitIssuanceMethod = permitIssuanceMethod;
    }

    /**
     * @return the permissionsAllowOccPeriodTypeChange
     */
    public boolean isPermissionsAllowOccPeriodTypeChange() {
        return permissionsAllowOccPeriodTypeChange;
    }

    /**
     * @param permissionsAllowOccPeriodTypeChange the
     * permissionsAllowOccPeriodTypeChange to set
     */
    public void setPermissionsAllowOccPeriodTypeChange(boolean permissionsAllowOccPeriodTypeChange) {
        this.permissionsAllowOccPeriodTypeChange = permissionsAllowOccPeriodTypeChange;
    }

    /**
     * @return the editModeOccPeriodManager
     */
    public boolean isEditModeOccPeriodManager() {
        return editModeOccPeriodManager;
    }

    /**
     * @param editModeOccPeriodManager the editModeOccPeriodManager to set
     */
    public void setEditModeOccPeriodManager(boolean editModeOccPeriodManager) {
        this.editModeOccPeriodManager = editModeOccPeriodManager;
    }

    /**
     * @return the editModeOccPeriodType
     */
    public boolean isEditModeOccPeriodType() {
        return editModeOccPeriodType;
    }

    /**
     * @param editModeOccPeriodType the editModeOccPeriodType to set
     */
    public void setEditModeOccPeriodType(boolean editModeOccPeriodType) {
        this.editModeOccPeriodType = editModeOccPeriodType;
    }

    /**
     * @return the permissionsAllowOccPeriodManagerChange
     */
    public boolean isPermissionsAllowOccPeriodManagerChange() {
        return permissionsAllowOccPeriodManagerChange;
    }

    /**
     * @param permissionsAllowOccPeriodManagerChange the
     * permissionsAllowOccPeriodManagerChange to set
     */
    public void setPermissionsAllowOccPeriodManagerChange(boolean permissionsAllowOccPeriodManagerChange) {
        this.permissionsAllowOccPeriodManagerChange = permissionsAllowOccPeriodManagerChange;
    }

    /**
     * @return the occPeriodManagerCandidateList
     */
    public List<User> getOccPeriodManagerCandidateList() {
        return occPeriodManagerCandidateList;
    }

    /**
     * @param occPeriodManagerCandidateList the occPeriodManagerCandidateList to
     * set
     */
    public void setOccPeriodManagerCandidateList(List<User> occPeriodManagerCandidateList) {
        this.occPeriodManagerCandidateList = occPeriodManagerCandidateList;
    }

    /**
     * @return the occPeriodManagerSelected
     */
    public User getOccPeriodManagerSelected() {
        return occPeriodManagerSelected;
    }

    /**
     * @param occPeriodManagerSelected the occPeriodManagerSelected to set
     */
    public void setOccPeriodManagerSelected(User occPeriodManagerSelected) {
        this.occPeriodManagerSelected = occPeriodManagerSelected;
    }

    /**
     * @return the permitIssuancePersonCandidateList
     */
    public List<HumanLink> getPermitIssuancePersonCandidateList() {
        return permitIssuancePersonCandidateList;
    }

    /**
     * @param permitIssuancePersonCandidateList the
     * permitIssuancePersonCandidateList to set
     */
    public void setPermitIssuancePersonCandidateList(List<HumanLink> permitIssuancePersonCandidateList) {
        this.permitIssuancePersonCandidateList = permitIssuancePersonCandidateList;
    }

    /**
     * @return the permissionsAllowCurrentOccPermitAmendment
     */
    public boolean isPermissionsAllowCurrentOccPermitAmendment() {
        return permissionsAllowCurrentOccPermitAmendment;
    }

    /**
     * @param permissionsAllowCurrentOccPermitAmendment the
     * permissionsAllowCurrentOccPermitAmendment to set
     */
    public void setPermissionsAllowCurrentOccPermitAmendment(boolean permissionsAllowCurrentOccPermitAmendment) {
        this.permissionsAllowCurrentOccPermitAmendment = permissionsAllowCurrentOccPermitAmendment;
    }

    /**
     * @return the currentOccPermitPreAmendmentFields
     */
    public OccPermitPreAmendmentFields getCurrentOccPermitPreAmendmentFields() {
        return currentOccPermitPreAmendmentFields;
    }

    /**
     * @param currentOccPermitPreAmendmentFields the
     * currentOccPermitPreAmendmentFields to set
     */
    public void setCurrentOccPermitPreAmendmentFields(OccPermitPreAmendmentFields currentOccPermitPreAmendmentFields) {
        this.currentOccPermitPreAmendmentFields = currentOccPermitPreAmendmentFields;
    }

}
