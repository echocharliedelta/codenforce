/*
 * Copyright (C) 2019 Nathan Dietz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.occupancy.application;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.Icon;
import com.tcvcog.tcvce.entities.IntensityClass;
import com.tcvcog.tcvce.entities.IntensitySchema;
import com.tcvcog.tcvce.integration.SystemIntegrator;
import java.io.Serializable;
import java.util.ArrayList;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Backing bean
 * @author Ellen Bascomb of Apartment 31Y
 */
public class IntensityBB extends BackingBeanUtils implements Serializable {

    private IntensityClass currentIntensityClass;
    private boolean editModeCurrentIntensityClass;
    
    private List<IntensityClass> intensityClassList;
    private List<IntensitySchema> schemaList;
    private List<Icon> iconList;
    

    private boolean editing;

    public IntensityBB() {

    }

    @PostConstruct
    public void initBean() {
        SystemIntegrator si = getSystemIntegrator();
        refreshIntensityAndSchemaList();
          try {
            iconList = si.getIconList();
        } catch (IntegrationException ex) {
            System.out.println(ex);
        }
        if(getIntensityClassList() != null && !intensityClassList.isEmpty()){
                currentIntensityClass = getIntensityClassList().get(0);
        }
        

    }
    
    /**
     * Asks the coordinator for fresh schema and intensity lists
     */
    private void refreshIntensityAndSchemaList(){
        SystemIntegrator si = getSystemIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        
        try {
            intensityClassList = sc.getIntensityClassList();
            schemaList = si.getIntensitySchemaList();

        } catch (IntegrationException ex) {
            System.out.println(ex.toString());
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to load schema list",
                            "This must be corrected by the system administrator"));
        }
    }
    
    /**
     * Fetches a new instance of the current intensity list
     */
    private void refreshCurrentIntensityClass(){
        SystemCoordinator sc = getSystemCoordinator();
        if(currentIntensityClass != null){
            try {
                currentIntensityClass = sc.getIntensityClass(currentIntensityClass.getClassID());
            } catch (IntegrationException ex) {
                System.out.println(ex.toString());
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Unable to refresh intensity class",
                                "This must be corrected by the system administrator"));
            }
            
        }
    }
    
    /**
     * Listener for user requests to view an intensity class from the master list
     * @param ic 
     */
    public void onViewEditIntensityClass(IntensityClass ic){
        currentIntensityClass = ic;
        System.out.println("IntensityBB.onSelectIntensityClass | class ID: " + currentIntensityClass.getClassID());
        
    }
    
    /**
     * Listener for user requests to insert or update an intensity class
     * @param ev 
     */
    public void toggleIntensityClassEdit(ActionEvent ev){
        System.out.println("IntensityBB.toggleIntensityClassEdit");
        SystemCoordinator sc = getSystemCoordinator();
        if(editModeCurrentIntensityClass){
            // we have a fresh insert to do
            if(currentIntensityClass != null && currentIntensityClass.getClassID() == 0){
                try {
                    int freshID = insertNewIntensityClass();
                    currentIntensityClass = sc.getIntensityClass(freshID);
                    // we need to do an update operation
                } catch (IntegrationException ex) {
                    System.out.println(ex);
                    getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Unable to refresh intensity class",
                                "This must be corrected by the system administrator"));
                }
            } else {
                commitEdits();
                refreshCurrentIntensityClass();
            }
            refreshIntensityAndSchemaList();
        }
        editModeCurrentIntensityClass = !editModeCurrentIntensityClass;
    }

    /**
     * internal caller to coordinator for intensity updates
     */
    private void commitEdits() {

        SystemCoordinator sc = getSystemCoordinator();

        try {
            sc.editIntensityClass(currentIntensityClass);
        } catch (IntegrationException | BObStatusException  ex) {
            System.out.println(ex.toString());
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to update Intensity Classes.",
                            "This must be corrected by the System Administrator"));
        }

    }

    /**
     * Listener for user requests to abort the intensity edit process
     */
    public void onAbortIntensityClassEdit() {
        editModeCurrentIntensityClass = false;
    }
    
    /**
     * Begins the process of creating a new intensity class
     * by asking the SysetmCoordinator for a skeleton object (minus an ID)
     * 
     * @param ev 
     */
    public void onCreateIntensityClassInit(ActionEvent ev){
        SystemCoordinator sc = getSystemCoordinator();
        currentIntensityClass = sc.getIntensityClassSkeleton();
        editModeCurrentIntensityClass = true;
    }

    /**
     * Internal organ for commiting the insert of a fresh intensity class
     */
    private int insertNewIntensityClass() {
        
        SystemCoordinator sc = getSystemCoordinator();
            try {
               return sc.addIntensityClass(currentIntensityClass);
            } catch (IntegrationException ex) {
                System.out.println(ex.toString());
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Unable to insert Intensity Class.",
                                "This must be corrected by the System Administrator"));
                return  0;
            }
    }

  

    /**
     * @return the selectedIntensityClass
     */
    public IntensityClass getSelectedIntensityClass() {
        return currentIntensityClass;
    }

    /**
     * @param selectedIntensityClass the selectedIntensityClass to set
     */
    public void setSelectedIntensityClass(IntensityClass selectedIntensityClass) {
        this.currentIntensityClass = selectedIntensityClass;
    }

    /**
     * @return the intensityList
     */
    public List<IntensityClass> getIntensityList() {
        return getIntensityClassList();
    }

    /**
     * @param intensityList the intensityList to set
     */
    public void setIntensityList(List<IntensityClass> intensityList) {
        this.setIntensityClassList(intensityList);
    }

    /**
     * @return the schemaList
     */
    public List<IntensitySchema> getSchemaList() {
        return schemaList;
    }

    /**
     * @param schemaList the schemaList to set
     */
    public void setSchemaList(List<IntensitySchema> schemaList) {
        this.schemaList = schemaList;
    }

    /**
     * @return the iconList
     */
    public List<Icon> getIconList() {
        return iconList;
    }

    /**
     * @param iconList the iconList to set
     */
    public void setIconList(List<Icon> iconList) {
        this.iconList = iconList;
    }

    /**
     * @return the editing
     */
    public boolean isEditing() {
        return editing;
    }

    /**
     * @param editing the editing to set
     */
    public void setEditing(boolean editing) {
        this.editing = editing;
    }

    /**
     * @return the editModeCurrentIntensityClass
     */
    public boolean isEditModeCurrentIntensityClass() {
        return editModeCurrentIntensityClass;
    }

    /**
     * @param editModeCurrentIntensityClass the editModeCurrentIntensityClass to set
     */
    public void setEditModeCurrentIntensityClass(boolean editModeCurrentIntensityClass) {
        this.editModeCurrentIntensityClass = editModeCurrentIntensityClass;
    }

    /**
     * @return the intensityClassList
     */
    public List<IntensityClass> getIntensityClassList() {
        return intensityClassList;
    }

    /**
     * @param intensityClassList the intensityClassList to set
     */
    public void setIntensityClassList(List<IntensityClass> intensityClassList) {
        this.intensityClassList = intensityClassList;
    }



}
