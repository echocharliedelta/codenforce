/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.occupancy.application;

/**
 * Specifies possible task types
 * @author Ellen Bascomb of Apartment 31Y
 */
public enum TaskTypeEnum {
    OPENING (       "Opening",
                    "Open and configure permit file fields",
                    "meeting_room",
                    ""),
    
    CLOSING (       "Closing",
                    "Triggers the closing of a permit file",
                    "door_back",
                    ""),
    
    MILESTONE (     "Milestone",
                    "Marks an impactful step in the occupancy process not otherwise typed (meaning not an inspection or permit task",
                    "emoji_flags",
                    ""),
    
    BRANCHCHOICE (  "Choice",
                    "Denotes a choice. Sucessor tasks represent possible choice outcomes (e.g. fail or pass inspection ",
                    "fork_left",
                    ""),
    
    PAYMENT (       "Payment",
                    "Task requires the recording of a payment",
                    "payments",
                    ""),
    
    INSPECTION (    "Inspection",
                    "Task requires conducting a field inspection",
                    "checklist",
                    ""),
    
    PERMIT (        "Permit",
                    "Task requires issuance of a permit",
                    "approval",
                    ""),
    
    LETTER (        "Letter",
                    "Task requires issuance of a letter",
                    "mail",
                    ""),
    
    FILE (          "File",
                    "Task requires uploading and linking a photo or document file",
                    "cloud_download",
                    ""),
    
    DOCUMENTPERSONREVIW ("Document or person review",
                         "Task requires the examination of a document or a perons's status",
                        "lab_research",
                        ""),
    
    COMMUNICATION (     "Communication",
                        "Task requires communication with a person or organization",
                        "call",
                        ""),
    
    EXTERNAL (          "External",
                        "Task must be completed by somebody besides officer, muni, or cog staff",
                        "arrow_left_alt",
                        ""),
    
    CUSTOM (            "Custom",
                        "User specified task not otherwise aligned with an existing type",
                        "function",
                        "");
    
    private final String title;
    private final String description;
    private final String materialIcon;
    private final String completionDialogVar;
    
    private TaskTypeEnum(String t, String d, String mati, String dialog){
        title = t;
        description = d;
        materialIcon = mati;
        completionDialogVar = dialog;
    }
    
    public String getTitle(){
        return title;
    }
    
    public String getDescription(){
        return description;
    }

    /**
     * @return the materialIcon
     */
    public String getMaterialIcon() {
        return materialIcon;
    }

    /**
     * @return the completionDialogVar
     */
    public String getCompletionDialogVar() {
        return completionDialogVar;
    }
    
    
}
