/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.occupancy.application;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.coordinators.EventCoordinator;
import com.tcvcog.tcvce.coordinators.OccupancyCoordinator;
import com.tcvcog.tcvce.coordinators.TaskCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.EventCategory;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.occupancy.OccPeriodType;
import com.tcvcog.tcvce.entities.occupancy.OccPermitType;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 * Backing bean for the task builder
 * @author pierre15
 */
public class TaskManageBB extends BackingBeanUtils {
    
    private TaskChain currentTaskChain;
    private TaskSpecification currentTaskSpec;
    
    private boolean showChainsAllMunis;
    private boolean showChainsInactive;
    
    private boolean editModeTaskChain;
    private boolean editModeTaskSpec;
    
    private List<TaskChain> taskChains;
        
    private List<OccPeriodType> occPeriodTypeList;
    
    private List<TaskSequenceTypeEnum> taskSequenceTypeCandidates;
    private List<TaskTypeEnum> taskTypeCandidates;
    private List<TaskDueDateTypeEnum> taskDueDateTypeCandidates;
    private List<TaskAssignedRoleEnum> taskRoleCandidates;
    private List<EventCategory> eventCategoryCandidates;
    
    private TaskCoordinator tc;
    
    // test tree stuff
    private TreeNode<TaskSpecification> taskChainTreeRoot;
    
    
    @PostConstruct
    public void initBean(){
        tc = getTaskCoordinator();
        OccupancyCoordinator oc = getOccupancyCoordinator();
        EventCoordinator ec = getEventCoordinator();
        try {
            
            occPeriodTypeList = oc.getOccPeriodTypeList(null, false);
            refreshTaskChainList();
            if(taskChains != null && !taskChains.isEmpty()){
                currentTaskChain = taskChains.get(0);
                refreshCurrentTaskChain();
            }
            
            taskSequenceTypeCandidates = tc.getTaskSequenecTypeEnumValues();
            taskTypeCandidates = tc.getTaskTypeEnumValues();
            taskDueDateTypeCandidates = tc.getTaskDueDateTypeEnumValues();
            taskRoleCandidates = tc.getTaskAssignedRoleEnumValues();
            eventCategoryCandidates = ec.getEventCategoryList();
            
            editModeTaskChain = false;
            editModeTaskSpec = false;
            showChainsAllMunis = true;
            showChainsInactive = false;

        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }
        
    }
    
    /**
     * Test function for sample tree
     */
    private void initTree(){
        taskChainTreeRoot = new DefaultTreeNode("Application", null);
        TreeNode reviewApp = new DefaultTreeNode("Review application", taskChainTreeRoot);
        TreeNode schedule = new DefaultTreeNode("Schedule inspection", reviewApp);
        TreeNode conduct = new DefaultTreeNode("Conduct inspection", schedule);
        TreeNode passFIN = new DefaultTreeNode("Passed inspection", conduct);
        TreeNode failFIN = new DefaultTreeNode("Fail inspection", conduct);
        TreeNode reinspect = new DefaultTreeNode("Reinspection", failFIN);
        TreeNode permit = new DefaultTreeNode("Generate permit", passFIN);
        TreeNode passReFIN = new DefaultTreeNode("Passed Reinspection", reinspect);
        TreeNode failReFIN = new DefaultTreeNode("Fail Reinspection", reinspect);

        
    }
    
    /**
     * Gets a frseh list of task chains from the coordinator
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public void refreshTaskChainList() throws IntegrationException, BObStatusException{
        Municipality m = null;
        if(!showChainsAllMunis){
            m = getSessionBean().getSessMuni();
        }
        taskChains = tc.getTaskChainList(m, showChainsInactive);
    }
    
    /**
     * Gets a new copy of the current task chain
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    private void refreshCurrentTaskChain() throws BObStatusException, IntegrationException{
        if(currentTaskChain != null && currentTaskChain.getChainID() != 0){
            currentTaskChain = tc.getTaskChain(currentTaskChain.getChainID());
        }
        taskChainTreeRoot = tc.buildTreeFromTaskChain(currentTaskChain);
        
    }
    
    
    private void refreshCurrenTaskSpec() throws BObStatusException, IntegrationException{
        if(currentTaskSpec != null && currentTaskSpec.getTaskSpecID() != 0){
            currentTaskSpec = tc.getTaskSpecification(currentTaskSpec.getTaskSpecID());
        }
        
    }
    
    /**
     * Listener for user requests to view a task chain
     * @param chain 
     */
    public void viewTaskChain(TaskChain chain){
        currentTaskChain = chain;
        try {
            refreshCurrentTaskChain();
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
        } 
    }
    
    /**
     * Listener for user requests to view a task spec
     * @param spec 
     */
    public void viewTaskSpec(TaskSpecification spec){
        currentTaskSpec = spec;
    }
    
    
    /**
     * Listener for user requests to start the task chain process
     * @param ev 
     */
    public void onTaskChainCreateInit(ActionEvent ev){
        currentTaskChain = tc.getTaskChainSkeleton(getSessionBean().getSessMuni());
        editModeTaskChain = true;
    }

    /**
     * Listener for user requests to create a task spec
     * @param ev 
     */
    public void onTaskSpecCreateInit(ActionEvent ev){
        try {
            currentTaskSpec = tc.getTaskSpecificationSkeleton(getSessionBean().getSessUser());
        } catch (BObStatusException ex) {
            System.out.println(ex);
        }
        editModeTaskSpec = true;
    }

    
    /**
     * Listener for users to click the edit/save button fo task chains
     * @param ev 
     */
    public void toggleTaskChainAddEdit(ActionEvent ev){
        if(editModeTaskChain){
            try{
                if(Objects.nonNull(currentTaskChain) && currentTaskChain.getChainID() == 0){
                    int freshID = tc.insertTaskChain(currentTaskChain, getSessionBean().getSessUser());
                    currentTaskChain.setChainID(freshID);
                    getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Success! Inserted new task chain with ID " + freshID, ""));
                } else {
                    tc.updateTaskChainMetadata(currentTaskChain, getSessionBean().getSessUser());
                    getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Success! Update of task chain ID " + currentTaskChain.getChainID(), ""));
                }
                refreshCurrentTaskChain();
                refreshTaskChainList();
            } catch (IntegrationException | BObStatusException | AuthorizationException ex){
                System.out.println(ex);
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Error adding or editing current task chain" + currentTaskChain.getChainID(), ""));
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Error adding or editing current task chain" + currentTaskChain.getChainID(), ""));
            } 
        }
        editModeTaskChain = !editModeTaskChain;
    }
    
    /**
     * Listener for user requests to abort the task chain edit operation
     * @param ev 
     */
    public void onAbortTaskChainEditOperation(ActionEvent ev){
        editModeTaskChain = false;
        getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                "Edit abort of task chain" + currentTaskChain.getChainID(), ""));
    }
    
    /**
     * Listener for user requests to abort the task spec edit operation
     * @param ev 
     */
    public void onAbortTaskSpecEditOperation(ActionEvent ev){
        editModeTaskSpec = false;
        getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                "Task spec edit abort" + currentTaskSpec.getTaskSpecID(), ""));
    }
    
    /**
     * Listener for user requests to begin the view/edit operation on a task spec
     * @param spec 
     */
    public void onTaskSpecViewEdit(TaskSpecification spec){
        currentTaskSpec = spec;
    }
    
    /**
     * Listener for users to click the edit/save button for task specifications
     * @param ev 
     */
    public void toggleTaskSpecAddEdit(ActionEvent ev){
        if(editModeTaskSpec){
            try{
                if(Objects.nonNull(currentTaskSpec) && currentTaskSpec.getTaskSpecID() == 0){
                    int freshid = tc.insertTaskSpecification(currentTaskSpec, currentTaskChain, getSessionBean().getSessUser());
                    currentTaskSpec.setTaskSpecID(freshid);
                    getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Success! Inserted task spec with ID " + freshid, ""));
                } else {
                    tc.updateTaskSpecification(currentTaskSpec, getSessionBean().getSessUser());
                    getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Success! Update of task spec with ID " + currentTaskSpec.getTaskSpecID(), ""));
                }
                refreshCurrenTaskSpec();
                refreshCurrentTaskChain();
                
            } catch (IntegrationException | BObStatusException | AuthorizationException ex){
                System.out.println(ex);
                getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Fatal error inserting or updating task specification", ""));
            } 
        }
        editModeTaskSpec = !editModeTaskSpec;
    }
    
    /**
     * Listener for users to start the deac process of a task chain
     * @param ev 
     */
    public void onDeacTaskChainInit(ActionEvent ev){
        // nothing to do
        System.out.println("TaskManageBB.onDeacTaskChainInit");
    }
    
    /**
     * Listener for user requests to complete the task chain deactivation process
     * @param ev 
     */
    public void onTaskChainDeacCommit(ActionEvent ev){
        try {
            tc.deactivateTaskChain(currentTaskChain, getSessionBean().getSessUser());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Successfully deactivated current task chain", ""));
            refreshCurrentTaskChain();
            refreshTaskChainList();
        } catch (BObStatusException | AuthorizationException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Fatal error deactivating task chain", ""));
            
        } 
    }
    
    /**
     * Listener for users to start the deac process of a task chain
     * @param ev 
     */
    public void onDeacTaskSpecInit(ActionEvent ev){
        // nothing to do
        System.out.println("TaskManageBB.onDeacTaskSpecInit");
    }
    
    /**
     * Listener for user requests to complete the task chain deactivation process
     * @param ev 
     */
    public void onTaskSpecDeacCommit(ActionEvent ev){
        try {
            tc.deactivateTaskSpecification(currentTaskSpec, getSessionBean().getSessUser());
            refreshCurrentTaskChain();
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Successfully deactivated current task spec", ""));
            refreshCurrenTaskSpec();
        } catch (BObStatusException | AuthorizationException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Fatal error deactivating task spec", ""));
            
        } 
    }
    
    
    
    
    
    /*************************************************************************
    ***************************** GETTERS AND SETTERS ************************
    *************************************************************************/

    /**
     * @return the currentTaskChain
     */
    public TaskChain getCurrentTaskChain() {
        return currentTaskChain;
    }

    /**
     * @param currentTaskChain the currentTaskChain to set
     */
    public void setCurrentTaskChain(TaskChain currentTaskChain) {
        this.currentTaskChain = currentTaskChain;
    }

    /**
     * @return the currentTaskSpec
     */
    public TaskSpecification getCurrentTaskSpec() {
        return currentTaskSpec;
    }

    /**
     * @param currentTaskSpec the currentTaskSpec to set
     */
    public void setCurrentTaskSpec(TaskSpecification currentTaskSpec) {
        this.currentTaskSpec = currentTaskSpec;
    }

    /**
     * @return the taskChains
     */
    public List<TaskChain> getTaskChains() {
        return taskChains;
    }

    /**
     * @param taskChains the taskChains to set
     */
    public void setTaskChains(List<TaskChain> taskChains) {
        this.taskChains = taskChains;
    }

    /**
     * @return the occPeriodTypeList
     */
    public List<OccPeriodType> getOccPeriodTypeList() {
        return occPeriodTypeList;
    }

    /**
     * @param occPeriodTypeList the occPeriodTypeList to set
     */
    public void setOccPeriodTypeList(List<OccPeriodType> occPeriodTypeList) {
        this.occPeriodTypeList = occPeriodTypeList;
    }

   

    /**
     * @return the taskSequenceTypeCandidates
     */
    public List<TaskSequenceTypeEnum> getTaskSequenceTypeCandidates() {
        return taskSequenceTypeCandidates;
    }

    /**
     * @param taskSequenceTypeCandidates the taskSequenceTypeCandidates to set
     */
    public void setTaskSequenceTypeCandidates(List<TaskSequenceTypeEnum> taskSequenceTypeCandidates) {
        this.taskSequenceTypeCandidates = taskSequenceTypeCandidates;
    }

    /**
     * @return the taskTypeCandidates
     */
    public List<TaskTypeEnum> getTaskTypeCandidates() {
        return taskTypeCandidates;
    }

    /**
     * @param taskTypeCandidates the taskTypeCandidates to set
     */
    public void setTaskTypeCandidates(List<TaskTypeEnum> taskTypeCandidates) {
        this.taskTypeCandidates = taskTypeCandidates;
    }

    /**
     * @return the taskDueDateTypeCandidates
     */
    public List<TaskDueDateTypeEnum> getTaskDueDateTypeCandidates() {
        return taskDueDateTypeCandidates;
    }

    /**
     * @param taskDueDateTypeCandidates the taskDueDateTypeCandidates to set
     */
    public void setTaskDueDateTypeCandidates(List<TaskDueDateTypeEnum> taskDueDateTypeCandidates) {
        this.taskDueDateTypeCandidates = taskDueDateTypeCandidates;
    }

    /**
     * @return the taskRoleCandidates
     */
    public List<TaskAssignedRoleEnum> getTaskRoleCandidates() {
        return taskRoleCandidates;
    }

    /**
     * @param taskRoleCandidates the taskRoleCandidates to set
     */
    public void setTaskRoleCandidates(List<TaskAssignedRoleEnum> taskRoleCandidates) {
        this.taskRoleCandidates = taskRoleCandidates;
    }

    /**
     * @return the editModeTaskSpec
     */
    public boolean isEditModeTaskSpec() {
        return editModeTaskSpec;
    }

    /**
     * @param editModeTaskSpec the editModeTaskSpec to set
     */
    public void setEditModeTaskSpec(boolean editModeTaskSpec) {
        this.editModeTaskSpec = editModeTaskSpec;
    }

    /**
     * @return the editModeTaskChain
     */
    public boolean isEditModeTaskChain() {
        return editModeTaskChain;
    }

    /**
     * @param editModeTaskChain the editModeTaskChain to set
     */
    public void setEditModeTaskChain(boolean editModeTaskChain) {
        this.editModeTaskChain = editModeTaskChain;
    }

    /**
     * @return the showChainsInactive
     */
    public boolean isShowChainsInactive() {
        return showChainsInactive;
    }

    /**
     * @param showChainsInactive the showChainsInactive to set
     */
    public void setShowChainsInactive(boolean showChainsInactive) {
        this.showChainsInactive = showChainsInactive;
    }

    /**
     * @return the showChainsAllMunis
     */
    public boolean isShowChainsAllMunis() {
        return showChainsAllMunis;
    }

    /**
     * @param showChainsAllMunis the showChainsAllMunis to set
     */
    public void setShowChainsAllMunis(boolean showChainsAllMunis) {
        this.showChainsAllMunis = showChainsAllMunis;
    }

    /**
     * @return the eventCategoryCandidates
     */
    public List<EventCategory> getEventCategoryCandidates() {
        return eventCategoryCandidates;
    }

    /**
     * @param eventCategoryCandidates the eventCategoryCandidates to set
     */
    public void setEventCategoryCandidates(List<EventCategory> eventCategoryCandidates) {
        this.eventCategoryCandidates = eventCategoryCandidates;
    }

    /**
     * @return the treeRoot
     */
    public TreeNode getTreeRoot() {
        return taskChainTreeRoot;
    }

    /**
     * @param treeRoot the treeRoot to set
     */
    public void setTreeRoot(TreeNode treeRoot) {
        this.taskChainTreeRoot = treeRoot;
    }
}
