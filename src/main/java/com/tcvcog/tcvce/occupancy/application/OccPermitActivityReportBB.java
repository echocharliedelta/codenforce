/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.occupancy.application;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.coordinators.OccupancyCoordinator;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.domain.SearchException;
import com.tcvcog.tcvce.entities.reports.ReportConfigOccActivity;
import jakarta.annotation.PostConstruct;
import jakarta.faces.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Backs the creation of an occupancy activity report from Date X to Date Y
 * @author pierre15
 */
public class OccPermitActivityReportBB 
        extends BackingBeanUtils{

    private ReportConfigOccActivity currentOccActivityReportConfig;
    
    public OccPermitActivityReportBB() {
    }
    
    @PostConstruct
    public void initBean() {
        System.out.println("OccPermitActivityReportBB.initBean");
        // setup blank report when we load our BB
        onOccPermitActivityReportInit(null);
    }
    
    /**
     * Listener to start the report generation process
     * @param ev 
     */
    public void onOccPermitActivityReportInit(ActionEvent ev){
        OccupancyCoordinator oc = getOccupancyCoordinator();
        currentOccActivityReportConfig = oc.getOccPermitActivityReportSkeleton(getSessionBean().getSessUser());
        System.out.println("OccPermitActivityReportBB.onOccPermitActivityReportInit");
        
        
    }
    
    /**
     * Listener to proceed with report generation from params
     * @return  
     */
    public String onOccPermitActivityReportGenerate(){
        OccupancyCoordinator oc = getOccupancyCoordinator();
        
        try {
            getSessionBean().setReportConfigOccActivity(oc.buildOccActivityReport(currentOccActivityReportConfig, getSessionBean().getSessUser()));
        } catch (BObStatusException | BlobException | IntegrationException | SearchException ex) {
            System.out.println(ex);
        } 
        System.out.println("OccPermitActivityReportBB.onOccPermitActivityReportGenerate");
        
        return "occPermittingActivityReport";
        
    }
    
    /**
     * Listener to abort report process
     * @param ev 
     */
    public void onActionAbort(ActionEvent ev){
        
        
    }
    
    
    // GETTERS AND SETTERS

    /**
     * @return the currentOccActivityReportConfig
     */
    public ReportConfigOccActivity getCurrentOccActivityReportConfig() {
        return currentOccActivityReportConfig;
    }

    /**
     * @param currentOccActivityReportConfig the currentOccActivityReportConfig to set
     */
    public void setCurrentOccActivityReportConfig(ReportConfigOccActivity currentOccActivityReportConfig) {
        this.currentOccActivityReportConfig = currentOccActivityReportConfig;
    }
    
    
    
}
