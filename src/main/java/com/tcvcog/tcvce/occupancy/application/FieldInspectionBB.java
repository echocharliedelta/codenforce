package com.tcvcog.tcvce.occupancy.application;

import com.tcvcog.tcvce.application.*;
import com.tcvcog.tcvce.session.SessionBean;
import com.tcvcog.tcvce.coordinators.*;
import com.tcvcog.tcvce.domain.*;
import com.tcvcog.tcvce.entities.*;
import com.tcvcog.tcvce.entities.occupancy.*;
import com.tcvcog.tcvce.entities.reports.ReportConfigOccInspection;
import com.tcvcog.tcvce.util.Constants;
import com.tcvcog.tcvce.util.viewoptions.ViewOptionsOccChecklistItemsEnum;
import jakarta.annotation.PostConstruct;
import java.io.Serializable;
import java.util.*;
import java.util.Objects;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.ExternalContext;
import jakarta.faces.context.FacesContext;
import jakarta.faces.event.ActionEvent;
import jakarta.servlet.http.HttpServletRequest;
import java.time.LocalTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.PrimeFaces;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;
import com.tcvcog.tcvce.application.interfaces.IFaceActivatableBOB;

/**
 * The premier backing bean for field inspections workflow. This applies to both
 * occupancy periods and CE cases!!
 *
 * @author jurplel (& ellen bascomb starting Jan 2022)
 */
public class FieldInspectionBB
        extends BackingBeanUtils
        implements Serializable {

    static final String REQUEST_PARAM_KEY_FOR_INSPECTION_LIST_COMPONENT = "initiating-inspection-list-component-id";
    static final String REQUEST_PARAM_KEY_FOR_INSPECTION_CREATE_FORM = "inspection-createedit-formpath";
    
    static final float INCHES_PER_FOOT = 12.0f;
    static final float CUBIC_INCHES_PER_CUBIC_FEET = 144.0f;

    private IFace_inspectable currentInspectable;
    private FieldInspection currentInspection;
    private OccInspectedSpace currentInspectedSpace;
    private OccInspectedSpaceElement currentInspectedSpaceElement;
    private OccSpaceTypeChecklistified currentSpaceType;

    /**
     * Toggles between browsing for pass/fail items in tree mode or the original
     * table mode. True means user is in tree mode
     */
    private boolean ordinanceEntryViewTreeMode;

    // UI stuff
    private boolean largeFindingsBoxes;
    private boolean showStoreFindingsLinks;
    
    private boolean filterByFullOrdText;
    private String ordinanceFilterText;
    
    /**
     * @Deprecated due to mess on inspection updates
     */
    private List<OccInsElementGroup> inspectedElementGroupListFiltered;

    private List<OccInspectedSpaceElement> inspectedElementListFiltered;

    private OccChecklistTemplate selectedChecklistTemplate;
    private List<OccChecklistTemplate> checklistTemplateList;

    private List<OccInspectionDetermination> determinationList;
    private OccInspectionDetermination selectedDetermination;
    private int formFailedFINFollowUpWindow;

    // fin creation fields
    
    private List<User> inspectorCandidateList;
    private User selectedInspector;
    private String finCreateEditcomponentForRefresh;
    
    private List<OccInspectionCause> causeList;
    private List<IntensityClass> failSeverityList;

    private String inspectionListComponentForUpdate;
    
    // validation fields: model values won't be updated if validation
    // fails so we have to store them here to see them in the validator
    private LocalTime inspectionTimeStartForValidation;
    private LocalTime inspectionTimeEndForValidation;
    
    
    // Space details
    private OccLocationDescriptor currentLocationDescriptor;
    private boolean dimensionEntryInchesOnly;
    private float dim1LengthFt;
    private float dim1WidthFt;
    private float dim2LengthFt;
    private float dim2WidthFt;
    
    private List<OccLocationDescriptor> allUniqueLocationDescriptors = new ArrayList<>();

    private OccInspectionStatusEnum selectedElementStatusForBatch;
    private boolean useDefaultFindingsOnCurrentOISE;

    private boolean editModeInspectionMetadata;

    // permissions bits
    private boolean permissionAllowAddUpdateInspectionMetadata;
    private boolean permissionAllowInspectionConduct;
    private boolean permissionAllowInspectionFinalize;

//    protected FieldInspectionReInspectionConfig reinspectionConfig;

    private boolean formMigrateFailedItemsOnFinalization;

    private int occPeriodIDFortransferFormField;
    private OccInspectionDispatch currentDispatch;

    // field inspection report options
    private ReportConfigOccInspection reportConfigFIN;
    private List<ViewOptionsOccChecklistItemsEnum> ordinanceViewOptionsList;
    private List<HumanLink> humanLinkCandidateList;
    private PropertyDataHeavy inspectablePropertyHost;

    private boolean editModeDispatch;

    // ****************************************
    // field-initiated field inspections tools
    // ****************************************
    private List<FieldInspectionLight> fieldInitFinList;
    private FieldInspection finForRouting;
    private boolean finRoutingUsingOccPathway;
    private MenuModel finRoutingStepsModel;
    private int finRoutingActiveStep;
    private PropertyDataHeavy fieldRoutingPropertyTarget;
    private String rawLocationHolding;
    
    private boolean finRevertXMuni;
    private List<Municipality> finRevertXMuniCandidateList;
    private Municipality finRevertXMuniSelected;
    

    // BEAN INIT
    /**
     * Initialization pathway for this bean.
     *
     * @throws BObStatusException
     */
    @PostConstruct
    public void initBean() {
        System.out.println("FieldInspectionBB.initBean");
        try {

            // pull from session-scoped SessionInspectionConductor 
            registerCurrentInspectable(null, true);
            // standard setup jazz 
            initCandidateObjectLists();
            initChecklistTemplates();
            initSeverityClassList();
            configurePermissions();
            initInspectionUISettings();

        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * sets basic UI params
     */
    private void initInspectionUISettings() {
        // UI Settings
        ordinanceEntryViewTreeMode = true;
        formMigrateFailedItemsOnFinalization = true;
        useDefaultFindingsOnCurrentOISE = true;
        dimensionEntryInchesOnly = false;
        largeFindingsBoxes = true;

    }

    // ************************************************************************
    // ******   PERMISSIONS AND CORE OBJECT REGISTRATION  *********************
    // ************************************************************************
    /**
     * Sets permissions bits based on the current inspection, inspectable, and
     * user ranks
     */
    private void configurePermissions() {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        // Set permissions bits from coordinator
        permissionAllowAddUpdateInspectionMetadata = oic.permissionsCheckpointAddUpdateFieldInspectionMetadata(getSessionBean().getSessUser());
        permissionAllowInspectionConduct = oic.permissionsCheckpointConductFieldInspection(getSessionBean().getSessUser());
        permissionAllowInspectionFinalize = oic.permissionsCheckpointFinalizeFieldInspection(getSessionBean().getSessUser());

    }

   

   

    /**
     * Used when the inspectable is already loaded, such as during dash
     * inspection init
     *
     * @param fin
     */
    public void onViewEditInspection(FieldInspectionLight fin) {
        onViewEditInspectionLinkClick(fin);
        editModeInspectionMetadata = false;
    }

    /**
     * Listener for user to view an inspection from either the CECase profile or
     * occ period profile. This method sets up our currentInspection and current
     * inspectable and current dispatch. It also sets our session objects
     *
     * @param fi cannot be null
     */   
    public void onViewEditInspectionLinkClick(FieldInspectionLight fi) {

        if (fi == null) {
            System.out.println("FieldInspectionBB.onViewEditInspectionLinkClick | called with null inspction");
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error: No inspection specified! ", ""));
            return;
        }
        extractComponentForReloadFromRequest();
        // pass the logic off to internal organ that will refresh and install the holder
        registerAndRefreshCurrentFieldInspectionAndInspectable(fi);
    }

    /**
     * A hacky way to only tell my inspection UI to update the event panel on
     * pages that have an event panel which is notably NOT the dashboard.
     *
     * @return
     */
    public String getEventListPanelIDForUpdateOnHolderSpecificPagesOnly() {
        FocusedObjectEnum domain = getSessionBean().getSessionDomain();
        if (domain != null && (domain == FocusedObjectEnum.CECASE || domain == FocusedObjectEnum.PROPERTY || domain == FocusedObjectEnum.OCCPERIOD)) {
            return EventCoordinator.EVENT_PANEL_FORM_ID_FOR_REFRESH;
        } else {
            return "";
        }
    }

    /**
     * Asks coordinator for new inspection and finds the current space type in
     * the returned freshly updated inspection object and makes its version of
     * the current space the current one
     *
     * I also tell the SessionBean to refresh the current occ period so the UI
     * just has to say to update any linked UI elements to see updates
     *
     * @param processInspectedSpace when true and current inspected space is not
     * null, i'll make sure that my current inspected space is fresh, too
     *
     */
    private void refreshCurrentInspectionAndRestoreSelectedSpace(boolean processInspectedSpace)
            throws IntegrationException, BObStatusException, BlobException {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        if (currentInspection != null) {
            registerAndRefreshCurrentFieldInspectionAndInspectable(currentInspection);

            if (Objects.nonNull(currentInspection.getAllUniqueLocationDescriptors())) {
                allUniqueLocationDescriptors = currentInspection.getAllUniqueLocationDescriptors();
            }
            System.out.println("FieldInspectionBB.refreshCurrentInspectionAndRestoreSelectedSpace | " + currentInspection.getInspectionID());
            if (currentInspectedSpace != null && processInspectedSpace) {
                currentLocationDescriptor = currentInspectedSpace.getLocation();
                // go find my current space in the inspection and make it the selected one
                for (OccInspectedSpace ois : currentInspection.getInspectedSpaceList()) {
                    if (ois.getInspectedSpaceID() == currentInspectedSpace.getInspectedSpaceID()) {
                        currentInspectedSpace = oic.configureElementDisplay(ois);
                        break;
                    }
                }
//                resetOrdinanceFilterFromCurrentInspectedSpace();
            }
        } else {
            throw new BObStatusException("No current inspection selected; cannot refresh");
        }
        if (currentInspection == null) {
            System.out.println("FieldInspectionBB.refreshCurrentInspectionAndRestoreSelectedSpace | ERROR: NULL inspection after refresh!!!");
        }
    }

     /**
     * Pathway for setting this bean's master currentInspection member and
     * making sure that our currentInspectable matches. I'll also refresh the
     * given fin and install its data heavy subclass.
     *
     * @param finLight
     */
    private void registerAndRefreshCurrentFieldInspectionAndInspectable(FieldInspectionLight fin) {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        if (fin != null) {
            try {
                currentInspection = oic.getFieldInspectionDataHeavy(oic.getOccInspectionLight(fin.getInspectionID()));
                if (currentInspection != null) {
                    // set our inspectable to match the given inspection, allowing 
                    // us to activate inspections from anywhere in the system
                    registerCurrentInspectable(oic.extractInspectionParentInspectable(fin, getSessionBean().getSessUser()), true);
                    // if we have a dispatch in our current inspection, then make it our bean's currentDispatch
                    currentDispatch = currentInspection.getDispatch();
                    getSessionInspectionConductor().setSessFieldInspection(currentInspection);
                    registerCurrentInspectionwithBlobUtilitiesBB();
                    currentInspection.configureSpaceList();
                } else {
                    System.out.println("FieldInspectionBB.onViewEditInspectionLinkClick | NULL currentInspection after view link click!!!");
                }
            } catch (BObStatusException | BlobException | IntegrationException | SearchException ex) {
                System.out.println(ex);
            }

        }
    }
    
     /**
     * Internal organ to setup this class's inspectable and therefore the
     * available
     */
    private void registerCurrentInspectable(IFace_inspectable inspectable, boolean reloadInspectionList) {

        if (inspectable == null) {
            currentInspectable = getSessionInspectionConductor().getSessInspectable();
        } else {
            currentInspectable = inspectable;
        }
        if(currentInspectable == null){
            System.out.println("FieldInspectionBB.registerCurrentInspectable | no current inspectable ");
        } else {
            if(currentInspectable instanceof CECaseDataHeavy csedh){
                getSessionBean().setSessCECase(csedh);
            } else if (currentInspectable instanceof OccPeriodDataHeavy opdh){
                getSessionOccupancyConductor().registerSessionOccPeriod(opdh);
            } else {
                System.out.println("FieldInspectionBB.registerAndRefreshCurrentInspectable | Unknown inspectable type");
            }
        }
        if(reloadInspectionList){
            OccInspectionCoordinator oic = getOccInspectionCoordinator();
            try {
                currentInspectable.setInspectionList(oic.getOccInspectionLightList(currentInspectable));
            } catch (IntegrationException | BObStatusException |  BlobException ex) {
                System.out.println(ex);
            } 
        }
    }
    
    /**
     * Utility for injecting into this bean a PropertyDataHeavy which is the
     * host of the current inspectable
     */
    private void loadPropertyDataHeavyForCurrentInspectable() {
        if (currentInspectable != null) {
            OccInspectionCoordinator oic = getOccInspectionCoordinator();
            try {
                inspectablePropertyHost = oic.getPropertyDataHeavy(currentInspectable, getSessionBean().getSessUser());
                getSessionBean().setSessProperty(inspectablePropertyHost);
                if (inspectablePropertyHost != null) {
                    System.out.println("FieldInspectionBB.loadPropertyDataHeavyForCurrentInspectable | inspectablePropertyHost: " + inspectablePropertyHost.getAddress().getAddressPretty1Line());
                } else {
                    System.out.println("FieldInspectionBB.loadPropertyDataHeavyForCurrentInspectable | unable to build property host");
                }
            } catch (BObStatusException | BlobException | IntegrationException | SearchException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                ex.getMessage(), ""));
            }
        }
    }

    // end registration and permissions
    // ************************************************************************
    // ************            REFRESH, SUPPORT & UTILITIES            ********
    // ************************************************************************
    /**
     * Sets up our severity class list
     */
    private void initSeverityClassList() {
        SystemCoordinator sysCor = getSystemCoordinator();
        try {

            failSeverityList = sysCor.getIntensitySchemaWithClasses(
                    getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("intensityschema_violationseverity"), getSessionBean().getSessMuni())
                    .getClassList();
        } catch (IntegrationException ex) {
            System.out.println(ex);
        }
    }

    /**
     * Joint logic method for forcing a refresh of this inspection's parent
     * which is an event holder to show the updated event status after EITHER a
     * finalization or a removal of finalization
     */
    private void refreshEventsRelatedToCurrentInspection() throws BObStatusException, IntegrationException {
        EventCoordinator ec = getEventCoordinator();
        // get event updates sorted out
        getSessionEventConductor().refreshCalendarAndFollowupBacklog(null);
        IFace_EventHolder holder = ec.castAttemptInspectableToEventHolder(currentInspectable);
        getSessionEventConductor().setSessEventListForRefreshUptake(ec.getEventList(holder));

    }

    /**
     * Extracts all values of th enum OccINspectionStatusEnum
     *
     * @return
     */
    public OccInspectionStatusEnum[] getStatuses() {
        return OccInspectionStatusEnum.values();
    }

    /**
     * organizes basic lists
     */
    private void initCandidateObjectLists() throws IntegrationException, BObStatusException {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        causeList = oic.getOccInspectionCauseListComplete(true, getSessionBean().getSessMuni());
        determinationList = oic.getOccInspectionDeterminationListComplete(true, getSessionBean().getSessMuni());
        ordinanceViewOptionsList = Arrays.asList(ViewOptionsOccChecklistItemsEnum.values());

    }
    
    /**
     * Toggles UI element to store findings
     */
    public void onToggleShowStoreFindingsLink(ActionEvent ev){
        System.out.println("FieldInspectionBB.onToggleShowStoreFindingsLink");
        showStoreFindingsLinks = !showStoreFindingsLinks;
    }

    // end utitlies
    // ************************************************************************
    // ************           INSPECTION CREATION                     *********
    // ************************************************************************
    
    
  
    
  
    
    /**
     * listener to start the inspector change process
     * @param ev 
     */
    public void onInspectorChangeInit(ActionEvent ev){
        System.out.println("FieldInspectionBB.onInspectorChangeInit");
        configureInspectorCandidateList();
        extractComponentInspectionCreateEdit();
        
    }

    /**
     * Builds list of possible inspectors
     */
    private void configureInspectorCandidateList(){
        UserCoordinator uc  = getUserCoordinator();
        try {
            inspectorCandidateList= uc.user_assembleUserListOfficerRequired(getSessionBean().getSessMuni(), true);
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
        } 
    }
    
    /**
     * Injects the chosen inspector into the current inspection
     * @param ev 
     */
    public void onInspectorChangeCommit(ActionEvent ev){
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        
        if(currentInspection != null && selectedInspector != null){
            currentInspection.setInspector(selectedInspector);
            if(currentInspection.getInspectionID() != 0){
                try {
                    oic.updateOccInspection(currentInspection, getSessionBean().getSessUser());
                    refreshCurrentInspectionAndRestoreSelectedSpace(false);
                    refreshInspectionListAndTriggerManagedListReload();
                } catch (IntegrationException | AuthorizationException | BObStatusException | BlobException ex) {
                    System.out.println(ex);
                    getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                ex.getMessage(), ""));
                } 
            }
        } else {
            getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Fatal error updating inspector! Please report issue to admins.", ""));
        }
        
    }
    
     /**
     * Called when a user is attempting to start a new inspection on either an
     * occ period or ce case
     *
     * @param inspectable
     */
    public void onCreateInspectionInitButtonChange(IFace_inspectable inspectable) {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        try {
            currentInspectable = inspectable;
            currentInspection = oic.getOccInspectionSkeleton(getSessionBean().getSessUser(), currentInspectable, getSessionBean().getSessUser());
            System.out.println("FieldInspectionBB.onInspectionCreateInit with sensible defaults ");
            extractComponentForReloadFromRequest();
        } catch (IntegrationException | AuthorizationException | BObStatusException | BlobException | EventException  ex) {
            System.out.println(ex);
              getFacesContext().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                    "Fatal error creating inspection; please report issue to developer", ""));
        } 
    }
    
    /**
     * Completes the inspection creation process; Requires that this class's 
     * current inspection is not null and currentInspectable is not null 
     * and matches the skeleton inspection components. When done, this class's
     * currentInspection will have a DB id and continue to be the CurrentInspection
     * complete with the auto-addable spaces attached and ready to roll.
     * 
     * @param ev
     */
    public void createInspectionCommit(ActionEvent ev) {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();

        try {
            FieldInspection freshFIN = oic.inspectionAction_commenceOccupancyInspection(
                                            currentInspection, 
                                            currentInspectable,
                                            getSessionBean().getSessUser());

            registerAndRefreshCurrentFieldInspectionAndInspectable(freshFIN);
            refreshInspectionListAndTriggerManagedListReload();
            if(currentInspection.getDispatch() != null){
                getSessionInspectionConductor().refreshFieldFinLists(null);
            }

            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Created new inspection!: " + currentInspection.getInspectionID(), ""));
        } catch (InspectionException | IntegrationException | BObStatusException | BlobException | AuthorizationException | EventException  ex) {
            System.out.println("Failed to create new OccInspection: " + ex);
        }
    }

    /**
     * Listener to end the creation process
     * @param ev 
     */
    public void onInspectionCreateAbort(ActionEvent ev){
        System.out.println("FieldInspectionBB.onInspectionCreateAbort");
        
    }
    
    /**
     * Asks the coordinator for a nice new list of field inspections and injects
     * this into the special session spot which managed refresh components will
     * check and inject for active updating.
     */
    private void refreshInspectionListAndTriggerManagedListReload() {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        if (currentInspectable != null) {
            try {
                List<FieldInspectionLight> filist = oic.getOccInspectionLightList(currentInspectable);
                currentInspectable.setInspectionList(filist);
                System.out.println("FieldInspectionBB.refreshInspectionListAndTriggerManagedListReload | filist size: " + filist.size());
                System.out.println("FieldInspectionBB.refreshInspectionListAndTriggerManagedListReload | component for reload: " + inspectionListComponentForUpdate);
                getSessionInspectionConductor().setSessFieldInspectionListForRefresh(filist);
            } catch (IntegrationException | BObStatusException | BlobException ex) {
                System.out.println(ex);
            }
        }
    }

    /**
     * Gets the list of possible checklist template objects and sets the member
     * variable checklistTemplates to its value.
     */
    private void initChecklistTemplates() {
        SessionBean sb = getSessionBean();
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        try {
            checklistTemplateList = oic.getOccChecklistTemplateList(sb.getSessMuni(), false);
        } catch (IntegrationException ex) {
            System.out.println("Failed to acquire list of checklist templates:" + ex);
        }
    }

    /**
     * adaptor listener for starting inspections from the dashboard on occ
     * periods (i.e. permit files) which requires a data heavy version, not this
     * propertyUnitHeavy version so this method assbles the DH and injects it
     * into the session then redirects to the normal front door for inspection
     * init
     *
     * @param per
     */
    public void onCreateInspectionOccPeriodInit(OccPeriod per) {
        if (per != null) {
            OccupancyCoordinator oc = getOccupancyCoordinator();
            onCreateInspectionInitButtonChange(getSessionOccupancyConductor().registerSessionOccPeriod(per));
        }
    }

    /**
     * Adaptor listener for starting a FIN on a cecase from dash which only
     * lists subclass CECaseProeprtyUnitHeavy and the init method needs
     * CECaseDataHeavy, which this method will acquire and then call
     * onCreateInspectionInitButtonChange
     *
     * @param csepuh
     */
    public void onCreateInspectionInitCECasePropUnitHeavy(CECase csepuh) {
        CaseCoordinator cc = getCaseCoordinator();
        if (csepuh != null) {
            try {
                CECaseDataHeavy csedh = cc.cecase_assembleCECaseDataHeavy(csepuh, getSessionBean().getSessUser());
                getSessionBean().setSessCECase(csedh);
                onCreateInspectionInitButtonChange(csedh);
            } catch (BObStatusException | IntegrationException | SearchException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Fatal error creating a new inspection on this cecase! ", ""));
            }
        }
    }

    /**
     * Entry point for starting an inspection from the dashboard start
     * inspection dialog where our inspectable is already loaded
     *
     * @param ev
     */
    public void onBeginInspectionConfig(ActionEvent ev) {
        System.out.println("FieldInspectionBB.onBeginInspectionConfig");
        // nothing to do here but send user to checklist chooser
    }

   

    /**
     * Extracts the component to update after creation or edit of an inspection
     * by the UI
     *
     */
    private void extractComponentForReloadFromRequest() {
        inspectionListComponentForUpdate
                = FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getRequestParameterMap()
                        .get(REQUEST_PARAM_KEY_FOR_INSPECTION_LIST_COMPONENT);
        System.out.println("FieldInspectionBB.extractComponentForReloadFromRequest | Component = " + inspectionListComponentForUpdate);
    }
    

    /**
     * Extracts the component to update after creation or edit of an inspection
     * by the UI
     *
     */
    private void extractComponentInspectionCreateEdit() {
        finCreateEditcomponentForRefresh =
                FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getRequestParameterMap()
                        .get(REQUEST_PARAM_KEY_FOR_INSPECTION_CREATE_FORM);
        System.out.println("FieldInspectionBB.extractComponentInspectionCreateEdit | Component = " + finCreateEditcomponentForRefresh);
    }
    
    /**
     * Listener to start the inspection checklist change process
     * @param ev 
     */
    public void onInspectionChecklistChangeInit(ActionEvent ev){
        System.out.println("FieldInspectionBB.onInspectionChecklistChangeInit");
        extractComponentInspectionCreateEdit();
    }

    /**
     * Listener for the user to be finished selecting a checklist Sets the
     * selected inspector to the current occ period's manager
     *
     * @param ev
     */
    public void onChecklistSelectionCompleteButtonClick(ActionEvent ev) {
        if(currentInspection != null && selectedChecklistTemplate != null){
            currentInspection.setChecklistTemplate(selectedChecklistTemplate);
            System.out.println("FieldInspectionBB.onChecklistSelectionCompleteButtonClick | selected checklist ID: " + selectedChecklistTemplate.getInspectionChecklistID());
            // only if we're not in new inspection creation mode do we write changes and refresh
            if(currentInspection.getInspectionID() != 0){
                try {
                    OccInspectionCoordinator oic = getOccInspectionCoordinator();
                    oic.updateOccInspection(currentInspection, getSessionBean().getSessUser());
                    refreshCurrentInspectionAndRestoreSelectedSpace(false);
                } catch (IntegrationException | BObStatusException | BlobException | AuthorizationException ex) {
                    System.out.println(ex);
                        getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Fatal error updating checklist! ", ""));
                } 
            }
        }
        
    }

   
    
    /**
     * If the current inspectable is an occ period, make the new inspection's
     * inspector by default that period's manager. Similarly, if the current
     * inspectable is a cecase, then make the new inspection's inspector that
     * case's manager;
     */
    private void selectInspectorUsingCurrentInspectionHolder() {
        if (currentInspectable != null) {
            if (currentInspectable instanceof OccPeriod) {
                selectedInspector = getSessionBean().getSessOccPeriod().getManager();
            } else if (currentInspectable instanceof CECase) {
                selectedInspector = getSessionBean().getSessCECase().getManager();
            }
        }
    }

    // end creation
    // ************************************************************************
    // ************         MOBILE DISPATCH                           *********
    // ************************************************************************
    /**
     * Listener for user clicks of the dispatch edit or revoke button on the
     * inspection profile screen
     *
     * @param ev
     */
    public void onToggleDispatchEditMode(ActionEvent ev) {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        if (currentInspection != null) {
            if (editModeDispatch) {
                if (currentDispatch != null) {
                    try {
                        if (currentDispatch.getDispatchID() == 0) {
                            int freshid = oic.insertOccInspectionDispatch(currentInspection, currentDispatch, getSessionBean().getSessUser());
                            System.out.println("FieldInspectionBB.onToggleDispatchEditMode: freshID: " + freshid);
                            currentDispatch.setDispatchID(freshid);
                            getFacesContext().addMessage(null,
                                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                                            "Dispatch inserted! ID number assigned: " + freshid, ""));
                        } else {
                            // we've got an existing dispatch to update
                            oic.updateOccInspectionDispatch(currentDispatch, getSessionBean().getSessUser());
                            getFacesContext().addMessage(null,
                                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                                            "Dispatch updated!", ""));
                        }
                        reloadDispatch();
                    } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
                        System.out.println(ex);
                        getFacesContext().addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                        "Fatal error: inserting or updating dispatch; ", ""));
                    }
                }
                // we need a dispatch skeleton
            } else {
                if (currentDispatch == null) {
                    currentDispatch = oic.getOccInspectionDispatchSkeleton(currentInspection, getSessionBean().getSessUser());
//                    currentInspection.setDispatch(currentDispatch);
                    System.out.println("FieldInspectionBB.onToggleDispatchEditMode: created skeleton");
                }
            }
            editModeDispatch = !editModeDispatch;
        }
    }

    private void reloadDispatch() {
        if (currentDispatch != null && currentDispatch.getDispatchID() != 0) {
            OccInspectionCoordinator oic = getOccInspectionCoordinator();
            try {
                currentDispatch = oic.getOccInspectionDispatch(currentDispatch.getDispatchID());
                currentInspection.setDispatch(currentDispatch);
                System.out.println("FieldInspectionBB.reloadDispatch ID " + currentDispatch.getDispatchID() + ", and injected into inspection!");
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Fatal error: inserting or updating dispatch; ", ""));
            }
        }
    }

    /**
     * Listener for user requests to cancel edits of the dispatch
     *
     * @param ev
     */
    public void onDispatchEditModeAbort(ActionEvent ev) {
        editModeDispatch = false;
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Dispatch edit aborted!", ""));
    }

    /**
     * Listener for user requests to cancel edits of the dispatch
     *
     * @param ev
     */
    public void onDispatchRevoke(ActionEvent ev) {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        try {
            oic.deactivateOccInspectionDispatch(currentDispatch, getSessionBean().getSessUser());
            // clean up from the mess
            refreshCurrentInspectionAndRestoreSelectedSpace(false);
            refreshInspectionListAndTriggerManagedListReload();
            getSessionInspectionConductor().refreshDispatchedInspectionList(null);
        } catch (BObStatusException | IntegrationException | AuthorizationException | BlobException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to revoke dispatch", ""));
        }
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Dispatch has been revoked!", ""));
    }

    // end dispatch 
    // ************************************************************************
    // ************             CORE INFO EDITING                     *********
    // ************************************************************************
    /**
     * Listener for user requests to start or end an occ inspection meta data
     * editing session
     *
     * @param ev
     */
    public void onToggleEditModeInspectionMetadata(ActionEvent ev) {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        System.out.println("FieldInspectionBB.onToggleEditModeInspectionMetadata : current ID " + currentInspection.getInspectionID());
        // if we're hitting the button and we're not in edit mode, don't udpate
        if (editModeInspectionMetadata) {
            try {
                oic.updateOccInspection(currentInspection, getSessionBean().getSessUser());
                System.out.println("FieldInspectionBB.onToggleEditModeInspectionMetadata : Updated inspection ID " + currentInspection.getInspectionID());
                // only do this if we don't get an error in any of our updates
                editModeInspectionMetadata = !editModeInspectionMetadata;
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Updated inspection ID: " + currentInspection.getInspectionID(), ""));
                refreshCurrentInspectionAndRestoreSelectedSpace(false);
                refreshInspectionListAndTriggerManagedListReload();
            } catch (IntegrationException | BObStatusException | BlobException | AuthorizationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                ex.getMessage(), ""));
            }
        } else {
            // turn on edit mode
            editModeInspectionMetadata = !editModeInspectionMetadata;
        }
    }

    /**
     * Listener for user requests to abort the occ inspection metadata update
     * process
     *
     * @param ev
     */
    public void abortEditsOccInspectionMetadata(ActionEvent ev) {
        //turn off edit mode
        editModeInspectionMetadata = false;
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Update cancelled!", ""));

    }

    // end core info editing
   
    
    // ************************************************************************
    // ************       INSPECTED SPACE ATTRIBUTES                  *********
    //                    
    // ************************************************************************

    
     /**
     * Listener for user requests to start 
     *
     * @param ois
     */
    public void onEditSpaceDetailsInit(OccInspectedSpace ois) {
        System.out.println("FieldInspectionBB.onSpaceTypeLocationEditInitLinkClick | OIS ID " + ois.getInspectedSpaceID());
        currentInspectedSpace = ois;
        currentInspectedSpace.configureVisibleElementList(ViewOptionsOccChecklistItemsEnum.FAILED_PASSEDWPHOTOFINDING);
        // gutting location descriptors
       // refreshAllUniqueLocationDescriptors();
        
    }
    
    
 
    /**
     * Listener for user requests to view a space for inspection.
     *
     * @param ois
     */
    public void onViewInspectedSpaceLinkClick(OccInspectedSpace ois) {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        currentInspectedSpace = oic.configureElementDisplay(ois);
        currentSpaceType = currentInspectedSpace.getType();
        currentLocationDescriptor = currentInspectedSpace.getLocation();
        resetOrdinanceFilterFromCurrentInspectedSpace();
        System.out.println("OccInspectionsBB.onViewInspectedSpaceLinkClick | ois: " + ois.getInspectedSpaceID());

    }
    
    /**
     * Listener for user requests to save changes to space details
     * @param ev 
     */
    public void onSpaceDetailsEditCommit(ActionEvent ev){
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        try {
            onDimensionValueChange();
            oic.inspectionAction_updateInspectedSpace(currentInspectedSpace, currentInspection, getSessionBean().getSessUser());
            getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Space details updated!", ""));
        } catch (BObStatusException  | AuthorizationException | IntegrationException ex) {
            System.out.println(ex);
              getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        ex.getMessage(), ""));
        } 
    }
    
    /**
     * Listener for user requests to abort edits
     * @param ev 
     */
    public void onSpaceDetailsEditAbort(ActionEvent ev){
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        try {
            // get a fresh copy of our space
            currentInspectedSpace = oic.getOccInspectedSpace(currentInspectedSpace.getInspectedSpaceID());
        } catch (BObStatusException | IntegrationException  ex) {
            System.out.println(ex);
        } 
    }
    
   
    
    /**
     * Listener for changes in the space dimension valuesedit-inspections-location-var
     */
    public void onDimensionValueChange(){
       if(currentInspectedSpace != null){
           currentInspectedSpace.onDimensionValueChange();
       }
    }
    
    /**
     * Listener for user changes to using only overall sq. ft or computing from l x w
     */
    public void onDimensionInputComponentsChange(){
        System.out.println("FieldInspectionBB.onDimensionInputComponentsChange");
        
        
    }
    
    
    // location descriptors archive
    
       /**
     * Listener for user requests to update location of currently inspected
     * space
     * @param loc
     */
    public void onEditSpaceTypeLocationCommit(OccLocationDescriptor loc) {
        currentLocationDescriptor = loc;
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        try {
            oic.inspectionAction_updateSpaceLocation(currentInspection, currentInspectedSpace, getSessionBean().getSessUser(), currentLocationDescriptor);
            refreshCurrentInspectionAndRestoreSelectedSpace(true);
            refreshAllUniqueLocationDescriptors();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Saved changes to space: " + currentInspectedSpace.getType().getSpaceTypeTitle() + " id " + currentInspectedSpace.getInspectedSpaceID(), ""));
        } catch (IntegrationException | BObStatusException | BlobException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
    }
    
    /**
     * No logic here yet, just an empty listener who doesn't care 
     * what you have to say
     * @param ev 
     */
    public void onLocationDescriptionSelectionDone(ActionEvent ev){
        System.out.println("FieldInspectionBB.onLocationDescriptionSelectionDone");
    }

    /**
     * Asks coordinator for LocationDescriptor object
     */
    public void initSkeletonLocDescriptor() {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        currentLocationDescriptor = oic.getOccLocationDescriptorSkeleton();
    }

    /**
     * Listener to user confirmation that the they want to add their new
     * location descriptor or update an existing one. During adds, 
     * the new location will also get assigned to the current inspected space
     * since that's the only way the descriptors can exist; they cannot roam free
     * @param ev
     */
    public void onAddUpdateLocationDescriptorCommit(ActionEvent ev) {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        
        if (currentLocationDescriptor == null) {
            System.out.println("Can't create new loc descriptor: skeleton location descriptor object is null");
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Can't create new loc descriptor: skeleton location descriptor object is null", ""));
             return;
        }

        try {
            if(currentLocationDescriptor.getLocationID() == 0){
                System.out.println("FieldInspectionBB.onAddUpdateLocationDescriptorCommit | writing new loc");
                // add operation
                // store and retrieve this location descriptors
                currentLocationDescriptor = oic.getOccLocationDescriptor(oic.addNewLocationDescriptor(currentLocationDescriptor));
                onEditSpaceTypeLocationCommit(currentLocationDescriptor);
            } else {
                System.out.println("FieldInspectionBB.onAddUpdateLocationDescriptorCommit | updating loc ID " + currentLocationDescriptor.getLocationID());
                // update operation 
                oic.updateOccLocationDescriptor(currentLocationDescriptor, getSessionBean().getSessUser());
                currentLocationDescriptor = oic.getOccLocationDescriptor(currentLocationDescriptor.getLocationID());
                refreshCurrentInspectionAndRestoreSelectedSpace(true);
            }
           refreshAllUniqueLocationDescriptors();
        } catch (IntegrationException | BObStatusException | BlobException ex) {
            System.out.println(ex);
             getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
    }
    
    
    /**
     * Asks the current inspection for its unique descriptors
     */
    public void refreshAllUniqueLocationDescriptors(){
         if (Objects.isNull(allUniqueLocationDescriptors)) {
                allUniqueLocationDescriptors = new ArrayList<>();
            }
            
            allUniqueLocationDescriptors = currentInspection.getAllUniqueLocationDescriptors();
            
    }
    /**
     * Listener for user requests to cancel location add/edit
     * @param ev 
     */
    public void onAddEditLocationDescriptorAbort(ActionEvent ev){
        System.out.println("FieldInspectionBB.onAddEditLocationDescriptorAbort");
    }
    
    /**
     * Listener for user requests to inject the selected location descriptor into BB
     * method. Save operation will update the actual inspected space
     * @param loc 
     */
    public void onSelectExistingLocationDescriptorForCurrentSpace(OccLocationDescriptor loc){
        currentLocationDescriptor = loc;
    }
    
    /**
     * Listener to start the loc descriptor edit action
     * @param loc 
     */
    public void onEditLocationDescriptorInit(OccLocationDescriptor loc){
        currentLocationDescriptor = loc;
    }
    
   
    
    // ************************************************************************
    // ************             CONDUCTING INSPECTION                  *********
    // ************************************************************************

    /**
     * Listener to jump directly to the space photos and confirmation screen
     *
     * @param ois
     */
    public void onViewInspectedSpaceSummary(OccInspectedSpace ois) {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        currentInspectedSpace = oic.configureElementDisplay(ois);

        System.out.println("OccInspectionsBB.onViewInspectedSpaceSummary | ois: " + ois.getInspectedSpaceID());
    }
    /**
     * Listener for user requests to move from selection of space type to
     * location descriptor choice
     *
     * @param ev
     */
    public void onAddSpaceTypeSelectedButtonChange(ActionEvent ev) {
        if (Objects.nonNull(currentSpaceType)) {
            currentLocationDescriptor = null;
            System.out.println("FieldInspectionBB.onAddSpaceTypeSelectedButtonChange | space type = " + currentSpaceType.getSpaceTypeTitle());
        } else {
            System.out.println("FieldInspectionBB.onAddSpaceTypeSelectedButtonChange | NO SPACE TYPE SELECTED! YIKES!");
        }
    }

    /**
     * Listener for user requests to add a chosen OccSpace to the current
     * inspection
     *
     */
    public void onAddSpaceToInspectionCommitButtonChange() {
        if (currentInspection == null) {
            System.out.println("Can't initialize add space to inspection: selected inspection object is null");
            return;
        }
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        try {
            if(currentSpaceType == null){
                getFacesContext().getCurrentInstance().validationFailed();
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Please select a space type", ""));
            }
//             Maybe its important that i'm not passing a user or OccInspectionStatusEnum but i think its fine.
            currentInspectedSpace = oic.inspectionAction_commenceInspectionOfSpaceTypeChecklistified(currentInspection,
                    currentInspection.getInspector(),
                    currentSpaceType,
                    OccInspectionStatusEnum.NOTINSPECTED,
                    null);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Added space to inspection", ""));
            refreshCurrentInspectionAndRestoreSelectedSpace(true);
            resetOrdinanceFilterFromCurrentInspectedSpace();
        } catch (IntegrationException | BObStatusException | BlobException ex) {
            System.out.println("Failed to add selected space to skeleton inspection object: " + ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Cannot add space to inspection." + ex.toString(), ""));
        }
    }

    /**
     * Listener for user requests to bring up the space type selection dialog
     *
     * @param ev
     */
    public void onAddSpaceInitLinkClick(ActionEvent ev) {
        System.out.println("FieldInspectionBB.onAddSpaceLinkClick");

    }

    /**
     * INjects a fresh copy of the current oise
     */
    private void refreshCurrentInspectedSpaceElement() {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        if (currentInspectedSpaceElement != null) {
            try {
                currentInspectedSpaceElement = oic.getOccInspectedSpaceElement(currentInspectedSpaceElement.getInspectedSpaceElementID());
            } catch (BObStatusException | IntegrationException ex) {
                System.out.println(ex);
            }
        }

    }

    /**
     * Grabs a new copy of the current inspected space; This isn't used because
     * we always get the whole entire inspection
     *
     */
    public void refreshCurrentInspectedSpace() {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        if (currentInspectedSpace != null) {
            try {
                currentInspectedSpace = oic.getOccInspectedSpace(currentInspectedSpace.getInspectedSpaceID());
                resetOrdinanceFilterFromCurrentInspectedSpace();
                System.out.println("FieldInspectionBB.refreshCurrentInspectedSpace");
            } catch (BObStatusException | IntegrationException ex) {
                System.out.println(ex);
            }
        }
    }

    /**
     * Listener for user clicks of the not inspected/pass/fail button This is
     * deprecated for non-ajax whole list updates
     *
     * @Deprecated
     * @param oise the element in the inspection accordion
     */
    public void onElementInspectionStatusButtonChange(OccInspectedSpaceElement oise) {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        System.out.println("FieldInspectionBB.onElementInspectionStatusButtonChange | oise: " + oise.getInspectedSpaceElementID() + " status: " + oise.getStatusEnum().getLabel());
        try {
            oise = oic.inspectionAction_recordElementInspectionByStatusEnum(oise, getSessionBean().getSessUser(), currentInspection, useDefaultFindingsOnCurrentOISE);
//            refreshCurrentInspectionAndRestoreSelectedSpace();
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Recorded status of element ID: " + oise.getInspectedSpaceElementID(), ""));
        } catch (AuthorizationException | BObStatusException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.toString(), ""));
        }
    }

    /**
     * Listener for a user to click the violated link on a tree view of the ords
     *
     * @param nodeord
     */
    public void onMarkElementViolated(TreeNodeOrdinanceWrapper<OccInspectedSpaceElement> nodeord) {
        OccInspectedSpaceElement oise = nodeord.getOrdinance();
        oise.setStatusEnum(OccInspectionStatusEnum.VIOLATION);
        updateOccInspectedSpaceElement(oise);
//        onSaveCurrentSpaceChanges(null);

    }

    /**
     * Listener for a user to click the passed link on a tree view of the ords
     *
     * @param nodeord
     */
    public void onMarkElementPassed(TreeNodeOrdinanceWrapper<OccInspectedSpaceElement> nodeord) {
        OccInspectedSpaceElement oise = nodeord.getOrdinance();
        oise.setStatusEnum(OccInspectionStatusEnum.PASS);
        updateOccInspectedSpaceElement(oise);
//        onSaveCurrentSpaceChanges(null);

    }

    /**
     * Shared Coordinator interactions to udpate a single OISE
     *
     *
     */
    private void updateOccInspectedSpaceElement(OccInspectedSpaceElement oise) {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        System.out.println("FieldInspectionBB.onMarkElementViolated + oiseid: " + oise.getCodeSetElementID());
        try {
            oic.inspectionAction_updateOccInspectedSpaceElement(currentInspectedSpace, oise, currentInspection, getSessionBean().getSessUser());
            refreshCurrentInspectionAndRestoreSelectedSpace(true);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Recorded status of element ID: " + oise.getInspectedSpaceElementID(), ""));
        } catch (BObStatusException | IntegrationException | BlobException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error updating space element.", ""));
        }
    }

    /**
     * Listener for user to click the "remove" button from the violated
     * ordinance list above the tree
     *
     * @param oise
     */
    public void onUninspectElement(OccInspectedSpaceElement oise) {
        oise.setStatusEnum(OccInspectionStatusEnum.NOTINSPECTED);
        updateOccInspectedSpaceElement(oise);

    }

    /**
     * listener to browse ordinances in tree mode. Saves state on toggle.
     *
     * @param ev
     */
    public void onBrowseOrdinancesInTreeMode(ActionEvent ev) {
        System.out.println("FieldInspectionBB.onBrowseOrdinancesInTreeMode");
        if (!ordinanceEntryViewTreeMode) {
            onSaveCurrentSpaceChanges(null);

        }
        ordinanceEntryViewTreeMode = true;
    }

    /**
     * Listener to browse ordinances in table mode. Saves state on toggle.
     *
     * @param ev
     */
    public void onBrowseOrdinancesInTableMode(ActionEvent ev) {
        System.out.println("FieldInspectionBB.onBrowseOrdinancesInTableMode");
        if (ordinanceEntryViewTreeMode) {
            onSaveCurrentSpaceChanges(null);
            // test how to get my view changes to "stick"
            resetOrdinanceFilterFromCurrentInspectedSpace();
        }
        ordinanceEntryViewTreeMode = false;
    }

    /**
     * Listener for user requests to apply a status to all elements in the
     * current spacetype
     *
     * @param ev
     */
    public void onBatchProcessElementLinkClick(ActionEvent ev) {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        if (selectedElementStatusForBatch != null) {
            try {
                oic.inspectionAction_batchConfigureInspectedSpace(currentInspectedSpace,
                        selectedElementStatusForBatch,
                        getSessionBean().getSessUser(),
                        currentInspection,
                        useDefaultFindingsOnCurrentOISE);
                refreshCurrentInspectionAndRestoreSelectedSpace(true);
                resetOrdinanceFilterFromCurrentInspectedSpace();
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Success! Applied status "
                                + selectedElementStatusForBatch.getLabel()
                                + " To all ordinances in this space!", ""));
            } catch (BObStatusException | IntegrationException | BlobException | AuthorizationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                ex.toString(), ""));
            }
        }
    }

    /**
     * listener for tab changes on fin entry screen
     *
     * @param event
     */
    public void onSwitchInspectionEntryTabViewChange(TabChangeEvent event) {
        System.out.println("FieldInspectionBB.onSwitchInspectionEntryTabViewChange");
        onSaveCurrentSpaceChanges(null);
    }

    /**
     * Wrapper for compatibility with passing method references through the 
     * custom component interfaces
     */
    public synchronized void onSaveCurrentSpaceChangesNoArgWrapper(){
        onSaveCurrentSpaceChanges(null);
    }
    
    /**
     * Listener for user requests to save updates to their current space; As of
     * March 2024 this path is used by both the tree and the table view
     *
     * @param ev
     */
    public synchronized void onSaveCurrentSpaceChanges(ActionEvent ev) {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        try {
            System.out.println("FieldInspectionBB.onSaveCurrentSpaceChanges");
            oic.inspectionAction_updateSpaceElementData(currentInspectedSpace, getSessionBean().getSessUser(), currentInspection, useDefaultFindingsOnCurrentOISE);
            refreshCurrentInspectionAndRestoreSelectedSpace(true);

            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Saved changes to space: " + currentInspectedSpace.getType().getSpaceTypeTitle() + " id " + currentInspectedSpace.getInspectedSpaceID(), ""));
        } catch (IntegrationException | BObStatusException | BlobException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));

        }
    }

    /**
     * LIstener for user requests to abandon the space editing endeavor complete
     *
     * @param ev
     */
    public void onAbortSpaceInspectionEntry(ActionEvent ev) {
        System.out.println("OccPeriodBB.onAbortSpaceInspectionEntry");
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Operation aborted", ""));

    }

    /**
     * Listener for user requests to commit edits to a single inspection item
     *
     * @param ev
     */
    public void onElementEditCommit(ActionEvent ev) {
        System.out.println("FieldInspectionBB.onElementEditCommit");
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        try {
            oic.inspectionAction_updateOccInspectedSpaceElement(currentInspectedSpace, currentInspectedSpaceElement, currentInspection, getSessionBean().getSessUser());
            refreshCurrentInspectionAndRestoreSelectedSpace(true);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Succesfully updated inspection item", ""));
        } catch (BObStatusException | IntegrationException | BlobException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
    }

    /**
     * Listener for user requests to abort the inspection item edit operation
     *
     * @param ev
     */
    public void onElementEditAbort(ActionEvent ev) {
        System.out.println("FieldInspectionBB.onElementEditAbort");

    }

    /**
     * Listener for user requests to go back and manage a space's ordinances
     *
     * @param ev
     */
    public void onReturnToSpaceInspectionEntry(ActionEvent ev) {
        System.out.println("OccPeriodBB.onReturnToSpaceInspectionEntry");
        resetOrdinanceFilterFromCurrentInspectedSpace();
    }

    /**
     * Listener for user requests to edit a space (from the inspection dialog)
     *
     * @param ev
     */
    public void onSpaceTypeEditLinkClick(ActionEvent ev) {
        System.out.println("FieldInspectionBB.onSpaceTypeEditLinkClick");

    }

    /**
     * Listener for user requests to view a specific space element in a dialog
     *
     * @param wrappedOrd
     */
    public void onSpaceElementViewLinkClick(TreeNodeOrdinanceWrapper<OccInspectedSpaceElement> wrappedOrd) {
        
        registerCurrentInspectionwithBlobUtilitiesBB();
        if (wrappedOrd != null) {
            System.out.println("InspectionBB.onSpaceElementViewLinkClick | csel ID: " + wrappedOrd.getOrdinance().getCodeSetElementID());
            currentInspectedSpaceElement = wrappedOrd.getOrdinance();
        } else {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Null WrapOrd Could not extract occ space type element", ""));
        }
    }

    /**
     * Listener for user requests to view a specific space element in a dialog
     *
     * @param oise
     */
    public void onSpaceElementViewLinkClick(OccInspectedSpaceElement oise) {
        registerCurrentInspectionwithBlobUtilitiesBB();
        if (oise != null) {
            System.out.println("InspectionBB.onSpaceElementViewLinkClick | csel ID: " + oise.getCodeSetElementID());
            currentInspectedSpaceElement = oise;
        } else {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Null oise: Could not extract occ space type element from oise input", ""));
        }
    }

    /**
     *  A hacky work around to make sure the current FIN is the blob utility bb's current
     * blob holder
     */
    private void registerCurrentInspectionwithBlobUtilitiesBB(){
        if(currentInspection != null){
            BlobUtilitiesBB bubb = (BlobUtilitiesBB) getFacesContext().getApplication().evaluateExpressionGet(getFacesContext(), "#{blobUtilitiesBB}", BlobUtilitiesBB.class);
            if(bubb != null){
                System.out.println("FieldInspectionBB.registerCurrentInspectionwithBlobUtilitiesBB | non null BlobUtiltiesBB | registering FIN ID: " + currentInspection.getInspectionID());
                bubb.registerBlobHolder(currentInspection);
            } else {
                System.out.println("FieldInspectionBB.registerCurrentInspectionwithBlobUtilitiesBB | ERROR null BlobUtiltiesBB no registration target" );
            }
        }
    }
   

    /**
     * Listener for user requests to start space type removal process
     *
     * @param ois
     */
    public void onSpaceRemoveInitLinkClick(OccInspectedSpace ois) {
        System.out.println("FieldInspectionBB.onSpaceTypeRemoveInitLinkClick | OIS ID " + ois.getInspectedSpaceID());
        currentInspectedSpace = ois;
    }

    /**
     * listener for user requests to abort space type removal
     *
     * @param ev
     */
    public void onSpaceRemoveAbort(ActionEvent ev) {
        System.out.println("FieldInspectionBB.onSpaceTypeRemoveAbort");
    }

    /**
     * Listener for user requests to delete a space
     *
     * @param ev
     */
    public void onSpaceRemoveCommitLinkClick(ActionEvent ev) {
        System.out.println("FieldInspectionBB.onSpaceTypeRemoveLinkClick");
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        try {
            oic.inspectionAction_removeSpaceFromInspection(currentInspectedSpace, getSessionBean().getSessUser(), currentInspection);
            refreshCurrentInspectionAndRestoreSelectedSpace(false);
            currentInspectedSpace = null;
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Success: The selected space has been removed from this inspection.", ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException | BlobException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Failure: Could not remove this inspected space from the inspection, sorry.i", ""));
        }

    }

    // end conducting inspections
    // ************************************************************************
    // ******************           BLOBS                      ****************
    // ************************************************************************
    /**
     * Listener for user requests to add to or view blobs on the oise
     *
     * @param oise
     */
    public void onManageBlobsOnInspectedElement(OccInspectedSpaceElement oise) {
        try {
            Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
            sessionMap.put("blob-list-component-to-update", FacesContext.getCurrentInstance()
                    .getExternalContext()
                    .getRequestParameterMap()
                    .get("blob-list-component-to-update"));

            getSessionBean().setAndRefreshSessionBlobHolderAndBuildUpstreamPool(oise);
        } catch (BObStatusException | BlobException | IntegrationException ex) {
            System.out.println(ex);
        }
    }

    /**
     * Listener for user requests to start the image upload process to an occ
     * inspection I take the current inspection and make it the session's
     * BlobHolder so the reuasable code modules can take over
     *
     * @param ev
     */
    public void onUploadImagesToInspectionInitButtonClick(ActionEvent ev) {
        System.out.println("OccInspectionsBB.onUploadImagesToInspectionInitButtonClick");
        BlobUtilitiesBB bubb = (BlobUtilitiesBB) getFacesContext().getApplication().evaluateExpressionGet(getFacesContext(), "#{blobUtilitesBB}", BlobUtilitiesBB.class);
        if(bubb != null){
            bubb.onUploadBlobInitButtonChange(currentInspection);
        }
            
    }

    /**
     * Adaptor method for loading photo pool from the confirmation dialog
     */
    public void onViewPhotoPoolLinkClickFromAjaxElement() {
        onViewPhotoPoolLinkClick(null);
    }

    /**
     * Listener for user requests to see the photos on an inspection Sets the
     * inspection as the blobholder for the blob UI to take over. And asks our
     * blob coordinator for my most recent blob list
     *
     * @param ev
     */
    public void onViewPhotoPoolLinkClick(ActionEvent ev) {

        System.out.println("OccInspectionsBB.onViewPhotoPoolLinkClick");
        try {
            registerCurrentInspectionwithBlobUtilitiesBB();
            getSessionBean().setAndRefreshSessionBlobHolderAndBuildUpstreamPool(currentInspection);
        } catch (BObStatusException | BlobException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Could not initiate photo pool", ""));

        }

    }

    /**
     * Special getter wrapper around the FieldInspection blob list that checks the
     * session for a new blob list that may have been injected by the
     * BlobUtilitiesBB
     *
     * @return the inspection's updated blob list
     */
    public List<BlobLight> getManagedBlobLightListFromInspection() {
        List<BlobLight> sessBlobListForUpdate = getSessionBean().getSessBlobLightListForRefreshUptake();
        // Make sure this list is, in fact, for our current case
        if (currentInspection != null) {
            if (sessBlobListForUpdate != null
                    && getSessionBean().getSessBlobLightListHolderForRefreshUptake() != null
                    && getSessionBean().getSessBlobLightListHolderForRefreshUptake().getBlobLinkEnum() == BlobLinkEnum.FIELD_INSPECTION
                    && getSessionBean().getSessBlobLightListHolderForRefreshUptake().getParentObjectID() == currentInspection.getInspectionID()) {
                System.out.println("fieldInspectionBB.getManagedBlobLightListFromInspection | found non-null session blob list for uptake: " + sessBlobListForUpdate.size());
                currentInspection.setBlobList(sessBlobListForUpdate);
                // clear session since we have the new list
                getSessionBean().setSessBlobLightListForRefreshUptake(null);
                return sessBlobListForUpdate;
            } else {
                if (currentInspection.getBlobList() != null) {
                    return currentInspection.getBlobList();
                }
            }
        }
        System.out.println("fieldInspectionBB.getManagedBlobLightListFromInspection | hark! null current inspection ");
        return new ArrayList<>();
    }

    /**
     * Listener for users to connect a photo from the pool to the current oise
     *
     * @param blob
     */
    public void onLinkPhotoToCurrentOISE(BlobLight blob) {
        BlobCoordinator bc = getBlobCoordinator();
        List<BlobLight> blist = new ArrayList<>();
        if (currentInspectedSpaceElement != null && blob != null) {
            System.out.println("FIeldInspectionBB.onLinkPhotoToCurrentOISE | blobid: " + blob.getPhotoDocID());
            blist.add(blob);
            try {
                bc.linkBlobHolderToBlobList(currentInspectedSpaceElement, blist);
                refreshCurrentInspectedSpaceElement();
            } catch (BObStatusException | IntegrationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Could not link blob to element", ""));
            }
        }

    }

    /**
     * Listener for user requests to remove a blob from the oise
     *
     * @param blob
     */
    public void onRemoveBlobFromOISE(BlobLight blob) {
        if (blob != null) {
            System.out.println("FIeldInspectionBB.onRemoveBlobFromOISE | blobid: " + blob.getPhotoDocID());
        }
        BlobCoordinator bc = getBlobCoordinator();
        try {
            bc.deleteLinksToPhotoDocRecord(blob, currentInspectedSpaceElement.getBlobLinkEnum(), getSessionBean().getSessUser());
            refreshCurrentInspectedSpaceElement();
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Could not remove link between blob and element", ""));
        }

    }

    /**
     * Listener for user requests to finish oise photo linking; I just update
     * the current inspection and space
     *
     * @param ev
     */
    public void onFinishOISEPhotoLinking(ActionEvent ev) {
        try {
            refreshCurrentInspectionAndRestoreSelectedSpace(true);
        } catch (IntegrationException | BObStatusException | BlobException ex) {
            System.out.println(ex);
        }
    }

    // end blobs
    // ************************************************************************
    // ************          FINALIZATION                             *********
    // ************************************************************************
    /**
     * Listener for user requests to start the certification process
     *
     * @param ev
     */
    public void onFinalizeInspectionInitButtonChange(ActionEvent ev) {
        if (currentInspection != null) {
            System.out.println("FieldInspectionBB.onCertifyInspectionInitButtonChange | FINID: " + currentInspection.getInspectionID());
            formFailedFINFollowUpWindow = getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod().getMuniProfile().getDefaulFailedFINFollowupDays();
        } else {
            System.out.println("FieldInspectionBB.onCertifyInspectionInitButtonChange | NULL currentInspection ");
        }
    }

    /**
     * listener for users to abort the inspection finalization process
     *
     * @param ev
     */
    public void onInspectionFinalizeAbortButtonChange(ActionEvent ev) {
        System.out.println("FieldInspectionBB.onInspectionFinalizeAbortButtonChange");

    }

    /**
     * Listener for user requests to certify the given
     *
     * @param ev
     */
    public void onFinalizeInspectionCommitButtonChange(ActionEvent ev) {

        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        try {
            if (currentInspection != null) {
                System.out.println("FieldInspectionBB.onCertifyInspectionCommitButtonChange | FINID: " + currentInspection.getInspectionID());
                currentInspection.setDetermination(selectedDetermination);

                oic.inspectionAction_finalizeInspection(currentInspection, formFailedFINFollowUpWindow, getSessionBean().getSessUser());

                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Inspection determination has been certified and inspection is now locked!", ""));
                refreshCurrentInspectionAndRestoreSelectedSpace(true);
                refreshInspectionListAndTriggerManagedListReload();
                refreshEventsRelatedToCurrentInspection();
            } else {
                System.out.println("FieldInspectionBB.onCertifyInspectionCommitButtonChange | null currentInspection!!");
                  getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Could not finalize inspection ", ""));
            }
        } catch (IntegrationException | AuthorizationException | BObStatusException | BlobException | EventException | SearchException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }
    }

    /**
     * Listener for user requests to remove Finalization of current occ
     * inspection
     */
    public void onRemoveFinalizationOfInspection() {
        if (currentInspection != null) {
            System.out.println("FieldInspectionBB.onRemoveFinalizationOfInspection | FINID: " + currentInspection.getInspectionID());
            OccInspectionCoordinator oic = getOccInspectionCoordinator();
            IFace_inspectable inspectable;
            if (currentInspection.getDomainEnum() == EventRealm.OCCUPANCY) {
                inspectable = getSessionBean().getSessOccPeriod();
            } else {
                inspectable = getSessionBean().getSessCECase();
            }
            try {
                oic.removeOccInspectionFinalization(getSessionBean().getSessUser(), currentInspection, inspectable);

                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Inspection has been decertified and determination removed!", ""));

                refreshCurrentInspectionAndRestoreSelectedSpace(true);
                refreshInspectionListAndTriggerManagedListReload();
                refreshEventsRelatedToCurrentInspection();

            } catch (IntegrationException | BObStatusException | BlobException | AuthorizationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Could not remove certification of occ inspection: " + ex.getMessage(), ""));
            }
        } else {
            System.out.println("FieldInspectionBB.onRemoveFinalizationOfInspection | NULL Current inspection");
        }
    }

    // end finalization
    // ************************************************************************
    // ************          DEACTIVATION                             *********
    // ************************************************************************
    /**
     * Listener for user requests to start the inspection deactivation process
     *
     * @param ev
     */
    public void onDeactivateInspectionInit(ActionEvent ev) {
        System.out.println("FieldInspectionBB.onDeactivateInspectionInit");
    }

    /**
     * Listener for user requests to start the inspection deactivation process
     *
     * @param ev
     */
    public void onDeactivateInspectionAbort(ActionEvent ev) {
        System.out.println("FieldInspectionBB.onDeactivateInspectionAbort");
    }

    /**
     * Listener for user requests to deactivate the selected inspection
     *
     * @param ev
     */
    public void onDeactivateInspectionButtonClick(ActionEvent ev) {
        if (currentInspection != null) {

            System.out.println("FieldInspectionBB.onDeactivateInspectionButtonClick | deactivating inspection " + currentInspection.getInspectionID());
            OccInspectionCoordinator oic = getOccInspectionCoordinator();
            try {
                oic.deactivateOccInspection(getSessionBean().getSessUser(), currentInspection, currentInspectable);
                refreshInspectionListAndTriggerManagedListReload();
                getSessionInspectionConductor().refreshFieldFinLists(null);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Occ Inspection deactivated with ID " + currentInspection.getInspectionID(), ""));
            } catch (BObStatusException | AuthorizationException | IntegrationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                ex.getMessage(), ""));
            }
        } else {
            System.out.println("FieldInspectionBB.onDeactivateInspectionButtonClick | cannot deactivate null current inspection");
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Page setup error; cannot deactivate inspection", ""));
        }
    }

    // end deac
    // ************************************************************************
    // ************             REROUTING/REVERTING                   *********
    // ************************************************************************
    /**
     * Listener to start the reversion process. Clears out any previous raw
     * address and unit.
     *
     * @param ev
     */
    public void onRevertInspectionInitButtonChange(ActionEvent ev) {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        if (currentInspection != null) {
            System.out.println("FieldInspectionBB.onRevertInspectionInitButtonChange");
            // save to write to remarks
            rawLocationHolding = currentInspection.getCombinedRawLocation();
            currentInspection.setRawPropertyAddress(null);
            currentInspection.setRawPropertyUnit(null);
            try {
                finRevertXMuni = false;
                finRevertXMuniCandidateList = oic.assembleRervertFinToUnassignedPoolXMuniCandidateList(getSessionBean().getSessUser(), currentInspection);
            } catch (BObStatusException | IntegrationException ex) {
                System.out.println(ex);
                 getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                ex.getMessage(), ""));
                
            } 
            finRevertXMuniSelected = null;
        }
    }

    /**
     * listener to stop reversion process
     *
     * @param ev
     */
    public void onRevertInspectionCancelButtonChange(ActionEvent ev) {
        try {
            System.out.println("FieldInspectionBB.onRevertInspectionCancelButtonChange");
            refreshCurrentInspectionAndRestoreSelectedSpace(false);
        } catch (IntegrationException | BObStatusException | BlobException ex) {
            System.out.println();
        }
    }

    /**
     * Listener to move an inspection to the same pool as field initiated
     * inspections that then allows user to assign the inspection to any case or
     * period, existing or newly created
     *
     * @return the dashboard nav string
     */
    public String onRevertInspectionToUnassignedPool() {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        try {
            StringBuilder sb = new StringBuilder();
            if (currentInspection != null && (currentInspection.getRawPropertyAddress() != null || currentInspection.getRawPropertyUnit() != null)) {
                sb.append("Inspection reverted back to unassigned pool; Previous raw location: ");
                sb.append(rawLocationHolding);
            } else {
                sb.append("Inspection reverted to unassigned pool but no previous "
                        + "raw location had been entered, probably because the reversion "
                        + "tool was used for rerouting or testing purposes not actual use by the handheld software tool");
            }
            currentInspection.setRemarks(sb.toString());
            if(finRevertXMuni){
                oic.revertFieldInspectionToUnassignedPool(currentInspection, finRevertXMuniSelected, getSessionBean().getSessUser());
            } else {
                oic.revertFieldInspectionToUnassignedPool(currentInspection, null, getSessionBean().getSessUser());
            }
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Inspection reverted to pool; success!", ""));
            getSessionInspectionConductor().refreshFieldFinLists(null);
        } catch (IntegrationException | BObStatusException | BlobException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
            return "";
        }
        return "missionControl";

    }

    // end reverting
    // ************************************************************************
    // ************           REPORTING                               *********
    // ************************************************************************
    /**
     * Listener for user requests to start the report building process
     *
     * @param ev
     */
    public void onFieldInspectionReportInitLinkClick(ActionEvent ev) {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        PropertyCoordinator pc = getPropertyCoordinator();
        IFace_inspectable inspectable = null;
        try {
            // setup our human link list for user selection 
            loadPropertyDataHeavyForCurrentInspectable();
            if (inspectablePropertyHost != null) {
                humanLinkCandidateList = inspectablePropertyHost.gethumanLinkList();
            } else {
                humanLinkCandidateList = new ArrayList<>();
            }
            if(currentInspection != null){
                int relatedUnitID = 0;
                
                // CE CASE RELEVANT PERSON LINKS
                if (currentInspection.getDomainEnum() == EventRealm.CODE_ENFORCEMENT) {
                    CECaseDataHeavy csedh = getSessionBean().getSessCECase();
                    inspectable = csedh;
                    if (csedh != null && csedh.gethumanLinkList() != null && !csedh.gethumanLinkList().isEmpty()) {
                        humanLinkCandidateList.addAll(csedh.gethumanLinkList());
                    }
                    relatedUnitID = csedh.getUnitID();
                    
                // OCC PERIOD RELEVANT PERSON LINKS
                } else if (currentInspection.getDomainEnum() == EventRealm.OCCUPANCY) {
                    OccPeriodDataHeavy opdh = getSessionBean().getSessOccPeriod();
                    inspectable = opdh;
                    if (opdh != null && opdh.gethumanLinkList() != null && !opdh.gethumanLinkList().isEmpty()) {
                        humanLinkCandidateList.addAll(opdh.gethumanLinkList());
                        // get our unit persons as well
                    }
                    if(opdh != null){
                        relatedUnitID = opdh.getPropertyUnitID();
                    } 
                    
                } else {
                    throw new BObStatusException("Field Inspection must have a domain");
                }
                PropertyUnitDataHeavy pudh = null;
                try {
                    if(relatedUnitID != 0){
                        System.out.println("FieldInspectionBB.onFieldInspectionReportInitLinkClick | Related unit ID: " + relatedUnitID);
                        pudh = pc.getPropertyUnitDataHeavy(pc.getPropertyUnit(relatedUnitID), getSessionBean().getSessUser());
                    }
                } catch (EventException | AuthorizationException ex) {
                    System.out.println(ex);
                } 
                if(pudh != null){
                    humanLinkCandidateList.addAll(pudh.gethumanLinkList());
                }
            } // close non-null inspection

            // have our coordinator setup the config object further
            reportConfigFIN = oic.getOccInspectionReportConfigDefault(
                    currentInspection,
                    inspectable,
                    getSessionBean().getSessUser());
            if(reportConfigFIN == null){
                throw new BObStatusException("FieldInspectionBB.onFieldInspectionReportInitLinkClick | null FIN report! Cannot proceed");
            }
            reportConfigFIN.setInspection(currentInspection);
            System.out.println("FieldInspectionBB.onFieldInspectionReportInitLinkClick | session property: " + getSessionBean().getSessProperty().getDescriptionString());
            reportConfigFIN.setInspectedProperty(getSessionBean().getSessProperty());

        } catch (IntegrationException | BObStatusException | BlobException | SearchException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));

        }
    }

    /**
     * Listener for user requests to start the report building process
     *
     * @param ev
     */
    public void onFieldInspectionReportCommitLinkClick(ActionEvent ev) {

        reportConfigFIN.getInspection().configureVisibleSpaceElementList(reportConfigFIN.getViewSetting());
        getSessionBean().setReportConfigFieldInspection(reportConfigFIN);
        
          // with help from ChatGPT 3.5 on 23FEB24
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();

        String baseURL = request.getRequestURL().toString().replace(request.getRequestURI(), request.getContextPath());
        String permitURL = baseURL + "/restricted/reports/reportInspection.xhtml";
        PrimeFaces.current().executeScript("window.open('" + permitURL + "', '_blank');");

        
    }

    // end reporting
    // ************************************************************************
    // ************           REINSPECTION STUFF                      *********
    // ************************************************************************

    /**
     * Primary re-inspection pathway: we'll get the inspection and 
     * use it to build a new inspection with a re-inspection config in its belly and
     * then just follow the same pathway for normal inspection init.
     *
     * @param fin
     */
    public void onReinspectionInitFromFinList(FieldInspectionLight fin) {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        
        // use the given FIN to setup our inspectable, which we'll use to init a 
        // skeleton inspection with a reinspection config in its belly
        registerAndRefreshCurrentFieldInspectionAndInspectable(fin);
        // then use our current inspection as the source inspection for the reinspection
        FieldInspectionReInspectionConfig reinspectionConfig = oic.getFieldInspectionReinspectionSettingsSkeleton(currentInspection);
        reinspectionConfig.setRequestingUser(getSessionBean().getSessUser());
        // our registration method means we can set this using the class member
        reinspectionConfig.setInspectable(currentInspectable);
        // now our current inspection is our skeleton into which we shall inject our reinspection config
        onCreateInspectionInitButtonChange(currentInspectable);
        extractComponentForReloadFromRequest();
        if(currentInspection != null){
            currentInspection.setReinspectionConfig(reinspectionConfig);
            try {
                currentInspection.setChecklistTemplate(oic.getChecklistTemplate(fin.getChecklistTemplateID()));
            } catch (IntegrationException ex) {
                System.out.println(ex);
            }
        } else {
            System.out.println("FieldInspectionBB.onReinspectionInitFromFinList | fatal error building inspection skeleton");
        }
        System.out.println("FieldInspectionBB.onReinspectionInitFromFinList | completed reinspection init");
    }

  
    
    /**
     * User requests to abort the reinspection process
     *
     * @param ev
     */
    public void onReinspectionOperationAbort(ActionEvent ev) {
       if(currentInspection != null){
           currentInspection.setReinspectionConfig(null);
       }

    }

    /**
     * To clear applied ordinace filter on deprecated accordion grouping
     */
    private void clearOrdinanceFilter() {
        ordinanceFilterText = "";
        inspectedElementGroupListFiltered = currentInspectedSpace.getInspectedElementGroupList();
    }

    /**
     * To build inspectedElementGroupListFiltered with respect to ordinance
     * filter text Applies to deprecated accordion grouping
     */
    private void applyOrdinanceFilter() {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        inspectedElementGroupListFiltered = oic.inspectionAction_configureInspectedElementGroupListFiltered(
                currentInspectedSpace.getInspectedElementGroupList(),
                ordinanceFilterText
        );
    }

    /**
     * To clear applied ordinace filter
     */
    private void resetOrdinanceFilterFromCurrentInspectedSpace() {
        ordinanceFilterText = "";
        inspectedElementListFiltered = currentInspectedSpace.getInspectedElementList();
    }

    /**
     * To build inspectedElementGroupListFiltered with respect to ordinance
     * filter text
     *
     * @param ev
     */
    public void applyOrdinanceFilterOnSimpleList() {
        System.out.println("FieldInspectionBB.applyOrdinanceFilterOnSimpleList");
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        onSaveCurrentSpaceChanges(null);
        inspectedElementListFiltered = oic.inspectionAction_configureInspectedElementListFiltered(currentInspectedSpace.getInspectedElementList(), ordinanceFilterText, filterByFullOrdText);
    }

    /**
     * Listener for user requests to clear ordinance filter
     */
    public void onClearOrdinanceFilter() {
        resetOrdinanceFilterFromCurrentInspectedSpace();
    }

    /**
     * Routes user to the profile of the inspection parent, which is useful when
     * viewing an inspection from the dashboard
     *
     * @param ev
     * @return
     */
    public String navigateToProfileOfCurrentInspectionParent() {
        if (currentInspectable != null) {
            try {
                return getSessionBean().navigateToPageCorrespondingToObject((IFaceActivatableBOB) currentInspectable);
            } catch (BObStatusException | AuthorizationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Session route exception: Unable to navigate to parent profile page, sorry", ""));
            }
        } else {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "No inspectable for navigation!", ""));
        }
        return "";

    }

    // ************************************************************
    // ************ FIELD INITATED INSPECTION ROUTING SHIT ********
    // ************************************************************
    /**
     * Listener to start the routing process for a field inspection that started
     * on the mobile app and doesn't have a home yet
     *
     * @param finLight
     */
    public void onRouteFieldInitiatedFinInit(FieldInspectionLight finLight) {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        if (finLight != null) {
            getSessionInspectionConductor().setSessFieldInspectionAwaitingRouting(finLight);
            try {
                finForRouting = oic.getFieldInspectionDataHeavy(finLight);
                System.out.println("Starting of routing for inspection ID: " + finLight.getInspectionID());
            } catch (IntegrationException | BObStatusException | BlobException ex) {
                System.out.println(ex);
            }
        }
        initRoutingSteps();
        finRoutingActiveStep = FieldFinRoutingStepEnum.PROPERTY.getStepNumber1Based();
    }

    /**
     * Listener for users who have selected a case or permit file and want to
     * attach the current field-initiated fin to this object
     *
     * @param inspectable
     */
    public void onRouteFieldInitiatedFinToInspectable(IFace_inspectable inspectable) {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        CECaseDataHeavy csedh = null;
        OccPeriodDataHeavy opdh = null;
        try {
            if (inspectable instanceof CECaseDataHeavy cs) {
                csedh = cs;
            } else if (inspectable instanceof OccPeriodDataHeavy op) {
                opdh = op;
            } else {
                throw new BObStatusException("Cannot convert given inspectable to period or case");
            }

            // only one of these should be non-null: cse or period
            oic.routeFieldInitiatedInspection(getSessionInspectionConductor().getSessFieldInspectionAwaitingRouting(), csedh, opdh, getSessionBean().getSessUser());
            registerCurrentInspectable(inspectable, true);
            getSessionInspectionConductor().setSessFieldInspectionAwaitingRouting(null);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Linking of field-initiated inspection successful!", ""));
        } catch (BObStatusException | AuthorizationException | IntegrationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), ""));
        }

    }

    /**
     * Listener for user requests to no longer be in FieldInit Fin Routing mode.
     *
     * @param ev
     */
    public void onClearFieldInitiatedFinRouting(ActionEvent ev) {
        System.out.println("FieldInspectionBB.onClearFieldInitiatedFinRouting");
        getSessionInspectionConductor().setSessFieldInspectionAwaitingRouting(null);
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Exiting field-initiated inspection routing mode", ""));
    }

    /**
     * Sets up our routing steps
     */
    private void initRoutingSteps() {
        finRoutingStepsModel = new DefaultMenuModel();
        for (FieldFinRoutingStepEnum step : FieldFinRoutingStepEnum.values()) {
            finRoutingStepsModel.getElements().add(createMenuItem(step.getStepTitle(), null));
        }

    }

    /**
     * Builds a single routing step objet
     *
     * @param value
     * @param command
     * @return
     */
    private DefaultMenuItem createMenuItem(String value, String command) {
        DefaultMenuItem item = new DefaultMenuItem();
        item.setAjax(false);
        item.setValue(value);
        item.setDisabled(true);
        item.setStyleClass("non-clickable");
//        item.setCommand(command);
//        item.setUpdate("permit-steps-panel");
        return item;
    }

    /**
     * listener to complete step 1
     *
     * @param prop
     */
    public void completeRoutingStep1ChooseProperty(Property prop) {
        PropertyCoordinator pc = getPropertyCoordinator();
        System.out.println("FieldInspectionBB.completeRoutingStep1ChooseProperty | propID: " + prop.getParcelKey());

        try {
            fieldRoutingPropertyTarget = pc.assemblePropertyDataHeavy(prop, getSessionBean().getSessUser());
            getSessionBean().setSessProperty(fieldRoutingPropertyTarget);
            System.out.println("Address: " + fieldRoutingPropertyTarget.getPrimaryAddressLink().getAddressPretty1Line());
            finRoutingActiveStep = FieldFinRoutingStepEnum.PATHWAY.getStepNumber1Based();
        } catch (IntegrationException | BObStatusException | BlobException | SearchException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error: Unable to setup proerty for field inspection assignment; Complain to administrators! " + ex.getMessage(), ""));

        }
    }

    /**
     * Listener to complete step 2 with occ pathway
     *
     * @param ev
     */
    public void completeRoutingStep2UseOccPathway(ActionEvent ev) {
        System.out.println("FieldInspectionBB.completeRoutingStep2UseOccPathway ");
        finRoutingActiveStep = FieldFinRoutingStepEnum.PERIODS_CASES_EXISTING.getStepNumber1Based();
        finRoutingUsingOccPathway = true;
    }

    /**
     * Listener to complete step 2 with CE case pathway
     *
     * @param ev
     */
    public void completeRoutingStep2UseCEPathway(ActionEvent ev) {
        System.out.println("FieldInspectionBB.completeRoutingStep2UseCEPathway ");
        finRoutingActiveStep = FieldFinRoutingStepEnum.PERIODS_CASES_EXISTING.getStepNumber1Based();
        finRoutingUsingOccPathway = false;
    }

    /**
     * Listener to complete step 3 with an existing case selection
     *
     * @param cse
     * @return
     */
    public String completeRoutingStep3ChooseExistingCECase(CECaseDataHeavy cse) {
        System.out.println("FieldInspectionBB.completeRoutingStep3ExistingCase | caseID: " + cse.getCaseID());
        CaseCoordinator cc = getCaseCoordinator();
        onRouteFieldInitiatedFinToInspectable(cse);

        try {
            getSessionInspectionConductor().refreshFieldFinLists(null);
            // reload
            cse = cc.cecase_assembleCECaseDataHeavy(cc.cecase_getCECase(cse.getCaseID(), getSessionBean().getSessUser()), getSessionBean().getSessUser());
            return getSessionBean().navigateToPageCorrespondingToObject(cse);
        } catch (BObStatusException | AuthorizationException | IntegrationException | SearchException ex) {
            System.out.println(ex);
        }
        return "";

    }

    /**
     * Listener to complete step 3 with an existing occ period
     *
     * @param period
     * @return
     */
    public String completeRoutingStep3ChooseExistingOccPeriod(OccPeriodDataHeavy period) {
        System.out.println("FieldInspectionBB.completeRoutingStep3ExistingPeriod " + period.getPeriodID());
        OccupancyCoordinator oc = getOccupancyCoordinator();
        onRouteFieldInitiatedFinToInspectable(period);
        try {
            getSessionInspectionConductor().refreshFieldFinLists(null);
            period = oc.assembleOccPeriodDataHeavy(period, getSessionBean().getSessUser());
            return getSessionBean().navigateToPageCorrespondingToObject(period);
        } catch (BObStatusException | AuthorizationException | IntegrationException ex) {
            System.out.println(ex);
        }
        return "";

    }

    /**
     * Listener for users who can't find a suitable ce case or permit file on
     * the current property on which to attach their field initiated inspection
     *
     * @param ev
     */
    public void onCreateNewPeriodCeCase(ActionEvent ev) {
        System.out.println("FieldInspectionBB.onCreateNewPeriodCECase ");
        SessionBean sb = getSessionBean();
        finRoutingActiveStep = FieldFinRoutingStepEnum.PERIOD_CASE_NEW.getStepNumber1Based();
        if (finRoutingUsingOccPathway) {

            PropertyProfileBB propPfBB = (PropertyProfileBB) getFacesContext().getApplication().evaluateExpressionGet(getFacesContext(), "#{propertyProfileBB}", PropertyProfileBB.class);
            propPfBB.onOccperiodCreateInitButtonChange(null);
            System.out.println("FieldInspectionBB.onCreateNewPeriodCECase | occ path; asked PropProfileBB to setup a new period");

        } else {
            System.out.println("FieldInspectionBB.onCreateNewPeriodCECase | cecase path; nothing to do here.");
            FacesContext context = FacesContext.getCurrentInstance();
            CECaseAddBB cecaseAddBB = (CECaseAddBB) context.getApplication().evaluateExpressionGet(context, "#{ceCaseAddBB}", CECaseAddBB.class);
            cecaseAddBB.onCECaseAddInitButtonChange(null);
        }

    }

    /**
     * Listener to complete the routing process with a CE case
     *
     * @param oneClickTemplate if null, I'll assume I'm making a new case from
     * the new case form; if not null, i'll use the given oneclick template
     * @return the ce case page of the new cecase
     */
    public String completeRoutingStep4CreateNewCECase(CECaseOneClickTemplate oneClickTemplate) {
        CECaseAddBB caseAddBB = (CECaseAddBB) getFacesContext().getApplication().evaluateExpressionGet(getFacesContext(), "#{ceCaseAddBB}", CECaseAddBB.class);
        String outcome = "";
        if (caseAddBB != null) {
            if (oneClickTemplate == null) {
                System.out.println("FieldInspectionBB.completeRoutingStep4NewCase | using manual form info");
                
                outcome = caseAddBB.onAddNewCaseCommitButtonChange();
            } else {
                System.out.println("FieldInspectionBB.completeRoutingStep4NewCase | using lightning case template ID: " + oneClickTemplate.getOneClickID());
                caseAddBB.onCreateCaseFromOneClickButton(oneClickTemplate);
                try {
                    // one click adds are ajax, so we don't go to the case profile page manually
                    outcome = getSessionBean().navigateToPageCorrespondingToObject(getSessionBean().getSessCECase());
                    getSessionInspectionConductor().registerSessionInspectable(currentInspectable);
                } catch (BObStatusException | AuthorizationException ex) {
                    System.out.println(ex);
                }
            }
            // our new case should be the session case and we can close out the inspection routing
            CECaseDataHeavy freshCse = getSessionBean().getSessCECase();
            if (freshCse != null) {
                System.out.println("FieldInspectionBB.completeRoutingStep4NewCase | new case ID: " + freshCse.getCaseID());
                onRouteFieldInitiatedFinToInspectable(freshCse);
            } else {
                System.out.println("FieldInspectionBB.completeRoutingStep4NewCase | ERROR Creating new case!: ");

            }
        }
        getSessionInspectionConductor().refreshFieldFinLists(null);
        return outcome;
    }

    /**
     * Listener to complete routing with step 4 new occupancy period
     *
     * @return the occ period profile page that shold contain the freshly routed
     * Field Inspection
     */
    public String completeRoutingStep4CreateNewOccPeriod() {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        System.out.println("FieldInspectionBB.completeRoutingStep4NewOccPeriod ");
        PropertyProfileBB propPfBB = (PropertyProfileBB) getFacesContext().getApplication().evaluateExpressionGet(getFacesContext(), "#{propertyProfileBB}", PropertyProfileBB.class);
        String outcome;
        outcome = propPfBB.onOccPeriodCreateCommitButtonChange();
        OccPeriodDataHeavy freshPeriod = getSessionBean().getSessOccPeriod();
        if (freshPeriod != null) {
            onRouteFieldInitiatedFinToInspectable(freshPeriod);
            getSessionInspectionConductor().registerSessionInspectable(currentInspectable);
            
        } else {
            System.out.println("FieldInspectionBB.completeRoutingStep4NewOccPeriod | Error creating new period so no home for FIN");
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fatal error: Unable to setup permit file for field inspection assignment; Complain to dev! ", ""));
        }
        getSessionInspectionConductor().refreshFieldFinLists(null);
        return outcome;
    }

    /**
     * Returns user to previous step
     *
     * @param ev
     */
    public void onFinRoutingFlowGoBackToStep1(ActionEvent ev) {
        finRoutingActiveStep = FieldFinRoutingStepEnum.PROPERTY.getStepNumber1Based();
        System.out.println("FieldInspectionBB.onFinRoutingFlowGoBackToStep1");
    }

    /**
     * Returns user to previous step
     *
     * @param ev
     */
    public void onFinRoutingFlowGoBackToStep2(ActionEvent ev) {
        finRoutingActiveStep = FieldFinRoutingStepEnum.PATHWAY.getStepNumber1Based();
        System.out.println("FieldInspectionBB.onFinRoutingFlowGoBackToStep2");

    }

    /**
     * Returns user to previous step
     *
     * @param ev
     */
    public void onFinRoutingFlowGoBackToStep3(ActionEvent ev) {
        finRoutingActiveStep = FieldFinRoutingStepEnum.PERIODS_CASES_EXISTING.getStepNumber1Based();
        System.out.println("FieldInspectionBB.onFinRoutingFlowGoBackToStep3");

    }

    // *****************************************
    // ************ GETTERS AND SETTERS ********
    // *****************************************
    public List<OccChecklistTemplate> getChecklistTemplateList() {
        return checklistTemplateList;
    }

    public OccChecklistTemplate getSelectedChecklistTemplate() {
        return selectedChecklistTemplate;
    }

    public void setSelectedChecklistTemplate(OccChecklistTemplate selectedChecklistTemplate) {
        this.selectedChecklistTemplate = selectedChecklistTemplate;
    }

    public User getSelectedInspector() {
        return selectedInspector;
    }

    public void setSelectedInspector(User selectedInspector) {
        this.selectedInspector = selectedInspector;
    }

    public OccLocationDescriptor getCurrentLocationDescriptor() {
        return currentLocationDescriptor;
    }

    public void setCurrentLocationDescriptor(OccLocationDescriptor currentLocationDescriptor) {
        this.currentLocationDescriptor = currentLocationDescriptor;
    }

  
    public FieldInspection getCurrentInspection() {
        return currentInspection;
    }

    public void setCurrentInspection(FieldInspection currentInspection) {
        this.currentInspection = currentInspection;
    }

    public OccInspectedSpace getCurrentInspectedSpace() {
        return currentInspectedSpace;
    }

    public void setCurrentInspectedSpace(OccInspectedSpace currentInspectedSpace) {
        this.currentInspectedSpace = currentInspectedSpace;
    }

    public boolean isEditModeInspectionMetadata() {
        return editModeInspectionMetadata;
    }

    public void setEditModeInspectionMetadata(boolean editModeInspectionMetadata) {
        this.editModeInspectionMetadata = editModeInspectionMetadata;
    }

    /**
     * @return the currentSpaceType
     */
    public OccSpaceTypeChecklistified getCurrentSpaceType() {
        return currentSpaceType;
    }

    /**
     * @param currentSpaceType the currentSpaceType to set
     */
    public void setCurrentSpaceType(OccSpaceTypeChecklistified currentSpaceType) {
        this.currentSpaceType = currentSpaceType;
    }

    /**
     * @return the selectedElementStatusForBatch
     */
    public OccInspectionStatusEnum getSelectedElementStatusForBatch() {
        return selectedElementStatusForBatch;
    }

    /**
     * @param selectedElementStatusForBatch the selectedElementStatusForBatch to
     * set
     */
    public void setSelectedElementStatusForBatch(OccInspectionStatusEnum selectedElementStatusForBatch) {
        this.selectedElementStatusForBatch = selectedElementStatusForBatch;
    }

    /**
     * @return the currentInspectedSpaceElement
     */
    public OccInspectedSpaceElement getCurrentInspectedSpaceElement() {
        return currentInspectedSpaceElement;
    }

    /**
     * @param currentInspectedSpaceElement the currentInspectedSpaceElement to
     * set
     */
    public void setCurrentInspectedSpaceElement(OccInspectedSpaceElement currentInspectedSpaceElement) {
        this.currentInspectedSpaceElement = currentInspectedSpaceElement;
    }

    /**
     * @return the useDefaultFindingsOnCurrentOISE
     */
    public boolean isUseDefaultFindingsOnCurrentOISE() {
        return useDefaultFindingsOnCurrentOISE;
    }

    /**
     * @param useDefaultFindingsOnCurrentOISE the
     * useDefaultFindingsOnCurrentOISE to set
     */
    public void setUseDefaultFindingsOnCurrentOISE(boolean useDefaultFindingsOnCurrentOISE) {
        this.useDefaultFindingsOnCurrentOISE = useDefaultFindingsOnCurrentOISE;
    }

    /**
     * @return the causeList
     */
    public List<OccInspectionCause> getCauseList() {
        return causeList;
    }

    /**
     * @return the determinationList
     */
    public List<OccInspectionDetermination> getDeterminationList() {
        return determinationList;
    }

    /**
     * @param causeList the causeList to set
     */
    public void setCauseList(List<OccInspectionCause> causeList) {
        this.causeList = causeList;
    }

    /**
     * @param determinationList the determinationList to set
     */
    public void setDeterminationList(List<OccInspectionDetermination> determinationList) {
        this.determinationList = determinationList;
    }

    /**
     * @return the occPeriodIDFortransferFormField
     */
    public int getOccPeriodIDFortransferFormField() {
        return occPeriodIDFortransferFormField;
    }

    /**
     * @param occPeriodIDFortransferFormField the
     * occPeriodIDFortransferFormField to set
     */
    public void setOccPeriodIDFortransferFormField(int occPeriodIDFortransferFormField) {
        this.occPeriodIDFortransferFormField = occPeriodIDFortransferFormField;
    }

    /**
     * @return the selectedDetermination
     */
    public OccInspectionDetermination getSelectedDetermination() {
        return selectedDetermination;
    }

    /**
     * @param selectedDetermination the selectedDetermination to set
     */
    public void setSelectedDetermination(OccInspectionDetermination selectedDetermination) {
        this.selectedDetermination = selectedDetermination;
    }

    /**
     * @return the reportConfigFIN
     */
    public ReportConfigOccInspection getReportConfigFIN() {
        return reportConfigFIN;
    }

    /**
     * @param reportConfigFIN the reportConfigFIN to set
     */
    public void setReportConfigFIN(ReportConfigOccInspection reportConfigFIN) {
        this.reportConfigFIN = reportConfigFIN;
    }

    /**
     * @return the failSeverityList
     */
    public List<IntensityClass> getFailSeverityList() {
        return failSeverityList;
    }

    /**
     * @param failSeverityList the failSeverityList to set
     */
    public void setFailSeverityList(List<IntensityClass> failSeverityList) {
        this.failSeverityList = failSeverityList;
    }

    /**
     * @return the currentInspectable
     */
    public IFace_inspectable getCurrentInspectable() {
        return currentInspectable;
    }

    /**
     * @param currentInspectable the currentInspectable to set
     */
    public void setCurrentInspectable(IFace_inspectable currentInspectable) {
        this.currentInspectable = currentInspectable;
    }

    /**
     * @return the inspectionListComponentForUpdate
     */
    public String getInspectionListComponentForUpdate() {
        return inspectionListComponentForUpdate;
    }

    /**
     * @param inspectionListComponentForUpdate the
     * inspectionListComponentForUpdate to set
     */
    public void setInspectionListComponentForUpdate(String inspectionListComponentForUpdate) {
        this.inspectionListComponentForUpdate = inspectionListComponentForUpdate;
    }

    /**
     * @return the formMigrateFailedItemsOnFinalization
     */
    public boolean isFormMigrateFailedItemsOnFinalization() {
        return formMigrateFailedItemsOnFinalization;
    }

    /**
     * @param formMigrateFailedItemsOnFinalization the
     * formMigrateFailedItemsOnFinalization to set
     */
    public void setFormMigrateFailedItemsOnFinalization(boolean formMigrateFailedItemsOnFinalization) {
        this.formMigrateFailedItemsOnFinalization = formMigrateFailedItemsOnFinalization;
    }

    /**
     * @return the ordinanceViewOptionsList
     */
    public List<ViewOptionsOccChecklistItemsEnum> getOrdinanceViewOptionsList() {
        return ordinanceViewOptionsList;
    }

    /**
     * @param ordinanceViewOptionsList the ordinanceViewOptionsList to set
     */
    public void setOrdinanceViewOptionsList(List<ViewOptionsOccChecklistItemsEnum> ordinanceViewOptionsList) {
        this.ordinanceViewOptionsList = ordinanceViewOptionsList;
    }

    /**
     * @return the editModeDispatch
     */
    public boolean isEditModeDispatch() {
        return editModeDispatch;
    }

    /**
     * @param editModeDispatch the editModeDispatch to set
     */
    public void setEditModeDispatch(boolean editModeDispatch) {
        this.editModeDispatch = editModeDispatch;
    }

    /**
     * @return the currentDispatch
     */
    public OccInspectionDispatch getCurrentDispatch() {
        return currentDispatch;
    }

    /**
     * @param currentDispatch the currentDispatch to set
     */
    public void setCurrentDispatch(OccInspectionDispatch currentDispatch) {
        this.currentDispatch = currentDispatch;
    }

    /**
     * @return the permissionAllowAddUpdateInspectionMetadata
     */
    public boolean isPermissionAllowAddUpdateInspectionMetadata() {
        return permissionAllowAddUpdateInspectionMetadata;
    }

    /**
     * @return the permissionAllowInspectionConduct
     */
    public boolean isPermissionAllowInspectionConduct() {
        return permissionAllowInspectionConduct;
    }

    /**
     * @return the permissionAllowInspectionFinalize
     */
    public boolean isPermissionAllowInspectionFinalize() {
        return permissionAllowInspectionFinalize;
    }

    public String getOrdinanceFilterText() {
        return ordinanceFilterText;
    }

    public void setOrdinanceFilterText(String ordinanceFilterText) {
        this.ordinanceFilterText = ordinanceFilterText;
    }

    public List<OccInsElementGroup> getInspectedElementGroupListFiltered() {
        return inspectedElementGroupListFiltered;
    }

    public void setInspectedElementGroupListFiltered(List<OccInsElementGroup> inspectedElementGroupListFiltered) {
        this.inspectedElementGroupListFiltered = inspectedElementGroupListFiltered;
    }

    public List<OccLocationDescriptor> getAllUniqueLocationDescriptors() {
        return allUniqueLocationDescriptors;
    }

    public void setAllUniqueLocationDescriptors(List<OccLocationDescriptor> allUniqueLocationDescriptors) {
        this.allUniqueLocationDescriptors = allUniqueLocationDescriptors;
    }

    /**
     * @return the humanLinkCandidateList
     */
    public List<HumanLink> getHumanLinkCandidateList() {
        return humanLinkCandidateList;
    }

    /**
     * @param humanLinkCandidateList the humanLinkCandidateList to set
     */
    public void setHumanLinkCandidateList(List<HumanLink> humanLinkCandidateList) {
        this.humanLinkCandidateList = humanLinkCandidateList;
    }

    /**
     * @return the inspectablePropertyHost
     */
    public PropertyDataHeavy getInspectablePropertyHost() {
        return inspectablePropertyHost;
    }

    /**
     * @param inspectablePropertyHost the inspectablePropertyHost to set
     */
    public void setInspectablePropertyHost(PropertyDataHeavy inspectablePropertyHost) {
        this.inspectablePropertyHost = inspectablePropertyHost;
    }

    /**
     * @return the filterByFullOrdText
     */
    public boolean isFilterByFullOrdText() {
        return filterByFullOrdText;
    }

    /**
     * @param filterByFullOrdText the filterByFullOrdText to set
     */
    public void setFilterByFullOrdText(boolean filterByFullOrdText) {
        this.filterByFullOrdText = filterByFullOrdText;
    }

    /**
     * @return the inspectedElementListFiltered
     */
    public List<OccInspectedSpaceElement> getInspectedElementListFiltered() {
        return inspectedElementListFiltered;
    }

    /**
     * @param inspectedElementListFiltered the inspectedElementListFiltered to
     * set
     */
    public void setInspectedElementListFiltered(List<OccInspectedSpaceElement> inspectedElementListFiltered) {
        this.inspectedElementListFiltered = inspectedElementListFiltered;
    }

    /**
     * @return the fieldInitFinList
     */
    public List<FieldInspectionLight> getFieldInitFinList() {
        return fieldInitFinList;
    }

    /**
     * @param fieldInitFinList the fieldInitFinList to set
     */
    public void setFieldInitFinList(List<FieldInspectionLight> fieldInitFinList) {
        this.fieldInitFinList = fieldInitFinList;
    }

    /**
     * @return the formFailedFINFollowUpWindow
     */
    public int getFormFailedFINFollowUpWindow() {
        return formFailedFINFollowUpWindow;
    }

    /**
     * @param formFailedFINFollowUpWindow the formFailedFINFollowUpWindow to set
     */
    public void setFormFailedFINFollowUpWindow(int formFailedFINFollowUpWindow) {
        this.formFailedFINFollowUpWindow = formFailedFINFollowUpWindow;
    }

    /**
     * @return the finRoutingActiveStep
     */
    public int getFinRoutingActiveStep() {
        return finRoutingActiveStep;
    }

    /**
     * @param finRoutingActiveStep the finRoutingActiveStep to set
     */
    public void setFinRoutingActiveStep(int finRoutingActiveStep) {
        this.finRoutingActiveStep = finRoutingActiveStep;
    }

    /**
     * @return the finRoutingStepsModel
     */
    public MenuModel getFinRoutingStepsModel() {
        return finRoutingStepsModel;
    }

    /**
     * @param finRoutingStepsModel the finRoutingStepsModel to set
     */
    public void setFinRoutingStepsModel(MenuModel finRoutingStepsModel) {
        this.finRoutingStepsModel = finRoutingStepsModel;
    }

    /**
     * @return the finForRouting
     */
    public FieldInspection getFinForRouting() {
        return finForRouting;
    }

    /**
     * @param finForRouting the finForRouting to set
     */
    public void setFinForRouting(FieldInspection finForRouting) {
        this.finForRouting = finForRouting;
    }

    /**
     * @return the fieldRoutingPropertyTarget
     */
    public PropertyDataHeavy getFieldRoutingPropertyTarget() {
        return fieldRoutingPropertyTarget;
    }

    /**
     * @param fieldRoutingPropertyTarget the fieldRoutingPropertyTarget to set
     */
    public void setFieldRoutingPropertyTarget(PropertyDataHeavy fieldRoutingPropertyTarget) {
        this.fieldRoutingPropertyTarget = fieldRoutingPropertyTarget;
    }

    /**
     * @return the finRoutingUsingOccPathway
     */
    public boolean isFinRoutingUsingOccPathway() {
        return finRoutingUsingOccPathway;
    }

    /**
     * @param finRoutingUsingOccPathway the finRoutingUsingOccPathway to set
     */
    public void setFinRoutingUsingOccPathway(boolean finRoutingUsingOccPathway) {
        this.finRoutingUsingOccPathway = finRoutingUsingOccPathway;
    }

    /**
     * @return the rawLocationHolding
     */
    public String getRawLocationHolding() {
        return rawLocationHolding;
    }

    /**
     * @param rawLocationHolding the rawLocationHolding to set
     */
    public void setRawLocationHolding(String rawLocationHolding) {
        this.rawLocationHolding = rawLocationHolding;
    }

    /**
     * @return the ordinanceEntryViewTreeMode
     */
    public boolean isOrdinanceEntryViewTreeMode() {
        return ordinanceEntryViewTreeMode;
    }

    /**
     * @param ordinanceEntryViewTreeMode the ordinanceEntryViewTreeMode to set
     */
    public void setOrdinanceEntryViewTreeMode(boolean ordinanceEntryViewTreeMode) {
        this.ordinanceEntryViewTreeMode = ordinanceEntryViewTreeMode;
    }

    /**
     * @return the inspectorCandidateList
     */
    public List<User> getInspectorCandidateList() {
        return inspectorCandidateList;
    }

    /**
     * @param inspectorCandidateList the inspectorCandidateList to set
     */
    public void setInspectorCandidateList(List<User> inspectorCandidateList) {
        this.inspectorCandidateList = inspectorCandidateList;
    }

   
    /**
     * @return the inspectionTimeStartForValidation
     */
    public LocalTime getInspectionTimeStartForValidation() {
        return inspectionTimeStartForValidation;
    }

    /**
     * @param inspectionTimeStartForValidation the inspectionTimeStartForValidation to set
     */
    public void setInspectionTimeStartForValidation(LocalTime inspectionTimeStartForValidation) {
        this.inspectionTimeStartForValidation = inspectionTimeStartForValidation;
    }

    /**
     * @return the inspectionTimeEndForValidation
     */
    public LocalTime getInspectionTimeEndForValidation() {
        return inspectionTimeEndForValidation;
    }

    /**
     * @param inspectionTimeEndForValidation the inspectionTimeEndForValidation to set
     */
    public void setInspectionTimeEndForValidation(LocalTime inspectionTimeEndForValidation) {
        this.inspectionTimeEndForValidation = inspectionTimeEndForValidation;
    }

    /**
     * @return the finCreateEditcomponentForRefresh
     */
    public String getFinCreateEditcomponentForRefresh() {
        return finCreateEditcomponentForRefresh;
    }

    /**
     * @param finCreateEditcomponentForRefresh the finCreateEditcomponentForRefresh to set
     */
    public void setFinCreateEditcomponentForRefresh(String finCreateEditcomponentForRefresh) {
        this.finCreateEditcomponentForRefresh = finCreateEditcomponentForRefresh;
    }

    /**
     * @return the dimensionEntryInchesOnly
     */
    public boolean isDimensionEntryInchesOnly() {
        return dimensionEntryInchesOnly;
    }

    /**
     * @param dimensionEntryInchesOnly the dimensionEntryInchesOnly to set
     */
    public void setDimensionEntryInchesOnly(boolean dimensionEntryInchesOnly) {
        this.dimensionEntryInchesOnly = dimensionEntryInchesOnly;
    }

    /**
     * @return the dim1LengthFt
     */
    public float getDim1LengthFt() {
        return dim1LengthFt;
    }

    /**
     * @param dim1LengthFt the dim1LengthFt to set
     */
    public void setDim1LengthFt(float dim1LengthFt) {
        this.dim1LengthFt = dim1LengthFt;
    }

    /**
     * @return the dim1WidthFt
     */
    public float getDim1WidthFt() {
        return dim1WidthFt;
    }

    /**
     * @param dim1WidthFt the dim1WidthFt to set
     */
    public void setDim1WidthFt(float dim1WidthFt) {
        this.dim1WidthFt = dim1WidthFt;
    }

    /**
     * @return the dim2LengthFt
     */
    public float getDim2LengthFt() {
        return dim2LengthFt;
    }

    /**
     * @param dim2LengthFt the dim2LengthFt to set
     */
    public void setDim2LengthFt(float dim2LengthFt) {
        this.dim2LengthFt = dim2LengthFt;
    }

    /**
     * @return the dim2WidthFt
     */
    public float getDim2WidthFt() {
        return dim2WidthFt;
    }

    /**
     * @param dim2WidthFt the dim2WidthFt to set
     */
    public void setDim2WidthFt(float dim2WidthFt) {
        this.dim2WidthFt = dim2WidthFt;
    }

    /**
     * @return the largeFindingsBoxes
     */
    public boolean isLargeFindingsBoxes() {
        return largeFindingsBoxes;
    }

    /**
     * @param largeFindingsBoxes the largeFindingsBoxes to set
     */
    public void setLargeFindingsBoxes(boolean largeFindingsBoxes) {
        this.largeFindingsBoxes = largeFindingsBoxes;
    }

    /**
     * @return the finRevertXMuni
     */
    public boolean isFinRevertXMuni() {
        return finRevertXMuni;
    }

    /**
     * @param finRevertXMuni the finRevertXMuni to set
     */
    public void setFinRevertXMuni(boolean finRevertXMuni) {
        this.finRevertXMuni = finRevertXMuni;
    }

    /**
     * @return the finRevertXMuniCandidateList
     */
    public List<Municipality> getFinRevertXMuniCandidateList() {
        return finRevertXMuniCandidateList;
    }

    /**
     * @param finRevertXMuniCandidateList the finRevertXMuniCandidateList to set
     */
    public void setFinRevertXMuniCandidateList(List<Municipality> finRevertXMuniCandidateList) {
        this.finRevertXMuniCandidateList = finRevertXMuniCandidateList;
    }

    /**
     * @return the finRevertXMuniSelected
     */
    public Municipality getFinRevertXMuniSelected() {
        return finRevertXMuniSelected;
    }

    /**
     * @param finRevertXMuniSelected the finRevertXMuniSelected to set
     */
    public void setFinRevertXMuniSelected(Municipality finRevertXMuniSelected) {
        this.finRevertXMuniSelected = finRevertXMuniSelected;
    }

    /**
     * @return the showStoreFindingsLinks
     */
    public boolean isShowStoreFindingsLinks() {
        return showStoreFindingsLinks;
    }

    /**
     * @param showStoreFindingsLinks the showStoreFindingsLinks to set
     */
    public void setShowStoreFindingsLinks(boolean showStoreFindingsLinks) {
        this.showStoreFindingsLinks = showStoreFindingsLinks;
    }

    
  
}
