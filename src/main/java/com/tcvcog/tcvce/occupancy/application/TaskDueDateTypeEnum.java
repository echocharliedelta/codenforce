/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.occupancy.application;


/**
 * Represents logic pathways for determining a task's due date
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */
public enum TaskDueDateTypeEnum {
    
    ABSOLUTE (                      "Absolute due date", 
                                    "Date fixed to calendar"),
    
    RELATIVETOTASK (                "Due date relative to another task's due date", 
                                    "This task will shift its due date to maintain a consistent distance from the linked task's due date."),
    
    RELATIVETOAPPLICATIONDOR (      "Due date relative to the linked occupancy application's date of record", 
                                    "This task will shift its due date to maintain a consistent distance from the linked application's date of record."),
    
    RELATIVETOINSPECTIONDOR (       "Due date relative to the linked inspection's date of record", 
                                    "This task will shift its due date to maintain a consistent distance from the linked inspetion's date of record."),
    
    RELATIVETOPERMITDOR (           "Due date relative to the linked permit's date of record", 
                                    "This task will shift its due date to maintain a consistent distance from the linked permit's date of record.");
    
    private final String title;
    private final String description;
    
    private TaskDueDateTypeEnum(String t, String descr){
        title = t;
        description = descr;
    }
    
    public String getTitle(){
        return title;
    }
    
    public String getDescription(){
        return description;
    }
    
}
