/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.occupancy.application;

import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.User;
import com.tcvcog.tcvce.entities.UserMuniAuthPeriod;

/**
 * Represents a user in a role who can be assigned a task
 * @author pierre15
 */
public class TaskAssignee {
    
    private int mappingID;
    private UserMuniAuthPeriod userUMAP;
    private TaskAssignedRoleEnum role;
    private Municipality muni;
    private String notes;
    private boolean active;
    
    
    /**
     * No Args constructor
     */
    public TaskAssignee(){
        // nothing to do 
    }
    
    /**
     * Constructor for TaskAssignee
     * @param umap
     * @param r 
     */
    public TaskAssignee(UserMuniAuthPeriod umap, TaskAssignedRoleEnum r){
        userUMAP = umap;
        role = r;
    }

    /**
     * @return the user
     */
    public UserMuniAuthPeriod getUser() {
        return userUMAP;
    }

    /**
     * @param user the user to set
     */
    public void setUser(UserMuniAuthPeriod user) {
        this.userUMAP = user;
    }

    /**
     * @return the role
     */
    public TaskAssignedRoleEnum getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(TaskAssignedRoleEnum role) {
        this.role = role;
    }

    /**
     * @return the mappingID
     */
    public int getMappingID() {
        return mappingID;
    }

    /**
     * @param mappingID the mappingID to set
     */
    public void setMappingID(int mappingID) {
        this.mappingID = mappingID;
    }

    /**
     * @return the muni
     */
    public Municipality getMuni() {
        return muni;
    }

    /**
     * @param muni the muni to set
     */
    public void setMuni(Municipality muni) {
        this.muni = muni;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }
    
}
