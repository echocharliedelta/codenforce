/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.occupancy.application;

import com.tcvcog.tcvce.entities.BlobLight;
import com.tcvcog.tcvce.entities.CECase;
import com.tcvcog.tcvce.entities.EventCnF;
import com.tcvcog.tcvce.entities.Human;
import com.tcvcog.tcvce.entities.Payment;
import com.tcvcog.tcvce.entities.UserMuniAuthPeriod;
import com.tcvcog.tcvce.entities.occupancy.FieldInspection;
import com.tcvcog.tcvce.entities.occupancy.FieldInspectionLight;
import com.tcvcog.tcvce.entities.occupancy.OccPermit;
import com.tcvcog.tcvce.entities.occupancy.OccPermitApplication;
import java.time.LocalDateTime;

/**
 * Embodies objects that can be linked to a task assigned
 * @author pierre15
 */
public class TaskLinkage {
    
    private int linkID;
    private int taskAssignedID;
    
    private OccPermitApplication permitApplication;
    private EventCnF event;
    private FieldInspectionLight fieldInspection;
    private OccPermit permit;
    private Payment payment;
    private BlobLight blob;
    private CECase ceCase;
    private Human human;
    private LocalDateTime deactivatedTS;
    private UserMuniAuthPeriod deactivatedBy;
    /**
     * no arg public constructo
     */
    public TaskLinkage(){
        
    }

    /**
     * Constructor that will return a skeleton object 
     * with the given TaskAssigned id properly
     * injected for future DB writes
     * @param task 
     */
    public TaskLinkage(TaskAssigned task){
        storeAssignmentID(task);
        
    }
    
    /**
     * Linked object type specific constructor
     * @param task
     * @param app I'll inject this task's assignment ID into my link
     */
    public TaskLinkage(TaskAssigned task, OccPermitApplication app){
        storeAssignmentID(task);
        this.permitApplication = app;
    }
    

    
    /**
     * Linked object type specific constructor
     * @param task
     * @param event
     */
    public TaskLinkage(TaskAssigned task, EventCnF event) {
        storeAssignmentID(task);
        this.event = event;
    }

    
    /**
     * Linked object type specific constructor
     * @param task
     * @param inspection
     */
    public TaskLinkage(TaskAssigned task, FieldInspection inspection) {
        storeAssignmentID(task);
        this.fieldInspection = inspection;
    }

    
    /**
     * Linked object type specific constructor
     * @param task
     * @param permit
     */
    public TaskLinkage(TaskAssigned task, OccPermit permit) {
        storeAssignmentID(task);
        this.permit = permit;
    }

    
    /**
     * Linked object type specific constructor
     * @param task
     * @param payment
     */
    public TaskLinkage(TaskAssigned task, Payment payment) {
        storeAssignmentID(task);
        this.payment = payment;
    }

    
    /**
     * Linked object type specific constructor
     * @param task
     * @param blob
     */
    public TaskLinkage(TaskAssigned task, BlobLight blob) {
        storeAssignmentID(task);
        this.blob = blob;
    }

    
    /**
     * Linked object type specific constructor
     * @param task
     * @param ceCase
     */
    public TaskLinkage(TaskAssigned task, CECase ceCase) {
        storeAssignmentID(task);
        this.ceCase = ceCase;
    }

    
    /**
     * Linked object type specific constructor
     * @param task
     * @param human
     */
    public TaskLinkage(TaskAssigned task, Human human) {
        storeAssignmentID(task);
        this.human = human;
    }

    /**
     * Constructor without any inputted linked object type
     * @param task 
     */
    private void storeAssignmentID(TaskAssigned task){
        if(task != null){
            taskAssignedID = task.getAssignmentID();
        }
        
    }

    /**
     * @return the linkID
     */
    public int getLinkID() {
        return linkID;
    }

    /**
     * @param linkID the linkID to set
     */
    public void setLinkID(int linkID) {
        this.linkID = linkID;
    }

    /**
     * @return the taskAssignedID
     */
    public int getTaskAssignedID() {
        return taskAssignedID;
    }

    /**
     * @param taskAssignedID the taskAssignedID to set
     */
    public void setTaskAssignedID(int taskAssignedID) {
        this.taskAssignedID = taskAssignedID;
    }

    /**
     * @return the permitApplication
     */
    public OccPermitApplication getPermitApplication() {
        return permitApplication;
    }

    /**
     * @param permitApplication the permitApplication to set
     */
    public void setPermitApplication(OccPermitApplication permitApplication) {
        this.permitApplication = permitApplication;
    }

    /**
     * @return the event
     */
    public EventCnF getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(EventCnF event) {
        this.event = event;
    }

    /**
     * @return the permit
     */
    public OccPermit getPermit() {
        return permit;
    }

    /**
     * @param permit the permit to set
     */
    public void setPermit(OccPermit permit) {
        this.permit = permit;
    }

    /**
     * @return the payment
     */
    public Payment getPayment() {
        return payment;
    }

    /**
     * @param payment the payment to set
     */
    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    /**
     * @return the blob
     */
    public BlobLight getBlob() {
        return blob;
    }

    /**
     * @param blob the blob to set
     */
    public void setBlob(BlobLight blob) {
        this.blob = blob;
    }

    /**
     * @return the ceCase
     */
    public CECase getCeCase() {
        return ceCase;
    }

    /**
     * @param ceCase the ceCase to set
     */
    public void setCeCase(CECase ceCase) {
        this.ceCase = ceCase;
    }

    /**
     * @return the human
     */
    public Human getHuman() {
        return human;
    }

    /**
     * @param human the human to set
     */
    public void setHuman(Human human) {
        this.human = human;
    }

    /**
     * @return the deactivatedTS
     */
    public LocalDateTime getDeactivatedTS() {
        return deactivatedTS;
    }

    /**
     * @param deactivatedTS the deactivatedTS to set
     */
    public void setDeactivatedTS(LocalDateTime deactivatedTS) {
        this.deactivatedTS = deactivatedTS;
    }

    /**
     * @return the deactivatedBy
     */
    public UserMuniAuthPeriod getDeactivatedBy() {
        return deactivatedBy;
    }

    /**
     * @param deactivatedBy the deactivatedBy to set
     */
    public void setDeactivatedBy(UserMuniAuthPeriod deactivatedBy) {
        this.deactivatedBy = deactivatedBy;
    }

    /**
     * @return the fieldInspection
     */
    public FieldInspectionLight getFieldInspection() {
        return fieldInspection;
    }

    /**
     * @param fieldInspection the fieldInspection to set
     */
    public void setFieldInspection(FieldInspectionLight fieldInspection) {
        this.fieldInspection = fieldInspection;
    }
    
    
    
    
    /**
     * 
     * occapplication_appid 	INTEGER CONSTRAINT taskassignedlinkages_occpermitapplicationid_fk REFERENCES occpermitapplication (applicationid),
	event_eventid 			INTEGER CONSTRAINT taskassignedlinkages_eventid_fk REFERENCES event (eventid),
	inspection_inspectionid INTEGER CONSTRAINT taskassignedlinkages_inspectionid_fk REFERENCES occinspection (inspectionid),
	permit_permitid			INTEGER CONSTRAINT taskassignedlinkages_permitid_fk REFERENCES occpermit (permitid),
	payment_paymentid 		INTEGER CONSTRAINT taskassignedlinkages_paymentid_fk REFERENCES moneyledger (transactionid),
	document_photodocid 	INTEGER CONSTRAINT taskassignedlinkages_photodocid_fk REFERENCES photodoc (photodocid),
	cecase_caseid			INTEGER CONSTRAINT taskassignedlinkages_caseid_fk REFERENCES cecase (caseid),
	human_humanid 			INTEGER CONSTRAINT taskassignedlinkages_humanid_fk REFERENCES human (humanid),
	deactivatedts 			TIMESTAMP WITH TIME ZONE,
	deactivatedby_umapid 	INTEGER CONSTRAINT taskassignedlinkages_deactivatedbyumapid_fk REFERENCES loginmuniauthperiod (muniauthperiodid)
        * 
     */
    
}
