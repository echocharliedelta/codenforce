/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.occupancy.application;

import com.tcvcog.tcvce.entities.CodeElement;
import com.tcvcog.tcvce.entities.EnforceableCodeElement;
import com.tcvcog.tcvce.entities.Municipality;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Contains a list of TaskSpecification objects and descriptive metadata
 * @author pierre15
 */
public class TaskChain implements Serializable{
    
    private int chainID;
    private String title;
    private String description;
    
    private Municipality muni;
    private CodeElement governingOrdinance;
    
    private List<TaskSpecification> taskList;
    
    private boolean active;

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.chainID;
        hash = 97 * hash + Objects.hashCode(this.title);
        hash = 97 * hash + Objects.hashCode(this.description);
        hash = 97 * hash + Objects.hashCode(this.muni);
        hash = 97 * hash + Objects.hashCode(this.governingOrdinance);
        hash = 97 * hash + Objects.hashCode(this.taskList);
        hash = 97 * hash + (this.active ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TaskChain other = (TaskChain) obj;
        return this.chainID == other.chainID;
    }
    
    

    /**
     * @return the chainID
     */
    public int getChainID() {
        return chainID;
    }

    /**
     * @param chainID the chainID to set
     */
    public void setChainID(int chainID) {
        this.chainID = chainID;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the muni
     */
    public Municipality getMuni() {
        return muni;
    }

    /**
     * @param muni the muni to set
     */
    public void setMuni(Municipality muni) {
        this.muni = muni;
    }

    /**
     * @return the governingOrdinance
     */
    public CodeElement getGoverningOrdinance() {
        return governingOrdinance;
    }

    /**
     * @param governingOrdinance the governingOrdinance to set
     */
    public void setGoverningOrdinance(CodeElement governingOrdinance) {
        this.governingOrdinance = governingOrdinance;
    }

    /**
     * @return the taskList
     */
    public List<TaskSpecification> getTaskList() {
        return taskList;
    }

    /**
     * @param taskList the taskList to set
     */
    public void setTaskList(List<TaskSpecification> taskList) {
        this.taskList = taskList;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }
    
}
