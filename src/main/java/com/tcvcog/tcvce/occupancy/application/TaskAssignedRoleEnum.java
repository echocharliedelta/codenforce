/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.occupancy.application;

/**
 * Instead of assigning tasks to a specific user always, this enum describes
 * the roles who can receive a task assignment, and then users are mapped to roles
 * within municipalities
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */
public enum TaskAssignedRoleEnum {
    
    CODEOFFICER ("Code Officer"),
    
    FILEMANAGEROFFICER ("Manager of permit file"),
    
    MUNICIPALMANAGER ("Manager of municipality"),
    
    MUNICIPALSTAFF ("Staff of municipality"),
    
    COGSTAFF ("COG administrator"),
    
    EXTERNALENTITY ("Third party (not muni or cog staff)"),
    
    USERSPECIFIC ("User specific"),
    
    UNSPECIFIED ("Not specified");
    
    private final String title;
    
    private TaskAssignedRoleEnum(String t){
        title = t;
    }
    
    public String getTitle(){
        return title;
    }
    
    
    
}
