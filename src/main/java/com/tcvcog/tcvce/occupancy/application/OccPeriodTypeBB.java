package com.tcvcog.tcvce.occupancy.application;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.coordinators.EventCoordinator;
import com.tcvcog.tcvce.coordinators.MunicipalityCoordinator;
import com.tcvcog.tcvce.coordinators.OccInspectionCoordinator;
import com.tcvcog.tcvce.coordinators.OccupancyCoordinator;
import com.tcvcog.tcvce.coordinators.TaskCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.EventCategory;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.occupancy.OccChecklistTemplate;
import com.tcvcog.tcvce.entities.occupancy.OccPeriodType;
import com.tcvcog.tcvce.entities.occupancy.OccPermitType;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OccPeriodTypeBB extends BackingBeanUtils implements Serializable {
    private List<OccPeriodType> periodTypeList;
    private OccPeriodType currentOccPeriodType;

    private boolean editModeOccPeriodType;

    private boolean includeInactiveOccPeriodTypes;
    private boolean showAllMunis;

    private List<OccPermitType> permitTypeList;
    private List<TaskChain> taskChainList;
    private List<Municipality> muniList;
    private List<OccChecklistTemplate> inspectionChecklistCandidateList;
    private List<EventCategory> originationEventCategoryCandidateList;

    public OccPeriodTypeBB() {
    }

    @PostConstruct
    public void initBean()  {
        System.out.println("OccPeriodTypeBB: Init");
        MunicipalityCoordinator mc = getMuniCoordinator();
        TaskCoordinator tc = getTaskCoordinator();
        EventCoordinator ec = getEventCoordinator();
        
        try {
            includeInactiveOccPeriodTypes = false;
            showAllMunis = true;
            editModeOccPeriodType = false;
            refreshOccPeriodTypeList();
            if (Objects.nonNull(getPeriodTypeList()) && !periodTypeList.isEmpty()) {
                setCurrentOccPeriodType(getPeriodTypeList().get(0));
            }
            muniList = mc.getMuniList(true);
            taskChainList = tc.getTaskChainList(null, false);
            originationEventCategoryCandidateList = ec.getEventCategoryList();
            
            refreshOccPermitTypeListComplete();
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }
    }
            
    /**
     * Loads/refreshes occ permit types
     */
    public void refreshOccPermitTypeListComplete(){
        OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            permitTypeList = oc.getOccPermitTypeListComplete(null, true);
        } catch (IntegrationException ex) {
            System.out.println(ex);
        }
        
    }

    /**
     * Loads a fresh lists of OccPeriodTypes from the db
     *
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public void refreshOccPeriodTypeList() throws BObStatusException {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        Municipality muni = null;
        if (!isShowAllMunis()) {
            muni = getSessionBean().getSessMuni();
        }
        try {
            setPeriodTypeList(oc.getOccPeriodTypeList(muni, !isIncludeInactiveOccPeriodTypes()));
        } catch (IntegrationException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to load OccPeriod Type List",
                            "This must be corrected by the system administrator"));
        }
    }
    
    /**
     * Grabs a fresh copy of the current period type
     */
    private void refreshCurrentOccPeriodType(){
        OccupancyCoordinator oc = getOccupancyCoordinator();
        if(currentOccPeriodType != null && currentOccPeriodType.getOccPeriodTypeID() != 0){
            try {
                currentOccPeriodType = oc.getOccPeriodType(currentOccPeriodType.getOccPeriodTypeID());
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
                 getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to refresh occ period type",
                            "This must be corrected by the system administrator"));
            } 
            
        }
    }

    /**
     * Injects the given period type into the current slot
     * @param periodType 
     */
    public void viewOccPeriodType(OccPeriodType periodType) {
        currentOccPeriodType = periodType;
        refreshCurrentOccPeriodType();
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Viewing OccPeriod Type: " + periodType.getOccPeriodTypeID(), ""));
    }

    /**
     * Listener for users to click the add or the edit button
     *
     * @param ev
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public void toggleOccPeriodTypeAddEditButtonChange(ActionEvent ev) throws BObStatusException {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        System.out.println("toggle OccPeriodType AddEditButtonChange");
        if (editModeOccPeriodType) {
            if (Objects.nonNull(getCurrentOccPeriodType()) && getCurrentOccPeriodType().getOccPeriodTypeID() == 0) {
                onInsertOccPeriodTypeCommit();
            } else {
                onUpdateOccPeriodTypeCommit();
            }
            refreshOccPeriodTypeList();
            refreshCurrentOccPeriodType();
        } else {
            // entering edit mode--build our candidate checklist list
            try {
                inspectionChecklistCandidateList = oic.getOccChecklistTemplateList(currentOccPeriodType.getMuni(), false);
            } catch (IntegrationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to build checklist candidate list",
                            ""));
            }
        }
        editModeOccPeriodType = !isEditModeOccPeriodType();
    }

    /**
     * Begins the OccPeriodType creation process
     *
     * @param ev
     */
    public void onOccPeriodTypeAddInit(ActionEvent ev) {
        System.out.println("OccPermiTypeBB.onOccPeriodTypeAddInit");
        OccupancyCoordinator oc = getOccupancyCoordinator();
        currentOccPeriodType = oc.getOccPeriodTypeSkeleton(getSessionBean().getSessMuni());
        setEditModeOccPeriodType(true);
    }

    /**
     * Listener for users to cancel their OccPeriod Type add/edit operation
     *
     * @param ev
     */
    public void onOperationOccPeriodTypeAddEditAbort(ActionEvent ev) {
        System.out.println("occPeriodTypeBB.toggleOccPeriodTypeAddEditButtonChange: ABORT");
        setEditModeOccPeriodType(false);
        editModeOccPeriodType = false;
    }

    /**
     * Works with coordinator to inject occ period type
     */
    public void onInsertOccPeriodTypeCommit() {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            int freshID = oc.insertOccPeriodType(currentOccPeriodType, getSessionBean().getSessUser());
            currentOccPeriodType.setOccPeriodTypeID(freshID);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Successful Insert New OccPeriod Type!", ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
        }
    }

    /**
     * Coordinates with DB to update an occ period record
     */
    public void onUpdateOccPeriodTypeCommit() {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        try {
            oc.updateOccPeriodType(currentOccPeriodType, getSessionBean().getSessUser());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "OccPeriod Type updated with Id " + getCurrentOccPeriodType().getOccPeriodTypeID(), ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Failed to Update Period Type", ""));
        }
    }


    /**
     * @return the periodTypeList
     */
    public List<OccPeriodType> getPeriodTypeList() {
        return periodTypeList;
    }

    /**
     * @param periodTypeList the periodTypeList to set
     */
    public void setPeriodTypeList(List<OccPeriodType> periodTypeList) {
        this.periodTypeList = periodTypeList;
    }

    /**
     * @return the currentOccPeriodType
     */
    public OccPeriodType getCurrentOccPeriodType() {
        return currentOccPeriodType;
    }

    /**
     * @param currentOccPeriodType the currentOccPeriodType to set
     */
    public void setCurrentOccPeriodType(OccPeriodType currentOccPeriodType) {
        this.currentOccPeriodType = currentOccPeriodType;
    }

    /**
     * @return the editModeOccPeriodType
     */
    public boolean isEditModeOccPeriodType() {
        return editModeOccPeriodType;
    }

    /**
     * @param editModeOccPeriodType the editModeOccPeriodType to set
     */
    public void setEditModeOccPeriodType(boolean editModeOccPeriodType) {
        this.editModeOccPeriodType = editModeOccPeriodType;
    }

    /**
     * @return the includeInactiveOccPeriodTypes
     */
    public boolean isIncludeInactiveOccPeriodTypes() {
        return includeInactiveOccPeriodTypes;
    }

    /**
     * @param includeInactiveOccPeriodTypes the includeInactiveOccPeriodTypes to set
     */
    public void setIncludeInactiveOccPeriodTypes(boolean includeInactiveOccPeriodTypes) {
        this.includeInactiveOccPeriodTypes = includeInactiveOccPeriodTypes;
    }

    /**
     * @return the showAllMunis
     */
    public boolean isShowAllMunis() {
        return showAllMunis;
    }

    /**
     * @param showAllMunis the showAllMunis to set
     */
    public void setShowAllMunis(boolean showAllMunis) {
        this.showAllMunis = showAllMunis;
    }

    /**
     * @return the muniList
     */
    public List<Municipality> getMuniList() {
        return muniList;
    }

    /**
     * @param muniList the muniList to set
     */
    public void setMuniList(List<Municipality> muniList) {
        this.muniList = muniList;
    }

    /**
     * @return the permitTypeList
     */
    public List<OccPermitType> getPermitTypeList() {
        return permitTypeList;
    }

    /**
     * @param permitTypeList the permitTypeList to set
     */
    public void setPermitTypeList(List<OccPermitType> permitTypeList) {
        this.permitTypeList = permitTypeList;
    }

    /**
     * @return the taskChainList
     */
    public List<TaskChain> getTaskChainList() {
        return taskChainList;
    }

    /**
     * @param taskChainList the taskChainList to set
     */
    public void setTaskChainList(List<TaskChain> taskChainList) {
        this.taskChainList = taskChainList;
    }

    /**
     * @return the originationEventCategoryCandidateList
     */
    public List<EventCategory> getOriginationEventCategoryCandidateList() {
        return originationEventCategoryCandidateList;
    }

    /**
     * @param originationEventCategoryCandidateList the originationEventCategoryCandidateList to set
     */
    public void setOriginationEventCategoryCandidateList(List<EventCategory> originationEventCategoryCandidateList) {
        this.originationEventCategoryCandidateList = originationEventCategoryCandidateList;
    }

    /**
     * @return the inspectionChecklistCandidateList
     */
    public List<OccChecklistTemplate> getInspectionChecklistCandidateList() {
        return inspectionChecklistCandidateList;
    }

    /**
     * @param inspectionChecklistCandidateList the inspectionChecklistCandidateList to set
     */
    public void setInspectionChecklistCandidateList(List<OccChecklistTemplate> inspectionChecklistCandidateList) {
        this.inspectionChecklistCandidateList = inspectionChecklistCandidateList;
    }

   

}
