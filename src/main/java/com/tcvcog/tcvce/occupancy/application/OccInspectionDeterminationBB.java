/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tcvcog.tcvce.occupancy.application;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.coordinators.EventCoordinator;
import com.tcvcog.tcvce.coordinators.OccInspectionCoordinator;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.EventCategory;
import com.tcvcog.tcvce.entities.Icon;
import com.tcvcog.tcvce.entities.occupancy.OccInspectionDetermination;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class OccInspectionDeterminationBB extends BackingBeanUtils implements Serializable {
    private List<OccInspectionDetermination> occInspectionDeterminationList;
    private OccInspectionDetermination currentOccInspectionDetermination;

    private boolean editModeOccInspectionDeterminationInfo;
    private boolean occInspectionDeterminationCreateMode;

    private boolean includeInactiveOccInspectionDeterminations;
    private List<EventCategory> eventCategoryList;
    private List<Icon> iconCandidateList;
    
    private boolean showAllMunis;

    public OccInspectionDeterminationBB() {
    }
    
    @PostConstruct
    public void initBean() {
        System.out.println("OccInspection Determination BB: Init");
        EventCoordinator ec = getEventCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        
        includeInactiveOccInspectionDeterminations = false;
        occInspectionDeterminationCreateMode = false;
        editModeOccInspectionDeterminationInfo = false;
        showAllMunis = true;
        try {
            refreshOccInspectionDeterminationList();
            if (Objects.nonNull(occInspectionDeterminationList) && !occInspectionDeterminationList.isEmpty()) {
                currentOccInspectionDetermination = occInspectionDeterminationList.get(0);
            }
            iconCandidateList = sc.getIconList(false);
            eventCategoryList = ec.getEventCategoryList();
            Collections.sort(eventCategoryList);
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex);
        }
    }
    
    /**
     * Loads a fresh lists of OccInspection determinations from the db
     *
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public void refreshOccInspectionDeterminationList() throws BObStatusException {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        try {
            if(showAllMunis){
                occInspectionDeterminationList = oic.getOccInspectionDeterminationListComplete(!includeInactiveOccInspectionDeterminations, null);                
            } else {
                occInspectionDeterminationList = oic.getOccInspectionDeterminationListComplete(!includeInactiveOccInspectionDeterminations, getSessionBean().getSessMuni());                
            }
            System.out.println("Size of list:" + occInspectionDeterminationList.size());
        } catch (IntegrationException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to load OccInspection Determination List",
                            "This must be corrected by the system administrator"));
        }
    }

    
    
    /**
     * Listener for user toggles of the show all munis checkbox
     */
    public void onToggleShowAllMunis(){
        try {
            refreshOccInspectionDeterminationList();
        } catch (BObStatusException ex) {
            System.out.println(ex);
        }
    }
    
    /**
     * Listener for users to click the add or the edit button
     *
     * @param ev
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public void toggleOccInspectionDeterminationAddEditButtonChange(ActionEvent ev) throws BObStatusException {
        System.out.println("toggle OccDetermination AddEditButtonChange");
        if (editModeOccInspectionDeterminationInfo) {
            if (Objects.nonNull(currentOccInspectionDetermination) && currentOccInspectionDetermination.getDeterminationID() == 0) {
                onInsertOccInspectionDeterminationCommit();
            } else {
                onUpdateOccInspectionDeterminationCommit();
            }
            refreshOccInspectionDeterminationList();
        }
        editModeOccInspectionDeterminationInfo = !editModeOccInspectionDeterminationInfo;
    }
    
    /**
     * Begins the OccInspection Determination creation process
     *
     * @param ev
     */
    public void onOccInspectionDeterminationAddInit(ActionEvent ev) {
        System.out.println("OccInspDeterminationManageBB.onOccInspectionDeterminationAddInit");
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        currentOccInspectionDetermination = oic.getOccInspectionDeterminationSkeleton(getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
        editModeOccInspectionDeterminationInfo = true;
    }
    
    /**
     * Listener for users to cancel their OccInspection Determination add/edit operation
     *
     * @param ev
     */
    public void onOperationOccDeterminationAddEditAbort(ActionEvent ev) {
        System.out.println("occInspectionDeterminationManageBB.toggleOccInspectionDeterminationAddEditButtonChange: ABORT");
        editModeOccInspectionDeterminationInfo = false;
        occInspectionDeterminationCreateMode = false;
    }

    public void onInsertOccInspectionDeterminationCommit() {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        try {
            currentOccInspectionDetermination = oic.insertOccInspectionDetermination(currentOccInspectionDetermination, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Successful Insert New OccInspection Determination!", ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
        }
    }

    public void onUpdateOccInspectionDeterminationCommit() {
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        try {
            oic.updateOccInspectionDetermination(currentOccInspectionDetermination, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "OccInspection Determination updated with Id " + currentOccInspectionDetermination.getDeterminationID(), ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Failed to Update Determination", ""));
        }
    }
    
    /**
     * Listener for user request to start the nuking process of a OccInspectionDetermination
     *
     * @param inspectionDetermination
     */
    public void onOccInspectionDeterminationNukeInitButtonChange(OccInspectionDetermination inspectionDetermination) {
        currentOccInspectionDetermination = inspectionDetermination;
    }
    
     /**
     * Listener for user requests to commit a OccInspection Determination nuke operation
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
     public void onOccInspectionDeterminationNukeCommitButtonChange() throws AuthorizationException {
         OccInspectionCoordinator oic = getOccInspectionCoordinator();
         try {
             oic.deactivateOccInspectionDetermination(currentOccInspectionDetermination, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
             refreshOccInspectionDeterminationList();
             getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                     "OccInspection Determination deactivated with ID: " + currentOccInspectionDetermination.getDeterminationID(), ""));
             if (Objects.nonNull(occInspectionDeterminationList) && !occInspectionDeterminationList.isEmpty()) {
                 currentOccInspectionDetermination = occInspectionDeterminationList.get(0);
             } else {
                 currentOccInspectionDetermination = null;
             }
         } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
             System.out.println(ex);
             getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                     ex.getMessage(), ""));
         }
     }

    public void viewOccInspectionDetermination(OccInspectionDetermination occInspectionDetermination) {
        currentOccInspectionDetermination = occInspectionDetermination;
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Viewing OccInspection Determination: " + occInspectionDetermination.getTitle(), ""));
    }

    public List<OccInspectionDetermination> getOccInspectionDeterminationList() {
        return occInspectionDeterminationList;
    }

    public void setOccInspectionDeterminationList(List<OccInspectionDetermination> occInspectionDeterminationList) {
        this.occInspectionDeterminationList = occInspectionDeterminationList;
    }

    public OccInspectionDetermination getCurrentOccInspectionDetermination() {
        return currentOccInspectionDetermination;
    }

    public void setCurrentOccInspectionDetermination(OccInspectionDetermination currentOccInspectionDetermination) {
        this.currentOccInspectionDetermination = currentOccInspectionDetermination;
    }

    public boolean isEditModeOccInspectionDeterminationInfo() {
        return editModeOccInspectionDeterminationInfo;
    }

    public void setEditModeOccInspectionDeterminationInfo(boolean editModeOccInspectionDeterminationInfo) {
        this.editModeOccInspectionDeterminationInfo = editModeOccInspectionDeterminationInfo;
    }

    public boolean isOccInspectionDeterminationCreateMode() {
        return occInspectionDeterminationCreateMode;
    }

    public void setOccInspectionDeterminationCreateMode(boolean occInspectionDeterminationCreateMode) {
        this.occInspectionDeterminationCreateMode = occInspectionDeterminationCreateMode;
    }

    public boolean isIncludeInactiveOccInspectionDeterminations() {
        return includeInactiveOccInspectionDeterminations;
    }

    public void setIncludeInactiveOccInspectionDeterminations(boolean includeInactiveOccInspectionDeterminations) {
        this.includeInactiveOccInspectionDeterminations = includeInactiveOccInspectionDeterminations;
    }

    public List<EventCategory> getEventCategoryList() {
        return eventCategoryList;
    }

    public void setEventCategoryList(List<EventCategory> eventCategoryList) {
        this.eventCategoryList = eventCategoryList;
    }

    /**
     * @return the showAllMunis
     */
    public boolean isShowAllMunis() {
        return showAllMunis;
    }

    /**
     * @param showAllMunis the showAllMunis to set
     */
    public void setShowAllMunis(boolean showAllMunis) {
        this.showAllMunis = showAllMunis;
    }

    /**
     * @return the iconCandidateList
     */
    public List<Icon> getIconCandidateList() {
        return iconCandidateList;
    }

    /**
     * @param iconCandidateList the iconCandidateList to set
     */
    public void setIconCandidateList(List<Icon> iconCandidateList) {
        this.iconCandidateList = iconCandidateList;
    }
    
}
