/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tcvcog.tcvce.occupancy.application;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.coordinators.OccInspectionCoordinator;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.occupancy.OccInspectionCause;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.event.ActionEvent;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OccInspectionCauseBB extends BackingBeanUtils implements Serializable {
    private List<OccInspectionCause> causeList;
    private OccInspectionCause currentOccInspectionCause;

    private boolean editModeOccInspectionCauseInfo;
    private boolean occInspectionCauseCreateMode;

    private boolean includeInactiveOccInspectionCauses;
    private boolean showAllMunis;

    public OccInspectionCauseBB() {
    }

    @PostConstruct
    public void initBean(){
        System.out.println("OccInspection Cause BB: Init");
        includeInactiveOccInspectionCauses = false;
        occInspectionCauseCreateMode = false;
        editModeOccInspectionCauseInfo = false;
        showAllMunis = true;
        try {
            refreshOccInspectionCauseList();
        } catch (BObStatusException ex) {
            System.out.println(ex);
        }
        if (Objects.nonNull(causeList) && !causeList.isEmpty()) {
            currentOccInspectionCause = causeList.get(0);
        }
    }

    /**
     * Loads a fresh lists of OccInspection Causes from the db
     *
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public void refreshOccInspectionCauseList() throws BObStatusException {
        OccInspectionCoordinator coordinator = getOccInspectionCoordinator();
        try {
            if(showAllMunis){
                causeList = coordinator.getOccInspectionCauseListComplete(!includeInactiveOccInspectionCauses, null);
            } else {
                causeList = coordinator.getOccInspectionCauseListComplete(!includeInactiveOccInspectionCauses, getSessionBean().getSessMuni());
            }
        } catch (IntegrationException ex) {
            getFacesContext().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Unable to load OccInspection Cause List",
                            "This must be corrected by the system administrator"));
        }
    }

    /**
     * Listener for user toggles of the show all munis checkbox
     */
    public void onToggleShowAllMunis(){
        try {
            refreshOccInspectionCauseList();
        } catch (BObStatusException ex) {
            System.out.println(ex);
        }
    }
    
    
    /**
     * Listener for user requests to view the selected cause
     * @param inspectionCause 
     */
    public void viewOccInspectionCause(OccInspectionCause inspectionCause) {
        currentOccInspectionCause = inspectionCause;
        getFacesContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Viewing OccInspection Cause: " + inspectionCause.getTitle(), ""));
    }

    /**
     * Listener for users to click the add or the edit button
     *
     * @param ev
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void toggleOccInspectionCauseAddEditButtonChange(ActionEvent ev) throws BObStatusException, AuthorizationException {
        System.out.println("toggle OccInspectionCause AddEditButtonChange");
        if (editModeOccInspectionCauseInfo) {
            if (Objects.nonNull(currentOccInspectionCause) && currentOccInspectionCause.getCauseID() == 0) {
                onInsertOccInspectionCauseCommit();
            } else {
                onUpdateOccInspectionCauseCommit();
            }
            refreshOccInspectionCauseList();
        }
        editModeOccInspectionCauseInfo = !editModeOccInspectionCauseInfo;
    }

    /**
     * Begins the OccInspection Cause creation process
     *
     * @param ev
     */
    public void onOccInspectionCauseAddInit(ActionEvent ev) {
        System.out.println("OccInspCauseManageBB.onOccInspectionCauseAddInit");
        OccInspectionCoordinator coordinator = getOccInspectionCoordinator();
        currentOccInspectionCause = coordinator.getOccInspectionCauseSkeleton(getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
        editModeOccInspectionCauseInfo = true;
    }

    public void onInsertOccInspectionCauseCommit() throws AuthorizationException {
        OccInspectionCoordinator coordinator = getOccInspectionCoordinator();
        try {
            currentOccInspectionCause = coordinator.insertOccInspectionCause(currentOccInspectionCause, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Successful Insert New OccInspection Cause!", ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
        }
    }

    public void onUpdateOccInspectionCauseCommit() {
        OccInspectionCoordinator coordinator = getOccInspectionCoordinator();
        try {
            coordinator.updateOccInspectionCause(currentOccInspectionCause, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "OccInspection Cause updated with Id " + currentOccInspectionCause.getCauseID(), ""));
        } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Failed to Update OccInspection Cause", ""));
        }
    }


    /**
     * Listener for users to cancel their OccInspection Cause add/edit operation
     *
     * @param ev
     */
    public void onOperationOccCauseAddEditAbort(ActionEvent ev) {
        System.out.println("occInspectionCauseManageBB.toggleOccInspectionCauseAddEditButtonChange: ABORT");
        editModeOccInspectionCauseInfo = false;
        occInspectionCauseCreateMode = false;
    }

    /**
     * Listener for user request to start the nuking process of a OccInspectionCause
     *
     * @param inspectionCause
     */
    public void onOccInspectionCauseNukeInitButtonChange(OccInspectionCause inspectionCause) {
        currentOccInspectionCause = inspectionCause;
    }

    /**
     * Listener for user requests to commit a OccInspection Cause nuke operation
     *
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void onOccInspectionCauseNukeCommitButtonChange() throws AuthorizationException, BObStatusException {
        OccInspectionCoordinator coordinator = getOccInspectionCoordinator();
        try {
            coordinator.deactivateOccInspectionCause(currentOccInspectionCause, getSessionBean().getSessUser().getKeyCard().getGoverningAuthPeriod());
            refreshOccInspectionCauseList();
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "OccInspection Cause deactivated with ID: " + currentOccInspectionCause.getCauseID(), ""));
            if (Objects.nonNull(causeList) && !causeList.isEmpty()) {
                currentOccInspectionCause = causeList.get(0);
            } else {
                currentOccInspectionCause = null;
            }
        } catch (IntegrationException | BObStatusException | AuthorizationException ex) {
            System.out.println(ex);
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    ex.getMessage(), ""));
        }
    }

    public List<OccInspectionCause> getCauseList() {
        return causeList;
    }

    public void setCauseList(List<OccInspectionCause> causeList) {
        this.causeList = causeList;
    }

    public OccInspectionCause getCurrentOccInspectionCause() {
        return currentOccInspectionCause;
    }

    public void setCurrentOccInspectionCause(OccInspectionCause currentOccInspectionCause) {
        this.currentOccInspectionCause = currentOccInspectionCause;
    }

    public boolean isEditModeOccInspectionCauseInfo() {
        return editModeOccInspectionCauseInfo;
    }

    public void setEditModeOccInspectionCauseInfo(boolean editModeOccInspectionCauseInfo) {
        this.editModeOccInspectionCauseInfo = editModeOccInspectionCauseInfo;
    }

    public boolean isOccInspectionCauseCreateMode() {
        return occInspectionCauseCreateMode;
    }

    public void setOccInspectionCauseCreateMode(boolean occInspectionCauseCreateMode) {
        this.occInspectionCauseCreateMode = occInspectionCauseCreateMode;
    }

    public boolean isIncludeInactiveOccInspectionCauses() {
        return includeInactiveOccInspectionCauses;
    }

    public void setIncludeInactiveOccInspectionCauses(boolean includeInactiveOccInspectionCauses) {
        this.includeInactiveOccInspectionCauses = includeInactiveOccInspectionCauses;
    }

    /**
     * @return the showAllMunis
     */
    public boolean isShowAllMunis() {
        return showAllMunis;
    }

    /**
     * @param showAllMunis the showAllMunis to set
     */
    public void setShowAllMunis(boolean showAllMunis) {
        this.showAllMunis = showAllMunis;
    }

}