/*
 * Copyright (C) 2017 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for mo re details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.coordinators;

import com.tcvcog.tcvce.entities.reports.ReportConfigCECaseList;
import com.tcvcog.tcvce.entities.reports.ReportConfigCECase;
import com.tcvcog.tcvce.entities.reports.ReportCEARList;
import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.LegendItem;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheClient;
import com.tcvcog.tcvce.domain.*;
import com.tcvcog.tcvce.entities.*;
import com.tcvcog.tcvce.entities.occupancy.FieldInspection;
import com.tcvcog.tcvce.entities.occupancy.FieldInspectionLight;
import com.tcvcog.tcvce.entities.occupancy.OccInspectedSpaceElement;
import com.tcvcog.tcvce.entities.reports.ReportCECaseListCatEnum;
import com.tcvcog.tcvce.entities.reports.ReportCECaseListStreetCECaseContainer;
import com.tcvcog.tcvce.entities.search.*;
import com.tcvcog.tcvce.integration.*;
import com.tcvcog.tcvce.util.Constants;
import com.tcvcog.tcvce.util.DateTimeUtil;
import com.tcvcog.tcvce.util.MessageBuilderParams;
import com.tcvcog.tcvce.util.StringUtil;
import com.tcvcog.tcvce.util.viewoptions.ViewOptionsActiveHiddenListsEnum;
import com.tcvcog.tcvce.util.viewoptions.ViewOptionsActiveListsEnum;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.faces.application.FacesMessage;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.pie.PieChartDataSet;
import org.primefaces.model.charts.pie.PieChartModel;

/**
 * The voluminous container for all logic blocs related to CE case object family members
 * @author ellen bascomb of apt 31y
 */
@Named("caseCoordinator")
@ApplicationScoped
public class CaseCoordinator 
        extends BackingBeanUtils 
        implements Serializable, IFaceCacheClient {

    /**
     * Where cases start
     */
    final CasePhaseEnum initialCECasePphase = CasePhaseEnum.PrelimInvestigationPending;

    final static int GRIDSQAURES_COLS_1 = 12;
    final static int GRIDSQAURES_COLS_2 = 6;
    final static int GRIDSQAURES_COLS_3 = 4;
    final static int GRIDSQAURES_COLS_4 = 3;
    
    final static int ZERO = 0;
    /**
     * 
     * Unspecified days to comply in an ordinance will mean draw from this
     * member TODO: make a municipality setting on a muniprofile
     */
    final int FALLBACK_DAYSTOCOMPLY = 30;
    final int PLACEHOLDER_TEMPLATEID = 9;
    
    final int VIOLATION_DEAC_ALLOW_BUFFER_DAYS = 1;
    
    /**
     * "Give them a bit more time"
     */
    final static int DEFAULT_EXTENSIONDAYS = 14;
    final static String ADDRESSLESS_PROPERTY_STREET_NAME = "Not attached to a street";

    final static int ARBITRARILY_LONG_TIME_YEARS = 113; // arbitrary magic number of years into the future (thanks WWALK)
    
    // TODO: Permissions; changeme to check muni profile for permissions flag
    final static RoleType MIN_RANK_TO_ISSUE_CITATION = RoleType.MuniStaff;

    @Inject
    private CECaseCacheManager ceCaseCacheManager;
    
    /**
     * Creates a new instance of CaseCoordinator
     */
    public CaseCoordinator() {

    }

    @PostConstruct
    public void initBean() {
        
        registerClientWithManager();
    }
    
    // *************************************************************************
    // *****                 CECASE CACHING                         *********
    // *************************************************************************
       
    /**
     * As of July 2024 we are caching CECases and subobjects
     */
    @Override
    public void registerClientWithManager() {
        ceCaseCacheManager.registerCacheClient(this);
    }

    
    // *************************************************************************
    // *****                 CECASE CHECKPOINTS                            *****
    // *************************************************************************
    
    
    /**
     * Permissions logic for opening a CE Case:
     * Require property to be in one's authorized muni and muni staff role floor
     * 
     * @param prop
     * @param ua
     * @return 
     */
    public boolean permissionsCheckpointOpenCECase(Parcel prop, UserAuthorized ua){
        if(prop == null || ua == null){
            return false;
        }
        if(ua.getCrossMuniViewAuthorizationTS() != null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        if(!ua.getKeyCard().isHasMuniStaffPermissions()){
            return false;
        }
        if(prop.getMuni().getMuniCode() != ua.getKeyCard().getGoverningAuthPeriod().getMuni().getMuniCode()){
            return false;
        }
        
        return true;
    }
    
    /**
     * Checks for user having code officer permissions to open one-touch case.Also calls permissionsCheckpointOpenCECase internally and adds logic
     * @param prop
     * @param ua
     * @return 
     */
    public boolean permissionsCheckpointOpenOneClickCase(Parcel prop, UserAuthorized ua){
        // build on the general case opening logic
        if(!permissionsCheckpointOpenCECase(prop, ua)){
            return false;
        }
        // if we aren't an enforcement official, then no soup
        if(!ua.getKeyCard().isHasEnfOfficialPermissions()){
            return false;
        }
        return true;
    }
    
    
    /**
     * Permissions checkpoint for determining user rights to close a cecase
     * @param ua cannot be null
     * @return if user can close a ce case
     */
    public boolean permissionsCheckpointCloseReopenCECase(UserAuthorized ua){
        if(ua == null){
            return false;
        }
        if(ua.getCrossMuniViewAuthorizationTS() != null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        if(!ua.getKeyCard().isHasMuniStaffPermissions()){
            return false;
        }
        if(ua.getGoverningMuniProfile().isRequireCodeOfficerTrueCloseCECase() && !ua.getKeyCard().isHasEnfOfficialPermissions()){
            return false;
        }
        if(ua.getGoverningMuniProfile().isManagerRequiredcCloseCECase() && !ua.getKeyCard().isHasMuniManagerPermissions()){
            return false;
        }
        
        return true;
    }
    
     
    /**
     * Presumed denial logic: most restrictive
     * Permissions checkpoint for determining user rights to deac a case:
     * which is allowed for system admins only
     * 
     * Yes, even manger users must email support to deac a case. Instead, they can
     * close a case with an appropriately descriptive event, such as "made in error"
     * 
     * 
     * @param cse cannot be null
     * @param ua cannot be null
     * @return if the user can deac a case
     */
    public boolean permissionCheckpointDeactivateCECase(CECase cse, UserAuthorized ua){
        if(ua == null ){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
      
        
        
        return false;
        
    }
    
    
   
    
    
    /**
     * Permissions checkpoint for business rule: add violation to cecase
     * @param ua
     * @return 
     */
    public boolean permissionsCheckpointViolationAdd(UserAuthorized ua){
        if(ua == null){
            return false;
        }
        if(ua.getCrossMuniViewAuthorizationTS() != null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        if(!ua.getKeyCard().isHasMuniStaffPermissions()){
            return false;
        }
        if(ua.getGoverningMuniProfile().isRequireCodeOfficerTrueAttachViolToCECase() && !ua.getKeyCard().isHasEnfOfficialPermissions()){
            return false;
        }
        if(!ua.getKeyCard().isHasMuniStaffPermissions()){
            return false;
        }
        return true;
    }
    
    
    /**
     * Permissions checkpoint for business rule: extend code violation stipulated compliance date
     * @param ua
     * @return 
     */
    public boolean permissionsCheckpointViolationExtendStipCompDate(UserAuthorized ua){
        if(ua == null){
            return false;
        }
        if(ua.getCrossMuniViewAuthorizationTS() != null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        if(!ua.getKeyCard().isHasMuniStaffPermissions()){
            return false;
        }
        if(ua.getGoverningMuniProfile().isManagerRequiredViolationExtStipComp() && !ua.getKeyCard().isHasMuniManagerPermissions()){
            return false;
        }
        return true;
    }

    
    /**
     * Permissions logic for determining ability to create an NOV and undertake any
     * operations on those letters.
     * 
     * Simple role floor: muni staff
     * 
     * No muni-customizable switches in here.
     * @param ua
     * @return 
     */
    public boolean permissionsCheckpointNOVCreateEdit(UserAuthorized ua){
        if(ua == null){
            return false;
        }
        if(ua.getCrossMuniViewAuthorizationTS() != null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        if(!ua.getKeyCard().isHasMuniStaffPermissions()){
            return false;
        }
        
        return true;
    }
    
    /**
     * Permissions logic for determining user rights to finalize an NOV
     * @param ua cannot be null
     * @return if NOV finalize operation is allowed
     */
     public boolean permissionsCheckpointNOVFinalize(UserAuthorized ua){
         if(ua == null){
            return false;
        }
         if(ua.getCrossMuniViewAuthorizationTS() != null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        if(!ua.getKeyCard().isHasMuniStaffPermissions()){
            return false;
        }
         if(ua.getGoverningMuniProfile().isRequireCodeOfficerTrueFinalizeNOV() && !ua.getKeyCard().isHasEnfOfficialPermissions()){
            return false;
        }
        if(ua.getGoverningMuniProfile().isManagerRequiredNOVFinalize() && !ua.getKeyCard().isHasMuniManagerPermissions()){
            return false;
        }
        return true;
    }
     
     
   
       /**
     * Permissions logic: citation open/close
     * @param ua cannot be null
     * @return if user possesses permission to open/close a citation 
     */
    public boolean permissionsCheckpointCitationOpenClose(UserAuthorized ua){
        if(ua == null){
            return false;
        }
        if(ua.getCrossMuniViewAuthorizationTS() != null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        if(!ua.getKeyCard().isHasMuniStaffPermissions()){
            return false;
        }
        // check if this profile requires code officer status for update and check if our user is a CEO
        if(ua.getGoverningMuniProfile().isRequireCodeOfficerTrueOpenCitation() && !ua.getKeyCard().isHasEnfOfficialPermissions()){
            return false;
        }
        if(ua.getGoverningMuniProfile().isManagerRequiredCitationOpenClose() && !ua.getKeyCard().isHasMuniManagerPermissions()){
            return false;
        }
        return true;
    }
    
    
    
    /**
     * Permissions logic: citation update
     * @param ua cannot be null
     * @return if user possesses permission to edit a citation 
     */
    public boolean permissionsCheckpointCitationUpdate(UserAuthorized ua){
        if(ua == null){
            return false;
        }
        if(ua.getCrossMuniViewAuthorizationTS() != null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        if(!ua.getKeyCard().isHasMuniStaffPermissions()){
            return false;
        }
        if(ua.getGoverningMuniProfile().isRequireCodeOfficerTrueUpdateCitation() && !ua.getKeyCard().isHasEnfOfficialPermissions()){
            return false;
        }
        return true;
    }
    
    /**
     * Permission gate for insert and updates on one-touch templates:
     * System admins have full permissions, users can edit template assigned to their muni
     * @param ua
     * @param template
     * @return 
     */
      public boolean permissionsCheckpointCECaseTemplateAddEdit(UserAuthorized ua, CECaseOneClickTemplate template){
        if(ua == null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        // users can edit their own one-touch case templates
        if(template != null 
                && template.getMuni() != null 
                && template.getMuni().getMuniCode() == ua.getKeyCard().getGoverningAuthPeriod().getMuni().getMuniCode()){
            return true;
        }
        return false;
    }
      
      
      /*
      ORPHAN JAVADOC
      
         * INCORRECT JAVA DOC
     * Called at the very end of the CECaseDataHeavy creation process by the
     * CaseIntegrator and simply checks for events that have a required
     * eventcategory attached and places a copy of the event in the Case's
     * member variable.
     *
     * This means that every time we refresh the case, the list is automatically
     * updated. DESIGN NOTE: A competing possible location for this method would
     * be on the CECaseDataHeavy object itself--in its getEventListActionRequest
     * method
     *
      */

    // *************************************************************************
    // *                     CODE ENF CASE GENERAL                             *
    // *************************************************************************
      
      
      /**
       * Mighty cache-backed factory for CECaseDataHeavy objects!
       * @param c
       * @param ua
       * @return
       * @throws BObStatusException
       * @throws IntegrationException
       * @throws SearchException 
       */
    public CECaseDataHeavy cecase_assembleCECaseDataHeavy(CECase c, UserAuthorized ua)
            throws BObStatusException,
                    IntegrationException,
                    SearchException {
        if(c == null || ua == null || c.getCaseID() == 0){
            throw new BObStatusException("Cannot assemble data heavy case with null case or user or caseid = 0");
        }
        
        return cecase_configureCECaseDataHeavy(c, ua);
    }
      
    /**
     * Forces a cache dump of given case
     * @param cse 
     */
    public void cecase_forceCaseCacheDump(CECase cse){
        ceCaseCacheManager.flush(cse);
    }
    
      
    /**
     * Cache-backed assmebly method
     * @param c MUST BE A CURRENT VERSION OF A CASE!! Designed to be called with cecase_getCECase(int) on this class!!!
     * @param ua
     * @return the CECaseDataHeavy with the action request list ready to roll
     */
    public CECaseDataHeavy cecase_configureCECaseDataHeavy(CECase c, UserAuthorized ua)
            throws DatabaseFetchRuntimeException {

        Credential cred = null;
        if (ua != null && c != null && c.getCaseID() != 0) {
            cred = ua.getKeyCard();

        } else {
            throw new DatabaseFetchRuntimeException("CaseCoordinator.cecase_assembleCECaseDataHeavy | Cannot construct cecaseDH with null case input or case ID = 0");
        }
        PropertyCoordinator pc = getPropertyCoordinator();
        BlobCoordinator bc = getBlobCoordinator();
        PersonCoordinator persc = getPersonCoordinator();
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        SearchCoordinator sc = getSearchCoordinator();

        // Wrap our base class in the subclass wrapper--an odd design structure, indeed
        CECaseDataHeavy cse = new CECaseDataHeavy(c);

        try {

            // these are flattened now on the base class. If you need these
            // objects, ask the coordinator for them
//            cse.setProperty(pc.getProperty(c.getParcelKey()));
//            cse.setPropUnit(pc.getPropertyUnit(c.getPropertyUnitID()));

            // PROPOSAL LIST
//            cse.setProposalList(wc.getProposalList(cse, cred));

            // EVENT RULE LIST
//            cse.setEventRuleList(wc.rules_getEventRuleImpList(cse, cred));

            // Human list
            cse.sethumanLinkList(persc.getHumanLinkList(cse));

            // Inspection list
            cse.setInspectionList(oic.getOccInspectionLightList(cse));

            // CEAR LIST
            QueryCEAR qcear = sc.initQuery(QueryCEAREnum.ATTACHED_TO_CECASE, cred);
            qcear.getPrimaryParams().setCecase_ctl(true);
            qcear.getPrimaryParams().setCecase_val(c);

            cse.setCeActionRequestList(sc.runQuery(qcear).getBOBResultList());

            // BLOB
            cse.setBlobList(bc.getBlobLightList(cse));

        } catch (BlobException | BObStatusException | IntegrationException | SearchException  ex) {
            System.out.println(ex);
            throw new DatabaseFetchRuntimeException("Erro building case data heavy");
        }

        // Skip payment stuff for now--i totally broke this stuff
//        try {
//            cse.setFeeList(pi.getFeeAssigned(c));
//
//            cse.setPaymentListGeneral(pi.getPaymentList(c));
//        } catch (IntegrationException ex) {
//            System.out.println(ex);
//        }
        return cse;
    }

    /**
     * Utility method for calling cecase_assembleCECaseDataHeavy() for each base
     * CECase in the given list
     *
     * @param cseList
     * @param ua
     * @return
     */
    public List<CECaseDataHeavy> cecase_assembleCECaseDataHeavyList(List<CECase> cseList, UserAuthorized ua) {
        List<CECaseDataHeavy> cseDHList = new ArrayList<>();
        if (cseList != null && !cseList.isEmpty()) {
            for (CECase cse : cseList) {
                try {
                    cseDHList.add(cecase_assembleCECaseDataHeavy(cecase_getCECase(cse.getCaseID(), ua), ua));
                } catch (BObStatusException | IntegrationException | SearchException ex) {
                    System.out.println("CaseCoordinator.assembleCECaseDataHeavy" + ex.toString());
                }

            }
        }
        return cseDHList;

    }
    /**
     * Utility method for calling cecase_assembleCECaseDataHeavy() for each base
     * CECase in the given list
     *
     * @param cseList
     * @return
     */
    public List<CECase> cecase_upcastCECaseDataHeavyList(List<CECaseDataHeavy> cseList) {
        List<CECase> cseDHList = new ArrayList<>();
        if (cseList != null && !cseList.isEmpty()) {
            for (CECaseDataHeavy csedh : cseList) {
                cseDHList.add((CECase) csedh);
            }
        }
        return cseDHList;

    }

   
  

    /**
     * *** PERMISSIONS CHECKPOINT ***
     * Manages the closing of a case prior to full violation compliance.Will
     * nullify any existing violations on the CECase and attach a close case
     * event based on the inputted value
     *
     * @param cse the case to force close
     * @param closeEventCat the EventCategory associated with the closure reason
     * @param ua the user doing the force closing
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.EventException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void cecase_forceclose(CECaseDataHeavy cse, EventCategory closeEventCat, UserAuthorized ua) throws BObStatusException, IntegrationException, EventException, AuthorizationException {
        CaseIntegrator ci = getCaseIntegrator();
        EventCoordinator ec = getEventCoordinator();
        if (cse == null) {
            throw new BObStatusException("Cannot close a null case");
        }
        
         if(!permissionsCheckpointCloseReopenCECase(ua)){
            throw new AuthorizationException("Authorization exception: Muni profile " + ua.getGoverningMuniProfile().getProfileID() 
                    + " requires manager rank to close a CE Case and user " + ua.getUserHuman() + " is rank " + ua.getKeyCard().getGoverningAuthPeriod().getRole().getRank());
        }
        
//        if(cse.getClosingDate() != null){
//            throw new BObStatusException("Cannot force close an already closed case");
//        }
        if (closeEventCat == null) {
            throw new BObStatusException("Cannot close case with null closing event");
        }

        if (closeEventCat.getEventType() != EventType.Closing) {
            throw new BObStatusException("Cannot close case with an event that's not of type: Closing");
        }

        // Nullify violations on case
        if (cse.getViolationList() != null && !cse.getViolationList().isEmpty()) {
            for (CodeViolation cv : cse.getViolationList()) {
                if (cv.getActualComplianceDate() == null) {
                    violation_NullifyCodeViolation(cv, LocalDateTime.now(), "Auto nullification during case closure",ua);
                }
            }
        }

//        cse.setClosingDate(LocalDateTime.now());
        ci.updateCECaseMetadata(cse);
        events_processClosingEvent(cse, ec.initEvent(cse, closeEventCat), ua);
        ceCaseCacheManager.flush(cse);

    }
    
    /**
     * Cache-backed getter for plain CECases
     * @param caseID
     * @param ua
     * @return
     * @throws IntegrationException I know I know it should be a bob status exception!
     */
    public CECase cecase_getCECase(int caseID, UserAuthorized ua)
            throws IntegrationException {
        if( ua == null || caseID == 0){
            throw new IntegrationException("Cannot assemble case with null case or user or caseid = 0");
        }
        
        if(ceCaseCacheManager.isCachingEnabled()){
            return ceCaseCacheManager.getCacheCECase().get(caseID, k -> cecase_configureCECase(caseID, ua));
        } else {
            return cecase_configureCECase(caseID, ua);
        }
        
        
    }

    /**
     * Primary pathway for retrieving the data-light superclass CECase.
     * Implements business logic.
     *
     * @param caseID
     * @param ua
     * @return
     
     */
    public CECase cecase_configureCECase(int caseID, UserAuthorized ua)
            throws DatabaseFetchRuntimeException {
        CaseIntegrator ci = getCaseIntegrator();
        EventCoordinator ec = getEventCoordinator();
        SystemIntegrator si = getSystemIntegrator();

        CECase cse = null;
        try {
            cse = ci.getCECase(caseID);
            if (cse != null) {

                // Inject all cases with lists of their violations, notices, citations, 
                // and events, which will be used to determine case phase
                cse.setNoticeList(nov_getNoticeOfViolationList(ci.novGetList(cse)));
                Collections.sort(cse.getNoticeList());
                Collections.reverse(cse.getNoticeList());

                List<Integer> vidl = ci.getCodeViolations(cse);
                List<CodeViolation> cvs = violation_getCodeViolations(vidl);
                cse.setViolationList(violation_getCodeViolationStatusHeavyList(cvs));
                Collections.sort(cse.getViolationList());
                
                cse.setCitationList(citation_getCitationList(cse));
                cse.setEventList(ec.getEventList(cse));

                // configure origination & closing events
                cse.setOriginationEvent(ec.findMostRecentEventByType(cse.getEventList(), EventType.Origination));
                if(cse.getClosingDate() != null){
                    cse.setClosingEvent(ec.findMostRecentEventByType(cse.getEventList(), EventType.Closing));
                }
                
                // if we change the case based on our audit, get a fresh case before proceeding
                boolean reloadTrigger = cecase_auditCECase(cse, getSessionBean().getSessUser());
                
                if(reloadTrigger){
                    cse = ci.getCECase(caseID);
                }
                cecase_configureMostRecentPastEvent(cse);
                if(cse == null){
                } else {
                    // upgraded Fall 2024 case priority system
                    cecase_configureAdministrativeAlerts(cse, ua);
                    cecase_configureCECaseActionDueDate(cse, ua);
                    // legacy case priority
                    cecase_configureCECasePriority(cse, ua.getGoverningMuniProfile());
                    if (cse.getPriority() != null) {
                        //now set the icon based on what phase we just assigned the case to
                        cse.setPriorityIcon(si.getIcon(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                                .getString(cse.getPriority().getIconPropertyLookup()))));
                    } 
                }
            }
        } catch (BObStatusException | BlobException | IntegrationException ex) {
            System.out.println(ex);
            throw new DatabaseFetchRuntimeException("Error building CECase");
        }
        return cse;
    }
    
    
    /**
     * Big important logic container for checking all sorts of attributes about
     * a CECase and rectifying the situation as possible, creating events and notes 
     * along the way
     * 
     * This method is 
     * 
     * @param cse at any stage, or phase, even before a DB write!
     * @param ua 
     */
    private boolean cecase_auditCECase(CECase cse, UserAuthorized ua) throws BObStatusException{
       
        if(cse == null || ua == null){
           throw new BObStatusException("Cannot audit null case or null user");
       }
        boolean reloadTrigger = false;
        StringBuilder auditLog = new StringBuilder();
        auditLog.append("AUDIT LOG for CECase ID: ");
        auditLog.append(cse.getCaseID());
        auditLog.append(Constants.FMT_HTML_BREAK);
        
        
       if(cse.getCaseManager() == null){
           auditLog.append("*NO case manager found");
           throw new BObStatusException(auditLog.toString());
       }
       
       // TODO: Replace placeholder logic. Origination date can fall back to most distant origination event
        if (cse.getOriginationDate() == null) {
            
        }
        
        if (cse.getParcelKey() == 0) {
            throw new BObStatusException("Cases must have a nonzero property id");
        }
      
       return reloadTrigger;
    }
    
    /**
     * Determines a case's most recent past event for use in priority jazz
     * 
     * @param cse 
     */
    private void cecase_configureMostRecentPastEvent(CECase cse){
        if(cse != null && cse.getEventList() != null && !cse.getEventList().isEmpty()){
            for(EventCnF ev : cse.getEventList()){
                if(ev.getTimeStart() != null){
                    if(ev.getTimeStart().isBefore(LocalDateTime.now())){
                        cse.setMostRecentPastEvent(ev);
                        cse.setDaysSinceLastEvent(DateTimeUtil.getTimePeriodAsDays(ev.getTimeStart(), LocalDateTime.now()));
                        break;
                    }
                }    
            }
        }
    }


    /**
     * Asks the Integrator for an icon based on case phase
     *
     * @param phase
     * @return
     * @throws IntegrationException
     */
    public Icon cecase_getIconByCasePhase(CasePhaseEnum phase) throws IntegrationException {
        CaseIntegrator ci = getCaseIntegrator();
        return ci.getIcon(phase);
    }
    
    /**
     * Reviews case sub-object statuses and attaches administrative TODO items
     * for related to status of said sub-objects.
     * 
     * @param cse
     * @param ua
     * @return 
     */
    private CECase cecase_configureAdministrativeAlerts(CECase cse, UserAuthorized ua) throws IntegrationException{
        SystemCoordinator sc = getSystemCoordinator();
        
        if(cse == null){
            return cse;
        }
        
        if(cse.getNoticeList() != null && cse.getNoticeList().isEmpty()){
            for(NoticeOfViolation nov: cse.getNoticeList()){
                if(nov.getLockedAndqueuedTS() == null){
                    nov.appendAdminFlag(new AdministrativeFlag( AdministrativeFlagTypeEnum.LETTER, 
                                                                "Finalize and lock letter",
                                                                sc.getIcon(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                                                                        .getString(AdministrativeFlagTypeEnum.LETTER.getIconPropertyLookup()))), 
                                                                nov.getNoticeID()));
                } else if(nov.getSentTS() == null){
                    nov.appendAdminFlag(new AdministrativeFlag( AdministrativeFlagTypeEnum.LETTER, 
                                                                "Send Letter",
                                                                sc.getIcon(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                                                                        .getString(AdministrativeFlagTypeEnum.LETTER.getIconPropertyLookup()))), 
                                                                nov.getNoticeID()));
                }
            }
        }
        
        
        
        
       return cse; 
    }
    /**
     * Second generation (v3.1.x+) of case priority which uses action dates written to
     * the DB instead of java only priority logic
     * 
     * @param cse
     * @param ua
     * @return 
     */
    private CECase cecase_configureCECaseActionDueDate(CECase cse, UserAuthorized ua){
        if(cse == null || ua == null){
            return cse;
        }
        
        // check for administrative flags
        // Flag 0: No violations on case
        // Flag 1: No letters
        // Flag 2: Draft letters, not marked as finalized,
        // Flag 3: Finalized letters, not marked as mailed
        // Flag 4: Citation and court date without new logs
        // Flag 5: All violations marked with compliance, nullification, or xfer-- ready for closure
        // Flag 6: FIN without finalization
               
        
        // 
        
        
        // no admin flags, so proceed with standard logic
        
        
        return cse;
    }
    
    
    /**
     * Primary logic block for interrogating a case to assign it a priority Enum 
     * and write a meaningful log
     * @param cse
     * @return 
     */
    private CECase cecase_configureCECasePriority(CECase cse, MuniProfile profile) throws BObStatusException, IntegrationException{
        if(cse == null){
            throw new BObStatusException("CaseCoordinator.cecase_configureCECasePriority: Cannot assign priority to null case");
        }
        
        if(profile == null){
            throw new BObStatusException("CaseCoordinator.cecase_configureCECasePriority: Cannot assign priority with null profile");
        }
        
        PropertyCoordinator pc = getPropertyCoordinator();
        Parcel parcel;
        try {
            parcel = pc.getProperty(cse.getParcelKey());
        } catch (AuthorizationException ex) {
            throw new BObStatusException(ex.getMessage());
        }
        
        // Get all letters that have been sent
        List<NoticeOfViolation> novSentList = cse.assembleNoticeList(ViewOptionsActiveListsEnum.VIEW_ACTIVE);
        // Get all violations that are active
        List<CodeViolationStatusHeavy> codeViolActiveList = cse.assembleViolationList(ViewOptionsActiveListsEnum.VIEW_ACTIVE);
        
       cse.logPriorityAssignmentMessage("Assigning priority to CECase ID: ", false);
       cse.logPriorityAssignmentMessage(String.valueOf(cse.getCaseID()), true);
       
        if (cse.getDeactivatedTS() != null){
            cse.logPriorityAssignmentMessage("Found deactivated case", true);
            cse.setPriority(PriorityEnum.DEACTIVATED);
            
        } else if (cse.getClosingDate() != null){
            cse.logPriorityAssignmentMessage("Case has a closing date;", true);
            cse.logPriorityAssignmentMessage("Assigning priority: CLOSED.", true);
            cse.setPriority(PriorityEnum.CLOSED);
            
        } else if (parcel != null && parcel.getParcelInfo() != null && parcel.getParcelInfo().isAbandoned()){
            cse.logPriorityAssignmentMessage("Case is not closed but is attached to a parcel inside abandonment window;", true);
            cse.logPriorityAssignmentMessage("This is an overriding priority and the logic is ignoring all citation and violation statuses;", true);
            cse.logPriorityAssignmentMessage("Assigning priority: ABANDONED STALL.", true);
            cse.setPriority(PriorityEnum.ABANDONMENT_STALL);
            
        } else if (!cecase_buildCitationListForPhaseAssignment(cse).isEmpty()) {
            cse.logPriorityAssignmentMessage("Found at least one citation;", true);
            cse.logPriorityAssignmentMessage("Assigning priority: CITATION.", true);
            cse.setPriority(PriorityEnum.CITATION);
            
        } else if (cse.getViolationList() == null || cse.getViolationList().isEmpty()) {
            cse.logPriorityAssignmentMessage("Found zero violations but I'm not a container case;", true);
            cse.logPriorityAssignmentMessage("Assigning priority: OPENING.", true);
            cse.setPriority(PriorityEnum.OPENING);
            // we have at least one violation
            // Let's figure out if we've been sitting open beyond allotted buffer
        } else if (codeViolActiveList != null && !codeViolActiveList.isEmpty()) {
            cse.logPriorityAssignmentMessage("Found ", false);
            cse.logPriorityAssignmentMessage(String.valueOf(codeViolActiveList.size()), false);
            cse.logPriorityAssignmentMessage(" active violations on case;", true);
            // see if we have at least one letter
            if(novSentList != null && !novSentList.isEmpty()){
                cse.logPriorityAssignmentMessage("Found ", false);
                cse.logPriorityAssignmentMessage(String.valueOf(novSentList.size()), false);
                cse.logPriorityAssignmentMessage(" sent letters on case;", true);
                
                NoticeOfViolation mostRecentLetter = novSentList.get(0);
                // bug fix for letters without type
                if(mostRecentLetter != null && mostRecentLetter.getTemplate() != null && mostRecentLetter.getTemplate().getTemplateType() != null){

                    cse.logPriorityAssignmentMessage("Using letter follow up window of : ", false);
                    cse.logPriorityAssignmentMessage(String.valueOf(mostRecentLetter.getTemplate().getTemplateType().getFollowUpWindowDays()), false);
                    cse.logPriorityAssignmentMessage(" days for letters of type: ", false);
                    cse.logPriorityAssignmentMessage(mostRecentLetter.getTemplate().getTemplateType().getTitle(), false);
                    cse.logPriorityAssignmentMessage(";", true);

                    long daysSinceLatestLetter = DateTimeUtil.getTimePeriodAsDays(mostRecentLetter.getDateOfRecord(), LocalDateTime.now());

                    cse.logPriorityAssignmentMessage("Most recent letter was marked as sent ", false);
                    cse.logPriorityAssignmentMessage(String.valueOf(daysSinceLatestLetter), false);
                    cse.logPriorityAssignmentMessage(" days ago on ", false);
                    cse.logPriorityAssignmentMessage(getPrettyDateNoTime(mostRecentLetter.getDateOfRecord()), false);
                    cse.logPriorityAssignmentMessage(" (Letter ID: ", false);
                    cse.logPriorityAssignmentMessage(String.valueOf(mostRecentLetter.getNoticeID()), false);
                    cse.logPriorityAssignmentMessage(")", true);


                    if(daysSinceLatestLetter < 0){
                        cse.logPriorityAssignmentMessage("Most recent letter was sent in the future? Hark!", true);
                        cse.setPriority(PriorityEnum.UNKNOWN);
    //                    return cse;
                    } else if(daysSinceLatestLetter <= mostRecentLetter.getTemplate().getTemplateType().getFollowUpWindowDays()) {
                        cse.logPriorityAssignmentMessage("Case is inside letter type appeals/waiting window;", true);
                        cse.logPriorityAssignmentMessage("Assigning priority: MONITORING", true);
                        cse.setPriority(PriorityEnum.MONITORING);
    //                    return cse;
                        // if we're within administrative buffer, then we're yellow
                    } else if(daysSinceLatestLetter > mostRecentLetter.getTemplate().getTemplateType().getFollowUpWindowDays() 
                            && daysSinceLatestLetter <= DateTimeUtil.getTimePeriodAsDays(mostRecentLetter.getDateOfRecord(), mostRecentLetter.getDateOfRecord().plusDays(profile.getPriorityParamDeadlineAdministrativeBufferDays())) ){
                        cse.logPriorityAssignmentMessage("Letter appeals/waiting period has expired, but inside administrative buffer;", true);
                        cse.logPriorityAssignmentMessage("Assigning priority: ACTION REQUIRED", true);
                        cse.setPriority(PriorityEnum.ACTION_REQUIRED);
    //                    return cse;

                    } else {
                        cse.logPriorityAssignmentMessage("Letter appeals/waiting period has expired, and you're OUTSIDE administrative buffer;", true);
                        cse.logPriorityAssignmentMessage("Assigning priority: ACTION PAST DUE", true);
                        cse.setPriority(PriorityEnum.ACTION_PASTDUE);
    //                    return cse;
                    }
                } else {
                    cse.logPriorityAssignmentMessage("Object status error (Fatal): Most recent letter is null or type is null;", false);
                    cse.setPriority(PriorityEnum.UNKNOWN);
                    
                }// close we have a most recent NOV and not null type

            } else {
                // we have No letters sent, so we need to know are we yellow or red?
                cse.logPriorityAssignmentMessage("Found zero sent letters;", true);
                CodeViolationStatusHeavy earliestV = codeViolActiveList.get(codeViolActiveList.size() - 1);
                
                long daysSinceEarliestActiveViolation = DateTimeUtil.getTimePeriodAsDays(earliestV.getDateOfRecord(), LocalDateTime.now());
                
                cse.logPriorityAssignmentMessage("Using violation-letter buffer: ", false);
                cse.logPriorityAssignmentMessage(String.valueOf(profile.getPriorityParamLetterSendBufferDays()), false);
                cse.logPriorityAssignmentMessage(" days; ", true);
                
                cse.logPriorityAssignmentMessage("Earliest code violation's date of record is ", false);
                cse.logPriorityAssignmentMessage(String.valueOf(daysSinceEarliestActiveViolation), false);
                cse.logPriorityAssignmentMessage(" days ago (Violaion ID: ", false);
                cse.logPriorityAssignmentMessage(String.valueOf(earliestV.getViolationID()), false);
                cse.logPriorityAssignmentMessage(", DOR: ", true);
                cse.logPriorityAssignmentMessage(getPrettyDate(earliestV.getDateOfRecord()), false);
                cse.logPriorityAssignmentMessage(");", true);
                
                if(daysSinceEarliestActiveViolation <= profile.getPriorityParamLetterSendBufferDays()){
                    cse.logPriorityAssignmentMessage("Assigning  priority: ACTION REQUIRED (Yellow).", true);
                    cse.setPriority(PriorityEnum.ACTION_REQUIRED);
                } else {
                    cse.logPriorityAssignmentMessage("Assigning  priority: PAST DUE ACTION (Red).", true);
                    cse.setPriority(PriorityEnum.ACTION_PASTDUE);
                }
//                return cse;
            } // close if/then based on NOV list size
            
            // Check stip comp dates and see if we need to adjust priority as assigned by letters
            LocalDateTime earliestStipCompDate = violation_determineEarliestStipCompDate(codeViolActiveList);
            if(earliestStipCompDate != null){
                long daysToEarliestStipComp = DateTimeUtil.getTimePeriodAsDays(LocalDateTime.now(), earliestStipCompDate);
                
                cse.logPriorityAssignmentMessage("Determined earliest stipulated compliance date is ", false);
                cse.logPriorityAssignmentMessage(getPrettyDate(earliestStipCompDate), false);
                
                cse.logPriorityAssignmentMessage(", which is ", false);
                cse.logPriorityAssignmentMessage(String.valueOf(daysToEarliestStipComp), false);
                if(daysToEarliestStipComp >= 0){
                    cse.logPriorityAssignmentMessage(" days away;", true);
                    cse.logPriorityAssignmentMessage("Hence: we are inside a stip comp window;", true);
                    cse.logPriorityAssignmentMessage("Governing object in this muni is set to ", false);
                    if(profile.isPrioritizeLetterFollowUpBuffer()){
                        cse.logPriorityAssignmentMessage("Letters/NOVs;", true);
                        cse.logPriorityAssignmentMessage("Priority assigned by Letters/NOVs will not be altered", true);
                    } else {
                        cse.logPriorityAssignmentMessage("Stipulated compliance dates;", true);
                        cse.logPriorityAssignmentMessage("Letter/NOV-based priority assignment will be overriden;", true);
                        cse.logPriorityAssignmentMessage("Assigning override priority: MONITORING;", true);
                        cse.setPriority(PriorityEnum.MONITORING);
                        return cse;
                    }
                    
                  // okay, our stip comp date is in the past, so we need to check governing object and then check for buffers  
                  // we are either yellow or red, depending on the administrative buffer
                } else {
                    PriorityEnum priorityStipComp;
                    cse.logPriorityAssignmentMessage(" days ago;", true);
                    long stipCompPositivePast = -1l * daysToEarliestStipComp;
                    if(DateTimeUtil.getTimePeriodAsDays(earliestStipCompDate, LocalDateTime.now()) <= profile.getPriorityParamDeadlineAdministrativeBufferDays()){
                        cse.logPriorityAssignmentMessage("We are INSIDE administrative buffer following the most recent violation stipulated compliance date;", true);
                        cse.logPriorityAssignmentMessage("Priority as governed by stipulated compliance dates is ACTION REQUIRED/Yellow;", true);
                        priorityStipComp = PriorityEnum.ACTION_REQUIRED;
                        if(cse.getPriority() != null && cse.getPriority() != priorityStipComp){
                            cse.logPriorityAssignmentMessage("Priority conflict between letter/NOV rules and stipulated compliance date rules!;", true);
                            if(profile.isPrioritizeLetterFollowUpBuffer()){
                                cse.logPriorityAssignmentMessage("Priority assigned by letter/NOV rules will NOT be altered;", true);
                            } else {
                                cse.logPriorityAssignmentMessage("Priority assigned by letter/NOV rules will be overriden by stipulated compliance date;", true);
                                cse.logPriorityAssignmentMessage("Assignign Priority ACTION REQUIRED/Yellow;", true);
                                cse.setPriority(priorityStipComp);
                            }
                        } else {
                            cse.logPriorityAssignmentMessage("Priority agreement between letters/NOVs and stip comp date;", true);
                        }
                        
                    } else {
                        cse.logPriorityAssignmentMessage("We are OUTSIDE administrative buffer following the most recent violation stipulated compliance date;", true);
                        cse.logPriorityAssignmentMessage("Priority as governed by stipulated compliance dates is ACTION PAST DUE/Red;", true);
                        priorityStipComp = PriorityEnum.ACTION_PASTDUE;
                        if(cse.getPriority() != null && cse.getPriority() != priorityStipComp){
                            cse.logPriorityAssignmentMessage("Priority conflict between letter/NOV rules and stipulated compliance date rules!;", true);
                            if(profile.isPrioritizeLetterFollowUpBuffer()){
                                cse.logPriorityAssignmentMessage("Priority assigned by letter/NOV rules will NOT be altered;", true);
                            } else {
                                cse.logPriorityAssignmentMessage("Priority assigned by letter/NOV rules will be overriden by stipulated compliance date;", true);
                                cse.logPriorityAssignmentMessage("Assignign Priority ACTION PAST DUE/RED;", true);
                                cse.setPriority(priorityStipComp);
                            }
                        } else {
                            cse.logPriorityAssignmentMessage("Priority agreement between letters/NOVs and stip comp date;", true);
                        }
                    }
                }
            } else {
                cse.logPriorityAssignmentMessage("Violation status anomaly: Violation list is not null but no stip comp date could be determined;", true);
            }
        // we have no violations 
        } else {
            cse.logPriorityAssignmentMessage("Cannot determine priority;", true);
            cse.logPriorityAssignmentMessage("Assigning priority: UNKNOWN", true);
            cse.setPriority(PriorityEnum.UNKNOWN);
        }
        // If we didn't assign anything, then we don't know what to do.
        if(cse.getPriority() == null){
            cse.logPriorityAssignmentMessage("Cannot determine priority;", true);
            cse.logPriorityAssignmentMessage("Assigning priority: UNKNOWN", true);
            cse.setPriority(PriorityEnum.UNKNOWN);
        }
        
        // Now check for red or yellow or anbandoned stall and then see if we have a green buffer 
        // if so, take those steps
        if(profile.isPriorityAllowEventCategoryGreenBuffers()){
            cse.logPriorityAssignmentMessage("Muni profile has allowed use of event category green priority buffers...checking;", true);
            if(cse.getPriority() == PriorityEnum.ABANDONMENT_STALL 
                    || cse.getPriority() == PriorityEnum.ACTION_REQUIRED 
                    || cse.getPriority() == PriorityEnum.ACTION_PASTDUE){
                cse.logPriorityAssignmentMessage("Found assigned priority elegible for event category green buffer override;", true);
                if(cse.getEventList() != null && !cse.getEventList().isEmpty()){
                    for(EventCnF ev: cse.getEventList()){
                        // check for events in the pasts
                        if(ev.getTimeStart() != null && ev.getTimeStart().isBefore(LocalDateTime.now())){
                            if(ev.getCategory()!= null && ev.getCategory().getGreenBufferDays() != 0){
                                if(ev.getCategory().getGreenBufferDays() <= DateTimeUtil.getTimePeriodAsDays(ev.getTimeStart(), LocalDateTime.now())){
                                    cse.logPriorityAssignmentMessage("Found event (ID:", false);
                                    cse.logPriorityAssignmentMessage(String.valueOf(ev.getEventID()), false);
                                    cse.logPriorityAssignmentMessage(") with a green buffer of ", false);
                                    cse.logPriorityAssignmentMessage(String.valueOf(ev.getCategory().getGreenBufferDays()), false);
                                    cse.logPriorityAssignmentMessage("days, which has triggered a priority green override;", true);
                                    cse.logPriorityAssignmentMessage("Assigning Priority: GREEN OVERRIDE BUFFER", true);
                                    cse.setPriority(PriorityEnum.MON_OVERRIDE);
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    cse.logPriorityAssignmentMessage("No events were found to trigger a green override;", true);
                }
            } else {
                cse.logPriorityAssignmentMessage("Case priority is not eligible for green override;", true);
            }
        } else {
            cse.logPriorityAssignmentMessage("Muni profile has disallowed use of event category green priority buffers;", true);
        }
        return cse;
    }
    
    
    
    
    /**
     * A CECaseDataHeavy's Stage is derived from its Phase based on the set of
     * business rules encoded in this method.
     * @Deprecated replaced in July 2022 with CasePriority
     * @param cse which needs its StageConfigured
     * @return the same CECas passed in with the CaseStageEnum configured
     * @throws BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    private CECase cecase_configureCECaseStageAndPhase(CECase cse) throws BObStatusException, IntegrationException {

        if (cse == null || cse.getCaseID() == 0) {
            throw new BObStatusException("cannot configure null case or one with ID 0");

        }

        // First determine case stage, then dig around as needed in the case
        // data to determine the appropriate phase
        // case stage basically emerges from violation status assessment
        SystemIntegrator si = getSystemIntegrator();
        CECaseStatus statusBundle = new CECaseStatus();

        if (cse.getClosingDate() != null) {
            statusBundle.setPhase(CasePhaseEnum.Closed);
            // jump right to court-based phase assignment if we have at least 1 elegible citation
        } else if (!cecase_buildCitationListForPhaseAssignment(cse).isEmpty()) {
            // Use catch all for citation at any stage of the process
            statusBundle.setPhase(CasePhaseEnum.Cited);
            // TODO: fix complex logic on citation phase stuff
//            statusBundle.setPhase(cecase_determineAndSetPhase_stageCITATION(cse));

        } else {
            // find overriding factors to have a closed 
            if (cse.getViolationList() != null && cse.getViolationList().isEmpty()) {
                // Open case, no violations yet: only one mapping
                statusBundle.setPhase(CasePhaseEnum.PrelimInvestigationPending);
                // we have at least one violation attached  
            } else {
                // If we don't have a mailed notice, then we're in Notice Delivery phase
                if (!cecase_determineIfNoticeHasBeenMailed(cse)) {
                    statusBundle.setPhase(CasePhaseEnum.IssueNotice);
                    // notice has been sent so we're in CaseStageEnum.Enforcement or beyond
                } else {
                    int maxVStage = violation_determineMaxViolationStatus(cse.getViolationList());
                    switch (maxVStage) {
                        case 0:  // all violations resolved
                            statusBundle.setPhase(CasePhaseEnum.FinalReview);
                            break;
                        case 1: // all violations within compliance window
                            statusBundle.setPhase(CasePhaseEnum.InsideComplianceWindow);
                            break;
                        case 2: // one or more EXPIRED compliance timeframes but no citations
                            statusBundle.setPhase(CasePhaseEnum.TimeframeExpiredNotCited);
                            break;
                        // we shouldn't hit this...
                        case 3: // at least 1 violation used in a citation that's attached to case
                            statusBundle.setPhase(cecase_determineAndSetPhase_stageCITATION(cse));
                            //                            cecase_determineAndSetPhase_stageCITATION(cse);
                            break;
                        default: // unintentional dumping ground 
                            statusBundle.setPhase(CasePhaseEnum.InactiveHolding);
                    } // close violation and citation block 
                } // close sent-notice-dependent block
            } // close violation-present-only block
        } // close non-container, non-closed block

        cse.setStatusBundle(statusBundle);

        if (cse.getStatusBundle() != null && cse.getStatusBundle().getPhase() != null) {
            //now set the icon based on what phase we just assigned the case to
            statusBundle.setPhaseIcon(si.getIcon(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                    .getString(cse.getStatusBundle().getPhase().getCaseStage().getIconPropertyLookup()))));
        }

        return cse;
    }

    /**
     * Utility method for culling out inactive citations, deactivated, etc.
     *
     * @param cse
     * @return
     */
    private List<Citation> cecase_buildCitationListForPhaseAssignment(CECase cse) {
        List<Citation> citList = new ArrayList<>();
        if (cse == null || cse.getCitationList() == null || cse.getCitationList().isEmpty()) {
            return citList;
        } else {
            for (Citation cit : cse.getCitationList()) {
                // TODO: fix me to work with docket status
                citList.add(cit);
            }
        }

        return citList;

    }

    /**
     * Logic container for case phase assignment for cases with at least 1
     * citation
     *
     * Assesses the list of events and citations on the case to determine the
     * appropriate post-hearing related case phase
     *
     * NOTE AS OF 29-NOV-2020: Let's just do: citation status, minus event
     * processing
     *
     * @param cse
     * @return
     */
    private CasePhaseEnum cecase_determineAndSetPhase_stageCITATION(CECase cse) {
        CasePhaseEnum courtPhase = null;

        int catIDCitationIssued = Integer.parseInt(getResourceBundle(Constants.EVENT_CATEGORY_BUNDLE).getString("court_citationissued"));
        int catIDHearingSched = Integer.parseInt(getResourceBundle(Constants.EVENT_CATEGORY_BUNDLE).getString("court_scheduledhearing"));
        int catIDHearingAttended = Integer.parseInt(getResourceBundle(Constants.EVENT_CATEGORY_BUNDLE).getString("court_hearingattended"));

        List<EventCnF> courtEvList = cecase_getEventListSubset(cse.getEventList(), EventType.Court);
        if (courtEvList == null || courtEvList.isEmpty()) {
            courtPhase = CasePhaseEnum.TimeframeExpiredNotCited;
            cse.logStatusNote("COURT: GAP IN EVENT RECORDING! No court events found, but We should have citation issued events; falling back to " + courtPhase.getLabel());
            return courtPhase;
        }

        // if we have a citation but no hearing scheduled, we're AwaitingAHearingDate
        LocalDateTime rightNow = LocalDateTime.now();
        LocalDateTime rightMostScheduledHearingDate = determineRightmostDateByEventCategoryID(courtEvList, catIDHearingSched);
        LocalDateTime rightMostHearingDate = determineRightmostDateByEventCategoryID(courtEvList, catIDHearingAttended);
        if (rightMostScheduledHearingDate == null) {
            courtPhase = CasePhaseEnum.AwaitingHearingDate;
            cse.logStatusNote("COURT: No hearing scheduled; assigned " + courtPhase.getLabel());
        } else {
            // if a hearing is scheduled in the future, we're HEARING PREP
            if (rightMostScheduledHearingDate.isAfter(rightNow)) {
                courtPhase = CasePhaseEnum.HearingPreparation;
                cse.logStatusNote("COURT: Found hearing schduled in the future; assigned " + courtPhase.getLabel());
                // if we've attended court and violations are still in the window, we're at InsideCourt....
            } else if (rightMostHearingDate.isBefore(rightNow)) {

                int maxVStage = violation_determineMaxViolationStatus(cse.getViolationList());
                switch (maxVStage) {
                    case 0:  // all violations resolved
                        courtPhase = CasePhaseEnum.Closed;
                        break;
                    case 1: // all violations within compliance window
                        courtPhase = CasePhaseEnum.InsideCourtOrderedComplianceTimeframe;
                        break;
                    case 2: // one or more EXPIRED compliance timeframes but no citations
                        courtPhase = CasePhaseEnum.CourtOrderedComplainceTimeframeExpired;
                        break;
                    // we shouldn't hit this...
                    default: // unintentional dumping ground 
                        courtPhase = CasePhaseEnum.CourtOrderedComplainceTimeframeExpired;
                } // close violation and citation block 
            }
        }
        return courtPhase;
    }

    /**
     * Utility method for extracting an event subset
     *
     * @param evList
     * @param et
     * @return
     */
    private List<EventCnF> cecase_getEventListSubset(List<EventCnF> evList, EventType et) {
        // get the events we want
        List<EventCnF> selectedEvents;
        selectedEvents = new ArrayList<>();
        if (evList != null && !evList.isEmpty()) {
            for (EventCnF ev : evList) {
                if (ev.getCategory().getEventType() == et) {
                    selectedEvents.add(ev);
                }
            }
        }

        return selectedEvents;
    }

    /**
     * Utility method for searching through a list of events and finding the
     * "latest in time" (i.e. right-most) event of a certain category
     *
     * @param evList complete event list to search
     * @param catID the category ID by which to create a comparison subset
     * @return if no event of this type is found, null is returned
     */
    private LocalDateTime determineRightmostDateByEventCategoryID(List<EventCnF> evList, int catID) {
        LocalDateTime highestDate = null;
        if (evList != null && !evList.isEmpty()) {
            for (EventCnF ev : evList) {
                if (ev.getCategory().getCategoryID() == catID && ev.getTimeStart() != null) {
                    if (highestDate == null) {
                        highestDate = ev.getTimeStart();
                    } else if (highestDate.isBefore(ev.getTimeStart())) {
                        highestDate = ev.getTimeStart();
                    }
                }
            }
        }

        return highestDate;
    }

    /**
     * TODO: Finish my guts
     *
     * @param cse
     * @return
     */
    public boolean cecase_determineIfNoticeHasBeenMailed(CECase cse) {
        return true;
    }

   

    /**
     * Asks the DB for a case history list and converts the list of IDs into
     * actual CECase objects
     *
     * @param ua
     * @return
     * @throws IntegrationException
     * @throws BObStatusException
     */
    public List<CECase> cecase_getCECaseHistory(UserAuthorized ua) throws IntegrationException, BObStatusException {
        CaseIntegrator caseInt = getCaseIntegrator();
        List<CECase> cl = new ArrayList<>();
        if (ua != null) {
            List<Integer> cseidl = caseInt.getCECaseHistoryList(ua.getMyCredential().getGoverningAuthPeriod().getUserID());
            if (!cseidl.isEmpty()) {
                for (Integer i : cseidl) {
                    cl.add(cecase_getCECase(i, ua));
                }
            }
        }
        return cl;
    }
    
    /**
     * *************************************************************************
     * *****************      CASE LIFECYCLE OPERATIONS    *********************
     * *************************************************************************
     */
    
    /**
     * Factory method for creating new CECases
     *
     * @param p
     * @param ua
     * @return
     * @throws com.tcvcog.tcvce.domain.IntegrationException If an error occurs
     * while generating a control code
     */
    public CECase cecase_initCECase(Property p, UserAuthorized ua) throws IntegrationException {
        CECase newCase = new CECase();

        // TURN OFF MANAGER AUTO INSERT
        // APR 2022 to debug manager issues
//        if (ua.getKeyCard().getGoverningAuthPeriod().getOathTS() != null) {
//            newCase.setCaseManager(ua);
//        }
        newCase.setCaseManager(ua);
        newCase.setOriginationEventCategory(ua.getMyCredential().getGoverningAuthPeriod().getMuniProfile().getDefaultCECaseOriginationCategory());
        newCase.setParcelKey(p.getParcelKey());
        newCase.setOriginationDate(LocalDateTime.now());

        return newCase;
    }
    
    
    /**
     * Set us up for our reopening operation
     * @param csedh
     * @param ua
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public OperationConfigurationCECaseReopen cecase_caseReopenInit(CECaseDataHeavy csedh, UserAuthorized ua) throws IntegrationException{
        EventCoordinator ec = getEventCoordinator();
        
        OperationConfigurationCECaseReopen reopenConfig = new OperationConfigurationCECaseReopen(csedh, ua);
        
        reopenConfig.setDocumentingEventCandidateEventCategoryList(ec.getEventCategeryList(EventType.Casereopen));
        reopenConfig.setDateOfRecord(LocalDateTime.now());

        if(permissionsCheckpointCloseReopenCECase(ua)){
            reopenConfig.setAllowedToProceed(true);
        } else {
            reopenConfig.setAllowedToProceed(false);
            reopenConfig.appendToOpLog("Requesting user lacks permission to reopen this case");
        }
        return reopenConfig;
        
    }

     /**
      * Receives finalized requests to reopen a CE case and processes this major operation.
      * @param reopenConfig
      * @return 
      */
    public OperationConfigurationCECaseReopen cecase_CaseReopenCommit(OperationConfigurationCECaseReopen reopenConfig) throws BObStatusException{
    
        EventCoordinator ec = getEventCoordinator();
        CaseIntegrator ci = getCaseIntegrator();
        
        if(reopenConfig == null || reopenConfig.getOperationTarget() == null){
            throw new BObStatusException("Cannot reopen case with null config or case");
        }
        reopenConfig.setOperationAttemptTS(LocalDateTime.now());
        reopenConfig.flushOpLog(); // if user is trying the resubmit, we'll flush old messages
        
        CECaseDataHeavy caseToReopen = reopenConfig.getOperationTarget();
        
        if(!permissionsCheckpointCloseReopenCECase(reopenConfig.getRequestingUser()));
        
        if(caseToReopen.getClosingDate() == null){
            reopenConfig.appendToOpLog(Constants.FMT_SPAN_REDALERT_BEGIN)
                    .append("The given case is not closed and therefore cannot be reopened!")
                    .append(getPrettyDate(caseToReopen.getClosingDate()))
                    .append(Constants.FMT_SPAN_REDALERT_END);
            return reopenConfig;
        }
        
        if(reopenConfig.getDateOfRecord().isBefore(caseToReopen.getClosingDate())){
            reopenConfig.appendToOpLog(Constants.FMT_SPAN_REDALERT_BEGIN)
                    .append("Reopening date must be after current case closing date of ")
                    .append(getPrettyDate(caseToReopen.getClosingDate()))
                    .append(Constants.FMT_SPAN_REDALERT_END);
            return reopenConfig;
        }
        
        // Okay we can proceed with the reopening
        
        StringBuilder reopenEvDescr = new StringBuilder();
        
        reopenEvDescr.append("Case named ").append(caseToReopen.getCaseName()).append(" (ID:").append(caseToReopen.getCaseID()).append(") ");
        reopenEvDescr.append("Reopened by ").append(reopenConfig.getRequestingUser().getUserHuman().getName()).append(". Original case opening date will remain unchanged.");
        reopenEvDescr.append(" Previous closing date was ").append(getPrettyDate(caseToReopen.getClosingDate())).append(".");
        
        if(!isStringEmptyIsh(reopenConfig.getOperationReason())){
            reopenEvDescr.append(Constants.FMT_HTML_BREAK);
            reopenEvDescr.append(" Reopening details: ").append(reopenConfig.getOperationReason());
        }
        
        try {
            // configure our case to reflect open status
            caseToReopen.setClosingDate(null);
            caseToReopen.setLastUpdatedby_UMAP(reopenConfig.getRequestingUser().getKeyCard().getGoverningAuthPeriod());
            ci.updateCECaseMetadata(caseToReopen);
            reopenConfig.appendToOpLog("Case successfully reopened with ID: ").append(caseToReopen.getCaseID());
            
        } catch (IntegrationException ex) {
            System.out.println(ex);
            reopenConfig.appendToOpLog(Constants.FMT_SPAN_REDALERT_BEGIN)
                    .append("Fatal error reopening case. Please report this to your helpful local system administrator.")
                    .append(Constants.FMT_SPAN_REDALERT_END);
            return reopenConfig;
        }
        
        reopenConfig.setOperationSuccessTS(LocalDateTime.now());
        
        try {
            EventCnF reopenEvent = ec.initEvent(caseToReopen, reopenConfig.getDocumentingEventCategorySelected());
            reopenEvent.setDescription(reopenEvDescr.toString());
            reopenEvent.configureEventTimesFromStartTime(LocalDateTime.now(), ZERO);
            ec.addEvent(reopenEvent, caseToReopen, reopenConfig.getRequestingUser());
        } catch (EventException | AuthorizationException | BObStatusException | IntegrationException ex) {
            System.out.println(ex);
            reopenConfig.appendToOpLog(Constants.FMT_SPAN_REDALERT_BEGIN)
                    .append("Case was reopened! But a fatal error occured logging the reopening event. Please report this to your helpful local system administrator and manually add an event documenting this action.")
                    .append(Constants.FMT_SPAN_REDALERT_END);            
        }
        ceCaseCacheManager.flush(caseToReopen);
        System.out.println("CaseCoordinator.cecase_CaseReopenCommit | " + reopenConfig.getOperationLog());
        return reopenConfig;
    }
    
    /**
     * Logic intermediary for determining if a case can be deactivated
     * and doing so
     *
     * @param cse
     * @param ua
     * @throws IntegrationException
     * @throws BObStatusException
     */
    public void cecase_deactivateCase(CECaseDataHeavy cse, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException {
        SystemIntegrator si = getSystemIntegrator();
        
        if(cse == null || ua == null){
            throw new BObStatusException("Cannot deac case with null case, or userauthorized");
        }
        if(cse.getDeactivatedTS() != null){
            throw new BObStatusException("Cannot deactivate an already deactivated case");
        }
        if(permissionCheckpointDeactivateCECase(cse, ua)){
            System.out.println("CaseCoordinator.cecase_deactivateCase | permission granted to deac case ID: " + cse.getCaseID());
            cse.setDeactivatedBy_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
            si.deactivateUMAPTrackedEntity(cse);

            
            // now deac all sorts of internal organs of the case
            
            try{
                // starting with human links
                if(cse.gethumanLinkList() != null && !cse.gethumanLinkList().isEmpty()){
                    PersonCoordinator pc = getPersonCoordinator();
                    pc.deactivateHumanLinks(cse.gethumanLinkList(), ua);
                    System.out.println("CaseCoordinator.cecase_deactivateCase | completed human link deac");
                }
            } catch (AuthorizationException | BObStatusException | IntegrationException ex){
                System.out.println(ex.getMessage());
                System.out.println("CaseCoordinator.cecase_deactivateCase | error deac human links");
            }
            
            //  inspections
            try{
                if(cse.getInspectionList() != null && !cse.getInspectionList().isEmpty()){
                    for(FieldInspectionLight fin: cse.getInspectionList()){
                        OccInspectionCoordinator oic = getOccInspectionCoordinator();
                        oic.deactivateOccInspection(ua, fin, cse);
                        System.out.println("CaseCoordinator.cecase_deactivateCase | field inspection deac ID: " + fin.getInspectionID());
                    }
                }
            } catch (AuthorizationException | BObStatusException | IntegrationException ex){
                System.out.println(ex.getMessage());
                System.out.println("CaseCoordinator.cecase_deactivateCase | error deac inspections");
            }
            // events
            try{
                if(cse.getEventList() != null && !cse.getEventList().isEmpty()){
                    for(EventCnF ev: cse.getEventList()){
                        EventCoordinator ec = getEventCoordinator();
                        ec.deactivateEvent(ev, cse, ua);
                        System.out.println("CaseCoordinator.cecase_deactivateCase | event deac ID: " + ev.getEventID());
                    }
                }
            } catch (AuthorizationException | BObStatusException | IntegrationException ex){
                System.out.println(ex.getMessage());
                System.out.println("CaseCoordinator.cecase_deactivateCase | error deac events");
            }
            // citations
            try{
                if(cse.getCitationList()!= null && !cse.getCitationList().isEmpty()){
                    for(Citation cit: cse.getCitationList()){
                        citation_deactivateCitation(cit, cse, ua);
                        System.out.println("CaseCoordinator.cecase_deactivateCase | cit deac ID: " + cit.getCecaseID());
                    }
                }
            } catch (AuthorizationException | BObStatusException | IntegrationException ex){
                System.out.println(ex.getMessage());
                System.out.println("CaseCoordinator.cecase_deactivateCase | error deac citations");
            }
            // letters
            try{
                if(cse.getNoticeList() != null && !cse.getNoticeList().isEmpty()){
                    for(NoticeOfViolation nov: cse.getNoticeList()){
                        nov_deactivate(nov, cse, ua);
                        System.out.println("CaseCoordinator.cecase_deactivateCase | nov deac ID: " + nov.getNoticeID());
                    }
                }
            } catch (AuthorizationException | BObStatusException ex){
                System.out.println(ex.getMessage());
                System.out.println("CaseCoordinator.cecase_deactivateCase | error deac letters");
            }
            // violations
            try{
                if(cse.getViolationList() != null && !cse.getViolationList().isEmpty()){
                    for(CodeViolation cv: cse.getViolationList()){
                        violation_deactivateCodeViolation(cv, ua);
                        System.out.println("CaseCoordinator.cecase_deactivateCase | violation deac ID: " + cv.getViolationID());
                    }
                }
            } catch (BObStatusException | IntegrationException ex){
                System.out.println(ex.getMessage());
                System.out.println("CaseCoordinator.cecase_deactivateCase | error deac violations");
            }
            ceCaseCacheManager.flush(cse);
        
        } else {
            throw new AuthorizationException("Deactivating user must have system admin rank");
        }
    }

    /**
     * Primary entry point for inserting new code enf cases.Two major pathways
 exist through this method: - creating cases as a result of an action
 request submission - creating cases from some other source than an action
 request Depending on the source, an appropriately note-ified case
 origination event is built and attached to the case that was just
 created.
     *
     * @param inputCase
     * @param pcl
     * @param ua
     * @param cear
     * @return the ID of the freshly inserted case
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws ViolationException
     * @throws com.tcvcog.tcvce.domain.EventException
     * @throws com.tcvcog.tcvce.domain.SearchException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public int cecase_insertNewCECase(CECase inputCase,
            Parcel pcl,
            UserAuthorized ua,
            CEActionRequest cear)
            throws  IntegrationException,
                    BObStatusException,
                    ViolationException,
                    EventException,
                    SearchException,
                    AuthorizationException {

        CaseIntegrator ci = getCaseIntegrator();
        CEActionRequestIntegrator ceari = getcEActionRequestIntegrator();
        EventCoordinator ec = getEventCoordinator();
        EventCnF originationEvent = null;
        UserCoordinator uc = getUserCoordinator();

        if(inputCase == null){
            throw new BObStatusException("Cannot insert new case with null input case");
        }
        EventCategory originationCategory  = inputCase.getOriginationEventCategory();
        
        if(!permissionsCheckpointOpenCECase(pcl, ua)){
            throw new AuthorizationException("Permission Denied: Open new CECase in cecase_insertNewCECase by permissionsCheckpointOpenCECase"); 
        } 
        
        inputCase.setLastUpdatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());

        if (inputCase.getCaseManager() == null) {
            throw new BObStatusException("CeCase Must have a non-null manager");
            
        }

        cecase_auditCECase(inputCase, ua);

        // the integrator returns to us a CECaseDataHeavy with the correct ID after it has
        // been written into the DB
        inputCase.setCreatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
        inputCase.setLastUpdatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
        
        // DO THE INSERT !! 
        int freshID = ci.insertNewCECase(inputCase);
        // get the fresh version
        inputCase = cecase_getCECase(freshID, ua);
        CECaseDataHeavy cedh = cecase_assembleCECaseDataHeavy(inputCase, ua);
        
        // If we were passed in an action request, connect it to the new case we just made
        if (cear != null) {
            System.out.println("CaseCoordinator.cecase_insertNewCECase | Found CEAR for linking");
            cear_connectCEARToCECase(cedh, cear, CEARProcessingRouteEnum.ATTACH_TO_NEW_CASE, ua);
        } 
        
        // setup our origination event
        if (originationCategory == null) {
            // since there's no action request, the assumed method is called "observation"
            originationCategory = ec.initEventCategory(
                    Integer.parseInt(getResourceBundle(
                            Constants.EVENT_CATEGORY_BUNDLE).getString("originiationByObservation")));

        }
        originationEvent = ec.initEvent(cedh, originationCategory);
        if(originationEvent != null){
            originationEvent.setCreatedBy(uc.user_getUser(ua.getUserID()));
            originationEvent.setTimeStart(cedh.getOriginationDate());
            // logic in our event coordinator will set the end time based on the event cat
            ec.addEvent(originationEvent, cedh, ua);
        } else {
            System.out.println("CECaseBB.cecase_insertNewCECase | error creating origination event");
        }

        return freshID;
    }

   
    
    /**
     * Checks for violation status and closes case
     *
     * @param cse
     * @param ua
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void cecase_closeCase(CECase cse, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException {
        CaseIntegrator ci = getCaseIntegrator();
        EventCoordinator ec = getEventCoordinator();
        
        if (cse == null || ua == null) {
            throw new BObStatusException("Cannot close a case with null Case or User");
        }
        // check for profile requiring manager to close case
        if(!permissionsCheckpointCloseReopenCECase(ua)){
            throw new AuthorizationException("Authorization exception: Muni profile " + ua.getGoverningMuniProfile().getProfileID() 
                    + " requires code officer role to close a CE Case and user " + ua.getUserHuman() + " does not have code officer role privileges.");
        }
        cse.setClosingDate(LocalDateTime.now());
        cse.setLastUpdatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());

        ci.updateCECaseMetadata(cse);

        // deac future events linked to citations and NOVs
        if(cse.getNoticeList() != null && !cse.getNoticeList().isEmpty()) {
            for(NoticeOfViolation nov: cse.getNoticeList()){
                ec.deactivateAllEventsLinkedToEventLinkedObject(nov, cse, true, true, ua);
            }
        }
        if(cse.getCitationList() != null && !cse.getCitationList().isEmpty()){
            for(Citation cit: cse.getCitationList()){
                if(cit.getStatusLog() != null && !cit.getStatusLog().isEmpty()){
                    for(CitationStatusLogEntry entry: cit.getStatusLog()){
                        ec.deactivateAllEventsLinkedToEventLinkedObject(entry, cse, true, true, ua);
                        
                    }
                }
            }
        }
        ceCaseCacheManager.flush(cse);
        
    }
    
        /**
     * Utility for getting cecase list from a list of IDs
     * @param idl
     * @return
     * @throws IntegrationException
     */
    public List<CECase> getCaseListFromIDList(List<Integer> idl, UserAuthorized ua) throws IntegrationException{
        List<CECase> clist = new ArrayList<>();
        if(idl != null && !idl.isEmpty()){
            for(Integer i: idl){
                clist.add(cecase_getCECase(i, ua));
            }
        }
        return clist;
        
        
    }
    
  
    /**
     * Implements business logic before updating a CECaseDataHeavy's core data
     * (opening date, closing date, name.). If all is well, pass to integrator.
     *
     * @param c the CECaseDataHeavy to be updated
     * @param ua the user doing the updating
     * @throws BObStatusException
     * @throws IntegrationException
     */
    public void cecase_updateCECaseMetadata(CECaseDataHeavy c, UserAuthorized ua) throws BObStatusException, IntegrationException {
        CaseIntegrator ci = getCaseIntegrator();
        if (ua == null || c == null) {
            throw new BObStatusException("Cannot update case with null case or user");
        }
        // the supplied user is the updator
        c.setLastUpdatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
        
        // Logic check for changes to the case closing date
        if (c.getClosingDate() != null) {
            if (c.getClosingDate().isBefore(c.getOriginationDate())) {
                throw new BObStatusException("You cannot update a case's origination date to be after its closing date");
            }
        }
        ci.updateCECaseMetadata(c);
        ceCaseCacheManager.flush(c);
    }
    
    
    /**
     * Coordinates with the user coordinator to assemble eligible users to manage
     * a ce case
     * @param ua
     * @return all users with active CEO or Manager (not sys admin) UMAPS in the given muni
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public List<User> assembleCECaseManagerCandidates(UserAuthorized ua) throws IntegrationException, AuthorizationException, BObStatusException{
        UserCoordinator uc = getUserCoordinator();
        return uc.user_assembleUserListOfficerRequired(ua.getKeyCard().getGoverningAuthPeriod().getMuni(), false);
        
    }
    
    /**
     * Updates a CECase's manager and logs an appropriate event in the case history documenting
     * the previous and updated manager.Logic check to ensure target manger has officer permissions in the town hosting
 the case whose manager is switching.
     *  
     * 
     * @param cse
     * @param targetManager
     * @param ua
     * @param mp 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.EventException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void cecase_updateCECaseManagerAndLogEvent(CECaseDataHeavy cse, User targetManager, UserAuthorized ua, MuniProfile mp) 
            throws BObStatusException, IntegrationException, EventException, AuthorizationException{
        if(cse == null || targetManager == null || ua == null || mp == null){
            throw new BObStatusException("Cannot update case manger with null case, target, userauth, or muniprofile");
        }
        CaseIntegrator ci = getCaseIntegrator();
        EventCoordinator ec = getEventCoordinator();
        User formerManager = cse.getCaseManager();
        
        cse.setCaseManager(targetManager);
        cse.setLastUpdatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
        
        ci.updateCECaseManager(cse);  
        
        ec.logManagerChange(cse, formerManager, ua);
        ceCaseCacheManager.flush(cse);
    }
    
    /**
     * Changes a CeCase's origination event category 
     * @param cse
     * @param revisedEventCategory 
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.EventException 
     */
    public void cecase_checkForAndUpdateCaseOriginationEventCategory(CECaseDataHeavy cse, EventCategory revisedEventCategory, UserAuthorized ua) 
            throws IntegrationException, EventException, BObStatusException{
        EventCoordinator ec = getEventCoordinator();
        if(revisedEventCategory != null && cse.getOriginationEvent() != null){
            // If we have an update, do the db write
            if(revisedEventCategory.getCategoryID() != cse.getOriginationEvent().getCategory().getCategoryID() ){
                System.out.println("CaseCoordinator.cecase_checkForAndUpdateCaseOriginationEventCategory");
                ec.updateEventCategoryMaintainType(cse.getOriginationEvent(), revisedEventCategory, ua);
                cecase_originationEventCategoryChangeNoteComposeAndWrite(cse, revisedEventCategory, ua);
            }
        }
    }
    
    /**
     * Writes and inserts a note about an origination event category update
     * @param cse
     * @param revisedEventCategory
     * @param ua
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    private void cecase_originationEventCategoryChangeNoteComposeAndWrite(CECaseDataHeavy cse, EventCategory revisedEventCategory, UserAuthorized ua) throws BObStatusException, IntegrationException{
                StringBuilder sb = new StringBuilder();
                sb.append("Case origination event category changed from ");
                sb.append(cse.getOriginationEvent().getCategory().getEventCategoryTitle());
                sb.append("(Category ID:");
                sb.append(cse.getOriginationEvent().getCategory().getCategoryID());
                sb.append(") to  ");
                sb.append(revisedEventCategory.getEventCategoryTitle());
                sb.append("(Category ID:");
                sb.append(revisedEventCategory.getCategoryID());
                sb.append(").");
                MessageBuilderParams mbp = new MessageBuilderParams(cse.getNotes(), "Origination event category change", null, sb.toString(), ua, null);
                cecase_updateCECaseNotes(mbp, cse);
                ceCaseCacheManager.flush(cse);
    }

    /**
     * Updates only the notes field on CECase
     *
     * @param mbp
     * @param c the CECaseDataHeavy to be updated
     * @throws BObStatusException
     * @throws IntegrationException
     */
    public void cecase_updateCECaseNotes(MessageBuilderParams mbp, CECase c) throws BObStatusException, IntegrationException {
        CaseIntegrator ci = getCaseIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        if (c == null || mbp == null) {
            throw new BObStatusException("Cannot append if notes, case, or user are null");
        }

        c.setNotes(sc.appendNoteBlock(mbp));
        c.setLastUpdatedby_UMAP(mbp.getUser().getKeyCard().getGoverningAuthPeriod());
        ci.updateCECaseNotes(c);
        ceCaseCacheManager.flush(c);
    }
    
    
    

    // *************************************************************************
    // *                     XML Status jazz                                   *
    // *************************************************************************
    
    public void cecase_updateCaseStatusXML(CECase cse, UserAuthorized ua) throws BObStatusException, IntegrationException{
        CaseIntegrator ci = getCaseIntegrator();
        
        if(cse == null || ua == null){
            throw new BObStatusException("Cannot update case XML with null case or user");
        }
        
        // some validation junk here?
        
        System.out.println("CaseCoordinator.cecase_updateCaseStatusXML | xml: " + cse.getStatusXML());
        
        ci.updateCECaseStatusXML(cse);
        
            
    }
    
    

    // *************************************************************************
    // *                     ONE CLICK CASE JUNK                               *
    // *************************************************************************
    
    /**
     * Factory for one click templates
     * @return 
     */
    public CECaseOneClickTemplate cecase_getOneClickTemplateSkeleton(Municipality muni){
        CECaseOneClickTemplate temp = new CECaseOneClickTemplate();
        temp.setMuni(muni);
        return temp;
    }
    
    
    /**
     * Logic gate for retrieving CECaseOneClickTemplates for one-click case buttons
     * @param templateID
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BlobException 
     */
    public CECaseOneClickTemplate cecase_getCECaseOneClickTemplate(int templateID) 
            throws BObStatusException, IntegrationException, BlobException{
        if(templateID == 0){
            throw new BObStatusException("Cannot fetch template with ID == 0");
        }
        CaseIntegrator ci = getCaseIntegrator();
        return ci.getCECaseOneClickTempalte(templateID);
        
        
    }
    
    /**
     * Extracts all one-click case templates by muni
     * @param municode, if null, all are returned
     * @param includeDeactivated when true, all objects returned
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.BlobException 
     */
    public List<CECaseOneClickTemplate> cecase_getCECaseOneClickTemplatesByMuni(Municipality municode, boolean includeDeactivated) 
            throws IntegrationException, BObStatusException, BlobException{
        
        CaseIntegrator ci = getCaseIntegrator();
        
        List<Integer> idl = ci.getCECaseOneClickTempalteListByMuni(municode, includeDeactivated);
        List<CECaseOneClickTemplate> templateList = new ArrayList<>();
        
        if(idl != null && !idl.isEmpty()){
            for(Integer id: idl){
                templateList.add(cecase_getCECaseOneClickTemplate(id));
            }
        }
        Collections.sort(templateList);
        return templateList;
    }
    
    /**
     * Checks case one touch templates for null muni, ord, and nov template; 
     * If orig cat or icon are null, default objects are injected;
     * If this method doesn't explode, you're safe to insert and update on the integrator
     * 
     * @param template
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    private void cecase_auditCECaseOneClickTemplate(CECaseOneClickTemplate template) throws BObStatusException, IntegrationException{
        EventCoordinator ec = getEventCoordinator();
        SystemCoordinator sc = getSystemCoordinator();
        if(template == null){
            throw new BObStatusException("cannot audit null template");
        }
        if(template.getMuni() == null){
            throw new BObStatusException("Template requries non-null muni");
        }
        if(template.getViolatedOrdinance() == null){
            throw new BObStatusException("Tempalte requires an ordinance");
        }
        if(template.getNovTemplate() == null){
            throw new BObStatusException("Template requires an NOV template");
        }
        // set generic orig category
        if(template.getOriginationEventCategory() == null){
            template.setOriginationEventCategory(ec.getEventCategory(Integer.parseInt(getResourceBundle(Constants.EVENT_CATEGORY_BUNDLE).getString("cecase_genericoriginationeventcatid"))));
            System.out.println("CaseCoordinator.auditCECaseOneClickTemplate | setting generic origination event");
        }
        if(template.getIcon() == null){
            template.setIcon(sc.getIcon(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("caseStageUnknownIconID"))));
        }
    }
    
    /**
     * Logic gate for inserting a one-click case
     * @param template
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public int cecase_insertCECaseOneClickTemplate(CECaseOneClickTemplate template, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        if(!permissionsCheckpointCECaseTemplateAddEdit(ua, template)){
            throw new AuthorizationException("User lacks permission to insert one click template");
        }
        
        CaseIntegrator ci = getCaseIntegrator();
        cecase_auditCECaseOneClickTemplate(template);
        return ci.insertCECaseOneClickTemplate(template);
    }
    
    /**
     * logic block for updates to one-click cases
     * @param template
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void cecase_updateCECaseOneClickTemplate(CECaseOneClickTemplate template, UserAuthorized ua) throws BObStatusException,  IntegrationException,   AuthorizationException{
        if(!permissionsCheckpointCECaseTemplateAddEdit(ua, template)){
            throw new AuthorizationException("User lacks permission to insert one click template");
        }
        
        CaseIntegrator ci = getCaseIntegrator();
        cecase_auditCECaseOneClickTemplate(template);
        ci.updateCECasOneClickTemplate(template);
    }
    
    
    
    /**
     * logic block for writing the dac timestamp on a template; 
     * @param template this must still pass the internal audit since we're using the
     * same general update method to deac
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void cecase_deactivateCECaseOneClickTemplate(CECaseOneClickTemplate template, UserAuthorized ua) throws BObStatusException,  IntegrationException,   AuthorizationException{
        if(!permissionsCheckpointCECaseTemplateAddEdit(ua, template)){
            throw new AuthorizationException("User lacks permission to insert one click template");
        }
        
        CaseIntegrator ci = getCaseIntegrator();
        cecase_auditCECaseOneClickTemplate(template);
        template.setDeactivatedTS(LocalDateTime.now());
        ci.updateCECasOneClickTemplate(template);
    }
    
    
    /**
     * Home of all the goodies required to build a fresh CECase from a single click!
     * I call the standard create new case method which throws the whole van out as an exceptions
     * 
     * @param pdh
     * @param template cannot be null
     * @param ua cannot be null
     * @return the case just created
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.ViolationException
     * @throws com.tcvcog.tcvce.domain.EventException
     * @throws com.tcvcog.tcvce.domain.SearchException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public CECase cecase_createCECaseFromOneClickTemplate(PropertyDataHeavy pdh, CECaseOneClickTemplate template, UserAuthorized ua) 
            throws BObStatusException, IntegrationException, ViolationException, EventException, EventException, SearchException, AuthorizationException, BlobException{
        if(template == null || ua == null || pdh == null){
            throw new BObStatusException("Cannot create one touch case with null input template or null user");
        }
        
        if(!permissionsCheckpointOpenOneClickCase(pdh, ua)){
            throw new AuthorizationException("User failed permissions check for opening one-click case");
        }
        
        // refresh our property
        PropertyCoordinator pc = getPropertyCoordinator();
        pdh = pc.assemblePropertyDataHeavy(pdh, ua);
        
        CECase cse = cecase_initCECase(pdh, ua);
        
        cse.setCaseName(template.getDefaultCaseName());
        // build our actual case
        cse.setCaseManager(ua);
        cse.setOriginationDate(LocalDateTime.now());
        cse.setOneClickTemplate(template);
        cse.setOriginationEventCategory(template.getOriginationEventCategory());
        
        int freshCase = cecase_insertNewCECase(cse, pdh, ua, null);
        cse = cecase_getCECase(freshCase, ua);
        
        // now attach the template's standard violation
        CodeViolation viol = violation_injectOrdinance(null, template.getViolatedOrdinance());
        violation_attachViolationToCase(viol, cse, ua);
        CECaseDataHeavy cdh = cecase_assembleCECaseDataHeavy(cecase_getCECase(freshCase, ua), ua);
        
        // now create an NOV and finalize it, ready for printing
        NoticeOfViolation nov = nov_GetNewNOVSkeleton(cdh, ua);
        nov = nov_assembleNOVFromTemplate(nov, template.getNovTemplate(), cse);
        
        try {
            nov_injectSensibleRecipientAndMAD(pdh, nov);
            nov.setNotifyingOfficer(ua);
            nov_InsertNotice(nov, cdh, ua);
            if(template.isAutoFinalizeNOV()){
                nov_LockAndQueue(cdh, nov, ua);
            }
        } catch (BObStatusException | IntegrationException ex){
            throw new BObStatusException("Case successfully created but could not build notice letter automatically: the property is missing mailing address and/or person links");
        }
        return cse;
    }
    

    // *************************************************************************
    // *                     REPORTING                                         *
    // *************************************************************************
    /**
     * Factory method for creating new reports listing action requests
     *
     * @param u
     * @param m
     * @return
     */
    public ReportCEARList report_getInitializedReportConficCEARs(User u, Municipality m) {
        ReportCEARList rpt = new ReportCEARList();
        rpt.setIncludePhotos(true);
        rpt.setPrintFullCEARQueue(false);
        rpt.setCreator(u);
        rpt.setMuni(m);
        rpt.setGenerationTimestamp(LocalDateTime.now());
        return rpt;
    }

    /**
     * Factory method for CECase reports with sensible initial settings
     *
     * @param c
     * @param ua
     * @return for setting view minimums
     */
    public ReportConfigCECase report_getDefaultReportConfigCECase(CECaseDataHeavy c, UserAuthorized ua) {
        ReportConfigCECase rpt = new ReportConfigCECase();

        rpt.setViewerRankAssumed(ua.getRole());
        
        rpt.setMaximumOutputForUserRank(false);
        
        // general
        rpt.setIncludeCaseName(true);

        // events
        rpt.setIncludeOfficerOnlyEvents(true);
        rpt.setIncludeMunicipalityDiclosedEvents(true);
        rpt.setIncludeHiddenEvents(false);
        rpt.setIncludeEventNotes(true);
        rpt.setIncludeInactiveEvents(false);
        rpt.setIncludeEventMetadata(true); 
        rpt.setIncludeEventPersonLinks(true);
        
        // notices of violation
        rpt.setIncludeNoticeFullText(true);
        rpt.setIncludeAllNotices(true);

        // CEARS and $$
        rpt.setIncludeActionRequsts(true);
        rpt.setIncludeActionRequestPhotos(true);
        rpt.setCearColumnCount(2);
        rpt.setIncludeFeeAndPaymentInfo(true);
        
        // FIRs
        rpt.setIncludeFieldInspectionReports(true);
        rpt.setIncludeFieldInspectionReportsWithPhotos(true);
        
        // violations
        rpt.setIncludeFullOrdinanceText(true);
        rpt.setIncludeViolationPhotos(true);
        rpt.setViolationPhotoColumnCount(2);
        rpt.setIncludeViolationNotes(true);
        
        // photos and blobs
        rpt.setIncludePropertyBroadviewPhoto(true);
        rpt.setIncludeCECasePhotoPool(true);
        rpt.setIncludeDocDownloadLinks(true);
        rpt.setCeCasePhotoPoolColumnCount(2);
        
        //perons
        rpt.setIncludeAssociatedPersons(true);
        rpt.setIncludeAssociatedPersonsOnParentProperty(true);
        rpt.setIncludePersonAddrPhoneEmail(true);

        return rpt;
    }

    /**
     * Factory method for reports on a list of cases
     *
     * @param ua for event view filtering
     * @return
     */
    public ReportConfigCECaseList report_getDefaultReportConfigCECaseList(UserAuthorized ua) {
        ReportConfigCECaseList rptList = new ReportConfigCECaseList();
        
        rptList.setTextSize(ReportTextSizeEnum.MEDIUM);
        
        rptList.setIncludeSectionReportSummary(true);
        rptList.setIncludeSectionReportSummary_openAsOfSOR(true);
        rptList.setIncludeSectionReportSummary_openAsOfEOR(true);
        
        rptList.setIncludeSectionReportSummary_casesOpened(true);
        rptList.setIncludeSectionReportSummary_casesClosed(true);
        rptList.setIncludeSectionReportSummary_newViolations(true);
        
        rptList.setIncludeSectionReportSummary_compViol(true);
        rptList.setIncludeSectionReportSummary_eventsLogged(true);
        rptList.setIncludeSectionReportSummary_eventsTotalTime(true);
        
        rptList.setIncludeSectionReportSummary_closurePieChart(true);
        rptList.setIncludeSectionReportSummary_ceCaseStatusPie(true);
        rptList.setIncludeSectionCodeViolationStatus(true);
        
        rptList.setIncludeSectionCodeViolationStatus_compliance(true);
        rptList.setIncludeSectionCodeViolationStatus_withinWindow(true);
        rptList.setIncludeSectionCodeViolationStatus_expiredWindow(true);
        
        rptList.setIncludeSectionCodeViolationStatus_cited(true);
        rptList.setIncludeSectionCitations(true);
        rptList.setIncludeSectionCitations_citationsAnyStage(true);
        
        rptList.setIncludeSectionCitations_pieChart(true);
        rptList.setIncludeSectionStreetLevelSummary(true);
        rptList.setIncludeSectionCaseByCaseDetail(true);
        
        rptList.setIncludeSectionCEActionRequestSummary(true);
        
        rptList.setIncludeSectionCaseByCaseDetail_fullOwnerContactInfo(true);
        rptList.setIncludeSectionCaseByCaseDetail_caseNames(true);
        rptList.setIncludeSectionCaseByCaseDetail_violationList(true);
        
        rptList.setIncludeSectionCaseByCaseDetail_extendedPropertyDetails(true);
        
        rptList.setIncludeSectionCaseByCaseDetail_eventSummary(true);
        rptList.setIncludeSectionCaseByCaseDetail_eventListViewerRank(ua.getRole());
        rptList.setIncludeSectionCaseByCaseDetail_eventDescription(false);
        
        return rptList;

    }

    /**
     * Prepares a report of municipal case activity for a given time period
     *
     * @param rpt the report config object with dates set for searching
     * @param ua the user requesting the report
     * @return the configured report
     * @throws SearchException
     */
    public ReportConfigCECaseList report_buildCECaseListReport(ReportConfigCECaseList rpt, UserAuthorized ua) throws SearchException {
        SearchCoordinator sc = getSearchCoordinator();
        if (rpt == null || ua == null) {
            return null;
        }
        rpt.setRequestingUser(ua);
        SearchParamsCECase spcse;
          
        // Phase 1: Cases open as of report START date 
        // Akin to "beginning inventory"
        QueryCECase queryOpenAsOfSOR = sc.initQuery(QueryCECaseEnum.OPEN_ASOFGIVENDATE, ua.getKeyCard());
        // query audit setup
        if (queryOpenAsOfSOR.getParamsList() != null && queryOpenAsOfSOR.getParamsList().size() == 2) {
            // setup params
            spcse = queryOpenAsOfSOR.getPrimaryParams();

            // client field injection of END date value: cases open as of START 
            if (spcse.getDateRuleList() != null && spcse.getDateRuleList().size() == 2) {
                // COORDINATOR injects date field SearchParamsCECaseDateFieldsEnum.ORIGINATION_DOFRECORD
                // Corodinator sets date start val to JAN 1 1970
                spcse.getDateRuleList().get(0).setDate_end_val(rpt.getDate_start_val());
                spcse.getDateRuleList().get(1).setDate_start_val(rpt.getDate_start_val().plusDays(1));
                // search coord injected now() as the end date
            }

            // client field injection: cases open as of EOR and not closed, even up to today
            if (queryOpenAsOfSOR.getParamsList() != null && !queryOpenAsOfSOR.getParamsList().isEmpty()) {
                SearchParamsCECase sponc = queryOpenAsOfSOR.getParamsList().get(1);
                sponc.getDateRuleList().get(0).setDate_end_val(rpt.getDate_start_val());
            }
        } else {
            System.out.println("CaseCoordinator.assembleCECaseListReport: Failed audit of Open SOR logic");
        }

        List<CECase> casesOpenAsOfSOR = sc.runQuery(queryOpenAsOfSOR, ua).getBOBResultList();
        System.out.println("Query: " + queryOpenAsOfSOR.getQueryLog());

        if (casesOpenAsOfSOR != null && !casesOpenAsOfSOR.isEmpty()) {
            System.out.println("CaseCoordinator.assembleCECaseListReport: OPEN AS OF START OR REPORT");
            for (CECase cse : casesOpenAsOfSOR) {
                System.out.print("CID: " + cse.getCaseID());
                System.out.println("| name: " + cse.getCaseName());
            }
        }

        rpt.setCaseListOpenAsOfDateStart(cecase_assembleCECaseDataHeavyList(casesOpenAsOfSOR, ua));
        
        // Phase 2: Cases opened during reporting period
        QueryCECase query_opened = sc.initQuery(QueryCECaseEnum.OPENED_INDATERANGE, ua.getKeyCard());
        spcse = query_opened.getPrimaryParams();
        spcse.setDate_startEnd_ctl(true);
        spcse.setDate_field(SearchParamsCECaseDateFieldsEnum.ORIGINATION_DOFRECORD);
        spcse.setDate_start_val(rpt.getDate_start_val());
        spcse.setDate_end_val(rpt.getDate_end_val());
        rpt.setCaseListOpenedInDateRange(cecase_assembleCECaseDataHeavyList(sc.runQuery(query_opened, ua).getBOBResultList(), ua));

        if (rpt.getCaseListOpenedInDateRange() != null) {
            System.out.println("CaseCoordinator.report_buildCECaseListReport: Opened List size " + rpt.getCaseListOpenedInDateRange().size());
        }
        
        // Phase 3: Cases closed during reporting period
        QueryCECase query_closed = sc.initQuery(QueryCECaseEnum.CLOSED_CASES, ua.getKeyCard());
        spcse = query_closed.getPrimaryParams();
        spcse.setDate_field(SearchParamsCECaseDateFieldsEnum.CLOSE);
        spcse.setDate_startEnd_ctl(true);
        spcse.setDate_start_val(rpt.getDate_start_val());
        spcse.setDate_end_val(rpt.getDate_end_val());
        rpt.setCaseListClosedInDateRange(cecase_assembleCECaseDataHeavyList(sc.runQuery(query_closed, ua).getBOBResultList(), ua));
        if (rpt.getCaseListClosedInDateRange() != null) {
            System.out.println("CaseCoordinator.report_buildCECaseListReport: Current List size " + rpt.getCaseListClosedInDateRange().size());
        }

        // Phase 4: Cases open as of end of reporting period
        // AKIN to "Ending Inventory"
        QueryCECase queryOpenedAsOfEndDate = sc.initQuery(QueryCECaseEnum.OPEN_ASOFENDDATE, ua.getKeyCard());

        if (queryOpenedAsOfEndDate.getParamsList() != null && queryOpenedAsOfEndDate.getParamsList().size() == 2) {

            // setup params
            spcse = queryOpenedAsOfEndDate.getPrimaryParams();

            // client field injection: cases open as of EOR but closed between EOR+1 and now()
            // meaning they were still open as of EOR
            if (spcse.getDateRuleList() != null && spcse.getDateRuleList().size() == 2) {
                spcse.getDateRuleList().get(0).setDate_end_val(rpt.getDate_end_val());
                // we have to make this adjustment since we still need to think of a case as open
                // if it has since closed and therefore has a NOT NULL closing date
                // The costs of getting a computer to think it's not now()
                spcse.getDateRuleList().get(1).setDate_start_val(rpt.getDate_end_val().plusDays(1));
                System.out.println("CaseCoordinator.report_buildCECaseListReport " + spcse.getDateRuleList());
            }

            // client field injection: cases open as of EOR and not closed, even today
            if (queryOpenedAsOfEndDate.getParamsList() != null && !queryOpenedAsOfEndDate.getParamsList().isEmpty()) {
                SearchParamsCECase sponc = queryOpenedAsOfEndDate.getParamsList().get(1);
                sponc.getDateRuleList().get(0).setDate_end_val(rpt.getDate_end_val());
                System.out.println("CaseCoordinator.report_buildCECaseListReport " + sponc.getDateRuleList());
            }
        }

        List<CECase> casesOpenAsOfEOR = sc.runQuery(queryOpenedAsOfEndDate, ua).getBOBResultList();
        System.out.println("Query: " + queryOpenedAsOfEndDate.getQueryLog());

        if (casesOpenAsOfEOR != null && !casesOpenAsOfEOR.isEmpty()) {
            System.out.println("CaseCoordinator.assembleCECaseListReport: OPEN AS OF END OF REPORT CASE LIST");
            for (CECase cse : casesOpenAsOfEOR) {
                System.out.print("CID: " + cse.getCaseID());
                System.out.println("| name: " + cse.getCaseName());
            }
        }

        rpt.setCaseListOpenAsOfDateEnd(cecase_assembleCECaseDataHeavyList(casesOpenAsOfEOR, ua));
        
      
        
      // AVERAGE CASE AGE  
        if (rpt.getCaseListOpenAsOfDateEnd() != null && !rpt.getCaseListOpenAsOfDateEnd().isEmpty()) {
            System.out.println("CaseCoordinator.report_buildCECaseListReport: Opened List size " + rpt.getCaseListOpenAsOfDateEnd().size());
            // compute average case age for opened as of date list
            long sumOfAges = 0l;
            for (CECase cse : rpt.getCaseListOpenAsOfDateEnd()) {
                sumOfAges += cse.getCaseAgeAsOf(rpt.getDate_end_val());
            }
            double avg = (double) sumOfAges / (double) rpt.getCaseListOpenAsOfDateEnd().size();
            DecimalFormat df = new DecimalFormat("###.#");
            rpt.setAverageAgeOfCasesOpenAsOfReportEndDate(df.format(avg));
        }

      

        try {
            // BUILD BY STREET
            rpt = report_ceCaseList_organizeByStreet(rpt);
        } catch (BObStatusException | IntegrationException ex) {
            System.out.println(ex);
        }

        // EVENTS
        QueryEvent query_ev = sc.initQuery(QueryEventEnum.MUNI_MONTHYACTIVITY, ua.getKeyCard());
        SearchParamsEvent spev = query_ev.getPrimaryParams();
        spev.setDate_startEnd_ctl(true);
        spev.setDate_start_val(rpt.getDate_start_val());
        spev.setDate_end_val(rpt.getDate_end_val());  
        rpt.setEventList(getEventCoordinator().filterEventListForViewFloor(sc.runQuery(query_ev, ua).getBOBResultList(), rpt.getIncludeSectionCaseByCaseDetail_eventListViewerRank()));

        // VIOLATIONS
        
        QueryCodeViolation qcodeVDORInPeriod = sc.initQuery(QueryCodeViolationEnum.LOGGED_IN_DATE_RANGE, ua.getKeyCard());
        SearchParamsCodeViolation params = qcodeVDORInPeriod.getPrimaryParams();
        params.setDate_relativeDates_ctl(false);
        params.setDate_start_val(rpt.getDate_start_val());
        params.setDate_end_val(rpt.getDate_end_val());
        rpt.setViolationsLoggedDateRange(sc.runQuery(qcodeVDORInPeriod).getResults());
        
        System.out.println("CaseCoordinator.report_buildCECaseListReport: Violation DOR in range query log" + qcodeVDORInPeriod.getQueryLog());
        
        QueryCodeViolation qcv = sc.initQuery(QueryCodeViolationEnum.COMP_PAST30, ua.getKeyCard());
        params = qcv.getPrimaryParams();
        params.setDate_relativeDates_ctl(false);
        params.setDate_start_val(rpt.getDate_start_val().minusYears(ARBITRARILY_LONG_TIME_YEARS));
        params.setDate_end_val(rpt.getDate_end_val());
        rpt.setViolationsAccumulatedCompliance(sc.runQuery(qcv).getResults());
        
        qcv = sc.initQuery(QueryCodeViolationEnum.COMP_PAST30, ua.getKeyCard());
        params = qcv.getPrimaryParams();
        params.setDate_relativeDates_ctl(false);
        params.setDate_start_val(rpt.getDate_start_val());
        params.setDate_end_val(rpt.getDate_end_val());
        rpt.setViolationsLoggedComplianceDateRange(sc.runQuery(qcv).getResults());
        
        qcv = sc.initQuery(QueryCodeViolationEnum.VIOLATIONS_INSIDE_COMPLIANCE_WINDOW, ua.getKeyCard());
        params = qcv.getPrimaryParams();
        params.setDate_relativeDates_ctl(false);
        // a violation that's in the compliance window as of END of report, 
        // will only be grabbed by asking if the the stip comp date is AFTER
        // the end of the report, and into the future some arbitrary number of days
        // That's why we're injecting the END date of the report as the START
        // date of our violation stip comp date query
        params.setDate_start_val(rpt.getDate_end_val());
        params.setDate_end_val(rpt.getDate_end_val().plusYears(ARBITRARILY_LONG_TIME_YEARS)); // arbitrary magic number of years into the future (thanks WWALK)
        rpt.setViolationsWithinStipCompWindow(sc.runQuery(qcv).getResults());
        
        qcv = sc.initQuery(QueryCodeViolationEnum.COMPLIANCE_WINDOW_EXPIRED_NOTCITED, ua.getKeyCard());
        params = qcv.getPrimaryParams();
        params.setDate_relativeDates_ctl(false);
        // since the violation is still active, we'll get expired ones
        // by capturing only the ones whose stip comp date is anytime in the past up to the report end date 
        params.setDate_start_val(rpt.getDate_end_val().minusYears(ARBITRARILY_LONG_TIME_YEARS));
        params.setDate_end_val(rpt.getDate_end_val()); 
        rpt.setViolationsOutsideCompWindowNOTCited(sc.runQuery(qcv).getResults());
        
        qcv = sc.initQuery(QueryCodeViolationEnum.NOCOMP_CITED_ANYTIME, ua.getKeyCard());
        params = qcv.getPrimaryParams();
        params.setDate_relativeDates_ctl(false);
        
        
        // this is an accumulated figure whose only bound is the repot end date
        params.setDate_start_val(rpt.getDate_end_val().minusYears(ARBITRARILY_LONG_TIME_YEARS));
        params.setDate_end_val(rpt.getDate_end_val());
        rpt.setViolationsNoComplianceButCited(sc.runQuery(qcv).getResults());
//        
//        initBarViolationsPastMonth(rpt);
//        initPieEnforcement(rpt);
        
        // deprecated as of AUG 2022 to use discrete stats
        // initPieViols(rpt);
        
        if(rpt.isIncludeSectionCEActionRequestSummary()){
            try {
                report_cecaseList_buildCEARSummary(rpt);
            } catch (IntegrationException | BObStatusException ex) {
                System.out.println(ex);
            } 
        }
        
        try{
            initPieCitation(rpt);
            initPieClosure(rpt);
        } catch (BObStatusException ex){
            System.out.println(ex);
        }

        return rpt;

    }
    
    /**
     * Organ for searching for and building a CE action request summary table
     * @param rptCseList
     * @return 
     */
    private ReportConfigCECaseList report_cecaseList_buildCEARSummary(ReportConfigCECaseList rptCseList) throws IntegrationException, BObStatusException, SearchException{
        SearchCoordinator sc = getSearchCoordinator();
        
        CEActionRequestSummaryBundle cearBundle = new CEActionRequestSummaryBundle();
        
        // fetch me all active cears submitted 
        QueryCEAR cearSearch = sc.initQuery(QueryCEAREnum.CUSTOM, rptCseList.getRequestingUser().getMyCredential());
        
        cearSearch.getPrimaryParams().setActive_ctl(true);
        cearSearch.getPrimaryParams().setActive_val(true);
        
        cearSearch.getPrimaryParams().setDate_startEnd_ctl(true);
        cearSearch.getPrimaryParams().setDate_field(SearchParamsCEActionRequestsDateFieldsEnum.SUBMISSION_TS);
        cearSearch.getPrimaryParams().setDate_start_val(rptCseList.getDate_start_val());
        cearSearch.getPrimaryParams().setDate_end_val(rptCseList.getDate_end_val());

        List<CEActionRequest> cearsInPeriod = sc.runQuery(cearSearch).getBOBResultList();
        cearBundle.setRequestsInPeriod(cearsInPeriod);
        
        QueryCEAR unprocessedPriorToReport = sc.initQuery(QueryCEAREnum.UNPROCESSED, rptCseList.getRequestingUser().getMyCredential());
        
        // we also want to report any unassigned rquests from any time before reporting period
        SearchParamsCEActionRequests paramsUnprocessedBeforeReportingPeriod = unprocessedPriorToReport.getPrimaryParams();
        paramsUnprocessedBeforeReportingPeriod.setDate_startEnd_ctl(true);
        paramsUnprocessedBeforeReportingPeriod.setDate_field(SearchParamsCEActionRequestsDateFieldsEnum.SUBMISSION_DOR);
        paramsUnprocessedBeforeReportingPeriod.setDate_start_val(SearchCoordinator.EARLIEST_DATE);
        paramsUnprocessedBeforeReportingPeriod.setDate_end_val(rptCseList.getDate_start_val());
        
        List<CEActionRequest> cearsUnprocessedBefore = sc.runQuery(unprocessedPriorToReport).getBOBResultList();
        cearBundle.setUnprocessedCEARSBeforeReporting(cearsUnprocessedBefore);
        
        // Help here from ChatGPT version 40
        // Group action requests by their issue type
        Map<CEActionRequestIssueType, List<CEActionRequest>> groupedByIssue = cearsInPeriod.stream()
                .collect(Collectors.groupingBy(CEActionRequest::getIssue));

        // Create CEActionRequestIssueRow objects for each issue group
        List<CEActionRequestIssueRow> rows = groupedByIssue.entrySet().stream()
                .map(entry -> {
                    CEActionRequestIssueType issueType = entry.getKey();
                    List<CEActionRequest> requests = entry.getValue();

                    // Count occurrences of each status within the requests
                    Map<CEActionRequestStatus, Long> statusCountMap = requests.stream()
                            .filter(request -> request.getRequestStatus() != null) // Ensure status is not null
                            .collect(Collectors.groupingBy(
                                    CEActionRequest::getRequestStatus,
                                    Collectors.counting()
                            ));

                    // Convert Long to Integer for compatibility with CEActionRequestIssueRow
                    Map<CEActionRequestStatus, Integer> issueOutcomeMap = statusCountMap.entrySet().stream()
                            .collect(Collectors.toMap(
                                    Map.Entry::getKey,
                                    entryStatus -> entryStatus.getValue().intValue()
                            ));

                    // Create and populate CEActionRequestIssueRow
                    CEActionRequestIssueRow row = new CEActionRequestIssueRow();
                    row.setIssue(issueType);
                    row.setRequestList(requests);
                    row.setIssueOutcomeMap(issueOutcomeMap);
                    System.out.println("CaseCoordinator.report_cecaseList_buildCEARSummary | issueOutcomeMap: " + issueOutcomeMap);

                    return row;
                })
                .collect(Collectors.toList());
        cearBundle.setIssueRowList(rows);
        
        // Build map of outcome to totals
        Map<CEActionRequestStatus, Long> totalCountByStatusMap = cearsInPeriod.stream()
                .filter(req -> req.getRequestStatus() != null)
                .collect(Collectors.groupingBy(CEActionRequest::getRequestStatus, Collectors.counting()));
        cearBundle.setTotalByOutcome(totalCountByStatusMap);
        
        long totalCEARSWithOutcome = totalCountByStatusMap.values().stream().mapToLong(Long::longValue).sum();
        cearBundle.setOutcomeTabulatedTotal(totalCEARSWithOutcome);
        
        rptCseList.setCearSummaryBundle(cearBundle);
        
        return rptCseList;
    }
    

    /**
     * Internal logic for taking apart case lists and turning them into
     * street-categorized maps
     *
     * @param rptCseList
     * @return the inputted report config object with its street container list
     * assembled
     */
    private ReportConfigCECaseList report_ceCaseList_organizeByStreet(ReportConfigCECaseList rptCseList) 
            throws IntegrationException, BObStatusException {
        PropertyCoordinator pc = getPropertyCoordinator();
        
        if (rptCseList != null) {
            Map<String, ReportCECaseListStreetCECaseContainer> streetCaseMap = new HashMap<>();
            List<ReportCECaseListCatEnum> reportListsList = Arrays.asList(ReportCECaseListCatEnum.values());
            for (ReportCECaseListCatEnum enm : reportListsList) {
                List<CECaseDataHeavy> cseList = null;
                switch (enm) {
                    case CLOSED:
                        cseList = rptCseList.getCaseListClosedInDateRange();
                        break;
                    case CONTINUING:
                        cseList = rptCseList.getCaseListOpenAsOfDateEnd();
                        break;
                    case OPENED:
                        cseList = rptCseList.getCaseListOpenedInDateRange();
                        break;
                }

                if (cseList != null && !cseList.isEmpty()) {
                    ReportCECaseListStreetCECaseContainer streetCaseContainer = null;

                    for (CECaseDataHeavy cse : cseList) {
                        // filter event list, overwrting the member
                        getEventCoordinator().filterEventHoldersEventList(cse, rptCseList.getIncludeSectionCaseByCaseDetail_eventListViewerRank());
                        
                        Property cseProp;
                        try {
                            cseProp = pc.getProperty(cse.getParcelKey());
                        } catch (AuthorizationException ex) {
                            throw new BObStatusException(ex.getMessage());
                        }
                        
                        // check for an address, and create a generic one if there isn't one
                        if (cseProp.getPrimaryAddressLink() == null) {
                            MailingAddressLink madLink = pc.getMailingAddressLinkSkeleton(pc.getMailingAddressSkeleton());
                            madLink.getStreet().setName(ADDRESSLESS_PROPERTY_STREET_NAME);
                            List<MailingAddressLink> madLinkList = new ArrayList<>();
                            madLinkList.add(madLink);
                            cseProp.setMailingAddressLinkList(madLinkList);
                        }
                        // start with streets we don't currently have a case list for
                        if (!streetCaseMap.containsKey(cseProp.getPrimaryAddressLink().getStreet().getName())) {
                            streetCaseContainer = new ReportCECaseListStreetCECaseContainer();
                            streetCaseContainer.setStreetName(cseProp.getPrimaryAddressLink().getStreet().getName());
                            switch (enm) {
                                case CLOSED:
                                    streetCaseContainer.getCaseClosedList().add(cse);
                                    break;
                                case CONTINUING:
                                    streetCaseContainer.getCaseContinuingList().add(cse);
                                    break;
                                case OPENED:
                                    streetCaseContainer.getCaseOpenedList().add(cse);
                                    break;
                            } // close switch
                            // process cases on streets already in our mapping
                        } else {
                            streetCaseContainer = streetCaseMap.get(cseProp.getPrimaryAddressLink().getStreet().getName());
                            switch (enm) {
                                case CLOSED:
                                    streetCaseContainer.getCaseClosedList().add(cse);
                                    break;
                                case CONTINUING:
                                    streetCaseContainer.getCaseContinuingList().add(cse);
                                    break;
                                case OPENED:
                                    streetCaseContainer.getCaseOpenedList().add(cse);
                                    break;
                            } // close switch
                        } // close if/else
                        // Write new or updated Street Case continaer to our map
                        streetCaseMap.put(cseProp.getPrimaryAddressLink().getStreet().getName(), streetCaseContainer);
                    } // close for over case list
                } // close null/emtpy check
            } // close for over enum vals

            report_ceCaseList_removeDupsByStreet(streetCaseMap);
            rptCseList.setStreetSCC(streetCaseMap);
            rptCseList.assembleStreetList();

        } // close param null check

        return rptCseList;

    }
    
    /**
     * When we remove the dupes the case counts by street are off when cases are opened & closed in the same period
     * so this method transfers the list counts to int vars before the de-duping. This MUST be 
     * @param streetCaseMap 
     */
    private void report_ceCaseList_recordCaseCountsPreDupRemoval(Map<String, ReportCECaseListStreetCECaseContainer> streetCaseMap){
        for (String rclsc : streetCaseMap.keySet()) {
            ReportCECaseListStreetCECaseContainer cont = streetCaseMap.get(rclsc);
            if(cont.getCaseClosedList() != null){
                cont.setCaseClosedListCountPreDup(cont.getCaseClosedList().size());
            }
            if(cont.getCaseOpenedList()!= null){
                cont.setCaseOpenedListCountPreDup(cont.getCaseOpenedList().size());
            }
            if(cont.getCaseContinuingList() != null){
                cont.setCaseContinuingListCountPreDup(cont.getCaseContinuingList().size());
            }
        }
    }

    /**
     * Removes cases which were opened and closed in the same period and leaves
     * them in the Closed group
     *
     * @param rpt
     */
    private void report_ceCaseList_removeDupsByStreet(Map<String, ReportCECaseListStreetCECaseContainer> streetCaseMap) {
        report_ceCaseList_recordCaseCountsPreDupRemoval(streetCaseMap);
        for (String rclsc : streetCaseMap.keySet()) {
            ReportCECaseListStreetCECaseContainer cont = streetCaseMap.get(rclsc);
            for (CECaseDataHeavy cse : cont.getCaseOpenedList()) {
                if (cont.getCaseClosedList().contains(cse)) {
                    System.out.println("CaseCoordinator.removeDupes: removing case ID " + cse.getCaseID());
                    cont.getCaseClosedList().remove(cse);
                }
            }
        }
    }

    /**
     * Builds a violation pie chart
     *
     * @Deprecated  in favor of discrete statistics since this pie 
     * was asserted to be misleading to report readers
     * @param rpt
     */
    private void initPieViols(ReportConfigCECaseList rpt) {
        CaseCoordinator cc = getCaseCoordinator();
        rpt.setPieViol(new PieChartModel());
        ChartData pieData = new ChartData();

        PieChartDataSet dataSet = new PieChartDataSet();
        List<Number> violationCounts = new ArrayList<>();

        List<CodeViolationStatusHeavy> vl = new ArrayList<>();
        for (CECaseDataHeavy cdh : rpt.assembleFullCaseLiset()) {
            vl.addAll(cdh.assembleViolationList(ViewOptionsActiveListsEnum.VIEW_ACTIVE));
        }

        // build our count map
        Map<CodeViolationStatusEnum, Integer> violMap = new HashMap<>();
        violMap.put(CodeViolationStatusEnum.UNRESOLVED_WITHINCOMPTIMEFRAME, 0);
        violMap.put(CodeViolationStatusEnum.UNRESOLVED_EXPIREDCOMPLIANCETIMEFRAME, 0);
        violMap.put(CodeViolationStatusEnum.RESOLVED, 0);
        violMap.put(CodeViolationStatusEnum.CITED_UNRESOLVED, 0);
        violMap.put(CodeViolationStatusEnum.NULLIFIED, 0);
        violMap.put(CodeViolationStatusEnum.TRANSFERRED, 0);
        

        for (CodeViolationStatusHeavy cv : vl) {
            CodeViolationStatusEnum vs = cv.getStatus();
            Integer catCount = violMap.get(vs);
            catCount += 1;
            violMap.put(cv.getStatus(), catCount);
        }

        rpt.setPieViolStatMap(violMap);
        rpt.setPieViolCompCount(violMap.get(CodeViolationStatusEnum.RESOLVED));
        Set<CodeViolationStatusEnum> keySet = violMap.keySet();
        List<String> pieColors = new ArrayList<>();
        List<LegendItem> legend = new ArrayList<>();

        SystemCoordinator sc = getSystemCoordinator();

        for (CodeViolationStatusEnum vse : keySet) {
            Integer i = violMap.get(vse);
            violationCounts.add(i);
            String color = sc.generateRandomRGBColor();
            pieColors.add(color);
            LegendItem li = new LegendItem();
            li.setColorRGBString(color);
            li.setValue(i);
            li.setTitle(vse.getLabel());
            legend.add(li);
        }

        rpt.setPieViolLegend(legend);
        dataSet.setData(violationCounts);
        dataSet.setBackgroundColor(pieColors);
        pieData.addChartDataSet(dataSet);
        rpt.getPieViol().setData(pieData);
    }

    /**
     * Builds a pie chart about case enforcement status
     *
     * @param rpt
     * @Deprecated use priority instead
     */
    private void initPieEnforcement(ReportConfigCECaseList rpt) {

        if (rpt != null) {

            rpt.setPieEnforcement(new PieChartModel());
            ChartData pieData = new ChartData();

            PieChartDataSet dataSet = new PieChartDataSet();
            List<Number> enfStatusNumList = new ArrayList<>();

            Map<String, Integer> statMap = new HashMap<>();

            statMap.put(CaseStageEnum.Investigation.getLabel(), 0);
            statMap.put(CasePhaseEnum.InsideComplianceWindow.getLabel(), 0);
            statMap.put(CasePhaseEnum.TimeframeExpiredNotCited.getLabel(), 0);
            statMap.put(CaseStageEnum.Citation.getLabel(), 0);

            for (CECaseDataHeavy cse : rpt.assembleNonClosedCaseList()) {
                if (cse.getStatusBundle().getPhase().getStage() == CaseStageEnum.Investigation) {
                    Integer statCount = statMap.get(CaseStageEnum.Investigation.getLabel());
                    statCount += 1;
                    statMap.put(CaseStageEnum.Investigation.getLabel(), statCount);

                }
                if (cse.getStatusBundle().getPhase().getCaseStage() == CaseStageEnum.Enforcement) {
                    if (cse.getStatusBundle().getPhase() == CasePhaseEnum.InsideComplianceWindow) {
                        Integer statCount = statMap.get(CasePhaseEnum.InsideComplianceWindow.getLabel());
                        statCount += 1;
                        statMap.put(CasePhaseEnum.InsideComplianceWindow.getLabel(), statCount);
                    }
                    if (cse.getStatusBundle().getPhase() == CasePhaseEnum.TimeframeExpiredNotCited) {
                        Integer statCount = statMap.get(CasePhaseEnum.TimeframeExpiredNotCited.getLabel());
                        statCount += 1;
                        statMap.put(CasePhaseEnum.TimeframeExpiredNotCited.getLabel(), statCount);
                    }
                }

                if (cse.getStatusBundle().getPhase().getStage() == CaseStageEnum.Citation) {
                    Integer statCount = statMap.get(CaseStageEnum.Citation.getLabel());
                    statCount += 1;
                    statMap.put(CaseStageEnum.Citation.getLabel(), statCount);
                }
            }

            Set<String> keySet = statMap.keySet();

            List<String> pieColors = new ArrayList<>();
            List<LegendItem> legend = new ArrayList<>();

            SystemCoordinator sc = getSystemCoordinator();

            for (String k : keySet) {
                Integer i = statMap.get(k);
                enfStatusNumList.add(i);
                String color = sc.generateRandomRGBColor();
                pieColors.add(color);
                LegendItem li = new LegendItem();
                li.setColorRGBString(color);
                li.setValue(i);
                li.setTitle(k);
                legend.add(li);

            }
            rpt.setPieEnforcementLegend(legend);

            dataSet.setBackgroundColor(pieColors);
            dataSet.setData(enfStatusNumList);
            pieData.addChartDataSet(dataSet);

            rpt.getPieEnforcement().setData(pieData);
        }
    }

    /**
     * Builds a pie chart about citations
     *
     * @param rpt
     */
    private void initPieCitation(ReportConfigCECaseList rpt) throws BObStatusException {
        if (rpt == null) {
            throw new BObStatusException("Cannot build chart with null report");
        }

            rpt.setPieCitation(new PieChartModel());
            ChartData pieData = new ChartData();

            PieChartDataSet dataSet = new PieChartDataSet();
            List<Number> statusCounts = new ArrayList<>();

            List<CECaseDataHeavy> nccl = rpt.assembleNonClosedCaseList();
            List<Citation> citl = new ArrayList<>();
            for (CECaseDataHeavy cse : nccl) {
                citl.addAll(cse.getCitationList());
            }

            rpt.setCitationList(citl);

            Map<String, Integer> citStatusMap = new HashMap<>();

            // COUNT BY Citation status
            if (!citl.isEmpty()) {
                for (Citation cit : citl) {
                    if (cit != null && cit.getMostRecentStatusLogEntry() != null && cit.getMostRecentStatusLogEntry().getStatus() != null) {
                        String statTitle = cit.getMostRecentStatusLogEntry().getStatus().getStatusTitle();
                        if (citStatusMap.containsKey(statTitle)) {
                            Integer statCount = citStatusMap.get(statTitle) + 1;
                            citStatusMap.put(statTitle, statCount);
                        } else {
                            citStatusMap.put(statTitle, 1);
                        }
                    }
                }
            }

            Set<String> keySet = citStatusMap.keySet();

            List<String> pieColors = new ArrayList<>();
            List<LegendItem> legend = new ArrayList<>();

            SystemCoordinator sc = getSystemCoordinator();

            for (String k : keySet) {
                Integer i = citStatusMap.get(k);
                statusCounts.add(i);
                String color = sc.generateRandomRGBColor();
                pieColors.add(color);
                LegendItem li = new LegendItem();
                li.setColorRGBString(color);
                li.setValue(i);
                li.setTitle(k);
                legend.add(li);

            }
            rpt.setPieCitationLegend(legend);
            dataSet.setData(statusCounts);
            dataSet.setBackgroundColor(pieColors);
            pieData.addChartDataSet(dataSet);
            rpt.getPieCitation().setData(pieData);

    }

    /**
     * Builds a pie chart about case closures
     *
     * @param rpt
     */
    private void initPieClosure(ReportConfigCECaseList rpt) {
        rpt.setPieClosure(new PieChartModel());
        ChartData pieData = new ChartData();

        PieChartDataSet dataSet = new PieChartDataSet();
        List<Number> closingEventCountList = new ArrayList<>();

        List<EventCnF> evList = new ArrayList<>();

        // GET ONLY CLOSING EVENTS
        if (rpt.getCaseListClosedInDateRange() != null && !rpt.getCaseListClosedInDateRange().isEmpty()) {
            for (CECaseDataHeavy cse : rpt.getCaseListClosedInDateRange()) {
                List<EventCnF> evl = cse.getEventList(ViewOptionsActiveHiddenListsEnum.VIEW_ALL);
                if (!evl.isEmpty()) {
                    for (EventCnF ev : evl) {
                        if (ev.getCategory().getEventType() == EventType.Closing) {
                            evList.add(ev);
                            break; // only add the first closing event encountered
                        }
                    }
                }
            }
        }

        Map<String, Integer> closeEventMap = new HashMap<>();

        // COUNT BY CLOSE EVENT CATEGORY TITLE
        if (!evList.isEmpty()) {
            for (EventCnF ce : evList) {
                if (closeEventMap.containsKey(ce.getCategory().getEventCategoryTitle())) {
                    Integer catCount = closeEventMap.get(ce.getCategory().getEventCategoryTitle()) + 1;
                    closeEventMap.put(ce.getCategory().getEventCategoryTitle(), catCount);
                } else {
                    closeEventMap.put(ce.getCategory().getEventCategoryTitle(), 1);
                }
            }
        }

        Set<String> keySet = closeEventMap.keySet();

        List<String> pieColors = new ArrayList<>();
        List<LegendItem> legend = new ArrayList<>();

        SystemCoordinator sc = getSystemCoordinator();

        for (String k : keySet) {
            Integer i = closeEventMap.get(k);
            closingEventCountList.add(i);
            String color = sc.generateRandomRGBColor();
            pieColors.add(color);
            LegendItem li = new LegendItem();
            li.setColorRGBString(color);
            li.setValue(i);
            li.setTitle(k);
            legend.add(li);

        }

        rpt.setPieClosureLegend(legend);

        // Inject into our pie
        dataSet.setData(closingEventCountList);
        dataSet.setBackgroundColor(pieColors);
        pieData.addChartDataSet(dataSet);
        rpt.getPieClosure().setData(pieData);
    }

  

    /**
     * Primary configuration mechanism for customizing report data from the
     * ceCases.xhtml display. The logic inside me makes sure that hidden events
     * don't make it out to the report, etc. So the ReportConfig object is ready
     * for display and printing
     *
     * @param rptCse
     * @param ua
     * @return
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public ReportConfigCECase report_prepareCECaseReport(ReportConfigCECase rptCse, UserAuthorized ua) throws IntegrationException, BObStatusException, SearchException, BlobException {
        PropertyCoordinator pc = getPropertyCoordinator();
        PersonCoordinator persC = getPersonCoordinator();
        
        if(rptCse == null || rptCse.getCse() == null){
            throw new BObStatusException("Cannot generate report with null config or case inside that config");
        }
        // we actually get an entirely new object instead of editing the 
        // one we used throughout the ceCases.xhtml
        CECaseDataHeavy c = rptCse.getCse();
        
        if(c.getManager() != null){
            rptCse.setCaseManagerPerson(persC.getPerson(c.getManager().getHuman()));
        }
        
        try {
            rptCse.setPropDH(pc.assemblePropertyDataHeavy(pc.getProperty(rptCse.getCse().getParcelKey()), ua));
        } catch (AuthorizationException ex) {
            throw new BObStatusException(ex.getMessage());
        }
        
        List<EventCnF> evList = new ArrayList<>();
        Iterator<EventCnF> iter = c.getEventList(ViewOptionsActiveHiddenListsEnum.VIEW_ALL).iterator();
        while (iter.hasNext()) {
            EventCnF ev = iter.next();

            // toss out hidden events unless the user wants them
            if (ev.isHidden() && !rptCse.isIncludeHiddenEvents()) {
                continue;
            }
            // toss out inactive events unless user wants them
            if (!ev.isActive() && !rptCse.isIncludeInactiveEvents()) {
                continue;
            }
            // toss out events only available internally to the muni users unless user wants them
            evList.add(ev);
        }
        rptCse.setEventListForReport(getEventCoordinator().filterEventListForViewFloor(evList, rptCse.getViewerRankAssumed()));

        List<NoticeOfViolation> noticeList = new ArrayList<>();
        Iterator<NoticeOfViolation> iterNotice = c.getNoticeList().iterator();
        while (iterNotice.hasNext()) {
            NoticeOfViolation nov = iterNotice.next();
            // skip unsent notices
            if (nov.getSentTS() == null) {
                continue;
            }
            // if the user dones't want all notices, skip returned notices
            if (!rptCse.isIncludeAllNotices() && nov.getReturnedTS() != null) {
                continue;
            }
            noticeList.add(nov);
        }
        rptCse.setNoticeListForReport(noticeList);
        
        // convert columns to CSS grid size
        rptCse.setCeCasePhotoPoolGridSquares(report_ceCase_setGridSquaresFromColCount(rptCse.getCeCasePhotoPoolColumnCount()));
        rptCse.setViolationPhotoGridSquares(report_ceCase_setGridSquaresFromColCount(rptCse.getViolationPhotoColumnCount()));
        rptCse.setCearPhotoGridSquares(report_ceCase_setGridSquaresFromColCount(rptCse.getCearColumnCount()));
        
        return rptCse;
    }
    
    /**
     * Translates column counts to grid squares for CSS display (divides 12 by col count)
     */
    private int report_ceCase_setGridSquaresFromColCount(int colCount){
        switch(colCount){
            case 1: 
                return GRIDSQAURES_COLS_1;
            case 2:
                return GRIDSQAURES_COLS_2;
            case 3:
                return GRIDSQAURES_COLS_3;
            case 4:
                return GRIDSQAURES_COLS_4;
            default:
                return GRIDSQAURES_COLS_2;
        }
    }

    // *************************************************************************
    // *                     PUBLIC FACING                                     *
    // *************************************************************************
    /**
     * Called by the PIBCECaseBB when a public user wishes to add an event to
     * the case they are viewing online. This method stitches together the
     * message text, messenger name, and messenger phone number before passing
     * the info back to the EventCoordinator
     *
     * @param caseID can be extracted from the public info bundle
     * @param msg the text of the message the user wants to add to the case
     * @param messagerName the first and last name of the person submitting the
     * message Note that this submission info is not YET wired into the actual
     * Person objects in the system.
     *
     * @param messagerPhone a simple String rendering of whatever the user types
     * in. Length validation only.
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.EventException
     */
    public void public_attachPublicMessage(int caseID, String msg, String messagerName, String messagerPhone) throws IntegrationException, BObStatusException, EventException {
        StringBuilder sb = new StringBuilder();
        sb.append("Case note added by ");
        sb.append(messagerName);
        sb.append(" with contact number: ");
        sb.append(messagerPhone);
        sb.append(": ");
        sb.append("<br /><br />");
        sb.append(msg);

        EventCoordinator ec = getEventCoordinator();
        ec.attachPublicMessagToCECase(caseID, sb.toString());

    }

    // *************************************************************************
    // *                     EVENTS                                            *
    // *************************************************************************
    /**
     * For inter-coordinator use only! Not called by backing beans. Primary
     * event life cycle control method which is called by EventCoordinator each
     * time an event is added to a code enf case. The primary business logic
     * related to which events can be attached to a case at any given case phase
     * is implemented in this coordinator method.
     *
     * Its core operation is to check case and event related qualities and
     * delegate further processing to event-type specific methods also found in
     * this coordinator
     *
     * @Deprecated the event addition chains were getting too rowdy
     * 
     * @param evList
     * @param cse the case to which the event should be added
     * @param ev the event to add to the case also included in this call
     * @return a reference to the same incoming List of EventCnF objects that is
     * passed in. Any additional events that need to be attached should be
     * appended to the end of this list
     *
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.ViolationException
     * @throws com.tcvcog.tcvce.domain.EventException
     */
    protected List<EventCnF> events_addEvent_processForCECaseDomain(
            List<EventCnF> evList,
            CECaseDataHeavy cse,
            EventCnF ev)
            throws BObStatusException,
            IntegrationException,
            ViolationException,
            EventException {
        EventType eventType = ev.getCategory().getEventType();
        EventIntegrator ei = getEventIntegrator();
        if (evList == null || cse == null || ev == null) {
            throw new BObStatusException("Null argument to addEvent_ceCaseDomain");
        }

        switch (eventType) {
            case Action:
                break;
            case Violation:
                // TODO: Finish me
                break;
            case Closing:
//                insertedEventID = processClosingEvent(cse, ev);
                break;
            default:
                ev.setCeCaseID(cse.getCaseID());
        } // close switch
        return evList;
    } // close method

  

    /**
     * Logic intermediary event for creating events documenting a CECase's
     * closure
     *
     * @param c
     * @param e
     * @return
     * @throws IntegrationException
     * @throws BObStatusException
     */
    private int events_processClosingEvent(CECaseDataHeavy c, EventCnF e, UserAuthorized ua) throws IntegrationException, BObStatusException {
        EventIntegrator ei = getEventIntegrator();

        CasePhaseEnum closedPhase = CasePhaseEnum.Closed;
//        c.setCasePhase(closedPhase);

        if (c.getClosingDate() == null) {
            c.setClosingDate(LocalDateTime.now());
            cecase_updateCECaseMetadata(c, ua);
        }
        // now load up the closing event before inserting it
        // we'll probably want to get this text from a resource file instead of
        // hardcoding it down here in the Java
        e.setCreatedBy(getSessionBean().getSessUser());
        e.appendToDescription(getResourceBundle(Constants.MESSAGE_TEXT).getString("automaticClosingEventDescription"));
        e.setNotes(getResourceBundle(Constants.MESSAGE_TEXT).getString("automaticClosingEventNotes"));
        return ei.insertEvent(e);
    }

    // *************************************************************************
    // *                     NOTICES OF VIOLATION                              *
    // *                     TEMPLATES                                         *
    // *************************************************************************
    
    
    /**
     * Factory for NOV templates
     * @return 
     */
    public NoticeOfViolationTemplate nov_getNoticeOfViolationSkeleton(){
        return new NoticeOfViolationTemplate();
    }
    
    
    /**
     * Retrieves an NOV skeleton
     * @param tempID
     * @return
     * @throws BObStatusException
     * @throws IntegrationException
     * @throws BlobException 
     */
    public NoticeOfViolationTemplate nov_getNoticeOfViolationTemplate(int tempID) throws BObStatusException, IntegrationException, BlobException{
        if(tempID == 0){
            throw new BObStatusException("Cannot get template by ID with ID == 0");
        }
        CaseIntegrator ci = getCaseIntegrator();
        
        NoticeOfViolationTemplate plate = ci.getNoticeOfViolationTemplate(tempID);
        
        return plate;
    }
    
    /**
     * Logic gate for inserting an NOV template
     * @param plate
     * @param ua
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public int nov_insertNoticeOfViolationTemplate(NoticeOfViolationTemplate plate, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(plate == null || ua == null){
            throw new BObStatusException("Cannot insert null notice or with null user");
        }
        CaseIntegrator ci = getCaseIntegrator();
        return ci.insertNoticeOfViolationTemplate(plate);
    }
    
    /**
     * Updates an NOV template
     * @param plate
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void nov_updateNoticeOfViolationTemplate(NoticeOfViolationTemplate plate, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(plate == null || ua == null){
            throw new BObStatusException("Cannot insert null notice or with null user");
        }
        CaseIntegrator ci = getCaseIntegrator();
        
        ci.updateNoticeOfViolationTemplate(plate);
        ceCaseCacheManager.flushAllCaches();
        
    }
    
    
    /**
     * Builds a list of available templates by muni
     * @param muni all munis returned if null
     * @return the active templates
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.BlobException
     */
    public List<NoticeOfViolationTemplate> nov_getNoticeOfViolationTemplateList(Municipality muni) throws IntegrationException, BObStatusException, BlobException{
        CaseIntegrator ci = getCaseIntegrator();
        List<NoticeOfViolationTemplate> plateList = new ArrayList<>();
        List<Integer> idList = ci.getNOVTemplates(muni);
        
        if(idList != null && !idList.isEmpty()){
            for(int id: idList){
                plateList.add(nov_getNoticeOfViolationTemplate(id));
            }
        }
        
        return plateList;
    }
    
    
    /**
     * Writes the deac TS to a NOV template
     * @param plate
     * @param ua
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public void nov_onTemplateDeac(NoticeOfViolationTemplate plate, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(plate != null){
            plate.setDeactivatedTS(LocalDateTime.now());
            nov_updateNoticeOfViolationTemplate(plate, ua);
        }
    }

            
    
    // *************************************************************************
    // *                     NOTICES OF VIOLATION                              *
    // *                     GENERAL                                           *
    // *************************************************************************
     

    /**
     * Sets mailing fields to null] Params changed to take in UserAuthorized
     * during corruption recovery
     *
     * @param nov
     * @param csedh
     * @param user
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public void nov_ResetMailing(NoticeOfViolation nov, CECaseDataHeavy csedh, UserAuthorized user) throws IntegrationException, AuthorizationException, BObStatusException {
        CaseIntegrator ci = getCaseIntegrator();
        EventCoordinator ec = getEventCoordinator();
        if (permissionsCheckpointNOVFinalize(user)) {
            ci.novResetMailingFieldsToNull(nov);
            ec.deactivateAllEventsLinkedToEventLinkedObject(nov, csedh, true, false, user);
            ceCaseCacheManager.flush(nov);
        } else {
            throw new AuthorizationException("User does not have sufficient acces righst to clear notice mailing fields");
        }
    }

    /**
     * Configures a notice of violation, which was adjusted in OCT 2023
     * to use a dedicated NoticeOfViolationTemplate object instead of 
     * the dual use TextBlock mess.
     * 
     * @param noticeID
     * @return
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public NoticeOfViolation nov_getNoticeOfViolation(int noticeID)
            throws IntegrationException, BObStatusException, BlobException {
        EventCoordinator ec = getEventCoordinator();
        CaseIntegrator ci = getCaseIntegrator();
        NoticeOfViolation nov = ci.novGet(noticeID);
        if (nov != null) {
            if (nov.getNotifyingOfficer() == null && nov.getCreationBy() != null) {
                nov.setNotifyingOfficer(nov.getCreationBy());
            }
            // check for legacy letters that need a special place holder 
            // template to store its legacy type
            if(nov.getNoticeTypeLegacyID() != 0){
                nov.setTemplate(nov_getPlaceholderTemplateForType(nov.getNoticeTypeLegacyID()));
            } else if(nov.getTemplate() == null){
                System.out.println("CaseCoordinator.nov_getNoticeOfViolation | NOV ID " + nov.getNoticeID() + " has no legacy type nor new template ID");
            }
            
            // This will set all the connected events to this NOV
            ec.configureEventLinkedLinkedEventList(nov);
        }

        return nov;
    }
    
    /**
     * For reverse compat this method will look up the NOV type by ID 
     * and inject it into the template
     * @param typeID
     * @return 
     */
    private NoticeOfViolationTemplate nov_getPlaceholderTemplateForType(int typeID) throws IntegrationException, IntegrationException, BlobException{
        NoticeOfViolationTemplate plate = new NoticeOfViolationTemplate();
        if(typeID != 0){
            plate.setTemplateType(nov_getNOVType(typeID));
        }
        plate.setTemplateID(PLACEHOLDER_TEMPLATEID);
        return plate;
                
    }

    /**
     * Utility method for extracting a list of NOV ID's from the db
     *
     * @param idl
     * @return
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.BlobException
     */
    public List<NoticeOfViolation> nov_getNoticeOfViolationList(List<Integer> idl)
            throws IntegrationException, BObStatusException, BlobException {
        List<NoticeOfViolation> novl = new ArrayList<>();
        if (idl != null && !idl.isEmpty()) {
            for (Integer i : idl) {
                novl.add(nov_getNoticeOfViolation(i));
            }
        }
        return novl;
    }

    /**
     * Called when first creating a notice of violation
     *
     * @param cse
     * @param mdh
     * @param ua
     * @return
     * @throws AuthorizationException
     */
    public NoticeOfViolation nov_GetNewNOVSkeleton(CECaseDataHeavy cse,  UserAuthorized ua) throws AuthorizationException {
        NoticeOfViolation nov = new NoticeOfViolation();

        nov.setViolationList(new ArrayList<>());
        nov.setDateOfRecord(LocalDateTime.now());
        
        // loop over unresolved violations on case and generate CodeViolationDisplayable obects
        Iterator<CodeViolation> iter = cse.getViolationListUnresolved().iterator();
        while (iter.hasNext()) {
            CodeViolation cv = iter.next();
            CodeViolationDisplayable cvd = new CodeViolationDisplayable(cv);
            // start with sensible default values
            cvd.setIncludeHumanFriendlyText(false);
            cvd.setIncludeOrdinanceText(true);
            cvd.setIncludeViolationPhotos(false);
            nov.getViolationList().add(cvd);
        }

        nov.setCreationBy(ua);
        nov.setNotifyingOfficer(ua);

        return nov;
    }

    /**
     * NOV is ready to send - And coordinate creating an event to document this
     *
     * @param c
     * @param nov
     * @param ua
     * @throws BObStatusException
     * @throws IntegrationException
     * @throws EventException
     * @throws ViolationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void nov_LockAndQueue(CECaseDataHeavy c,
            NoticeOfViolation nov,
            UserAuthorized ua)
            throws BObStatusException, IntegrationException, EventException, ViolationException, AuthorizationException {

        PersonCoordinator pc = getPersonCoordinator();
        
        if (c == null || nov == null || ua == null || nov.getRecipient() == null || nov.getRecipientMailingAddress() == null) {
            throw new BObStatusException("Cannot lock notice with null case, nov, or recipient Person or mailing or user");
        }
        if (nov.getRecipient() == null || nov.getRecipientMailingAddress() == null) {
            throw new BObStatusException("Cannot lock notice: Recipient is null or recipient has no mailing address. ");
        }
        
        if(nov.getNotifyingOfficerPerson() == null && nov.getNotifyingOfficer() != null){
            nov.setNotifyingOfficerPerson(pc.getPersonByHumanID(nov.getNotifyingOfficer().getHumanID()));
        } 
        // an exception used to thrown in an else here which didn't make any sense and was
        // removed during a debugging session on 5 December 2023
        // ECD is unsure how NOVs were ever getting finalized with such an else clause in place!!????

        if(nov.getNotifyingOfficerPerson() != null){
            if (nov.getNotifyingOfficerPerson().getEmailList() == null
                    || nov.getNotifyingOfficerPerson().getEmailList().isEmpty()
                    || nov.getNotifyingOfficerPerson().getPhoneList() == null
                    || nov.getNotifyingOfficerPerson().getPhoneList().isEmpty()) {
                throw new BObStatusException("Cannot lock notice with null notifying officer person or empty phone or empty email list inside said person");
            }
        }
        
        if(!permissionsCheckpointNOVFinalize(ua)){
            throw new AuthorizationException("Authorization Exception: Current user does not have permissions to lock and Queue this notice"); 
        }

        CaseIntegrator ci = getCaseIntegrator();

        if (nov.getLockedAndqueuedTS() == null) {
            nov.setLockedAndqueuedTS(LocalDateTime.now());
            nov.setLockedAndQueuedBy(ua);

            // Pull out values of chosen Human and associated mailing address 
            // and inject into permament NOV fields for record keeping
            nov.setFixedAddrXferTS(LocalDateTime.now());
            nov.setFixedRecipientName(nov.getRecipient().getName());

            if(c.getPropertyUnitID() != 0){
                nov.setFixedRecipientUnitNumber(c.getPropertyUnitNumberFlat());
            }
            
            // LEGACY field-level fixed MAD writing -- replaced by a single field write from the mad link pretty two line
            nov.setFixedRecipientBldgNo(nov.getRecipientMailingAddress().getBuildingNo());
            nov.setFixedRecipientStreet(nov.getRecipientMailingAddress().getStreet().getName());
            nov.setFixedRecipientCity(nov.getRecipientMailingAddress().getStreet().getCityStateZip().getCity());
            nov.setFixedRecipientState(nov.getRecipientMailingAddress().getStreet().getCityStateZip().getState());
            nov.setFixedRecipientZip(nov.getRecipientMailingAddress().getStreet().getCityStateZip().getZipCode());

            // modern approach as of 1-MARCH-2024
            nov.setFixedRecipientMailingAddressCombinedEscapeFalse(nov.getRecipientMailingAddress().getAddressPretty2LineEscapeFalse());
            
            nov.setFixedNotifyingOfficerName(nov.getNotifyingOfficerPerson().getName());
            nov.setFixedNotifyingOfficerTitle(nov.getNotifyingOfficerPerson().getJobTitle());
            nov.setFixedNotifyingOfficerPhone(nov.getNotifyingOfficerPerson().getPhoneList().get(0).getPhoneNumber());
            nov.setFixedNotifyingOfficerEmail(nov.getNotifyingOfficerPerson().getEmailList().get(0).getEmailaddress());
            nov.setFixedNotifyingOfficerSignaturePhotoDocID(nov.getNotifyingOfficer().getSignatureBlobID());

            ci.novLockAndQueueForMailing(nov);

            System.out.println("CaseCoordinator.novLockAndQueue | NOV locked in integrator: complete");

        } else {
            throw new BObStatusException("Notice is already locked and queued for sending");
        }
        ceCaseCacheManager.flush(nov);
    }

    /**
     * Create a new text block skeleton with injectrable set to true
     *
     * @param muni
     * @return
     */
    public TextBlock nov_getTemplateBlockSekeleton(Municipality muni) {
        TextBlock tb = new TextBlock();
        tb.setMuni(muni);
        tb.setInjectableTemplate(true);
//        tb.setTextBlockCategoryID(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
//                .getString("nov_textblocktemplate_categoryid")));
        tb.setTextBlockText("inject violations with: ***VIOLATIONS***");

        return tb;

    }
    
    /**
     * Extracts all available NOV Types by Muni
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public List<NoticeOfViolationType> nov_getNOVTypeList() throws IntegrationException, BlobException{
        CaseIntegrator ci = getCaseIntegrator();
        return nov_getNOVTypeListByID(ci.novGetTypeList());
    }
    
    /**
     * Basic getter for NOV types
     * @param typeID
     * @return
     * @throws IntegrationException 
     */
    public NoticeOfViolationType nov_getNOVType(int typeID) throws IntegrationException, BlobException{
        CaseIntegrator ci = getCaseIntegrator();
        if(typeID == 0){
            return null;
        }
        return ci.novGetType(typeID);
        
    }
    
    /**
     * Internal organ for getting NOVTypes by a list of IDs
     * @param idl
     * @return
     * @throws IntegrationException 
     */
    public List<NoticeOfViolationType> nov_getNOVTypeListByID(List<Integer> idl) throws IntegrationException, BlobException{
        List<NoticeOfViolationType> typeList = new ArrayList<>();
        if(idl != null && !idl.isEmpty()){
            CaseIntegrator ci = getCaseIntegrator();
            for(Integer i: idl){
                typeList.add(ci.novGetType(i));
            }
        }
        return typeList;
    }

    /**
     * Called when the NOV is ready to get written to the DB --but before
     * queuing for sending
     *
     * @param nov
     * @param cse
     * @param usr
     * @return
     * @throws IntegrationException
     */
    public int nov_InsertNotice(NoticeOfViolation nov, CECaseDataHeavy cse, User usr) throws IntegrationException, BObStatusException {
        if(nov == null || cse == null || usr == null){
            throw new BObStatusException("cannot insert notice with null notice, notice type, case, or user ");
        }
        if(nov.getTemplate() == null){
            throw new BObStatusException("cannot insert notice with null notice template");
        }
        CaseIntegrator ci = getCaseIntegrator();
        nov.setCreationBy(usr);
        
        System.out.println("CaseCoordinator.novInsertNotice");
        int freshid = ci.novInsert(cse, nov);
        ceCaseCacheManager.flush(cse);
        return freshid;
    }

    /**
     * Logic pass-through for NOV updates
     *
     * @param nov
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public void nov_update(NoticeOfViolation nov) throws IntegrationException, BObStatusException {
        CaseIntegrator ci = getCaseIntegrator();
        if (nov != null) {
            if (nov.getLockedAndqueuedTS() == null) {
                ci.novUpdateNotice(nov);
                ceCaseCacheManager.flush(nov);
            } else {
                throw new BObStatusException("Cannot update a locked notice.");
            }
        }
    }

    /**
     * Business logic intermediary method for marking a NOV as having already
     * been sent to the property and sets up the follow-up event
     *
     * @param ceCase
     * @param nov
     * @param user
     * @throws BObStatusException
     * @throws EventException
     * @throws IntegrationException
     */
    public void nov_markAsSentAndLogSentAndFollowupEvents(CECaseDataHeavy ceCase, NoticeOfViolation nov, UserAuthorized user) throws BObStatusException, EventException, IntegrationException, AuthorizationException {

        if(ceCase == null ||  nov == null || user == null){
            throw new BObStatusException("Cannot mark NOV as sent with null case or nov or user");
        }
        CaseIntegrator ci = getCaseIntegrator();

        nov.setSentTS(LocalDateTime.now());
        nov.setSentBy(user);
        ci.novRecordMailing(nov);

        nov_logMailingAndFollowupEvents(ceCase, nov, user);
        
        ceCaseCacheManager.flush(nov);
        
        // turn off this cesspool of event code
//        ec.processEventEmitter(nov, EventEmissionEnum.NOTICE_OF_VIOLATION_SENT, user, ceCase);
//        nov_logNOVSentEvent(ceCase, nov, user);
        // revert back to basic stuff
        
    }
    
    /**
     * LOgic container for actually setting up and writing the NOV related events to the
     * appropriate tables
     *
     * @param ceCase
     * @param nov
     * @param user
     * @throws BObStatusException
     * @throws EventException
     * @throws IntegrationException 
     */
    private void nov_logMailingAndFollowupEvents(CECaseDataHeavy ceCase, NoticeOfViolation nov, UserAuthorized user) throws BObStatusException, EventException, IntegrationException, AuthorizationException{
        EventCoordinator ec = getEventCoordinator();
        if(nov.getTemplate() != null && nov.getTemplate().getTemplateType() != null){
            if(nov.getTemplate().getTemplateType().getEventCatSent() != null){
                EventCnF novMailedEvent = ec.initEvent(ceCase, nov.getTemplate().getTemplateType().getEventCatSent());
                novMailedEvent.setTimeStart(LocalDateTime.now());
                novMailedEvent.setTimeEnd(novMailedEvent.getTimeStart().plusMinutes(nov.getTemplate().getTemplateType().getEventCatSent().getDefaultDurationMins()));
                novMailedEvent.appendToDescription(nov.getTemplate().getTemplateType().getEventCatSent().getHostEventDescriptionSuggestedText());
                // this actually does the event write to the event table
                ec.addEvent(novMailedEvent, ceCase, user);
            } else {
                System.out.println("CaseCoordinator.nov_markAsSent | no event cat for sent in type");
            }
            // SEPT 2024: replaced by action due by date on NOV which propogates into the cecase
//            
            if(nov.getTemplate().getTemplateType().getEventCatFollowUp() != null){
                EventCnF novFollowupEvent = ec.initEvent(ceCase, nov.getTemplate().getTemplateType().getEventCatFollowUp() );
                novFollowupEvent.setTimeStart(LocalDateTime.now().plusDays(nov.getFollowupEventDaysRequest()));
                novFollowupEvent.setTimeEnd(novFollowupEvent.getTimeStart().plusMinutes(nov.getTemplate().getTemplateType().getEventCatFollowUp().getDefaultDurationMins()));
                novFollowupEvent.appendToDescription(nov.getTemplate().getTemplateType().getEventCatFollowUp().getHostEventDescriptionSuggestedText());
                // this actually does the event write to the event table
                List<EventCnF> freshEvList = ec.addEvent(novFollowupEvent, ceCase, user);
                if(freshEvList != null && !freshEvList.isEmpty()){
                    if(freshEvList.size() == 1){
                        EventCnF followupEv = freshEvList.get(0);
                        ec.linkEventToEventLinked(followupEv, nov);
                    } else {
                        System.out.println("CaseCoordinator.nov_logMailingAndFollowupEvents | detected more than one follow-up event! Skipping linking");
                    }
                }
            } else {
                System.out.println("CaseCoordinator.nov_markAsSent | no event cat for followup in type");
            }
            ceCaseCacheManager.flush(nov);
        }
    }
    
    /**
     * Used during the automated one-click case feature to setup recipient and mad link 
     * based on sensible defaults
     * @param pdh
     * @param nov
     * @return
     * @throws BObStatusException 
     */
    private NoticeOfViolation nov_injectSensibleRecipientAndMAD(PropertyDataHeavy pdh, NoticeOfViolation nov) throws BObStatusException, IntegrationException{
        if(pdh == null || nov == null){
            throw new BObStatusException("Cannot inject persons and MADs with null inputs");
        }
        SystemCoordinator sc = getSystemCoordinator();
            
        // load up our target roles and our fallback roles
        LinkedObjectRole recipientLORFirstChoice = sc.getLinkedObjectRole(
                Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("nov_recipient_firstchoice_lorid")));
        LinkedObjectRole recipientLORFallback = sc.getLinkedObjectRole(
                Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("nov_recipient_fallback_lorid")));
        LinkedObjectRole madLORFirstChoice = sc.getLinkedObjectRole(
                Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("nov_madlink_firstchoice_lorid")));
        LinkedObjectRole madLORFallback = sc.getLinkedObjectRole(
                Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("nov_madlink_fallback_lorid")));

        StringBuilder sb = new StringBuilder("One-Click NOV config log: <br>");
        
        Person recipient = nov_filterHumanLinkList(pdh.gethumanLinkList(), recipientLORFirstChoice);
        // logging
        if(recipient != null && recipientLORFirstChoice != null){
            sb.append("Found first choice recipient role: ");
            sb.append(recipientLORFirstChoice.getTitle());
            sb.append("<br>");
        }
        if(recipient == null){
            recipient = nov_filterHumanLinkList(pdh.gethumanLinkList(), recipientLORFallback);
            // logging
            if(recipient != null && recipientLORFallback != null){
                sb.append("Found fallback choice recipient role: ");
                sb.append(recipientLORFallback.getTitle());
                sb.append("<br>");
            }
        }
        // if we don't have primary or fall back recipient, just grab the first person in the human link list
        if(recipient == null && pdh.gethumanLinkList() != null && !pdh.gethumanLinkList().isEmpty()){
            recipient = pdh.gethumanLinkList().get(0);
            sb.append("Last resort recipient: first person in the link list: ");
            sb.append(recipient.getName());
            sb.append("<br>");
        }
        
        MailingAddressLink madLink = nov_filterMADLinkList(pdh.getMailingAddressLinkList(), madLORFirstChoice);
        // logging
        if(madLink != null && madLORFirstChoice != null){
            sb.append("Found primary mailing address link: ");
            sb.append(madLORFirstChoice.getTitle());
            sb.append("<br>");
        }
        if(madLink == null){
            madLink = nov_filterMADLinkList(pdh.getMailingAddressLinkList(), madLORFallback);
            // logging
            if(madLink != null && madLORFallback != null){
                sb.append("Found fall back mailing address link: ");
                sb.append(madLORFallback.getTitle());
                sb.append("<br>");
            }
        }
        // if we don't have first choice role or second, then just use the first one we've got in the list
        if(madLink == null){
            madLink = pdh.getPrimaryAddressLink();
        }
        if(recipient != null && madLink != null){
            sb.append("Found both a recipient and an address: proceeding with mailing!");
            sb.append("<br>");
            nov.setRecipient(recipient);
            nov.setRecipientMailingAddress(madLink);
        } else {
            sb.append("NOV auto config failure: throwing exception and exiting");
            System.out.println("CaseCoordinator.nov_injectSensibleRecipientAndMAD | Config dump:");
            System.out.println(sb.toString());
            throw new BObStatusException("Could not find recipient and mailing address on property, even with fallback roles");
        }
        nov.setNotes(sb.toString());
        return nov;
    }
    
    /**
     * Convenience method for looking for a human link of the given role
     * @param links
     * @param lor
     * @return 
     */
    private HumanLink nov_filterHumanLinkList(List<HumanLink> links, LinkedObjectRole lor){
        if(links != null && !links.isEmpty()){
            for(HumanLink link: links){
                if(link.getLinkedObjectRole().equals(lor)){
                    return link;
                }
            }
        } else {
            return null;
        }
        return null;
    }
    
    /** 
     * Convenience looper over madlinks looking for target role
     * @param links
     * @param lor
     * @return 
     */
    private MailingAddressLink nov_filterMADLinkList(List<MailingAddressLink> links, LinkedObjectRole lor){
        if(links != null && !links.isEmpty()){
            for(MailingAddressLink link: links){
                if(link.getLinkedObjectRole().equals(lor)){
                    return link;
                }
            }
        } else {
            return null;
        }
        return null;
    }
    

    /**
     * Logic container for configuring a NOV as returned by the postal service
     *
     * @param c
     * @param nov
     * @param user
     * @throws IntegrationException
     * @throws BObStatusException
     */
    public void nov_markAsReturned(CECaseDataHeavy c, NoticeOfViolation nov, User user) throws IntegrationException, BObStatusException {
        CaseIntegrator ci = getCaseIntegrator();
        nov.setReturnedTS(LocalDateTime.now());
        nov.setReturnedBy(user);
        ci.novRecordReturnedNotice(nov);
        ceCaseCacheManager.flush(nov);
    }

    /**
     * Checks logic before deleting NOVs.It's rare to be able to delete
 something but an unmailed NOV is a very low impact loss
     *
     * @param nov
     * @param csedh
     * @param ua
     * @throws BObStatusException
     */
    public void nov_deactivate(NoticeOfViolation nov, CECaseDataHeavy csedh, UserAuthorized ua) throws BObStatusException, AuthorizationException {
        CaseIntegrator ci = getCaseIntegrator();
        EventCoordinator ec = getEventCoordinator();
        if(nov == null || ua == null){
            throw new BObStatusException("Cannot deactivate an NOV given a null notice of user");
            
        }
        //cannot delete a letter that was already sent
        if (nov.getSentTS() != null && !ua.getKeyCard().isHasSysAdminPermissions()) {
            throw new BObStatusException("Cannot delete a letter that has been sent");
        } else {
            try {
                if(!permissionsCheckpointNOVFinalize(ua)){
                    throw new AuthorizationException("Authorization Exception: current user does not have permissions to deactivate an NOV");        
                }
                ci.novDeactivate(nov);
                ec.deactivateAllEventsLinkedToEventLinkedObject(nov, csedh, true, false, ua);
                ceCaseCacheManager.flush(nov);
            } catch (IntegrationException ex) {
                System.out.println(ex);
                getFacesContext().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Unable to delete notice of violation due to a database error", ""));
            }
        }
    }
    
    /**
     * Factory for NOVViolationType objects (e.g. Notice of Violation, Request for Compliance)
     * @return 
     */
    public NoticeOfViolationType nov_getNoticeOfViolationTypeSkeleton(){
        return new NoticeOfViolationType();
    }
   
    /**
     * Tool for building an NOV from a template block.
     * Does not DO ANY DB writes. Client must manage writing after user
     * confirmation
     *
     * @param nov
     * @param plate
     * @param cse
     * @return
     * @throws BObStatusException
     */
    public NoticeOfViolation nov_assembleNOVFromTemplate(NoticeOfViolation nov, NoticeOfViolationTemplate plate, CECase cse) throws BObStatusException {

        if (nov == null || plate == null || cse == null) {
            throw new BObStatusException("Cannot assemble a notice from template with null NOV, template or case");
        }
        // Inject the template, which all importantly contains the type
        nov.setTemplate(plate);
        String template = plate.getTemplateTextEscapeFalse();
        int startOfInjectionPoint = template.indexOf(Constants.NOV_VIOLATIONS_INJECTION_POINT);
        System.out.println("CaseCoor.nov_assembleNOVFromTemplate: Injection point found starts at: " + startOfInjectionPoint);
        // If the injection point is not found, put the entire template block as text before Violations
        if (startOfInjectionPoint != -1) {
            nov.setNoticeTextBeforeViolations(template.substring(0, startOfInjectionPoint));
            nov.setNoticeTextAfterViolations(template.substring(startOfInjectionPoint + Constants.NOV_VIOLATIONS_INJECTION_POINT.length()));
        } else {
            nov.setNoticeTextBeforeViolations(template);
        }

        nov.setIncludeStipulatedCompDate(nov.getTemplate().getTemplateType().isIncludeStipCompDate());
        
        return nov;
    }

    /**
     * Updates only the notes field on Notice of Violation
     *
     * @param mbp
     * @param nov
     * @throws BObStatusException
     * @throws IntegrationException
     */
    public void nov_updateNotes(MessageBuilderParams mbp, NoticeOfViolation nov) throws BObStatusException, IntegrationException {
        CaseIntegrator ci = getCaseIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        if (nov == null || mbp == null) {
            throw new BObStatusException("Cannot append if notes, case, or user are null");
        }

        nov.setNotes(sc.appendNoteBlock(mbp));

        ci.novUpdateNotes(nov);
        ceCaseCacheManager.flush(nov);
    }
    
    /**
     * Inserts an NOV type
     * @param novt
     * @return
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public int nov_insertNOVType(NoticeOfViolationType novt) throws BObStatusException, IntegrationException{
        if(novt == null){
            throw new BObStatusException("Cannot insert new NOV Type with null NOV type");
            
        }
        CaseIntegrator ci = getCaseIntegrator();
        return ci.novInsertNOVType(novt);
    }
    
    /**
     * Updates an NOV type
     * @param novt
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public void nov_updateNOVType(NoticeOfViolationType novt) throws BObStatusException, IntegrationException{
        if(novt == null){
            throw new BObStatusException("Cannot update null NOV type");
        }
        CaseIntegrator ci = getCaseIntegrator();
        ci.novUpdateNOVType(novt);
        ceCaseCacheManager.flushAllCaches();
    }

    /**
     * Deactivation point for NOV types
     * @param novt 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void nov_deactivateNOVType(NoticeOfViolationType novt) throws BObStatusException, IntegrationException{
        if(novt == null){
            throw new BObStatusException("Cannot deactivate null NOV type");
        }
        CaseIntegrator ci = getCaseIntegrator();
        novt.setDeactivatedTS(LocalDateTime.now());
        ci.novUpdateNOVType(novt);
        ceCaseCacheManager.flushAllCaches();
        
    }
    
     
    // *************************************************************************
    // *                     CITATIONS                                         *
    // *************************************************************************


    /**
     * Extracts all court entities from DB
     *
     * @param requireActive: boolean, set to true to extract only active court entities
     * @return
     * @throws IntegrationException
     * @throws BObStatusException
     */
    public List<CourtEntity> getCourtEntityListComplete(boolean requireActive) throws IntegrationException, BObStatusException {
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        return cei.getCourtEntityList(requireActive);
    }

    /**
     * Extract single court entity by courtEntityID from DB
     *
     * @param courtEntityID
     * @return
     * @throws IntegrationException
     * @throws BObStatusException
     */
    public CourtEntity getCourtEntity(int courtEntityID) throws IntegrationException, BObStatusException {
        if (courtEntityID == 0) {
            throw new BObStatusException("cannot fetch a courtEntity of ID 0");
        }

        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        return cei.getCourtEntity(courtEntityID);
    }

    /**
     * Factory for Court Entity objects
     * @param umap
     * @return new Court Entity with id = 0
     */
    public CourtEntity getCourtEntitySkeleton(UserMuniAuthPeriod umap) {
        CourtEntity courtEntity = new CourtEntity();
        courtEntity.setCreatedby_UMAP(umap);
        courtEntity.setLastUpdatedby_UMAP(umap);
        return courtEntity;
    }

    /**
     * Insertion point for court entity
     *
     * @param courtEntity
     * @param umap
     * @return
     * @throws IntegrationException
     * @throws AuthorizationException
     * @throws BObStatusException
     */
    public CourtEntity insertCourtEntity(CourtEntity courtEntity, UserMuniAuthPeriod umap) throws IntegrationException, AuthorizationException, BObStatusException {
        if (Objects.isNull(courtEntity) || Objects.isNull(umap)) {
            throw new BObStatusException("Cannot insert courtEntity with null courtEntity or UMAP");
        }
        if (umap.getRole() != RoleType.SysAdmin) {
            throw new AuthorizationException("UMAP must be rank: Sys Admin to insert courtEntity");
        }
        courtEntity.setCreatedby_UMAP(umap);
        courtEntity.setLastUpdatedby_UMAP(umap);

        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        courtEntity.setCourtEntityID(cei.insertCourtEntity(courtEntity));
        return courtEntity;
    }

    /**
     * To update court entity
     *
     * @param courtEntity
     * @param umap
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws AuthorizationException
     */
    public void updateCourtEntity(CourtEntity courtEntity, UserMuniAuthPeriod umap) throws IntegrationException, BObStatusException, AuthorizationException {
        if (Objects.isNull(courtEntity) || Objects.isNull(umap)) {
            throw new BObStatusException("Cannot update courtEntity with null courtEntity or UMAP");
        }
        if (umap.getRole() != RoleType.SysAdmin) {
            throw new AuthorizationException("UMAP must be rank: Sys Admin to update courtEntity");
        }
        courtEntity.setLastUpdatedby_UMAP(umap);

        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        cei.updateCourtEntity(courtEntity);
        ceCaseCacheManager.flushAllCaches();
    }

    /**
     * To deactivate court entity
     *
     * @param courtEntity
     * @param umap
     * @throws BObStatusException
     * @throws AuthorizationException
     * @throws IntegrationException
     */
    public void deactivateCourtEntity(CourtEntity courtEntity, UserMuniAuthPeriod umap) throws BObStatusException, AuthorizationException, IntegrationException {
        if (Objects.isNull(courtEntity) || Objects.isNull(umap)) {
            throw new BObStatusException("Cannot deactivate courtEntity with null courtEntity or UMAP");
        }
        if (umap.getRole() != RoleType.SysAdmin) {
            throw new AuthorizationException("UMAP must be rank: Sys Admin to deactivate courtEntity");
        }
        courtEntity.setDeactivatedBy_UMAP(umap);
        SystemIntegrator si = getSystemIntegrator();
        si.deactivateUMAPTrackedEntity(courtEntity);
        ceCaseCacheManager.flushAllCaches();
    }
    
    
    /**
     * Logic container for assembling a list of citations by a CECase
     *
     * @param cse
     * @return fully-baked citation objects
     * @throws IntegrationException
     */
    public List<Citation> citation_getCitationList(CECase cse) throws IntegrationException, BObStatusException, BlobException {
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        List<Citation> citList = new ArrayList<>();
        if (cse != null) {
            List<Integer> citIDList = cei.getCitations(cse);
            if (citIDList != null && !citIDList.isEmpty()) {
                for (Integer i : citIDList) {
                    citList.add(citation_getCitation(i));
                }
            }
        }
        return citList;
    }

    /**
     * Logic passthrough for acquiring a list of all possible citation statuses
     * that can be attached to a CitationStatusLogEntry
     *
     * @param includeInactive
     * @return sorted status list
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public List<CitationStatus> citation_getCitationStatusList(boolean includeInactive) throws IntegrationException, BObStatusException {
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        List<CitationStatus> statusList = cei.getCitationStatusList(includeInactive);
        if (statusList != null && !statusList.isEmpty()) {
            Collections.sort(statusList);
        }
        return statusList;

    }

    /**
     * Logic pass through for citation filing types (e.g. private criminal
     * complaint vs. non-traffic)
     *
     * @return
     * @throws IntegrationException
     * @throws BObStatusException
     */
    public List<CitationFilingType> citation_getCitationFilingTypeList() throws IntegrationException, BObStatusException {
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        return cei.getCitationFilingTypes();

    }

    /**
     * Getter for Citation objects
     *
     * @param citationID
     * @return
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public Citation citation_getCitation(int citationID) throws IntegrationException, BObStatusException, BlobException {
        if (citationID == 0) {
            throw new BObStatusException("Cannot get citation with an ID of 0");
        }
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        Citation cit = cei.getCitation(citationID);

        return citation_configureCitation(cit);
    }

    /**
     * Logic method for getting citations all set up
     *
     * @param cit
     * @return
     */
    private Citation citation_configureCitation(Citation cit) throws IntegrationException, BObStatusException, BlobException {
        PersonCoordinator pc = getPersonCoordinator();
        BlobCoordinator bc = getBlobCoordinator();

        cit.setViolationList(citation_getCitationCodeViolationList(cit));
        
        if(cit.getViolationList() != null && !cit.getViolationList().isEmpty()){
            cit.setCecaseID(cit.getViolationList().get(0).getCeCaseID());
        }
        
        cit.setStatusLog(citation_getCitationStatusLog(cit));
        cit.sethumanLinkList(pc.getHumanLinkList(cit));
        cit.setDocketNos(citation_getCitationDocketRecordList(cit));
        cit.setBlobList(bc.getBlobLightList(cit));
        cit.setCitationPenaltyList(citation_getCitationPenaltyList(cit));


        return cit;

    }
    
    /**
     * Grabs all the citation violation links and configures them
     * @param cit
     * @return 
     */
    private List<CitationCodeViolationLink> citation_getCitationCodeViolationList(Citation cit) throws IntegrationException, BObStatusException{
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        
        List<CitationCodeViolationLink> cvlist = cei.getCodeViolationsByCitation(cit);
        if(cvlist != null && !cvlist.isEmpty()){
            for(CitationCodeViolationLink cvl: cvlist){
                cvl.setCitationPenaltyList(citation_getCitationPenaltyList(cvl));
            }
        }
        
        return cvlist;
        
    }
    

    /**
     * Initializes a Citation object with a default start status
     *
     * @param creator
     * @param cse
     * @param citingOfficer
     * @return
     * @throws BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public Citation citation_getCitationSkeleton(CECase cse, User creator, User citingOfficer) throws BObStatusException, IntegrationException {
        // turned off before we have a method that can check for a user's permissions
        // when they are NOT the session's authorized user
//        if (!creator.getKeyCard().isHasEnfOfficialPermissions()) {
//            throw new BObStatusException("Users must have enforcement office permissions to create a citation");
//        }
        Citation cit = new Citation();

        cit.setCreatedBy(creator);
        cit.setFilingOfficer(citingOfficer);
        cit.setCitationNo(generateInternalCitationID(cse));
        cit.setDateOfRecord(LocalDateTime.now());
        cit.setOrigin_courtentity(getSessionBean().getSessMuni().getCourtEntities().get(0));

        List<CodeViolation> cvlst = new ArrayList<>();

        // As long as the violation does not have a complianec date, add it to the list
        // for user approval
        if (cse.getViolationList() != null && !cse.getViolationList().isEmpty()) {
            for (CodeViolation v : cse.getViolationList()) {
                if (v.getActualComplianceDate() == null 
                        && v.getStipulatedComplianceDate().isBefore(LocalDateTime.now())
                        && v.getNullifiedTS() == null
                        && v.getTransferredTS() == null
                        && v.isActive()) {
                    cvlst.add(v);
                    System.out.println("CaseCoordinator.citation_getCitationSkeleton: Adding ViolID: " + v.getViolationID());
                }
            }
        }
        cit.setViolationList(buildCitationCodeViolationLinkSkeletonList(cvlst));
        
        return cit;
    }
  
   

    /**
     * Generates a temporary internal citation number using the format SEAN
     * GRAMZ prefers: CIT-BLDGNO-DATE
     *
     * @param cse
     * @return the proposed internal ID
     */
    private String generateInternalCitationID(CECase cse) throws BObStatusException, IntegrationException {
        if (cse == null) {
            throw new BObStatusException("cannot generate citation no with null case");
        }
        PropertyCoordinator pc = getPropertyCoordinator();

        Property p;
        try {
            p = pc.getProperty(cse.getParcelKey());
        } catch (AuthorizationException ex) {
            throw new BObStatusException(ex.getMessage());
        }
        StringBuilder sb = new StringBuilder();
        sb.append("PCC-");
        sb.append(p.getAddress().getBuildingNo());
        LocalDateTime ldt = LocalDateTime.now();
        sb.append("-");
        sb.append(ldt.getMonthValue());
        sb.append("-");
        sb.append(ldt.getDayOfMonth());
        sb.append("-");
        sb.append(ldt.getYear());
        return sb.toString();

    }

    /**
     * Convenience method for injecting a code violation into a
     * CitationCodeViolation for skeletons to play with in their bellies
     *
     * @param cvl
     * @return
     */
    private List<CitationCodeViolationLink> buildCitationCodeViolationLinkSkeletonList(List<CodeViolation> cvl) {
        List<CitationCodeViolationLink> ccvl = new ArrayList<>();
        if (cvl != null) {
            for (CodeViolation cv : cvl) {
                ccvl.add(new CitationCodeViolationLink(cv));
            }
        }

        return ccvl;

    }
    
    
    /**
     * Builds an update bundle of citation-violation links from the given citation
     * @param cit
     * @param stat
     * @param ua
     * @return 
     */
    public CitationCodeViolationLinkUpdateBundle citation_buildCitationViolationBatchUpdateBundle(Citation cit, CitationViolationStatusEnum stat, UserAuthorized ua) throws BObStatusException{
        if(cit == null || stat == null || ua == null){
            throw new BObStatusException("Cannot create bundle with null citation, status, or user");
        }
        Map<Boolean, List<CitationCodeViolationLink>> terminalMap = cit.getViolationList().stream()
                .collect(Collectors.partitioningBy(link -> link.getCitVStatus() != null && link.getCitVStatus().isTerminalStatus()));
        
        List<CitationCodeViolationLinkUpdateStatus> updatableLinkList = new ArrayList<>();
        // build our wrapper classes so the user can see both current status and status to update
        for(CitationCodeViolationLink cvlink: terminalMap.get(false)){
            updatableLinkList.add(new CitationCodeViolationLinkUpdateStatus(cvlink));
        }
        
        List<CitationCodeViolationLinkUpdateStatus> noUpdateList = new ArrayList<>();
        for(CitationCodeViolationLink cvlink: terminalMap.get(true)){
            noUpdateList.add(new CitationCodeViolationLinkUpdateStatus(cvlink));
        }
        
        CitationCodeViolationLinkUpdateBundle bundle = new  CitationCodeViolationLinkUpdateBundle(
                                                                cit, 
                                                                noUpdateList,
                                                                updatableLinkList
                                                                );
        bundle.registerBatchStatus(stat);
        bundle.setRequestingUser(ua);
        return bundle;
    }
    
    /**
     * Primary processing step for citation violation update bundles
     * @param bundle
     * @return 
     */
    public CitationCodeViolationLinkUpdateBundle citation_processCitationViolationBatchUpdateBundle(CitationCodeViolationLinkUpdateBundle bundle) throws BObStatusException, AuthorizationException, IntegrationException, BlobException{
        if(bundle == null   || bundle.getParentCitation() == null 
                            || bundle.getRequestingUser() == null
                            || bundle.getBatchedStatusRequest() == null
                            || bundle.getCvLinksToUpdate() == null){
            throw new BObStatusException("Cannot process citation violation bundle with null bundle, citation, user, batch status, or link list");
        }
        
        if(!permissionsCheckpointCitationUpdate(bundle.getRequestingUser())){
            throw new AuthorizationException("user lacks permission to update citation");
        }
        
        StringBuilder sb = new StringBuilder();
        sb.append("Part of batch update incident to logging citation status to ").append(bundle.getBatchedStatusRequest().getLabel());
        for(CitationCodeViolationLinkUpdateStatus cvlink: bundle.getCvLinksToUpdate()){
            
            citation_updateCitationCodeViolationLink(cvlink.extractUpdatedCVLink(), sb.toString(), bundle.getRequestingUser());
            ceCaseCacheManager.flushObjectFromCache(cvlink.getCcvLink().getViolationID());
            ceCaseCacheManager.flushObjectFromCache(cvlink.getCcvLink().getCitationID());
        }
        
        return bundle;
    }

    /**
     * Logic intermediary for recording that a Citation has been issued, meaning
     * sent to the proper magisterial authority
     *
     * @param c
     * @param ua
     * @return
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public int citation_insertCitation(Citation c,
            UserAuthorized ua)
            throws  IntegrationException,
                    BObStatusException,
                    AuthorizationException {
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        int freshID = 0;
        if (c == null
                || ua == null
                || c.getFilingOfficer() == null
                || c.getViolationList() == null) {
            throw new BObStatusException("Cannot issue citation with null citation, creator, issuing officer, or violation list.");
        }
        if (c.getViolationList().isEmpty()) {
            throw new BObStatusException("Citations must cite  at least one code violation;");
        }
        
        if(!permissionsCheckpointCitationOpenClose(ua)){
            throw new AuthorizationException("Authorization exception: Muni profile " + ua.getGoverningMuniProfile().getProfileID() 
                    + " requires manager rank to insert a citation and user " + ua.getUserHuman() + " is role " + ua.getKeyCard().getGoverningAuthPeriod().getRole().getLabel());
        }

        c.setCreatedBy(ua);
        c.setCreatedTS(LocalDateTime.now());
        c.setLastUpdatedBy(ua);
        c.setLastUpdatedTS(LocalDateTime.now());

        // Create CitationCodeViolationLink objects to wrap each incoming CodeViolation
        // with metadata
        if (c.getViolationList() != null && !c.getViolationList().isEmpty()) {
            for (CitationCodeViolationLink cv : c.getViolationList()) {
                cv.setLinkCreatedByUserID(ua.getUserID());
                cv.setLinkLastUpdatedByUserID(ua.getUserID());
                CitationViolationStatusEnum cvse = getDefaultCitationViolationStatusEnumVal();
                cv.setCitVStatus(cvse);
                cv.setLinkSource(sc.getBObSource(Integer.parseInt( 
                        getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("bobsource_internalhuman"))));
            }
        }
        freshID = cei.insertCitation(c);
        c.setCitationID(freshID);
        cei.insertCitationCodeViolationLink(c);
        ceCaseCacheManager.flushObjectFromCache(c.getCecaseID());
        return freshID;

    }

    /**
     * Logic container for returning the default status of a code violation
     * attached to a single citation
     *
     * @return
     */
    private CitationViolationStatusEnum getDefaultCitationViolationStatusEnumVal() {
        return CitationViolationStatusEnum.AWAITING_PLEA;

    }

    /* **********************************************
    /* ********** CITATION STATUS LOGS **************
    /* **********************************************
    
    
    
    /**
     * Factory method for citation status log entry objects
     * @return the factoried object
     */
    public CitationStatusLogEntry citation_getStatusLogEntrySkeleton(Citation cit) {
        CitationStatusLogEntry csle = new CitationStatusLogEntry();

        csle.setDateOfRecord(LocalDateTime.now());
        if (cit != null && cit.getOrigin_courtentity() != null) {
            csle.setCourtEntity(cit.getOrigin_courtentity());
        }
        return csle;
    }

    /**
     * Builds a complete citation status log
     *
     * @param cit
     * @return
     */
    public List<CitationStatusLogEntry> citation_getCitationStatusLog(Citation cit) throws BObStatusException, IntegrationException {
        if (cit == null) {
            throw new BObStatusException("Cannot build citationlog with null citation");
        }

        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        List<Integer> idl = cei.citation_getStatusLog(cit);
        List<CitationStatusLogEntry> log = new ArrayList<>();
        if (idl != null && !idl.isEmpty()) {
            for (Integer i : idl) {
                log.add(citation_getCitationStatusLogEntry(i));
            }
        }
        Collections.sort(log);
        Collections.reverse(log);
        return log;
    } 

    /**
     * Extracts a single citation status log entry from the db
     *
     * @param logid
     * @return
     */
    public CitationStatusLogEntry citation_getCitationStatusLogEntry(int logid) throws IntegrationException, BObStatusException {
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        EventCoordinator ec = getEventCoordinator();
        
        CitationStatusLogEntry entry = cei.getCitationStatusLogEntry(logid);
        ec.configureEventLinkedLinkedEventList(entry);
        
        
        return entry; 

    }

    /**
     * Entry point for making a new citation status log entry on a particular
     * citation.
     *
     * @param cit to which the log entry shall be attached
     * @param csle the log entry to link to the given citation which contains its status type object; cannot be null
     * @param cse the parent case
     * @param ua doing the logging
     * @return the ID of the fresh record in the citationcitationstatus table
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public int citation_insertCitationStatusLogEntry(Citation cit,
            CitationStatusLogEntry csle,
            CECase cse,
            UserAuthorized ua)
            throws BObStatusException, IntegrationException, AuthorizationException, EventException {
        
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        
        if (cit == null
                || csle == null
                || ua == null
                || csle.getStatus() == null) {
            throw new BObStatusException("Cannot make a log entry with null citation, log entry, or user authorized");

        }

        if(!permissionsCheckpointCitationUpdate(ua)){
                throw new AuthorizationException("Authorization exception: Muni profile " + ua.getGoverningMuniProfile().getProfileID()  
                    + " requires code officer role to update a citation and user " + ua.getUserHuman() + " does not have code officer role privileges.");  
        }
        
        csle.setCreatedBy(ua);
        csle.setLastUpdatedBy(ua);
        cit.setLastUpdatedBy(ua);
        int freshID = cei.insertCitationStatusLogEntry(cit, csle);
        csle = citation_getCitationStatusLogEntry(freshID);
        
        citation_configureCitationStatusLogEvents(csle, cse, ua);
        ceCaseCacheManager.flush(cit);
        
        return freshID; 

    }
    
    /**
     * Shared logic block for setting up events in response to citation status logs
     * @param csle
     * @param cse
     * @param ua
     * @throws BObStatusException
     * @throws EventException
     * @throws IntegrationException 
     */
    private void citation_configureCitationStatusLogEvents(CitationStatusLogEntry csle, CECase cse, UserAuthorized ua) 
            throws BObStatusException, EventException, IntegrationException, AuthorizationException{
        
        EventCoordinator ec = getEventCoordinator();

        // check if we need to spawn any associated events
        if(csle.getStatus().getEventCategoryTrigger() != null){
            EventCnF triggeredEvent = ec.initEvent(cse, csle.getStatus().getEventCategoryTrigger());
            
            triggeredEvent.setTimeStart(csle.getDateOfRecord());
            triggeredEvent.setTimeEnd(triggeredEvent.getTimeStart().plusMinutes(csle.getStatus().getEventCategoryTrigger().getDefaultDurationMins()));
            triggeredEvent.appendToDescription(csle.getStatus().getEventCategoryTrigger().getHostEventDescriptionSuggestedText());
            // this actually does the event write to the event table
            List<EventCnF> freshEvList = ec.addEvent(triggeredEvent, cse, ua);
            if(freshEvList != null && !freshEvList.isEmpty()){
                for(EventCnF ev: freshEvList){
                    ec.linkEventToEventLinked(ev, csle);
                }
            }
        }
        
    }
    

    /**
     * Updates a citation status log entry
     *
     * @param cit
     * @param csle
     * @param cse
     * @param ua
     * @throws BObStatusException for any null inputs
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     * @throws com.tcvcog.tcvce.domain.EventException
     */
    public void citation_updateCitationStatusLogEntry(Citation cit,
            CitationStatusLogEntry csle,
            CECase cse,
            UserAuthorized ua)
            throws BObStatusException,
            IntegrationException,
            AuthorizationException,
            EventException {

        if (cit == null || csle == null || ua == null || csle.getStatus() == null) {
            throw new BObStatusException("Cannot update citation status with null citation, log entry or its status, or user");
        }

        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        EventCoordinator ec = getEventCoordinator();
        
        if(!permissionsCheckpointCitationUpdate(ua)){
                throw new AuthorizationException("Authorization exception: Muni profile " + ua.getGoverningMuniProfile().getProfileID()  
                    + " requires code officer role to update a citation and user " + ua.getUserHuman() + " does not have code officer role privileges."); 
        }
        csle.setLastUpdatedBy(ua);

        cei.updateStatusLogEntry(csle);
        
        // now clear our linked events and write them anew
        ec.deactivateAllEventsLinkedToEventLinkedObject(csle, cse, true, false, ua);
        citation_configureCitationStatusLogEvents(csle, cse, ua);
        ceCaseCacheManager.flush(cit);
        
    }

    /**
     * Deactivates a citation log entry
     *
     * @param csle
     * @param ua doing the deactivating
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void citation_deactivateCitationStatusLogEntry(CitationStatusLogEntry csle, CECase cse,UserAuthorized ua) 
            throws IntegrationException, BObStatusException, AuthorizationException {
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        EventCoordinator ec = getEventCoordinator();

        if(csle == null || ua == null){
            throw new BObStatusException("Cannot deactivate log entry with null log entry or null user"); 
        }
        
        if(!permissionsCheckpointCitationUpdate(ua)){
                throw new AuthorizationException("Authorization exception: Muni profile " + ua.getGoverningMuniProfile().getProfileID()  
                    + " requires code officer role to deactivate a citation log entry and user " + ua.getUserHuman() + " does not have code officer role privileges."); 
        }
        csle.setDeactivatedBy(ua);
        csle.setLastUpdatedBy(ua);
        // database sets timestamps!!

        cei.deactivateCitationStatusLogEntry(csle);
        
        // mop up events
        ec.deactivateAllEventsLinkedToEventLinkedObject(csle, cse, true, false, ua);
        ceCaseCacheManager.flushObjectFromCache(csle.getCitationID());
    }
    
    
    
    
    
    /**
     * Getter for CitationStatus objects
     * @param statusID
     * @return
     * @throws BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public CitationStatus citation_getCitationStatus(int statusID) throws BObStatusException, IntegrationException{
        if(statusID == 0){
            throw new BObStatusException("Cannot get citation status with ID = 0");
        }
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        return cei.getCitationStatus(statusID);
    }
    
    /**
     * factory for citation status objects, which get injected into the belly of the
     * actual CitationStatusLogEntry objects.
     * @return 
     */
    public CitationStatus citation_getCitationStatusSkeleton(){
        CitationStatus cs = new CitationStatus();
        
        return cs;
    }
    
    /**
     * Insertion point for citations statuses
     * @param status
     * @param ua
     * @return
     * @throws BObStatusException
     * @throws AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public int citation_insertCitationStatus(CitationStatus status, UserAuthorized ua) throws BObStatusException, AuthorizationException, IntegrationException{
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        
        if(ua == null || status == null){
            throw new BObStatusException("Cannot insert new citation status with null status or null user");
        }
        
        if(!ua.getKeyCard().isHasSysAdminPermissions()){
            throw new AuthorizationException("user lacks permissions to insert or update citation status objects");
        }
        return cei.insertCitationStatus(status);
    }
    
    /**
     * update pathway for citation status objects
     * @param status
     * @param ua     
     * @throws BObStatusException
     * @throws AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void citation_updateCitationStatus(CitationStatus status, UserAuthorized ua) throws BObStatusException, AuthorizationException, IntegrationException{
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        
        if(ua == null || status == null){
            throw new BObStatusException("Cannot update citation status with null status or null user");
        }
        
        if(!ua.getKeyCard().isHasSysAdminPermissions()){
            throw new AuthorizationException("user lacks permissions to insert or update citation status objects");
        }
        cei.updateCitationStatus(status);
        ceCaseCacheManager.getCacheCitation().invalidateAll();
    }
    
    /**
     * Deac pathway for citation status objects
     * @param status
     * @param ua
     * @throws BObStatusException
     * @throws AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void citation_deactivateCitationStatus(CitationStatus status, UserAuthorized ua) throws BObStatusException, AuthorizationException, IntegrationException{
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        
        if(ua == null || status == null){
            throw new BObStatusException("Cannot deac citation status with null status or null user");
        }
        
        if(!ua.getKeyCard().isHasSysAdminPermissions()){
            throw new AuthorizationException("user lacks permissions to insert or update citation status objects");
        }
        cei.deactivateCitationStatus(status);
    }
    
    
    

 /* **********************************************/
 /* **************** DOCKETS  ********************/
 /* **********************************************/
    /**
     * Factory method for citation dockets Sets the DOR to now and the origin
     * court entity to the head of the muni list
     *
     * @param cit
     * @return
     */
    public CitationDocketRecord citation_getCitationDocketRecordSkeleton(Citation cit) {
        CitationDocketRecord cdr = new CitationDocketRecord();
        if (cit != null && cit.getCitationID() != 0) {
            cdr.setCitationID(cit.getCitationID());
            cdr.setDateOfRecord(LocalDateTime.now());
            if (getSessionBean().getSessMuni().getCourtEntities() != null && !getSessionBean().getSessMuni().getCourtEntities().isEmpty()) {
                cdr.setCourtEntity(getSessionBean().getSessMuni().getCourtEntities().get(0));
            }
            return cdr;
        }
        return null;
    }
    
    /**
     * Utility for gathering and configuring all the citation dockets in a given citation
     * @param cit
     * @return 
     */
    private List<CitationDocketRecord> citation_getCitationDocketRecordList(Citation cit) throws BObStatusException, IntegrationException{
        if(cit == null){
            throw new BObStatusException("Cannot get citaiton dockets with null citation");
        }
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        List<Integer> cdridl = cei.getCitationDocketRecords(cit);
        List<CitationDocketRecord> cdrl = new ArrayList<>();
        if(cdridl != null && !cdridl.isEmpty()){
            for(Integer i: cdridl){
                CitationDocketRecord cdr = citation_getCitationDocketRecord(i);
                cdr.setCaseID(cit.getCecaseID());
                cdrl.add(cdr);
            }
        }
        return cdrl;
    }

    
    
    
    /**
     * Extracts a single docket entry from the DB
     * @param docketID
     * @return
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public CitationDocketRecord citation_getCitationDocketRecord(int docketID) throws BObStatusException, IntegrationException {
        if (docketID == 0) {
            throw new BObStatusException("cannot get docket from ID = 0");
        }
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        return citation_configureCitationDocketRecord(cei.getCitationDocketRecord(docketID));

    }
    
    /**
     * Sets members on dockets, like their human link list
     * @param docket
     * @return
     * @throws BObStatusException 
     */
    private CitationDocketRecord citation_configureCitationDocketRecord(CitationDocketRecord docket) throws BObStatusException, IntegrationException{
        if(docket == null){
            throw new BObStatusException("Cannot configure a null docket");
        }
        PersonCoordinator pc = getPersonCoordinator();
        docket.sethumanLinkList(pc.getHumanLinkList(docket)); 
        return docket;
    }

    /**
     * Logic intermediary for dockets
     *
     * @param cdr
     * @param ua
     * @return ID of fresh docket
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public int citation_insertDocketEntry(CitationDocketRecord cdr, UserAuthorized ua) throws IntegrationException, AuthorizationException {
        int freshID = 0;
        if (cdr != null && ua != null) {
            CourtEntityIntegrator cei = getCourtEntityIntegrator();
            
            if(ua.getGoverningMuniProfile().isRequireCodeOfficerTrueUpdateCitation() && !ua.getKeyCard().isHasEnfOfficialPermissions()){
                throw new AuthorizationException("Authorization exception: Muni profile " + ua.getGoverningMuniProfile().getProfileID()   
                    + " requires code officer role to update a citation and user " + ua.getUserHuman() + " does not have code officer role privileges."); 
            }
            cdr.setCreatedBy(ua);
            cdr.setLastUpdatedBy(ua);
            freshID = cei.insertCitationDocket(cdr);
            // we have no good way of finding out our case to flush, so flush everybody
            ceCaseCacheManager.flushObjectFromCache(cdr.getCitationID());
        }
        return freshID;

    }

    /**
     * Logic pass through for updates to dockets
     *
     * @param cdr
     * @param ua
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void citation_updateDocketEntry(CitationDocketRecord cdr, UserAuthorized ua) throws IntegrationException, AuthorizationException {
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        if (cdr != null && ua != null) {
            
            if(ua.getGoverningMuniProfile().isRequireCodeOfficerTrueUpdateCitation() && !ua.getKeyCard().isHasEnfOfficialPermissions()){
                throw new AuthorizationException("Authorization exception: Muni profile " + ua.getGoverningMuniProfile().getProfileID()  
                    + " requires code officer role to update a citation and user " + ua.getUserHuman() + " does not have code officer role privileges."); 
            }
            cdr.setLastUpdatedBy(ua);
            cei.updateCitationDocket(cdr);
            ceCaseCacheManager.flushObjectFromCache(cdr.getCitationID());
        }

    }

    /**
     * Logic intermediary for deactivation of citation Dockets
     *
     * @param cdr
     * @param ua
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void citation_deactivateDocketEntry(CitationDocketRecord cdr, UserAuthorized ua) throws IntegrationException, AuthorizationException {
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        if (cdr != null && ua != null) {
             if(ua.getGoverningMuniProfile().isRequireCodeOfficerTrueUpdateCitation() && !ua.getKeyCard().isHasEnfOfficialPermissions()){
                throw new AuthorizationException("Authorization exception: Muni profile " + ua.getGoverningMuniProfile().getProfileID()   
                    + " requires code officer role to update a citation and user " + ua.getUserHuman() + " does not have code officer role privileges."); 
            }
            cdr.setDeactivatedBy(ua);
            cdr.setLastUpdatedBy(ua);
            cei.deactivateCitationDocket(cdr);
            ceCaseCacheManager.flushObjectFromCache(cdr.getCitationID());
        }
    }

    /**
     * *** PERMISSIONS CHECKPOINT ***
     * Logic intermediary for updating fields on Citations in the DB
     *
     * @param c
     * @param ua
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void citation_updateCitation(Citation c, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException {
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        if(c == null || ua == null){
            throw new BObStatusException("Cannot update citation with null citation or user");
        }
        
        if(!permissionsCheckpointCitationUpdate(ua)){
            throw new AuthorizationException("Authorization exception: Muni profile " + ua.getGoverningMuniProfile().getProfileID()    
                    + " requires code officer role to update a citation and user " + ua.getUserHuman() + " does not have code officer role privileges."); 
        }
        cei.updateCitation(c);
        ceCaseCacheManager.flush(c);

    }

    /**
     * Updates a record in the citationviolation table
     *
     * @param ccvl
     * @param updateNotes
     * @param ua
     * @throws BObStatusException
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void citation_updateCitationCodeViolationLink(CitationCodeViolationLink ccvl, String updateNotes, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException {
        SystemCoordinator sc = getSystemCoordinator();
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        if (ccvl == null || ccvl.getCitationViolationID() == 0 || ua == null) {
            throw new BObStatusException("Cannot update codeviolationcitation link with null inputs");
        }

        if(!permissionsCheckpointCitationUpdate(ua)){
            throw new AuthorizationException("Authorization exception: Muni profile " + ua.getGoverningMuniProfile().getProfileID()   
                    + " requires code officer role to update a citation and user " + ua.getUserHuman().getName() + " does not have code officer role privileges."); 
        }
        
        StringBuilder freshNote = new StringBuilder();
        freshNote.append("Citation-Violation link ID ");
        freshNote.append(ccvl.getCitationViolationID());
        freshNote.append(" status updated to ");
        freshNote.append(ccvl.getCitVStatus().getLabel());
        freshNote.append("<br />");
        freshNote.append("With notes: ");
        freshNote.append("<br />");
        freshNote.append(updateNotes);

        MessageBuilderParams mbp = new MessageBuilderParams(ccvl.getLinkNotes(),
                null, null, freshNote.toString(), getSessionBean().getSessUser(), null);

        ccvl.setLinkNotes(sc.appendNoteBlock(mbp));
        ccvl.setLinkLastUpdatedByUserID(ua.getUserID());
        cei.updateCitationCodeViolationLink(ccvl);
        // I hope this works
        ceCaseCacheManager.flushObjectFromCache(ccvl.getCitationID());
        ceCaseCacheManager.flushObjectFromCache(ccvl.getViolationID());

    }

    /**
     * Deactivates a record in the citationviolation table
     *
     * @param ccvl
     * @param ua
     * @throws BObStatusException
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void citation_deactivateCitationCodeViolationLink(CitationCodeViolationLink ccvl, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException {
        if (ccvl == null || ccvl.getCitationViolationID() == 0 || ua == null) {
            throw new BObStatusException("Cannot update codeviolationcitation link with null inputs");
        }

        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        
        if(!permissionsCheckpointCitationUpdate(ua)){
                throw new AuthorizationException("Authorization exception: Muni profile " + ua.getGoverningMuniProfile().getProfileID()   
                    + " requires code officer role to update a citation and user " + ua.getUserHuman() + " does not have code officer role privileges."); 
        }
        ccvl.setLinkCreatedByUserID(ua.getUserID());
        ccvl.setLinkDeactivatedByUserID(ua.getUserID());
        cei.deactivateCitationCodeViolationLink(ccvl);
        ceCaseCacheManager.flushObjectFromCache(ccvl.getCitationID());
        ceCaseCacheManager.flushObjectFromCache(ccvl.getViolationID());

    }

    /**
     * Logic intermediary for updating fields on Citations in the DB and
     * deactivating all associated objects: citation dockets, log records, blob
     * links, person links, and codeviolation links, and event links
     *
     * @param c
     * @param cse
     * @param ua
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void citation_deactivateCitation(Citation c, CECase cse, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException {
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        UserCoordinator uc = getUserCoordinator();
        PersonCoordinator pc = getPersonCoordinator();
        BlobCoordinator bc = getBlobCoordinator();

        if (c != null && ua != null) {
            if(!permissionsCheckpointCitationOpenClose(ua)){
                throw new AuthorizationException("Authorization exception: Muni profile " + ua.getGoverningMuniProfile().getProfileID()  
                    + " requires code officer role to remove a citation and user " + ua.getUserHuman() + " does not have code officer role privileges."); 
            }
            
            c.setDeactivatedBy(ua);
            cei.deactivateCitation(c);
            System.out.println("CaseCoordinator.citation_removeCitation | deactivated citation ID: " + c.getCecaseID());
            // deactivate associated objects: log entries
            if (c.getStatusLog() != null && !c.getStatusLog().isEmpty()) {
                for (CitationStatusLogEntry csle : c.getStatusLog()) {
                    citation_deactivateCitationStatusLogEntry(csle, cse, ua);
                    System.out.println("CaseCoordinator.citation_removeCitation | deactivated status log ID: " + csle.getLogEntryID());
                }
            }
            // deactivate associated objects: dockets
            if (c.getDocketNos() != null && !c.getDocketNos().isEmpty()) {
                for (CitationDocketRecord cdr : c.getDocketNos()) {
                    citation_deactivateDocketEntry(cdr, ua);
                    System.out.println("CaseCoordinator.citation_removeCitation | deactivated docket ID: " + cdr.getDocketID());
                }
            }
            // deactivate associated objects: citationviolations
            if (c.getViolationList() != null && !c.getViolationList().isEmpty()) {
                for (CitationCodeViolationLink ccvl : c.getViolationList()) {
                    citation_deactivateCitationCodeViolationLink(ccvl, ua);
                    System.out.println("CaseCoordinator.citation_removeCitation | deactivated citation violation link: " + ccvl.getCitationViolationID());
                }
            }
            // deactivate associated objects: personlinks
            if (c.gethumanLinkList() != null && !c.gethumanLinkList().isEmpty()) {
                for (HumanLink hl : c.gethumanLinkList()) {
                    pc.deactivateHumanLink(hl, ua);
                    System.out.println("CaseCoordinator.citation_removeCitation | deactivated human link link: " + hl.getLinkID());
                }
            }
            // deactivate associated objects: bloblinks
            if (c.getBlobList() != null && !c.getBlobList().isEmpty()) {
                for (BlobLight bl : c.getBlobList()) {
                    bc.deleteLinksToPhotoDocRecord(bl, BlobLinkEnum.CITATION, getSessionBean().getSessUser());
                    System.out.println("CaseCoordinator.citation_removeCitation | deactivated links to blob light: " + bl.getPhotoDocID());
                }
            }
            // deactivate associated objects: related events
            // Not sure until we write the events!
            ceCaseCacheManager.flush(c);
        }
    }

    /**
     * Updates only the notes field on Citation
     *
     * @param mbp
     * @param cit
     * @throws BObStatusException
     * @throws IntegrationException
     */
    public void citation_updateNotes(MessageBuilderParams mbp, Citation cit) throws BObStatusException, IntegrationException {
        CaseIntegrator ci = getCaseIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        if (cit == null || mbp == null) {
            throw new BObStatusException("Cannot append if notes, case, or user are null");
        }

        cit.setNotes(sc.appendNoteBlock(mbp));

        ci.updateCitationNotes(cit);
        ceCaseCacheManager.flush(cit);
    }
    
    /**
     * get citation by id and returns a list of citation
     *
     * @param cIDsList
     * @return ctList
     * @throws BObStatusException
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BlobException
     */
    public List<Citation> getCitationList(List<Integer> cIDsList) throws IntegrationException, BObStatusException, BlobException {
        List<Citation> ctList = new ArrayList<>();
        if (cIDsList != null && !cIDsList.isEmpty()){
            for (Integer i : cIDsList) {
                ctList.add(citation_getCitation(i));
            }
        }
        return ctList;
    }
    
    /**
     * Utility method for setting the latest log entry status and Parent case ,
     * property details to list
     *
     * @param ctList
     * @param ua
     * @return ctList
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public List<CitationCECasePropertyHeavy> assembleCitationCECasePropertyHeavyList(List<CitationCECasePropertyHeavy> ctList, UserAuthorized ua) throws IntegrationException, BObStatusException {
        PropertyCoordinator pc = getPropertyCoordinator();
        if (Objects.nonNull(ctList) && !ctList.isEmpty())
        {
            for (CitationCECasePropertyHeavy citationCaseHeavy : ctList)
            {
                List<CitationStatusLogEntry> statusLog = citationCaseHeavy.getStatusLog();
                if (Objects.nonNull(statusLog) && !statusLog.isEmpty())
                {
                    Optional<CitationStatusLogEntry> latestCitationStatus = statusLog.stream().collect(Collectors.maxBy(Comparator.comparing(CitationStatusLogEntry::getLastUpdatedTS)));
                    citationCaseHeavy.setLatestLogEntry(latestCitationStatus.get());
                }
                if (citationCaseHeavy.getCecaseID() != 0)
                {
                    
                    citationCaseHeavy.setCecaseProUHeavy(cecase_getCECase(citationCaseHeavy.getCecaseID(), ua));
                }
            }
        }
        return ctList;

    }
    

//    --------------------------------------------------------------------------
//    ********************* Citation Penalties *********************************
//    --------------------------------------------------------------------------
    
    
    /**
     * Returns a citation penalty
     * @param penaltyID
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public CitationPenalty citation_getCitationPenalty(int penaltyID) throws BObStatusException, IntegrationException{
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        if(penaltyID == 0){
            throw new BObStatusException("Cannot get citation penalty of id 0");
        }
        return cei.getCitationPenalty(penaltyID);
        
    }
    
    
    /**
     * Returns our list of Penalty objects by implementing class
     * @param polder
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public List<CitationPenalty> citation_getCitationPenaltyList(IfaceCitationPenaltyHolder polder) throws BObStatusException, IntegrationException{
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        if(polder == null){
            throw new BObStatusException("Cannot get citation penalty list for a null penalty holder");
        }
        
        List<Integer> plist = cei.getCitationPenaltyList(polder);
        List<CitationPenalty> penList = new ArrayList<>();
        for(Integer i: plist){
            penList.add(cei.getCitationPenalty(i));
        }
        
        return penList;
        
    }
    
    /**
     * Generator of penalties
     * @param holder
     * @return
     * @throws BObStatusException 
     */
    public CitationPenalty citation_getCitationPenaltySkeleton(IfaceCitationPenaltyHolder holder) throws BObStatusException{
        if(holder == null){
            throw new BObStatusException("Cannot get citation penalty holder skeleton with null holder");
        }
        CitationPenalty pen = new CitationPenalty();
        if(holder instanceof Citation cit){
            pen.setCitationID(cit.getCitationID());
        } else if (holder instanceof CitationCodeViolationLink cvl){
            pen.setCitationViolationID(cvl.getCitationViolationID());
        } 
        pen.setDateOfRecord(LocalDate.now());
        pen.setPenaltyType(CitationPenaltyTypeEnum.FINEOFFICERREQUESTED);
        return pen;
        
    } 
    
    /**
     * Insertion pathway for citation penalties.
     * 
     * @param penalty
     * @param holder
     * @param cse
     * @param ua
     * @return
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public int citation_insertCitationPenalty(CitationPenalty penalty, IfaceCitationPenaltyHolder holder, CECase cse, UserAuthorized ua) throws BObStatusException, IntegrationException{
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        
        if(penalty == null || holder == null || ua == null || cse == null){
            throw new BObStatusException("Cannot insert penalty with null holder, penalty, or user");
        }
        if(!cse.isOpen()){
            throw new BObStatusException("Cannot update citations on closed cases");
        }
        
        penalty.setCreatedBy(ua);
        penalty.setLastUpdatedBy(ua);
        int freshid = cei.insertCitationPenalty(penalty);
        ceCaseCacheManager.flushObjectFromCache(penalty.getCitationID());
        return freshid;
    } 
    
    /**
     * Update pathway for citation penalties
     * @param penalty
     * @param holder
     * @param cse
     * @param ua
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public void citation_updateCitationPenalty(CitationPenalty penalty, IfaceCitationPenaltyHolder holder, CECase cse, UserAuthorized ua) throws BObStatusException, IntegrationException{
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        
        if(penalty == null || holder == null || cse == null || ua == null){
            throw new BObStatusException("Cannot insert penalty with null holder, penalty, or user");
        }
        if(!cse.isOpen()){
            throw new BObStatusException("Cannot update citations on closed cases");
        }
        
        penalty.setLastUpdatedBy(ua);
        cei.updateCitationPenalty(penalty);
        ceCaseCacheManager.flushObjectFromCache(penalty.getCitationID());
    }
    
    /**
     * Deactivates a citation penalty
     * @param penalty
     * @param holder
     * @param cse
     * @param ua
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public void citation_deactivateCitationPenalty(CitationPenalty penalty, IfaceCitationPenaltyHolder holder, CECase cse, UserAuthorized ua) throws BObStatusException, IntegrationException{
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        
        if(penalty == null || holder == null || ua == null || cse == null){
            throw new BObStatusException("Cannot insert penalty with null holder, penalty, or user");
        }
        
        if(!cse.isOpen()){
            throw new BObStatusException("Cannot update citations on closed cases");
        }
        
        penalty.setLastUpdatedBy(ua);
        penalty.setDeactivatedBy(ua);
        penalty.setDeactivatedTS(LocalDateTime.now());
        
        cei.updateCitationPenalty(penalty);
        ceCaseCacheManager.flushObjectFromCache(penalty.getCitationID());
    } 
    
    

//    --------------------------------------------------------------------------
//    ********************* CE Action Requests *********************************
//    --------------------------------------------------------------------------
    
    /**
     * Creates a data heavy version of a CEAR
     * @param cear if the cear has a nonzero parcelkey,
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public CEActionRequestPropertyHeavy cear_getCEARPropertyHeavy(CEActionRequest cear) 
            throws IntegrationException, BObStatusException{
        PropertyCoordinator pc = getPropertyCoordinator();
        CEActionRequestPropertyHeavy cearph = new CEActionRequestPropertyHeavy(cear);
        if(cear != null & cear.getParcelKey() != 0){
            try {
                cearph.setRequestProperty(pc.getProperty(cear.getParcelKey()));
            } catch (AuthorizationException ex) {
                throw new BObStatusException(ex.getMessage());
            }
        }
        
        return cearph;
    }
    
    /**
     * Convenience method for looping over a given list of CEARs and creating 
     * PropertyHeavy versions of each.
     * @param cearList
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public List<CEActionRequestPropertyHeavy> cear_getCEARPropertyHeavyList(List<CEActionRequest> cearList) 
            throws IntegrationException, BObStatusException{
        List<CEActionRequestPropertyHeavy> cearphl = new ArrayList<>();
        if(cearList != null && !cearList.isEmpty()){
            for(CEActionRequest cear: cearList){
                cearphl.add(cear_getCEARPropertyHeavy(cear));
            }
        }
        return cearphl;
    }
    
    /**
     * Factory method for our CEActionRequests - initializes the date as well
     *
     * @return The CEActionRequest ready for populating with user values
     */
    public CEActionRequest cear_getInititalizedCEActionRequest() {
        System.out.println("CaseCoordinator.getNewActionRequest");
        CEActionRequest cear = new CEActionRequest();
        cear.setDateOfRecord(LocalDateTime.now());
        cear.setRequestorPersonType(PersonType.Public);
        cear.setBlobList(new ArrayList<>());
        return cear;
    }
     /**
     * Writes to the selected reuqest the status with the the given String key
     * 
     * @param newStatusKey 
     */
    private void cear_updateRequestStatus(CEActionRequest cear, CEARProcessingRouteEnum route) 
            throws IntegrationException, BObStatusException {
        CaseCoordinator cc = getCaseCoordinator();
        if(cear == null || route == null){
            throw new BObStatusException("cannot update request status with null CEAR or route");
        }
        cear.setRequestStatus(cc.cear_getCEARStatus(Integer.parseInt( 
                getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE) 
                        .getString(route.getCEAR_STATUS_STRING_FOR_DB_KEY_LOOKUP()))));  
               
    }
    /**
     * Links the given CEAR to the given CECase which occurs on the CEAR table only
     * @param cse
     * @param cear 
     * @param route 
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void cear_connectCEARToCECase(CECase cse, CEActionRequest cear, CEARProcessingRouteEnum route, UserAuthorized ua) 
            throws IntegrationException, BObStatusException {
        
        if(cse == null || cear == null || ua == null){
            throw new BObStatusException("Cannot route request with null cear or case or user"); 
        }
        
        if(cear.getParcelKey() != cse.getParcelKey()){
            throw new BObStatusException("Request parcel key and the given CE Case's parent parcel key do not match; "
                    + "immediate abort of case attachment operation; This logic error must be addressed by a system Admin");
        }
        
        CEActionRequestIntegrator ceari = getcEActionRequestIntegrator();
        SystemCoordinator sc = getSystemCoordinator();

        // if we are choosing a case to connect the cear to but don't have the parcel linked
        // we need to do that first
        if(cear.getParcelKey() == 0){
            cear.setParcelKey(cse.getParcelKey());
            System.out.println("CaseCoordinator.cear_connectCEARToCECase | Found parcel key==0 on case link, so updating that now." );
        }
        
        System.out.println("CaseCoordinator.cear_connectCEARToCECase | operation ini | CEAR ID:" + cear.getRequestID());

        cear.setCaseID(cse.getCaseID());
        cear.setCaseAttachmentTimeStamp(LocalDateTime.now());
        cear.setCaseAttachmentUser(ua);
        cear.setLastUpdatedBy(ua);
        cear_updateRequestStatus(cear, route);
        
        MessageBuilderParams mbp = new MessageBuilderParams(cear.getPublicExternalNotes());
        mbp.setUser(ua);
        mbp.setHeader("Request attached to existing CE Case");
        StringBuilder sb = new StringBuilder();
        sb.append("Case ID:");
        sb.append(String.valueOf(cse.getCaseID()));
        sb.append(". Name: ");
        sb.append(cse.getCaseName());
        mbp.setNewMessageContent(sb.toString());
        cear.setPublicExternalNotes(sc.appendNoteBlock(mbp));
        
        ceari.updateActionRequestInternalFields(cear);
        ceCaseCacheManager.flush(cear);
        ceCaseCacheManager.flush(cse);
        
        
        System.out.println("CaseCoordinator.cear_connectCEARToCECase | operation complete connected to case ID:" + cse.getCaseID());
    }

    /**
     * Logic gate for CEAR initial insertions; designed for use 
     * after the "concern description" phase, before blobs, and before requesting
     * user info.We have an early insert so blobs can be connected properly
     * 
     * @param cear
     * @param ua
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public int cear_insertCEARFirstStage(CEActionRequest cear, UserAuthorized ua) 
            throws IntegrationException, BObStatusException{
        CEActionRequestIntegrator ceari = getcEActionRequestIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        cear.setRequestStatus(ceari.getRequestStatus(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("actionRequestInitialStatusCode"))));
        cear.setRequestPublicCC(GRIDSQAURES_COLS_1);
        cear.setRequestPublicCC(sc.generateControlCodeFromTime());
        cear.setCreatedBy(ua);
        cear.setLastUpdatedBy(ua);
        return ceari.insertCEActionRequest(cear);
    } 
    
    /**
     * Updates internal fields of CEAR such as CEAR status and notes.Does NOT update fields populated in public-facing form, even when those 
 populations occurred by an authenticated user
     * 
     * @param cear
     * @param ua 
     * @param route it not null, this method calls the internal update CEAR
     * status method BEFORE shipping the CEAR to DB integration
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void cear_updateCEAR(CEActionRequest cear, UserAuthorized ua, CEARProcessingRouteEnum route) 
            throws IntegrationException, BObStatusException{
       CEActionRequestIntegrator ceari = getcEActionRequestIntegrator();
        if(cear != null && ua != null){
            if(route != null){
                cear_updateRequestStatus(cear, route);
            }
           cear.setLastUpdatedBy(ua);
           ceari.updateActionRequestInternalFields(cear);
           ceCaseCacheManager.flush(cear);
        } else {
            throw new BObStatusException("Cannot update CEAR with null CEAR or User");
        }
    }
    
    /**
     * Logic and permissions check for CEAR deac; as of 26 MAY 2023, users must be 
     * system admin to deac a CEAR
     * @param cear
     * @param ua system admin required
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void cear_deactivateCEAR(CEActionRequest cear, UserAuthorized ua) throws BObStatusException, AuthorizationException, IntegrationException{
        if(ua == null || cear == null){
            throw new BObStatusException("Cannot deac cear with null cear or ua");
        }
        PermissionsCoordinator permCor = getPermissionsCoordinator();
        SystemIntegrator si = getSystemIntegrator();
        if(permCor.permissionsCheckpointSystemAdminRank(ua)){
            si.deactivateTrackedEntity(cear, ua);
            ceCaseCacheManager.flush(cear);
        } else {
            throw new AuthorizationException("user must have system admin rank to deac a CEAR"); 
        }
    }   
    
    /**
     * Called during the final stage of CEAR submission when the public user inserts their
     * contact information
     * 
     * @param cear
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public void cear_updatePublicFields(CEActionRequest cear) 
            throws IntegrationException, BObStatusException{
       CEActionRequestIntegrator ceari = getcEActionRequestIntegrator();
        if(cear != null){
           ceari.updateCEARPublicFields(cear);
           ceCaseCacheManager.flush(cear);
        } else {
            throw new BObStatusException("Cannot update CEAR requestor fields with null CEAR");
        }
    }
    
    /**
     * Appends the given note at the given scope to the given cear by the given user
     * 
     * @param cear
     * @param note
     * @param noteScope
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void cear_updateNotes(CEActionRequest cear, String note, NoteScopeEnum noteScope, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(cear == null || note== null || noteScope == null || ua == null){
            throw new BObStatusException("Cannot append note with null cear, note, scope, or user");
        }
        SystemCoordinator sc = getSystemCoordinator();
        CEActionRequestIntegrator ceari = getcEActionRequestIntegrator();
        MessageBuilderParams mbp = null;
        switch(noteScope){
            case EXTERNAL -> {
                mbp = new MessageBuilderParams(cear.getPublicExternalNotes());
                cear.setPublicExternalNotes(sc.appendNoteBlock(mbp));
            }
            case INTERNAL -> {
                mbp = new MessageBuilderParams(cear.getMuniNotes());
                cear.setMuniNotes(sc.appendNoteBlock(mbp));
            }
        }
        if(mbp != null){
            mbp.setUser(ua);
            mbp.setNewMessageContent(note);
        }
        
        ceari.updateCEARNotes(cear); 
        ceCaseCacheManager.flush(cear);
        
    }
    
    /**
     * Logic implementation for updating a CEAR's property to the given target one AND
     * appending a log entry to the CEAR muni visible notes;
     * 
     * If the given CEAR is listed as not at a particular property, that status will be 
     * turned OFF since the request will now be associated with an actual property.
     * 
     * @param cear with old parcel links, if any;
     * @param targetProp
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void cear_updateCEARProperty(CEActionRequestPropertyHeavy cear, Property targetProp, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(cear == null || targetProp == null || ua == null){
            throw new BObStatusException("Cannot update CEAR property with a null cear, target prop or requesting user");
        }
        SystemCoordinator sc = getSystemCoordinator();
        
        StringBuilder log = new StringBuilder();
        Property formerProp = cear.getRequestProperty();
        
        // The actual connection u pdate
        cear.setParcelKey(targetProp.getParcelKey());
        
        // Logging stuff
        log.append("Property updated from ");
        log.append(getPrettyDate(LocalDateTime.now()));
            log.append(" from ");
        if(formerProp != null){
            log.append(formerProp.getAddress().getAddressPretty1Line());
        } else {
            log.append("no previously linked property");
            if(cear.isNotAtKnownAddress() && cear.getNonaddressableDescription() != null){
                log.append(" [A location description was given of: ");
                log.append(cear.getNonaddressableDescription());
                log.append("]");
            }
        } // end previous property logging
        log.append(" TO: ");
        log.append(targetProp.getAddress().getAddressPretty1Line());
        
        // Now that we have an actual property, toggle off not at known address flag
        if(cear.isNotAtKnownAddress()){
            log.append(". The original request was submitted as not being located ");
            log.append("on a specific property and this designation was removed during this property linking operation. ");
            cear.setNotAtKnownAddress(false);
        }
        
        // Wipe out any case attachments if they exist
        if(cear.getCaseAttachmentTimeStamp() != null){
            cear.setCaseAttachmentTimeStamp(LocalDateTime.MIN);
            cear.setCaseID(0);
            MessageBuilderParams mbp = new MessageBuilderParams(cear.getMuniNotes());
            mbp.setHeader("Case link removed");
            mbp.setNewMessageContent("During property update an existing CECase link was found, which cannot persist across property changes so this case attachment has been nuked.");
            mbp.setUser(ua);
            cear.setMuniNotes(sc.appendNoteBlock(mbp));

        }
        
        MessageBuilderParams mbp = new MessageBuilderParams(cear.getMuniNotes());
        mbp.setHeader("Property connection update");
        mbp.setNewMessageContent(log.toString());
        mbp.setUser(ua);
        cear.setMuniNotes(sc.appendNoteBlock(mbp));
        
        cear_updateCEAR(cear, ua, null);
        ceCaseCacheManager.flush(cear);
        
    }
    
    /**
     * Business logic intermediary method for CEActionRequests.Calls the
 CEAction Integrator
     *
     * @param cearid
     * @return
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.BlobException
     */
    public CEActionRequest cear_getCEActionRequest(int cearid) throws IntegrationException, BObStatusException, BlobException {
        CEActionRequestIntegrator ceari = getcEActionRequestIntegrator();
        BlobCoordinator bc = getBlobCoordinator();
        UserCoordinator uc = getUserCoordinator();
        PersonCoordinator pc = getPersonCoordinator();
        
        
        CEActionRequest cear = ceari.getActionRequestByRequestID(cearid);
        cear.setDaysSinceSubmission(getDaysSince(cear.getDateOfRecord()));
        cear.setBlobList(bc.getBlobLightList(cear)); 
        if(cear.getCreatedByUserID() != 0){
            cear.setInternalUserSubmitter(!uc.determineIfUserIsPublicUser(uc.user_getUser(cear.getCreatedByUserID())));
        }
        cear.sethumanLinkList(pc.getHumanLinkList(cear));
        
        return cear;
    }

    /**
     * Utility method for determining whether or not the panel of Code
     * Enforcement request routing buttons can be pressed. Used by the view for
     * setting disabled properties on buttons requests
     *
     * @param req the current CE Request
     * @param u current user
     * @return True if the current user can route the given ce request
     */
    public boolean cear_determineCEActionRequestRoutingActionEnabledStatus(
            CEActionRequest req,
            UserAuthorized u) {
        if (req != null && u.getMyCredential() != null) {
            if ((req.getRequestStatus().getStatusID()
                    == Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                            .getString("actionRequestInitialStatusCode")))
                    && u.getMyCredential().isHasEnfOfficialPermissions()) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Extracts issue types by muni
     * @param muni if null all active issue type are returned regardless of muni link possibly
     * leading to duplicates if two munis have the same issue mapped in the DB table
     * @param includeInactive
     * @return
     * @throws IntegrationException 
     */
    public List<CEActionRequestIssueType> cear_getIssueTypes(Municipality muni, boolean includeInactive) throws IntegrationException {
        CEActionRequestIntegrator ceari = getcEActionRequestIntegrator();
        List<CEActionRequestIssueType> typeList = ceari.getRequestIssueTypeList(muni, includeInactive);
        return typeList;
    }
    
    
    
    /**
     * Primary getter for issue types
     * @param issueTypeID
     * @return
     * @throws BObStatusException 
     */
    public CEActionRequestIssueType cear_getCEARIssueType(int issueTypeID) throws BObStatusException, IntegrationException{
        CEActionRequestIntegrator ceari = getcEActionRequestIntegrator();
        if(issueTypeID == 0){
            throw new BObStatusException("Cannot get issue type with ID 0");
        }
        return ceari.getRequestIssueType(issueTypeID);
        
    }
    
    /**
     * Factory for issue types
     * @param muni
     * @return 
     */
    public CEActionRequestIssueType cear_getIssueTypeSkeleton(Municipality muni){
        CEActionRequestIssueType cearit = new CEActionRequestIssueType();
        cearit.setMuni(muni);
        cearit.setActive(true);
        return cearit;
        
    }
    
    /**
     * Insertion pathway for issue types
     * @param issueType
     * @param ua
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public int cear_insertIssueType(CEActionRequestIssueType issueType, UserAuthorized ua) throws BObStatusException, IntegrationException{
        CEActionRequestIntegrator ceari = getcEActionRequestIntegrator();
        if(issueType == null || ua == null){
            throw new BObStatusException("Cannot insert new issue type with null issue type or user");
        }
        cear_auditCEAR(issueType);
        
        return ceari.insertCEARIssueType(issueType);
    }
    
    /**
     * Internal auditing methodod
     * @param issueType
     * @throws BObStatusException 
     */
    private void cear_auditCEAR(CEActionRequestIssueType issueType) throws BObStatusException{
        if(issueType.getName() == null){
            throw new BObStatusException("CE ACtion request issue types require a name");
        }
        if(issueType.getMuni() == null){
            throw new BObStatusException("CE Action Request issue types require a muni");
        }
      
    }
    
    /**
     * Update pathway for issue types
     * @param issueType
     * @param ua
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void cear_UpdateIssueType(CEActionRequestIssueType issueType, UserAuthorized ua) throws BObStatusException, IntegrationException{
        CEActionRequestIntegrator ceari = getcEActionRequestIntegrator();
        if(issueType == null || ua == null){
            throw new BObStatusException("Cannot insert new issue type with null issue type or user");
        }
        cear_auditCEAR(issueType);
        
        ceari.updateCEARIssueType(issueType);
        ceCaseCacheManager.flushAllCaches();
    }
    
    /**
     * Deac pathway for CEAR issue types
     * @param issueType
     * @param ua
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void cear_deactivateIssueType(CEActionRequestIssueType issueType, UserAuthorized ua) throws BObStatusException, IntegrationException{
        CEActionRequestIntegrator ceari = getcEActionRequestIntegrator();
        if(issueType == null || ua == null){
            throw new BObStatusException("Cannot insert new issue type with null issue type or user");
        }
        ceari.deactivateCEARIssueType(issueType);
    }
    
    
    /**
     * Logic pass through for CEARI status objects
     * @param statusID
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public CEActionRequestStatus cear_getCEARStatus(int statusID) throws IntegrationException, BObStatusException{
        CEActionRequestIntegrator ceari = getcEActionRequestIntegrator();
        return ceari.getRequestStatus(statusID);
    }
    
    /**
     * Extracts all active statuses from the DB
     * @return
     * @throws IntegrationException 
     */
    public List<CEActionRequestStatus> getCEARStatusList() throws IntegrationException{
        CEActionRequestIntegrator ceari = getcEActionRequestIntegrator();
        return ceari.getRequestStatusList();
    }
    
    // *************************************************************************
    // *                     Violations                                        *
    // *************************************************************************
    
    /**
     * Returns a list, with perhaps one or more violations for which 
     * compliance has not been achieved 
     * 
     * @param cedh
     * @return a list, perhaps with Vs
     */
    public List<CodeViolation> violation_assembleEligibleViolationListForBatchOperation(CECaseDataHeavy cedh) throws BObStatusException{
        if(cedh == null){
            throw new BObStatusException("Cannot build V list with null case");
        }
        List<CodeViolation> cvlist = cedh.getViolationListUnresolved();
        
        return cvlist;
        
    }
    
    
    
    /**
     * Utility method for iterating over a given list of violations
     * and determining the soonest stip comp date
     * 
     * @param vlist
     * @return perhaps a non-null date if at least one violation is in the inputted list
     * and has a stip comp date
     */
    public LocalDateTime violation_determineEarliestStipCompDate(List<? extends CodeViolation> vlist){
        LocalDateTime earliesSCD = null;

        // written by cnf Chat GPT
        earliesSCD=    vlist.stream()
                            .map(CodeViolation::getStipulatedComplianceDate) // Extract the compliance dates
                            .filter(Objects::nonNull) // Remove null values
                            .min(LocalDateTime::compareTo) // Find the earliest date
                            .orElse(null); // Return null if no dates exist
        
//        if(vlist != null && !vlist.isEmpty()){
//            for(CodeViolation cv: vlist){
//                if(cv.getStipulatedComplianceDate() != null){
//                    if(earliesSCD == null){
//                        earliesSCD = cv.getStipulatedComplianceDate();
//                    } else {
//                        if(earliesSCD.isAfter(cv.getStipulatedComplianceDate())){
//                            earliesSCD = cv.getStipulatedComplianceDate();
//                        }
//                    }
//                }
//            }
//        }
        return earliesSCD;
    }
    
    /**
     * Violation status enum values have associated ordinal values which this
     * method looks at to determine the highest one, which it returns and then
     * goes on break
     *
     * @param vList
     * @return
     */
    public int violation_determineMaxViolationStatus(List<CodeViolationStatusHeavy> vList) {
        int maxStatus = -1;
        if (vList != null) {
            for (CodeViolationStatusHeavy cv : vList) {
                if (cv.getStatus().getOrder() > maxStatus) {
                    maxStatus = cv.getStatus().getOrder();
                }
            }
        }
        return maxStatus;
    }

    /**
     * Iterates over all of a case's violations and checks for compliance. If
     * all of the violations have a compliance date, the case is automatically
     * closed and a case closing event is generated and added to the case
     *
     * @param c the case whose violations should be checked for compliance
     * @param ua
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws ViolationException
     * @throws com.tcvcog.tcvce.domain.EventException
     */
    public void violation_checkForFullComplianceAndCloseCaseIfTriggered(CECaseDataHeavy c, UserAuthorized ua)
            throws IntegrationException, BObStatusException, ViolationException, EventException, AuthorizationException {

        EventCoordinator ec = getEventCoordinator();
        EventIntegrator ei = getEventIntegrator();

        boolean complianceWithAllViolations = false;

        ListIterator<CodeViolationStatusHeavy> fullViolationLi = c.getViolationList().listIterator();

        CodeViolation cv = null;
        while (fullViolationLi.hasNext()) {
            cv = fullViolationLi.next();
            // if there are any outstanding code violations, toggle switch to 
            // false and exit the loop. Phase change will not occur
            if (cv.getActualComplianceDate() != null) {
                complianceWithAllViolations = true;
            } else {
                complianceWithAllViolations = false;
                break;
            }
        } // close while

        EventCnF complianceClosingEvent;

        if (complianceWithAllViolations) {
            complianceClosingEvent = ec.initEvent(c, ei.getEventCategory(Integer.parseInt(getResourceBundle(
                    Constants.EVENT_CATEGORY_BUNDLE).getString("closingAfterFullCompliance"))));
            ec.addEvent(complianceClosingEvent, c, ua);

        } // close if
        ceCaseCacheManager.flush(c);
    }

    /**
     * Factory method for starting the process of making a new CodeViolation
     * object
     *
     * @param c
     * @return
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public CodeViolation violation_getCodeViolationSkeleton(CECaseDataHeavy c) throws BObStatusException, IntegrationException {
        CodeViolation v = new CodeViolation();
        SystemCoordinator sc = getSystemCoordinator();
         
        v.setSeverityIntensity(sc.getIntensityClass(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("defaultviolationseverity"))));
        v.setDateOfRecord(LocalDateTime.now());
        if (c != null) {
            v.setCeCaseID(c.getCaseID());
        } else {
            throw new BObStatusException("Cannot attach violation to null case");
        }

        // control is passed back to the violationAddBB which stores this 
        // generated violation under teh activeCodeViolation in the session
        // which the ViolationAddBB then picks up and edits
        return v;
    }

    /**
     * Standard coordinator method which calls the integration method after
     * checking business rules. ASLO attaches blobs if present
     *
     * @param cv
     * @param cse
     * @param ua
     * @return the database key assigned to the inserted violation
     * @throws IntegrationException
     * @throws ViolationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.EventException
     * @throws com.tcvcog.tcvce.domain.SearchException
     */
    public int violation_attachViolationToCase(CodeViolation cv, CECase cse, UserAuthorized ua)
            throws IntegrationException, ViolationException, BObStatusException, EventException, SearchException {

        if (cv == null || cse == null || ua == null) {
            throw new BObStatusException("Cannot attach violation with null viol, case, or user");
        }
        if(cv.getViolationID() != 0){
            throw new BObStatusException("Incoming violation for add has a nonzero ID--meaning it's already added");
        }

        CaseIntegrator ci = getCaseIntegrator();
        PaymentCoordinator pc = getPaymentCoordinator();
        BlobCoordinator blobc = getBlobCoordinator();
        
        int insertedViolationID;
        cv.setCeCaseID(cse.getCaseID());
        violation_verifyCodeViolationAttributes(cse, cv);
        if (cv.getLastUpdatedBy()== null) {
            cv.setLastUpdatedBy(ua);
        }
        if (cv.getCreatedBy() == null) {
            cv.setCreatedBy(ua);
        }
        
        insertedViolationID = ci.insertCodeViolation(cv);
        pc.insertAutoAssignedFees(cse, cv);
        // deal with blobs
        if(cv.getBlobList() != null && !cv.getBlobList().isEmpty()){
            System.out.println("CaseCoordinator.violation_attachViolationToCase | linking blob to violation on viol insert! size: " + cv.getBlobList().size());
            // ordinarily a violation wouldn't have blobs on it from a fresh case attachment
            // but it might from an inspection!
            cv.setViolationID(insertedViolationID);
            blobc.linkBlobHolderToBlobList(cv, cv.getBlobList() );
        }
        ceCaseCacheManager.flush(cv);
        return insertedViolationID;
    }

    /**
     *
     * @param vid
     * @return
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public CodeViolation violation_getCodeViolation(int vid)
            throws IntegrationException, BObStatusException {
        
        CaseIntegrator ci = getCaseIntegrator();
        CodeViolation cv = ci.getCodeViolation(vid);
        
        try {
            cv = violation_configureCodeViolation(cv);
        } catch (BlobException ex) {
            System.out.println(ex);
            throw new BObStatusException("Could not retrieve blobs on Violations");
        }
        
        return cv;
    }

    /**
     * Unused as of March 2022--nice idea though
     *
     * @param vid
     * @return
     * @throws ViolationException
     * @throws IntegrationException
     * @throws SearchException
     * @throws BObStatusException
     */
    public CodeViolationPropCECaseHeavy violation_getCodeViolationPropCECaseHeavy(int vid) throws ViolationException, IntegrationException, SearchException, BObStatusException {
        PropertyCoordinator pc = getPropertyCoordinator();
        CodeViolationPropCECaseHeavy cvpcdh = null;
        if (vid != 0) {
            CodeViolation cv = violation_getCodeViolation(vid);
            cvpcdh = new CodeViolationPropCECaseHeavy(cv);

        } else {
            throw new ViolationException("Cannot construct a CodeViolationPropCECaseHeavy with violation ID of 0");
        }
        return cvpcdh;
    }

    /**
     * Uses date fields on the populated CodeViolation to determine a status
     * string and icon for UI Called by the integrator when creating a code
     * violation
     *
     * @param cv
     * @return the CodeViolation with correct icon and status
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    private CodeViolation violation_configureCodeViolation(CodeViolation cv) throws IntegrationException, BObStatusException, BlobException {
        CaseIntegrator ci = getCaseIntegrator();
        CourtEntityIntegrator cei = getCourtEntityIntegrator();
        BlobCoordinator bc = getBlobCoordinator();
        CodeCoordinator cc = getCodeCoordinator();
        cv.setBlobList(bc.getBlobLightList(cv));
        cv.setCitationIDList(cei.getCitationIDsByViolation(cv.getViolationID()));
        cv.setNoticeIDList(ci.novGetNOVIDList(cv));
        cv.setCannedFindingsCandidates(cc.assembleCannedFindingsByFindingsHolder(cv));
        return cv;
    }
    
    /**
     * Convenience method for iterating over the violations that aren't status heavy and
     * returns the status heavy variety
     * @param cvl
     * @return 
     */
    public List<CodeViolationStatusHeavy> violation_getCodeViolationStatusHeavyList(List<CodeViolation> cvl) throws IntegrationException, BObStatusException, BlobException{
        ArrayList<CodeViolationStatusHeavy> cvlsh = new ArrayList<>();
        if(cvl != null && !cvl.isEmpty()){
            for(CodeViolation cv: cvl){
                cvlsh.add(violation_getCodeViolationStatusHeavy(cv));
            }
        }
        
        return cvlsh;
    }
        
    /**
     * Creates the status heavy subclass of violations
     * @param v
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.BlobException 
     */
    public CodeViolationStatusHeavy violation_getCodeViolationStatusHeavy(CodeViolation v) 
            throws IntegrationException, BObStatusException, BlobException{

        SystemIntegrator si = getSystemIntegrator();
        
        CodeViolationStatusHeavy cv = new CodeViolationStatusHeavy(v);
        if(v == null || v.getViolationID() == 0){
            return cv;
        }
        // this is a good candidate for caching
        List<Citation> citationList = getCitationList(cv.getCitationIDList());

        // Branch status assignment logic based on citation pathway or non-citation pathway
        if(citationList != null && !citationList.isEmpty()){
            violation_configureViolationStatusCitation(cv, citationList);
        } else {
            violation_configureViolationStatusNonCitation(cv);
        }
        
        cv.setIcon(si.getIcon(Integer.parseInt(getResourceBundle(Constants.VIOLATIONS_BUNDLE)
                .getString(cv.getStatus().getIconPropertyName()))));

        
        return cv;
        
    }
    
    /**
     * Non-citation status assignment for code violations
     * @param cv
     * @return 
     */
    private CodeViolationStatusHeavy violation_configureViolationStatusNonCitation(CodeViolationStatusHeavy cv){
        StringBuilder sb = new StringBuilder();
        
        if(cv.getComplianceTimeStamp() != null){
             // we have a resolved violation
            cv.setStatus(CodeViolationStatusEnum.RESOLVED);
            sb.append("Compliance achieved on ").append(getPrettyDateNoTime(cv.getComplianceTimeStamp()));
            
        } else if (cv.getNullifiedTS() != null) {
            // nullification overrides all
            cv.setStatus(CodeViolationStatusEnum.NULLIFIED);
            sb.append("On ").append(getPrettyDateNoTime(cv.getNullifiedTS()));
        }  else if(cv.getTransferredTS() != null){
            // transferred then take precedence
            cv.setStatus(CodeViolationStatusEnum.TRANSFERRED);
            sb.append("On ").append(getPrettyDateNoTime(cv.getTransferredTS()));
        } else if (cv.getActualComplianceDate() == null) {
            // No compliance or nullification so check compliance window 
            if (cv.getDaysUntilStipulatedComplianceDate() >= ZERO) {
                // violation still within compliance timeframe
                cv.setStatus(CodeViolationStatusEnum.UNRESOLVED_WITHINCOMPTIMEFRAME);
                sb.append("Compliance due on ").append(getPrettyDateNoTime(cv.getStipulatedComplianceDate())).append(" (in ").append(cv.getDaysUntilStipulatedComplianceDate()).append(" days)");
            } else {
                // violation has NOT been cited, but is past compliance timeframe end date
                cv.setStatus(CodeViolationStatusEnum.UNRESOLVED_EXPIREDCOMPLIANCETIMEFRAME);
                sb.append("Compliance past due on ").append(getPrettyDateNoTime(cv.getStipulatedComplianceDate())).append(" (").append(cv.getDaysUntilStipulatedComplianceDate()).append(" day ago)");
            }
        } else {
          cv.setStatus(CodeViolationStatusEnum.UNKNOWN);
        }
        cv.setStatusString(sb.toString());
        
        return cv;
    }
    
    /**
     * Component of the citation status assignment flow
     * @param viol
     * @param citlist
     * @return 
     */
    private CodeViolationStatusHeavy violation_configureViolationStatusCitation(CodeViolationStatusHeavy cv, List<Citation> citationList){
        // lets look through our citations for unresolved outcomes concerning this violation
        boolean allCitedInstancesAdjudicated = true;
        for(Citation cit: citationList){
            if(cit.getViolationList() != null && !cit.getViolationList().isEmpty()){
                for(CitationCodeViolationLink cvvh: cit.getViolationList()){
                    if(cvvh.getViolationID() == cv.getViolationID() && cvvh.getCitVStatus() != null && !cvvh.getCitVStatus().isTerminalStatus()){
                        allCitedInstancesAdjudicated = false;
                        break;
                    } 
                }
            }
        }
        // set our adjudication status
        if(allCitedInstancesAdjudicated){
            cv.setStatus(CodeViolationStatusEnum.CITED_ADJUDICATED);
        } else {
            cv.setStatus(CodeViolationStatusEnum.CITED_UNRESOLVED);
        }
//        cv.setStatus(CodeViolationStatusEnum.CITED_UNRESOLVED);

        return violation_buildCitationStatusString(cv, citationList);
    }
    
    
    /**
     * Builds a human friendly note on citation-violation status
     * @param viol 
     */
    private CodeViolationStatusHeavy violation_buildCitationStatusString(CodeViolationStatusHeavy viol, List<Citation> citationList){
        StringBuilder sb = new StringBuilder();
        if(citationList != null && !citationList.isEmpty()){
            for(Citation cit: citationList){
                sb.append("Citation No. ").append(cit.getCitationNo()).append(Constants.FMT_HTML_BREAK);
                sb.append("Cited on ").append(getPrettyDateNoTime(cit.getDateOfRecord())).append(" by ").append(cit.getFilingOfficer().getUserHuman().getName()).append(Constants.FMT_HTML_BREAK)
                  .append("Status: ");
                for(CitationCodeViolationLink cvvl: cit.getViolationList()){
                    if(cvvl.getViolationID() == viol.getViolationID() && cvvl.getCitVStatus() != null){
                        sb.append(cvvl.getCitVStatus().getLabel());
                    }
                }
                sb.append(Constants.FMT_HTML_BREAK);
            }
            viol.setStatusString(sb.toString());
        } 
        return viol;
    }

    /**
     * Internal logic container for checking status of BObs before allowing
     * updating of the underlying CodeViolation record
     *
     * @param cse
     * @param cv
     * @return
     * @throws ViolationException
     */
    private void violation_verifyCodeViolationAttributes(CECase cse, CodeViolation cv) throws ViolationException {
        if (cse == null || cv == null) {
            throw new ViolationException("Cannot verify code violation attributes with null case or violation");
        }
        if (cv.getCeCaseID() != cse.getCaseID()) {
            throw new ViolationException("Violation is not mapped to parent case ID");
        }

        if (cse.getPriority() != null && !cse.getPriority().isQualifiesAsOpen()) {
            throw new ViolationException("Cannot update code violations on closed cases!");
        }
        if (cse.getStatusBundle() != null && cse.getStatusBundle().getPhase() == CasePhaseEnum.Closed) {
            throw new ViolationException("Cannot update code violations on closed cases!");
        }
        if (cv.getStipulatedComplianceDate().isBefore(cv.getDateOfRecord())) {
            throw new ViolationException("Stipulated compliance date cannot be before the violation's date of record");
        }

    }

    /**
     * Asks the given field inspection for its failed ordinances that are slated
     * for migration and injects them into CodeViolations for the user to tweak.
     * When the user is done, they should all get injected into the
     * CodeViolationMigratingSettings object and sent to the method in this
     * class which will conduct the actual migration
     *
     * @param fin from which the failed items are to be extracted
     * @return a list, perhaps with the CodeViolations that correspond to each
     * of the failed items
     * @throws BObStatusException with null inputs or insufficient inputs
     *
     */
    public List<CodeViolation> violation_buildViolationListFromFailedInspectionItems(FieldInspection fin)
            throws BObStatusException {

        if (fin == null) {
            throw new BObStatusException("Cannot build violation list from null field inspection");
        }

        List<CodeViolation> cvl = new ArrayList<>();
        System.out.println("CaseCoordinator.violation_buildViolationListFromFailedInspectionItems | current FIN ID: " + fin.getInspectionID());
        List<OccInspectedSpaceElement> ecel = fin.extractFailedItemsForCECaseMigration(false);
        System.out.println("CaseCoordinator.violation_buildViolationListFromFailedInspectionItems | violated ecel size: " + ecel.size());
        if (!ecel.isEmpty()) {
            for (OccInspectedSpaceElement oise : ecel) {
                CodeViolation cv = new CodeViolation();
                cv.setViolatedEnfElement((EnforceableCodeElement) oise);
                cv.setDescription(oise.getInspectionNotes());
                cv.setSeverityIntensity(oise.getFaillureSeverity());
                cv.setCreatedBy(oise.getLastInspectedBy());
                cv.setLastUpdatedBy(oise.getLastInspectedBy());
                cv.setPenalty(oise.getNormPenalty());
                cv.setBlobList(oise.getBlobList());
                cvl.add(cv); // method will make new violation
            }
        }
        return cvl;
    }
    
    /**
     * Iterates over a list of cecases and only returns those which can receive
     * new violations during the migration process either from another CECase
     * or from a field inspection attached to either a cecase or an occ period
     * @param targetProp
     * @param ua
     * @return a list, perhaps containing one or more eligible cases
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public List<CECase> cecase_assembleListOfOpenCases(PropertyDataHeavy targetProp, UserAuthorized ua) throws BObStatusException{
        PropertyCoordinator pc = getPropertyCoordinator();
        List<CECase> cl;
        List<CECase> candidateList = new ArrayList<>();
        try {
            cl = cecase_upcastCECaseDataHeavyList(pc.assemblePropertyDataHeavy(targetProp, ua).getCeCaseList());
        } catch (IntegrationException | BObStatusException | BlobException | SearchException ex) {
            throw new BObStatusException("Could not rebuild property to assemble case list");
        }
        if(cl != null && !cl.isEmpty()){
            for(CECase cse: cl){
                //skip closed cases
                if(cse.getClosingDate() != null){
                    continue;
                }
                candidateList.add(cse);
            }
        }
        return candidateList;
    }

    /**
     * Generator method for violation migration settings, with sensible defaults
     * applied before returning
     *
     * @return
     */
    public CodeViolationMigrationSettings violation_migration_getCodeViolationMigrationSettingsSkeleton() {
        CodeViolationMigrationSettings cvms = new CodeViolationMigrationSettings();
        cvms.setViolationListReadyForInsertion(new ArrayList<>());
        cvms.setViolationListSuccessfullyMigrated(new ArrayList<>());
        cvms.setLinkInspectedElementPhotoDocsToViolation(true);
        cvms.setUseInspectedElementFindingsAsViolationFindings(true);
        cvms.setMigrateWithoutMarkingSourceViolsAsTransferred(false);
        cvms.appendToMigrationLog("Init complete");

        return cvms;
    }

    /**
     * Iterates over a list of case violations and extracts those without
     * compliance, or nullification
     *
     * @param cse
     * @return a list, perhaps containing some violations
     */
    public List<CodeViolation> violation_migration_assembleViolationsForMigrationFromCECase(CECase cse) throws ViolationException {
        if (cse == null) {
            throw new ViolationException("Cannot extract violations from null case");
        }
        List<CodeViolation> vtxferl = new ArrayList<>();
        if (cse.getViolationList() != null && !cse.getViolationList().isEmpty()) {
            for (CodeViolation cv : cse.getViolationList()) {
                if (cv.getActualComplianceDate() == null
                        && cv.getNullifiedTS() == null) {
                    vtxferl.add(cv);
                }
            }
        }
        return vtxferl;
    }

    /**
     * The primary entry way for migrating code violations from all sorts of
     * places to either a new or existing code enforcement case
     *
     * @param cvms containing the migration pathway and all the necessary
     * trimmings
     * @param ua
     * @return a list, perhaps with one or more IDs of the new Code Violations
     * written to the DB
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public CodeViolationMigrationSettings violation_migration_migrateViolations(CodeViolationMigrationSettings cvms, UserAuthorized ua)
            throws BObStatusException, AuthorizationException {
        if (cvms == null) {
            throw new BObStatusException("Cannot migrate code violations with null settings or null pathway in that settings or violationlist or empty list");
        }
        
        if(!permissionsCheckpointViolationAdd(ua)){
            throw new AuthorizationException("Authorization exception: Current user does not have sufficient permissions to attach violations to cases, and therefore cannot migrate vioaltions to an open or existing case");
            
        }

        List<Integer> freshViolationList = new ArrayList<>();

        try {
            // setup basic configuration stuff
            violation_migration_auditMigrationSettings(cvms);
            violation_migration_configureTargetCase(cvms, ua);
            violation_migration_assembleFreshViolations(cvms);

            
            // then write the new violations
            for (CodeViolation cv : cvms.getViolationListReadyForInsertion()) {
                freshViolationList.add(violation_attachViolationToCase(cv, cvms.getCeCaseParent(), cvms.getUserEnactingMigration()));
            }

            if(!cvms.isMigrateWithoutMarkingSourceViolsAsTransferred()){
                violation_migration_recordTransferOfTransferrables(cvms);
            } else {
                cvms.appendToMigrationLog("User indiciated NOT to mark source violations as transferrred; skipping this step.");
            }
            
            cvms.getViolationListSuccessfullyMigrated().addAll(violation_getCodeViolations(freshViolationList));
            
            ceCaseCacheManager.flush(cvms.getCeCaseParent());
            ceCaseCacheManager.flush(cvms.getSourceCase());
        } catch (ViolationException | BObStatusException | EventException | IntegrationException | SearchException | BlobException ex) {
            System.out.println(ex);
            String exname = "Exception caught: " + ex.getClass().getName();
            cvms.appendToMigrationLog(exname);
            cvms.appendToMigrationLog(ex.getMessage());
        }

        return cvms;
    }

    /**
     * Iterates over a list of instances of Transferrable and records their
     * movement in the DB
     *
     * @param trables
     * @throws ViolationException
     */
    private void violation_migration_recordTransferOfTransferrables(CodeViolationMigrationSettings cvms) throws ViolationException, IntegrationException {
        if (cvms == null) {
            throw new ViolationException("Cannot mark transferrables as transferred with null input");
        }
        CaseIntegrator ci = getCaseIntegrator();
        List<IFace_transferrable> trlist = cvms.getTransferrableList();
        if(trlist != null || !trlist.isEmpty()){

            for (IFace_transferrable trable : cvms.getTransferrableList()) {
                trable.setTransferredBy(cvms.getUserEnactingMigration());
                trable.setTransferredTS(LocalDateTime.now());
                trable.setTransferredToCECaseID(cvms.getCeCaseParent().getCaseID());

                ci.updateTransferrable(trable);
                StringBuilder sb = new StringBuilder();
                sb.append("Recorded transfer of transferrable in table: ");
                sb.append(trable.getTransferEnum().getTargetTableID());
                sb.append(" With PK:  ");
                sb.append(trable.getDBKey());
                System.out.println("CaseCoordinator.violation_migration_recordTransferOfTransferrables : " + sb.toString());
                cvms.appendToMigrationLog(sb.toString());
            }
            
        } else {
            throw new ViolationException("Cannot transfer with empty list of transferrables");
        }

    }

    /**
     * Internal logic checker for code violation migration settings
     *
     * @param cvms
     * @throws BObStatusException
     */
    private void violation_migration_auditMigrationSettings(CodeViolationMigrationSettings cvms) throws ViolationException {

        if (cvms.getPathway() == null) {
            throw new ViolationException("Audit failure: Null pathway");
        }
        
        if (cvms.getViolationListToMigrate() == null) {
            throw new ViolationException("Audit failure: Null violation list to migrate");
        }
        
        if (cvms.getViolationListToMigrate().isEmpty()) {
            throw new ViolationException("Audit failure: Empty violation list to migrate");
        }
        
        if (cvms.getUserEnactingMigration() == null) {
            throw new ViolationException("Cannot migrate with null user authorized doing migration");
        }
    }

    /**
     * Internal logic organ for examining the migration settings object and
     * building a new case if needed
     *
     * @param cvms not null
     */
    private void violation_migration_configureTargetCase(CodeViolationMigrationSettings cvms, UserAuthorized ua)
            throws ViolationException, IntegrationException, BObStatusException, SearchException, BlobException, AuthorizationException {
        if (cvms == null) {
            throw new ViolationException("Cannot configure case with null cvms");
        }

        if (cvms.getPathway().isUseExistingCase()) {
            if (cvms.getCeCaseParent() == null) {
                throw new ViolationException("Cannot migrate to an existing case with null case on settings");
            }
            // We need a new case
        } else {
            if (cvms.getProp() == null) {
                throw new ViolationException("Cannot make new case for violations with null property");
            }
            CECase targetCase = cecase_initCECase(cvms.getProp(), cvms.getUserEnactingMigration());
            if (cvms.getNewCaseDateOfRecord() != null) {
                targetCase.setOriginationDate(cvms.getNewCaseDateOfRecord());
            } else {
                targetCase.setOriginationDate(LocalDateTime.now());
            }
            targetCase.setCaseName(cvms.getNewCECaseName());
            targetCase.setCaseManager(cvms.getNewCECaseManager());
            targetCase.setOriginationEventCategory(cvms.getNewCECaseOriginationEventCategory());

            try {
                targetCase = cecase_getCECase(cecase_insertNewCECase(targetCase, cvms.getProp(), cvms.getUserEnactingMigration(), null), ua);
            } catch (EventException ex) {
                System.out.println(ex);
                cvms.appendToMigrationLog("FATAL: Could not insert new CECase due to event exception.");
            }

            cvms.setCeCaseParent(targetCase);
        }
    }

    /**
     * Internal organ for actually setting up each new violation from the list
     * of violations to migrate
     *
     * @param cvms Caller is responsible for having audited this
     */
    private void violation_migration_assembleFreshViolations(CodeViolationMigrationSettings cvms) throws ViolationException {
        if (cvms.getViolationListReadyForInsertion() == null) {
            cvms.setViolationListReadyForInsertion(new ArrayList<>());
        }
        for (CodeViolation cv : cvms.getViolationListToMigrate()) {
            StringBuilder sb = new StringBuilder("Preparing to migrate violation ID: ");
            sb.append(cv.getViolationID());
            cvms.appendToMigrationLog(sb.toString());

            CodeViolation targetViol = new CodeViolation();

            targetViol.setCeCaseID(cvms.getCeCaseParent().getCaseID());
            targetViol.setViolatedEnfElement(cv.getViolatedEnfElement());

            if (cvms.getViolationDateOfRecord() != null) {
                targetViol.setDateOfRecord(cvms.getViolationDateOfRecord());
            } else {
                targetViol.setDateOfRecord(LocalDateTime.now());
            }

            targetViol.setStipulatedComplianceDate(cvms.getUnifiedStipComplianceDate());

            targetViol.setDescription(cv.getDescription());
            if(targetViol.getDescription() == null){
                targetViol.setDescription("[no description]");
            }
            targetViol.setPenalty(cv.getPenalty());

            // deal with blobs
            if (cvms.isLinkInspectedElementPhotoDocsToViolation()) {
                if(cv.getBlobList() != null && !cv.getBlobList().isEmpty()){
                    System.out.println("CaseCoordinator.violation_migration_assembleFreshViolations | incoming code violation blob list size: " + cv.getBlobList().size());
                    targetViol.setBlobList(cv.getBlobList());
                    
                } else {
                    System.out.println("CaseCoordinator.violation_migration_assembleFreshViolations | null or empty incoming cv blob list");
                }
            }
            targetViol.setSeverityIntensity(cv.getSeverityIntensity());
            targetViol.setCreatedBy(cvms.getUserEnactingMigration());
            targetViol.setLastUpdatedBy(cvms.getUserEnactingMigration());

            targetViol.setNotes(violation_migration_buildTargetViolationNotes(cv, cvms));

            // finally, add our new violation to the list for insertion
            cvms.getViolationListReadyForInsertion().add(targetViol);

        }
    }

    /**
     * Internal logic organ for determining the target violation stip comp date
     *
     * @param cv
     * @param cvms
     * @return
     */
    private LocalDateTime violation_migration_determineTargetViolStipCompDate(CodeViolation cv, CodeViolationMigrationSettings cvms) throws ViolationException {
        if (cv == null || cvms == null) {
            throw new ViolationException("Cannot determine new stip date with null cv or settings object");
        }
        LocalDateTime targetVStipDate;
        if (cv.getStipulatedComplianceDate() != null) {
            targetVStipDate = cv.getStipulatedComplianceDate();
        } else {
            if (cvms.getUnifiedStipComplianceDate() != null) {
                targetVStipDate = cvms.getUnifiedStipComplianceDate();
            } else {
                if (cv.getViolatedEnfElement().getNormDaysToComply() != 0) {
                    targetVStipDate = LocalDateTime.now().plusDays(cv.getViolatedEnfElement().getNormDaysToComply());
                } else {
                    targetVStipDate = LocalDateTime.now().plusDays(FALLBACK_DAYSTOCOMPLY);
                }
            }
        }
        return targetVStipDate;
    }

    private static final String VIOLMIG_INJECTION_MARKER_NOW = "[NOW]";
    private static final String VIOLMIG_INJECTION_MARKER_FIELDINSPECTIONID = "[FINID]";
    private static final String VIOLMIG_INJECTION_MARKER_SOURCECECASEID = "[CASEID]";
    private static final String VIOLMIG_INJECTION_MARKER_SOURCEOCCPERIOD = "[PERIODID]";

    /**
     * Internal organ for building a nice, descriptive note on the violations
     * being migrated so we know where they came from
     * TODO: Get me working!
     *
     * @param cv yet to be inserted that needs the note
     * @param cvms with all the goodies
     */
    private String violation_migration_buildTargetViolationNotes(CodeViolation cv, CodeViolationMigrationSettings cvms) throws ViolationException {
        if (cv == null || cvms == null) {
            throw new ViolationException("Cannot determine new stip date with null cv or settings object");
        }

        String vnote = cvms.getPathway().getViolationNoteInjectableString();

        String now = LocalDateTime.now().toString();
        vnote = vnote.replace(VIOLMIG_INJECTION_MARKER_NOW, now);

        if (cvms.getSourceInspection() != null) {
            String insID = String.valueOf(cvms.getSourceInspection().getInspectionID());
            if (insID != null) {
                vnote = vnote.replace(VIOLMIG_INJECTION_MARKER_FIELDINSPECTIONID, insID);
            }
        }
        if (cvms.getSourceCase() != null) {
            String scaseID = String.valueOf(cvms.getSourceCase().getCaseID());
            if (scaseID != null) {
                vnote = vnote.replace(VIOLMIG_INJECTION_MARKER_SOURCECECASEID, scaseID);
            }
        }
        if (cvms.getSourceOccPeriod() != null) {
            String speriodID = String.valueOf(cvms.getSourceOccPeriod().getPeriodID());
            if (speriodID != null) {
                vnote = vnote.replace(VIOLMIG_INJECTION_MARKER_SOURCEOCCPERIOD, speriodID);
            }
        }
        String ln = "Violation Note: " + vnote;
        // BROKEN as of 21 SEP 2022
        // Placeholder injection
//        cvms.appendToMigrationLog(ln);
        cvms.appendToMigrationLog("This violation was migrated from a field inspection on this case, a different case, a permit file!");

        return vnote;
    }

    /**
     * CodeViolation Factory: 
     * Logic holder for injecting a code element into a code violation and
     * setting sensible default values based on preferences by muni
     *
     * @param cv can be null, if not null, the given ece will be injected into that object
     * otherwise we'll make a new one
     * 
     * @param ece to be injected
     * @return the CodeViolation with injected ordinance
     * @throws BObStatusException
     */
    public CodeViolation violation_injectOrdinance(CodeViolation cv,
            EnforceableCodeElement ece)
            throws BObStatusException, IntegrationException {

        if (ece != null) {

            if (cv == null) {
                cv = new CodeViolation();
            }

            int daysInFuture;
            if (ece.getNormDaysToComply() != 0) {
                daysInFuture = ece.getNormDaysToComply();
            } else {
                daysInFuture = FALLBACK_DAYSTOCOMPLY;
            }
            cv.setViolatedEnfElement(ece);
            
            cv.setStipulatedComplianceDate(LocalDateTime.now().plusDays(daysInFuture));
            cv.setDateOfRecord(LocalDateTime.now());
            cv.setPenalty(ece.getNormPenalty());
            if(cv.getSeverityIntensity() == null){
                SystemCoordinator sc = getSystemCoordinator();
                cv.setSeverityIntensity(sc.getIntensityClass(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("defaultviolationseverity"))));
            }

        } else {
            throw new BObStatusException("Cannot inject null ordinance or cannot inject into null code violation");
        }
        return cv;
    }
    
    /**
     * Convenience method for updating a batch of code violations
     * @param cse
     * @param cvList
     * @param ua
     * @throws BObStatusException
     * @throws ViolationException
     * @throws IntegrationException 
     */
    public void violation_updateCodeViolationList(CECaseDataHeavy cse, List<CodeViolation> cvList, UserAuthorized ua) throws BObStatusException, ViolationException, IntegrationException{
        if (cse == null || cvList == null || ua == null || cvList.isEmpty()) {
            throw new BObStatusException("Cannot update a code violation given a null case, violation, or user");
        }
        for(CodeViolation cv: cvList){
            violation_updateCodeViolation(cse, cv, ua);
        }
    }

    /**
     * Access point for updating a CodeViolation record - for description changes,
     *
     * @param cse
     * @param cv
     * @param u
     * @throws ViolationException
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public void violation_updateCodeViolation(CECaseDataHeavy cse,
            CodeViolation cv,
            UserAuthorized u)
            throws ViolationException,
            IntegrationException,
            BObStatusException {

        EventCoordinator ec = getEventCoordinator();
        CaseIntegrator ci = getCaseIntegrator();
        EventIntegrator ei = getEventIntegrator();

        if (cse == null || cv == null || u == null) {
            throw new BObStatusException("Cannot update a code violation given a null case, violation, or user");
        }

        cv.setLastUpdatedBy(u);
        ci.updateCodeViolation(cv);
        ceCaseCacheManager.flush(cv);

    } // close method

    /**
     * Updates only the notes field on violation.This method takes care of
     * pulling out existing notes and prepending the new notes
     *
     * @param mbp
     * @param viol
     * @param ua
     * @throws BObStatusException
     * @throws IntegrationException
     */
    public void violation_updateNotes(MessageBuilderParams mbp, CodeViolation viol, UserAuthorized ua) throws BObStatusException, IntegrationException {
        CaseIntegrator ci = getCaseIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        if (viol == null || mbp == null) {
            throw new BObStatusException("Cannot append if notes, case, or user are null");
        }

        viol.setNotes(sc.appendNoteBlock(mbp));
        viol.setLastUpdatedBy(ua);
        ci.updateCodeViolationNotes(viol);
        
        ceCaseCacheManager.flush(viol);
    }
    
    /**
     * Utility method for deactivating an entire list of CodeViolations
     * @param cvList
     * @param ua
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public void violation_deactivateCodeViolations(List<CodeViolation> cvList, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(cvList != null && ua != null && !cvList.isEmpty()){
            for(CodeViolation cv: cvList){
                violation_deactivateCodeViolation(cv, ua);
            }
        }
        
    }

    /**
     * Attempts to deactivate a code violation, but will thow an Exception if
     * the CodeViolation has been used in a notice or in a citation
     *
     * @param cv
     * @param ua If a system admin, internal business rules checks will be bypassed!!
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void violation_deactivateCodeViolation(CodeViolation cv, UserAuthorized ua) throws BObStatusException, IntegrationException {
        SystemIntegrator si = getSystemIntegrator();

        if(ua == null || cv == null){
            throw new BObStatusException("Cannot deac Code Violation with null user input or violation");
        }
         
        if(!ua.getKeyCard().isHasSysAdminPermissions()){
            if (cv.getCitationIDList() != null && !cv.getCitationIDList().isEmpty()) {
                throw new BObStatusException("Cannot deactivate a violation that has been used in a citation");
            }

            if (cv.getNoticeIDList() != null && !cv.getNoticeIDList().isEmpty()) {
                throw new BObStatusException("Cannot deactivate a violation that has been used in a notice");
            }
        }
        if(cv.getCreatedTS().isBefore(LocalDateTime.now().minusDays(VIOLATION_DEAC_ALLOW_BUFFER_DAYS))){
            throw new BObStatusException("Cannot deactivate a violation that has been added to a case more than 24 hours ago");
        }

        si.deactivateTrackedEntity(cv, ua);
        
        ceCaseCacheManager.flush(cv);

    }

    /**
     * Attempts to deactivate a code violation, but will thow an Exception if
     * the CodeViolation has been used in a notice or in a citation
     *
     * @param cv
     * @param ua
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    private void violation_NullifyCodeViolation(CodeViolation cv, LocalDateTime nullifyDOR, String reason, UserAuthorized ua) throws BObStatusException, IntegrationException {
        CaseIntegrator ci = getCaseIntegrator();

        if (cv == null || ua == null || nullifyDOR == null) {
            throw new BObStatusException("Cannot nullify a violation with null CV or User");
        }
        if(cv.getActualComplianceDate() != null){
            throw new BObStatusException("Cannot nullify a violation once comlpliance has been achieved");
        }
        if(cv.getDeactivatedTS() != null){
            throw new BObStatusException("Cannot nullify an inactive violation");
        }
        if(nullifyDOR.isBefore(cv.getDateOfRecord())){
            throw new BObStatusException("Cannot record nullification date before violation date of record");
        }
        
        // configure object
        cv.setNullifiedTS(nullifyDOR);
        cv.setNullifiedUser(ua);
        System.out.println("CaseCoordinator.violation_NullifyCodeViolation: ready to nullify in integrator");
        cv.setLastUpdatedBy(ua);
        
        // write to DB
        ci.updateCodeViolation(cv);
        
        MessageBuilderParams mbp = new MessageBuilderParams(cv.getNotes());
        mbp.setHeader("Violation Nullified");
        mbp.appendToNewMessageContent("Reason for nullification: ");
        mbp.appendToNewMessageContent(reason);
        violation_updateNotes(mbp, cv, ua);
        
        ceCaseCacheManager.flush(cv);

        
    }

    

    /**
     * Wires up a given violation's findings to be the default findings for
     * violations of the given enforcable code element inside the CodeViolation
     * passed in TODO: Add option of checking that user's permissions and
     * updating all those muni's code sets as well
     *
     * @param cv
     * @param ua
     * @param cascadeToAuthCodebooks triggers the calling of an internal method
     * for updating all code books that are default in any muni in which the
     * UserAuthorized has enforcement official permissions (or better)
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public void violation_makeFindingsDefaultInCodebook(CodeViolation cv, UserAuthorized ua, boolean cascadeToAuthCodebooks) throws BObStatusException, IntegrationException, AuthorizationException {

        if (cv == null || ua == null) {
            throw new BObStatusException("Cannot update default findings with null violation, codeset, or user");
        }

        CodeCoordinator codeCoor = getCodeCoordinator();
        codeCoor.updateEnfCodeElementMakeFindingsDefault(cv.getDescription(), cv.getViolatedEnfElement(), ua);

    }
    
  

    /**
     * Logic gateway for updates to a code violation's stipulated compliance
     * date
     *
     * @param cv with an updated stip date only. For other violation changes, see 
     * methods prefixed with violation_ in this Coordinator
     * @param extEvDesc
     * @param cse on which the code violation for update should be connected. Verification 
     * will occur
     * @param ua
     * @throws com.tcvcog.tcvce.domain.BObStatusException this violation doesn't go with this case
     * @throws com.tcvcog.tcvce.domain.ViolationException If stip comp date on or before origination date
     * @throws com.tcvcog.tcvce.domain.IntegrationException I can't write to the db
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    private void violation_extendStipulatedComplianceDate(CodeViolation cv, LocalDateTime revisedStipCompDate, String reason, CECaseDataHeavy cse, UserAuthorized ua)
            throws BObStatusException, ViolationException, IntegrationException, AuthorizationException {

        SystemCoordinator sc = getSystemCoordinator();
        
        if (cv == null || cse == null || ua == null) {
            throw new BObStatusException("Cannot extend compliance date given a null violation, case, or user");
        }
        if(!permissionsCheckpointViolationExtendStipCompDate(ua)){
            throw new AuthorizationException("Permissions check failure: current permissions profile prohibits current user from extending stipulated compliance date");
        }
        if (cv.getStipulatedComplianceDate() == null || revisedStipCompDate == null) {
            throw new BObStatusException("Cannot extend a null stipulated compliance date or extend to a null date");
        }
        if(revisedStipCompDate.isBefore(cv.getDateOfRecord())){
            throw new BObStatusException("Cannot update a violation to have a stip comp date before the violation's date of record.");
        }
        
        // build note
        MessageBuilderParams mbp = new MessageBuilderParams(cv.getNotes());
        mbp.setUser(ua);
        mbp.setHeader("Stipulated compliance date extended");
        
        mbp.appendToNewMessageContent("Previous stipulated compliance date of ");
        mbp.appendToNewMessageContent(DateTimeUtil.getPrettyDate(cv.getStipulatedComplianceDate()));
        mbp.appendToNewMessageContent(" has been changed to ");
        mbp.appendToNewMessageContent(DateTimeUtil.getPrettyDate(revisedStipCompDate));
        mbp.appendToNewMessageContent(" by user: ");
        mbp.appendToNewMessageContent(ua.getUserHuman().getName());
        mbp.appendToNewMessageContent(" on ");
        mbp.appendToNewMessageContent(DateTimeUtil.getPrettyDate(LocalDateTime.now()));
        mbp.appendToNewMessageContent(". Reason for extension: ");
        mbp.appendToNewMessageContent(reason);
        cv.setNotes(sc.appendNoteBlock(mbp));
      
        // inject our new stip comp date
        cv.setStipulatedComplianceDate(revisedStipCompDate);
        
        violation_updateCodeViolation(cse, cv, ua);
        violation_updateNotes(mbp, cv, ua);
        
        ceCaseCacheManager.flush(cv);
    }
    
    
    /**
     * One and only pathway for recording the compliance achievement for a code violation
     *
     * @param cse
     * @param cv
     * @param u
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.ViolationException
     * @throws com.tcvcog.tcvce.domain.EventException
     */
    private void violation_recordCompliance( CodeViolation cv, LocalDateTime compDate, String reason, CECaseDataHeavy cse, UserAuthorized u)
            throws IntegrationException, BObStatusException, ViolationException, EventException {

        if (cse == null || cv == null || u == null || compDate == null) {
            throw new BObStatusException("Cannot record compliance on null case, viol, or user");
        }
        if (cv.getNullifiedTS() != null) {
            throw new ViolationException("Cannot set compliance on a nullified violation");
        }
        if (cv.getDeactivatedTS() != null) {
            throw new ViolationException("Cannot set compliance on a deactivated violation");
        }
        if (compDate == null) {
            throw new ViolationException("Cannot set compliance with null actual compliance date");
        }
        if(compDate.isBefore(cv.getDateOfRecord())){
            throw new ViolationException("Date of compliance cannot be before violation date of record");
        }
        CaseIntegrator ci = getCaseIntegrator();

        cv.setActualComplianceDate(compDate);
        // update violation record for compliance
        cv.setComplianceUser(u);
        cv.setLastUpdatedBy(u);

        ci.updateCodeViolationCompliance(cv);
        
        // write notes
        MessageBuilderParams mbp = new MessageBuilderParams(cv.getNotes());
        mbp.setHeader("Compliance achieved!");
        mbp.appendToNewMessageContent("Officer mitigation actions observed: ");
        mbp.appendToNewMessageContent(reason);
        violation_updateNotes(mbp, cv, u);
        
        ceCaseCacheManager.flush(cv);
        // Based on user feedback, we are suspending auto-closing of case
        // upon all violation coming into compliance
        // as of 2-DEC-2021

//        violation_checkForFullComplianceAndCloseCaseIfTriggered(cse, u);
    }
    
    
    /**
     * factory for vbatch operation objects
     * @param ua
     * @param csedh
     * @return 
     */
    public CodeViolationBatchOperationConfiguration violation_getCodeViolationBatchOperationConfigurationSkeleton(UserAuthorized ua, CECaseDataHeavy csedh) throws BObStatusException{
        
        if(ua == null || csedh == null){
            throw new BObStatusException("Cannot create new batch with null input user or case");
        }
        CodeViolationBatchOperationConfiguration vbatch = new CodeViolationBatchOperationConfiguration(csedh, ua);
        vbatch.setDateOfRecordForOperation(LocalDateTime.now());
        // somewhat arbitrary default
        vbatch.setFormExtendStipCompUsingDate(false); // update using days into future by default
        vbatch.setComplianceExtensionDays(DEFAULT_EXTENSIONDAYS);
        vbatch.setSelectedCodeViolations(new ArrayList<>());
        return vbatch;
        
    }
    
    
    /**
     * Receives violation update requests for one or more violation
     * @param vBatchConfig with non null case, operation, user, and non empty violation list
     * @return the same config bundle passed in
     * @throws BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.EventException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public CodeViolationBatchOperationConfiguration violation_batchOperationExecute(CodeViolationBatchOperationConfiguration vBatchConfig) 
            throws BObStatusException, IntegrationException, EventException, AuthorizationException{
        EventCoordinator ec = getEventCoordinator();
        
        if(vBatchConfig == null || vBatchConfig.getCeCase() == null || vBatchConfig.getRequestingUser() == null || vBatchConfig.getOperation() == null){
            throw new BObStatusException("Cannot undertake batch with null violation list, operation, user, or case");
        }
        if(vBatchConfig.getSelectedCodeViolations().isEmpty()){
            throw new BObStatusException("Cannot apply batch operation to empty violation list");
        }
        
        System.out.println("CaseCoordinator.CodeViolationBatchOperationConfiguration | commencing batch operation ");
        StringBuilder sb = new StringBuilder();
        sb.append(vBatchConfig.getOperation().getLabel()).append(Constants.FMT_HTML_BREAK);
        sb.append(vBatchConfig.getOperation().getOperationReasonFieldLabel()).append(": ").append(vBatchConfig.getOperationReason()).append(Constants.FMT_HTML_BREAK);
        
        sb.append("Violation count: ").append(vBatchConfig.getSelectedCodeViolations().size()).append(Constants.FMT_HTML_BREAK);
        sb.append("See violation notes for details.");
        for(CodeViolation cv: vBatchConfig.getSelectedCodeViolations()){
            vBatchConfig.incrementProcessCount();
            try{
                switch(vBatchConfig.getOperation()){
                    case STIPCOMP_EXTENSION -> {
                        // setup our new comp date based on user extension mechanism
                        if(!vBatchConfig.isFormExtendStipCompUsingDate()){
                            System.out.println("CaseCoordinator.CodeViolationBatchOperationConfiguration | extending using day count");
                            vBatchConfig.setUpdatedStipulatedComplianceDate(LocalDateTime.now().plusDays(vBatchConfig.getComplianceExtensionDays()));
                        }
                        violation_extendStipulatedComplianceDate(cv, vBatchConfig.getUpdatedStipulatedComplianceDate(), vBatchConfig.getOperationReason(), vBatchConfig.getCeCase(), vBatchConfig.getRequestingUser());
                    }
                    case COMPLIANCE -> {
                        violation_recordCompliance(cv, vBatchConfig.getDateOfRecordForOperation(), vBatchConfig.getOperationReason(), vBatchConfig.getCeCase(), vBatchConfig.getRequestingUser());
                    }
                    case NULLIFY -> {
                        violation_NullifyCodeViolation(cv, vBatchConfig.getDateOfRecordForOperation(),vBatchConfig.getOperationReason() ,vBatchConfig.getRequestingUser());
                    }
                } // close switch
                vBatchConfig.incrementSuccessCount();
                ceCaseCacheManager.flush(cv);
            } catch (AuthorizationException | BObStatusException | EventException | IntegrationException | ViolationException ex){
                System.out.println(ex);
                sb.append("Fatal error processing violation: ").append(ex.getMessage()).append(Constants.FMT_HTML_BREAK);
                vBatchConfig.incrementErrorCount();
            } // close try
        } // close loop over violations in batch
        
        // only log an event if we have at least one successful operation
        if(vBatchConfig.getSuccessCount() >= 1){
            EventCategory evcat = ec.getEventCategory(Integer.parseInt(getResourceBundle(Constants.EVENT_CATEGORY_BUNDLE).getString(vBatchConfig.getOperation().getEventCatLoggingKey())));
            System.out.println("CaseCoordinator.CodeViolationBatchOperationConfiguration | done iterating | writing event to case ID " + vBatchConfig.getCeCase().getCaseID());
            EventCnF logEvent = ec.initEvent(vBatchConfig.getCeCase(), evcat);
            logEvent.setDescription(sb.toString());
            ec.addEvent(logEvent, vBatchConfig.getCeCase(), vBatchConfig.getRequestingUser());
        }
        
        
        vBatchConfig.setOperationLog(sb);
        // create case note and post batch operation configuration
        
        return vBatchConfig;
    }

    /**
     * Utility method for building  a refreshed and sorted list of fresh
     * code violations from a list of violations.
     * 
     * @param cvList
     * @return
     * @throws IntegrationException
     * @throws BObStatusException
     */
    public List<CodeViolation> violation_refreshCodeViolationList(List<CodeViolation> cvList) throws IntegrationException, BObStatusException {
       List<Integer> violationIDs = null;
       if(cvList != null && !cvList.isEmpty()){
            violationIDs = cvList.stream()
                    .map(CodeViolation::getViolationID)
                    .collect(Collectors.toList());
            List<CodeViolation> freshVList = violation_getCodeViolations(violationIDs);
            Collections.sort(freshVList);
            return freshVList;
       } else {
           return new ArrayList<>();
       }
    }
    
    /**
     *
     * @param cvIDList
     * @return
     * @throws IntegrationException
     */
    public List<CodeViolation> violation_getCodeViolations(List<Integer> cvIDList) throws IntegrationException, BObStatusException {
        List<CodeViolation> vl = new ArrayList<>();

        if (cvIDList != null && !cvIDList.isEmpty()) {
            for (Integer i : cvIDList) {
                vl.add(violation_getCodeViolation(i));
            }
        }
        return vl;
    }
    
     /**
     * Filters the enfList as per filter text filterText is matched against
     * ordinance header string and/or non IRC style header string
     *
     * @param enfList
     * @param filterText
     * @param filterFullText pass through to search string builder
     * @return
     */
    public List<EnforceableCodeElement> violationAction_configureEnforcableCodeElementListFiltered(
            List<EnforceableCodeElement> enfList,
            String filterText,
            boolean filterFullText) {
        if (Objects.nonNull(enfList))
        {
            if (Objects.nonNull(filterText) && !filterText.isEmpty())
            {
                return enfList.stream()
                        .filter(element -> violationAction_isOrdinanceFilterMatches(filterText, element, filterFullText))
                        .collect(Collectors.toList());
            } else
            {
                return enfList;
            }
        }
        return new ArrayList<>();
    }

    /**
     * To check ordinance filter text matches with header string or non IRC
     * Style Header String
     *
     * @param filterText
     * @param enfCodeElement
     * @return
     */
    private boolean violationAction_isOrdinanceFilterMatches(String filterText, 
                                                            EnforceableCodeElement enfCodeElement,
                                                            boolean filterFullText) {
        StringBuilder sb = new StringBuilder();
        if (Objects.nonNull(enfCodeElement.getHeaderString()))
        {
            sb.append(enfCodeElement.getHeaderString());
        }
        if (Objects.nonNull(enfCodeElement.getNonIrcStyleHeaderString()))
        {
            sb.append(enfCodeElement.getNonIrcStyleHeaderString());
        }
        if (filterFullText && enfCodeElement.getOrdTechnicalText() != null)
        {
            sb.append(enfCodeElement.getOrdTechnicalText());
        }
        return StringUtil.isStringContainsI(sb.toString(), filterText);
    }
    
    /**
     * BITS method: not previously commented
     * @param filteredElementList
     * @param enfCodeElementListPreviousSelected
     * @return 
     */
    public List<EnforceableCodeElement> getFilterSelectedList(List<EnforceableCodeElement> filteredElementList,
            Set<EnforceableCodeElement> enfCodeElementListPreviousSelected) {
        return filteredElementList.stream()
                .filter(enfCodeElementListPreviousSelected::contains)
                .collect(Collectors.toList());
    }
    
    /**
     * BITS method not previously commented
     * @param enfCodeElementListPreviousSelected
     * @param selectedElementList
     * @param filteredElementList
     * @return 
     */
    public Set<EnforceableCodeElement> updatePreviousSelectedList(Set<EnforceableCodeElement> enfCodeElementListPreviousSelected,
            List<EnforceableCodeElement> selectedElementList,
            List<EnforceableCodeElement> filteredElementList) {
        Set<EnforceableCodeElement> removedElements = new HashSet<>();

        filteredElementList.stream()
                .filter(occ -> !selectedElementList.contains(occ))
                .forEach(occ ->
                {
                    if (enfCodeElementListPreviousSelected.remove(occ))
                    {
                        removedElements.add(occ);
                    }
                });

        return removedElements;
    }

} // close class
