/*
 * Copyright (C) 2017 Turtle Creek Valley
 * Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.O
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.coordinators;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.EventException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.domain.SearchException;
import com.tcvcog.tcvce.domain.ViolationException;
import com.tcvcog.tcvce.entities.*;
import com.tcvcog.tcvce.entities.reports.ReportConfigCEEventList;
import com.tcvcog.tcvce.entities.UserAuthorized;
import com.tcvcog.tcvce.entities.occupancy.OccPeriod;
import com.tcvcog.tcvce.integration.EventIntegrator;
import java.io.Serializable;
import com.tcvcog.tcvce.util.Constants;
import com.tcvcog.tcvce.util.viewoptions.ViewOptionsActiveHiddenListsEnum;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import jakarta.faces.application.FacesMessage;
import com.tcvcog.tcvce.util.MessageBuilderParams;
import jakarta.faces.context.ExternalContext;
import jakarta.faces.context.FacesContext;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.fortuna.ical4j.data.CalendarOutputter;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.parameter.Cn;
import net.fortuna.ical4j.model.parameter.Role;
import net.fortuna.ical4j.model.property.Attendee;
import net.fortuna.ical4j.model.property.Description;
import net.fortuna.ical4j.model.property.DtEnd;
import net.fortuna.ical4j.model.property.DtStart;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Summary;
import net.fortuna.ical4j.model.property.Version;

/**
 *
 * Object playing the role of coordinator in the MVC framework by interfacing between
 * the JSF backing beans and the database integration classes.
 * 
 * This role involves several duties:
 * 
 * <ol>
 * <li>Generating new events for Code Enforcement and occupancy</li>
 * <li>Checking event creation permissions before allowing events of
 * restricted categories to be attached to other system objects.</li>
 * </ol>
 * 
 * 
 * @author Ellen Bascomb
 */
public class EventCoordinator extends BackingBeanUtils implements Serializable{

    static final int DEFAULT_EVENT_DURATION_MINS = 15;
    public static final String EVENT_PANEL_FORM_ID_FOR_REFRESH = "event-masterlist-form";
    
    
    /**
     * Creates a new instance of EventCoordinator
     */
    public EventCoordinator() {
        
    }
    
//    --------------------------------------------------------------------------
//    *************************** EVENT PERMISSIONS ****************************
//    --------------------------------------------------------------------------
    
    
    public boolean permissionsCheckpointEventAdd(EventCnF ev, UserAuthorized ua){
        if(ev == null || ua == null || ev.getCategory() == null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        // Check our role floor for enact
        if(ua.getKeyCard().getGoverningAuthPeriod().getRole().getRank() >= ev.getCategory().getRoleFloorEventEnact().getRank()){
            return true;
        }
        
        return false;
    }
    
    
    /**
     * Event edits limited to system admins, 
     * the manager of the parent object or the creator of the event itself
     * @param overseen
     * @param ev
     * @param ua
     * @return 
     */
    public boolean permissionsCheckpointEventEditDeac(IFace_PermissionsManagerOverseen overseen, EventCnF ev, UserAuthorized ua){
        if(overseen == null || ev == null || ua == null){
            return false;
        }
        if(ua.getCrossMuniViewAuthorizationTS() != null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        // check our role floor for update-- kick them out if they can't meet the floor, even if they are the creator
        // or soemehow the manager of the period
        if(ua.getKeyCard().getGoverningAuthPeriod().getRole().getRank() < ev.getCategory().getRoleFloorEventUpdate().getRank()){
            return false;
        }
        if(overseen.getCreatorUserID() == ua.getUserID()){
            return true;
        }
        if(ev.getCreatorUserID() == ua.getUserID()){
            return true;
        }
        // managers can edit and deac events on their cases or permit files 
        // that even they didn't create
        if(overseen.getManagerOverseer() != null){
            if(overseen.getManagerOverseer().getUserID() == ev.getCreatorUserID()){
                return true;
            }
        }
        return false;
    }
    
    /**
     * Implements filter logic to only return events that the satisfy the role floor restriction
     * @param evList empty list returned if null
     * @param viewerRank empty list returned if null
     * @return the filtered list
     */
    public List<EventCnF> filterEventListForViewFloor(List<EventCnF> evList, RoleType viewerRank){
        if(evList == null || viewerRank == null){
            return new ArrayList<>();
        }
        List<EventCnF> eventsForViewing = new ArrayList<>();
        if(!evList.isEmpty()){
            for(EventCnF ev: evList){
                if(viewerRank.getRank() >= ev.getCategory().getRoleFloorEventView().getRank()){
                    eventsForViewing.add(ev);
                } 
            }
        }
        return eventsForViewing;
    }
    
    /**
     * Convenience method for setting the event list of an event holder using this class's
     * event view filter which implements a view rank floor
     * @param holder
     * @param viewerRank
     * @return 
     */
    public IFace_EventHolder filterEventHoldersEventList(IFace_EventHolder holder, RoleType viewerRank){
        if(holder != null && viewerRank != null){
            holder.setEventList(filterEventListForViewFloor(holder.getEventList(), viewerRank));
        }
        return holder;
    }
    
//    --------------------------------------------------------------------------
//    *************************** EVENT MAIN ***********************************
//    --------------------------------------------------------------------------
    
    
    /**
     * Filtering tool for a list of events and a given view option
     * @param eventList for filtering
     * @param voahle if null, all events will be added 
     * @return A new list, containing zero or more events that were in the inputted list
     */
    public List<EventCnF> filterEventList(List<EventCnF> eventList, ViewOptionsActiveHiddenListsEnum voahle){
        
       List<EventCnF> visEventList = new ArrayList<>();
        if (eventList != null) {
            for (EventCnF ev : eventList) {
                if(voahle != null){

                    switch (voahle) {
                        case VIEW_ACTIVE_HIDDEN:
                            if (ev.getDeactivatedTS() == null
                                    && ev.isHidden()) {
                                visEventList.add(ev);
                            }
                            break;
                        case VIEW_ACTIVE_NOTHIDDEN:
                            if (ev.getDeactivatedTS() == null
                                    && !ev.isHidden()) {
                                visEventList.add(ev);
                            }
                            break;
                        case VIEW_ALL:
                            visEventList.add(ev);
                            break;
                        case VIEW_INACTIVE:
                            if (ev.getDeactivatedTS() != null) {
                                visEventList.add(ev);
                            }
                            break;
                        default:
                            visEventList.add(ev);
                    } // close switch
                } else {
                    visEventList.add(ev);
                }
            } // close for   
        } // close null check
        return visEventList;
    }
    
    
    /**
     * Filtering tool for a list of events and a given view option
     * @param eventList for filtering
     * @param voahle if null, all events will be added 
     * @return A new list, containing zero or more events that were in the inputted list
     */
    public List<EventCnF> filterEventPropUnitCasePeriodHeavyList(List<EventCnF> eventList, ViewOptionsActiveHiddenListsEnum voahle){
        
       List<EventCnF> visEventList = new ArrayList<>();
        if (eventList != null) {
            for (EventCnF ev : eventList) {
                if(voahle != null){
                    switch (voahle) {
                        case VIEW_ACTIVE_HIDDEN:
                            if (ev.getDeactivatedTS() == null
                                    && ev.isHidden()) {
                                visEventList.add(ev);
                            }
                            break;
                        case VIEW_ACTIVE_NOTHIDDEN:
                            if (ev.getDeactivatedTS() == null
                                    && !ev.isHidden()) {
                                visEventList.add(ev);
                            }
                            break;
                        case VIEW_ALL:
                            visEventList.add(ev);
                            break;
                        case VIEW_INACTIVE:
                            if (ev.getDeactivatedTS() != null) {
                                visEventList.add(ev);
                            }
                            break;
                        default:
                            visEventList.add(ev);
                    } // close switch
                } else {
                    visEventList.add(ev);
                }
            } // close for   
        } // close null check
        return visEventList;
    }
    
    /**
     * Primary access point for events by ID
     * @param eventID
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public EventCnF getEvent(int eventID) throws IntegrationException{
        EventIntegrator ei = getEventIntegrator();
        if(eventID == 0){
            return null;
        }
        EventCnF ev = ei.getEvent(eventID);
        try {
            configureEvent(ev);
        } catch (EventException | BObStatusException  ex) {
            System.out.println(ex);
        }
        return ev;
    }
    
    /**
     * Extracts events for bobs that hold them
     * @param evHolder
     * @return the sorted list in most recent to least recent order
     * @throws IntegrationException 
     */
    public List<EventCnF> getEventList(IFace_EventHolder evHolder) throws IntegrationException{
        
        EventIntegrator ei = getEventIntegrator();
        List<Integer> evidl = ei.getEventList(evHolder);
        List<EventCnF> evList = new ArrayList<>();
        for(Integer i: evidl){
            evList.add(getEvent(i));
        }
        Collections.sort(evList);
        Collections.reverse(evList);
        
        return evList;
    }
    
    /**
     * Factory for EventCategories
     * @return 
     */
    public EventCategory getEventCategorySkeleton(){
        EventCategory cat = new EventCategory();
        cat.setActive(true);
        cat.setHidable(true);
        cat.setRoleFloorEventEnact(RoleType.MuniReader);
        cat.setRoleFloorEventView(RoleType.MuniReader);
        cat.setRoleFloorEventUpdate(RoleType.MuniReader);
        
        
        return cat;
        
        
    }
    
    /**
     * Retrieves event Categories by Type
     * @param et
     * @return
     * @throws IntegrationException 
     */
    public List<EventCategory> getEventCategeryList(EventType et) throws IntegrationException{
        EventIntegrator ei = getEventIntegrator();
        List<EventCategory> evCatList = new ArrayList<>();
        if(et != null){
            evCatList.addAll(ei.getEventCategoryList(et));
        }
        return evCatList;
        
    }
    
    /**
     * Utility method for getting events in a list by only an ID list
     * @param evIDList
     * @return
     * @throws IntegrationException 
     */
    public List<EventCnF> getEventList(List<Integer> evIDList) throws IntegrationException{
        List<EventCnF> evList = new ArrayList<>();
        if(evIDList != null && !evIDList.isEmpty()){
            for(Integer i: evIDList){
                evList.add(getEvent(i));
            }
        }
        return evList;
    }
    
 
    
    
    /**
     * Returns the parcelkey of the containing parcel
     * @param ev
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public int getEventParcelKey(EventCnF ev) throws IntegrationException{
        EventIntegrator ei = getEventIntegrator();
        return ei.getEventParcelKey(ev);
    } 
    
    
    
    
    /**
     * I Create new EventCnF objects!
     * The only public method for creating a new event on an ERG.My guts will
     * call a bunch of methods in related coordinators to trigger appropriate
     * workflow actions. This method is overbuilt during a time when logging a single
     * event might spawn the logging of additional events at the time 
     * of the original write, hence the returned list of completed events
     * 
     * @param ev with only user-supplied data inserted; I'll fill in unified stuff
     * like eventcreator and such: does NOT need an ID yet
     * @param erg as of June 2020 V.0.9 implementers are CECase and OccPeriod objects
     * @param ua
     * @return A list of freshly inserted events, all freshly extracted from the DB
     * so what's in this list is what's in the DB. The head of the list is the
     * event that was passed into this method
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.EventException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException doesn't meet role floor
     */
    public List<EventCnF> addEvent(     EventCnF ev, 
                                        IFace_EventHolder erg,
                                        UserAuthorized ua) 
                            throws      BObStatusException, 
                                        EventException, 
                                        IntegrationException,
                                        AuthorizationException{


        if (ev == null || erg == null || ua == null) {
            throw new BObStatusException("Cannot process event with incomplete args");
        }
        
        if(!permissionsCheckpointEventAdd(ev, ua)){
            throw new AuthorizationException("User lacks authority to enact events of this category");
        }
        
        // *************************
        // Check Connections to the mother BOb
        // *************************
//        if (erg.getBObID() == 0){
//            throw new EventException("EventCoordinator.addEvent | FATAL ERROR: Received an object without its primary key mapped to getBobID() or object has no ID yet");
//        }
        // make sure we don't have a cross-domain conflict
        if (erg instanceof OccPeriod && ev.getCeCaseID() == 0 && ev.getParcelKey() == 0) {
            ev.setOccPeriodID(erg.getBObID());
        } else if (erg instanceof CECase && ev.getOccPeriodID() == 0 && ev.getParcelKey() == 0) {
            ev.setCeCaseID(erg.getBObID());
        } else if (erg instanceof PropertyDataHeavy && ev.getCeCaseID() == 0 && ev.getOccPeriodID() == 0){
            ev.setParcelKey(erg.getBObID());
        } else {
            throw new EventException("EventCoordinator.addEvent | FATAL ERROR: Improperly injected event with mother's PK");
        }
        
        configureEventTimes(ev, ua);

        // ****************
        // Event essentials
        // ****************
        ev.setCreatedBy(ua);
        ev.setLastUpdatedBy(ua);
        ev.setLastUpdatedTS(LocalDateTime.now());
        
        ev.setHidden(false);

        // **********************************
        // Allow domain coordinators to check
        // **********************************
        
        List<EventCnF> evsToAddQu = new ArrayList<>();
        
        // position our primary event at the head of the list
        evsToAddQu.add(ev);
        // then let the other domain folks add to this stack if needed

        List<EventCnF> evList = addEvent_processStack(evsToAddQu);
        // yikes -- this is a big cache dump on event add (ECD noted on 5-SEP-24)
        getEventCacheManager().flushUpstreamEventObjects();
        return  evList;
    }
    
    /**
     * Internal method for checking and tweaking if necessary the time stamps
     * on event objects
     * 
     * @param ev
     * @param ua 
     */
    private void configureEventTimes(EventCnF ev, UserAuthorized ua){
        if(ev == null || ua == null){
            return;
        }
        SystemCoordinator sc = getSystemCoordinator();
        
        // *************
        // TIME AUDITING
        // *************
        
        LocalDateTime now = LocalDateTime.now();
        
        // check and adjust start/end times
        if(ev.getTimeStart() == null){
            ev.setTimeStart(now);
        }
        
        // compute default end time in case we need it

        MessageBuilderParams mbp;
        LocalDateTime timeEndComputed = null;
        
        // deal with no end time
        if(ev.getTimeStart() != null && ev.getTimeEnd() == null && ev.getCategory() != null){
            timeEndComputed = ev.getTimeStart().plusMinutes(ev.getCategory().getDefaultDurationMins());
            ev.setTimeEnd(timeEndComputed);
     
            mbp = new MessageBuilderParams();
            mbp.setExistingContent(ev.getNotes());
            mbp.setHeader("Auto-edit of event details");
            mbp.setUser(ua);
            mbp.setCred(ua.getMyCredential());
            mbp.setExplanation("No end time was specified on incoming event so " +
                    "an end time was computed using the event category's default duration");
            ev.setNotes(sc.appendNoteBlock(mbp));
        }
        
        // Deal with non-chronological start and end times
        if(ev.getTimeStart() != null 
                && ev.getTimeEnd() != null 
                && ev.getTimeEnd().isBefore(ev.getTimeStart())){
            ev.setTimeEnd(timeEndComputed);
            mbp = new MessageBuilderParams();
            mbp.setExistingContent(ev.getNotes());
            mbp.setHeader("Auto-edit of event details");
            mbp.setUser(ua);
            mbp.setCred(ua.getMyCredential());
            mbp.setExplanation("The end time of the incoming event cannot occur "
                    + "before the start time; They must be in forward chrono order;"
                    + "A new end time was automatically computed using the event category's"
                    + "default duration");
            ev.setNotes(sc.appendNoteBlock(mbp));
        }
        
    }
    
    
    /**
     * I Iterate over a List of events that works like a Queue in that I'll
     * insert one after the other but stop if any of them don't make it into DB.
     * 
     * @param qu
     * @return
     * @throws IntegrationException 
     */
    private List<EventCnF> addEvent_processStack(List<EventCnF> qu) throws IntegrationException, BObStatusException, EventException{
        EventIntegrator ei = getEventIntegrator();
        List<EventCnF> doneList = new ArrayList<>();
        if(qu != null && !qu.isEmpty()){
            for(EventCnF ev: qu){
                auditEvent(ev);
                int id = ei.insertEvent(ev);
                if(id != 0){
                    doneList.add(getEvent(id));
                } else {
                    break;
                }
            }
        }
        return doneList;
    }
    
    
   
    
    /**
     * Called by this class ONLY to set member variables based on business
     * rules before sending the event onto its requesting method. Checks for request processing, 
     * sets intended responder for action requests, etc.
     * @param ev
     * @param user
     * @return a nicely configured EventCEEcase
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    private EventCnF configureEvent(EventCnF ev) throws IntegrationException, EventException, BObStatusException{
        PersonCoordinator pc = getPersonCoordinator();
        
        if(ev == null){
            return null;
        }
        
        // check for end times, and if one is missing, update event on the Java side only
        // this applies to events created early in the system where end times weren't being written
        if(ev.getTimeStart() != null && ev.getTimeEnd() == null){
            System.out.println("EventCoordinator.configureEvent | Found event without end time; updating with end time | eventid: " + ev.getEventID());
            ev.setTimeEnd(ev.getTimeStart().plusMinutes(DEFAULT_EVENT_DURATION_MINS));
            updateEventEndTimeOnly(ev);
        }
        
        //set duration
        long lduration = 0;
        if(ev.getTimeStart() != null && ev.getTimeEnd() != null){
            if(ev.getTimeEnd().isAfter(ev.getTimeStart())){
                long sec = ev.getTimeEnd().toEpochSecond(ZoneOffset.UTC) - ev.getTimeStart().toEpochSecond(ZoneOffset.UTC);
                lduration = (long) ((double) sec / 60.0);
            }
        }
        ev.setDuration(lduration);
        
        // Declare this event as either in the CE or Occ domain or parcel with 
        // our hacky little enum thingy 
        // ELLEN BASCOMB of APT 31Y SEP22: Not so hacky, actually
          if(ev.getCeCaseID() != 0){
                ev.setDomain(EventRealm.CODE_ENFORCEMENT);
            } else if(ev.getOccPeriodID() != 0){
                ev.setDomain(EventRealm.OCCUPANCY);
            }  else if(ev.getParcelKey() != 0){
                ev.setDomain(EventRealm.PARCEL);
            } else {
                throw new EventException("EventCnF must have either an occupancy period ID, or CECase ID");
            }
          
     
        
        // build case/period ID string from our temp fields
        switch(ev.getDomain()){
            case CODE_ENFORCEMENT -> {
                 StringBuilder sb = new StringBuilder("Code Enf case: ");
                sb.append(ev.getCaseName());
                ev.setCasePeriodPropertyString(sb.toString());
            }
            case OCCUPANCY -> {
                StringBuilder sb = new StringBuilder("Permit file on Unit: ");
                sb.append(ev.getPropertyUnitNumber());
                ev.setCasePeriodPropertyString(sb.toString());
            }
            case PARCEL -> {
                StringBuilder sb = new StringBuilder("Property: ");
                sb.append(ev.getParcelAddressOneLine());
                ev.setCasePeriodPropertyString(sb.toString());
            }
        }
//        
//        auditEvent(ev);
//       
        ev.sethumanLinkList(pc.getHumanLinkList(ev));
        configureEventLinkedLinkedEventList(ev);
        return ev;
    }
    
    
    
    /**
     * Implements business logic to ensure EventCnF correctness; called by insert
     * and edit event methods; Throws an EventException if there's an error
     * 
     * @param ev
     * @throws com.tcvcog.tcvce.domain.EventException
     */
    private void auditEvent(EventCnF ev) throws EventException{
        if(ev.getTimeStart() == null){
            throw new EventException("Events must have a start time");
        }
        if(ev.getTimeEnd() != null){
            if(ev.getTimeEnd().isBefore(ev.getTimeStart())){
                throw new EventException("Events with end times must not have an end time before start time");
            }
        }
         if(ev.getCeCaseID() == 0 && ev.getOccPeriodID() == 0 && ev.getParcelKey() == 0){
                throw new EventException("EventCnF cannot have a zero for CECase and OccPeriod  and parcel ID");
            }
          
        
    }
    
    
    /**
     * Business rule aware pathway to update fields on EventCnF objects
     * When updating Person links, this method clears all previous connections
     * and rebuilds the mapping from scratch on each update.
     * 
     * @param ev
     * @param targetCat
     * @param ua
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.EventException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void updateEventCategoryMaintainType(EventCnF ev, EventCategory targetCat, UserAuthorized ua) throws IntegrationException, EventException, BObStatusException{
        EventIntegrator ei = getEventIntegrator();
         if(ev == null || ua == null){
            throw new BObStatusException("Event and User cannot be null");
        }
        if(ev.getCategory() == null || ev.getCategory().getRoleFloorEventUpdate() == null || (ev.getCategory().getRoleFloorEventUpdate().getRank() > ua.getRole().getRank())){
            throw new BObStatusException("User's rank does not allow deactivation of given event or ranks are not configured properly");
        }
        
        if(targetCat != null && targetCat.getEventType() == ev.getCategory().getEventType()){
            auditEvent(ev);
            ev.setLastUpdatedBy(ua);
            ev.setLastUpdatedTS(LocalDateTime.now());
            ev.setCategory(targetCat);
            ei.updateEventCategoryMaintainType(ev);
            getEventCacheManager().flush(ev);
        } else {
            throw new EventException("An event's type cannot change; the new category must be of the same type as the event's original type");
        }
    }
    
    /**
     * Business rule aware pathway to update fields on EventCnF objects
     * When updating Person links, this method clears all previous connections
     * and rebuilds the mapping from scratch on each update.
     * 
     * @param ev
     * @param holder
     * @param ua
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.EventException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void updateEvent(EventCnF ev, IFace_EventHolder holder, UserAuthorized ua) throws IntegrationException, EventException, BObStatusException, AuthorizationException{
     
        EventIntegrator ei = getEventIntegrator();
         if(ev == null || ua == null){
            throw new BObStatusException("Event and User cannot be null");
        }
        if(ev.getCategory() == null || ev.getCategory().getRoleFloorEventUpdate() == null || (ev.getCategory().getRoleFloorEventUpdate().getRank() > ua.getRole().getRank())){
            throw new BObStatusException("User's rank does not allow deactivation of given event or ranks are not configured properly");
        }
        if(!permissionsCheckpointEventEditDeac(holder, ev, ua)){
            throw new AuthorizationException("Cannot edit an event without being the event's creator or a manager of the event's parent object or a system admin"); 
        }
        configureEventTimes(ev, ua);
        auditEvent(ev);
        ev.setLastUpdatedBy(ua);
        ev.setLastUpdatedTS(LocalDateTime.now());
        updateEvent(ev);
        getEventCacheManager().flush(ev);
    }
    
    /**
     * Single point of call to EventIntegrator's updateEvent 
     * 
     * @param ev 
     */
    private void updateEvent(EventCnF ev) throws IntegrationException, BObStatusException{
        EventIntegrator ei = getEventIntegrator();
        if(ev == null || ev.getEventID() == 0){
            throw new BObStatusException("cannot update null event or one with ID = 0");
        }
        ei.updateEvent(ev);
       
        
    }
    
    
    /**
     * Special updater pathway for only end times
     * @param ev 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void updateEventEndTimeOnly(EventCnF ev) throws IntegrationException{
        EventIntegrator ei = getEventIntegrator();
        ei.updateEventEndTime(ev);
        getEventCacheManager().flush(ev);
    }
    
    /**
     * Toggles active back on
     * @param ev
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.EventException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void reactivateEvent(EventCnF ev, UserAuthorized ua) throws EventException, BObStatusException, IntegrationException{
         if(ev == null || ua == null){
            throw new BObStatusException("Event and User cannot be null");
        }
        if(ev.getCategory() == null || ev.getCategory().getRoleFloorEventUpdate() == null || (ev.getCategory().getRoleFloorEventUpdate().getRank() > ua.getRole().getRank())){
            throw new BObStatusException("User's rank does not allow deactivation of given event or ranks are not configured properly");
        }
        EventIntegrator ei = getEventIntegrator();
        ev.setDeactivatedBy(null);
        ev.setDeactivatedTS(null);
        auditEvent(ev);
        ev.setLastUpdatedBy(ua);
        ev.setLastUpdatedTS(LocalDateTime.now());
        updateEvent(ev);
    }
    
    /**
     * Writes the given mbp to the notes field of a given event
     * @param mbp
     * @param ev
     * @param ua 
     */
    public void updateEventNotes(MessageBuilderParams mbp, EventCnF ev, UserAuthorized ua) throws IntegrationException, BObStatusException{
        SystemCoordinator sc = getSystemCoordinator();
        EventIntegrator ei = getEventIntegrator();
        if(mbp == null || ev == null || ua == null){
            throw new BObStatusException("Cannot update notes with null message, ev, or user");
        }
        ev.setNotes(sc.appendNoteBlock(mbp));
        ei.updateEventNotes(ev);        
        getEventCacheManager().flush(ev);
    }
    
    
    /**
     * Akin to delete
     * @param ev
     * @param holder
     * @param ua
     * @throws IntegrationException 
     */
    public void deactivateEvent(EventCnF ev, IFace_EventHolder holder, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException{
        if(ev == null || ua == null ){
            throw new BObStatusException("Event and User cannot be null");
        }
        
        if(ev.getCategory() == null || ev.getCategory().getRoleFloorEventUpdate() == null || (ev.getCategory().getRoleFloorEventUpdate().getRank() > ua.getRole().getRank())){
            throw new BObStatusException("User's rank does not allow deactivation of given event or ranks are not configured properly or event doesn't have a category");
        }
        EventIntegrator ei = getEventIntegrator();
        if(!permissionsCheckpointEventEditDeac(holder, ev, ua)){
            throw new AuthorizationException("Cannot edit an event without being the event's creator or a system admin"); 
        }
        
        
        ev.setLastUpdatedBy(ua);
        ev.setLastUpdatedTS(LocalDateTime.now());
        ev.setDeactivatedBy(ua);
        ev.setDeactivatedTS(LocalDateTime.now());
        // we really don't need a deac note since we save deac by
//        ev.setNotes(sc.formatAndAppendNote(ua, "Event deactivated by User with credential sig " + ua.getMyCredential().getSignature(), ev.getNotes()));
        updateEvent(ev);
        getEventCacheManager().flush(ev);
        
    }
    
   
    /**
     * Core coordinator method called by all other classes who want to 
     * create their own event. Restricts the event type based on current
     * case phase (closed cases cannot have action, origination, or compliance events.
     * Includes the instantiation of EventCnF objects
     * 
     * @param erg which for Beta v0.9 includes CECase and OccPeriod object AND now properties (as of mid 2023); null means
     * caller will need to insert the BOb ID later
     * @param ec the type of event to attach to the case
     * @return an initialized event with basic properties set
     * @throws BObStatusException thrown if the case is in an improper state for proposed event
     * @throws com.tcvcog.tcvce.domain.EventException
     */
    public EventCnF initEvent(IFace_EventHolder erg, EventCategory ec) throws BObStatusException, EventException {
        
        CECase cse = null;
        OccPeriod op = null;
        PropertyDataHeavy pdh = null;
        
        // the moment of event instantiation!!!!
        EventCnF e = new EventCnF();
        
        if(erg != null){
            if(erg instanceof CECase){
                cse = (CECase) erg;
                // TODO: Update for priority
                if(cse.getStatusBundle() != null){
                    
                    if(cse.getStatusBundle().getPhase() == CasePhaseEnum.Closed && 
                       (
                           ec.getEventType() == EventType.Action
                           || 
                           ec.getEventType() == EventType.Origination
                           ||
                           ec.getEventType() == EventType.Violation
                       )
                   ){
                       throw new BObStatusException("This event cannot be attached to a closed case");
                   }
                }
                e.setCeCaseID(cse.getCaseID());
                e.setDomain(EventRealm.CODE_ENFORCEMENT);
            } else if (erg instanceof OccPeriod){
                op = (OccPeriod) erg;
                e.setOccPeriodID(op.getPeriodID());
                e.setDomain(EventRealm.OCCUPANCY);
                System.out.println("EventCoordinator.initEvent | Event is getting occ period set to " + e.getOccPeriodID());
            } else if (erg instanceof PropertyDataHeavy){
                pdh = (PropertyDataHeavy) erg;
                e.setParcelKey(pdh.getParcelKey());
                e.setDomain(EventRealm.PARCEL);
            }
        }
        if(ec != null){
            e.setCategory(ec);
            e.setTimeStart(LocalDateTime.now());
            e.setTimeEnd(e.getTimeStart().plusMinutes(ec.getDefaultDurationMins()));
            e.appendToDescription(ec.getHostEventDescriptionSuggestedText());
        }
        e.setActive(true);
        e.setHidden(false);
        return e;
    }
    
    /**
     * Used by workflow BB
     * @param evDoneList
     * @return 
     */
    public String buildEventInfoMessage(List<EventCnF> evDoneList){
        StringBuilder sb = new StringBuilder();
            for(EventCnF evZ: evDoneList){
                sb = new StringBuilder();

                sb.append(evZ.getCategory().getEventType().getLabel());
                sb.append(": ");
                sb.append(evZ.getCategory().getEventCategoryTitle());
                sb.append(" (");
                sb.append(evZ.getCategory().getEventCategoryDesc());
                sb.append(") ");
            }
        return sb.toString();
    }
    
    
    /**
     * Figures out who the parent of the given event is and returns it,
     * having fetched it from the appropriate coordinator.
     * 
     * @param ev whose parent caller needs
     * @param ua
     * @return  the parent
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.SearchException
     * @throws com.tcvcog.tcvce.domain.BlobException
     */
    public IFace_EventHolder extractParentEventHolderFromEvent(EventCnF ev, UserAuthorized ua) throws IntegrationException, BObStatusException, SearchException, BlobException{
        if(ev == null || ua == null){
            return (IFace_EventHolder) new BObStatusException("Cannot determine holder of null event or with null user");
        }
        IFace_EventHolder holder = null;
        
        switch(ev.getDomain()){
            case CODE_ENFORCEMENT -> {
                CaseCoordinator cc = getCaseCoordinator();
                return cc.cecase_getCECase(ev.getCeCaseID(), ua);
            }
            case OCCUPANCY -> {
                OccupancyCoordinator oc = getOccupancyCoordinator();
                return oc.getOccPeriod(ev.getOccPeriodID(), ua);
            }
            case PARCEL -> {
                PropertyCoordinator pc = getPropertyCoordinator();
                PropertyDataHeavy pdh;
                try {
                    pdh = pc.assemblePropertyDataHeavy(pc.getProperty(ev.getParcelKey()), ua);
                } catch (AuthorizationException ex) {
                    throw new BObStatusException(ex.getMessage());
                }
                return pdh;
            }
            default -> System.out.println("Cannot determine holder of event with general domain enum"); 
        }
        return holder;
    }
    
    /**
     * Utility for casting an inspectable to an event holder
     * @param inspectable
     * @return an implementing class of IFace_EventHolder
     * @throws BObStatusException if it can't be done
     */
    public IFace_EventHolder castAttemptInspectableToEventHolder(IFace_inspectable inspectable) throws BObStatusException{
        IFace_EventHolder holder =  null;
        if(inspectable instanceof IFace_EventHolder h){
            holder = h;
        } else {
            throw new BObStatusException("cannot deac related events due to a class cast error. This must be fixed by a developer");
        }
        return holder;
    }
    
  
    
    
//    --------------------------------------------------------------------------
//    *************************** FOLLOW UP DEPARTMENT *************************
//    --------------------------------------------------------------------------
    
    
    /**
     * Adapter method for adding an event that is itself a follow up to 
     * another event.Inside, I call the much complex addEvent method in this
     * class, and afterwards, do the linking that should satisfy the follow up 
     * requirement.
     * 
     * @param ev
     * @param erg
     * @param ua
     * @param followUpTarget
     * @return
     * @throws BObStatusException
     * @throws EventException
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
     public List<EventCnF> addEventAsFollowUpEvent(     EventCnF ev, 
                                                        IFace_EventHolder erg,
                                                        UserAuthorized ua,
                                                        EventCnF followUpTarget) 
                            throws      BObStatusException, 
                                        EventException, 
                                        IntegrationException,
                                        AuthorizationException{
         List<EventCnF> freshEventList = addEvent(ev, erg, ua);
         
         // only link the first added event to the follow up target
         if(followUpTarget != null && freshEventList != null && !freshEventList.isEmpty()){
             EventCnF fuev = freshEventList.get(0);
             // The direction of this link is somewhat non-intuitive:
             // Our event requiring the followup is our followUpTarget
             // so the first argument is the event that is the act of following up
             linkEventToEventLinked(fuev, followUpTarget);
             // make sure our fresh event list has the follow up requiring event
             // containing a record that it was, in fact, followed up on
             freshEventList.add(0, getEvent(fuev.getEventID()));
         }
         getEventCacheManager().flushUpstreamEventObjects();
         return freshEventList;
         
     }
     
     
     /**
      * Looks through all the events on the given event holder and 
      * satisfies the follow up requirement for any followup events 
      * lacking such satisfaction.
      * 
      * @param evHolder 
      */
     public void satisfyAllFollowUpEvents(IFace_EventHolder evHolder){
         
     }
     
    
    
//    --------------------------------------------------------------------------
//    ***************************** EVENT LINKAGES *****************************
//    --------------------------------------------------------------------------
   
    /**
     * Sets the linked event list on the given implementing class
     * @param evLinked
     * @return
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public IFace_EventLinked configureEventLinkedLinkedEventList(IFace_EventLinked evLinked) 
            throws BObStatusException, IntegrationException{
        if(evLinked == null){
            throw new BObStatusException("Cannot configure null event linked implementer");
        }
        EventIntegrator ei = getEventIntegrator();
    
        // TODO: prevent cyles somewhere in here so if an event gets linked to itself
        // the system doesn't loop in event creation forever.
        evLinked.setLinkedEvents(getEventList(ei.getLinkedEventIDList(evLinked)));
        
        return evLinked;
    }
    
    /**
     * Writes new links in the eventlinkage table
     * @param ev this is the event whose ID will go in the event_eventid field
     * of the eventlinkage table; In follow up land, this first argument is the 
     * event which satisfies the follow up requirement; this event is the act of following up
     * @param evLinked Whose ID will be written in the appropriate column of eventlinkage.
     * In follow up event land, this is the event requiring the follow up
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public void linkEventToEventLinked(EventCnF ev, IFace_EventLinked evLinked) 
            throws BObStatusException, IntegrationException{
        if(ev == null || evLinked == null){
            throw new BObStatusException("Cannot link events to null event or event linked implementing class");            
        }
        if(ev.getEventID() == 0 || evLinked.getLinkTargetPrimaryKey() == 0){
            throw new BObStatusException("Cannot link event with an ID = 0 or an eventLinked object whose PK is zero");
        }
        
        EventIntegrator ei = getEventIntegrator();
        ei.linkObjectToEvent(ev, evLinked);
        
    }
    
    /**
     * Deactivates all connections between the given event and the event linked 
     * object.Used when deactivating an event so as to not have active links 
     * to deactivated events.
     * This could probably be a database trigger.
     * 
     * @param ev
     * @param evLinked
     * @param ua doing the deactivating
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public void deactivateEventLinks(EventCnF ev, IFace_EventLinked evLinked, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(ev == null || evLinked == null){
            throw new BObStatusException("Cannot deac link events to null event or event linked implementing class");            
        }
        EventIntegrator ei = getEventIntegrator();
        ei.deactivateEventLinks(ev, evLinked);
        getEventCacheManager().flush(ev);
    }
    
    /**
     * Deactivates all event links to the given implementing class of EventLinked.I call deactivateEventLinks(...) in this class under the hood.
     * Used when an  
 object which has events links is transitioned into a state in which 
 its linked events, such as its follow-up events, are no longer relevant,
 Such as when resetting the mailing status on an NOV, or nullifying a violation.
     * 
     * @param evLinked whose events links should be deactivated._
     * @param holder 
     * @param deacUnderlyingEvents when true I'll also deactivate those events 
     * to which this implementing class was linked. 
     * @param preservePastEvent when true, events whose start time is BEFORE now() will NOT have their links deactivated or the underlying event deactivated; 
     * This is designed for situations like closing a CE Case where we want to mop up future linked events to clear the future TOODs but preserve past
     * records of meaningful events in the past
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void deactivateAllEventsLinkedToEventLinkedObject(IFace_EventLinked evLinked, IFace_EventHolder holder, boolean deacUnderlyingEvents, boolean preservePastEvent, UserAuthorized ua) 
            throws BObStatusException, IntegrationException, AuthorizationException{
        if(evLinked == null){
            throw new BObStatusException("Cannot deactivate linked events to a null event linked implementing class");
        }
        if(evLinked.getLinkedEvents() != null && !evLinked.getLinkedEvents().isEmpty()){
            for(EventCnF ev: evLinked.getLinkedEvents()){
                // skip past events and ev links for deac when caller requests preservation of past events
                // meaning only remove future reminder events
                if(ev.getTimeStart() != null && ev.getTimeStart().isBefore(LocalDateTime.now()) && preservePastEvent){
                    continue;
                }
                deactivateEventLinks(ev, evLinked, ua);
                if(deacUnderlyingEvents){
                    deactivateEvent(ev, holder, ua);
                }
            }
            getEventCacheManager().flushAllCaches();
        }
    }
    
    
//    --------------------------------------------------------------------------
//    ***************************** SEARCH AND VIEW ****************************
//    --------------------------------------------------------------------------
   
    /**
     * Logic container for choosing a sensible event view enum value
     * @param ua not used
     * @return 
     */
   
    public ViewOptionsActiveHiddenListsEnum determineDefaultEventView(UserAuthorized ua){
        return ViewOptionsActiveHiddenListsEnum.VIEW_ACTIVE_NOTHIDDEN;
    }
    
    
      
    /**
     * Generator method for the EventCnF object
     * @return 
     */
    public ReportConfigCEEventList initDefaultReportConfigEventList(){
        SearchCoordinator sc = getSearchCoordinator();
        ReportConfigCEEventList config = new ReportConfigCEEventList();
        config.setIncludeAttachedPersons(true);
        config.setIncludeCaseActionRequestInfo(false);
        config.setGenerationTimestamp(LocalDateTime.now());
        config.setIncludeEventTypeSummaryChart(true);
        config.setIncludeActiveCaseListing(false);
        config.setIncludeCompleteQueryParamsDump(false);
        config.setSortInRevChrono(true);
        return config;
    }
    
    
    /**
     * Iterates over the list of events and returns the first event of the given category
     * @param evList
     * @param cat
     * @return
     * @throws BObStatusException 
     */
    public EventCnF determinePresenceOfEventCategoryInList(List<EventCnF> evList, EventCategory cat) throws BObStatusException{
         // Check for null inputs
        if (evList == null || cat == null) {
            throw new BObStatusException("Input list or category cannot be null");
        }

        for (EventCnF event : evList) {
            if (event != null && event.getCategory() == cat) {
                return event;
            }
        }
        return null;
    }
    
    
//    --------------------------------------------------------------------------
//    ************************* EVENT CATEGORIES *******************************
//    --------------------------------------------------------------------------
    
    
    /**
     * Logic intermediary retrieval method for EventCategories
     * @param catID
     * @return
     * @throws IntegrationException 
     */
    public EventCategory getEventCategory(int catID) throws IntegrationException{
        
        EventIntegrator ei = getEventIntegrator();
        return ei.getEventCategory(catID);
    }
    
    /**
     * Assembles a subset of EventType and EventCategory objects for 
     * viewing by a given user; based on the rank of the User passed in
     * @param ua
     * @return 
     */
    public Map<EventType, List<EventCategory>> assembleEventTypeCatMap_toView(UserAuthorized ua){
       Map<EventType, List<EventCategory>> typeCatMap = new HashMap<>();
       List<EventType> typeList = new ArrayList();
       typeList.addAll(Arrays.asList(EventType.values()));
       if(!typeList.isEmpty()){
           for(EventType typ: typeList){
               try {
                   typeCatMap.put(typ, determinePermittedEventCategories_toView(typ, ua));
               } catch (IntegrationException ex) {
                   System.out.println(ex);
               }
           }
       }
       
       return typeCatMap;
    }
    
    
    /**
     * Internal method for selecting only EventCategories that the given UserAuthorized
     * is allowed to view
     * @param et
     * @param ua
     * @return
     * @throws IntegrationException 
     */
    private List<EventCategory> determinePermittedEventCategories_toView(EventType et, UserAuthorized ua) throws IntegrationException{
        EventIntegrator ei = getEventIntegrator();
        List<EventCategory> catList = ei.getEventCategoryList(et);
        List<EventCategory> allowedCats = new ArrayList<>();
        if(catList != null &&!catList.isEmpty()){
            for(EventCategory ec: catList){
                if(ec.getRoleFloorEventView() != null){
                    if(ec.getRoleFloorEventView().getRank() >= ua.getMyCredential().getGoverningAuthPeriod().getRole().getRank()){
                        allowedCats.add(ec);
                    }
                }
            }
        }
        return allowedCats;
    }
    
    
    
    
    /**
     * Creates a Mapping of EventTypes and permitted EventCategories for enacting 
     * and event
     * @param domain
     * @param erg
     * @param ua
     * @return 
     */
    public Map<EventType, List<EventCategory>> assembleEventTypeCatMap_toEnact(
                                                EventRealm domain,
                                                IFace_EventHolder erg,
                                                UserAuthorized ua) throws BObStatusException{
       Map<EventType, List<EventCategory>> typeCatMap = new HashMap<>();
       List<EventType> typeList = determinePermittedEventTypes(domain, erg, ua);
       if(typeList != null && !typeList.isEmpty()){
           for(EventType typ: typeList){
               typeCatMap.put(typ, determinePermittedEventCategories(typ, ua));
           }
       }
       return typeCatMap;
    }
    
    /**
     * Entry point for logic components that select which EventType objects
     * and later, event cats, the user can see on a given load of the events 
     * viewer. This logic will review what our event domain is, the thing
     * onto which we might be attaching events (which as of Jun 2020 are CECase
     * objects or OccPeriod objects), and the attacher
     * @param domain
     * @param erg which will be interrogated for is open/closed status
     * @param ua doing potential creation of an event
     * @return 
     */
    public List<EventType> determinePermittedEventTypes(    EventRealm domain,
                                                            IFace_EventHolder erg,
                                                            UserAuthorized ua) throws BObStatusException{
        List<EventType> typeList = new ArrayList<>();
        if(domain == null || erg == null || ua == null){
            return typeList;
        }
        
        // implement logic based on event domain and check for sensible matches
        switch(domain){
            case CODE_ENFORCEMENT:
                if(erg instanceof CECase){
                    typeList.addAll(determinePermittedEventTypesForCECase((CECase) erg, ua));
                }
                break;
            case OCCUPANCY:
                if(erg instanceof OccPeriod){
                    typeList.addAll(determinePermittedEventTypesForOcc((OccPeriod) erg, ua));
                }
                break;
            case PARCEL:
                if(erg instanceof PropertyDataHeavy){
                    typeList.addAll(determinePermittedEventTypesForParcel((PropertyDataHeavy) erg, ua));
                }
                break;
            case UNIVERSAL:
                typeList.add(EventType.Custom);
                typeList.add(EventType.Meeting);
                typeList.add(EventType.Communication);
                typeList.add(EventType.PropertyAlert);

                
                break;
            default:
        }
        return typeList;
    }
    
    
     /**
     * Implements business rules for determining which event types are allowed
     * to be attached to the given CECaseDataHeavy based on the case's phase and the
     * user's permissions in the system.
     *
     * @param c the CECaseDataHeavy on which the event would be attached
     * @param u the User doing the attaching
     * @return allowed EventTypes for attaching to the given case
     */
    private List<EventType> determinePermittedEventTypesForCECase(CECase c, UserAuthorized u) throws BObStatusException {
         if(u == null){
            throw new BObStatusException("Cannot determine permitted event types with null user authorized");
        }
        List<EventType> typeList = new ArrayList<>();
        int rank = u.getRole().getRank();
        if (rank >= RoleType.MuniStaff.getRank()) {
            typeList.add(EventType.Action);
            typeList.add(EventType.Timeline);
            typeList.add(EventType.Origination);
            typeList.add(EventType.Occupancy);
            typeList.add(EventType.PropertyAlert);
            typeList.add(EventType.Citation);
            typeList.add(EventType.CaseAdmin);
            typeList.add(EventType.Inspection);
            
            
        }
        if (rank >= RoleType.MuniReader.getRank()) {
            typeList.add(EventType.Communication);
            typeList.add(EventType.Meeting);
            typeList.add(EventType.Custom);
        }
        return typeList;
    }
    
    
    /**
     * Business rule logic container for choosing permitted eventTypes
     * @param period not used yet as of DEC-2022
     * @param u
     * @return 
     */
    private List<EventType> determinePermittedEventTypesForOcc(OccPeriod period, UserAuthorized u) throws BObStatusException {
        if(u == null){
            throw new BObStatusException("Cannot determine permitted event types with null user authorized");
        }
        List<EventType> typeList = new ArrayList<>();
        int rank = u.getRole().getRank();
        if (rank >= RoleType.MuniStaff.getRank()) {
            typeList.add(EventType.Action);
            typeList.add(EventType.Timeline);
            typeList.add(EventType.Occupancy);
            typeList.add(EventType.PropertyAlert);
            typeList.add(EventType.Inspection);
        }
        if (rank >= RoleType.MuniReader.getRank()) {
            typeList.add(EventType.Communication);
            typeList.add(EventType.Meeting);
            typeList.add(EventType.Custom);
        }
        return typeList;
    }
    
    /**
     * Business rule logic container for choosing permitted eventTypes
     * @param period
     * @param u
     * @return 
     */
    private List<EventType> determinePermittedEventTypesForParcel(PropertyDataHeavy pdh, UserAuthorized u) throws BObStatusException {
        if(u == null){
            throw new BObStatusException("Cannot determine permitted event types with null user authorized");
        }
        List<EventType> typeList = new ArrayList<>();
        int rank = u.getRole().getRank();
        if (rank >= RoleType.MuniStaff.getRank()) {
            typeList.add(EventType.Action);
            typeList.add(EventType.Timeline);
            typeList.add(EventType.Occupancy);
            typeList.add(EventType.PropertyAlert);

        }
        if (rank >= RoleType.MuniReader.getRank()) {
            typeList.add(EventType.Communication);
            typeList.add(EventType.Meeting);
            typeList.add(EventType.Custom);
        }
        return typeList;
    }
    
    /**
     * Logic intermediary for extracting all EventTypes in the system
     * @return 
     */
    public List<EventType> getEventTypesAll(){
        List<EventType> typeList = new ArrayList<>();
        typeList.addAll(Arrays.asList(EventType.values()));
        return typeList;
    }
    
    
    /**
     * Factory method for creating bare event categories.
     * This is used when creating search parameter objects where we want event
     * types without a specific category, but we need a EventCategory shell
     * in which to insert the EventType
     * 
     * @return an EventCategory container with basic properties set
     */
    public EventCategory initEventCategory(){
        EventCategory ec =  new EventCategory();
        ec.setHidable(true);
        // TODO: finishing autoconfiguring these 
        return ec;
    }
    
    /**
     * Factory method for creating event categories when only the categoryID
     * is available. This is handy since the insertEvent method on the integrator
     * only needs the categoryID for storing in the database
     * @param catID the categoryID of the EventCategory you want
     * @return an instantiated EventCategory object
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public EventCategory initEventCategory(int catID) throws IntegrationException{
        EventIntegrator ei = getEventIntegrator();
        EventCategory ec =  ei.getEventCategory(catID);
        return ec;
    }
  
    
    /**
     * Extracts a complete list of all active event categories
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public List<EventCategory> getEventCategoryList() throws IntegrationException{
        EventIntegrator ei = getEventIntegrator();
        return ei.getEventCategoryList();
        
        
    }
    
    /**
     * Utility method for iterating over the event cat list and only
     * passing on active ones
     * @param catList
     * @return
     */
    public List<EventCategory> assembleEventCategoryListActiveOnly(List<EventCategory> catList) {
        EventIntegrator ei = getEventIntegrator();
        List<EventCategory> catListActiveOnly = new ArrayList<>();
        for(EventCategory cat: catList){
            if(cat.isActive()){
                catListActiveOnly.add(cat);
            }
        }
        return catListActiveOnly;
    }
    
    
    /**
     * Logic container for choosing which event categories to allow the user
     * to use for event creation UI. Used to build a mapping of EventTypes and categories
     * 
     * @param et
     * @param u
     * @return 
     */
    public List<EventCategory> determinePermittedEventCategories(EventType et, UserAuthorized u) {
        EventIntegrator ei = getEventIntegrator();
        List<EventCategory> rawCats = new ArrayList<>();
        List<EventCategory> allowedCats = new ArrayList<>();
        if(et == null){
            return allowedCats;
        }
        try {
            rawCats.addAll(ei.getEventCategoryList(et));
        } catch (IntegrationException ex) {
            System.out.println(ex);
        }
        
        for(EventCategory cat: rawCats){
            boolean include = false;
            if(cat.isActive()){
                if(u != null && u.getKeyCard() != null){
                    // ensure that the user's rank is at least what is required 
                    // to enact an event of this Category
                    if(cat.getRoleFloorEventEnact() != null){
                        // enforce our enactment floor
                        if(u.getKeyCard().getGoverningAuthPeriod().getRole().getRank() >= cat.getRoleFloorEventEnact().getRank()){
                                include = true;
                        } 
                    }
                }       
            }
            if(include){
                allowedCats.add(cat);
            }
        }
        java.util.Collections.sort(allowedCats);
        return allowedCats;
    }
    
    /**
     * Utility method for examining a list of aribtrary events and selecting 
     * the most recent active event by specified type
     * @param evList pool to search
     * @param targetType to assign from the eventList
     * @return ref to the event that meets this criteria, null if not found
     * 
     */
    public EventCnF findMostRecentEventByType(List<EventCnF> evList, EventType targetType){
        EventCnF chosenEvent = null;
        
        List<EventCnF> targetEventCandidates = new ArrayList<>();
        for(EventCnF ev: evList){
            if(ev.getCategory().getEventType() == targetType && ev.isActive()){
                targetEventCandidates.add(ev);
            }
        }
        if(!targetEventCandidates.isEmpty()){
            Collections.sort(targetEventCandidates);
            Collections.reverse(targetEventCandidates);
            chosenEvent = targetEventCandidates.get(0);
        }
        
        return chosenEvent;
    }
    
    /**
     * Unprotected event category insert point
     * @param ec
     * @return
     * @throws IntegrationException 
     */
    public int addEventCategory(EventCategory ec) throws IntegrationException, BObStatusException{
        EventIntegrator ei = getEventIntegrator();
        auditEventCategory(ec);
        return ei.insertEventCategory(ec);
    }
    
    /**
     * Uprotected event category update path.
     * Legacy active boolean flag preserved even during 2022 updates!!
     * Set active to false to turn off
     * @param ec
     * @throws IntegrationException 
     */
    public void updateEventCategory(EventCategory ec) throws IntegrationException, BObStatusException{
        EventIntegrator ei = getEventIntegrator();
        if(ec == null){
            throw new BObStatusException("Cannot update null cat");
        }
        auditEventCategory(ec);
        ei.updateEventCategory(ec);
        // dump our caches (like half of all caches on an event cat update)
        getCECaseCacheManager().flushAllCaches();
        getPropertyCacheManager().flushAllCaches();
        getOccupancyCacheManager().flushAllCaches();
        getEventCacheManager().flushAllCaches();
        
    }
    
    /**
     * Logic block to check for loops in our alerting system
     * @param ec
     * @throws BObStatusException 
     */
    private void auditEventCategory(EventCategory ec) throws BObStatusException{
        if(ec != null){
            if(ec.isAlertEvent()){
                if(ec.getAlertStopCategory() == null){
                    throw new BObStatusException("Alert events must have a stop category!");
                } else {
                    if(ec.getAlertStopCategory().isAlertEvent()){
                        throw new BObStatusException("Alert event stop categories must NOT themselves be alert events!");
                    }
                }
            } else {
                // with no alert status true, don't allow any stop cat
                ec.setAlertStopCategory(null);
            }
        }
        
    }
    
    
    
//    --------------------------------------------------------------------------
//    ************************ EVENT ALERTS ************************************
//    --------------------------------------------------------------------------
    
    
    
    /**
     * Looks through all the events on a given property data heavy, including
     * inside its own cecases and occ periods and sets alerts that apply to the 
     * overall property
     * 
     * @param pdh
     * @throws BObStatusException 
     */
    public void configurePropertyAlerts(PropertyDataHeavy pdh) throws BObStatusException{
        if(pdh == null){
            throw new BObStatusException("Cannot congiure event alerts on null PDH");
        }
        List<PropertyAlert> alertList = new ArrayList<>();
        if(pdh.getEventListCumulative()!= null && !pdh.getEventListCumulative().isEmpty()){
            for(EventCnF ev: pdh.getEventListCumulative()){
                if(ev.getCategory() != null && ev.getCategory().isAlertEvent()){
                    PropertyAlert alert = new PropertyAlert(ev);
                    alert.setAlertStopEvent(searchForAlertStopEvent(pdh.getEventListCumulative(), ev));
                    alertList.add(alert);
                }
            }
        }
        
        PropertyAlertContainer container = new PropertyAlertContainer(alertList);
        pdh.setPropertyAlerts(container);
        
    }
    
    
    /**
     * A property alert event is deactivated by the presence of an event 
     * on the same property that matches that alert event category's stop
     * alert category whose start time is after the alert event 
     * and before any next instance of that alert event
     * @param evList to search for the qualifying stop alert event
     * @return null if no qualifying stop alert event is located in the list
     */
    private EventCnF searchForAlertStopEvent(List<EventCnF> evList, EventCnF alertEvent) throws BObStatusException{
        EventCnF qualifyingStopEvent = null;
        if(alertEvent == null || alertEvent.getTimeStart() == null){
            throw new BObStatusException("Alert events cannot be null and must not have a null start time");
        }
        if(evList != null && !evList.isEmpty() 
                && alertEvent.getCategory().getAlertStopCategory() != null){
            
            /**
            * Implementation of searchForAlertStopEvent method.
            * This method was PARTIALLY provided by OpenAI's ChatGPT.
            * For more information, visit https://openai.com/
            */
            // Iterate through each event in the list
            for(EventCnF stopEvCand : evList){
                // Check if event is a stop event for the alertEvent
                if(stopEvCand.getTimeStart() != null 
                    && stopEvCand.getCategory().equals(alertEvent.getCategory().getAlertStopCategory())
                    && stopEvCand.getTimeStart().isAfter(alertEvent.getTimeStart())) {
                    
                    // okay, we have an event in our list which matches the required stop alert category
                    // and has been enacted AFTER our alert event so it's a good candidate for stopping our alert
                    qualifyingStopEvent = stopEvCand;
                    
                    // EDGE CASE: additional alert evnts of the same cat
                    // Check if there is a subsequent event of the same cat as our incoming alertEvent category
                    // i.e. look for alert events in the future to which this stop alert event might apply
                    for(EventCnF futureMatchingAlertEventCandidate : evList){
                        if(futureMatchingAlertEventCandidate.getTimeStart() != null 
                                // of the same category
                            && futureMatchingAlertEventCandidate.getCategory().equals(alertEvent.getCategory())
                                // occurring before our candidate
                            && futureMatchingAlertEventCandidate.getTimeStart().isBefore(stopEvCand.getTimeStart())
                                // but AFTER our alert event
                            && futureMatchingAlertEventCandidate.getTimeStart().isAfter(alertEvent.getTimeStart())){
                                // means the stop event we found is NO LONGER qualifying to stop our given ALERT event
                                // (It should qualify as a stop event for the intervening alert event we just found
                                //  but that's not the question we're answering here)
//                                System.out.println("EventCoordinator.searchForAlertStopEvent | Disqualifying our stop event ID " 
//                                        + qualifyingStopEvent.getEventID() 
//                                        + " from stopping our alert event ID " 
//                                        + alertEvent.getEventID() 
//                                        + " due to an intervening alert event to which it is more likely a legitimate stop event");
                                qualifyingStopEvent = null;
                                break;
                        }
                    }
                }
            }
        }
        return qualifyingStopEvent;
   }
    
    
    
    
//    --------------------------------------------------------------------------
//    **************** OPERATION SPECIFIC EVENT CONFIG *************************
//    --------------------------------------------------------------------------
  
      /**
     * Shared method by ce case and occ period coordinators for creating a manager change event
     * 
     * @param officerManaged
     * @param formerManager
     * @param ua
     * @return 
     */
    public List<EventCnF> logManagerChange( IFace_officerManaged officerManaged, User formerManager, UserAuthorized ua) throws BObStatusException, EventException, IntegrationException, AuthorizationException{
        EventCoordinator ec = getEventCoordinator();
        
        List<EventCnF> eventList = new ArrayList<>();
        System.out.println("CaseCoordinator.cecase_updateCECaseManagerAndLogEvent : Manager change success");
        EventCnF managerChangeEvent = ec.initEvent(officerManaged, ec.getEventCategory(Integer.parseInt(
                    getResourceBundle(Constants.EVENT_CATEGORY_BUNDLE)
                            .getString("cecase_update_manager"))));
        StringBuilder sb = new StringBuilder();
        if(formerManager != null){
            sb.append("Manager changed from: ");
            sb.append(formerManager.getHuman().getName());
            sb.append(" (username:");
            sb.append(formerManager.getHuman().getName());
            sb.append(", ID:");
            sb.append(formerManager.getUserID());
            sb.append(") ");
        } 
        sb.append("To: ");
        sb.append(officerManaged.getManagerOverseer().getHuman().getName());
        sb.append(" (username:");
        sb.append(officerManaged.getManagerOverseer().getHuman().getName());
        sb.append(", ID:");
        sb.append(officerManaged.getManagerOverseer().getUserID());
        sb.append(") by ");
        sb.append(ua.getHuman().getName());
        sb.append(" (username:");
        sb.append(ua.getUserHuman().getName());
        sb.append(", ID:");
        sb.append(ua.getUserID());
        sb.append(".");
        
        managerChangeEvent.appendToDescription(sb.toString());
        
        ec.addEvent(managerChangeEvent, officerManaged, ua);
        
        return eventList;
        
    }
    
    /**
     * Creates a populated event to log the change of a code violation update.The event is coming to us from the violationEditBB with the description and disclosures flags
 correct. 
     * This method needs to set the description from the resource bundle, and 
 set the date of record to the current date
     * @param ceCase the CECaseDataHeavy whose violation was updated
     * @param cv the code violation being updated
     * @param event An initialized event
     * @param ua
     * @throws IntegrationException bubbled up from the integrator
     * @throws EventException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.ViolationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void generateAndInsertCodeViolationUpdateEvent(CECaseDataHeavy ceCase, CodeViolation cv, EventCnF event, UserAuthorized ua) 
            throws IntegrationException, EventException, BObStatusException, ViolationException, AuthorizationException{
        EventIntegrator ei = getEventIntegrator();
        CaseCoordinator cc = getCaseCoordinator();
        String updateViolationDescr = getResourceBundle(Constants.MESSAGE_TEXT).getString("violationChangeEventDescription");
       
        // hard coded for now
//        event.setCategory(ei.getEventCategory(117));
        event.setCeCaseID(ceCase.getCaseID());
//        event.setDateOfRecord(LocalDateTime.now());
        event.appendToDescription(updateViolationDescr);
        //even descr set by violation coordinator
        event.setCreatedBy(getSessionBean().getSessUser());
        // disclose to muni from violation coord
        // disclose to public from violation coord
        event.setActive(true);
        addEvent(event, ceCase, ua);
        
        
    }
    
      /**
     * Takes in a well-formed event message from the CaseCoordinator and
     * initializes the appropriate properties on the event before insertion
     * @param caseID id of the case to which the event should be attached
     * @param message the text of the event description message
     * @throws IntegrationException in the case of broken integration process
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.EventException
     */
    public void attachPublicMessagToCECase(int caseID, String message) throws IntegrationException, BObStatusException, EventException{
        EventIntegrator ei = getEventIntegrator();
        UserCoordinator uc = getUserCoordinator();
        
        int publicMessageEventCategory = Integer.parseInt(getResourceBundle(Constants.EVENT_CATEGORY_BUNDLE).getString("publicCECaseMessage"));
        EventCategory ec = initEventCategory(publicMessageEventCategory);
        
        // setup all the event properties
        EventCnF event =  initEvent(null, ec);
        event.setCategory(ec);
//        event.setDateOfRecord(LocalDateTime.now());
        event.appendToDescription(message);
        event.setCreatedBy(uc.user_getUserRobot());
        event.setActive(true);
        event.setHidden(false);
        event.setNotes("Event created by a public user");
        
        // sent the built event to the integrator!
        ei.insertEvent(event);
    }
    
    /**
     * Configures an event which represents the moment of compliance with
     * a code violation attached to a code enforcement case
     * 
     * @param violation
     * @return a partially-baked event ready for inserting
     * @throws IntegrationException 
     */
    public EventCnF generateViolationComplianceEvent(CodeViolation violation) throws IntegrationException{
        EventCnF e = new EventCnF();
        EventIntegrator ei = getEventIntegrator();
          e.setCategory(ei.getEventCategory(Integer.parseInt(getResourceBundle(
                Constants.EVENT_CATEGORY_BUNDLE).getString("complianceEvent"))));
        e.appendToDescription("Compliance with municipal code achieved");
        e.setActive(true);
        
        StringBuilder sb = new StringBuilder();
        sb.append("Compliance with the following code violations was observed:");
        sb.append("<br /><br />");
        sb.append(violation.getViolatedEnfElement().getHeaderString());
        sb.append("<br /><br />");
        e.appendToDescription(sb.toString());
        EventCnF cev = new EventCnF(e);
        return cev;
    }
    
    
    /**
     * Foundation of the calendar export system
     * @param <T>
     * @param selEventCnFs 
     */
    public <T extends EventCnF> void exportCalendar(List<T> selEventCnFs) {
        FacesContext facesContext = getFacesContext();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();

        try
        {
            // Generate the .ics file
            Calendar calendar = generateCalendar(selEventCnFs);
            CalendarOutputter outputter = new CalendarOutputter();
            outputter.setValidating(false);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            outputter.output(calendar, baos);
            byte[] icsContent = baos.toByteArray();

            // Set response headers for file download
            response.reset();
            response.setContentType("text/calendar");
            response.setHeader("Content-Disposition", "attachment; filename=events.ics");
            response.setContentLength(icsContent.length);

            // Write the file content to the response output stream
            // Write the file content to the response output stream
            try (ServletOutputStream outputStream = response.getOutputStream())
            {
                outputStream.write(icsContent);
            }

            facesContext.responseComplete();

        } catch (IOException | NoSuchElementException e)
        {
            System.out.println("Exception: " + e.getMessage());
            getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    e.getMessage(), ""));
        }
    }
    
    /**
     * Creates a Calendar object from the given event list
     * @param <T>
     * @param selectedEvents
     * @return 
     */
     private <T extends EventCnF> Calendar generateCalendar(List<T> selectedEvents) {
        Calendar calendar = new Calendar();
        calendar.getProperties().add(new ProdId("-//tcvce//codenforce//EN"));
        calendar.getProperties().add(Version.VERSION_2_0);

        for (EventCnF ev : selectedEvents)
        {
            VEvent event = new VEvent();

            try
            {
                DateTime start = new DateTime(ev.getTimeStart().atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli());
                DateTime end = new DateTime(ev.getTimeEnd().atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli());
                String description = ev.getDescription();
                event.getProperties().add(new DtStart(start));
                event.getProperties().add(new DtEnd(end));
                event.getProperties().add(new Summary(ev.getCategory().getEventCategoryTitle() + " " + ev.getCategory().getEventType().getLabel()));
                event.getProperties().add(new Description(description));

                List<HumanLink> humanLinks = ev.gethumanLinkList();
                if (Objects.nonNull(humanLinks))
                {
                    if (!humanLinks.isEmpty())
                    {
                        for (HumanLink human : humanLinks)
                        {
                            List<ContactEmail> emailList = human.getEmailList();
                            if (!emailList.isEmpty())
                            {
                                ContactEmail email = emailList.get(0);
                                // Add attendee with email
                                Attendee attendee = new Attendee(URI.create("mailto:" + email.getEmailaddress()));
                                attendee.getParameters().add(Role.REQ_PARTICIPANT);
                                attendee.getParameters().add(new Cn(human.getName()));
                                event.getProperties().add(attendee);
                            } else
                            {
                                // Add attendee without email
                                Attendee attendee = new Attendee();
                                attendee.getParameters().add(Role.REQ_PARTICIPANT);
                                attendee.getParameters().add(new Cn(human.getName()));
                                event.getProperties().add(attendee);
                            }
                        }
                    }
                }

                calendar.getComponents().add(event);
            } catch (NullPointerException e)
            {
                System.out.println("NullPointerException: " + e.getMessage());
            } catch (Exception ex)
            {
                System.out.println("Exception: " + ex.getMessage());
            }
        }
        return calendar;
    }
} // close class