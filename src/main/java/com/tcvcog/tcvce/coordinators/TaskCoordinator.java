/*
 * Copyright (C) 2023 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.coordinators;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.occupancy.application.TaskAssigned;
import com.tcvcog.tcvce.occupancy.application.TaskAssignedRoleEnum;
import com.tcvcog.tcvce.occupancy.application.TaskAssignee;
import com.tcvcog.tcvce.occupancy.application.TaskChain;
import com.tcvcog.tcvce.occupancy.application.TaskDueDateTypeEnum;
import com.tcvcog.tcvce.occupancy.application.TaskLinkage;
import com.tcvcog.tcvce.occupancy.application.TaskSequenceTypeEnum;
import com.tcvcog.tcvce.occupancy.application.TaskSpecification;
import com.tcvcog.tcvce.occupancy.application.TaskTypeEnum;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.EventCnF;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.UserAuthorized;
import com.tcvcog.tcvce.entities.UserMuniAuthPeriod;
import com.tcvcog.tcvce.entities.occupancy.OccPeriod;
import com.tcvcog.tcvce.entities.occupancy.OccPeriodDataHeavy;
import com.tcvcog.tcvce.integration.TaskIntegrator;
import com.tcvcog.tcvce.occupancy.application.TaskAssignedStatusEnum;
import com.tcvcog.tcvce.occupancy.application.TaskOrganizer;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 * Home for logic related to the task subsystem, which initially in SEPT 2023
 * applies to Occ Period and permitting task sequence management.
 * 
 * This class replaces the now deprecated WorkFlow subfamily
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */
public class TaskCoordinator extends BackingBeanUtils{
    
    
      
    /*******************************************************************
     ************** TASK PERMISSIONS    ********************************
     *******************************************************************
     */
      
    /**
     * Security gateway for CRUD on task chains
     * @param ua
     * @return 
     */
    public boolean permissionsCheckpointManageTaskChains(UserAuthorized ua){
        if(ua == null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        if(!ua.getKeyCard().isHasMuniReaderPermissions()){
            return false;
        }
        return true;
        
    }
    
    /**
     * Security gate for completing a task
     * @param task
     * @param ua
     * @return 
     */
    public boolean permissionsCheckpointCompleteTask(TaskAssigned task, UserAuthorized ua){
        if(ua == null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        if(!ua.getKeyCard().isHasMuniReaderPermissions()){
            return false;
        }
        return true;
    }
    
    /**
     * Security gate for nullifying task
     * @param task
     * @param ua
     * @return 
     */
    public boolean permissionsCheckpointNullifyDeacTask(TaskAssigned task, UserAuthorized ua){
        if(ua == null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        if(!ua.getKeyCard().isHasMuniReaderPermissions()){
            return false;
        }
        return true;
        
    }
    
    
      
    /*******************************************************************
     ************** TASK ENUMS *****************************************
     *******************************************************************
     */
    
    
    /**
     * Getter of all values in the TaskTypeEnum as a List
     * @return 
     */
    public List<TaskTypeEnum> getTaskTypeEnumValues(){
        return Arrays.asList(TaskTypeEnum.values());
    }
    
    /**
     * Getter of all values in the TaskAssignedRoleEnum as a List
     * @return 
     */
    public List<TaskAssignedRoleEnum> getTaskAssignedRoleEnumValues(){
        return Arrays.asList(TaskAssignedRoleEnum.values());
    }
    
    
    /**
     * Getter of all values in the TaskDueDateTypeEnum as a List
     * @return 
     */
    public List<TaskDueDateTypeEnum> getTaskDueDateTypeEnumValues(){
        return Arrays.asList(TaskDueDateTypeEnum.values());
    }
    
    
    /**
     * Getter of all values in the TaskDueDateTypeEnum as a List
     * @return 
     */
    public List<TaskSequenceTypeEnum> getTaskSequenecTypeEnumValues(){
        return Arrays.asList(TaskSequenceTypeEnum.values());
    }
    
      
    /*******************************************************************
     ************** TASK SPECIFICATIONS ********************************
     *******************************************************************
     */
    
    /**
     * Factory method for TaskSpecification objects; Call before inserting into DB
     * @param ua
     * @return 
     */
    public TaskSpecification getTaskSpecificationSkeleton(UserAuthorized ua) throws BObStatusException{
        if(ua == null){
            throw new BObStatusException("cannot get TaskSpec skeleton with null requesting user");
        }
        TaskSpecification spec = new TaskSpecification();
        spec.setActive(true);
        return spec;
        
    }
    
    /**
     * Getter for TaskSpecifications which are the not implemented
     * Task superclass.When implemented, the subclass TaskAssigned 
     * is used which lives in a specific OccPeriod
     * 
     * @param specificationID
     * @return  the populated TaskSpecification
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public TaskSpecification getTaskSpecification(int specificationID) throws BObStatusException, IntegrationException{
        if(specificationID == 0){
            throw new BObStatusException("Cannot get task specification with ID == 0");
        }
        
       TaskIntegrator ti = getTaskIntegrator();
        
       TaskSpecification spec = ti.getTaskSpecification(specificationID);
       // check to avoid cycles in incorrectly configured specs
       if(spec.getPredecessorID() != 0 && spec.getPredecessorID() != spec.getTaskSpecID()){
           spec.setPredecessor(getTaskSpecification(spec.getPredecessorID()));
       }
       
       return spec;
        
        
    }
    
    /**
     * Builds a list of task specs by chain
     * @param chain
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public List<TaskSpecification> getTaskSpecificationList(TaskChain chain) throws IntegrationException, BObStatusException{
        TaskIntegrator ti = getTaskIntegrator();
        List<TaskSpecification> specList = new ArrayList();
        List<Integer> specIDList = ti.getTaskSpecifcationsByChain(chain);
        if(specIDList != null && !specIDList.isEmpty()){
            for(Integer id: specIDList){
                specList.add(getTaskSpecification(id));
            }
        }
        return specList;
    }
    
    /**
     * Adds the given TaskSpecification to the DB
     * @param spec with fields populated for insert, with an ID of 0
     * @param chain
     * @param ua requesting the insert
     * @return the DB key of the just inserted record
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public int insertTaskSpecification(TaskSpecification spec, TaskChain chain, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        
        if(spec == null || ua == null || spec.getTaskSpecID() != 0 || chain == null){
            throw new BObStatusException("Cannot insert TaskSpec with null spec or user or nonzero ID");
        }
        if(!permissionsCheckpointManageTaskChains(ua)){
            throw new AuthorizationException("Permissions exception: user lacks rank to insert task specification");
        }
        TaskIntegrator ti = getTaskIntegrator();
        int freshSpecID = ti.insertTaskSpecification(spec);
        // now connect chain to spec
        spec = getTaskSpecification(freshSpecID);
        spec.setChainParentID(chain.getChainID());
        ti.connectTaskSpecToTaskChain(spec);
        
        return freshSpecID;
        
    }
    
    /**
     * Update task specification
     * 
     * @param spec
     * @param ua
     * @throws BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void updateTaskSpecification(TaskSpecification spec, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        
        if(spec == null || ua == null || spec.getTaskSpecID() == 0){
            throw new BObStatusException("Cannot insert TaskSpec with null spec or user or ID of zero");
        }
        if(!permissionsCheckpointManageTaskChains(ua)){
            throw new AuthorizationException("Permissions exception: user lacks rank to update task specification");
        }
        TaskIntegrator ti = getTaskIntegrator();
        ti.updateTaskSpecification(spec);
    }
    
    /**
     * Deactivates the given task spec
     * @param spec
     * @param ua
     * @throws BObStatusException 
     */
    public void deactivateTaskSpecification(TaskSpecification spec, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        
        if(spec == null || ua == null || spec.getTaskSpecID() == 0){
            throw new BObStatusException("Cannot insert TaskSpec with null spec or user or ID of zero");
        }
        if(!permissionsCheckpointManageTaskChains(ua)){
            throw new AuthorizationException("Permissions exception: user lacks rank to deac task specification");
        }
        TaskIntegrator ti = getTaskIntegrator();
        ti.deactivateTaskSpecification(spec);
    }
    
    /**
     * Organizes a primefaces Tree from a chain
     * @param chain
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public TreeNode<TaskSpecification> buildTreeFromTaskChain(TaskChain chain) throws BObStatusException{
        if(chain == null || chain.getTaskList() == null){
            throw new BObStatusException("Cannot build tree from null chain or chain with null task list");
        }
        
        TreeNode<TaskSpecification> root = new DefaultTreeNode<>(new TaskSpecification("File Open"), null);
        TreeNode<TaskSpecification> current =  null;
        // make a map for easy extraction
        Map<Integer, TaskSpecification> specMap = new HashMap<>();
        Map<Integer, TreeNode<TaskSpecification>> nodeMap = new HashMap<>();
        for(TaskSpecification spec: chain.getTaskList()){
            specMap.put(spec.getTaskSpecID(), spec);
        }
        // now build the primefaces tree, extracting the parents as declared in the spec
        for(TaskSpecification spec: chain.getTaskList()){
//            current = (TreeNode) specMap.get(spec.getTaskSpecID());
            if(spec.getPredecessor() != null){
                if(nodeMap.containsKey(spec.getPredecessor().getTaskSpecID())){
                    // Linking to parent basically must happen here in the constructor. The setter doesn't call the internal configuration method
                    current = new DefaultTreeNode(spec, nodeMap.get(spec.getPredecessor().getTaskSpecID()));
                } else {
                    System.out.println("Could not access parent node for spec: " + spec.getTitle()+ " | skipping node!");
                }
            } else {
                System.out.println("TaskCoordinator.buildTree | No predecessor node | linking to root | there should only be one of me");
                current = new DefaultTreeNode(spec, root);
            }
            if(current != null){
                current.setExpanded(true);
                nodeMap.put(spec.getTaskSpecID(), current);
                System.out.println("Just attached SPEC ID: " + current.toString() + " my parent is " + current.getParent().toString());
            }
        }
        
        return root;
        
    }
    

    
    /*******************************************************************
     * ************ TASK ASSIGNED *************************************
     *******************************************************************
     */

    /**
     * Sets up the TaskAssigned list inside the TaskOrganizer for the given Occ Period.
     * @param opdh Must have a non-null occ period type since that's where the TaskChain 
     * mapping occurs.
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void initializeOccPeriodTaskSubsystem(OccPeriodDataHeavy opdh, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(opdh == null || ua == null){
            throw new BObStatusException("Cannot init tasks with null period or user");
        }
        if(opdh.getPeriodType() == null){
            throw new BObStatusException("Cannot init tasks on an occ period with a null type");
        }
        
        assignTaskChainToOccPeriod(opdh.getPeriodType().getChain(), opdh);
        
    }
    
    
    /**
     * Logic organ for setting the status of the TaskAssigned in the belly of an
     * OccPeriodDataHeavy whose task system has already been initialized.
     * 
     * @param opdh 
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void configureOccPeriodTasks(OccPeriodDataHeavy opdh, UserAuthorized ua) 
            throws BObStatusException, IntegrationException, AuthorizationException{
        if(opdh == null ){
            return;
        }
        opdh.setTaskOrganizer(getTaskOrganizer(opdh, ua));
    }
    
    
    /**
     * Organizes a primefaces Tree from a list of TaskAssigned objects
     * This code is basically duplicated from the TaskSpec version because 
     * Eric didn't want to figure out Generics right now.
     * @param assignedList
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public TreeNode<TaskAssigned> buildTreeFromTaskAssigned(List<TaskAssigned> assignedList) throws BObStatusException{
        if(assignedList == null){
            throw new BObStatusException("Cannot build tree from null task assigned list");
        }

        TreeNode<TaskAssigned> root = new DefaultTreeNode<>(new TaskAssigned(new TaskSpecification("File Open")), null);
        TreeNode<TaskAssigned> current =  null;
        // make a map for easy extraction
        Map<Integer, TaskSpecification> taMap = new HashMap<>();
        Map<Integer, TreeNode<TaskAssigned>> nodeMap = new HashMap<>();
        for(TaskSpecification spec: assignedList){
            taMap.put(spec.getTaskSpecID(), spec);
        }
        // now build the primefaces tree, extracting the parents as declared in the spec
        for(TaskAssigned ta: assignedList){
//            current = (TreeNode) specMap.get(spec.getTaskSpecID());
            if(ta.getPredecessor() != null){
                if(nodeMap.containsKey(ta.getPredecessor().getTaskSpecID())){
                    current = new DefaultTreeNode(ta, nodeMap.get(ta.getPredecessor().getTaskSpecID()));
                } else {
                    System.out.println("Could not access parent node for spec: " + ta.getTitle()+ " | skipping node!");
                }
            } else {
                System.out.println("TaskCoordinator.buildTree | No predecessor node | linking to root | there should only be one of me");
                current = new DefaultTreeNode(ta, root);
            }
            if(current != null){
                current.setExpanded(true); // show full tree for now
                nodeMap.put(ta.getTaskSpecID(), current);
                System.out.println("Just attached SPEC ID: " + current.toString() + " my parent is " + current.getParent().toString());
            }
        }
        return root;
    }
    
    /**
     * Factory method for TaskAssigned
     * @param spec
     * @param period
     * @param ua
     * @return  object with ID == 0 for config. Then call insertTaskAssigned
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public TaskAssigned getTaskAssignedSkeleton(TaskSpecification spec, 
                                                OccPeriod period, 
                                                UserAuthorized ua) 
            throws BObStatusException{
        if(spec == null || period == null || ua == null){
            throw new BObStatusException("Cannot create TaskAssigned skeleton with null requesting user");            
        }
        
        TaskAssigned ta = new TaskAssigned(spec);
        ta.setOccPeriodID(period.getPeriodID());
        return ta;
    }
    
    
    /**
     * Getter for TaskAssigned objects
     * 
     * @param taskAssignedID
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public TaskAssigned getTaskAssigned(int taskAssignedID) throws IntegrationException, BObStatusException{
        TaskIntegrator ti = getTaskIntegrator();
        TaskAssigned ta = ti.getTaskAssigned(taskAssignedID);
        
        
        
        return ta;
    }
    
    
    
    
    /**
     * Assembles a container object for grouping assigned tasks and the tree
     * version of those tasks associated with an OccPeriod
     * @param per
     * @param ua
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public TaskOrganizer getTaskOrganizer(OccPeriodDataHeavy per, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        TaskOrganizer org = new TaskOrganizer();
        org.setTaskAssignedList(getTaskAssignedListByOccPeriod(per));
        org.setTaskTree(buildTreeFromTaskAssigned(org.getTaskAssignedList()));
        configureTaskOrganizer(org, per, ua);
        return org;
    }
    
    /**
     * Massive method for assigning the status of various tasks
     * @param org
     * @param ua
     * @return 
     */
    private TaskOrganizer configureTaskOrganizer(TaskOrganizer org, OccPeriodDataHeavy opdh, UserAuthorized ua) 
            throws BObStatusException, IntegrationException, AuthorizationException{
        if(org == null || ua == null || opdh == null){
            throw new BObStatusException("cannot configure null task oragnizer or with null user");
        }
        
        List<TaskAssigned> taskAssignmentList = org.getTaskAssignedList();
        
        if(taskAssignmentList != null && !taskAssignmentList.isEmpty()) {
            for(TaskAssigned task: taskAssignmentList){
                System.out.println("tc.configureTaskOrganizer | Evaluating task ID: " + task.getAssignmentID());
                // cope with incomplete tasks
                if(!determineIfTaskAssignedIsComplete(task)){
                    evaluateIncompleteTaskForCompletion(task, opdh, ua);
                } else {
                    task.setStatusEnum(TaskAssignedStatusEnum.COMPLETE);
                }
            }
        }
        return org;
    }
    
    /**
     * Uses the incoming task's type to scour the parent occ period for evidence of
     * completeness
     * @param task
     * @param opdh
     * @param ua
     * @return 
     */
    private TaskAssigned evaluateIncompleteTaskForCompletion(   TaskAssigned task, 
                                                                OccPeriodDataHeavy opdh, 
                                                                UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        if(task == null || opdh == null || ua == null || task.getTaskType() == null){
            throw new BObStatusException("Cannot evaluate task completion by type with nul task, period or user or type");
        }

        // The most straightforward completion pathway is a matching event cat on the period
        task = evaluateIncompleteTaskForCompletionUsingEventCatsAndUpdateDB(task, opdh, ua);
        
        if(!determineIfTaskAssignedIsComplete(task)){

            switch (task.getTaskType()) {
                case OPENING -> {
                    System.out.println("Handling OPENING task");
                    // we'll call all of these complete
                    recordTaskCompletion(task, null, null, ua);
                }
                case CLOSING -> {
                    System.out.println("Handling CLOSING task");
                }
                case MILESTONE -> {
                    System.out.println("Handling MILESTONE task");
                }
                case BRANCHCHOICE -> {
                    System.out.println("Handling BRANCHCHOICE task");
                }
                case PAYMENT -> {
                    System.out.println("Handling PAYMENT task");
                    // payment tasks not supported; These will be event driven only
                }
                case INSPECTION -> {
                    System.out.println("Handling INSPECTION task");
                }
                case PERMIT -> {
                    System.out.println("Handling PERMIT task");
                }
                case LETTER -> {
                    System.out.println("Handling LETTER task");
                }
                case FILE -> {
                    System.out.println("Handling FILE task");
                }
                case DOCUMENTPERSONREVIW -> {
                    System.out.println("Handling DOCUMENTPERSONREVIW task");
                }
                case COMMUNICATION -> {
                    System.out.println("Handling COMMUNICATION task");
                }
                case EXTERNAL -> {
                    System.out.println("Handling EXTERNAL task");
                }
                case CUSTOM -> {
                    System.out.println("Handling CUSTOM task");
                }
                default -> {
                    System.out.println("Handling default case");
                }
            }
            // Assess task for over due status usings its due date
        } else {
            // Pause to implement until after initial testing
        }

        
        return task;
    }
    
    
    
    /**
     * Lil logic block for checking task assigned completion status
     * @param task
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public boolean determineIfTaskAssignedIsComplete(TaskAssigned task) throws BObStatusException{
        if(task == null){
            throw new BObStatusException("Cannot determine completion status of null input");
        }
        return task.getDateCompletionTS() == null;
    }
    
    /**
     * Internal organ for evaluating task completion status using the state of the given occ period
     * @param task refreshed to reflect any record changes by this method's logic
     * @param opdh
     * @return the task assigned with the completion timestamp written to DB and the fresh copy returned
     */
    private TaskAssigned evaluateIncompleteTaskForCompletionUsingEventCatsAndUpdateDB(
                                                        TaskAssigned task, 
                                                        OccPeriodDataHeavy opdh, 
                                                        UserAuthorized ua) 
                                                throws  BObStatusException,     
                                                        IntegrationException, 
                                                        AuthorizationException{
        EventCoordinator ec = getEventCoordinator();
        TaskIntegrator ti = getTaskIntegrator();
        
        if(task == null || opdh == null || ua == null){
            throw new BObStatusException("Cannot evaluate task with nul task, period or user");
        }
        
        // first check if there's an event cat that matches the completion event cat on the taskspec
        if(task.getEventCategoryForCompletion() != null){
            EventCnF compEv = ec.determinePresenceOfEventCategoryInList(opdh.getEventList(), task.getEventCategoryForCompletion());
            if(compEv != null){
                // We have a matching completion event, so mark completion and link the event
                recordTaskCompletion(task, compEv.getTimeStart(), new TaskLinkage(task, compEv), ua);
            }
        }
        
        return task;
    }
    
    /**
     * Sets the members on the given TaskAssigned and writes the changes to the DB
     * with this class's updateTaskAssigned method
     * @param task to be completed
     * @param compDOR the reference date to inject into dateCompletionDOR. If null, now() is used
     * @param ua doing the completion
     * @param linkage if not null a link will be made otherwise this is skipped
     * @return the updated task
     */
    private TaskAssigned recordTaskCompletion(  TaskAssigned task, 
                                                LocalDateTime compDOR, 
                                                TaskLinkage linkage, 
                                                UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        if(task == null || ua == null){
            throw new BObStatusException("Canot record completion for task with null task or user");
        }
        
        TaskIntegrator ti = getTaskIntegrator();
        if(compDOR != null){
            task.setDateCompletionDateOfRecord(compDOR);
        } else {
            task.setDateCompletionDateOfRecord(LocalDateTime.now());
        }
        task.setCompletionCertificationByUMAP(ua.getKeyCard().getGoverningAuthPeriod());
        task.setDateCompletionTS(LocalDateTime.now());
        task.setStatusEnum(TaskAssignedStatusEnum.COMPLETE);
        
        ti.updateTaskAssigned(task);
        // now deal with links
        LinkTaskToObject(linkage, ua);
        // refrsh
        task = getTaskAssigned(task.getAssignmentID());
        return task;
        
    }
    
    
    
    /**
     * Creates TaskAssigned objects out of the given chain's TaskSpecification list
     * and writes all this to the DB
     * @param chain
     * @param per 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void assignTaskChainToOccPeriod(TaskChain chain, OccPeriod per) throws BObStatusException, IntegrationException{
        if(chain == null || per == null || chain.getTaskList() == null){
            throw new BObStatusException("Cannot assign tasks from null chain or to a null period");
        }
        TaskIntegrator ti = getTaskIntegrator();
        for(TaskSpecification tspec: chain.getTaskList()){
            TaskAssigned ta = new TaskAssigned(tspec);
            ti.insertTaskAssigned(ta);
        }       
    }
    
    /**
     * Retrieval point for a list of TaskAssigned objects
     * @param per
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public List<TaskAssigned> getTaskAssignedListByOccPeriod(OccPeriod per) throws BObStatusException, IntegrationException{
        if(per == null){
            throw new BObStatusException("Cannot get task list by period with null period input");
        }
        TaskIntegrator ti = getTaskIntegrator();
        TaskCoordinator tc = getTaskCoordinator();
        List<Integer> idList = ti.getTaskAssignedListByOccPeriod(per);
        List<TaskAssigned> assignments = new ArrayList<>();
        if(idList != null && !idList.isEmpty()){
            for(Integer id: idList){
                assignments.add(tc.getTaskAssigned(id));
            }
        }
        return assignments;
    }
    
     /**
     * Adds the given TaskAssigned to the DB
     * @param ta
     * @param ua requesting the insert
     * @return the DB key of the just inserted record
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public int insertTaskAssigned(TaskAssigned ta, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(ta == null || ua == null || ta.getAssignmentID() != 0){
            throw new BObStatusException("Cannot insert TaskAssignment with null task or user or nonzero ID");
        }
        TaskIntegrator ti = getTaskIntegrator();
        return ti.insertTaskAssigned(ta);
    }
    
    
    /**
     * Gateway for marking a task as completed which will update the relevant 
     * taskassigned record and attach the relevant event to the occ period, 
     * and link both of these
     * @param ta
     * @param opdh
     * @param ua 
     */
    public void completeTask(TaskAssigned ta, OccPeriodDataHeavy opdh, UserAuthorized ua){
        
        
        
    }
    
    
    /**
     * Update task assignment.These aret asks that are connected to an OccPeriod (aka Permit File)
     * This method is private because clients of the tasks system must call
     * completeTask or other related methods that will enforce business rules
     * 
     * @param ta
     * @param ua
     * @throws BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    private void updateTaskAssigned(TaskAssigned ta, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        if(ta == null || ua == null || ta.getAssignmentID() == 0){
            throw new BObStatusException("Cannot insert TaskAssignemnt with null ta or user or ID of zero");
        }
        if(!permissionsCheckpointCompleteTask(ta, ua)){
            throw new AuthorizationException("Permissions exception: user lacks rank to insert task specification");
        }
        TaskIntegrator ti = getTaskIntegrator();
        ti.updateTaskAssigned(ta);
    }
    
    /**
     * Deactivates the given task assignment
     * @param ta
     * @param ua
     * @throws BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void deactivateTaskAssigned(TaskAssigned ta, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        if(ta == null || ua == null || ta.getAssignmentID() == 0){
            throw new BObStatusException("Cannot insert TaskAssigned with null ta or user or ID of zero");
        }
        if(!permissionsCheckpointNullifyDeacTask(ta, ua)){
            throw new AuthorizationException("Permissions exception: user lacks rank to insert task specification");
        }
        TaskIntegrator ti = getTaskIntegrator();
        ti.deactivateTaskAssigned(ta);
    }
    
    
    /**
     * Factory for task chains
     * @param muni
     * @return 
     */
    public TaskChain getTaskChainSkeleton(Municipality muni){
        TaskChain chain = new TaskChain();
        chain.setMuni(muni);
        chain.setActive(true);
        return chain;
    }
    
    /**
     * Getter for TaskChain objects
     * @param chainID
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public TaskChain getTaskChain(int chainID) throws BObStatusException, IntegrationException{
        if(chainID == 0){
            throw new BObStatusException("Cannot get task chain with ID == 0");
        }
        TaskIntegrator ti = getTaskIntegrator();
        TaskChain chain = ti.getTaskChain(chainID);
        List<TaskSpecification> specList = getTaskSpecificationList(chain);
        Collections.sort(specList);
        chain.setTaskList(specList);
        return chain;
        
        
    }
    
    /**
     * Extracts all task chains from the DB
     * @param muni returns all munis if null
     * @param includeInactive
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public List<TaskChain> getTaskChainList(Municipality muni, boolean includeInactive) throws IntegrationException, BObStatusException{
        TaskIntegrator ti = getTaskIntegrator();
        List<Integer> chainIDL = ti.getTaskChainIDListComplete();
        List<TaskChain> chainList = new ArrayList<>();
        if(chainIDL != null && !chainIDL.isEmpty()){
            for(Integer id: chainIDL){
                TaskChain chain = getTaskChain(id);
                if(chain.getMuni() != null && muni != null && chain.getMuni().getMuniCode() != muni.getMuniCode()){
                    continue;
                }
                if(!chain.isActive() && !includeInactive){
                    continue;
                }
                chainList.add(chain);
            }
        }
        return chainList;
    }
    
    /**
     * Task chain insertion point
     * @param chain
     * @param ua
     * @return the ID of the freshly inserted Chain
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public int insertTaskChain(TaskChain chain, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        if(chain == null || ua == null){
            throw new BObStatusException("Cannot insert task chain with null chain or user");
        }
        if(!permissionsCheckpointManageTaskChains(ua)){
            throw new AuthorizationException("Permissions exception: user lacks rank to insert task specification");
        }
        TaskIntegrator ti = getTaskIntegrator();
        return ti.insertTaskChain(chain);
        
        
    }
    
    /**
     * Updates a given task chain's title and description.Use UpdateTaskChainTasks
 for changes to the actual Tasks in a chain
     * @param chain
     * @param ua  requesting the update
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void updateTaskChainMetadata(TaskChain chain, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        if(chain == null || ua == null){
            throw new BObStatusException("Cannot update task chain with null chain or user");
        }
        if(!permissionsCheckpointManageTaskChains(ua)){
            throw new AuthorizationException("Permissions exception: user lacks rank to insert task specification");
        }
        TaskIntegrator ti = getTaskIntegrator();
        ti.updateTaskChain(chain);
    }
    
    /**
     * Toggles active to false on a given task chain;
     * 
     * @param chain
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void deactivateTaskChain(TaskChain chain, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        if(chain == null || ua == null){
            throw new BObStatusException("Cannot deactivate task chain with null chain or user");    
        }
        if(!permissionsCheckpointManageTaskChains(ua)){
            throw new AuthorizationException("Permissions exception: user lacks rank to insert task specification");
        }
        TaskIntegrator ti = getTaskIntegrator();
        ti.deactivateTaskChain(chain);
    }
    
    /**
     * Factory method for TaskLinkage objects
     * @param task to which the links shall be made; then write the links
     * with LinkTaskToObject method
     * @return the skeleton object with the TaskAssigned id written
     */
    public TaskLinkage getTaskLinkageSkeleton(TaskAssigned task){
        return new TaskLinkage(task);
        
    }
    
    /**
     * Connects a given TaskLinkage to the TaskAssignedID contained within
     * 
     * @param linkage must have nonzero taskAssignedID
     * @param ua requesting the link
     * @return  the ID of the fresh link
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public int LinkTaskToObject(TaskLinkage linkage, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
    
        if(linkage == null || ua == null || linkage.getTaskAssignedID() == 0){
            throw new BObStatusException("cannot link task to object with null link, user, or link with assignment ID of 0");
        }
        
        TaskIntegrator ti = getTaskIntegrator();
        return ti.insertTaskLinkage(linkage);
        
    }
    
    
    /*******************************************************************
     * ************ TASK ROLE MAPPINGS *********************************
     *******************************************************************
     */

    
    /**
     * Assembles a list of all possible roles for task assignment
     * @param muni if null, all role mappings are returned for configuration purposes
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public List<TaskAssignee> getTaskAssigneeCandidateList(Municipality muni) throws IntegrationException, BObStatusException{
        TaskIntegrator ti = getTaskIntegrator();
        List<TaskAssignee> assigneeCandidates = ti.getTaskAssigneesByMuni(muni);
        return assigneeCandidates;
    }
    
    /**
     * Creates a skeleton mapping object between user roles and a user for task management
     * purposes
     * 
     * @param targetUMAP
     * @param role
     * @return 
     */
    public TaskAssignee getTaskAssignmentSkeleton(UserMuniAuthPeriod targetUMAP, TaskAssignedRoleEnum role){
        return new TaskAssignee(targetUMAP, role);
        
    }
    
    /**
     * Logic gate for task assignments.
     * @param assignmentID
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public TaskAssignee getTaskAssignee(int assignmentID) throws BObStatusException, IntegrationException{
        if(assignmentID == 0){
            throw new BObStatusException("Cannot get task assignment with input ID == 0");
        }
        TaskIntegrator ti = getTaskIntegrator();
        return ti.getTaskAssignee(assignmentID);
    }
    
     /**
      * Creates a mapping between a UMAP and a role assignment for tasks
     * @param assignee
      * @param ua
      * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
      */
    public int mapUserToTaskRole(TaskAssignee assignee, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{ 
        if(assignee == null || ua == null || assignee.getRole() == null || assignee.getUser() == null){
            throw new BObStatusException("Cannot write new mapping with null target UMAP, role, or UA");
        }
        if(!permissionsCheckpointManageTaskChains(ua)){
            throw new AuthorizationException("Permissions exception: user lacks rank to insert task specification");
        }
        TaskIntegrator ti = getTaskIntegrator();
        return ti.connectUserToRole(assignee);
    }
    
    /**
     * Wipes the given mapping of a UMAP to an assignment Role
     * @param assignee
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void clearMappingOfUserToTaskRole(TaskAssignee assignee, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        if(assignee == null || ua == null){
            throw new BObStatusException("Cannot wipe apping with null target UMAP, role, or UA");
        }
        if(!permissionsCheckpointManageTaskChains(ua)){
            throw new AuthorizationException("Permissions exception: user lacks rank to insert task specification");
        }
        TaskIntegrator ti = getTaskIntegrator();
        ti.removeUserRoleConection(assignee);
    }
}

    
