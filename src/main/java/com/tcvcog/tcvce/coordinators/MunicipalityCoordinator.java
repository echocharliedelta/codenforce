/*
 * Copyright (C) 2019 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.coordinators;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.EventException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.domain.SearchException;
import com.tcvcog.tcvce.entities.CECase;
import com.tcvcog.tcvce.entities.CodeSet;
import com.tcvcog.tcvce.entities.County;
import com.tcvcog.tcvce.entities.MuniProfile;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.MunicipalityDataHeavy;
import com.tcvcog.tcvce.entities.RoleType;
import com.tcvcog.tcvce.entities.UserAuthorized;
import com.tcvcog.tcvce.entities.UserMuniAuthPeriod;
import com.tcvcog.tcvce.entities.occupancy.OccPeriod;
import com.tcvcog.tcvce.integration.CourtEntityIntegrator;
import com.tcvcog.tcvce.integration.MunicipalityIntegrator;
import com.tcvcog.tcvce.integration.OccupancyIntegrator;
import com.tcvcog.tcvce.util.Constants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.tcvcog.tcvce.application.interfaces.IFaceEventRuleGoverned;

/**
 * Business logic container for all things municipality
 * @author ellen bascomb
 */
public class MunicipalityCoordinator extends BackingBeanUtils implements Serializable {

    public final int DEFAULT_DEADLINE_BUFFER_DAYS = 10;
    public final int DEFAULT_VIOLATION_NO_LETTER_BUFFER_DAYS = 3;
    public final boolean PRIORITIZE_LETTER_BUFFER = false;
    
    public final RoleType EDIT_CROSS_MUNI_SOURCE_FLOOR = RoleType.SysAdmin;
    public final RoleType MUNI_USER_LIST_ROLE_FLOOR = RoleType.MuniStaff;
    
    /**
     * Creates a new instance of MuniCoordinator
     */
    public MunicipalityCoordinator() {
    }
    
    
    /**
     * permissions checkpoint for muni edits--sys admin required
     * @param ua
     * @return 
     */
      public boolean permissionsCheckpointEditMuni(UserAuthorized ua) {
        if (ua == null) {
            return false;
        }
        
        if (ua.getKeyCard().isHasSysAdminPermissions()) {
            return true;
        }
        
        return false;
    }

    public MunicipalityDataHeavy assembleMuniDataHeavy(Municipality muni, UserAuthorized ua) 
            throws IntegrationException, AuthorizationException, BObStatusException, EventException, BlobException {
        
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        MunicipalityDataHeavy mdh = mi.getMunDataHeavy(muni.getMuniCode());
        return configureMuniDataHeavy(mdh, ua);
    }

    /**
     * Sets various variables and lists on a data heavy muni.
     * I also set the user authorized muni profile if it's not set
     * @param mdh
     * @param ua
     * @return
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws AuthorizationException
     * @throws EventException 
     */
    private MunicipalityDataHeavy configureMuniDataHeavy(MunicipalityDataHeavy mdh, UserAuthorized ua) 
            throws IntegrationException, BObStatusException, AuthorizationException, EventException, BlobException {
        if(mdh != null && ua != null){
            PropertyCoordinator pc = getPropertyCoordinator();
            CourtEntityIntegrator cei = getCourtEntityIntegrator();
            UserCoordinator uc = getUserCoordinator();
            BlobCoordinator bc = getBlobCoordinator();
            CodeCoordinator cc =getCodeCoordinator();
            MunicipalityIntegrator mi = getMunicipalityIntegrator();
            // FIX THIS WHEN WE HAVE STABLE AUTHORIZATION PROCEDURES
//            mdh.setUserList(uc.extractUsersFromUserAuthorized(uc.getUserAuthorizedListForConfig(mdh)));
            try {
                if(mdh.getMuniProfileID() != 0){
                    mdh.setProfile(getMuniProfile(mdh.getMuniProfileID()));
                }
                
                // ECD is suspect of this call: We shouldn't be configuring a user in the muni config method
                // inject the current muniprofile into our user authorized for all things permissions
                if(ua.getGoverningMuniProfile() == null){
                    ua.setGoverningMuniProfile(mdh.getProfile());
                }
                if(mdh.getDefaultCodeSetID() != 0){
                    mdh.setCodeSet(cc.getCodeSet(mdh.getDefaultCodeSetID()));
                } else {
                    throw new BObStatusException("Muni has a default code set of ID = 0, which is not allowed; Please set a legitimate code set ID");
                }
                mdh.setIssuingCodeSourceList(mi.getCodeSourcesByMuni(mdh));
                mdh.setMuniPropertyDH(pc.assemblePropertyDataHeavy(pc.getProperty(mdh.getMuniOfficePropertyId()), ua));
                mdh.setCourtEntities(cei.getCourtEntityList(mdh.getMuniCode())); 
                mdh.setSwornOfficerList(uc.user_assembleUserListOfficerRequired(mdh, false));
                mdh.setUserListMuniActiveStaffAndUp(uc.user_assembleUserListMuniActiveRankFloored(mdh, MUNI_USER_LIST_ROLE_FLOOR));
                mdh.setZipList(pc.getZipListByMunicipality(mdh));
                mdh.setBlobList(bc.getBlobLightList(mdh));
                mdh.setCountyList(mi.getCountyList(mdh));
//                mdh.setHumanLinkList(persc.getHumanLinkList(mdh));
                
                //added by wwalk 4.6.23
                String mapThumbFileName = mdh.getMuniName().replace(" ", "_");
                mdh.setMapThumbFilePath(mapThumbFileName);
                //end wwalk add 4.6.23
                
            } catch (SearchException ex) {
                System.out.println(ex);
                System.out.println("MunicipalityCoordinator.configureMuniDataHeavy |SEARCH EXCEPTION | municode: " + mdh.getMuniCode());
            }

            mdh.setPropertyCount(pc.computeTotalProperties(mdh.getMuniCode()));
        }

        return mdh;

    }
    
    /**
     * Creates a skeleton object of a muni data heavy
     * @return the skeleton
     */
    public MunicipalityDataHeavy createMunicipalityDataHeavySkeleton(){
        return new MunicipalityDataHeavy();
    }

    /**
     * Retrieves a single municipality from the database
     * @param muniCode
     * @return
     * @throws IntegrationException 
     */
    public Municipality getMuni(int muniCode) throws IntegrationException, BObStatusException {
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        if(muniCode == 0){
            throw new BObStatusException("MunicipalityCoordinator.getMuni | cannot get municode == 0");
        }
        return mi.getMuni(muniCode);
    }
    
    /**
     * Convenience method for extracting the given muni's default occ period.Used during searching for field-initiated field inspections from the handheld app
     * @param muniCode
     * @return non zero if the given muni exists and has a non-zero default occ period ID
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public int getMuniDefaultOccPeriodID(int muniCode) throws IntegrationException, BObStatusException{
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        
        return mi.getMuniDefaultOccPeriodID(muniCode);
        
    }
    
    /**
     * Retrieves a County from the DB
     * @param countyID
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public County getCounty(int countyID) throws BObStatusException, IntegrationException{
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        return mi.getCounty(countyID);
        
    }
    
    
    /**
     * Returns all counties
     * @param includeInactive
     * @return a list, perhaps with counties
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public List<County> getCountyListComplete(boolean includeInactive) throws IntegrationException, BObStatusException{
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        return mi.getCountyListComplete(includeInactive); 
        
    }
    
    /**
     * Inserts a county into the DB
     * @param cnty
     * @return
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public int insertCounty(County cnty) throws IntegrationException, BObStatusException{
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        return mi.insertCounty(cnty);
    }
    
    /**
     * Updates a county record in the DB, including deactivating by setting active to false
     * @param cnty
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public void updateCounty(County cnty) throws BObStatusException, IntegrationException{
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        mi.updateCounty(cnty);
        getSystemMuniCacheManager().getCacheCounty().invalidate(cnty.getCacheKey());
        getPropertyCacheManager().getCacheParcel().invalidateAll();
        
    }
    
    /**
     * A hacky way to get a default occ period
     * @param ua
     * @return
     * @throws IntegrationException
     * @throws AuthorizationException 
     */
    public OccPeriod selectDefaultMuniOccPeriod(UserAuthorized ua) throws IntegrationException, AuthorizationException, BlobException {
        OccupancyCoordinator oc = getOccupancyCoordinator();
        MunicipalityDataHeavy mdh = null;
        if (ua != null) {
            try {
                mdh = assembleMuniDataHeavy(ua.getMyCredential().getGoverningAuthPeriod().getMuni(), ua);
            } catch (BObStatusException | EventException | AuthorizationException | BlobException ex) {
                System.out.println(ex);
            }
            if (mdh != null && mdh.getDefaultOccPeriodID() != 0) {
                return oc.getOccPeriod(mdh.getDefaultOccPeriodID(), ua);
            }
        }
        return null;
    }
    
    /**
     * Selects an event rule goverend object
     * @param mdh
     * @return 
     */
    public IFaceEventRuleGoverned determineERG(MunicipalityDataHeavy mdh){
        CaseCoordinator cc = getCaseCoordinator();
        // first, choose a property info case if we have one
        List<CECase> cecaseList = new ArrayList<>();
        cecaseList = cc.cecase_upcastCECaseDataHeavyList(mdh.getMuniPropertyDH().getCeCaseList());
        if(cecaseList != null && !cecaseList.isEmpty()){
            return (IFaceEventRuleGoverned) cecaseList.get(0);
            
        }
        return null;
    }

    /**
     * !!SECURITY CRITICAL!! Implements logic to generate the authorized list of
     * Municipalities to which a given Administrator User can assign a new User
     * in the system.
     *
     * @param user
     * @return
     * @throws IntegrationException
     */
    public List<Municipality> getPermittedMunicipalityListForAdminMuniAssignment(UserAuthorized user) throws IntegrationException, BObStatusException {
        List<Municipality> muniList = new ArrayList<>();
        if (user.getRole() != null && user.getRole() == RoleType.SysAdmin) {
            muniList.addAll(getMuniList(false));
       }
        return muniList;
    }
    
    /**
     * Returns municipalities which have enabled CEAR submission
     * @return only munis accepting public CEARs
     */
    public List<Municipality> getMuniListAllowCEAR() throws IntegrationException{
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        return mi.getMuniListAllowCEAR();
    }
    
    /**
     * Extracts a complete muni list
     * @param includeInactive
     * @return
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public List<Municipality> getMuniList(boolean includeInactive) throws IntegrationException, BObStatusException{
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        List<Integer> muniIDList = mi.getMuniList(!includeInactive);
        List<Municipality> muniList = new ArrayList<>();
        if(muniIDList != null && !muniIDList.isEmpty()){
            for(Integer i: muniIDList){
                muniList.add(getMuni(i));
            }
        }
        return muniList;
    }
    
    /**
     * Assembles a list of municiapalities in which the given user has code officer
     * status
     * 
     * @param ua for searching
     * @return the munis where the given officer is CEO
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public List<Municipality> assembleMuniListUserIsCodeOfficer(UserAuthorized ua) throws BObStatusException, IntegrationException{
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        
        if(ua == null){
            throw new BObStatusException("Cannot assemble muni list for null user");
        }
        List<Municipality> muniList = new ArrayList<>();
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            muniList.addAll(getMuniList(false));
        } else {
            // not system admin
            muniList = mi.assembleMuniListByOfficerUser(ua);
        }
        
        return muniList;
    }
    

    /**
     * Builds a list of all system munis based on active status
     * 
     * @param requireActive the value of includeInactive
     * @param ua requesting the list of munis
     * 
     * @throws IntegrationException
     * @throws AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.EventException 
     * @throws com.tcvcog.tcvce.domain.BlobException 
     * 
     * @return the com.tcvcog.tcvce.entities.MunicipalityDataHeavy 
     */
    public List<MunicipalityDataHeavy> getMuniDataHeavyList(boolean requireActive, UserAuthorized ua) 
            throws IntegrationException, AuthorizationException, BObStatusException, EventException, EventException, BlobException {
        
        List<MunicipalityDataHeavy> mdhlist = new ArrayList<>();
        List<Municipality> muniLightList = getMuniList(requireActive);
        if(muniLightList != null && !muniLightList.isEmpty()){
            for(Municipality muni: muniLightList){
                mdhlist.add(assembleMuniDataHeavy(muni, ua));
            }
        }
        return mdhlist;
    }
    
    /**
     * Writes a new municipality to the system and I'll have to set some random 
     * defaults to make sure the muni doesn't have emtpy objects for code book, etc.
     * 
     * @param muni
     * @param umap
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void insertMuni(MunicipalityDataHeavy muni, UserMuniAuthPeriod umap) throws IntegrationException, AuthorizationException, BObStatusException{
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        CodeCoordinator cc = getCodeCoordinator();
        if(muni == null || umap == null || muni.getMuniCode() == 0){
            throw new BObStatusException("Cannot insert muni with null muni or UMAP or muni with code 0");
        }
        if(umap.getRole() != RoleType.SysAdmin){
            throw new AuthorizationException("UMAP must be rank: Sys Admin to insert muni");
        }    
        
        CodeSet defaultCodeSet = cc.getCodeSet(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                                .getString("default_codeset_for_muni_insert")));
        MuniProfile defaultMuniProfile = getMuniProfile(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                                .getString("default_muniprofile_for_muni_insert")));
        muni.setCodeSet(defaultCodeSet);
        muni.setProfile(defaultMuniProfile);
        
        muni.setCreatedby_UMAP(umap);
        muni.setLastUpdatedby_UMAP(umap);
        
        mi.insertMuniDataHeavy(muni);
        
        // don't do these yet--wait for updates
//        mi.linkMuniToCounties(muni, muni.getCountyList());
//        mi.linkMuniToCourtEntities(muni);
//        mi.linkMuniToZips(muni);
    }
    
    /**
     * Updates fields on the Municipality object
     * @param muni
     * @param ua
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void updateMuniInfo(MunicipalityDataHeavy muni, UserAuthorized ua) throws IntegrationException, AuthorizationException, BObStatusException{
        if(muni == null || ua == null){
            throw new BObStatusException("Cannot update muni with null muni or UMAP");
        }
        if(!permissionsCheckpointEditMuni(ua)){
            throw new AuthorizationException("UMAP must be rank: Sys Admin to update muni");
        }    
        
        // some logic checking here perhaps?
        muni.setLastUpdatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
        
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        
        mi.updateMuniDataHeavyInfo(muni);
        
        getSystemMuniCacheManager().flush(muni);
    }
    
    
    /**
     * Logic for updates to muni 
     * @param muni
     * @param ua
     * @throws AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void updateMuniObjectLinks(MunicipalityDataHeavy muni, UserAuthorized ua) throws AuthorizationException, IntegrationException, BObStatusException{
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        
        if(!permissionsCheckpointEditMuni(ua)){
            throw new AuthorizationException("UMAP must be rank: Sys Admin to update muni");
        }    
        
         muni.setLastUpdatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
         mi.updateMuniObjectLinks(muni);
        // now deal with county links, court links, and zip links by first wiping, then writing all links
        mi.clearAllMuniCountyLinks(muni);
        if(muni.getCountyList() != null && !muni.getCountyList().isEmpty()){
            mi.linkMuniToCounties(muni, muni.getCountyList()); 
        }
        mi.clearAllMuniCourtLinks(muni);
        if(muni.getCourtEntities() != null && !muni.getCourtEntities().isEmpty()){
            mi.linkMuniToCourtEntities(muni);
        }
        mi.clearAllMuniCodeSourceLinks(muni);
        if(muni.getIssuingCodeSourceList() != null && !muni.getIssuingCodeSourceList().isEmpty()){
            mi.linkMuniToSource(muni);
        }
        // no caching to clear I reckon
        
    }
    
    
    /**
     * Logic for updates to muni modules
     * @param mdh
     * @param ua
     * @throws AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void updateMuniModules(MunicipalityDataHeavy mdh, UserAuthorized ua) throws AuthorizationException, IntegrationException{
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        
        if(!permissionsCheckpointEditMuni(ua)){
            throw new AuthorizationException("UMAP must be rank: Sys Admin to update muni");
        }    
        mdh.setLastUpdatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
        mi.updateMuniModules(mdh);
        
    }
    
    
    /**
     * Logic for updates to muni 
     * @param mdh
     * @param ua
     * @throws AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void updateMuniPermissions(MunicipalityDataHeavy mdh, UserAuthorized ua) throws AuthorizationException, IntegrationException{
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        
        if(!permissionsCheckpointEditMuni(ua)){
            throw new AuthorizationException("UMAP must be rank: Sys Admin to update muni");
        }    
        mdh.setLastUpdatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
        mi.updateMuniPermissions(mdh);
        
        
    }
    
    
    /**
     * Logic for updates to muni 
     * @param mdh
     * @param ua
     * @throws AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void updateMuniPrinting(MunicipalityDataHeavy mdh, UserAuthorized ua) throws AuthorizationException, IntegrationException{
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        
        if(!permissionsCheckpointEditMuni(ua)){
            throw new AuthorizationException("UMAP must be rank: Sys Admin to update muni");
        }    
        mdh.setLastUpdatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
        mi.updateMuniPrinting(mdh);
        
    }
    
    
    
    /**
     * Update a muni's zip links.This process will wipe all old ZIPs and
     * write the new links
     * 
     * @param mdh 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void updateMuniZIPLinks(MunicipalityDataHeavy mdh) throws IntegrationException, BObStatusException{
        if(mdh == null){
            throw new IntegrationException("Cannot update zip links with null input");
        }
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        mi.clearAllMuniZipLinks(mdh);
        mi.linkMuniToZips(mdh);
        
    }
    
    
    /**
     * Updates fields on the Municipality object
     * @param muni
     * @param umap
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void deactivateMuni(MunicipalityDataHeavy muni, UserMuniAuthPeriod umap) throws IntegrationException, AuthorizationException, BObStatusException{
        if(muni == null || umap == null){
            throw new BObStatusException("Cannot deac muni with null muni or UMAP");
        }
        if(umap.getRole() != RoleType.SysAdmin){
            throw new AuthorizationException("UMAP must be rank: Sys Admin to deac muni");
        }    
        
        muni.setLastUpdatedby_UMAP(umap);
        muni.setDeactivatedBy_UMAP(umap);
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        mi.deactivateMuni(muni);
        // no go an deac all the UMAPs associated with this muni
        // TODO: write logic on user coordinator
        
        getSystemMuniCacheManager().getCacheMuni().invalidate(muni.getCacheKey());
        
    }
    
    
    /**
     * Writes a new muniProfilecipality to the system
     * @param muniProfile
     * @param umap
     * @return fresh from DB with id
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public MuniProfile insertMuniProfile(MuniProfile muniProfile, UserMuniAuthPeriod umap) throws IntegrationException, AuthorizationException, BObStatusException{
        if(muniProfile == null || umap == null){
            throw new BObStatusException("Cannot insert muniProfile with null muniProfile or UMAP");
        }
        if(umap.getRole() != RoleType.SysAdmin){
            throw new AuthorizationException("UMAP must be rank: Sys Admin to insert muniProfile");
        }    
      
        muniProfile.setCreatedby_UMAP(umap);
        muniProfile.setLastUpdatedby_UMAP(umap);
//        we have to use the original object since it holds the permit types
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        muniProfile.setProfileID(mi.insertMuniProfile(auditMuniProfileLogic(muniProfile)));
        
        mi.linkMuniProfileToOccPeriodTypes(muniProfile);
//        Return the new object from the fresh ID
        muniProfile = getMuniProfile(muniProfile.getProfileID());
        
        return muniProfile;
    }
    
  
    
    /**
     * Updates fields on the Municipality object
     * @param muniProfile
     * @param umap
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void updateMuniProfile(MuniProfile muniProfile, UserMuniAuthPeriod umap) throws IntegrationException, AuthorizationException, BObStatusException{
        if(muniProfile == null || umap == null){
            throw new BObStatusException("Cannot update muniProfile with null muniProfile or UMAP");
        }
        if(umap.getRole() != RoleType.SysAdmin){
            throw new AuthorizationException("UMAP must be rank: Sys Admin to update muniProfile");
        }    
        // some logic checking here perhaps?
        muniProfile.setLastUpdatedby_UMAP(umap);
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        mi.updateMuniProfile(auditMuniProfileLogic(muniProfile));
        
        // clear and write new profile-permit type links
        mi.clearAllMuniProfileOccPermitTypeLinks(muniProfile);
        mi.linkMuniProfileToOccPeriodTypes(muniProfile);
        
        getSystemMuniCacheManager().flush(muniProfile);
        
    }
    
    /**
     * Logic check for municipality profiles which enforces the hierarchical
     * ordering of code officer and manager rank permissions directives,
     * which is to say if a profile specifies manager rank required for operation X, 
     * that profile must also require code officer permissions for operation X.
     * 
     * This method will adjust ceo permissions flags in cases in which a manger 
     * rank is required but the corresponding CEO permissions flag is not set 
     * 
     * @param profile
     * @return 
     */
    private MuniProfile auditMuniProfileLogic(MuniProfile profile) throws BObStatusException{
        if(profile == null){
            throw new BObStatusException("Cannot audit a null permissions profile ");
        }
        // These checks turn ON the CEO required flags if the incoming profile has the manager 
        // directive ON but NOT the corresponding s
        // Inspection finalization hierarchy verification
        if(profile.isManagerRequiredInspectionFinalize() && !profile.isRequireCodeOfficerTrueFinalizeInspection()){
            profile.setRequireCodeOfficerTrueFinalizeInspection(true);
        }
        // NOV finlize hierarchy verification
        if(profile.isManagerRequiredNOVFinalize() && !profile.isRequireCodeOfficerTrueFinalizeNOV()){
            profile.setRequireCodeOfficerTrueFinalizeNOV(true);
        }
        // Citation open/close hierarchy verification        
        if(profile.isManagerRequiredCitationOpenClose() && !profile.isRequireCodeOfficerTrueOpenCitation()){
            profile.setRequireCodeOfficerTrueOpenCitation(true);
        }
        // CECase closure hierarchy verification
        if(profile.isManagerRequiredcCloseCECase() && !profile.isRequireCodeOfficerTrueCloseCECase()){
            profile.setRequireCodeOfficerTrueCloseCECase(true);
        }
        // Permitting hierarchy verification
        if(profile.isManagerRequiredPermitFinalize() && !profile.isRequireCodeOfficerTrueIssuePermits()){
            profile.setRequireCodeOfficerTrueIssuePermits(true);
        }
        
        
        return profile;
    }
    
    
    /**
     * Updates fields on the Municipality object
     * @param muniProfile
     * @param umap
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void deactivateMuniProfile(MuniProfile muniProfile, UserMuniAuthPeriod umap) throws IntegrationException, AuthorizationException, BObStatusException{
        if(muniProfile == null || umap == null){
            throw new BObStatusException("Cannot deac muniProfile with null muniProfile or UMAP");
        }
        if(umap.getRole() != RoleType.SysAdmin){
            throw new AuthorizationException("UMAP must be rank: Sys Admin to deac muniProfile");
        }    
        // some logic checking here perhaps?
        muniProfile.setLastUpdatedby_UMAP(umap);
        muniProfile.setDeactivatedBy_UMAP(umap);
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        mi.deactivateMuniProfile(muniProfile);
        
        getSystemMuniCacheManager().getCacheMuniProfile().invalidate(muniProfile.getCacheKey());
        getSystemMuniCacheManager().getCacheUMAP().invalidateAll();
    }
    
    /**
     * Factory for muni profile objects
     * @param umap of the requesting user
     * @return the new Profile with id = 0
     */
    public MuniProfile getMuniProfileSkeleton(UserMuniAuthPeriod umap){
        MuniProfile mp = new MuniProfile();
        mp.setCreatedby_UMAP(umap);
        mp.setLastUpdatedby_UMAP(umap);
        return mp;
    }
    
    
    /**
     * Logic pass through for muni profiles. Set's default values where needed.
     * @param profileID
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public MuniProfile getMuniProfile(int profileID) throws IntegrationException, BObStatusException{
        if(profileID == 0){
            throw new BObStatusException("cannot fetch a profile of ID 0");
        }
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        MuniProfile mp = mi.getMuniProfile(profileID);
        
        OccupancyIntegrator oi = getOccupancyIntegrator();
        
        // not working yet
//        mp.setEventRuleSetCE(wi.rules_getEventRuleSet(mp.getProfileID()));
        mp.setOccPermitTypeList(oi.getOccPermitTypeListByMuniProfileID(mp.getProfileID()));
        
        if(mp.getOccPermitTypeList() == null){
            mp.setOccPermitTypeList(new ArrayList<>());
        }
        
        // for reverse compatibility on profiles already in the system before logic was solidified
        return auditMuniProfileLogic(mp);
        
    }
    
    /**
     * Extracts all municipality profiles from DB
     * @param includeInactive
     * @return
     * @throws IntegrationException 
     */
    public List<MuniProfile> getMuniProfilesList(boolean includeInactive) throws IntegrationException{
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        List<Integer> profileIDList = null;
        List<MuniProfile> profileList = new ArrayList<>();
        try{
            profileIDList = mi.getMuniProfileList(includeInactive);
            if(profileIDList != null && !profileIDList.isEmpty()){
                for(Integer i: profileIDList){
                    profileList.add(getMuniProfile(i));
                }
            }
        } catch (BObStatusException | IntegrationException ex) {
            throw new IntegrationException(ex.getMessage());
        }
        return profileList;
    }
    
  

}
