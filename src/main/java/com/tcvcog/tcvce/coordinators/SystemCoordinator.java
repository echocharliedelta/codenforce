/*
 * Copyright (C) 2017 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.coordinators;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.application.IFace_pinnable;
import com.tcvcog.tcvce.application.interfaces.IFaceCachable;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheClient;
import com.tcvcog.tcvce.application.interfaces.IFaceCacheManager;
import com.tcvcog.tcvce.application.interfaces.IFace_Loggable;
import com.tcvcog.tcvce.application.interfaces.IFace_updateAuditable;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.entities.Manageable;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.entities.Credential;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.*;
import com.tcvcog.tcvce.entities.BOBSource;
import com.tcvcog.tcvce.entities.Icon;
import com.tcvcog.tcvce.entities.IntensityClass;
import com.tcvcog.tcvce.entities.IntensitySchema;
import com.tcvcog.tcvce.entities.NavigationItem;
import com.tcvcog.tcvce.entities.NavigationSubItem;
import com.tcvcog.tcvce.entities.PrintStyle;
import com.tcvcog.tcvce.entities.PropertyUseType;
import com.tcvcog.tcvce.entities.User;
import com.tcvcog.tcvce.entities.UserAuthorized;
import com.tcvcog.tcvce.integration.CaseIntegrator;
import com.tcvcog.tcvce.integration.LogIntegrator;
import com.tcvcog.tcvce.integration.MunicipalityIntegrator;
import com.tcvcog.tcvce.integration.PersonCacheManager;
import com.tcvcog.tcvce.integration.PropertyCacheManager;
import com.tcvcog.tcvce.integration.SystemIntegrator;
import com.tcvcog.tcvce.integration.SystemMuniUserCacheManager;
import com.tcvcog.tcvce.util.Constants;
import com.tcvcog.tcvce.util.DateTimeUtil;
import com.tcvcog.tcvce.util.LogEntry;
import com.tcvcog.tcvce.util.MessageBuilderParams;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Named;
import jakarta.servlet.jsp.jstl.tlv.PermittedTaglibsTLV;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;


/**
 * Logic holder for all sorts of misc cross-object
 * and system objects
 * 
 * @author ellen bascomb of apt 31y
 */
@Named("systemCoordinator") 
@ApplicationScoped
public class SystemCoordinator extends BackingBeanUtils implements Serializable {

    private final RoleType MIN_RANK_TO_APPEND_NOTES = RoleType.MuniReader;
    private Map<Integer, String> muniCodeNameMap;
    
    /**
     * Holds the system-wide list of all object family CacheManagers
     * and this list will be iteratvely by admin user facing controls
     */
    private List<IFaceCacheManager> cacheManagers;
    static boolean initialCachingState = true;
    private final String CNF_STATE = "BETA TESTING";

    /**
     * Creates a new instance of LoggingCoordinator
     */
    public SystemCoordinator() {
    }

    @PostConstruct
    public void initBean() {
        System.out.println("SystemCoordinator.initBean");
        initCachingSubsystem();
    }
    
    /**
     * A hacky way to get SystemCoordinator initiated before login
     * @return the CNF_STATE
     */
    public String getCNF_STATE() {
        return CNF_STATE;
    }
    
    /**
     * Sets up all our caches
     */
    private void initCachingSubsystem(){
        cacheManagers = new ArrayList<>();
        // build our cacheManager list manually
        cacheManagers.add(getSystemMuniCacheManager());
        cacheManagers.add(getCodeCacheManager());
        cacheManagers.add(getPropertyCacheManager());
        
        cacheManagers.add(getPersonCacheManager());
        cacheManagers.add(getCECaseCacheManager());
        cacheManagers.add(getOccupancyCacheManager());
        
        cacheManagers.add(getOccInspectionCacheManager());
        cacheManagers.add(getEventCacheManager());
        cacheManagers.add(getBlobCacheManager());
        
        
        if(initialCachingState){
            System.out.println("SystemCoordinator.initBean | enabling all caching");
            enableAllCaching();
            // until we sort out handheld caching
            // fin caching turned back on Jan 2025 for testing
//            if(getOccInspectionCacheManager() != null){
//                    getOccInspectionCacheManager().disableClientCaching();
//            }
        } else {
            System.out.println("SystemCoordinator.initBean | disabling all caching");
            disableAllCaching();
        }
        
    }
    
    
    /**
     * Called by implementing classes of the IFaceCacheManager such that 
     * the caches can be properly flushed by this class if requested.
     * @param manager 
     */
    public void registerCacheManager(IFaceCacheManager manager){
        if(manager != null && cacheManagers != null){
            if(!cacheManagers.contains(manager)){
                cacheManagers.add(manager);
                System.out.println("SystemCoordinator.registerCachemnager | registered:  " + manager.getClass().getName());
            } else {
                System.out.println("SystemCoordinator.registerCachemnager | already registered; not adding:  " + manager.getClass().getName());
            }
        }
    }
    
    /**
     * Primary entryway for any coordinator update methods of cachable BOBs.
     * I'll ask the cachable for its own dependency tree and flush the entire line.
     * 
     * @param cachable 
     */
    public void flushCachable(IFaceCachable cachable) throws BObStatusException{
        if(cachable == null){
            throw new BObStatusException("Cannot flush null cachable");
        }
        
    }
    
    
    
    /**
     * Dumps all cache entries system wide. For now, we'll let anybody with muni staff or better do so
     */
    public void flushAllSystemCaches() {
        /**
         * Initial implementation in June 2024 includes these CacheManager
         * implementing classes that should be registered during full
         * cache utilization.
         * 
         * COMPLETED CACHE MANAGERS
         * 
         * SystemMuniUserCacheManager // DONE
         * CodeCacheManager // DONE
         * PropertyCacheManager  // WIP - flushing facility required
         * 
         * PersonCacheManager // DONE -- Um, What if we just cache Person objects instead of all their bits individually?
         * CECaseCacheManager // DONE with only CECase and CseDH!!
         *          ** REMEMBER THAT THIS IS WEIRD --we are trying a cache of the entire data heavy object in one swoop
         * OccupancyCacheManager // DONE
         * 
         * OccInspectionCacheManager // DONE
         * EventCacheManager // DONE (just two objects: eventCnF and cats)
         * BlobCacheManager // done
         * 
         * TODO
         */
        // iterate over each manager and flush them
        if(cacheManagers != null && !cacheManagers.isEmpty()){

            for(IFaceCacheManager manager: cacheManagers){
                manager.flushAllCaches();
                System.out.println("SystemCoordinator.flushAllSystemCaches | Flushed: " + manager.getClass().getName());
            }
        }
    }
    
    /**
     * Calls disable caching on all caches
     */
    public void disableAllCaching(){
        if (cacheManagers != null && !cacheManagers.isEmpty()) {
            for (IFaceCacheManager manager : cacheManagers) {
                manager.disableClientCaching();
                System.out.println("SystemCoordinator.disableAllCaching | Disabled: " + manager.getClass().getName());
            }
        }
    }
    
    
    /**
     * Calls writeCacheStats on all managers
     */
    public void writeAllCacheStats(){
        if (cacheManagers != null && !cacheManagers.isEmpty()) {
            for (IFaceCacheManager manager : cacheManagers) {
                manager.writeCacheStats();
            }
        }
    }
    
    /**
     * Calls enable caching on all cache managers
     */
    public void enableAllCaching(){
        if (cacheManagers != null && !cacheManagers.isEmpty()) {

            for (IFaceCacheManager manager : cacheManagers) {
                manager.enableClientCaching();
                System.out.println("SystemCoordinator.enableAllCaching | enabled: " + manager.getClass().getName());
            }
        }
    }
    
    
    /**
     * Called by the CacheManagers when adding to their client list
     * @param client 
     */
    public void preRegistrationconfigureCacheClient(IFaceCacheClient client){
        if(client != null){
            
            System.out.println("SystemMuniUserCacheManager.preRegistrationconfigureCacheClient | class: " + client.getClass());
        }
    }
    
    /**
     * Logic pass through method for calls to the system integrator which will
     * make database inserts recording any exploration of one of our BObs
     *
     * @param u
     * @param ob
     * @throws IntegrationException
     */
    public void logObjectView(User u, IFace_Loggable ob) throws IntegrationException {
        SystemIntegrator si = getSystemIntegrator();
        System.out.println("SystemCoordinator.logObjectView | Disabled until after launch 2022");
        //  si.logObjectView(u, ob);

    }

    /**
     * Central access point for writing all LogEntry objects to the DB
     *
     * @param entry
     * @return
     */
    public int makeLogEntry(LogEntry entry) {
        LogIntegrator li = getLogIntegrator();
        return li.writeLogEntry(entry);
    }

    
    /**
     * Toggles the pinned status of a given object
     * @param pinnable 
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.IntegrationException if either param == null
     */
    public void togglePinning(IFace_pinnable pinnable, UserAuthorized ua) throws IntegrationException{
        if(pinnable == null || ua == null){
            throw new IntegrationException("SystemCoordinator.togglePinning | Cannot toggle pinned status of a null pinnable or user");
        }
        SystemIntegrator si = getSystemIntegrator();
        
        if(pinnable.isPinned()){
            si.unPinObject(pinnable);
        } else {
            si.pinObject(pinnable);
        }
    }
    
    
      /**
    * Removes standalone &lt;p&gt;&lt;/p&gt; tags from rich text editor output.
    * Got help from custom Chat GPT
    * 
    * @param rt
    * @return 
    */
    public String cleanRichText(String rt){
        
        // old regexp from CHAT GPT
//        String regex = "^\\s*<p>(.*?)</p>\\s*$";
        
        if(rt == null) {
            return rt;
        }
         if (rt.trim().isEmpty()) {
            return null;  // Set to null if content is empty
        }
        Document doc = Jsoup.parseBodyFragment(rt);
        Element body = doc.body();

        // Check if there is exactly one <p> element and it wraps the entire content
        if (body.children().size() == 1 && body.child(0).tagName().equals("p")) {
            return body.child(0).html(); // Returns the inner HTML of the single <p> element
        }
        return rt; // Return the original input if it's not a single paragraph

    }
    
    /**
     * Generates a rich-text (contains HTML to be NOT escaped) given various
     * input values
     *
     * @param objectID
     * @param BObName
     * @param formerVal
     * @param updatedVal
     * @return
     */
    public String generateFieldUpdateNoteBody(int objectID,
            String BObName,
            String formerVal,
            String updatedVal) {
        StringBuilder sb = new StringBuilder();
        sb.append(Constants.FMT_HTML_BREAK);
        sb.append("FIELD UPDATE");
        sb.append(" of ");
        sb.append(BObName);
        sb.append(" ID: ");
        sb.append(objectID);
        sb.append(Constants.FMT_HTML_BREAK);
        sb.append("Prev val: ");
        sb.append(formerVal);
        sb.append(Constants.FMT_HTML_BREAK);
        sb.append("New val: ");
        sb.append(updatedVal);
        sb.append(Constants.FMT_HTML_BREAK);
        return sb.toString();

    }

    /**
     * Skeleton of a system that may be needed to generate and release carefully
     * some level of "internal guest" level access Credential
     *
     * @param ua
     */
    protected void requestBaseInternalAccessCredential(UserAuthorized ua) {
        // TODO: Finish guts
    }
    
    /**
     * Iterates through all values in LinkedObjectSchemaEnum and returns
     * only those that are of the inputted family (e.g. human links or address links)
     * @param fam not null
     * @return a list, perhaps with instances of LinkedObjectFamilyEnum
     * @throws BObStatusException for null input
     */
    public List<LinkedObjectSchemaEnum> assembleLinkedObjectSchemaEnumListByFamily(LinkedObjectFamilyEnum fam) throws BObStatusException{
        if(fam == null){
            throw new BObStatusException("Cannot assemble linked object schema list with null family");
        }
        
        List<LinkedObjectSchemaEnum> rawSchemaList = new ArrayList<>();
        List<LinkedObjectSchemaEnum> chosenSchemas = new ArrayList<>();
        rawSchemaList.addAll(Arrays.asList(LinkedObjectSchemaEnum.values()));
        if(!rawSchemaList.isEmpty()){
            for(LinkedObjectSchemaEnum lose: rawSchemaList){
                if(lose.getLinkedObjectFamilyEnum() == fam){
                    chosenSchemas.add(lose);
                }
            }
        }
        return chosenSchemas;
    }
    
    
    
    /**
     * Coordinates the creation of all LinkedObjectRoles given a link schema enum instance
     * @param lose which set of roles you want
     * @return a list, possibly containing LinkedObjectRoles ; never null
     */
    public List<LinkedObjectRole> assembleLinkedObjectRolesBySchema(LinkedObjectSchemaEnum lose) throws IntegrationException, BObStatusException{
        List<LinkedObjectRole> roleList = new ArrayList<>();
        SystemIntegrator si = getSystemIntegrator();
        
        List<Integer> idl = si.getLinkedObjectRoleListBySchemaFamily(lose);
        if(idl != null && !idl.isEmpty()){
            for(Integer i: idl){
                roleList.add(si.getLinkedObjectRole(i));
            }
        }
        return roleList;
    }

    /**
     * The official note appending tool of the entire codeNforce system!
     * Consider all other appendNoteXXX methods scattered about to be rogue
     * agents, operating without warrant, independent of any meaningful
     * standards or oversight
     *
     * @param mbp containing as much information as possible which will be
     * formatted into a nice note block
     * @return the exact text of the new note in HTML, with any previous text
     * included in the message builder params object post-pended to the incoming
     * note
     */
    public String appendNoteBlock(MessageBuilderParams mbp) {
        StringBuilder sb = new StringBuilder();
        if (mbp == null) {
            return sb.toString();
        }

        sb.append(Constants.FMT_HTML_BREAK);
        sb.append(Constants.FMT_NOTE_START);

        // TITLE
        if (mbp.getHeader() != null) {
            sb.append(Constants.FMT_HTML_BREAK);
            sb.append(mbp.getHeader());
        }

        if (mbp.getExplanation() != null) {
            sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
            sb.append(mbp.getExplanation());
        }
        
        // NOTE content
         if (mbp.getNewMessageContent() != null) {
            sb.append(Constants.FMT_HTML_BREAK);
            // don't prepend the characters "Content: "!!!
//            sb.append(Constants.FMT_CONTENT);
            sb.append(mbp.getNewMessageContent());
        }
         
        // CREATOR INFO
        sb.append(Constants.FMT_HTML_BREAK);
        sb.append(Constants.FMT_NOTEBYLINE);
        if (mbp.getUser() != null) {

            if (mbp.getUser().getHuman() != null) {
                sb.append(mbp.getUser().getHuman().getName());
                sb.append(Constants.FMT_SPACE_LITERAL);
            }
            sb.append(Constants.FMT_HTML_BREAK);
            sb.append(Constants.FMT_USER);
            sb.append(mbp.getUser().getUsername());
            sb.append(Constants.FMT_DTYPE_OBJECTID_INLINEOPEN);
            sb.append(Constants.FMT_ID);
            sb.append(mbp.getUser().getUserID());
            sb.append(Constants.FMT_DTYPE_OBJECTID_INLINECLOSED);
        }
        sb.append(Constants.FMT_AT);
        sb.append(stampCurrentTimeForNote());

        if (mbp.getCred() != null && mbp.isIncludeCredentialSig()) {
            sb.append(Constants.FMT_SIGNATURELEAD);
            sb.append(mbp.getCred().getSignature());
        }

        sb.append(Constants.FMT_HTML_BREAK);
        sb.append(Constants.FMT_NOTE_END);
        sb.append(Constants.FMT_HTML_BREAK);

        if (mbp.getExistingContent() != null) {
            sb.append(mbp.getExistingContent());
        }
        return sb.toString();
    }
    
    public String universalAppendNoteBlock(MessageBuilderParams mbp) {
        // Create an instance of the Date class
        Date currentDate = new Date();

        // Define the desired date format
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE dd MMM yyyy");

        StringBuilder sb = new StringBuilder();
        if (Objects.isNull(mbp))
        {
            return sb.toString();
        }

        sb.append(Constants.FMT_HTML_BREAK);
        sb.append(Constants.NOTE_FIRSTLINE_START);
        sb.append(dateFormat.format(currentDate));
        sb.append(Constants.NOTE_FIRSTLINE_END);

        // TITLE
        if (Objects.nonNull(mbp.getHeader()))
        {
            sb.append(Constants.FMT_HTML_BREAK);
            sb.append(mbp.getHeader());
        }

        if (Objects.nonNull(mbp.getExplanation()))
        {
            sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
            sb.append(mbp.getExplanation());
        }

        // NOTE content
        if (Objects.nonNull(mbp.getNewMessageContent()))
        {
            sb.append(Constants.FMT_HTML_BREAK);
            sb.append(Constants.FMT_HTML_BREAK);
            sb.append(mbp.getNewMessageContent());
        }
        
        // CREATOR INFO
        sb.append(Constants.FMT_HTML_BREAK);
        sb.append(Constants.NOTEBYLINE);
        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
        if (Objects.nonNull(mbp.getUser()))
        {

            if (Objects.nonNull(mbp.getUser().getHuman()))
            {
                sb.append(mbp.getUser().getHuman().getName());
                sb.append(Constants.FMT_SPACE_LITERAL);
            }
            sb.append(Constants.FMT_HTML_BREAK);
            sb.append(Constants.FMT_USER);
            sb.append(mbp.getUser().getUsername());
            sb.append(Constants.FMT_DTYPE_OBJECTID_INLINEOPEN);
            sb.append(Constants.FMT_ID);
            sb.append(mbp.getUser().getUserID());
            sb.append(Constants.FMT_DTYPE_OBJECTID_INLINECLOSED);
        }
        sb.append(Constants.FMT_AT);
        sb.append(stampCurrentTimeForNote());

        if (Objects.nonNull(mbp.getCred()) && mbp.isIncludeCredentialSig())
        {
            sb.append(Constants.FMT_SIGNATURELEAD);
            sb.append(mbp.getCred().getSignature());
        }

        sb.append(Constants.FMT_HTML_BREAK);
        sb.append(Constants.NOTE_END);
        sb.append(Constants.FMT_HTML_BREAK);

        if (Objects.nonNull(mbp.getExistingContent()))
        {
            sb.append(mbp.getExistingContent());
        }
        return sb.toString();
    }
    
    /**
     * Logic pass through for note writing using the standardized interface
     * 
     * @param nh Caller must prepare the note holder with all the correct content
     * of its note field so this method and the integrator can just yank and go
     * @param ua doing the noting
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void writeNotes(IFace_noteHolder nh, UserAuthorized ua) throws IntegrationException, BObStatusException{
        SystemIntegrator si = getSystemIntegrator();
        if(nh == null || ua == null || nh.getDBKey() == 0){
            throw new BObStatusException("Cannot append notes with null notes, user, or PK of 0");
        }
        // enforce minimum permissions
        if(ua.getKeyCard().getGoverningAuthPeriod().getRole().getRank() >= MIN_RANK_TO_APPEND_NOTES.getRank() ){
            si.writeNotes(nh);
        }
        
    }
    
    /**
     *
     * @param auditableObject
     * @return string to append
     */
    public String getFieldDump(IFace_updateAuditable<?> auditableObject) {
        if(auditableObject != null){
            
            Class<?> fieldEnumClass = auditableObject.getFieldDumpEnum();
            Field[] fields = fieldEnumClass.getDeclaredFields();
            StringBuilder sb = new StringBuilder();
            for (Field field : fields)
            {
                if (field.isEnumConstant())
                {
                    String fieldName = field.getName();
                    String friendlyName = getFieldFriendlyName(field);
                    Object fieldValue = getFieldValue(auditableObject, fieldName);
                    sb.append(friendlyName)
                            .append(": ")
                            .append(formatFieldValue(fieldValue))
                            .append(Constants.FMT_HTML_BREAK);
                }
            }
            return sb.toString();
        }
        else {
            return "";
        }
    }

    /**
     * Return the fields friendly name
     */
    private String getFieldFriendlyName(Field field) {
        try
        {
            Class<?> enumClass = field.getType();
            if (enumClass.isEnum())
            {
                Enum<?> enumConstant = (Enum<?>) field.get(null);
                Method getFriendlyNameMethod = enumClass.getMethod("getFriendlyName");
                Object friendlyName = getFriendlyNameMethod.invoke(enumConstant);
                return friendlyName.toString();
            }
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e)
        {
            System.out.println(e.toString());
        }
        return null;
    }

    /**
     * Return the value of field
     */
    private Object getFieldValue(IFace_updateAuditable<?> auditableObject, String fieldName) {
        try
        {
            Method[] methods = auditableObject.getClass().getMethods();
            for (Method method : methods)
            {
                if (method.getName().equalsIgnoreCase("get" + fieldName))
                {
                    Object fieldValue = method.invoke(auditableObject);
                    return fieldValue;
                }

                if (method.getName().equalsIgnoreCase("is" + fieldName))
                {
                    Object fieldValue = method.invoke(auditableObject);
                    return fieldValue;
                }

            }
            System.out.println("No getter method found for field: " + fieldName);
        } catch (IllegalAccessException | SecurityException | InvocationTargetException e)
        {
            System.out.println(e.toString());
        }
        return null;
    }

    public String formatFieldValue(Object fieldValue) {
        if (Objects.nonNull(fieldValue))
        {
            if (!fieldValue.toString().isEmpty())
            {
                String valueType = fieldValue.getClass().getSimpleName();
                switch (valueType)
                {
                    case "Boolean":
                    {
                        return fieldValue.toString().equals("true") ? "Yes" : "No";
                    }
                    case "LocalDate":
                    {
                        return getPrettyLocalDateOnlyNoTime((LocalDate) fieldValue);
                    }
                    case "LocalDateTime":
                    {
                        return getPrettyDate((LocalDateTime) fieldValue);
                    }
                }
            } else
            {
                return "none";
            }
        } else
        {
            return "none";
        }
        return fieldValue.toString();
    }
    
    /**
     * Utility for getting patch table to UI
     * @return the Patch IDs
     */
    public String getDBPatchIDList() throws IntegrationException{
        SystemIntegrator si = getSystemIntegrator();
        return si.getDatabasePatchRecord();
    }

    /**
     * Utility method for creating a string of the current date
     *
     * @return
     */
    public String stampCurrentTimeForNote() {
        return DateTimeUtil.getPrettyDate(LocalDateTime.now());
    }
    
    /**
     * Factory for intensity classes
     * @return 
     */
    public IntensityClass getIntensityClassSkeleton(){
        IntensityClass ic = new IntensityClass();
        ic.setActive(true);
        return ic;
    }
    
    /**
     * Logic block for intensity classes
     * @param classid
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public IntensityClass getIntensityClass(int classid) throws IntegrationException{
        SystemIntegrator si = getSystemIntegrator();
        if(classid != 0){
            return si.getIntensityClass(classid);
        }
        return null;
    }
    
    /**
     * Logic block for adding a new intensity class
     * @param ic
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public int addIntensityClass(IntensityClass ic) throws IntegrationException{
        SystemIntegrator si = getSystemIntegrator();
        return si.insertIntensityClass(ic);
    }
    
    
    /**
     * Logic block for updating and deactivating an intensity class
     * @param ic
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void editIntensityClass(IntensityClass ic) throws IntegrationException, BObStatusException{
        SystemIntegrator si = getSystemIntegrator();
        if(ic == null){
            throw new BObStatusException("Could not update a null intensity class");
        }
        
        si.updateIntensityClass(ic);
        
        SystemMuniUserCacheManager smucm = getSystemMuniCacheManager();
        smucm.flush(ic);
        
    }
    
    /**
     * Returns active intensity classes
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public List<IntensityClass> getIntensityClassList() throws IntegrationException{
        SystemIntegrator si = getSystemIntegrator();
         List<IntensityClass> classList = si.getIntensityClassList(null, null); 
         List<IntensityClass> classListFinal = new ArrayList<>();
        if (classList != null && !classList.isEmpty()) {
            for (IntensityClass ic : classList) {
                if (ic.isActive()) {
                    classListFinal.add(ic);
                }
            }
        }
        return classListFinal;
    }
    
    

    /**
     * Assembles an intensity schema with only active classes inside
     *
     * @param schemaName
     * @param muni
     * @return
     * @throws IntegrationException
     */
    public IntensitySchema getIntensitySchemaWithClasses(String schemaName, Municipality muni) throws IntegrationException {
        SystemIntegrator si = getSystemIntegrator();
        IntensitySchema is = new IntensitySchema(schemaName);
        List<IntensityClass> classList = si.getIntensityClassList(schemaName, muni);
        List<IntensityClass> classListFinal = new ArrayList<>();
        if (classList != null && !classList.isEmpty()) {
            for (IntensityClass ic : classList) {
                if (ic.isActive()) {
                    classListFinal.add(ic);
                }
            }
        }
        if (!classListFinal.isEmpty()) {
            Collections.sort(classListFinal);
        }
        is.setClassList(classListFinal);
        return is;
    }
    
    /**
     * Creates a random rbg color in the form
     * rgb(xxx,xxx,xxx)
     * @return 
     */
    public String generateRandomRGBColor(){
         
        Random r = new Random();
        int red = r.nextInt(256);
            int green = r.nextInt(256);
            int blue = r.nextInt(256);
            StringBuilder sb = new StringBuilder();
            sb.append("rgb(");
            sb.append(red);
            sb.append(",");
            sb.append(green);
            sb.append(",");
            sb.append(blue);
            sb.append(")");
            return sb.toString();
        
        
    }
    

    /**
     * Builds a complete list of BOb sources for drop downs
     *
     * @return
     */
    public List<BOBSource> getBobSourceListComplete() {
        List<BOBSource> sourceList = new ArrayList<>();
        SystemIntegrator si = getSystemIntegrator();
        List<Integer> idl = null;
        try {
            idl = si.getBobSourceListComplete();
        } catch (IntegrationException ex) {
            System.out.println(ex);
        }
        if (idl != null && !idl.isEmpty()) {
            for (Integer i : idl) {
                try {
                    if(i != 0){
                        BOBSource s = si.getBOBSource(i);
                        if(s != null && s.isUserattributable()){
                            sourceList.add(s);
                        }
                    }
                } catch (BObStatusException ex) {
                    System.out.println(ex);
                }
            }
        }
        return sourceList;
    }
    
    
    /**
     * Logic pass through for bob source objects
     * @param sourceid
     * @return
     * @throws IntegrationException 
     */
   public BOBSource getBObSource(int sourceid) throws IntegrationException, BObStatusException{
       if(sourceid==0){
           return null;
       }
       SystemIntegrator si = getSystemIntegrator();
       
       return si.getBOBSource(sourceid);
       
   }
   
   /**
    * 
    * @param m Manageable
    * @return int times used in the database
    * @throws IntegrationException 
    */
   public int checkForUse(Manageable m) throws IntegrationException {
       SystemIntegrator si = getSystemIntegrator();
       return si.checkManagableForUse(m);
   }
   
   /**
    * access point for Icon objects
    * @param iconID
    * @return
    * @throws IntegrationException 
    */
   public Icon getIcon(int iconID) throws IntegrationException {
      SystemIntegrator si = getSystemIntegrator();
      return si.getIcon(iconID);
   }
   
   /**
    * Pathway for deactivating an icon, and flushing all caches
    * @param i
    * @param ua
    * @throws IntegrationException
    * @throws BObStatusException 
    */
   public void deactivateIcon(Icon i, UserAuthorized ua) throws IntegrationException, BObStatusException {
       SystemIntegrator si = getSystemIntegrator();
       si.deactivateIcon(i);
       flushAllSystemCaches();
   }
   
    /**
     * To update icon
     *
     * @param i
     * @param ua
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws AuthorizationException
     */
    public void updateIcon(Icon i, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException {
        if (Objects.isNull(i) || Objects.isNull(ua)) {
            throw new BObStatusException("Cannot update icon with null courtEntity or UMAP");
        }
        if (ua.getKeyCard().getGoverningAuthPeriod().getRole() != RoleType.SysAdmin) {
            throw new AuthorizationException("UMAP must be rank: Sys Admin to update icon");
        }
        SystemIntegrator si = getSystemIntegrator();
        
        si.updateIcon(i);
        getSystemMuniCacheManager().flush(i);
    }
   
    /**
     * Insertion point for icon
     *
     * @param i
     * @param umap
     * @return
     * @throws IntegrationException
     * @throws AuthorizationException
     * @throws BObStatusException
     */
    public Icon insertIcon(Icon i, UserMuniAuthPeriod umap) throws IntegrationException, BObStatusException, AuthorizationException {
        if (Objects.isNull(i) || Objects.isNull(umap)) {
            throw new BObStatusException("Cannot insert icon with null icon or UMAP");
        }
        if (umap.getRole() != RoleType.SysAdmin) {
            throw new AuthorizationException("UMAP must be rank: Sys Admin to insert icon");
        }
        SystemIntegrator si = getSystemIntegrator();
        i.setID(si.insertIcon(i));
        return i;
    }
   
    /**
     * Factory for Icon objects
     *
     * @return new Icon with id = 0
     */
    public Icon getIconSkeleton() {
        Icon i = new Icon();
        return i;
    }
   
   public List<Icon> getIconList(boolean includeDeactivated) throws IntegrationException {
       SystemIntegrator si = getSystemIntegrator();
       return si.getIconList(includeDeactivated);
   }
   
   public List<Icon> getIconList() throws IntegrationException {
       SystemIntegrator si = getSystemIntegrator();
       return si.getIconList();
   }
   
   public int putCheckForUse(PropertyUseType p) throws IntegrationException {
       SystemIntegrator si = getSystemIntegrator();
       return si.putCheckForUse(p);
   }
   
   public PropertyUseType getPut(int putID) throws IntegrationException {
      SystemIntegrator si = getSystemIntegrator();
      return si.getPut(putID);
   }
   
   public List<PropertyUseType> getPutList() throws IntegrationException {
       return getPutList(false);
   }
   
   public List<PropertyUseType> getPutList(boolean includeDeactivated) throws IntegrationException {
       SystemIntegrator si = getSystemIntegrator();
       return si.getPutList(includeDeactivated);
   }

    /**
     * Adapter method for taking in simple note info, not in Object format and
     * creating the populated MessageBuilderParams instance required by the
     * official note appending tool of the entire codeNforce system
     *
     * @param u
     * @param noteToAppend
     * @param existingText
     * @return
     */
    public String formatAndAppendNote(UserAuthorized u, String noteToAppend, String existingText) {
        MessageBuilderParams mbp = new MessageBuilderParams(existingText,
                noteToAppend,
                null,
                null,
                u,
                null);
        return appendNoteBlock(mbp);

    }

    /**
     * Adapter method for taking in simple note info, not in Object format and
     * creating the populated MessageBuilderParams instance required by the
     * official note appending tool of the entire codeNforce system
     *
     * @param u
     * @param cred
     * @param noteToAppend
     * @param existingText
     * @return
     */
    public String formatAndAppendNote(UserAuthorized u, Credential cred, String noteToAppend, String existingText) {
        MessageBuilderParams mbp = new MessageBuilderParams(existingText,
                noteToAppend,
                null,
                null,
                u,
                cred);

        return appendNoteBlock(mbp);

    }

    /**
     * @return the muniCodeNameMap
     */
    public Map<Integer, String> getMuniCodeNameMap() {
        if (muniCodeNameMap == null) {

            Map<Integer, String> m = null;
            MunicipalityIntegrator mi = getMunicipalityIntegrator();
            try {
                m = mi.getMunicipalityMap();
            } catch (IntegrationException ex) {
                System.out.println(ex);
            }
            muniCodeNameMap = m;
        }
        return muniCodeNameMap;
    }


    /**
     * @param muniCodeNameMap the muniCodeNameMap to set
     */
    public void setMuniCodeNameMap(Map<Integer, String> muniCodeNameMap) {
        this.muniCodeNameMap = muniCodeNameMap;
    }
    
    // *********************************************************************
    // ******************** TEXT BLOCK INFRASTRUCTURE  *********************
    // *********************************************************************
    
    
    /**
     * Gets a text block from the DB by ID
     * @param blockid
     * @return
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public TextBlock getTextBlock(int blockid) throws IntegrationException, BObStatusException{
        CaseIntegrator ci = getCaseIntegrator();
        if(blockid == 0){
            throw new BObStatusException("cannot get a text block with ID 0");
        }
        return ci.getTextBlock(blockid);
    }
    
    /**
     * Get an Integer[] from a List of text Blocks; used during dynamic saving 
     * of permits
     * @param blockList
     * @return 
     */
    public Integer[] getTextBlockIntegerArrayFromBlocks(List<TextBlock> blockList){
        
      Integer[] ids = null;
        if(blockList != null && !blockList.isEmpty()){
            ids = (Integer[]) blockList.stream().map(TextBlock::getBlockID).toArray(Integer[]::new);
        }
        return ids;
    }
    
    /**
     * Extracts text blocks from the DB for user selection;
     * @param tbc if null, category is ignored in query
     * @param m if null, all muni's blocks are returned
     * @return a list, perhaps containing 1 or more text blocks
     */
    public List<TextBlock> getTextBlockList(TextBlockCategory tbc, Municipality m) throws IntegrationException, BObStatusException{
        CaseIntegrator ci = getCaseIntegrator();
        List<Integer> blockIDList = ci.getTextBlockIDList(tbc, m);
       return getTextBlocksByIDList(blockIDList);
    }
    
    /**
     * Utility method for iterating over a list of TextBlock IDs
     * and returning the object list
     * @param idl
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public List<TextBlock> getTextBlocksByIDList(List<Integer> idl) throws IntegrationException, BObStatusException{
         List<TextBlock> blockListFinal = new ArrayList<>();
        for(Integer i: idl){
            blockListFinal.add(getTextBlock(i));
        }
        return blockListFinal;
    }
    
    /**
     * Extracts all text blocks from DB, even deactivated ones for config
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public List<TextBlock> getTextBlockListComplete() throws IntegrationException, BObStatusException{
        CaseIntegrator ci = getCaseIntegrator();
        return getTextBlocksByIDList(ci.getAllTextBlocks());
        
        
    }
    
    
    /**
     * Factory for TextBlock objects
     * @param muni
     * @return 
     */
    public TextBlock getTextBlockSkeleton(Municipality muni){
        TextBlock tb = new TextBlock();
        tb.setMuni(muni);
        return tb;
    }
    
    
    /**
     * Factory for TextBlockCategory objects
     * @param muni
     * @return 
     */
    public TextBlockCategory getTextBlockCategorySkeleton(Municipality muni){
        TextBlockCategory tbc = new TextBlockCategory();
        tbc.setMuni(muni);
        return tbc;
        
    }
    
    /**
     * Extracts all text block categories from the DB
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public List<TextBlockCategory> getTextBlockCategoryListComplete() throws IntegrationException, BObStatusException{
        CaseIntegrator ci = getCaseIntegrator();
        return getTextBlockCategoryListFromIDList(ci.getTextBlockCategoryList(null));
        
    }
    /**
     * Extracts text block categories from the DB for certificates
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public List<TextBlockCategory> getTextBlockCategoryListForCertificates() throws IntegrationException, BObStatusException{
        
          List<Integer> idl = Stream.of(TextBlockPermitFieldEnum.values()).map(enumValue -> {
                    String stringValue = getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString(enumValue.getDbFixedValueLookup());
                    return Integer.parseInt(stringValue);
                }).collect(Collectors.toList());
        return getTextBlockCategoryListFromIDList(idl);
        
    }
    
    /**
     * Logic intermediary for fetching text block categories
     * @param catid
     * @return
     * @throws IntegrationException 
     */
    public TextBlockCategory getTextBlockCategory(int catid) throws IntegrationException{
        
        CaseIntegrator ci = getCaseIntegrator();
        
        if(catid == 0){
            return null;
        }
        
        return ci.getTextBlockCategory(catid);
        
    }
    
    
    /**
     * Utility methodf or getting a text block category list from IDs only
     * @param idl
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
     public List<TextBlockCategory> getTextBlockCategoryListFromIDList(List<Integer> idl) throws IntegrationException, BObStatusException{
         
         List<TextBlockCategory> blockListFinal = new ArrayList<>();
        for(Integer i: idl){
            blockListFinal.add(getTextBlockCategory(i));
        }
        return blockListFinal;
    }
    
    /**
     * Primary insertion point for text blocks
     * @param tb
     * @param ua
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public int insertTextBlock(TextBlock tb, UserAuthorized ua) throws BObStatusException, IntegrationException{
        CaseIntegrator ci = getCaseIntegrator();
        if(tb == null || ua == null){
            throw new BObStatusException("Cannot insert text block with null block or null user");
        }
        tb.setTextBlockText(cleanRichText(tb.getTextBlockText()));
        return ci.insertTextBlock(tb);
        
    }
    /**
     * Primary insertion point for text blocks
     * @param tbc
     * @return  the fresh ID
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public int insertTextBlockCategory(TextBlockCategory tbc) throws BObStatusException, IntegrationException{
        CaseIntegrator ci = getCaseIntegrator();
        if(tbc == null ){
            throw new BObStatusException("Cannot insert text block Cat with null block or null user");
        }
        
        return ci.insertTextBlockCategory(tbc);
        
    }
    
    
    /**
     * Logic and permissions check for updates to a text block
     * @param tb
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void updateTextBlock(TextBlock tb, UserAuthorized ua) throws IntegrationException, BObStatusException{
        CaseIntegrator ci = getCaseIntegrator();
        if(tb == null || ua == null){
            throw new BObStatusException("Cannot insert text block with null block or null user");
        }
        tb.setTextBlockText(cleanRichText(tb.getTextBlockText()));
        ci.updateTextBlock(tb);
        
    }
    
    /**
     * Logic and permissions check for updates to a text block
     * @param tbc
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void updateTextBlockCategory(TextBlockCategory tbc) throws IntegrationException, BObStatusException{
        CaseIntegrator ci = getCaseIntegrator();
        if(tbc == null ){
            throw new BObStatusException("Cannot update text block with null block cat ");
        }
        
        ci.updateTextBlockCategory(tbc);
        
    }

    /**
     * Logic and permissions check for deactivation of TextBlocks
     * 
     * @param tb
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void deactivateTextBlock(TextBlock tb, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(tb == null || ua == null){
            throw new BObStatusException("Cannot deactivate text block with null block or null user");
        }
        CaseIntegrator ci = getCaseIntegrator();
        tb.setDeactivatedTS(LocalDateTime.now());
        ci.updateTextBlock(tb);
        
        
    }
    
    

    /**
     * Logic and permissions check for deactivation of TextBlocksCats
     * 
     * @param tbc
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void deactivateTextBlockCategory(TextBlockCategory tbc) throws BObStatusException, IntegrationException{
        if(tbc == null){
            throw new BObStatusException("Cannot deactivate text block cat with null block or null user");
        }
        CaseIntegrator ci = getCaseIntegrator();
        tbc.setDeactivatedTS(LocalDateTime.now());
        ci.updateTextBlockCategory(tbc);
        
        
    }
    
    
    
    
    
    
    // *********************************************************************
    // ******************** NAVIGATION INFRASTRUCTURE *********************
    // *********************************************************************

    //xiaohong add
    //Store SubNav Items into List: Dashboard
    public List<NavigationSubItem> getDashboardNavList() {
        ArrayList<NavigationSubItem> navList;
        navList = new ArrayList<>();
        return navList;
    }

  
    //Nav Bar
    //Sub NavItem: Property
    private final NavigationSubItem propertySearch = getNavSubItem("Search", "/restricted/cogstaff/prop/propertySearchProfile.xhtml", "fa fa-sign-in", false);
    private final NavigationSubItem propertyInfo = getNavSubItem("Info", "/restricted/cogstaff/prop/propertySearchProfile.xhtml", "fa fa-sign-in", false);
    private final NavigationSubItem propertyUnits = getNavSubItem("Units", "/restricted/cogstaff/prop/propertyUnits.xhtml", "fa fa-sign-in", false);
    private final NavigationSubItem propertyUnitChanges = getNavSubItem("Unit Changes", "/restricted/cogstaff/prop/propertyUnitChanges.xhtml", "fa fa-sign-in", false);

    //Store SubNav Items into List: Property
    public List<NavigationSubItem> getPropertyNavList() {
        ArrayList<NavigationSubItem> navList;
        navList = new ArrayList<>();
        navList.add(propertySearch);
        navList.add(propertyInfo);
        navList.add(propertyUnits);
        navList.add(propertyUnitChanges);
        return navList;
    }

    //Sub NavItem: Persons
    // listed in file order
    private final NavigationSubItem personCECases = getNavSubItem("CE Cases", "/restricted/cogstaff/person/personCECases.xhtml", "fa fa-sign-in", false);
    private final NavigationSubItem personChanges = getNavSubItem("Changes", "/restricted/cogstaff/person/personChanges.xhtml", "fa fa-sign-in", false);
    private final NavigationSubItem personInfo = getNavSubItem("Info", "/restricted/cogstaff/person/personInfo.xhtml", "fa fa-sign-in", false);
    private final NavigationSubItem personOccPeriods = getNavSubItem("Occ periods", "/restricted/cogstaff/person/personOccPeriods.xhtml", "fa fa-sign-in", false);
    private final NavigationSubItem personProperties = getNavSubItem("Properties", "/restricted/cogstaff/person/personProperties.xhtml", "fa fa-sign-in", false);
    private final NavigationSubItem personPublic = getNavSubItem("Public", "/restricted/cogstaff/person/personPublic.xhtml", "fa fa-sign-in", false);
    private final NavigationSubItem personSearch = getNavSubItem("Search", "/restricted/cogstaff/person/personSearch.xhtml", "fa fa-sign-in", false);

    //Store SubNav Items into List: Person
    // listed in display order
    public List<NavigationSubItem> getPersonNavList() {
        ArrayList<NavigationSubItem> navList;
        navList = new ArrayList<>();
        navList.add(personSearch);
        navList.add(personInfo);
        navList.add(personProperties);
        navList.add(personOccPeriods);
        navList.add(personCECases);
        navList.add(personPublic);
        navList.add(personChanges);
        return navList;
    }

    //Sub NavItem: Code
    private final NavigationSubItem codeSources = getNavSubItem("Sources", "/restricted/cogstaff/code/codeSourceManage.xhtml", "fa fa-sign-in", false);
    private final NavigationSubItem codeDetails = getNavSubItem("Details", "", "fa fa-sign-in", false);

    //Store SubNav Items into List: Code
    public List<NavigationSubItem> getCodeNavList() {
        ArrayList<NavigationSubItem> navList;
        navList = new ArrayList<>();
        navList.add(codeSources);
        navList.add(codeDetails);
        return navList;
    }

    public List<NavigationItem> navList() {

        ArrayList<NavigationItem> navList;
        navList = new ArrayList<>();
        try {
            //NavItem: Dashboard
            NavigationItem dashboardItem = getNavItem("/restricted/missionControl.xhtml", "Dashboard", "fa fa-dashboard", getDashboardNavList());
            //NavItem: Property
            NavigationItem propertyItem = getNavItem("/restricted/cogstaff/prop/propertySearchProfile.xhtml", "Property", "fa fa-home", getPropertyNavList());
            //NavItem: Persons
            NavigationItem personItem = getNavItem("/restricted/cogstaff/person/personSearch.xhtml", "Person", "fa fa-female", getPersonNavList());
            //NavItem: Code
            NavigationItem codeItem = getNavItem("/restricted/cogstaff/code/codeSourceManage.xhtml", "Code", "fa fa-book", getCodeNavList());

            navList.add(dashboardItem);
            navList.add(propertyItem);
            navList.add(personItem);
//            navList.add(CEItem);
            navList.add(codeItem);
        } catch (Exception e) {
            System.out.println(e);
        }
        return navList;

    }

    //Side Tool Bar
    //Sidebar Sub Nav Item: Municipal Code
    private final NavigationSubItem codeSource = getNavSubItem("Ordinances", "/restricted/cogstaff/code/codeElementManage.xhtml", "fa fa-university", false);
    private final NavigationSubItem codeBook = getNavSubItem("Code Books", "/restricted/cogstaff/code/codeSetManage.xhtml", "fa fa-gavel", false);

    //Store SubNav Items into List:Code
    public List<NavigationSubItem> getSidebarCodeConfigList() {
        ArrayList<NavigationSubItem> navList;
        navList = new ArrayList<>();
        navList.add(codeSource);
        navList.add(codeBook);
        return navList;
    }

    //Sidebar Sub Nav Item: CE
    private final NavigationSubItem caseEvent = getNavSubItem("Case Event", "/restricted/cogstaff/ce/eventConfiguration.xhtml", "fa fa-sign-in", false);
    private final NavigationSubItem courtEntity = getNavSubItem("Court Entity", "/restricted/cogstaff/ce/courtEntityManage.xhtml", "fa fa-sign-in", false);
    private final NavigationSubItem notice = getNavSubItem("Notice", "/restricted/cogstaff/ce/textBlockManage.xhtml", "fa fa-sign-in", false);

    //Store SubNav Items into List:Code
    public List<NavigationSubItem> getSidebarCEConfigList() {
        ArrayList<NavigationSubItem> navList;
        navList = new ArrayList<>();
//        navList.add(caseEvent);
//        navList.add(notice);
        navList.add(courtEntity);
        return navList;
    }

    private final NavigationSubItem feeManage = getNavSubItem("Fees", "/restricted/cogstaff/occ/feeManage.xhtml", "fa fa-sign-in", false);
    private final NavigationSubItem feeTypeManage = getNavSubItem("Fee types", "/restricted/cogstaff/occ/feeTypeManage.xhtml", "fa fa-sign-in", false);
    private final NavigationSubItem feePermissions = getNavSubItem("Occ Fees", "/restricted/cogstaff/occ/feePermissions", "fa fa-sign-in", false);

    //Store SubNav Items into List:Payment
    public List<NavigationSubItem> getSidebarPaymentList() {
        ArrayList<NavigationSubItem> navList;
        navList = new ArrayList<>();
        navList.add(feeManage);
        navList.add(feeTypeManage);
        navList.add(feePermissions);
        return navList;
    }

    //Sidebar Sub Nav Item: Occ
    private final NavigationSubItem checklist = getNavSubItem("Checklists", "/restricted/cogstaff/occ/checklists.xhtml", "fa fa-sign-in", false);
    private final NavigationSubItem permitting = getNavSubItem("Permitting Config", "/restricted/cogstaff/occ/permittingConfiguration.xhtml", "fa fa-sign-in", false);
    private final NavigationSubItem payment = getNavSubItem("Payment", "/restricted/cogstaff/occ/checklists.xhtml", "fa fa-sign-in", false);
    private final NavigationSubItem feeType = getNavSubItem("Fee Type", "/restricted/cogstaff/occ/paymentManage.xhtml", "fa fa-sign-in", false);

    //Store SubNav Items into List:Code
    public List<NavigationSubItem> getSidebarOccConfigList() {
        ArrayList<NavigationSubItem> navList;
        navList = new ArrayList<>();
        navList.add(permitting);
        navList.add(checklist);
        navList.add(payment);
        navList.add(feeType);
        return navList;
    }

    //Sidebar Sub Nav Item: Reports
    private final NavigationSubItem events = getNavSubItem("Event Activity", "/restricted/cogstaff/event/events.xhtml", "fa fa-flag", false);
    private final NavigationSubItem eventConfig = getNavSubItem("Event setup and categories", "/restricted/cogstaff/event/eventCatConfig.xhtml", "fa fa-sign-in", false);
//    private final NavigationSubItem eventRuleConfig = getNavSubItem("Event Rules", "/restricted/cogstaff/event/eventRuleConfiguration.xhtml", "fa fa-sign-in", false);

    //Store SubNav Items into List: Reports
    public List<NavigationSubItem> getSidebarReportList() {
        ArrayList<NavigationSubItem> navList;
        navList = new ArrayList<>();
        navList.add(events);
        navList.add(eventConfig);
//        navList.add(eventRuleConfig);
        return navList;
    }

    //Sidebar Sub Nav Item: System
    private final NavigationSubItem users = getNavSubItem("Users", "/restricted/cogadmin/userConfig.xhtml", "pi pi-users", false);
    private final NavigationSubItem manage = getNavSubItem("Municipalities", "/restricted/cogstaff/muni/muniManage.xhtml", "pi pi-building", false);
    private final NavigationSubItem oid = getNavSubItem("Occ Det's", "/restricted/cogadmin/occInspectionDeterminationManage.xhtml", "fa fa-pencil-square-o", false);
    private final NavigationSubItem blobs = getNavSubItem("Files", "/restricted/cogadmin/manageBlob.xhtml", "fa fa-folder", false);
    private final NavigationSubItem umapLog = getNavSubItem("Session Log", "umapLog", "pi pi-book", false);
    private final NavigationSubItem linkedObjectRole = getNavSubItem("Linked Object Role", "/restricted/cogadmin/linkedObjectRoleManage.xhtml", "pi pi-user-plus", false);
    private final NavigationSubItem icon = getNavSubItem("Manage Icons", "/restricted/cogadmin/iconManage.xhtml", "pi pi-prime", false);
    private final NavigationSubItem multiObject = getNavSubItem("Manage Dropdown Lists", "/restricted/cogadmin/multiObjectManage.xhtml", "pi pi-list", false);
    private final NavigationSubItem adminControl = getNavSubItem("Admin control panel", "/restricted/cogadmin/adminControlPanel.xhtml", "pi pi-list", false);
    


    //Store SubNav Items into List: System
    public List<NavigationSubItem> getSidebarSystemList() {
        ArrayList<NavigationSubItem> navList;
        navList = new ArrayList<>();
        navList.add(users);
        navList.add(manage);
        navList.add(adminControl);
        navList.add(linkedObjectRole);
        navList.add(multiObject);
        navList.add(icon);
        navList.add(umapLog);
        return navList;
    }

    //Sidebar Sub Nav Item: Help
    private final NavigationSubItem howto = getNavSubItem("How-To", "/public/system/documentation/howtos/howtos.xhtml", "fa fa-sign-in", false);

    //Store SubNav Items into List: Help
    public List<NavigationSubItem> getSidebarHelpList() {
        ArrayList<NavigationSubItem> navList;
        navList = new ArrayList<>();
        navList.add(howto);
        return navList;
    }
    
    private final NavigationSubItem cear = getNavSubItem("Submit action request", "/public/services/requestCEActionFlow/requestCEActionFlow.xhtml", "pi pi-flag", false);
    
//Store SubNav Items into List: Help
    public List<NavigationSubItem> getSideBarActionRequestList() {
        ArrayList<NavigationSubItem> navList;
        navList = new ArrayList<>();
        navList.add(cear);
        return navList;
    }

    /**
     * Builds a set of side bar navigation page options for users based on their role
     * @param ua requesting the side bar
     * @return 
     */
    public List<NavigationItem> buildSideBarNavList(UserAuthorized ua) {

        ArrayList<NavigationItem> navList;
        navList = new ArrayList<>();

        /**
         * send back no items, but don't kill the page
         */
        if (ua == null) {
            return navList;
        }
        // note: no page URLs are stored for side meaning - meanng side bar items don't 
        // get selected automatically based on current page
        // and are not related to a currently loaded object
        try {
            //NavItem: Occ
            NavigationItem OccconfigItem = getNavItem("", "Occupancy", "pi pi-folder-open", getSidebarOccConfigList());
            //NavItem: Case
            NavigationItem ceCaseItem = getNavItem("", "Case", "pi pi-folder-open", getSidebarCEConfigList());
            //NavItem: Code
            NavigationItem codeconfigItem = getNavItem("", "Municipal Code", "pi pi-book", getSidebarCodeConfigList());
            //NavItem: Event Activity
            NavigationItem reportItem = getNavItem("", "Event tools", "pi pi-tag", getSidebarReportList());
            //NavItem: Payments
            NavigationItem paymentsItem = getNavItem("", "Payments", "pi pi-wallet", getSidebarPaymentList());
            //NavItem: System
            NavigationItem systemItem = getNavItem("", "System", "pi pi-cog", getSidebarSystemList());
            //NavItem: Help
            NavigationItem helpItem = getNavItem("", "Help", "pi pi-question", getSidebarHelpList());
            NavigationItem cearSub = getNavItem("", "Action requests", "pi pi-flag", getSideBarActionRequestList());

            if (ua.getMyCredential().isHasSysAdminPermissions()) {
                navList.add(reportItem);
                navList.add(ceCaseItem);
                navList.add(systemItem);
                navList.add(codeconfigItem);
                navList.add(OccconfigItem);
                navList.add(paymentsItem);
            }
            // for all users
            navList.add(cearSub);
            navList.add(helpItem);
        } catch (Exception e) {
            System.out.println(e);
        }
        return navList;
    }

    public NavigationSubItem getNavSubItem(String value, String path, String icon, boolean disable) {
        NavigationSubItem mn = new NavigationSubItem();
        mn.setValue(value);
        mn.setPagePath(path);
        mn.setIcon(icon);
        mn.setDisable(disable);
        return mn;
    }

    public NavigationItem getNavItem(String searchPageUrl, String navCategory, String icon, List navSubList) {
        NavigationItem ni = new NavigationItem();
        ni.setValue(navCategory);
        ni.setIcon(icon);
        ni.setSubNavitem(navSubList);
        ni.setSearchpageurl(searchPageUrl);
        return ni;
    }

    // *************************************************************************
    // ****************** TEXT COMPARISONS  ************************************
    // *************************************************************************
    
    /**
     * Applies the Levenshtein distance algorithm to incoming texts. Strips white 
     * space before running the algo. This algo counts the number of changes required
     * to make the two Strings the same. Inputting "asdf" and "asdg" would return a value of 1
     * @param text1 cannot be null
     * @param text2 cannot be null
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public int computeLevenshteinDistance(String text1, String text2) throws BObStatusException{
        if(text1 == null || text1 == null){
            throw new BObStatusException("Cannot compute distance between 1 or more empty strings");
        }
        String cleanString1 = text1.strip();
        String cleanString2 = text2.strip();
        int levenDist = LevenshteinDistance.getDefaultInstance().apply(cleanString1, cleanString2);
        return levenDist;
        
    }
    

    // *************************************************************************
    // ****************** PRINT STYLE STUFF ************************************
    // *************************************************************************
    
    
    /**
     * Factory for PrintStyle objects
     * @return the skeleton object with id = 0
     */
    public PrintStyle getPrintStyleSkeleton(){
        return new PrintStyle();
    }
    
    /**
     * Extracts a list of print styles from the db
     * @return
     * @throws IntegrationException 
     */
    public List<PrintStyle> getPrintStyleList() throws IntegrationException {
        SystemIntegrator si = getSystemIntegrator();
        return si.getPrintStyleList();
    }

    /**
     * Extracts a single print style from the db
     * @param styleid
     * @return
     * @throws IntegrationException 
     */
    public PrintStyle getPrintStyle(int styleid) throws IntegrationException {
        SystemIntegrator si = getSystemIntegrator();
        return si.getPrintStyle(styleid);
    }
    
    
    /**
     * Insertion point for print styles
     * @param ps
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public int insertPrintStyle(PrintStyle ps) throws BObStatusException, IntegrationException{
        SystemIntegrator si = getSystemIntegrator();
        if(ps == null){
            throw new BObStatusException("Cannot insert a null PrintStyle");
        }
        return si.insertPrintStyle(ps);
    }
    
    
    /**
     * Insertion point for print styles
     * @param ps
     * @param ua
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void updatePrintStyle(PrintStyle ps, UserAuthorized ua) throws BObStatusException, IntegrationException{
        SystemIntegrator si = getSystemIntegrator();
        if(ps == null){
            throw new BObStatusException("Cannot uppdate a null PrintStyle");
        }
        si.updatePrintStyle(ps);
        
        // flush our caches
        SystemMuniUserCacheManager smucm = getSystemMuniCacheManager();
        smucm.flush(ps);
    }
    
    // *************************************************************************
    // ****************** UMAP-BASED PERMISSIONS STUFF *************************
    // *************************************************************************
    

    
    /**
     * Permissions checkpoint for tracking updates of Objects which extend UMAPTrackedEntity
     * @param ute
     * @param umap 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void updateUMAPTrackedEntity(UMAPTrackedEntity ute, UserMuniAuthPeriod umap) throws BObStatusException, IntegrationException{
        if(ute == null || umap == null){
            throw new BObStatusException("Cannot update UMAPTracked entity with null inputs");
        }
        SystemIntegrator si = getSystemIntegrator();
        ute.setLastUpdatedby_UMAP(umap);
        si.updateStampUMAPTrackedEntity(ute);
    }
    
    /**
     * Permissions checkpoint for deactivation of objects which extend UMAPTrackedEntity
     * 
     * @param ute
     * @param umap 
     */
    public void deactivateUMAPTrackedEntity(UMAPTrackedEntity ute, UserMuniAuthPeriod umap) throws BObStatusException, IntegrationException{
        if(ute == null || umap == null){
            throw new BObStatusException("Cannot deactivate UMAPTracked entity with null inputs");
        }
        SystemIntegrator si = getSystemIntegrator();
        ute.setDeactivatedBy_UMAP(umap);
        si.deactivateUMAPTrackedEntity(ute);
        
    }
    
    /**
     * Get the list of linkedObjectRole based on active/inactive flag.
     * @param isActive
     * @return List(LinkedObjectRole>)
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public List<LinkedObjectRole> getLinkedObjectRolesList(boolean isActive) throws IntegrationException, BObStatusException{
        SystemIntegrator si = getSystemIntegrator();
        return si.getLinkedObjectRoleList(isActive);
    }

    /**
     * Get the LinkedObjectRole object based on roleid.
     * @param id
     * @return LinkedObjectRole
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public LinkedObjectRole getLinkedObjectRole(int id) throws IntegrationException, BObStatusException {
        if (id == 0) {
            throw new BObStatusException("cannot fetch a LinkedObjectRole of ID 0");
        }

        SystemIntegrator si = getSystemIntegrator();
        return si.getLinkedObjectRole(id);
    }
    
    /**
     * Chops up the current time to get seven random digits
     * @return
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public int generateControlCodeFromTime() throws IntegrationException {
        int controlCode;
        long dateInMs = new Date().getTime();
        String numAsString = String.valueOf(dateInMs);
        String reducedNum = numAsString.substring(7);
        controlCode = Integer.parseInt(reducedNum);
        return controlCode;
    }
    
    /**
     * To insert the LinkedObjectRole into the DB.
     * @param loRole
     * @param umap
     * @return LinkedObjectRole
     * @throws IntegrationException
     * @throws AuthorizationException
     * @throws BObStatusException 
     */
    public LinkedObjectRole insertLinkedObjectRole(LinkedObjectRole loRole, UserMuniAuthPeriod umap) throws IntegrationException, AuthorizationException, BObStatusException {
        if (Objects.isNull(loRole) || Objects.isNull(umap)) {
            throw new BObStatusException("Cannot insert LinkedObjectRole with null LinkedObjectRole or UMAP");
        }
        if (umap.getRole() != RoleType.SysAdmin) {
            throw new AuthorizationException("UMAP must be rank: Sys Admin to insert LinkedObjectRole");
        }
        loRole.setCreatedby_UMAP(umap);
        loRole.setLastUpdatedby_UMAP(umap);

        SystemIntegrator si = getSystemIntegrator();
        loRole.setRoleID(si.insertLinkedObjectRole(loRole));
        return loRole;
    }
    
    /**
     * To update the LinkedObjectRole into the DB.
     * @param loRole
     * @param ua
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws AuthorizationException 
     */
    public void updateLinkedObjectRole(LinkedObjectRole loRole, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException {
        if (Objects.isNull(loRole) || Objects.isNull(ua)) {
            throw new BObStatusException("Cannot update LinkedObjectRole with null LinkedObjectRole or UMAP");
        }
        if (ua.getKeyCard().getGoverningAuthPeriod().getRole() != RoleType.SysAdmin) {
            throw new AuthorizationException("UMAP must be rank: Sys Admin to update courtEntity");
        }
        loRole.setLastUpdatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());

        SystemIntegrator si = getSystemIntegrator();
        
        si.updateLinkedObjectRole(loRole);
        
        getSystemMuniCacheManager().flush(loRole);
    }
    
    
    /**
     * Create a LinkedObjectRole instance.
     * @param umap
     * @return 
     */
    public LinkedObjectRole getLinkedObjectRoleSkeleton(UserMuniAuthPeriod umap) {
        LinkedObjectRole loRole = new LinkedObjectRole();
        loRole.setCreatedby_UMAP(umap);
        loRole.setLastUpdatedby_UMAP(umap);
        return loRole;
    }

    /**
     * To deactivate the LinkedObjectRole. 
     * @param loRole
     * @param umap
     * @throws BObStatusException
     * @throws AuthorizationException
     * @throws IntegrationException 
     */
    public void deactivateLinkedObjectRole(LinkedObjectRole loRole, UserMuniAuthPeriod umap) throws BObStatusException, AuthorizationException, IntegrationException {
        if (Objects.isNull(loRole) || Objects.isNull(umap)) {
            throw new BObStatusException("Cannot deactivate LinkedObjectRole with null LinkedObjectRole or UMAP");
        }
        if (umap.getRole() != RoleType.SysAdmin) {
            throw new AuthorizationException("UMAP must be rank: Sys Admin to deactivate LinkedObjectRole");
        }
        loRole.setDeactivatedBy_UMAP(umap);
        SystemIntegrator si = getSystemIntegrator();
        si.deactivateUMAPTrackedEntity(loRole);
    }
    
    /**
     * Get the list of LinkedObjectSchemaEnum.
     * @return List(LinkedObjectSchemaEnum)
     * @throws BObStatusException 
     */
    public List<LinkedObjectSchemaEnum> getLinkedObjectSchemaEnumList() throws BObStatusException{
        SystemIntegrator si = getSystemIntegrator();
        return si.populateLinkedObjectSchemaEnumList();
    }
    
    
    
    // *************************************************************************
    // ****************** XMUNI FACILITY INFRASTRUCTURE ************************
    // *************************************************************************
    
    /**
     * Factory for xmuni activation records
     * @return ref to new object
     */
    public XMuniActivationRecord getXMuniActivationRecordSkeleton(){
        return new XMuniActivationRecord();
    }
    
    public XMuniActivationRecord getXMuniActivationRecord(int xmrid) throws BObStatusException, IntegrationException{
        SystemIntegrator si = getSystemIntegrator();
        if(xmrid != 0){
            return si.getXMuniActivationRecord(xmrid);
        }
        return null;
        
    }
    
    /**
     * Populates, checks, and writes a new record to the xmuniactivation table
     * @param xmr
     * @param hlpoih
     * @param ua
     * @return 
     */
    public int insertXMuniActivationRecord(XMuniActivationRecord xmr, HumanLinkParentObjectInfoHeavy hlpoih, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(xmr == null || hlpoih == null || ua == null){
            throw new BObStatusException("Cannot insert xmr with null record or user");
        }
        SystemIntegrator si = getSystemIntegrator();
        xmr.setHumanLinkConduit(hlpoih); // not used as of first implementation -- this should be all we need
        
        xmr.setxMuni(hlpoih.getMuiniParent());
        xmr.setHumanConduit(hlpoih);
        xmr.setLinkConduitRole(hlpoih.getLinkedObjectRole());
        xmr.setParentKey(hlpoih.getParentObjectID());
        
        xmr.setActivatorUMAP(ua.getKeyCard().getGoverningAuthPeriod());
        // activation ts set by PG now()
        xmr.setActivatorUMAP(ua.getKeyCard().getGoverningAuthPeriod());
        
        return si.insertXMuniActivationRecord(xmr);
        
        
    }
    
    /**
     * Writes an exit timestamp for the given xmuni activation record
     * @param xmr 
     */
    public void logXMuniExit(XMuniActivationRecord xmr) throws BObStatusException, IntegrationException{
        if(xmr == null){
            throw new BObStatusException("Cannot log exit with null xmr");
        }
        SystemIntegrator si = getSystemIntegrator();
        si.recordXMuniExit(xmr);
        System.out.println("SystemCoordinator.logXMuniExit");
        
    }

    
    
    
}
