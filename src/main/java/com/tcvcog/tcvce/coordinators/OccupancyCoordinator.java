/*
 * Copyright (C) Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.coordinators;

import com.tcvcog.tcvce.entities.reports.ReportConfigOccActivityPermitTypeListCont;
import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.domain.*;
import com.tcvcog.tcvce.entities.occupancy.*;
import com.tcvcog.tcvce.entities.*;
import com.tcvcog.tcvce.integration.*;
import com.tcvcog.tcvce.entities.occupancy.OccPeriodDataHeavy;
import com.tcvcog.tcvce.entities.occupancy.OccPermitType;
import com.tcvcog.tcvce.entities.occupancy.OccPermit;
import com.tcvcog.tcvce.entities.reports.ReportConfigOccPermit;
import com.tcvcog.tcvce.integration.OccInspectionIntegrator;
import com.tcvcog.tcvce.integration.OccupancyIntegrator;
import com.tcvcog.tcvce.util.Constants;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import com.tcvcog.tcvce.entities.reports.ReportConfigOccActivity;
import com.tcvcog.tcvce.entities.reports.ReportConfigOccActivityFinListCont;
import com.tcvcog.tcvce.entities.search.QueryCodeViolation;
import com.tcvcog.tcvce.entities.search.QueryCodeViolationEnum;
import com.tcvcog.tcvce.entities.search.QueryFieldInspection;
import com.tcvcog.tcvce.entities.search.QueryFieldInspectionEnum;
import com.tcvcog.tcvce.entities.search.QueryOccPermit;
import com.tcvcog.tcvce.entities.search.QueryOccPermitEnum;
import com.tcvcog.tcvce.entities.search.QueryProperty;
import com.tcvcog.tcvce.entities.search.SearchParamsOccPermit;
import com.tcvcog.tcvce.entities.search.SearchParamsOccPermitDateFieldsEnum;
import com.tcvcog.tcvce.integration.PersonIntegrator;
import com.tcvcog.tcvce.integration.OccInspectionCacheManager;
import com.tcvcog.tcvce.util.MessageBuilderParams;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * King of all business logic implementation for the entire Occupancy object
 * tree the central of which is the Business Object OccPeriod
 *
 * @author ellen bascomb of apt 31y
 */
public class OccupancyCoordinator extends BackingBeanUtils implements Serializable {

    private final int MINIMUM_RANK_INSPECTOREVENTS = 5;
    private final int MINIMUM_RANK_STAFFEVENTS = 3;
    private final int DEFAULT_OCC_PERIOD_START_DATE_OFFSET = 30;
    private final String OCCPERMIT_DEFAULT_COL_SEP = "-TO-";
    private final int OCCPERMIT_INSPECTION_COUNT_THREE_REPORTED_INSPECTIONS = 3;
    private final int DEFAULT_REINSPECTION_WINDOW_DAYS = 30;
    private final String DEFAULT_OCC_PERIOD_TYPE_TITLE = "Occupancy permitting";
    private final int DEFAULT_OCC_PERIOD_TYPE_ID = 999;
    private final int DEFAULT_CERT_HEADER_WIDTH = 800;
    
    private final int DEFAULT_PERMIT_EXPIRY_WINDOW_DAYS = 30;
    private final int PERMIT_AMENDMENT_WINDOW_DAYS = 45;
    private final String PERMIT_AMENDMENT_SUFFIX = "-AMENDED";
    private final String PERMIT_REVOCATION_SUFFIX = "-REVOKED";

    private final String PERMIT_LEGACY_SELLEROWNER_LABEL = "Property Owner(s)/Seller(s):";
    private final String PERMIT_LEGACY_BUYERTENANT_LABEL = "Property buyer(s)/tenant(s)";

    /**
     * Creates a new instance of OccupancyCoordinator
     */
    public OccupancyCoordinator() {
    }

    /**
     * ********************************************
     */
    /**
     * * PERMISSIONS CHECKPOINTS         **
     */
    /**
     * ********************************************
     */
    /**
     * Permissions logic for opening an Occ period: Require property to be in
     * one's authorized muni and muni staff role floor
     *
     * @param ua
     * @return
     */
    public boolean permissionsCheckpointOpenOccPeriod(UserAuthorized ua) {
        if (ua == null) {
            return false;
        }
        if (ua.getCrossMuniViewAuthorizationTS() != null) {
            return false;
        }
        if (ua.getKeyCard().isHasSysAdminPermissions()) {
            return true;
        }
        if (!ua.getKeyCard().isHasMuniStaffPermissions()) {
            return false;
        }
        return true;
    }

    /**
     * Non muni specific perm logic for drafting permits: Always sys admins with
     * muni staff role floor
     *
     * @param ua
     * @return
     */
    public boolean permissionsCheckpointOccPermitDraft(UserAuthorized ua) {
        if (ua == null) {
            return false;
        }
        if (ua.getKeyCard().isHasSysAdminPermissions()) {
            return true;
        }
        if (!ua.getKeyCard().isHasMuniStaffPermissions()) {
            return false;
        }
        return true;
    }

    /**
     * Security gate for administrative tasks on occupancy objects such as
     * OccPermit management, occ period management, and task chains
     *
     * @param ua
     * @return
     */
    public boolean permissionsCheckpointOccupancyAdministrate(UserAuthorized ua) {
        if (ua == null) {
            return false;
        }
        if (ua.getKeyCard().isHasSysAdminPermissions()) {
            return true;
        }
        return false;
    }

    /**
     * Permissions checkpoint for determining user rights to issue an occ permit
     *
     * @param ua cannot be null
     * @return if user can close a ce case
     */
    public boolean permissionsCheckpointOccPermitIssueAmend(UserAuthorized ua) {
        if (ua == null) {
            return false;
        }
        if (ua.getKeyCard().isHasSysAdminPermissions()) {
            return true;
        }
        if (!ua.getKeyCard().isHasMuniStaffPermissions()) {
            return false;
        }
        if (ua.getGoverningMuniProfile().isRequireCodeOfficerTrueIssuePermits() && !ua.getKeyCard().isHasEnfOfficialPermissions()) {
            return false;
        }
        if (ua.getGoverningMuniProfile().isManagerRequiredPermitFinalize() && !ua.getKeyCard().isHasMuniManagerPermissions()) {
            return false;
        }
        return true;
    }
    /**
     * Permissions checkpoint for determining user rights to issue an occ permit
     *
     * @param ua cannot be null
     * @return if user can close a ce case
     */
    public boolean permissionsCheckpointOccPermitAdminOverride(UserAuthorized ua) {
        if (ua == null) {
            return false;
        }
        if (ua.getKeyCard().isHasSysAdminPermissions()) {
            return true;
        }
        
        return false;
    }
    /**
     * Permissions checkpoint for determining user rights to change a period manager:
     * Managers can be changed by any manager user, the current period manager, 
     * or sys admins
     *
     * @param ua cannot be null
     * @param period
     * @return if user can close a ce case
     */
    public boolean permissionsCheckpointOccPeriodManagerUpdate(UserAuthorized ua, OccPeriod period) {
        if (ua == null || period == null) {
            return false;
        }
        if (ua.getKeyCard().isHasSysAdminPermissions()) {
            return true;
        }
        if (!ua.getKeyCard().isHasMuniManagerPermissions()) {
            return true;
        }
        if (period.getManager() != null && period.getManager().equals(ua)) {
            return true;
        }
        
        return false;
    }
    /**
     * Permissions checkpoint for determining user rights to issue an occ permit
     *
     * @param ua cannot be null
     * @return if user can close a ce case
     */
    public boolean permissionsCheckpointOccPeriodTypeUpdate(UserAuthorized ua) {
        if (ua == null) {
            return false;
        }
        if (ua.getKeyCard().isHasSysAdminPermissions()) {
            return true;
        }
        if (!ua.getKeyCard().isHasMuniStaffPermissions()) {
            return false;
        }
       
        return true;
    }
    
    
    
    /**
     * Permissions gateway for permit revocation:
     * requires system admin, manager rank, or the issuing officer is revoking
     * 
     * @param permit
     * @param ua
     * @return 
     */
    public boolean permissionsCheckpointRevokePermit(OccPermit permit, UserAuthorized ua) {
        if (ua == null) {
            return false;
        }
        if (ua.getKeyCard().isHasSysAdminPermissions()) {
            return true;
        }
        if(ua.getKeyCard().isHasMuniManagerPermissions()){
            return true;
        }
        if(permit != null && permit.getIssuingOfficer() != null && permit.getIssuingOfficer().getUserID() == ua.getUserID()){
            return true;
        }
       
        return false;
    }

    /**
     * ********************************************
     */
    /**
     * * OCC PERIODS CHECKPOINTS         **
     */
    /**
     * ********************************************
     */
    /**
     * Primary retrieval point for extracted OccPeriod objects. Backing beans
     * should not be calling Integrators directly since the Coordinator is
     * responsible for implementing initialization logic usually through a call
     * to configureBObXXXX() method
     *
     * @param periodID
     * @param ua the value of ua
     * @throws IntegrationException
     * @return the com.tcvcog.tcvce.entities.occupancy.OccPeriod
     */
    public OccPeriod getOccPeriod(int periodID, UserAuthorized ua) throws IntegrationException, BlobException {
        OccupancyIntegrator oi = getOccupancyIntegrator();
        OccPeriod op = null;
        try {
            op = configureOccPeriod(oi.getOccPeriod(periodID), ua);
        } catch (EventException | AuthorizationException | BObStatusException | ViolationException ex) {
            System.out.println(ex);
        }
        return op;

    }

    /**
     * factory for occ period types
     *
     * @param muni
     * @return
     */
    public OccPeriodType getOccPeriodTypeSkeleton(Municipality muni) {
        OccPeriodType opt = new OccPeriodType();
        opt.setMuni(muni);
        opt.setActive(true);
        return opt;

    }

    /**
     * Component for managing transition to occ period types with this maker
     * generic one
     *
     * @param muni
     * @return
     */
    public OccPeriodType getDefaultOccPeriodType(Municipality muni) {
        OccPeriodType opt = getOccPeriodTypeSkeleton(muni);
        opt.setReinspectionWindowDays(DEFAULT_REINSPECTION_WINDOW_DAYS);
        opt.setTitle(DEFAULT_OCC_PERIOD_TYPE_TITLE);
        opt.setOccPeriodTypeID(DEFAULT_OCC_PERIOD_TYPE_ID);
        return opt;
    }

    /**
     * Insertion point for occ period types
     *
     * @param opt
     * @param ua
     * @return
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public int insertOccPeriodType(OccPeriodType opt, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException {
        OccupancyIntegrator oi = getOccupancyIntegrator();
        if (opt == null) {
            throw new BObStatusException("Cannot insert occ period type with null input");
        }
        if (!permissionsCheckpointOccupancyAdministrate(ua)) {
            throw new AuthorizationException("User does not have permission to undertake occupancy administrative functions");
        }

        int freshID = oi.insertOccPeriodType(opt);
        opt = getOccPeriodType(freshID);
        oi.connectOccPeriodTypeToPermitTypes(opt);
        return freshID;

    }
    
    /**
     * Coordinates with the user coordinator to assemble eligible users to manage
     * an occ period.
     * @param ua
     * @return all users with active CEO or Manager (not sys admin) UMAPS in the given muni
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public List<User> assembleOccPeriodManagerCandidates(UserAuthorized ua) throws IntegrationException, AuthorizationException, BObStatusException{
        UserCoordinator uc = getUserCoordinator();
        return uc.user_assembleUserListOfficerRequired(ua.getKeyCard().getGoverningAuthPeriod().getMuni(), false);
        
    }
    
       /**
     * Logic gate for updates to occ period types.This also includes deac by
     * toggling active to false;
     *
     * @param opdh whose manager is switching
     * @param requestedManager that the caller wants to be this period's new manager
     * @param ua doing the requesting
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void updateOccPeriodManager(OccPeriodDataHeavy opdh, User requestedManager, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException, EventException {
        OccupancyIntegrator oi = getOccupancyIntegrator();
        EventCoordinator ec = getEventCoordinator();
        if (opdh == null) {
            throw new BObStatusException("Cannot update a null occ period's manager");
        }
        if (!permissionsCheckpointOccPeriodManagerUpdate(ua, opdh)) {
            throw new AuthorizationException("User does not have permission to update the manager of this permit file");
        }
        User formerManager = opdh.getManager();
        opdh.setManager(requestedManager);
        oi.updateOccPeriodManager(opdh);
        ec.logManagerChange(opdh, formerManager, ua);
        getOccupancyCacheManager().flush(opdh);
        
    }

    /**
     * Logic gate for updates to occ period types. I'll change only the period type
     * and log the change in the notes
     *
     * @param opdh
     * @param requestedType to which the user wants the given period to be assigned
     * @param ua
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void updateAnOccPeriodsType(OccPeriodDataHeavy opdh, OccPeriodType requestedType, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException {
        OccupancyIntegrator oi = getOccupancyIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        
        if (opdh == null) {
            throw new BObStatusException("Cannot update a null occ period type");
        }
        if (!permissionsCheckpointOccPeriodTypeUpdate(ua)) {
            throw new AuthorizationException("User does not have permission to undertake occupancy administrative functions");
        }
        
        // only allow types to change if there are no in progress or finalized permits
        if(opdh.getPermitList() != null && !opdh.getPermitList().isEmpty()){
            // disabled during transition to occ period types
            // throw new BObStatusException("Cannot update permit file type if there are any in progress or finalized certificates. First deactivate all certificates and try type change again.");
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Previous file type: ");
        sb.append(opdh.getPeriodType().getTitle());
        sb.append(" (ID:");
        sb.append(opdh.getPeriodType().getOccPeriodTypeID());
        sb.append(") changed to type: ");
        sb.append(requestedType.getTitle());
        sb.append(" (ID:");
        sb.append(requestedType.getOccPeriodTypeID());
        sb.append(").");
        
        MessageBuilderParams mbp = new MessageBuilderParams(opdh.getNotes(), "Change of permit file type", null , sb.toString(), ua, null);
        opdh.setNotes(sc.appendNoteBlock(mbp));
        sc.writeNotes(opdh, ua);
        opdh.setPeriodType(requestedType);
        oi.updateGivenOccPeriodsType(opdh);
        
        getOccupancyCacheManager().flush(opdh);

        
    }
 
    /**
     * Logic gate for updates to occ period types.This also includes deac by
     * toggling active to false;
     *
     * @param opt
     * @param ua
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void updateOccPeriodType(OccPeriodType opt, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException {
        OccupancyIntegrator oi = getOccupancyIntegrator();
        if (opt == null) {
            throw new BObStatusException("Cannot update a null occ period type");
        }
        if (!permissionsCheckpointOccupancyAdministrate(ua)) {
            throw new AuthorizationException("User does not have permission to undertake occupancy administrative functions");
        }
        
        oi.updateOccPeriodType(opt);
        oi.clearOccPeriodTypeToOccPermitTypeLinks(opt);
        oi.connectOccPeriodTypeToPermitTypes(opt);
        getOccupancyCacheManager().flush(opt);
    }
 

    /**
     * getter for occ period types
     *
     * @param typeID
     * @return
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public OccPeriodType getOccPeriodType(int typeID) throws IntegrationException, BObStatusException {
        OccupancyIntegrator oi = getOccupancyIntegrator();
        if (typeID == 0) {
            throw new BObStatusException("cannot get type from type ID 0");
        }
        OccPeriodType opt = oi.getOccPeriodType(typeID);
        opt.setPermitTypes(getOccPermitTypesByOccPeriodType(opt));

        return opt;

    }

    /**
     * Extracts all active occ period types from DB, optionally specific to a given muni
     *
     * @param muni returns all munis if null
     * @param includeInactive
     * @return
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public List<OccPeriodType> getOccPeriodTypeList(Municipality muni, boolean includeInactive) throws IntegrationException {

        OccupancyIntegrator oi = getOccupancyIntegrator();
        List<OccPeriodType> optIncoming = oi.getOccPeriodTypeListComplete();
        List<OccPeriodType> optListOutgoing = new ArrayList<>();
        if (optIncoming != null && !optIncoming.isEmpty()) {
            for (OccPeriodType type : optIncoming) {

                if (type.getMuni() != null && muni != null && type.getMuni().getMuniCode() != muni.getMuniCode()) {
                    continue;
                }
                if (!type.isActive() && !includeInactive) {
                    continue;
                }
                optListOutgoing.add(type);
            }
        }

        return optListOutgoing;
    }

    /**
     * Extracts permit types by occ period type
     *
     * @param opt
     * @return
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public List<OccPermitType> getOccPermitTypesByOccPeriodType(OccPeriodType opt) throws BObStatusException, IntegrationException {
        if (opt == null) {
            throw new BObStatusException("cannot get permit types by period type iwth null period type");
        }

        OccupancyIntegrator oi = getOccupancyIntegrator();

        List<Integer> idList = oi.getOccPermitTypesByOccPeriodType(opt);
        List<OccPermitType> typeList = new ArrayList<>();

        if (idList != null && !idList.isEmpty()) {
            for (Integer id : idList) {
                if(id != 0) typeList.add(oi.getOccPermitType(id));
            }
        }
        return typeList;
    }

    /**
     * Convenience method for getting a list of DatHeavy occ periods from a
     * simple period list
     *
     * @param perList
     * @param ua
     * @return
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws SearchException
     */
    public List<OccPeriodDataHeavy> getOccPeriodDataHeavyList(List<OccPeriod> perList, UserAuthorized ua) throws IntegrationException, BObStatusException, SearchException {
        List<OccPeriodDataHeavy> opdhl = new ArrayList<>();
        for (OccPeriod op : perList) {
            opdhl.add(assembleOccPeriodDataHeavy(op, ua));
        }
        return opdhl;
    }

    /**
     * Typesafe adaptor for getOccPeriodIDListByUnitID()
     *
     * @param pu
     * @param u
     * @return
     * @throws IntegrationException
     * @throws AuthorizationException
     * @throws EventException
     * @throws BObStatusException
     * @throws ViolationException
     */
    public List<OccPeriod> getOccPeriodList(PropertyUnit pu, UserAuthorized u)
            throws IntegrationException,
            AuthorizationException,
            EventException,
            BObStatusException,
            ViolationException,
            BlobException {
        OccupancyIntegrator oi = getOccupancyIntegrator();

        if (pu == null || u == null) {
            throw new BObStatusException("Cannot get Occ Perid list with null prop unit or user");
        }

        List<Integer> opidl = oi.getOccPeriodIDListByUnitID(pu.getUnitID());
        List<OccPeriod> occPeriodList = new ArrayList<>();
        if (opidl != null && !opidl.isEmpty()) {
            for (Integer i : opidl) {
                occPeriodList.add(getOccPeriod(i, u));
            }
        }

        return occPeriodList;
    }

    /**
     * Retrieval point for Data-rich occupancy periods. As of early december
     * 2023, this method does refresh the incoming period
     *
     * @param per
     * @param ua
     * @return
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public OccPeriodDataHeavy assembleOccPeriodDataHeavy(OccPeriod per, UserAuthorized ua) throws IntegrationException, BObStatusException {
        if (per == null || ua == null) {
            throw new BObStatusException("Cannot assemble an OccPeriod data heavy without base period or Credential");
        }

        OccupancyIntegrator oi = getOccupancyIntegrator();
        BlobCoordinator bc = getBlobCoordinator();
        TaskCoordinator tc = getTaskCoordinator();
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        PersonCoordinator persc = getPersonCoordinator();
        OccPeriodDataHeavy opdh = null;

        // now get all the lists from their respective integrators
        // this is the Java version of table joins in SQL; we're doing them interatively
        // in our integrators for each BOB
        try {
            per = getOccPeriod(per.getPeriodID(), ua);
            opdh = new OccPeriodDataHeavy(per);

            // APPLICATION LIST
            opdh.setApplicationList(oi.getOccPermitApplicationList(opdh));

            // TODO: Humanization upgrade after Ben's integration
            opdh.sethumanLinkList(persc.getHumanLinkList(opdh));

            // These are workflow objects that aren't in use as of SEPT 2023
            // PROPOSAL LIST
            // Potentially turn off proposal process? if its broken or erroring?
//            opdh.setProposalList(chc.getProposalList(opdh, ua.getKeyCard()));
            // EVENT RULE LIST
            // Potentially turn off this thingy? if its broken or erroring?
//            opdh.setEventRuleList(chc.rules_getEventRuleImpList(opdh, ua.getKeyCard()));
            // INSPECTION LIST
            opdh.setInspectionList(oic.getOccInspectionLightList(opdh));

            // FEE AND PAYMENT LIST
//          TODO: Activate fee/payment stuff  
//            opdh.setPaymentListGeneral(pai.getPaymentList(opdh));
//            opdh.setFeeList(pai.getFeeAssigned(opdh));
            // BLOB LIST
            opdh.setBlobList(bc.getBlobLightList(opdh));

            tc.configureOccPeriodTasks(opdh, ua);

            configureOccPeriodDataHeavyStatus(opdh, ua);

            // Commented out 26-JAN-2022 here since we  aren't implementing rigid logic
            // on occ period permit generation based on inspection output data
            // TODO: Reconsider governing inspection idea            //
//            opdh.setGoverningInspection(designateGoverningInspection(opdh));
        } catch (BObStatusException
                | EventException
                | AuthorizationException
                | IntegrationException
                | ViolationException
                | BlobException ex) {
            System.out.println(ex);
        }

        return opdh;
    }

    /**
     * Convenience method for dealing with the peculiar generic system not
     * recognizing is a relationships among subclasses.
     *
     * @param opdhList to cast up
     * @return list cast up
     */
    public List<OccPeriod> upcastOccPeriodDataHeavyToOccPeriod(List<OccPeriodDataHeavy> opdhList) {
        List<OccPeriod> perList = new ArrayList<>();
        if (opdhList != null && !opdhList.isEmpty()) {
            for (OccPeriodDataHeavy dh : opdhList) {
                perList.add((OccPeriod) dh);
            }
        }
        return perList;
    }
    
  

    /**
     * Creates a list of allowed permit types by muni profile
     *
     * @param profile
     * @return
     * @throws BObStatusException
     */
    public List<OccPermitType> getOccPermitTypesFromProfileID(MuniProfile profile) throws BObStatusException {
        if (profile == null) {
            throw new BObStatusException("Cannot get permit type list with null muni profile");

        }
        OccupancyIntegrator oi = getOccupancyIntegrator();
        List<OccPermitType> typeList = new ArrayList<>();
        try {
            typeList = oi.getOccPermitTypeListByMuniProfileID(profile.getProfileID());
        } catch (IntegrationException ex) {
            System.out.println(ex.toString());
        }
        return typeList;

    }

    /**
     * Logic container for determining occ period status based on a OPDH
     * <p>
     * As of Beta 0.9, we're just arbitrarily setting the status to unknown
     *
     * @param opdh
     * @param cred
     * @return
     */
    private OccPeriodDataHeavy configureOccPeriodDataHeavyStatus(OccPeriodDataHeavy opdh, UserAuthorized ua) {
        if (opdh == null) {
            return opdh;
        }
        OccPeriodStatusBundle statusBundle = new OccPeriodStatusBundle();

        List<OccPermit> permitList = opdh.getPermitsFinalized();
        List<FieldInspectionLight> finListFinalized = opdh.getInspectionListFinalized();
        List<FieldInspectionLight> finListInProcess = opdh.getInspectionListInProcess();
        try {
            if (!permitList.isEmpty()) {
                // we have at least one finalized permit, so organize the list oldest to most current
                // so later down the list is the more current
                Collections.sort(permitList);
                switch (permitList.size()) {
                    case 1 -> {
                        // with only one permit, lets see if its temporary
                        OccPermit perm = permitList.get(0);
                        if (perm.getPermitType() != null) {
                            if (!perm.getPermitType().isExpires()) {
                                // non-expiring, we're done
                                statusBundle.setStatusEnum(OccPeriodStatusEnum.NONEXPPERMIT_ISSUED);
                                statusBundle.appendToStatusLog("*Assigning status: nonexpirign permit issued");
                            } else {
                                // we have an expiring single permit, so check its expiry window wrt now()
                                if (determineTempPermitStillValid(perm)) {
                                    statusBundle.setStatusEnum(OccPeriodStatusEnum.TEMPPERMIT_ISSUED_VALIDITYWINDOW_INSIDE);
                                    statusBundle.appendToStatusLog("*Assigning status: Inside permit validity window");
                                } else {
                                    statusBundle.setStatusEnum(OccPeriodStatusEnum.TEMPPERMIT_ISSUED_VALIDITYWINDOW_EXPIRED);
                                    statusBundle.appendToStatusLog("*Assigning status: Outside permit validity window");
                                }
                            }
                        } else {
                            statusBundle.setStatusEnum(OccPeriodStatusEnum.UNKNOWN);
                            statusBundle.appendToStatusLog("*Assigning status: unknown | no permit type");

                        }
                    }
                    case 2 -> {
                        OccPermit perm1 = permitList.get(0);
                        OccPermit perm2 = permitList.get(1);
                        // first check that our older permit is not non-expiring and then the second is expiring
                        if (!perm1.getPermitType().isExpires() && perm2.getPermitType().isExpires()) {
                            statusBundle.setStatusEnum(OccPeriodStatusEnum.SEQUENCE_ANOMALY);
                            statusBundle.appendToStatusLog("Anomaly: Most distant permit does not expire, but more recent does.");
                            // now make sure they're not both permanent
                        } else if (!perm1.getPermitType().isExpires() && !perm2.getPermitType().isExpires()) {
                            statusBundle.setStatusEnum(OccPeriodStatusEnum.SEQUENCE_ANOMALY);
                            statusBundle.appendToStatusLog("Anomaly: Both finalized permits cannot be permanent");
                        }
                        if (perm1.getStaticdateofexpiry() != null) {
                            // okay, we have two permits and the first expires, and the second doesn't
                            if (perm1.getStaticdateofexpiry().isBefore(perm2.getStaticdateofissue())) {
                                statusBundle.appendToStatusLog("INFO: The second, non-expiring permit (ID:" + perm2.getPermitID() + ") was issued AFTER the first, temporary permit's expiry date (ID:" + perm2.getPermitID() + ")");
                            } else {
                                statusBundle.appendToStatusLog("INFO: The second, non-expiring permit (ID:" + perm2.getPermitID() + ") was issued INSIDE the first, temporary permit's expiry date (ID:" + perm2.getPermitID() + "). Nice work.");
                            }
                        } else {
                            // we've got a null date of expiry on an expiring permit!
                            statusBundle.setStatusEnum(OccPeriodStatusEnum.UNKNOWN);
                            statusBundle.appendToStatusLog("*Assigning status: unknown | We've got a null date of expiry on an expiring permit!");

                        }
                        statusBundle.setStatusEnum(OccPeriodStatusEnum.NONEXPPERMIT_ISSUED);
                        statusBundle.appendToStatusLog("*Assigning status: nonexpiring permit issued");
                    }
                    default -> {
                        // more than 2 permits--something's weird
                        statusBundle.setStatusEnum(OccPeriodStatusEnum.SEQUENCE_ANOMALY);
                        statusBundle.appendToStatusLog("More than 2 finalized permits--anomaly");
                        statusBundle.appendToStatusNote("More than 2 finalized permits found; human review required.");
                    }
                }
            } else {
                // no permits so we'll look at inspections
                // INSPECTION EXAMINATION
                if (finListFinalized.isEmpty() && finListInProcess.isEmpty()) {
                    // No inspections finalized or in process
                    statusBundle.setStatusEnum(OccPeriodStatusEnum.PRE_INSPECTION);
                    statusBundle.appendToStatusLog("*No inspections finalized or in process");
                } else if (finListFinalized.isEmpty() && !finListInProcess.isEmpty()) {
                    // No fins finalized but at least one in process
                    statusBundle.setStatusEnum(OccPeriodStatusEnum.INSPECTION_IN_PROCESS);
                    statusBundle.appendToStatusLog("*No inspections finalized but at least one in process");
                } else if (!finListFinalized.isEmpty()) {
                    // we've got at least one finalized inspection, lets explore more
                    statusBundle.appendToStatusLog("*At least one finalized inspection");
                    // get our inspections in Chrono order
                    Collections.sort(finListFinalized);
                    Collections.reverse(finListFinalized);
                    FieldInspectionLight fin = finListFinalized.get(0);
                    statusBundle.appendToStatusLog("*Examining most recent finalized inspection ID:" + fin.getInspectionID());
                    if (fin.getDetermination() != null) { // this is just a good faith check-- we should all have determniation objects
                        if (fin.getDetermination().isQualifiesAsPassed()) {
                            statusBundle.appendToStatusLog("--Qualifies as a PASS");
                            statusBundle.setStatusEnum(OccPeriodStatusEnum.PRE_PERMIT);
                        } else {
                            // ONLY FAILED inspections hit this logic block
                            statusBundle.appendToStatusLog("--Failed inspection");
                            if (determinePeriodStillInsideReinspectionWindow(opdh, fin)) {
                                statusBundle.setStatusEnum(OccPeriodStatusEnum.FAILED_FIN_REINSPECTIONWINDOW_INSIDE);
                            } else {
                                statusBundle.setStatusEnum(OccPeriodStatusEnum.FAILED_FIN_REINSPECTIONWINDOW_EXPIRED);
                            }
                        }
                    }
                }
            }
        } catch (BObStatusException ex) {
            statusBundle.setStatusEnum(OccPeriodStatusEnum.UNKNOWN);
            statusBundle.appendToStatusLog("Exception in status processing logic:");
            statusBundle.appendToStatusLog(ex.getMessage());
        }

        opdh.setStatus(statusBundle);
        return opdh;
    }

    /**
     * Check reinspection window
     *
     * @param opdh
     * @param fin
     * @return
     */
    private boolean determinePeriodStillInsideReinspectionWindow(OccPeriodDataHeavy opdh, FieldInspectionLight fin) throws BObStatusException {
        if (opdh == null || opdh.getPeriodType() == null || fin == null || fin.getEffectiveDateOfRecord() == null) {
            throw new BObStatusException("cannot determine reinspection window for null period, fin, or periodtype");
        }
        return fin.getEffectiveDateOfRecord().plusDays(opdh.getPeriodType().getReinspectionWindowDays()).isAfter(LocalDateTime.now());
    }

    /**
     * Evaluates the given permit for expiry with respect to now()
     *
     * @param permit
     * @return true if the permit is within its validity window, false if
     * expired
     */
    private boolean determineTempPermitStillValid(OccPermit permit) throws BObStatusException {
        if (permit == null || permit.getPermitType() == null) {
            throw new BObStatusException("Cannot evaluate validity of null permit or permit without a type");
        }
        if (!permit.getPermitType().isExpires()) {
            return true;
        } else {
            // our permit expires.
            if (permit.getStaticdateofexpiry() != null) {
                return !permit.getStaticdateofexpiry().isBefore(LocalDate.now());
            } else {
                throw new BObStatusException("Temporary permit contained null static expiry date");
            }
        }
    }

    /**
     * Shell container for holding configuration logic applicable to OccPeriods
     * minus their many lists
     *
     * @param period
     * @param ua not used
     * @return
     * @throws EventException
     * @throws AuthorizationException
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws ViolationException
     */
    private OccPeriod configureOccPeriod(OccPeriod period, UserAuthorized ua)
            throws EventException, AuthorizationException, IntegrationException, BObStatusException, ViolationException, BlobException {

        SystemIntegrator si = getSystemIntegrator();
        EventCoordinator ec = getEventCoordinator();
        PropertyCoordinator pc = getPropertyCoordinator();

        if (period == null || ua == null) {
            throw new BObStatusException("cannot configure with null period or user");
        }
        period.setEventList(ec.getEventList(period));

        // We needed to avoid endless cycles by flattinging out property and unit objects
        // by injecting only their IDs and String names.
        Property p = pc.getProperty(period.getParentParcelKey());
        if (p != null) {
            if (p.getAddress() != null && p.getAddress().getAddressPretty2LineEscapeFalse() != null) {
                period.setPropertyAddressStringEscapeFalse(p.getAddress().getAddressPretty2LineEscapeFalse());
                // if we have a property without an address, just use the county parcel ID
            } else {
                period.setPropertyAddressStringEscapeFalse(p.getCountyParcelID());
            }
            period.setMuni(p.getMuni());
        }

        // if we don't have a type from the DB, inject a generic one
        if (period.getPeriodType() == null) {
            period.setPeriodType(getDefaultOccPeriodType(ua.getMyCredential().getGoverningAuthPeriod().getMuni()));
        }

        // PERMIT LIST
        period.setPermitList(getOccPermitList(period, ua));

        try{
            Collections.sort(period.getPermitList());
        } catch (IllegalArgumentException ex){
            System.out.println("OccupancyCoordinator.configureOccPeriod | sort error in compareTo");
            System.out.println(ex);
            // for debugging only
//            testOccPermitSorting(period.getPermitList());
            
        }

        // Choose a governing permit
        List<OccPermit> plist = period.getPermitsFinalized();
        if (plist != null && !plist.isEmpty()) {
            Collections.sort(plist);
            // sort in ascending order by date, then select the last one
            period.setGoverningOccPermit(plist.get(plist.size() - 1));
            period.setGoverningOccPermitStatus(OccPermitGoverningStatusEnum.GOVERNING_PERMIT_CHOSEN);
        } else {
            period.setGoverningOccPermitStatus(OccPermitGoverningStatusEnum.NO_FINALIZED_PERMIT);
        }
        
        // setup caching
        // this means anytime an object that this period depends on is updated
        // this object can be told to go flush the relevant caches
//        period.setCacheManager(getOccInspectionCacheManager());
        

//        Pinning not yet implemented
//          TODO: Implement pinning
//        period.setPinner(ua);
//        period.setPinned(si.getPinnedStatus(period));
        return period;
    }
    
    
    /**
     * testing contract adherence for property compareTo
     * @param queryProp 
     */
    private void testOccPermitSorting(List<OccPermit> permitList){
        System.out.println("OccupancyCoordinator.testOccPermitSorting");
        if(permitList != null && !permitList.isEmpty()){
            for(OccPermit targetPermit: permitList){
                System.out.println("Target permitID: " + targetPermit.getPermitID());
                for(OccPermit testPermit: permitList){
                    System.out.println("Test permit ID: " + testPermit.getPermitID());
                    
                    System.out.println("OPEN AI TESTS ----------------->");
                     // Reflexivity
                    if (targetPermit.compareTo(targetPermit) != 0) {
                        System.out.println("FAIL Reflexivity failed for: " + targetPermit.getPermitID());
                    }

                    // Symmetry
                    int compareResult1 = targetPermit.compareTo(testPermit);
                    int compareResult2 = testPermit.compareTo(targetPermit);
                    if (Integer.signum(compareResult1) != -Integer.signum(compareResult2)) {
                        System.out.println("FAIL Symmetry failed for: " + targetPermit.getPermitID() + " and " + testPermit.getPermitID());
                    }

                    // Transitivity (simplified for two properties)
                    if (targetPermit.compareTo(testPermit) > 0 && testPermit.compareTo(targetPermit) > 0) {
                        System.out.println("FAIL Transitivity failed for: " + targetPermit.getPermitID() + " and " + testPermit.getPermitID());
                    }

                    // Consistency with equals
                    
                    if ((targetPermit.compareTo(testPermit) == 0) != targetPermit.equals(testPermit)) {
                        System.out.println("FAIL Consistency with equals failed for: " + targetPermit.getPermitID() + " and " + testPermit.getPermitID());
                    }
                    System.out.println("END OPEN AI TESTS <-----------------");

                }
                System.out.println("****************** NEXT TARGET ******************");
            }
        }
    }

    /**
     * Logic intermediary for occ permits
     *
     * @param permit
     * @param ua
     * @return
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public OccPermit configureOccPermit(OccPermit permit, UserAuthorized ua) throws BObStatusException, IntegrationException {
        if (permit == null) {
            throw new BObStatusException("Cannot configure occ permit with null permit ");
        }
        if (ua == null) {
            throw new BObStatusException("Cannot configure occ permit with null ua");
        }
        EventCoordinator ec = getEventCoordinator();
        PersonCoordinator pc = getPersonCoordinator();

        // set our human links based on our ID list
        // first load up our human links
        permit.sethumanLinkList(pc.getHumanLinkList(permit));

        configureOccPermitDynamicPersonLinks(permit);

        //added by wwalk 12.26.22 for reportOccPermit.xhtml
        // then updated by Ellen Bascomb of Apartment 31Y on 3FEB2022 to get logic out of the constructor
        if (permit.getStaticpropertyinfo() != null) {
            permit.setStaticpropertyinfo(permit.getStaticpropertyinfo().replace("<br>", "<br><br>"));
        }

        // legacy owner/seller and buyer/tenant label compatibility
        if (permit.getStaticownersellerlabel() == null) {
            permit.setStaticownersellerlabel(PERMIT_LEGACY_SELLEROWNER_LABEL);
        }
        if (permit.getStaticbuyertenantlabel() == null) {
            permit.setStaticbuyertenantlabel(PERMIT_LEGACY_BUYERTENANT_LABEL);
        }
        if (permit.getStaticpropertyinfo() == null) {
            permit.setStaticpropertyinfo("no property info");
        }

        ec.configureEventLinkedLinkedEventList(permit);
        
        // adjust reference number to reflect amendments
        // which DOES NOT change the DB reference number, it only appends an -AMENDED
        // if the permit has been amended
        if(permit.getFinalizedts() != null && permit.getReferenceNo() != null & permit.getAmendementCommitTS() != null){
            String amendedRefNo = permit.getReferenceNo() + PERMIT_AMENDMENT_SUFFIX;
            permit.setReferenceNo(amendedRefNo);
        }
        // and we follow the same pattern for revocation
        if(permit.getFinalizedts() != null && permit.getReferenceNo() != null & permit.getRevokedTS() != null){
            String revokedRefNo = permit.getReferenceNo() + PERMIT_REVOCATION_SUFFIX;
            permit.setReferenceNo(revokedRefNo);
        }

        
        return injectEmptyLists(permit);

    }

    /**
     *
     * @param permit cannot be null
     * @return the passed in permit with the links set
     */
    private OccPermit configureOccPermitDynamicPersonLinks(OccPermit permit) throws IntegrationException, BObStatusException {
        PersonCoordinator pc = getPersonCoordinator();
        permit.setOwnerSellerLinkList(pc.getHumanOccPermitLinkList(permit.getOwnerSellerLinkIDList()));
        permit.setBuyerTenantLinkList(pc.getHumanOccPermitLinkList(permit.getBuyerTenantLinkIDList()));
        permit.setManagerLinkList(pc.getHumanOccPermitLinkList(permit.getManagerLinkIDList()));
        permit.setTenantLinkList(pc.getHumanOccPermitLinkList(permit.getTenantLinkIDList()));
        return permit;
    }

    /**
     * Injects empty lists in list members that didn't get data during config
     *
     * @param permit
     * @return
     */
    private OccPermit injectEmptyLists(OccPermit permit) {

        if (permit.getOwnerSellerLinkList() == null) {
            permit.setOwnerSellerLinkList(new ArrayList<>());
        }
        if (permit.getBuyerTenantLinkList() == null) {
            permit.setBuyerTenantLinkList(new ArrayList<>());
        }
        if (permit.getManagerLinkList() == null) {
            permit.setManagerLinkList(new ArrayList<>());
        }
        if (permit.getTenantLinkList() == null) {
            permit.setTenantLinkList(new ArrayList<>());
        }
        if (permit.getTextBlocks_stipulations() == null) {
            permit.setTextBlocks_stipulations(new ArrayList<>());
        }
        if (permit.getTextBlocks_notice() == null) {
            permit.setTextBlocks_notice(new ArrayList<>());
        }
        if (permit.getTextBlocks_comments() == null) {
            permit.setTextBlocks_comments(new ArrayList<>());
        }
        return permit;

    }

    /**
     * Generator method for creating a new occ period skeleton for creation
     * origination
     *
     * @param pu
     * @param ua the current user, and if they have an officer swearing then
     * they'll become the unit manager
     * @return
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public OccPeriod getOccPeriodSkeleton(PropertyUnit pu, UserAuthorized ua) throws BObStatusException {
        if (ua == null) {
            throw new BObStatusException("Cannot make occ period skeleton with null user");

        }
        OccPeriod per = new OccPeriod();
        if (pu != null) {
            per.setPropertyUnitID(pu.getUnitID());
        }
        per.setManager(ua);

        return per;
    }

    /**
     * Coordinates with the integrator to build an occ period history list
     *
     * @param ua
     * @param cred
     * @return
     */
    public List<OccPeriod> assembleOccPeriodHistoryList(UserAuthorized ua) {
        OccupancyIntegrator oi = getOccupancyIntegrator();

        List<OccPeriod> opList = new ArrayList<>();
        try {
            for (Integer i : oi.getOccPeriodHistoryList(ua.getKeyCard().getGoverningAuthPeriod().getUserID())) {
                opList.add(getOccPeriod(i, ua));
            }
        } catch (IntegrationException | BlobException ex) {
            System.out.println(ex);
        }

        return opList;

    }

    /**
     * TODO: Finish
     *
     * @param period
     */
    public void configureRuleSet(OccPeriodDataHeavy period) {
//        List<EventRuleImplementation> evRuleList = period.getb(ViewOptionsEventRulesEnum.VIEW_ALL);
//        for(EventRuleAbstract era: evRuleList){
//            if(era.getPromptingDirective()!= null){
//                // TODO: Finish
//            }
//        }
    }

    public List<EventType> getPermittedEventTypes(OccPeriod op, UserAuthorized u) {
        List<EventType> typeList = new ArrayList<>();
        int rnk = u.getRole().getRank();

        if (rnk >= MINIMUM_RANK_INSPECTOREVENTS) {
            typeList.add(EventType.Action);
            typeList.add(EventType.Timeline);
        }

        if (rnk >= MINIMUM_RANK_STAFFEVENTS) {
            typeList.add(EventType.Communication);
            typeList.add(EventType.Meeting);
            typeList.add(EventType.Custom);
            typeList.add(EventType.Occupancy);
        }

        return typeList;
    }

    public void updateOccPeriodPropUnit(OccPeriod period, PropertyUnit pu) throws IntegrationException {
        OccupancyIntegrator oi = getOccupancyIntegrator();
        period.setPropertyUnitID(pu.getUnitID());
        oi.updateOccPeriod(period);
        getOccupancyCacheManager().flush(period);
    }

    /**
     * Logic container for checking the basic permissions for authorization of
     * an occupancy period. An authorized occupancy period is one for which an
     * occupancy permit can be issued
     *
     * @param period
     * @param u doing the authorizing; must have code officer permissions
     * @throws AuthorizationException
     * @throws BObStatusException
     * @throws IntegrationException
     */
    public void toggleOccPeriodAuthorization(OccPeriod period, UserAuthorized u) throws AuthorizationException, BObStatusException, IntegrationException {
        OccupancyIntegrator oi = getOccupancyIntegrator();
        if (u.getKeyCard().isHasEnfOfficialPermissions()) {
            // TODO: Figure out occupancy period status and authorization permission

            // If it is not currently authorized
            if (period.getAuthorizedBy() == null) {
                // And it is ready for authorization
                if (period.getEndDateCertifiedBy() != null
                        && period.getStartDateCertifiedBy() != null
                        && period.getPeriodTypeCertifiedBy() != null) {
                    // Authorize it!
                    period.setAuthorizedBy(u);
                    period.setAuthorizedTS(LocalDateTime.now());
                    oi.updateOccPeriod(period);
                    getOccupancyCacheManager().flush(period);
                } else {
                    throw new BObStatusException("Occupancy period is not ready for authorization");
                }
            } else {
                getOccupancyCacheManager().flush(period);
                // If it's already authorized, toggle it back to unauthorized
                period.setAuthorizedBy(null);
                period.setAuthorizedTS(null);
                oi.updateOccPeriod(period);
            }
        } else {
            throw new AuthorizationException("Users must have enforcement official permissions to authorize or deauthorize an occupancy period");
        }
        getOccupancyCacheManager().flush(period);
    }

    /**
     * An occ permit report is the container that prints an occ permit, it's not
     * a report about occ permits, for that see reportOccupancyList
     *
     * @param permit
     * @param period
     * @param propUnit
     * @param u
     * @return with sensible defaults
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public ReportConfigOccPermit getOccPermitReportConfigDefault(OccPermit permit,
            OccPeriod period,
            PropertyUnit propUnit,
            User u) throws IntegrationException, BObStatusException {
        PropertyCoordinator pc = getPropertyCoordinator();
        ReportConfigOccPermit rpt = new ReportConfigOccPermit();
        rpt.setTitle(getResourceBundle(Constants.MESSAGE_TEXT).getString("report_occpermit_default_title"));

        rpt.setPermit(permit);
        rpt.setPeriod(period);
        try {
            rpt.setPropUnitWithProp(pc.getPropertyUnitWithProp(propUnit.getUnitID()));
        } catch (AuthorizationException ex) {
            throw new BObStatusException(ex.getMessage());
            
        }
        rpt.setCreator(u);

        return rpt;
    }

    /**
     * Logic check for occ period deactivation
     *
     * @param opdh
     * @param ua
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void deactivateOccPeriod(OccPeriodDataHeavy opdh, UserAuthorized ua) throws BObStatusException, AuthorizationException, IntegrationException {
        OccupancyIntegrator oi = getOccupancyIntegrator();
        if (opdh == null || ua == null) {
            throw new BObStatusException("Cannot deac with null period or umap");
        }
        // users can deac their own permit files, but only manager or better can deac 
        // periods that aren't theirs
        if (!ua.getKeyCard().isHasSysAdminPermissions()) { // sys admins bypass all checks
            if (opdh.getManager() != null) {
                if (opdh.getManager().getUserID() != ua.getUserID()) {
                    throw new AuthorizationException("Users can only deactivate their own permit files or must be manager or better rank");
                }
            }
            // check for finalized permits
            if (opdh.getPermitList() != null && !opdh.getPermitList().isEmpty()) {
                for (OccPermit op : opdh.getPermitList()) {
                    if (op.getFinalizedts() != null) {
                        throw new AuthorizationException("No user can deactivate a permit file containing a finalized permit;");
                    }
                }
            }
            // check for finalized inspections
            if (opdh.getInspectionList() != null && !opdh.getInspectionList().isEmpty()) {
                for (FieldInspectionLight fin : opdh.getInspectionList()) {
                    if (fin.getDeterminationTS() != null) {
                        throw new AuthorizationException("No user can deactivate a permit file containing a finalized field inspection");
                    }
                }
            }
        }
        // we're okay to deac
        opdh.setDeactivatedBy(ua);
        opdh.setDeactivatedTS(LocalDateTime.now());
        oi.deactivateOccPeriod(opdh);
        getOccupancyCacheManager().flush(opdh);

    }

    // *************************************************************************
    // ********************* OCC PERMITS & Occ Reporting   *********************
    // *************************************************************************
    static final int DAYS_IN_MONTH = 30;
    static final String OCC_PERMIT_ACTIVITY_REPORT_TITLE = "Occupancy Permitting and Inspection Activity Report";

    /**
     * Prepares a report configuration object for occupancy activity
     *
     * @param ua
     * @return
     */
    public ReportConfigOccActivity getOccPermitActivityReportSkeleton(UserAuthorized ua) {
        ReportConfigOccActivity rptConfig = new ReportConfigOccActivity();

        rptConfig.setDate_start_val(LocalDateTime.now().minusDays(DAYS_IN_MONTH));
        rptConfig.setDate_end_val(LocalDateTime.now());

        rptConfig.setTitle(OCC_PERMIT_ACTIVITY_REPORT_TITLE);

        rptConfig.setCreator(ua);
        rptConfig.setMuni(ua.getKeyCard().getGoverningAuthPeriod().getMuni());

        rptConfig.setIncludeInspectionCountForPermits(true);

        // set default switch settings
        rptConfig.setDetailPermits(true);
        rptConfig.setIncludePermitApplicationAndInspectionDates(true);
        rptConfig.setIncludePermitIssingOfficer(true);
        rptConfig.setIncludePermitNumber(true);

        rptConfig.setOccApplicationCountFreeEntry(0);
        rptConfig.setShowOccApplicationCountFreeEntry(false);

        // Inspection switches
        // Tally methods switches
        rptConfig.setActivateSectionFieldInspections(true);
        rptConfig.setDetailFieldInspections(true);

        // filters inside
        rptConfig.setCountCECaseInspections(false);
        rptConfig.setCountOccPeriodInspections(true);
        // contingent on detailFieldInspections
        rptConfig.setShowInspectionMetaData(true);
        rptConfig.setShowInspectionFailedItems(true);
        rptConfig.setShowInspectionFailedItemsFindings(false);

        // violations
        rptConfig.setActivateTransferredViolationCount(true);
        rptConfig.setDetailMigratedViolations(true);

        // not yet supported
        rptConfig.setIncludeInspectionsOutsideDateRangeForIssuedPermits(false);
        rptConfig.setShowInspectionsBackingPermits(true);

        return rptConfig;
    }

    /**
     * Takes in a report config object and actually runs the queries and
     * assembles the report information for display in a printable page.This is
     * a computationally HEAVY method that might take many seconds to run for a
     * lot of permits
     *
     *
     * @param config
     * @param ua
     * @return
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BlobException
     */
    public ReportConfigOccActivity buildOccActivityReport(ReportConfigOccActivity config, UserAuthorized ua) throws BObStatusException, IntegrationException, BlobException, SearchException {
        if (config == null || ua == null) {
            throw new BObStatusException("Cannot build report with null config or user");
        }

        reportOccActivityPopulatePermitData(config, ua);
        reportOccActivityPopulateFailedInspectionData(config, ua);
        if (config.isActivateTransferredViolationCount()) {
            reportOccActivityPopulateViolationMigrations(config, ua);
        }

        config.setGenerationTimestamp(LocalDateTime.now());

        return config;

    }

    /**
     * component in occ activity report: violations!
     *
     * @param config
     * @param ua
     * @return
     * @throws BObStatusException
     * @throws IntegrationException
     */
    private ReportConfigOccActivity reportOccActivityPopulateViolationMigrations(ReportConfigOccActivity config, UserAuthorized ua) throws BObStatusException, IntegrationException, SearchException {
        SearchCoordinator sc = getSearchCoordinator();

        QueryCodeViolation queryViols = sc.initQuery(QueryCodeViolationEnum.TRANSFERRRED, ua.getKeyCard());
        queryViols.getPrimaryParams().setDate_startEnd_ctl(true);
        queryViols.getPrimaryParams().setDate_relativeDates_ctl(false);
        queryViols.getPrimaryParams().setDate_start_val(config.getDate_start_val());
        queryViols.getPrimaryParams().setDate_end_val(config.getDate_end_val());

        config.setMigratedViolations(sc.runQuery(queryViols).getResults());
        return config;
    }

    /**
     * Component in the occ activity report sequence
     *
     * @param config NOT NULL
     * @param ua NOT NULL
     * @return the given config
     * @throws BObStatusException
     */
    private ReportConfigOccActivity reportOccActivityPopulatePermitData(ReportConfigOccActivity config, UserAuthorized ua) throws BObStatusException, IntegrationException, BlobException {

        SearchCoordinator sc = getSearchCoordinator();

        QueryOccPermit queryPermits = sc.initQuery(QueryOccPermitEnum.ALL_FINALIZED, ua.getKeyCard());
        queryPermits.setRequestingUser(ua);
        SearchParamsOccPermit param = queryPermits.getPrimaryParams();
        param.setDate_startEnd_ctl(true);
        param.setDate_field(SearchParamsOccPermitDateFieldsEnum.STATICDATEOFISSUE);
        param.setDate_start_val(config.getDate_start_val());
        param.setDate_end_val(config.getDate_end_val());

        try {
            queryPermits = sc.runQuery(queryPermits);
        } catch (SearchException ex) {
            throw new BObStatusException("Could not search for permits");
        }
        config.setPermitListRaw(queryPermits.getBOBResultList());

        List<OccPeriodDataHeavy> periodHeavyList = new ArrayList<>();
        if (config.getPermitListRaw() != null && !config.getPermitListRaw().isEmpty()) {
            // Build me a list of occ period data heavyies for each of our finalized permits in this report period
            for (OccPermitPropUnitHeavy permit : config.getPermitListRaw()) {
                OccPeriodDataHeavy opdh = assembleOccPeriodDataHeavy(getOccPeriod(permit.getPeriodID(), ua), ua);
                periodHeavyList.add(opdh);

            }
        }
        config.setPeriodListRaw(periodHeavyList);
        buildPermitTypePermitMap(config);
        return config;

    }

    /**
     * Builds a mapping of permit types to a list of permits of that type for
     * reporting purposes
     *
     * @param config
     * @return the same passed in config, with at least an empty mapping
     */
    private ReportConfigOccActivity buildPermitTypePermitMap(ReportConfigOccActivity config) throws BObStatusException {
        if (config == null || config.getPermitListRaw() == null) {
            throw new BObStatusException("Cannot build map with null config or null permitListRaw in config");
        }
        Map<OccPermitType, List<OccPeriodDataHeavy>> typeMap = new HashMap<>();

        if (!config.getPeriodListRaw().isEmpty()) {
            for (OccPeriodDataHeavy period : config.getPeriodListRaw()) {
                if (period.getGoverningOccPermit() == null) {
                    System.out.println("OccupancyCoordinator.buildPermitTypePermitMap | skipping permit/period | null governing permit on period ID " + period.getPeriodID());
                    continue;
                }
                // If we have a key of this type, add our current permit, otherwise write the pair
                if (typeMap.containsKey(period.getGoverningOccPermit().getPermitType())) {
                    typeMap.get(period.getGoverningOccPermit().getPermitType()).add(period);
                } else {
                    List<OccPeriodDataHeavy> typeSpecificPeriodList = new ArrayList<>();
                    typeSpecificPeriodList.add(period);
                    typeMap.put(period.getGoverningOccPermit().getPermitType(), typeSpecificPeriodList);
                }
            }
            // now flatten our map into a list for display
            Set<OccPermitType> typeSet = typeMap.keySet();
            List<ReportConfigOccActivityPermitTypeListCont> containerList = new ArrayList<>();
            for (OccPermitType opt : typeSet) {
                List<OccPeriodDataHeavy> opdhlist = typeMap.get(opt);
                ReportConfigOccActivityPermitTypeListCont cont = new ReportConfigOccActivityPermitTypeListCont(opt, opdhlist);
                // count reinspections vs initials
                if (config.isIncludeInspectionCountForPermits()) {
                    for (OccPeriodDataHeavy perdh : opdhlist) {
                        List<FieldInspectionLight> periodFinList = perdh.getInspectionListFinalized();
                        for (FieldInspectionLight fin : periodFinList) {
                            if (fin.getFollowUpToInspectionID() == 0) {
                                cont.getFinInitialList().add(fin);
                            } else {
                                cont.getFinReinspectionList().add(fin);
                            }
                        }
                    }
                }
                containerList.add(cont);
            }
            config.setPermitTypeListContainerList(containerList);
        }
        config.setPermitTypeListMap(typeMap);
        return config;
    }

    static final String LIST_TITLE_OCC = "Occupancy Inspections";
    static final String LIST_TITLE_CE = "Code enforcement inspections";

    /**
     * Completes the failed field inspection component of the occ activity
     * report
     *
     * @param config NOT NULL
     * @return
     */
    private ReportConfigOccActivity reportOccActivityPopulateFailedInspectionData(ReportConfigOccActivity config, UserAuthorized ua) throws SearchException, IntegrationException, BObStatusException, BlobException {

        SearchCoordinator sc = getSearchCoordinator();
        OccInspectionCoordinator oic = getOccInspectionCoordinator();
        List<ReportConfigOccActivityFinListCont> containerList = new ArrayList<>();

        if (config.isCountOccPeriodInspections()) {
            QueryFieldInspection finOccFailQuery = sc.initQuery(QueryFieldInspectionEnum.OCC_REPORT_FIN_PASSORFAIL, ua.getKeyCard());
            finOccFailQuery.getPrimaryParams().setDate_start_val(config.getDate_start_val());
            finOccFailQuery.getPrimaryParams().setDate_end_val(config.getDate_end_val());
            finOccFailQuery.getPrimaryParams().setDt_qualifies_val(false);
            finOccFailQuery.getPrimaryParams().setDomain_val(EventRealm.OCCUPANCY);
            config.setFinListOccFail(oic.getFieldInspectionDataHeavyList(sc.runQuery(finOccFailQuery).getBOBResultList()));
            containerList.add(new ReportConfigOccActivityFinListCont(LIST_TITLE_OCC, config.getFinListOccFail()));
            
            // get passed
            QueryFieldInspection finOccPassQuery = sc.initQuery(QueryFieldInspectionEnum.OCC_REPORT_FIN_PASSORFAIL, ua.getKeyCard());
            finOccPassQuery.getPrimaryParams().setDate_start_val(config.getDate_start_val());
            finOccPassQuery.getPrimaryParams().setDate_end_val(config.getDate_end_val());
            finOccPassQuery.getPrimaryParams().setDt_qualifies_val(true);
            finOccPassQuery.getPrimaryParams().setDomain_val(EventRealm.OCCUPANCY);
            config.setFinListOccPass(oic.getFieldInspectionDataHeavyList(sc.runQuery(finOccPassQuery).getBOBResultList()));
            if(config.getFinListOccFail() != null && config.getFinListOccPass() != null){
                config.setFinListOccPassFailTotal(config.getFinListOccFail().size() + config.getFinListOccPass().size());
            } else {
                config.setFinListOccPassFailTotal(0);
            }
        }

        if (config.isCountCECaseInspections()) {
            QueryFieldInspection finQuery = sc.initQuery(QueryFieldInspectionEnum.OCC_REPORT_FIN_PASSORFAIL, ua.getKeyCard());
            finQuery.getPrimaryParams().setDate_start_val(config.getDate_start_val());
            finQuery.getPrimaryParams().setDate_end_val(config.getDate_end_val());
            finQuery.getPrimaryParams().setDomain_val(EventRealm.CODE_ENFORCEMENT);
            config.setFinListCE(oic.getFieldInspectionDataHeavyList(sc.runQuery(finQuery).getBOBResultList()));
            containerList.add(new ReportConfigOccActivityFinListCont(LIST_TITLE_CE, config.getFinListCE()));
        }
        config.setFinListContainerList(containerList);

        return config;

    }

    /**
     * Factory for OccPermits
     *
     * @param ua
     * @param mdh
     * @return
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public OccPermit getOccPermitSkeleton(UserAuthorized ua, MunicipalityDataHeavy mdh) throws AuthorizationException, AuthorizationException {
        OccPermit permit = new OccPermit();
        if (!permissionsCheckpointOccPermitDraft(ua)) {
            throw new AuthorizationException("Authorization denied to draft an occupancy permit");
        }

        permit.setIssuingOfficer(ua);
        permit.setReferenceNo("[generated at finalization]");
        permit.setCreatedBy(ua);
        return permit;
    }

    /**
     * Logic organ for taking in a bunch of objects and making sensible
     * assignments to dynamic fields for the code officer to review before
     * permit finalization.This method will explain the results of its check by
     * writing HTML to the configurationLog member on OccPermit for the user to
     * review.
     *
     * As of July 2024 this method will set pass/fail switch for inspection
     * pass requirement
     *
     * @param permit cannot be null, cannot be finalized or nullified, must have a type in its belly
     * @param per null is okay
     * @param ua cannot be null
     * @param pdh null is okay
     * @param mdh
     * @return a reference to the passed in permit with sensible dynamic values
     * applied based on incoming objects.
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public OccPermit occPermitAssignSensibleDynamicValuesAndAudit(OccPermit permit,
            OccPeriodDataHeavy per,
            UserAuthorized ua,
            PropertyDataHeavy pdh,
            MunicipalityDataHeavy mdh) throws BObStatusException {
        if (permit == null || ua == null) {
            throw new BObStatusException("OccupancyCoordinator.occPermitAssignSensibleDynamicValuesAndAudit | Cannot assign sensible dynamic values with null permit or user");
        }
        boolean pass = true;
        permit.clearDynamicPopulationLog();
        permit.setIssuingOfficer(ua);

        // setup our property fields
        permit.setParcelInfo(pdh.getParcelInfo());

        // set our issuing code source proposal list
        if (mdh != null && mdh.getIssuingCodeSourceList() != null && permit.getIssuingCodeSourceList() != null) {
            permit.getIssuingCodeSourceList().addAll(mdh.getIssuingCodeSourceList());
        }

        List<FieldInspectionLight> finList = per.getInspectionList();
        // if we don't have any inspections, log and deny permit creation
        if (finList == null || finList.isEmpty()) {
            permit.appendToDynamicPopulationLog("Inspections: [Fatal] Occ Period contains zero field inspections");
            permit.appendToDynamicPopulationLog(Constants.FMT_HTML_BREAK);
            pass = false;
            // okay, we've got at least one field inspection , so check if it's finalized
        } else {
            permit.appendToDynamicPopulationLog("Inspections: [Info] Occ Period contains at least 1 field inspection");
            permit.appendToDynamicPopulationLog(Constants.FMT_HTML_BREAK);
            // get finalized inspections
            List<FieldInspectionLight> finalizedFINs = new ArrayList<>();
            for (FieldInspectionLight fin : finList) {
                if (fin.getDetermination() != null) {
                    finalizedFINs.add(fin);
                }
            }
            // if we have no finalized FINs, log and deny
            if (finalizedFINs.isEmpty()) {
                permit.appendToDynamicPopulationLog("Inspections: [FATAL] Occ period contains zero finalized inspections. Review your inspections and finalize them and try again");
                pass = false;
            } else {
                permit.appendToDynamicPopulationLog("Inspections: [Info] Occ Period contains at least 1 finalized field inspection");
                permit.appendToDynamicPopulationLog(Constants.FMT_HTML_BREAK);
                // get newest to oldest.
                // we still need 
                Collections.sort(finalizedFINs);
                Collections.reverse(finalizedFINs);
                // get our Final inspection
                FieldInspectionLight inspectionFinal = finalizedFINs.get(0);
                // The only one of the three inspection dates needs to come from
                // an inspection whose finalization determination qualifies as passing

                if (inspectionFinal.getDetermination().isQualifiesAsPassed()) {
                    permit.setDynamicFinalInspectionFINRef(inspectionFinal);
                    permit.setDynamicfinalinspection(LocalDate.from(inspectionFinal.getEffectiveDateOfRecord()));

                } else {
                    // TODO: What kind of checks do we want here?
                    permit.appendToDynamicPopulationLog("Inspections: [FATAL] Most recent field inspection has a determination that does not qualify as a passed inspection: ");

                    pass = false;
                }
                // Get the initial inspection
                FieldInspectionLight inspectionInitial = finList.get(finalizedFINs.size() - 1);
                permit.appendToDynamicPopulationLog("Inspections: [Info] Initial inspection assigned with ID " + inspectionInitial.getInspectionID());
                permit.appendToDynamicPopulationLog(Constants.FMT_HTML_BREAK);
                permit.setDynamicInitialInspectionFINRef(inspectionInitial);
                permit.setDynamicInitialInspection(LocalDate.from(inspectionInitial.getEffectiveDateOfRecord()));

                // We need a reinspection date, which if there are three or more 
                // is our second to last
                int inspectionCount = finList.size();
                FieldInspectionLight inspeectionReinspetion = null;
                switch (inspectionCount) {
                    case 0:
                        permit.appendToDynamicPopulationLog("Inspections: [FATAL] Inspection assignment logic error OPFIN1: --zero inspection count impossible in count case 0");
                        break;
                    case 1:
                        inspeectionReinspetion = null;
                        permit.appendToDynamicPopulationLog("Inspections: [INFO] Reinspection assigned same inspection as Final Inspection (ID:" + inspectionFinal.getInspectionID() + ")");
                        break;
                    case 2:
                        inspeectionReinspetion = inspectionFinal;
                        permit.appendToDynamicPopulationLog("Inspections: [INFO] Reinspection assigned same inspection as Initial Inspection (ID:" + inspectionInitial.getInspectionID() + ")");
                        break;
                    case 3:
                    // >3: no break--fall through to default
                    default:
                        // Commented out on 7 August 2023 to deal with weird permit finalization bug
                        // from JD--this exception led to what appeared to be a permissions lock out on permit finalization
                        // the error was an index out of range on this next commented out line: 
                        // Index 1 out of bounds for length 1 
                        // which doesn't make sense to have hit down here in the default case instead of case 1
                        //inspeectionReinspetion = finalizedFINs.get(1); // get second most recent inspection and call it the reinspection
                        permit.appendToDynamicPopulationLog("Field inspection finalization default case activation: Manual inspection configuration required");
                        System.out.println("OccupancyCoordinator.occPermitAssignSensibleDynamicValuesAndAudit | DEFAULT case");
                }
                // inject into the permit
                permit.setDynamicReInspectionFINRef(inspeectionReinspetion);
                if (inspeectionReinspetion != null) {
                    permit.setDynamicreinspectiondate(LocalDate.from(inspeectionReinspetion.getEffectiveDateOfRecord()));
                } else {
                    permit.appendToDynamicPopulationLog("Inspections: [INFO] No reinspection assigned, probably because there was only one finalized inspection");

                }
            }
        } // done setting inspeections and their dates

        // configure date of issuance to today
        permit.setDynamicdateofissue(LocalDate.now());
        // Configure expiry date on types that expire
        if (permit.getPermitType() != null && permit.getPermitType().isExpires()) {
            if (permit.getPermitType().getDefaultValidityPeriodDays() == 0) {
                permit.setDynamicDateExpiry(LocalDate.from(LocalDateTime.now().plusDays(DEFAULT_PERMIT_EXPIRY_WINDOW_DAYS)));
            } else {
                permit.setDynamicDateExpiry(LocalDate.from(LocalDateTime.now().plusDays(permit.getPermitType().getDefaultValidityPeriodDays())));
            }
        }
        if (permit.getIssuingOfficer() == null) {
            permit.appendToDynamicPopulationLog("Issuing officer: Cannot issue permit without an issuing officer");
        }
        configurePermitPersonColumnHeaders(permit);
        
        // setup our default text blocks
        if(permit.getPermitType() != null){
            OccPermitType ptype = permit.getPermitType();
            
            // stipulations
            if(permit.getTextBlocks_stipulations() == null){
                permit.setTextBlocks_stipulations(new ArrayList<>());
            }
            if(ptype.getPreaddTextBlocksStipulations() != null && !ptype.getPreaddTextBlocksStipulations().isEmpty()){
                permit.getTextBlocks_stipulations().addAll(ptype.getPreaddTextBlocksStipulations());
            }
            // notices
            if(permit.getTextBlocks_notice()== null){
                permit.setTextBlocks_notice(new ArrayList<>());
            }
            if(ptype.getPreaddTextBlocksNotices() != null && !ptype.getPreaddTextBlocksNotices().isEmpty()){
                permit.getTextBlocks_notice().addAll(ptype.getPreaddTextBlocksNotices());
            }
            // Comments
            if(permit.getTextBlocks_comments() == null){
                permit.setTextBlocks_comments(new ArrayList<>());
            }
            if(ptype.getPreaddTextBlocksComments() != null && !ptype.getPreaddTextBlocksComments().isEmpty()){
                permit.getTextBlocks_comments().addAll(ptype.getPreaddTextBlocksComments());
            }
            
        }
        
        // Hold off on persons here;
        // If nobody nixed our pass, then timestamp the dynamic fields
        if (pass) {
            permit.setDynamicPopulationReadyForFinalizationTS(LocalDateTime.now());
        }
        return permit;
    }
    
    
    /**
     * Listener for users to update only the default text blocks on the given occ
     * permit type.
     * @param permit this method will extract the text blocks from the given permit 
     * and inject them into this permit's type and then send that type to the integrator
     * @throws IntegrationException 
     */
    public void updateOccPermitTypeAutoAddTextBlocks(OccPermit permit) throws IntegrationException, BObStatusException{
        OccupancyIntegrator oi = getOccupancyIntegrator();
        if(permit == null || permit.getPermitType() == null){
            throw new BObStatusException("Cannot update occ permit type default text blocks with null permit input");
        }
        OccPermitType ptype = getOccPermitTypeSkeleton(null);
        
        ptype.setPreaddTextBlocksStipulations(permit.getTextBlocks_stipulations());
        ptype.setPreaddTextBlocksNotices(permit.getTextBlocks_notice());
        ptype.setPreaddTextBlocksComments(permit.getTextBlocks_comments());
        
        oi.updateOccPermitTypeDefaultTextBlocks(ptype);
        // no cache dumping required here
    }

    /**
     * Getter for occupancy permits
     *
     * @param permitID
     * @param ua
     * @return
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public OccPermit getOccPermit(int permitID, UserAuthorized ua) throws IntegrationException, BObStatusException, BlobException {
        OccupancyIntegrator oi = getOccupancyIntegrator();
        if (permitID == 0) {
            throw new BObStatusException("OccupancyCoordinator.getOccPermit | Cannot fetch a permit with ID 0");
        }
        return configureOccPermit(oi.getOccPermit(permitID), ua);
    }
    
    /**
     * Figures out the given permit's parent muni used for person link configuration
     * @param permit
     * @param ua
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BlobException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public Municipality getMuniParentOfOccPermit(OccPermit permit, UserAuthorized ua) throws IntegrationException, BlobException, BObStatusException{ 
        PropertyCoordinator pc = getPropertyCoordinator();
        if(permit != null){
            OccPeriod op = getOccPeriod(permit.getPeriodID(), ua);
            try {
                return pc.getProperty(op.getParentParcelKey()).getMuni();
            } catch (AuthorizationException ex) {
                throw new BObStatusException(ex.getMessage());
            }
        }
        return null;
    }
    

    /**
     * Manufactures an occupancy permit that contains the host propertyunit and
     * all their inner goodies for display of Permits in a search result that
     * span parcels
     *
     * @param permitLight
     * @param ua
     * @return
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public OccPermitPropUnitHeavy getOccPermitPropertyUnitHeavy(OccPermit permitLight, UserAuthorized ua) throws IntegrationException, BObStatusException, BlobException {
        if (permitLight == null) {
            return null;
        }

        PropertyCoordinator pc = getPropertyCoordinator();
        OccupancyCoordinator oc = getOccupancyCoordinator();

        OccPermitPropUnitHeavy oppuh = new OccPermitPropUnitHeavy(permitLight);
        try {
            oppuh.setPropUnitWithProp(pc.getPropertyUnitWithProp(oc.getOccPeriod(permitLight.getPeriodID(), ua).getPropertyUnitID()));
        } catch (AuthorizationException ex) {
            throw new BObStatusException(ex.getMessage());
        }

        return oppuh;
    }

    /**
     * The official retrieval point for all permits NOT in draft stage by muni,
     * which means issued, nullified, AND--if a logic error occurs--DEACTIVATED
     * issued or issued/nullified
     *
     * @param muni
     * @return
     */
    public List<OccPermitPropUnitHeavy> getOccPermitDocket(Municipality muni) {
        PropertyCoordinator pc = getPropertyCoordinator();
        List<OccPermitPropUnitHeavy> plist = new ArrayList<>();
        return plist;
    }

    /**
     *
     * @param op
     * @param ua
     * @return
     * @throws BObStatusException
     */
    public List<OccPermit> getOccPermitList(OccPeriod op, UserAuthorized ua) throws BObStatusException, IntegrationException, BlobException {

        if (op == null || ua == null) {
            throw new BObStatusException("OccupancyCoordinator.getOccPermitList | Cannot get occ permit list with null occ period or user");
        }
        if (op.getPeriodID() == 0) {
            throw new BObStatusException("OccupancyCoordinator.getOccPermitList | Cannot get occ permit list with occ period whose ID == 0");
        }
        OccupancyIntegrator oi = getOccupancyIntegrator();
        List<Integer> opermitIDList = oi.getOccPermitIDList(op);

        List<OccPermit> occPermitList = new ArrayList<>();
        if (opermitIDList != null && !opermitIDList.isEmpty()) {
            for (Integer i : opermitIDList) {
                occPermitList.add(getOccPermit(i, ua));
            }
        }
        return occPermitList;
    }

    /**
     * insertion pathway for occ permits, which are just drafts until auditing
     * and finalization
     *
     * @param permit
     * @param per
     * @param ua
     * @return
     * @throws BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public int insertOccPermit(OccPermit permit, OccPeriod per, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException {
        if (permit == null || ua == null || per == null) {
            throw new BObStatusException("Cannot insert occ permit with null permit or user or period");
        }
        if (!permissionsCheckpointOccPermitDraft(ua)) {
            throw new AuthorizationException("Permission Denied: User forbidden from inserting occ permit drafts");
        }
        OccupancyIntegrator oi = getOccupancyIntegrator();
        permit.setPeriodID(per.getPeriodID());
        permit.setCreatedBy(ua);
        permit.setLastUpdatedBy(ua);
        int freshID = oi.insertOccPermit(permit);
        getOccupancyCacheManager().flush(per);
        return  freshID;

    }

    /**
     * Updates the meta data of an occ permit only. Use separate method for
     * static fields
     *
     * @param permit
     * @param ua
     * @throws BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void updateOccPermit(OccPermit permit, UserAuthorized ua) throws BObStatusException, IntegrationException {
        if (permit == null || ua == null) {
            throw new BObStatusException("Cannot update  occ permit with null permit or user");
        }
        if (permit.getFinalizedts() != null) {
            throw new BObStatusException("Cannot update an already finalized occ permit");
        }
        if (permit.getNullifiedTS() != null) {
            throw new BObStatusException("OccupancyCoordinator.updateOccPermit: Cannot update a nullified permit");
        }

        OccupancyIntegrator oi = getOccupancyIntegrator();
        permit.setLastUpdatedBy(ua);
        oi.updateOccPermit(permit);
        getOccupancyCacheManager().flush(permit);

    }

    /**
     * Updates the dynamic fields, and will internally call this class's
     * configureAndUpdatePermitDynamicPersonFieldsOnly to write the dynamic person links.
     * These steps are separate to allow for sterile amendment of person fields ONLY
     *
     * @param permit
     * @param opdh
     * @param ua
     * @throws BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void updateOccPermitDynamicFields(OccPermit permit, OccPeriodDataHeavy opdh, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException {
        if (permit == null || ua == null) {
            throw new BObStatusException("Cannot update occ permit with null permit or user");
        }
       
        if (permit.getNullifiedTS() != null) {
            throw new BObStatusException("OccupancyCoordinator.updateOccPermit: Cannot update a nullified permit");
        }

        OccupancyIntegrator oi = getOccupancyIntegrator();

        permit.setLastUpdatedBy(ua);
        // deac old links if any of these links are previous occpermit - human links, leaving
        // the incoming links intact which could be property-human, property unit human, or occ period human

        oi.updateOccPermitDynamicFieldsExceptPersons(permit);
        // now configure and update dynamic person links
        // these are separted to allow for isolation of amendable fields
        configureAndUpdatePermitDynamicPersonFieldsOnly(permit, ua);
        getOccupancyCacheManager().flush(permit);

    }
    
    /**
     * Configures and writes to the DB only this permit's dynamic personlinks; It clears them
     * first and then writes the new links. We'll call this AFTER permit amendment finalization
     * and not DURING so we never have to UNDO a dynamic change back to previous values if the 
     * amendment process is aborted.
     * @param permit
     * @param ua 
     */
    public void configureAndUpdatePermitDynamicPersonFieldsOnly(OccPermit permit, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException{
        OccupancyIntegrator oi = getOccupancyIntegrator();
        PersonCoordinator pc = getPersonCoordinator();
        
        pc.deleteOccPermitHumanLinks(permit);

        // inject our new permit-specific linked object role from a fixed DB key in the linkedobjectroletable
        occPermitConfigureHumanLinkListForDynamicLinking(permit.getOwnerSellerLinkList(), OccPermitPersonListEnum.CURRENT_OWNER);
        occPermitConfigureHumanLinkListForDynamicLinking(permit.getBuyerTenantLinkList(), OccPermitPersonListEnum.NEW_OWNER);
        occPermitConfigureHumanLinkListForDynamicLinking(permit.getManagerLinkList(), OccPermitPersonListEnum.MANAGERS);
        occPermitConfigureHumanLinkListForDynamicLinking(permit.getTenantLinkList(), OccPermitPersonListEnum.SALE_WITH_TENANTS);

        // now write our fresh links
        // these links go to the PersonCoordinator as fully fledged HumanLink objects and 
        // we are injeting the returned value of the human link IDs into the occ permit 
        // for easy writing as an array in the Integrator.
        // So users of the OccPermit object don't have to worry about the ID list, just the HumanLink object list
        permit.setOwnerSellerLinkIDList(pc.insertHumanLinks(permit, permit.getOwnerSellerLinkList(), ua));
        permit.setBuyerTenantLinkIDList(pc.insertHumanLinks(permit, permit.getBuyerTenantLinkList(), ua));
        permit.setManagerLinkIDList(pc.insertHumanLinks(permit, permit.getManagerLinkList(), ua));
        permit.setTenantLinkIDList(pc.insertHumanLinks(permit, permit.getTenantLinkList(), ua));
        
        oi.updateOccPermitDynamicFieldsPersonLinksOnly(permit);
        getOccupancyCacheManager().flush(permit);
        
    }

    /**
     * Deacs only occpermit human links
     *
     * @param linkList
     * @param permitRoleEnum
     * @return
     * @throws IntegrationException
     * @throws BObStatusException
     */
    private List<HumanLink> occPermitDeleteExistingOccPermitHumanLinksOnly(List<HumanLink> linkList, UserAuthorized ua) throws IntegrationException, BObStatusException {
        PersonCoordinator pc = getPersonCoordinator();

        if (linkList != null && !linkList.isEmpty() && ua != null) {
            for (HumanLink link : linkList) {
                if (link.getLinkedObjectSchemaEnum() != null && link.getLinkedObjectSchemaEnum() == LinkedObjectSchemaEnum.OccPermitHuman) {
//                    pc.deleteOccPermitHumanLinks(link);
                }
            }
        }
        return linkList;
    }

    /**
     * Writes a new Link Object Role to the given human links before injection
     * into the DB
     *
     * @param linkList
     * @param permitRoleEnum
     */
    private List<HumanLink> occPermitConfigureHumanLinkListForDynamicLinking(List<HumanLink> linkList, OccPermitPersonListEnum permitRoleEnum) throws IntegrationException, BObStatusException {
        SystemCoordinator sc = getSystemCoordinator();

        if (linkList != null && !linkList.isEmpty() && permitRoleEnum != null) {
            LinkedObjectRole role = sc.getLinkedObjectRole(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString(permitRoleEnum.getRoleDBFixedValueLookupKey())));
            for (HumanLink link : linkList) {
                link.setLinkedObjectRole(role);
            }
        }
        return linkList;

    }

    /**
     * Writes a name and timestamp to the nullification fields a permit
     *
     * @param permit
     * @param period
     * @param ua
     * @throws BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void nullifyOccPermit(OccPermit permit, OccPeriod period, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException {
        if (permit == null || ua == null) {
            throw new BObStatusException("OccupancyCoordinator.nullifyOccPermit | Cannot nullify occ permit with null permit or user");
        }
        if (permit.getFinalizedts() == null) {
            throw new BObStatusException("OccupancyCoordinator.nullifyOccPermit | Cannot nullify an occ permit that has NOT yet been finlized. You can deactivate a draft, though!");
        }
        if (!permissionsCheckpointOccPermitIssueAmend(ua)) {
            throw new AuthorizationException("Permission Denied: User not permitted to issue permits and therefore cannot nullify them");
        }

        OccupancyIntegrator oi = getOccupancyIntegrator();
        EventCoordinator ec = getEventCoordinator();

        permit.setNullifiedBy(ua);
        permit.setNullifiedTS(LocalDateTime.now());
        permit.setLastUpdatedBy(ua);
        oi.nullifyOccupancyPermit(permit);
        // now clear and deac events linked to this permit
        ec.deactivateAllEventsLinkedToEventLinkedObject(permit, period, true, false, ua);
        // this flush method will also flush the parent period
        getOccupancyCacheManager().flush(permit);

    }
    
    /**
     * Container for accessing the static DB value key for the permit follow up event
     * @return
     * @throws IntegrationException 
     */
    public EventCategory getPermitFollowupRequredEventCategory() throws IntegrationException{
        EventCoordinator ec = getEventCoordinator();
        return ec.getEventCategory(Integer.parseInt(getResourceBundle(Constants.EVENT_CATEGORY_BUNDLE).getString("permit_issuance_reminder")));
    }

    /**
     * Updates the meta data of an occ permit only.Use separate method for
     * static fields
     *
     * @param permit
     * @param period
     * @param ua
     * @param m
     * @throws BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void occPermitFinalize(OccPermit permit, OccPeriod period, UserAuthorized ua, Municipality m)
            throws BObStatusException, IntegrationException, AuthorizationException, EventException, BlobException {
        if (permit == null || ua == null) {
            throw new BObStatusException("Cannot finalize occ permit with null permit or user");
        }
        OccupancyIntegrator oi = getOccupancyIntegrator();
        permit.setReferenceNo(generateOccPermitReferenceNumber(permit, m));
      
        // removed from audit due to spurious failed audits
//        if (permit.getFinalizationAuditPassTS() == null) {
//            throw new BObStatusException("Finalization cannot occur with a failed audit");
//        }
        if (!permissionsCheckpointOccPermitIssueAmend(ua)) {
            throw new AuthorizationException("Permission Denied: User not permitted to issue permits");
        }

        occPermitFinalizeWriteUpdates(permit, period, ua);
        
      

    }

    /**
     * Updates the meta data of an occ permit only. Use separate method for
     * static fields
     *
     * @param permit
     * @param ua
     * @param m
     * @throws BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void occPermitFinalizeOverrideAudit(OccPermit permit, OccPeriod period, UserAuthorized ua, Municipality m)
            throws BObStatusException, IntegrationException, AuthorizationException, EventException, BlobException {
        if (permit == null || ua == null) {
            throw new BObStatusException("Cannot finalize occ permit with null permit or user");
        }
        permit.setReferenceNo(generateOccPermitReferenceNumber(permit, m));
        if (permit.getFinalizationAuditPassTS() == null) {
            System.out.println("OccupancyCoordinator.occPermitFinalizeOverrideAudit | Overriding failed permit audit by " + ua.getUsername());
        }
        if (!permissionsCheckpointOccPermitIssueAmend(ua)) {
            throw new AuthorizationException("Permission Denied! User is forbidden from overriding failed permit finalization audits");
        }
        occPermitFinalizeWriteUpdates(permit, period, ua);
    }
    
    /**
     * Checks for system admin permission for override of all permit rules. It's caller's job
     * to get a fresh object from the DB                                                                                                                                                                                                                                                                                                                      
     * @param permit
     * @param ua
     * @throws BObStatusException 
     */
    public void occPermitToggleAdminOverride(OccPermit permit, UserAuthorized ua) throws BObStatusException, AuthorizationException, IntegrationException{
        OccupancyIntegrator oi = getOccupancyIntegrator();
                
        if(permit == null || ua == null){
            throw new BObStatusException("Cannot implement admin override with null user or permit");
        }
        if(!permissionsCheckpointOccPermitAdminOverride(ua)){
            throw new AuthorizationException("User cannot register an admin override for this certificiate");
        }
                                     
        // we work with both writing and revoking override
        if(permit.getAdminOverrideTS() == null){
            System.out.println("OccupancyCoordinator.occPermitToggleAdminOverride | incoming permit no override; writing override");
            // we are writing an override, since incoming permit does not have override flag
            permit.setAdminOverrideUMAP(ua.getKeyCard().getGoverningAuthPeriod());
            permit.setAdminOverrideTS(LocalDateTime.now());
        } else {
            System.out.println("OccupancyCoordinator.occPermitToggleAdminOverride | incoming permit AHS override; REVOKING override");
            permit.setAdminOverrideUMAP(null);
            permit.setAdminOverrideTS(null);
        }
        
        permit.setLastUpdatedBy(ua);
        oi.updateOccPermitAdminOverride(permit);
        getOccupancyCacheManager().flush(permit);
        
    }

    /**
     * Writes finalization fields and sends to integrator for update
     *
     * @param permit
     * @param ua
     * @throws IntegrationException
     */
    private void occPermitFinalizeWriteUpdates(OccPermit permit, OccPeriod period, UserAuthorized ua) throws IntegrationException, BObStatusException, EventException, AuthorizationException, BlobException {
        OccupancyIntegrator oi = getOccupancyIntegrator();

        permit.setLastUpdatedBy(ua);
        permit.setFinalizedBy(ua);
        permit.setFinalizedts(LocalDateTime.now());

        oi.updateOccPermit(permit);
        getOccupancyCacheManager().flush(permit);

    }

    /**
     * Sets up two events to document permit finalization and, if a temporary,
     * an event that marks the end of the permit's temporary window.
     * 
     * I also look at the permit and see if there was an issuance reminder 
     * event and if there was I'll mark that as having been followed up on.
     *
     * @param permit freshly issued permit
     * @param period
     * @param evDescr to be injected into the issuance event's belly: Should 
     * probably say how the permit was distributed
     * @param ua doing the issuance
     * @param recipient the human link that received the permit, not used March 2025
     * @return the events that have been added during this process
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.EventException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public List<EventCnF> occPermitDeployRecordingAndFollowUpEvents(OccPermit permit, 
                                                                    OccPeriod period,
                                                                    String evDescr,
                                                                    HumanLink recipient,
                                                                    UserAuthorized ua)
            throws BObStatusException, EventException, IntegrationException, AuthorizationException, BlobException {
        if (permit == null || period == null || ua == null || ua.getKeyCard().getGoverningAuthPeriod() == null || ua.getKeyCard().getGoverningAuthPeriod().getMuniProfile() == null) {
            throw new BObStatusException("Cannot deploy events with null permit, user, or user's UMAP profile");
        }
        EventCoordinator ec = getEventCoordinator();
        List<EventCnF> evList = new ArrayList<>();
        // general permit issuance logging event
        EventCategory permitIssuedCat = ua.getKeyCard().getGoverningAuthPeriod().getMuniProfile().getEventCatPermitIssued();
        StringBuilder sb = null;
        EventCnF permitIssuedEvent = null;
        
        if (permitIssuedCat != null) {
            permitIssuedEvent = ec.initEvent(period, permitIssuedCat);
            permitIssuedEvent.appendToDescription(evDescr);
            // configure times 
            permitIssuedEvent.configureEventTimesFromStartTime(permit.getStaticdateofissue().atStartOfDay(), 0);
            evList.addAll(ec.addEvent(permitIssuedEvent, period, ua));
        }
        // TCO epiry event
        EventCategory tcoFollowUpCat = ua.getKeyCard().getGoverningAuthPeriod().getMuniProfile().getEventCatExpiredTCOFollowUp();
        if (tcoFollowUpCat != null && permit.getStaticdateofexpiry() != null) {
            EventCnF expiringPermitFollowUpEvent = ec.initEvent(period, tcoFollowUpCat);
            sb = new StringBuilder("Expiring permit type: ");
            sb.append(permit.getPermitType().getPermitTitle());
            sb.append(Constants.FMT_SEMICOLON);
            sb.append(Constants.FMT_SPACE_LITERAL);
            sb.append("Permit no. ");
            sb.append(permit.getReferenceNo());
            expiringPermitFollowUpEvent.appendToDescription(sb.toString());
            // follow up at noon on the declared date of expiry
            expiringPermitFollowUpEvent.configureEventTimesFromStartTime(permit.getStaticdateofexpiry().atStartOfDay().withHour(12), 0);
            evList.addAll(ec.addEvent(expiringPermitFollowUpEvent, period, ua));
        }

        // link new events to the permit we just made
        for (EventCnF ev : evList) {
            ec.linkEventToEventLinked(ev, permit);
        }
        
        permit = getOccPermit(permit.getPermitID(), ua);
        EventCnF fupEv = ec.determinePresenceOfEventCategoryInList(permit.getLinkedEvents(), getPermitFollowupRequredEventCategory());
        if(fupEv != null){
            System.out.println("OccupancyCoordinator.occPermitDeployRecordingAndFollowUpEvents | Found an permit follow up event that is now satisfied, so marking it as such");
            ec.linkEventToEventLinked(ec.determinePresenceOfEventCategoryInList(evList, permitIssuedCat), fupEv);
        }
        getOccupancyCacheManager().flush(period);
        getOccupancyCacheManager().flush(permit);

        return evList;
    }

    
    
    
    /**
     * Logic mechanism for checking a permit's readiness for finalization This
     * reviews permit static fields, after the user has had a chance to review
     * the automatically populated fields.
     * 
     * NOTE: removed from service 25SEP2024 due to spurious failed audits
     *
     * @param permit
     * @param ua
     * @return
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public boolean occPermitAuditForFinalization(OccPermit permit, UserAuthorized ua) throws BObStatusException {
        
        // SHORT CIRCUITED CODE -- NOT USED
        
        boolean auditPass = true;

        if (permit == null || ua == null) {
            throw new BObStatusException("Cannot aduit a null pointer or user");

        }

        permit.clearFinalizationAuditLog();
        // Officer, muni, ids
        if (permit.getPermitID() == 0) {
            permit.appendToFinalizationAuditLog("FATAL Occ Permit Finalization error code OPF0: Permit ID cannot be zero");
            auditPass = false;
        }
        if (permit.getStaticofficername() == null) {
            permit.appendToFinalizationAuditLog("FATAL Occ Permit Finalization error code OPF2: null officer");
            auditPass = false;
        }
        // TURN OFF THIS SINCE REF no not generated until the very end
//       if(permit.getReferenceNo() == null){
//           permit.appendToFinalizationAuditLog("FATAL Occ Permit Finalization error code OPF3: no reference number");
//           auditPass = false;
//       }

        // bypass date audits because custom validators will do all the work in step 2
        // legacy date audits are stored in repo under codeConnect/archive
        if (auditPass) {
            permit.setFinalizationAuditPassTS(LocalDateTime.now());
            permit.appendToFinalizationAuditLog("Audit result: Pass timestamp injected!");
        } else {
            permit.appendToFinalizationAuditLog("Audit result: FAILURE - No timestamp for you");
        }
        
        // give everybody a stamp
        permit.setFinalizationAuditPassTS(LocalDateTime.now());
        return true;

//        return auditPass;
    }

    /**
     * Deactivates an occ permit only. Use separate method for static fields
     *
     * @param permit Cannot be finalized
     * @param ua
     * @throws BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void occPermitDeactivate(OccPermit permit, UserAuthorized ua) throws BObStatusException, IntegrationException {
        if (permit == null || ua == null) {
            throw new BObStatusException("Cannot finalize occ permit with null permit or user");
        }
        if (permit.getFinalizedts() != null) {
            throw new BObStatusException("Umm, you aren't allowed to deactivate a finalized permit");
        }
        OccupancyIntegrator oi = getOccupancyIntegrator();
        permit.setLastUpdatedBy(ua);
        permit.setDeactivatedBy(ua);
        permit.setDeactivatedTS(LocalDateTime.now());

        oi.updateOccPermit(permit);
        getOccupancyCacheManager().flush(permit);

    }
    
    
    
    // *************************************************************************
    // ********************* OCC PERMITS STATIC FIELD WRITES *******************
    // *************************************************************************

    /**
     * The lifeblood logic house for extracting the objects from the OccPermit's
     * non static fields and extracting the appropriate data and writing fixed
     * String/Date values to the OccPermit's static fields and then writing
     * those changes to the DB.
     * 
     * NOT TO BE USED DURING THE AMENDMENT PROCESS
     *
     * @param period
     * @param permit must contain the issuing officer
     * @param ua the user currently requesting the permit creation; NOT the
     * issuing officer
     * @param mdh the current muni
     * @throws BObStatusException
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.SearchException
     * @throws com.tcvcog.tcvce.domain.BlobException
     * @throws com.tcvcog.tcvce.domain.EventException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void occPermitPopulateStaticFieldsFromDynamicFieldsAndWriteToDB( OccPeriodDataHeavy period,
                                                                            OccPermit permit,
                                                                            UserAuthorized ua,
                                                                            MunicipalityDataHeavy mdh)
        throws BObStatusException, IntegrationException, SearchException, BlobException, EventException, AuthorizationException {
        
        
        OccupancyIntegrator oi = getOccupancyIntegrator();

        if (permit == null || ua == null || period == null || mdh == null) {
            throw new BObStatusException("Cannot insert occ permit with null permit or user or period or munidh");
        }
        if(permit.isCurrentPermitAmendmentMode()){
            
        }

        if (permit.getFinalizedts() != null) {
            throw new BObStatusException("Cannot update static fields of a finalized permit!");
        }
        if (permit.getPermitType() == null) {
            throw new BObStatusException("Cannot write static fields with null permit type");
        }

        occPermitClearStaticFields(permit);

        // sequentially engage logic to write static fields from our dynamic ones
        occPermitPopulateStaticFields_headings(period, permit, ua, mdh);
        occPermitPopulateStaticFields_property(period, permit, ua, mdh);
        occPermitPopulateStaticFields_persons(permit);
        occPermitPopulateStaticFields_dates(period, permit, ua, mdh);
        occPermitPopulateStaticFields_text(period, permit, ua, mdh);
        occPermitPopulateStaticFields_authority(period, permit, ua, mdh);
     
        // pick back up after branching for amendment mode vs. non-amendment mode
        permit.setLastUpdatedBy(ua);
        // write to DB
        oi.updateOccPermitStaticFields(permit);
        getOccupancyCacheManager().flush(period);
    }
    
    /**
     * Used during the amendment process ONLY to write static values from the given permit's
     * internal dynamic persons. Note that unlike the normal permit creation process where
     * static values are written to the DB and then accessible from the DB for review 
     * prior to finalization, the amendment process does NOT involve saving static fields
     * for review--only at the time of finalization so we don't have ratty data in the DB
     * from a mid-way-through amendment prcess
     * 
     * @param permit
     * @param ua
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public void occPermitPopulateAmendableStaticFieldsWithoutDBWrite(OccPermit permit, UserAuthorized ua) throws BObStatusException, IntegrationException { 
        if(permit.getAmendementInitTS() != null && permit.getAmendementCommitTS() != null){
            throw new BObStatusException("Cannot amend a permit whose amendments have already been committed");
        }
        
        occPermitPopulateStaticFields_persons(permit);
        
    }

    /**
     * Logic block for configuration of an occupancy permit's heading property
     * fields from various members on feeding objects.
     *
     * @param period
     * @param permit
     * @param ua
     * @param mdh
     */
    private void occPermitPopulateStaticFields_headings(OccPeriodDataHeavy period,
            OccPermit permit,
            UserAuthorized ua,
            MunicipalityDataHeavy mdh) {

        permit.setStatictitle(permit.getPermitType().getPermitTitle());
        
        if(mdh.getDefaultMuniHeaderImageWidthPX() != 0){
            permit.setStaticheaderWidthPx(mdh.getDefaultMuniHeaderImageWidthPX());
        } else {
            permit.setStaticheaderWidthPx(DEFAULT_CERT_HEADER_WIDTH);
        }

        if (permit.getPermitType().getPersonColumnLinkingText() != null) {
            permit.setStaticcolumnlink(permit.getPermitType().getPersonColumnLinkingText());
        } else {
            permit.setStaticcolumnlink(OCCPERMIT_DEFAULT_COL_SEP);
        }

        if (mdh.getDefaultMuniHeaderImage() != null) {
            permit.setStaticheaderphotodocid(mdh.getDefaultMuniHeaderImage().getPhotoDocID());
        }

        // muni addresss
        StringBuilder sb;
        sb = new StringBuilder();
        sb.append(mdh.getMuniName());
        sb.append(Constants.FMT_HTML_BREAK);
        sb.append(mdh.getMuniPropertyDH().getAddress().getAddressPretty2LineEscapeFalse());
        permit.setStaticmuniaddress(sb.toString());
        
        permit.setStaticdisplaymuniaddress(permit.getPermitType().isPermitDisplayMuniAddress());
    }

    /**
     * Logic block for configuration of an occupancy permit's static property
     * fields from various members on feeding objects.
     *
     * @param period
     * @param permit
     * @param ua
     * @param mdh
     */
    private void occPermitPopulateStaticFields_property(OccPeriodDataHeavy period,
            OccPermit permit,
            UserAuthorized ua,
            MunicipalityDataHeavy mdh) throws IntegrationException, BObStatusException, EventException, AuthorizationException, SearchException, BlobException {
        PropertyCoordinator pc = getPropertyCoordinator();

        StringBuilder sb;

        // now do property and unit static fields
        PropertyUnitWithProp puwp = pc.getPropertyUnitWithProp(period.getPropertyUnitID());
        PropertyUnitDataHeavy pudh = pc.getPropertyUnitDataHeavy(puwp, ua);
        PropertyDataHeavy pdh = pc.assemblePropertyDataHeavy(pc.getProperty(period.getParentParcelKey()), ua);
        sb = new StringBuilder();
        if (puwp != null) {
            if ((!puwp.getUnitNumber().equals(pc.DEFAULTUNITDESIGNATOR)) || (!pc.isUnitDefault(puwp))) {
                sb.append("Unit: ");
                sb.append(puwp.getUnitNumber());
                sb.append(Constants.FMT_HTML_BREAK);
                // if we have a unit-specific mailing address, insert it here
                if (pudh.getPrimaryMailingAddressLink() != null) {
                    sb.append("Unit mailing: ");
                    sb.append(pudh.getPrimaryMailingAddressLink().getAddressPretty1Line());
                    sb.append(Constants.FMT_HTML_BREAK);
                }
            }
            if (puwp.getProperty().getAddress() != null) {
                sb.append(puwp.getProperty().getAddress().getAddressPretty2LineEscapeFalse());
                sb.append(Constants.FMT_HTML_BREAK);
            }
            sb.append("County parcel ID: ");
            sb.append(puwp.getProperty().getCountyParcelID());
            permit.setStaticpropertyinfo(sb.toString());
        }
        // based on parcel info, populate parcel info static fields
        permit.setParcelInfo(pdh.getParcelInfo());

        // inject parcel info fields
        permit.setStaticpropclass(permit.getParcelInfo().getPropClass());
        permit.setStaticusecode(permit.getParcelInfo().getUseGroup());
        permit.setStaticproposeduse(permit.getPermitType().getAuthorizeduses());
        permit.setStaticconstructiontype(permit.getParcelInfo().getConstructionType());

    }

    /**
     * Logic block for configuration of an occupancy permit's static person
     * fields from various members on feeding objects.
     *
     * @param period
     * @param permit
     * @param ua
     * @param mdh
     */
    private void occPermitPopulateStaticFields_persons(OccPermit permit) throws BObStatusException, IntegrationException {
        
        StringBuilder sb;
         // populate our switches for our four person link categories
         permit.setStaticdisplayperslinkleftcol(permit.getPermitType().isPersonColumnLeftActivate());
         permit.setStaticdisplayperslinkrightcol(permit.getPermitType().isPersonColumnRightActivate());
         permit.setStaticdisplayperslinkmanagers(permit.getPermitType().isPersonColumnManagerActivate());
         permit.setStaticdisplayperslinktenants(permit.getPermitType().isPersonColumnTenantActivate());
         
        if (permit.getOwnerSellerLinkList() != null && !permit.getOwnerSellerLinkList().isEmpty() && permit.isStaticdisplayperslinkleftcol()) {
            if (permit.getOwnerSellerType() != null) {
                  if(permit.getOwnerSellerType() != OccPermitPersonListEnum.CUSTOM_PERSON_LABEL){  
                    permit.setStaticownersellerlabel(permit.getOwnerSellerType().getLabel());
                  } else { // use custom label
                    permit.setStaticownersellerlabel(permit.getPermitType().getPersonColumnLeftCustomLabel());
                  }
            } // if we don't have one, the generic dual label will be injected on the way out of DB
            sb = new StringBuilder();
            for (HumanLink hl : permit.getOwnerSellerLinkList()) {
                sb.append(buildPersonContactString(hl));
            }
            permit.setStaticownerseller(sb.toString());
        }

        if (permit.getBuyerTenantLinkList() != null && !permit.getBuyerTenantLinkList().isEmpty() && permit.isStaticdisplayperslinkrightcol()) {
            if (permit.getBuyerTenantType() != null) {
                if(permit.getBuyerTenantType() != OccPermitPersonListEnum.CUSTOM_PERSON_LABEL){
                    permit.setStaticbuyertenantlabel(permit.getBuyerTenantType().getLabel());
                } else { // use custom label
                    permit.setStaticbuyertenantlabel(permit.getPermitType().getPersonColumnRightCustomLabel());
                }
            }
            sb = new StringBuilder();
            for (HumanLink hl : permit.getBuyerTenantLinkList()) {
                sb.append(buildPersonContactString(hl));
            }
            permit.setStaticbuyertenant(sb.toString());
        }

        if (permit.getManagerLinkList() != null && !permit.getManagerLinkList().isEmpty() && permit.isStaticdisplayperslinkmanagers()) {
            sb = new StringBuilder();
            for (HumanLink hl : permit.getManagerLinkList()) {
                sb.append(buildPersonContactString(hl));
            }
            permit.setStaticmanager(sb.toString());
        }

        if (permit.getTenantLinkList() != null && !permit.getTenantLinkList().isEmpty() && permit.isStaticdisplayperslinktenants()) {
            sb = new StringBuilder();
            for (HumanLink hl : permit.getTenantLinkList()) {
                sb.append(buildPersonContactString(hl));
            }
            permit.setStatictenants(sb.toString());
        }

    }

    /**
     * Logic block for configuration of an occupancy permit's static date fields
     * from various members on feeding objects.
     *
     * @param period
     * @param permit
     * @param ua
     * @param mdh
     */
    private void occPermitPopulateStaticFields_dates(OccPeriodDataHeavy period,
            OccPermit permit,
            UserAuthorized ua,
            MunicipalityDataHeavy mdh) {

        // move over dates
        permit.setStaticdateofapplication(permit.getDynamicDateOfApplication());
        permit.setStaticinitialinspection(permit.getDynamicInitialInspection());
        permit.setStaticreinspectiondate(permit.getDynamicreinspectiondate());
        permit.setStaticfinalinspection(permit.getDynamicfinalinspection());
        permit.setStaticdateofissue(permit.getDynamicdateofissue());
        if (permit.getPermitType().isExpires()) {
            permit.setStaticdateofexpiry(permit.getDynamicDateExpiry());
        }
    }

    /**
     * Logic block for configuration of an occupancy permit's static stipulations, 
     * comments, and notices fields
     * from various members on feeding objects.
     *
     * @param period
     * @param permit
     * @param ua
     * @param mdh
     */
    private void occPermitPopulateStaticFields_text(OccPeriodDataHeavy period,
            OccPermit permit,
            UserAuthorized ua,
            MunicipalityDataHeavy mdh) {
        permit.setStaticstipulations(buildSingleStringFromTextBlockList(permit.getTextBlocks_stipulations(), permit.getText_stipulations()));
        permit.setStaticnotice(buildSingleStringFromTextBlockList(permit.getTextBlocks_notice(), permit.getText_notices()));
        permit.setStaticcomments(buildSingleStringFromTextBlockList(permit.getTextBlocks_comments(),permit.getText_comments()));
    }

    /**
     * Utility method for weaving together name and contact into a string for
     * printing
     *
     * @param hl
     * @return
     * @throws BObStatusException
     */
    private String buildPersonContactString(HumanLink hl) throws BObStatusException, IntegrationException {
        StringBuilder sb = new StringBuilder();
        PersonCoordinator pc = getPersonCoordinator();
        
        if (hl == null) {
            throw new BObStatusException("cannot build person string with null link");
        }
        
        // i want fresh persons!
        hl = pc.getHumanLink(hl.getLinkID(), hl.getLinkedObjectRole().getSchema());
        System.out.println("OccupancyCoordintaor.buildPersonContactString | Fresh human ID: " + hl.getHumanID());

        sb.append(hl.getName());
        sb.append(Constants.FMT_HTML_BREAK);
        
        System.out.println("OccupancyCoordintaor.buildPersonContactString | mailingAddressListPretty: " + hl.getMailingAddressListPretty());
        if(hl.getMailingAddressLinkList() != null){
            System.out.println("OccupancyCoordintaor.buildPersonContactString | mailingAddressLinkLIst: " + hl.getMailingAddressLinkList().size());
        } else {
            System.out.println("OccupancyCoordintaor.buildPersonContactString | Empty mailing address link list ");
        }
        if (hl.getMailingAddressListPretty() != null) {
            sb.append(Constants.FMT_HTML_BREAK);
            pc.generateMADLinkListPretty(hl);
            sb.append(hl.getMailingAddressListPretty());
        }
        if (hl.getPhoneListPretty() != null) {
            sb.append(Constants.FMT_HTML_BREAK);
            sb.append(hl.getPhoneListPretty());
        }

        if (hl.getEmailListPretty() != null) {
            sb.append(Constants.FMT_HTML_BREAK);
            sb.append(hl.getEmailListPretty());
        }
        return sb.toString().replace("<br><br>", "<br>");
    }
    
    /**
     * Logic block for configuration of an occupancy permit's static officer
     * fields from various members on feeding objects.
     *
     * @param period
     * @param permit
     * @param ua
     * @param mdh
     */
    private void occPermitPopulateStaticFields_authority(OccPeriodDataHeavy period,
            OccPermit permit,
            UserAuthorized ua,
            MunicipalityDataHeavy mdh) throws BObStatusException, IntegrationException {
        if (permit.getIssuingOfficer() == null) {
            throw new BObStatusException("Cannot populate occ permit with null issuing officer");
        }
        if (permit.getIssuingCodeSourceList() == null) {
            throw new BObStatusException("Cannot populate occ permit with null code source");
        }
        
        PersonCoordinator persc = getPersonCoordinator();

        // build code source string
        StringBuilder sb = new StringBuilder();
        if (permit.getIssuingCodeSourceList() != null && !permit.getIssuingCodeSourceList().isEmpty()) {
            // remove dupes
            List<CodeSource> uniqueSourceList = permit.getIssuingCodeSourceList().stream()
                                                        .distinct()
                                                        .collect(Collectors.toList());
            
            int sourcecount = 0;
            for (CodeSource src : uniqueSourceList) {
                sb.append(src.toString());
                sourcecount += 1;
                // only add breaks if there are 2 or more sources and this is NOT the last source in the list
                if (permit.getIssuingCodeSourceList().size() > 1 && sourcecount < permit.getIssuingCodeSourceList().size()) {
                    sb.append(Constants.FMT_HTML_BREAK);
                }
            }
            permit.setStaticissuedundercodesourceid(sb.toString());
        }

        if (permit.getIssuingOfficerPerson() == null) {
            permit.setIssuingOfficerPerson(persc.getPerson(permit.getIssuingOfficer().getHuman()));
        }

        // build issuing officer
        sb = new StringBuilder();
        sb.append(permit.getIssuingOfficer().getHuman().getName());
        sb.append(Constants.FMT_HTML_BREAK);
        if (permit.getIssuingOfficer().getHuman().getJobTitle() != null) {
            sb.append(permit.getIssuingOfficer().getHuman().getJobTitle());
            sb.append(Constants.FMT_HTML_BREAK);
        }
        if (permit.getIssuingOfficerPerson().getPrimaryPhone() != null) {
            sb.append(permit.getIssuingOfficerPerson().getPrimaryPhone().getPhoneNumber());
            sb.append(Constants.FMT_HTML_BREAK);
        }
        if (permit.getIssuingOfficerPerson().getPrimaryEmail() != null) {
            sb.append(permit.getIssuingOfficerPerson().getPrimaryEmail().getEmailaddress());
        }
        permit.setStaticofficername(sb.toString());

        // issuing officer signature
        permit.setStaticOfficerSignaturePhotoDocID(permit.getIssuingOfficer().getSignatureBlobID());

    }

    /**
     * Uses the permit type to set the permit's column labels
     */
    private void configurePermitPersonColumnHeaders(OccPermit permit) {
        if (permit != null && permit.getPermitType() != null) {
            permit.setDynamicPersonsColumnLink(permit.getPermitType().getPersonColumnLinkingText());
            permit.setOwnerSellerType(permit.getPermitType().getPersonColumnLeftLabel());
            permit.setBuyerTenantType(permit.getPermitType().getPersonColumnRightLabel());
        }
    }

    /**
     * Allows for a fresh start writing in static values
     *
     * @param permit
     */
    private void occPermitClearStaticFields(OccPermit permit) {
        permit.setStaticdateofapplication(null);
        permit.setStaticinitialinspection(null);
        permit.setStaticreinspectiondate(null);
        permit.setStaticfinalinspection(null);

        permit.setStaticdateofissue(null);
        permit.setStaticdateofexpiry(null);
        permit.setStatictitle(null);
        permit.setStaticmuniaddress(null);
        permit.setStaticdisplaymuniaddress(true);

        permit.setStaticpropertyinfo(null);
        permit.setStaticownerseller(null);
        permit.setStaticcolumnlink(null);
        permit.setStaticbuyertenant(null);

        permit.setStaticproposeduse(null);
        permit.setStaticusecode(null);
        permit.setStaticconstructiontype(null);
        permit.setStaticpropclass(null);

        permit.setStaticofficername(null);
        permit.setStaticissuedundercodesourceid(null);
        permit.setStaticstipulations(null);
        permit.setStaticcomments(null);

        permit.setStaticmanager(null);
        permit.setStatictenants(null);
        permit.setStaticleaseterm(null);
        permit.setStaticleasestatus(null);

        permit.setStaticpaymentstatus(null);
        permit.setStaticnotice(null);

    }

    

    /**
     * Takes in a list of text blocks and spits out a single string with breaks
     * in between blocks
     *
     * @param tbl
     * @return
     */
    private String buildSingleStringFromTextBlockList(List<TextBlock> tbl, String freeForm) {
        if (tbl == null && freeForm == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        if (tbl != null && !tbl.isEmpty()) {
            for (TextBlock tb : tbl) {
                sb.append(tb.getTextBlockText());
                sb.append(Constants.FMT_HTML_BREAK);
            }
        }
        if(!isStringEmptyIsh(freeForm)){
            sb.append(Constants.FMT_HTML_BREAK);
            sb.append(freeForm);
        }
                
        return sb.toString();
    }
    
    /**
     * Creates a hacky way of permit issuanec forms to display the option of
     * issuing the permit to the borough staff
     * @return 
     */
    public HumanLink createMuniStaffHumanLink(){
        PersonCoordinator pc = getPersonCoordinator();
        HumanLink muniLink = new HumanLink(new Person(new Human()));
        muniLink.setName("Municpality staff");
        return muniLink;
        
        
    }

    /**
     * Builds a custom reference number for the permit of the form
     * MUNICODE-YY-SEQ eg. 814-22-CNF-1
     *
     * @param permit
     * @param ua
     * @return
     */
    private String generateOccPermitReferenceNumber(OccPermit permit, Municipality m) throws BObStatusException, IntegrationException {
        OccupancyIntegrator oi = getOccupancyIntegrator();
        StringBuilder sb = new StringBuilder();
        sb.append(m.getMuniCode());
        sb.append(Constants.FMT_DTYPE_KEY_SEP_DESC);
        sb.append(LocalDateTime.now().getYear());
        sb.append(Constants.FMT_DTYPE_KEY_SEP_DESC);
        sb.append("CNF");
        sb.append(Constants.FMT_DTYPE_KEY_SEP_DESC);

        int currentCount = oi.countFinalizedPermitsByMuni(m);
        int increcount = currentCount + 1;
        sb.append(increcount);
        return sb.toString();

    }
    
    
    // *************************************************************************
    // ********************* OCC PERMITS AMENDMENTS  ***************************
    // *************************************************************************

    /**
     * Determines if the given permit and permit officer is eligible for 
     * Amendment
     * 
     * @param permit
     * @param ua
     * @return 
     */
    public boolean allowPermitAmendment(OccPermit permit, UserAuthorized ua){
        if(permit == null || ua == null){
            return false;
        }
        if(!permissionsCheckpointOccPermitIssueAmend(ua)){
            return false;
        }
        // permit must be finalized
        if(permit.getFinalizedts() == null){
            return false;
        }
        // cannot amend an amended cert
        if(permit.getAmendementCommitTS() != null){
            return false;
        }
        // check that we are inside the amendment window 
        if((permit.getStaticdateofissue() != null && permit.getStaticdateofissue().atStartOfDay().plusDays(PERMIT_AMENDMENT_WINDOW_DAYS).isAfter(LocalDateTime.now())) || ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        return false;
    }
    
    /**
     * Starts the amendment process which means writing a timestamp
     * to the given permit's AmendmentInitTS field and the requesting
     * user's UMAP ID to amenedmendByUMAP
     * 
     * @param permit to be amended, exactly as it was BEFORE amendment
     * @param ua 
     * @return  The refreshed permit object INCLUDING its AmendedFields member
     * set which this class's permitAmendCommit will require to be populated
     * in order to actually write the new static (and dynamic) values
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BlobException 
     */
    public OccPermit permitAmendInit(OccPermit permit, UserAuthorized ua) throws BObStatusException, IntegrationException, BlobException{
        OccupancyIntegrator oi = getOccupancyIntegrator();
        if(!allowPermitAmendment(permit, ua)){
            throw new BObStatusException("Permit cannot be amended due to permissions or amendment window violation");
        }
        permit.setAmendmendedByUmap(ua.getKeyCard().getGoverningAuthPeriod());
        // record on the permit that we've started the amendment process which will
        // trigger amendment-specific controls to appear in our flow
        oi.updateOccPermitAmendmentInit(permit);
        // refresh our permit
        permit = getOccPermit(permit.getPermitID(), ua);
        // inject our pre-fields that the amendmentCommit method will require
        getOccupancyCacheManager().flush(permit);
        return permit;
        
    }
    
    /**
     * Builds a record of the given permit's tenants, buyers, managers, and tenants across sale
     * @param permit
     * @return the bundle of existing values for documentation on amendment finalization
     */
    public OccPermitPreAmendmentFields occPermitExtractPreAmendmentFields(OccPermit permit){
        OccPermitPreAmendmentFields preFields = new OccPermitPreAmendmentFields();
        if(permit != null){
            // extract our pre-amendment fields
            preFields.setParentPermitID(permit.getPermitID());
            preFields.setPreAmendmentOwnerSellerLinkList(permit.getOwnerSellerLinkList());
            preFields.setPreAmendmentBuyerTenantLinkList(permit.getBuyerTenantLinkList());
            preFields.setPreAmendmentManagerLinkList(permit.getManagerLinkList());
            preFields.setPreAmendmentTenantLinkList(permit.getTenantLinkList());

            
        }
        return preFields;
    }
    
    /**
     * Turns OFF amendment mode for permits which started the amendment process
     * @param permit
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void permitAmendAbort(OccPermit permit, UserAuthorized ua) throws IntegrationException, BObStatusException{
        OccupancyIntegrator oi = getOccupancyIntegrator();
        if(permit != null && ua != null && permit.getAmendementInitTS() != null && permit.getAmendementCommitTS() == null){
            oi.updateOccPermitAmendmentAbort(permit);
            getOccupancyCacheManager().flush(permit);
        } else {
            throw new BObStatusException("Permit amendment cannot be aborted");
        }
    }
    
    
    /**
     * Commits the permit amendment process which means writing a timestamp 
     * to the AmendmentCommitTS field. 
     * 
     * @param permit with a non-null preAmendmentFields object which should have been populated
     *        by this class's permitAmendInit method. I'll write the old values to the given permit's notes for archival reference
     * @param ua requesting the committal of amendment
     */
    public void permitAmendCommit(OccPermit permit, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        OccupancyIntegrator oi = getOccupancyIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        PersonCoordinator pc = getPersonCoordinator();
        
        
        if(permit.getPreAmendmentFields() == null){
            throw new BObStatusException("Cannot amend permit without pre-amendment person link fields as not null");
        }
        if(permit.getPreAmendmentFields().getParentPermitID() != permit.getPermitID()){
            throw new BObStatusException("Cannot amend permit without matching pre-amendment field permit ID and this permit's ID");
        }
        
        permit.setAmendmendedByUmap(ua.getKeyCard().getGoverningAuthPeriod());
                       
        // document our pre-amendment person link fields
        StringBuilder sb = new StringBuilder();
        sb.append("Previous owner-seller persons: ");
        sb.append(Constants.FMT_HTML_BREAK);
        sb.append(pc.buildHumanLinkListString(permit.getPreAmendmentFields().getPreAmendmentOwnerSellerLinkList()));
        sb.append("Previous buyer-tenant persons: ");
        sb.append(Constants.FMT_HTML_BREAK);
        sb.append(pc.buildHumanLinkListString(permit.getPreAmendmentFields().getPreAmendmentBuyerTenantLinkList()));
        sb.append("Previous manager persons: ");
        sb.append(Constants.FMT_HTML_BREAK);
        sb.append(pc.buildHumanLinkListString(permit.getPreAmendmentFields().getPreAmendmentManagerLinkList()));
        sb.append("Previous tenant persons: ");
        sb.append(Constants.FMT_HTML_BREAK);
        sb.append(pc.buildHumanLinkListString(permit.getPreAmendmentFields().getPreAmendmentTenantLinkList()));
        MessageBuilderParams mbp = new MessageBuilderParams(permit.getNotes(), "Person links BEFORE amendment", null, sb.toString(), ua, null);
        permit.setNotes(sc.appendNoteBlock(mbp));
        
        // update our dynamic values so our second generation permits which are 
        // amended have accurate dynamic data
        configureAndUpdatePermitDynamicPersonFieldsOnly(permit, ua);
        // set our current step to 6 for amended permits--we are doing the 
        // automated step system's legwork specially here beacuse
        // we're not using the normal updateDynamic fields pathway
        permit.setDynamicLastStep(OccPermitFlowStepEnum.REVIEW.getStepNumber1Based());
        
        // write our new static person fields
        oi.updateOccPermitAmendmentCommit(permit);
        getOccupancyCacheManager().flush(permit);
    }
    
    /**
     * Logic gate for checking credentials for permit revocation
     * @param permit
     * @param period which the permit calls home
     * @param ua 
     */
    public void permitRevokeCommit(OccPermit permit, OccPeriod period, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException, EventException{
        OccupancyIntegrator oi = getOccupancyIntegrator();
        EventCoordinator ec = getEventCoordinator();
        
        if(permit == null || period == null || ua == null){
            throw new BObStatusException("Cannot revoke null permit or with null revoking user or with null permit file");
        }
        if(permit.getPeriodID() != period.getPeriodID()){
            throw new BObStatusException("Permit's period ID does match passed parent periodID");
        }
        if(!permissionsCheckpointRevokePermit(permit, ua)){
            throw new AuthorizationException("User lacks sufficient privileges to revoke permit: must be system admin, manager, or issuing officer");
        }
        
        // permit must be finalized and not nullified and not deactivated
        if(!permit.isActive()){
            throw new BObStatusException("Deactivated certificates cannot be revoked");
        }
        if(permit.getFinalizedts() == null){
            throw new BObStatusException("Certificates must be finalized before being revoked");
        }
        if(permit.getNullifiedTS() != null){
            throw new BObStatusException("Nullified certificates cannot be revoked");
        }
        
        // do the revocation
        oi.updateOccPermitRevocation(permit);
        
        // log the event
        EventCategory evCatRevoke = ec.getEventCategory(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("permit_revocation_eventcat")));
        EventCnF evRevoke = ec.initEvent(period, evCatRevoke);
        StringBuilder sb = new StringBuilder();
        sb.append("Certificate Revoked with reference No. ");
        sb.append(permit.getReferenceNo());
        sb.append(" by officer: ");
        sb.append(ua.getUserHuman().getName());
        sb.append(" with reason: ");
        sb.append(permit.getRevokedReason());
        evRevoke.setDescription(sb.toString());
        
        List<EventCnF> evList = ec.addEvent(evRevoke, period, ua);
        if(evList != null && !evList.isEmpty()){
            ec.linkEventToEventLinked(evList.get(0), permit );
        }
        getOccupancyCacheManager().flush(permit);
        
    }

    /**
     * Factory for OccLocationDescriptors
     *
     * @return
     */
    public OccLocationDescriptor getOccLocationDescriptorSkeleton() {
        return new OccLocationDescriptor(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                .getString("locationdescriptor_implyfromspacename")));
    }

    /**
     * Logic intermediary for creating new occ location descriptors
     *
     * @param old
     * @return
     * @throws IntegrationException
     */
    public int addNewLocationDescriptor(OccLocationDescriptor old) throws IntegrationException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        int freshLocID = 0;
        freshLocID = oii.insertLocationDescriptor(old);

        return freshLocID;
    }
    
    
    /**
     * Updates a record of the occ location descriptor table
     * @param loc
     * @param ua 
     */
    public void updateLocationDesriptor(OccLocationDescriptor loc, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(loc == null || ua == null){
            throw new BObStatusException("Cannot update a null location descriptor or with a null user");
        }
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        OccInspectionCacheManager oicm = getOccInspectionCacheManager();
        
        
        oii.updateLocationDescriptor(loc);
        
        oicm.flush(loc);
        
    }

    /**
     * Updates DB to mark the passed in FieldInspection the governing one in the
     * given OccPeriod
     *
     * @param period
     * @return the governing Inspection
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public FieldInspectionLight designateGoverningInspection(OccPeriodDataHeavy period) throws BObStatusException {
        List<FieldInspectionLight> inspectionList = period.getInspectionList();
        FieldInspectionLight selIns = null;
        // logic for determining the currentOccInspection
        if (inspectionList != null) {
            if (inspectionList.size() == 1) {
                selIns = inspectionList.get(0);
            } else {
                Collections.sort(inspectionList);
                for (FieldInspectionLight ins : inspectionList) {
                    // TODO: if you are refactoring this, isActive used to be checked here!
                    selIns = ins;
                }
            }
        }
        try {
            if (period.getGoverningInspection() != null && selIns != null) {
                if (selIns.getInspectionID() != period.getGoverningInspection().getInspectionID()) {
                    activateOccInspection(selIns);
                }
            }
        } catch (IntegrationException ex) {
            throw new BObStatusException("Cannot designate governing inspection");
        }
        return selIns;
    }

    /**
     * Initialization method for creating a of an OccPeriod with sensible
     * default values for first insertion into DB
     *
     * Also attaches an initialization event to the occ period
     *
     * @param pu
     * @param period
     * @param ua
     * @param openingNotes becomes opening event description
     * @return
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.EventException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     * @throws com.tcvcog.tcvce.domain.BlobException
     */
    public int insertOccPeriod(PropertyUnit pu,
            OccPeriod period,
            UserAuthorized ua,
            String openingNotes)
            throws IntegrationException, BObStatusException, EventException, AuthorizationException, BlobException {
        SystemIntegrator si = getSystemIntegrator();
        OccupancyIntegrator oi = getOccupancyIntegrator();

        if (pu == null || period == null || period.getPropertyUnitID() == 0 || ua == null) {
            throw new BObStatusException("cannot create new occ period with null period, type, unit of id 0, or user");
        }

        // Check for alignment between property 
        PropertyCoordinator pc = getPropertyCoordinator();
        Property targetProp = pc.getProperty(pu.getParcelKey());
        if (targetProp != null) {
            if (targetProp.getMuni().getMuniCode() != ua.getKeyCard().getGoverningAuthPeriod().getMuni().getMuniCode()) {
                throw new AuthorizationException("User not allowed to create a permit file on a unit attached to a property that's not in the current UMAP's authorized muni");
            }
        } else {
            throw new BObStatusException("Could not load the target unit's parent property for business logic checks;");
        }

        if (!permissionsCheckpointOpenOccPeriod(ua)) {
            throw new AuthorizationException("User does not have permission to create a permit file on this property unit");
        }

        period.setStartDate(LocalDateTime.now());
        period.setEndDate(null);
        period.setStartDateCertifiedBy(null);
        period.setStartDateCertifiedTS(null);

        period.setEndDateCertifiedBy(null);
        period.setEndDateCertifiedTS(null);

        period.setCreatedBy(ua);
        period.setLastUpdatedBy(ua);

        period.setSource(si.getBOBSource(Integer.parseInt(
                getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                        .getString("occPeriodNewInternalBOBSourceID"))));

        System.out.println("OccupancyCoordinator.insertOccPeriod | period: " + period);
        EventCategory initEC = period.getOriginationEventCategory();
        // send to integrator!
        int freshID = oi.insertOccPeriod(period);
        period = getOccPeriod(freshID, ua);

        if (initEC != null) {
            EventCoordinator ec = getEventCoordinator();
            EventCnF ev = ec.initEvent(period, initEC);
            // write opening notes from the occ period creation form
            if (openingNotes != null && ev != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("Notes from user during opening: ");
                sb.append(openingNotes);
                ev.appendToDescription(sb.toString());
                ev.setCreatedBy(ua);
                ev.setLastUpdatedBy(ua);
                ev.setOccPeriodID(freshID);
                ec.addEvent(ev, period, ua);
            }
        }

        return freshID;
    }

    /**
     * Factory method for creating new OccPermitApplications
     *
     * @param muniCode
     * @return
     * @throws IntegrationException If an error occurs while generating a
     * control code
     */
    public OccPermitApplication initOccPermitApplication(int muniCode) throws IntegrationException {
        SystemCoordinator sc = getSystemCoordinator();
        OccPermitApplication occpermitapp = new OccPermitApplication();
        occpermitapp.setPublicControlCode(sc.generateControlCodeFromTime());
        occpermitapp.setSubmissionDate(LocalDateTime.now());
        return occpermitapp;
    }

    /**
     * Logic pass through method for updates on the OccPeriod
     *
     * @param period
     * @param ua
     * @throws IntegrationException
     * @throws BObStatusException if the OccPeriod is authorized
     */
    public void editOccPeriod(OccPeriod period, UserAuthorized ua) throws IntegrationException, BObStatusException {
        OccupancyIntegrator oi = getOccupancyIntegrator();

        // disable authorization
//        if (period.getAuthorizedTS() != null) {
//            throw new BObStatusException("Cannot change period type or manager on an authorized period; the period must first be unauthorized");
//        }
        if (period.getStartDate() != null && period.getEndDate() != null && period.getStartDate().isAfter(period.getEndDate())) {
            throw new BObStatusException("An permit file's start date must be before its end date");
        }
        period.setLastUpdatedBy(ua);

        oi.updateOccPeriod(period);
        getOccupancyCacheManager().flush(period);
    }

    /**
     * Attaches a note to the given Occ Period.
     *
     * @param mbp
     * @param period whose note field contains the properly formatted note that
     * includes all old note text. This is best done by a call to the
     * SystemCoordinator's appendNoteBlock() method
     * @throws IntegrationException
     */
    public void attachNoteToOccPeriod(MessageBuilderParams mbp, OccPeriod period) throws BObStatusException, IntegrationException {
        OccupancyIntegrator oi = getOccupancyIntegrator();
        SystemCoordinator sc = getSystemCoordinator();

        if (period == null || mbp == null) {
            throw new BObStatusException("Cannot append if notes, occperiod, or user are null");
        }

        period.setNotes(sc.appendNoteBlock(mbp));
        period.setLastUpdatedBy(mbp.getUser());
        oi.updateOccPeriod(period);
        getOccupancyCacheManager().flush(period);
    }

    public void activateOccInspection(FieldInspectionLight is) throws IntegrationException {
        // Nothing to do here yet

    }

    /**
     * Sets boolean requirementSatisfied on an OccPermitApplication based on the
     * application reason, the person requirement for that reason, and the
     * PersonTypes of the Persons attached to the application. Also checks for
     * Applicant. If Preferred Contact is null, then it sets the Preferred
     * Contact equal to the applicant
     *
     * @param opa
     * @return the sanitize OccPermitApplication
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public OccPermitApplication verifyOccPermitPersonsRequirement(OccPermitApplication opa) throws BObStatusException {
        OccAppPersonRequirement pr = opa.getReason().getPersonsRequirement();
        pr.setRequirementSatisfied(false); // If we meet the requirement, we will reach the end and set this to true.

        if (opa.getApplicantPerson() == null) {
            throw new BObStatusException("Please specify an applicant.");
        }

        if (opa.getPreferredContact() == null) {

// TODO: finsih me for humanization
//            opa.setPreferredContact(opa.getApplicantPerson());
        }

        List<PersonType> applicationPersonTypes = new ArrayList<>();
        // TODO: finish for humanization
//        for (PersonOccApplication applicationPerson : opa.getAttachedPersons()) {
//            if (applicationPerson.getFirstName() == null || applicationPerson.getFirstName().contentEquals("")) {
//                throw new BObStatusException("The first name field is not optional. "
//                        + "If you are filling in the name of a business, "
//                        + "please put your business\' full name in the first name field.");
//            }
//            applicationPersonTypes.add(applicationPerson.getApplicationPersonType());
//        }

        for (PersonType personType : pr.getRequiredPersonTypes()) {
            if (!applicationPersonTypes.contains(personType)) {
                throw new BObStatusException("Please specify a(n) " + personType.getLabel());
            }
        }
        pr.setRequirementSatisfied(true);//we've reached the end, the requirement was satisfied.
        return opa;
    }

    /**
     * Inserts an application to the database and attaches it to the default
     *
     * @param application
     * @param ua
     * @return the application ID
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.EventException
     * @throws com.tcvcog.tcvce.domain.BlobException
     */
    public int insertOccPermitApplication(OccPermitApplication application, UserAuthorized ua)
            throws IntegrationException, AuthorizationException,
            BObStatusException, EventException, BlobException {

        OccupancyIntegrator opi = getOccupancyIntegrator();

        OccPeriod connectedPeriod = null;

        if (getSessionBean().getSessUser() != null) {
            //if we are in the middle of an internal session, the muni will be stored in the SessMuni field.
            connectedPeriod = getOccPeriod(getSessionBean().getSessMuni().getDefaultOccPeriodID(), ua);
        } else {
            //if sessUser is null, we're in an external session, which stores the current muni in sessMuniQueued
            MunicipalityCoordinator mc = getMuniCoordinator();
            UserCoordinator uc = getUserCoordinator();

            MunicipalityDataHeavy temp = mc.assembleMuniDataHeavy(getSessionBean().getSessMuniQueued(), uc.auth_getPublicUserAuthorized());

            connectedPeriod = getOccPeriod(temp.getDefaultOccPeriodID(), ua);

        }
        application.setConnectedPeriod(connectedPeriod);

        int applicationId = opi.insertOccPermitApplication(application);

        application.setId(applicationId);

        insertOccApplicationPersonLinks(application);

        return applicationId;

    }

    /**
     * Inserts a new
     *
     * @param application
     * @param notes
     * @return the ID of the newly inserted OccPeriod
     * @throws IntegrationException
     * @throws AuthorizationException
     * @throws BObStatusException
     * @throws EventException
     * @throws InspectionException
     * @throws ViolationException
     * @throws com.tcvcog.tcvce.domain.BlobException
     */
    public int attachApplicationToNewOccPeriod(OccPermitApplication application, String notes)
            throws IntegrationException, AuthorizationException,
            BObStatusException, EventException,
            InspectionException, ViolationException, BlobException {

        PropertyCoordinator pc = getPropertyCoordinator();
        MunicipalityCoordinator mc = getMuniCoordinator();
        SystemIntegrator si = getSystemIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        UserAuthorized user = getSessionBean().getSessUser();
        OccupancyIntegrator oi = getOccupancyIntegrator();

        Property prop = pc.getPropertyUnitWithProp(application.getApplicationPropertyUnit().getUnitID()).getProperty();

        MunicipalityDataHeavy muni = mc.assembleMuniDataHeavy(prop.getMuni(), user);
//        TODO: FIX ME POST HUMANIZATION

//        OccPeriod connectedPeriod = insertOccPeriod(
//                prop,
//                application.getApplicationPropertyUnit(),
//                application.getReason().getProposalPeriodType(),
//                user,
//                muni);
//
//        connectedPeriod.setNotes(sc.formatAndAppendNote(user, notes, connectedPeriod.getNotes()));
//
//        connectedPeriod.setSource(si.getBOBSource(
//                Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
//                        .getString("occPeriodPublicUserBOBSourceID"))));
//
//        int newPeriodID = addOccPeriod(connectedPeriod, user);
//
//        //Now we need to update the Application with the fact that it was ttached
//        connectedPeriod.setPeriodID(newPeriodID);
//        application.setConnectedPeriod(connectedPeriod);
        MessageBuilderParams mcc = new MessageBuilderParams();
        mcc.setUser(getSessionBean().getSessUser());
        mcc.setExistingContent(application.getExternalPublicNotes());
        if (application.getStatus() == OccApplicationStatusEnum.NewUnit) {
            mcc.setHeader(getResourceBundle(Constants.MESSAGE_TEXT).getString("acceptedNewUnitOccPermitApplicationHeader"));
            mcc.setExplanation(getResourceBundle(Constants.MESSAGE_TEXT).getString("acceptedNewUnitOccPermitApplicationExplanation"));
        } else {
            //If it's not attached to a new unit, it should be attached to an existing one.
            mcc.setHeader(getResourceBundle(Constants.MESSAGE_TEXT).getString("acceptedExistingUnitOccPermitApplicationHeader"));
            mcc.setExplanation(getResourceBundle(Constants.MESSAGE_TEXT).getString("acceptedExistingUnitOccPermitApplicationExplanation"));
        }
        mcc.setNewMessageContent(notes);

        application.setExternalPublicNotes(sc.appendNoteBlock(mcc));

        oi.updateOccPermitApplication(application);

        return 0;

    }

    /**
     * Inserts all the persons attached to an OccPermitApplication into the
     * occpermitapplicationperson table. The default value for the applicant
     * column is false, and that column will be set to true when the applicant
     * person is the same as a person within the OccPermitApplication's
     * attachedPersons variable. The boolean for the preferred contact is set
     * similarly. WARNING: Assumes all person objects are already in the
     * database. If any personID == 0, you're going to get an integration
     * Exception.
     *
     * @param application
     * @throws IntegrationException
     */
    public void insertOccApplicationPersonLinks(OccPermitApplication application) throws IntegrationException {

        OccupancyIntegrator oi = getOccupancyIntegrator();

        //: TODO update for humanization
//        List<PersonOccApplication> applicationPersons = application.getAttachedPersons();
//        for (PersonOccApplication person : applicationPersons) {
        //see javadoc
//            if (person.getPersonID() == 0) {
//                throw new IntegrationException("OccupancyCoordinator.insertOccApplicationPersonLinks() detected a person not yet in the database."
//                        + " Please insert persons into the database before running this method!");
//            }
        /* If the person  is the applicantPerson on the 
            OccPermitApplication, set applicant to true*/
//            person.setApplicant(application.getApplicantPerson().getHumanID() == person.getHumanID());

        /* If the person is the preferredContact on the 
            OccPermitApplication, set preferredcontact to true */
//            person.setPreferredContact(application.getPreferredContact().getPersonID() == person.getPersonID());
//
//            oi.insertOccApplicationPerson(person, application.getId());
//        }
    }

    public void updateOccPermitApplicationPersons(OccPermitApplication opa) throws IntegrationException {

        PersonIntegrator pi = getPersonIntegrator();
        OccupancyIntegrator oi = getOccupancyIntegrator();
        PropertyIntegrator pri = getPropertyIntegrator();
        PropertyCoordinator pc = getPropertyCoordinator();
        // TODO Update for humanization
//        
//        List<PersonOccApplication> existingList = pi.getPersonOccApplicationListWithInactive(opa);
//
//        PersonOccApplication applicationPerson = new PersonOccApplication();
//
//        for (PersonOccApplication existingPerson : existingList) {
//
//            boolean removed = true;
//
//            Iterator itr = opa.getAttachedPersons().iterator();
//
//            while (itr.hasNext()) {
//
//                applicationPerson = (PersonOccApplication) itr.next();

        /* If the person  is the applicantPerson on the 
                    OccPermitApplication, set applicant to true*/
//                applicationPerson.setApplicant(opa.getApplicantPerson() != null && opa.getApplicantPerson().equals(applicationPerson));

        /* If the person is the preferredContact on the 
                    OccPermitApplication, set preferredcontact to true */
//                applicationPerson.setPreferredContact(opa.getPreferredContact() != null && opa.getPreferredContact().equals(applicationPerson));
//
//                if (applicationPerson.getPersonID() == 0) {
//
//                    applicationPerson.setPersonType(applicationPerson.getApplicationPersonType());
//
//                    Property prop = pc.getProperty(opa.getApplicationPropertyUnit().getPropertyID());
//
//                    applicationPerson.setMuniCode(prop.getMuni().getMuniCode());
//
//                    applicationPerson.setPersonID(pi.insertPerson(applicationPerson));
//
//                    oi.insertOccApplicationPerson(applicationPerson, opa.getId());
        //We've inserted this new person to the database already. 
        //Let's remove them so we don't insert them every time the for loop fires
//                    itr.remove();
//                    break;
//                } else if (applicationPerson.getPersonID() == existingPerson.getPersonID()) {
//                    removed = false;
//
//                    applicationPerson.setLinkActive(true);
//
//                    pi.updatePerson(applicationPerson);
//
//                    oi.updatePersonOccPeriod(applicationPerson, opa);
//
//                    break;
//                }
//            }
//            if (removed == true) {
//
//                //we never found it in the while loop above, it's been removed
//                existingPerson.setLinkActive(false);
//                oi.updatePersonOccPeriod(existingPerson, opa);
//
//            }
//        }
    }

    /**
     * For inter-coordinator processing only! I get called by the EvCoor during
     * EventCnF insertion
     *
     * @param evList
     * @param period
     * @param ua
     * @return a reference to the same list that was passed in with any
     * additional events added to the queue for insertion by the
     * EventCoordinator
     * @throws IntegrationException
     */
    protected List<EventCnF> addEvent_processForOccDomain(List<EventCnF> evList, OccPeriod period, UserAuthorized ua) throws IntegrationException {
        // No guts yet!

        return evList;
    }

    /**
     * Extracts all active occ permit types
     *
     * @param requireActive
     * @return the list
     * @throws IntegrationException
     */
    public List<OccPermitType> getOccPermitTypeList(boolean requireActive) throws IntegrationException {
        OccupancyIntegrator oi = getOccupancyIntegrator();
        return oi.getOccPermitTypeList(requireActive);
    }

    /**
     * Extracts all active occ permit types
     *
     * @param muni
     * @param requireActive
     * @return the list
     * @throws IntegrationException
     */
    public List<OccPermitType> getOccPermitTypeListComplete(Municipality muni, boolean requireActive) throws IntegrationException {
        OccupancyIntegrator oi = getOccupancyIntegrator();
        return oi.getOccPermitTypeListComplete(muni, requireActive);
    }

    /**
     * Factory for OCcPermit Type objects
     *
     * @param umap
     * @return permit type with id = 0
     */
    public OccPermitType getOccPermitTypeSkeleton(UserMuniAuthPeriod umap) {
        OccPermitType occPermitType = new OccPermitType();
        
        occPermitType.setPermittable(true);
        occPermitType.setRequireInspectionPass(true);
        occPermitType.setCreatedby_UMAP(umap);
        occPermitType.setLastUpdatedby_UMAP(umap);
        occPermitType.setMuni(umap.getMuni());
        return occPermitType;
    }

    /**
     * To update permit type
     *
     * @param permitType
     * @param umap
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws AuthorizationException
     */
    public void updateOccPermitType(OccPermitType permitType, UserMuniAuthPeriod umap) throws IntegrationException, BObStatusException, AuthorizationException {
        if (Objects.isNull(permitType) || Objects.isNull(umap)) {
            throw new BObStatusException("Cannot update OccPermitType with null permitType or UMAP");
        }
        if (umap.getRole() != RoleType.SysAdmin) {
            throw new AuthorizationException("UMAP must be rank: Sys Admin to update PermitType");
        }
        permitType.setLastUpdatedby_UMAP(umap);

        OccupancyIntegrator oi = getOccupancyIntegrator();
        oi.updateOccPermitType(permitType);
        getOccupancyCacheManager().flush(permitType);
    }

    /**
     * Insertion point for OccPermit Type
     *
     * @param permitType
     * @param umap
     * @return OccPermitType
     * @throws IntegrationException
     * @throws AuthorizationException
     * @throws BObStatusException
     */
    public OccPermitType insertOccPermitType(OccPermitType permitType, UserMuniAuthPeriod umap) throws IntegrationException, BObStatusException, AuthorizationException {
        if (Objects.isNull(permitType) || Objects.isNull(umap)) {
            throw new BObStatusException("Cannot insert OccPermit Type with null permit type or UMAP");
        }
        if (umap.getRole() != RoleType.SysAdmin) {
            throw new AuthorizationException("UMAP must be rank: Sys Admin to insert permit type");
        }
        permitType.setCreatedby_UMAP(umap);
        permitType.setLastUpdatedby_UMAP(umap);

        OccupancyIntegrator oi = getOccupancyIntegrator();
        permitType.setTypeID(oi.insertOccPermitType(permitType));
        return permitType;
    }

    /**
     * To deactivate OccPermit Type
     *
     * @param occPermitType
     * @param umap
     * @throws BObStatusException
     * @throws AuthorizationException
     * @throws IntegrationException
     */
    public void deactivateOccPermitType(OccPermitType occPermitType, UserMuniAuthPeriod umap) throws BObStatusException, AuthorizationException, IntegrationException {
        if (Objects.isNull(occPermitType) || Objects.isNull(umap)) {
            throw new BObStatusException("Cannot deactivate OccPermit Type with null permitType or UMAP");
        }
        if (umap.getRole() != RoleType.SysAdmin) {
            throw new AuthorizationException("UMAP must be rank: Sys Admin to deactivate OccPermit Type");
        }
        occPermitType.setDeactivatedBy_UMAP(umap);
        SystemIntegrator si = getSystemIntegrator();
        si.deactivateUMAPTrackedEntity(occPermitType);
        getOccupancyCacheManager().flush(occPermitType);
    }

} // close class

