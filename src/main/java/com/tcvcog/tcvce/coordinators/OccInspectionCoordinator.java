/*
 * Copyright (C) 2021 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.coordinators;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.EventException;
import com.tcvcog.tcvce.domain.InspectionException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.domain.SearchException;
import com.tcvcog.tcvce.entities.CECaseDataHeavy;
import com.tcvcog.tcvce.entities.CECase;
import com.tcvcog.tcvce.entities.CodeSet;
import com.tcvcog.tcvce.entities.EventRealm;
import com.tcvcog.tcvce.entities.EnforceableCodeElement;
import com.tcvcog.tcvce.entities.EventCategory;
import com.tcvcog.tcvce.entities.EventCnF;
import com.tcvcog.tcvce.entities.IFace_EventHolder;
import com.tcvcog.tcvce.entities.IFace_inspectable;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.MunicipalityDataHeavy;
import com.tcvcog.tcvce.entities.Property;
import com.tcvcog.tcvce.entities.PropertyDataHeavy;
import com.tcvcog.tcvce.entities.PropertyUnit;
import com.tcvcog.tcvce.entities.RoleType;
import com.tcvcog.tcvce.entities.User;
import com.tcvcog.tcvce.entities.UserAuthorized;
import com.tcvcog.tcvce.entities.UserMuniAuthPeriod;
import com.tcvcog.tcvce.entities.occupancy.*;
import com.tcvcog.tcvce.entities.reports.ReportConfigOccInspection;
import com.tcvcog.tcvce.integration.SystemIntegrator;
import com.tcvcog.tcvce.integration.OccChecklistIntegrator;
import com.tcvcog.tcvce.integration.OccInspectionIntegrator;
import com.tcvcog.tcvce.util.Constants;
import com.tcvcog.tcvce.util.StringUtil;
import com.tcvcog.tcvce.util.viewoptions.ViewOptionsOccChecklistItemsEnum;
import com.tcvcog.tcvce.occupancy.application.FieldInspectionReInspectionConfig;
import com.tcvcog.tcvce.integration.OccInspectionCacheManager;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Business logic reservoir for field inspections, formerly called Occupancy
 * Inspections but since field inspections are used for both silos, that
 * distinction is now moot
 *
 * @author Ellen Bascomb of apt 31Y
 */
public class OccInspectionCoordinator extends BackingBeanUtils implements Serializable {

    final static String NO_ELEMENT_CATEGORY_TITLE = "Uncategorized";
    final static String SPACE = " ";
    final static boolean COMMENCE_SPACE_WITH_DEF_FAIL_FINDINGS = false;
    final static int DEFAULT_PHOTO_WIDTH = 600;
    final static int DEFAULT_INSPECTION_DURATION_HOURS = 1;

    public OccInspectionCoordinator() {
    }

    /**
     * Permissions check for permissions to edit a checklist generally, which
     * controls adding new checklist controls. Note the companion checkpoint
     * called permissionsCheckpointChecklistEdit which requires a checklist
     * object and is used during permissions determinations pertaining to a
     * specific checklist
     *
     * @param ua requring edit access
     * @return is permission granted?
     */
    public boolean permissionCheckpointChecklistEditGeneral(UserAuthorized ua) {
        if (ua == null) {
            return false;
        }
        if (ua.getKeyCard().isHasSysAdminPermissions()) {
            return true;
        }
        if (ua.getCrossMuniViewAuthorizationTS() != null) {
            return false;
        }
        if (ua.getGoverningMuniProfile().isManagerRequiredChecklistEdit() && !ua.getKeyCard().isHasMuniManagerPermissions()) {
            return false;
        }
        return true;
    }

    /**
     * Permissions check for editing a checklist: user must be authorized in the
     * muni who own the checklist and the muni profile determines if user must
     * have rank: manager User must be the
     *
     * @param temp cannot be null
     * @param ua cannot be null
     * @return
     */
    public boolean permissionsCheckpointChecklistEdit(OccChecklistTemplate temp, UserAuthorized ua) {
        if (temp == null || ua == null) {
            return false;
        }
        if (ua.getCrossMuniViewAuthorizationTS() != null) {
            return false;
        }
        if (ua.getKeyCard().isHasSysAdminPermissions()) {
            return true;
        }
        if (!ua.getKeyCard().isHasMuniStaffPermissions()) {
            return false;
        }
        // users can only edit checklists owned by their muni
        if (temp.getMuni().getMuniCode() == ua.getKeyCard().getGoverningAuthPeriod().getMuni().getMuniCode()) {
            if (ua.getGoverningMuniProfile().isManagerRequiredChecklistEdit() && !ua.getKeyCard().isHasMuniManagerPermissions()) {
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Permissions check for editing a checklist ancillary components, which
     * include space types
     *
     * @param ua cannot be null
     * @return
     */
    public boolean permissionsCheckpointChecklistEditAncillaryObjects(UserAuthorized ua) {
        if (ua == null) {
            return false;
        }
        if (ua.getCrossMuniViewAuthorizationTS() != null) {
            return false;
        }
        if (ua.getKeyCard().isHasSysAdminPermissions()) {
            return true;
        }
        if (!ua.getKeyCard().isHasMuniStaffPermissions()) {
            return false;
        }
        if (ua.getGoverningMuniProfile().isManagerRequiredChecklistEdit() && !ua.getKeyCard().isHasMuniManagerPermissions()) {
            return false;
        }
        return true;
    }

    /**
     * Permissions logic for adding "raw" inspections to a ce case or occ
     * period. No muni-specific settings. Allow sys admin and muni staff role
     * floor
     *
     * @param ua
     * @return
     */
    public boolean permissionsCheckpointAddUpdateFieldInspectionMetadata(UserAuthorized ua) {
        if (ua == null) {
            return false;
        }
        if (ua.getCrossMuniViewAuthorizationTS() != null) {
            return false;
        }
        if (ua.getKeyCard().isHasSysAdminPermissions()) {
            return true;
        }
        if (!ua.getKeyCard().isHasMuniStaffPermissions()) {
            return false;
        }
        return true;
    }
    
    

    /**
     * Permissions logic for determining rights for user to conduct a field
     * inspection which should be checked when assigning an inspector to a field
     * inspection and when adding or updating occ inspected spaces
     *
     * @param ua
     * @return
     */
    public boolean permissionsCheckpointConductFieldInspection(UserAuthorized ua) {
        if (ua == null) {
            return false;
        }
        if (ua.getCrossMuniViewAuthorizationTS() != null) {
            return false;
        }
        if (ua.getKeyCard().isHasSysAdminPermissions()) {
            return true;
        }
        if (!ua.getKeyCard().isHasMuniStaffPermissions()) {
            return false;
        }
        if (ua.getGoverningMuniProfile().isRequireCodeOfficerTrueConductInspection() && !ua.getKeyCard().isHasEnfOfficialPermissions()) {
            return false;
        }
        return true;
    }

    /**
     * Permissions logic for determining rights of given user to finalize the
     * given inspection
     *
     * @param ua
     * @return
     */
    public boolean permissionsCheckpointFinalizeFieldInspection(UserAuthorized ua) {
        if (ua == null) {
            return false;
        }
        if (ua.getCrossMuniViewAuthorizationTS() != null) {
            return false;
        }
        if (ua.getKeyCard().isHasSysAdminPermissions()) {
            return true;
        }
        if (!ua.getKeyCard().isHasMuniStaffPermissions()) {
            return false;
        }
        if (ua.getGoverningMuniProfile().isManagerRequiredInspectionFinalize() && !ua.getKeyCard().isHasMuniManagerPermissions()) {
            return false;
        }
        if (ua.getGoverningMuniProfile().isRequireCodeOfficerTrueFinalizeInspection() && !ua.getKeyCard().isHasEnfOfficialPermissions()) {
            return false;
        }
        return true;
    }

    /**
     * Permissions logic for determining given user rights to modify target
     * muni's codebook during the clone checklist operation
     *
     * @param ua
     * @return
     */
    public boolean permissionsCheckpointChecklistCloneAddMissingOrdsToTargetCodeBook(UserAuthorized ua) {
        if (ua == null) {
            return false;
        }
        if (ua.getCrossMuniViewAuthorizationTS() != null) {
            return false;
        }
        if (ua.getKeyCard().isHasSysAdminPermissions()) {
            return true;
        }
        return false;
    }
    /**
     * System admins can add from any codebook
     *
     * @param ua
     * @return
     */
    public boolean permissionsCheckpointChecklistCrossCodebookAdd(UserAuthorized ua) {
        if (ua == null) {
            return false;
        }
        if (ua.getKeyCard().isHasSysAdminPermissions()) {
            return true;
        }
        return false;
    }

    /**
     * Logic gate for determinig if the given user can route the given FIN
     *
     * @param fin
     * @param ua
     * @return
     */
    public boolean permissionsCheckpointFieldInspectionRoute(FieldInspectionLight fin, UserAuthorized ua) {
        if (ua == null || fin == null) {
            return false;
        }
        if (ua.getKeyCard().isHasSysAdminPermissions() || ua.getKeyCard().isHasMuniManagerPermissions()) {
            return true;
        }
        if (fin.getInspector() != null && fin.getInspector().getUserID() == ua.getUserID()) {
            return true;
        }
        return false;

    }
    
    /**
     * Logic check for reverting an inspection back to the unassigned pool, 
     * which means on the muni default occ period and adding routing string
     * @param fin
     * @param ua
     * @return 
     */
    public boolean permissionsCheckpointAllowInspectionRevert(FieldInspection fin, UserAuthorized ua) {
        if (ua == null) {
            return false;
        }
        if (ua.getKeyCard().isHasSysAdminPermissions()) {
            return true;
        }
        if(ua.getKeyCard().isHasMuniManagerPermissions()){
            return true;
        }
        if(fin != null && fin.getInspector() != null && fin.getInspector().getUserID() == ua.getUserID()){
            return true;
        }
       
        return false;
    }

    /**
     * Factory of field inspections inspections which will attempt to configure
     * sensible defaults including the default checklist based on the given
     * inspectable. When complete, call
     * inspectionAction_commenceOccupancyInspection(...) with this object and
     * supporting objects
     *
     *
     * @param inspector will get injected into our skeleton as the inspector
     * @param inspectable used to choose the default inspection checklist
     * @param requestor requesting user whose munidataheavy will help derive default
     * inspection for CE domain
     * @return a skeleton object without a DB id for configuration by the user.
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.EventException
     * @throws com.tcvcog.tcvce.domain.BlobException
     */
    public FieldInspection getOccInspectionSkeleton(UserAuthorized inspector, IFace_inspectable inspectable, UserAuthorized requestor) 
            throws IntegrationException, AuthorizationException, BObStatusException, EventException, BlobException {

        MunicipalityCoordinator mc = getMuniCoordinator();
        FieldInspection fin = new FieldInspection();

        fin.setInspector(inspector);
        configureInspectionWithSensibleDateTimes(fin);
        
        OccChecklistTemplate checklist = null;
        if(inspectable != null){
            // hunt for our checklist
            switch (inspectable.getDomainEnum()) {
                case CODE_ENFORCEMENT -> {
                    // the muni itself carries a default CE checklist
                    checklist = mc.assembleMuniDataHeavy(requestor.getKeyCard().getGoverningAuthPeriod().getMuni(), requestor).getDefaultInspectionChecklist();
                }
                case OCCUPANCY -> {
                    // we need our inspectable to be an occ period
                    if (inspectable instanceof OccPeriod op) {
                        checklist = op.getPeriodType().getDefaultInspectionChecklist();
                        if (checklist == null) {
                            System.out.println("OccInspectionCoordinator.getOccInspectionSkeleton | Current inspectable is occ period but its type doesn't have a default checklist!");
                        }
                    } else {
                        System.out.println("OccInspectionCoordinator.getOccInspectionSkeleton | Inspectable is not castable to occ period!");
                    }
                }
                default ->
                    System.out.println("OccInspectionCoordinator.getOccInspectionSkeleton | Inspectable domain misconfigured!");
            }
        }
        fin.setChecklistTemplate(checklist);
        if(checklist != null && checklist.getDefaultInspectionCause() != null){
            System.out.println("Found non-null default cause; injecting " + checklist.getDefaultInspectionCause().getTitle());
            fin.setCause(checklist.getDefaultInspectionCause());
            
        }
        // encoruage use of the field handheld software tool
        if(requestor.getKeyCard().getGoverningAuthPeriod().getMuniProfile().isDispatchInspectionsByDefault()){
            fin.setDispatchOnCreationCommit(true);
        }
        // interestingly, regardless of our default setting, we'll inject a skeleton dispatch
        // in case creator wants to dispatch on fin add anyways
        fin.setDispatch(getOccInspectionDispatchSkeleton(fin, requestor));
        // go get our dispatch
        

        return fin;
    }

    /* ****************************************************************
     * ******  Primary getters for light and heavy variants ***********
     * ****************************************************************
     */
    /**
     * Logic container for retrieving and configuring an FieldInspection, the
     *
     * @param inspectionID
     * @return
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws com.tcvcog.tcvce.domain.BlobException
     */
    public FieldInspectionLight getOccInspectionLight(int inspectionID)
            throws IntegrationException, BObStatusException, BlobException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        BlobCoordinator bc = getBlobCoordinator();

        // get the base object with non-list members
        FieldInspectionLight finLight = oii.getFieldInspectionLight(inspectionID);

        finLight = configureOccInspection(finLight);
        return finLight;
    }

    /**
     * Assembles a list of all OccInspections for a given instance of
     * IFace_inspectable
     *
     * @param inspectable
     * @return
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws com.tcvcog.tcvce.domain.BlobException
     */
    public List<FieldInspectionLight> getOccInspectionLightList(IFace_inspectable inspectable)
            throws IntegrationException, BObStatusException, BlobException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        if (inspectable == null) {
            throw new BObStatusException("OccInspectionCoordinator.getOccInspectionLightList | Cannot retrieve inspections from null inspectable");
        }
        List<Integer> inspectionIDList = oii.getOccInspectionList(inspectable);

        List<FieldInspectionLight> inspectionList = new ArrayList();
        for (Integer id : inspectionIDList) {
            inspectionList.add(getOccInspectionLight(id));
        }

        return inspectionList;
    }

    /**
     * Logic for setting members on the Occupancy Inspection objects
     *
     * @param finLight
     * @return
     * @throws BObStatusException
     */
    private FieldInspectionLight configureOccInspection(FieldInspectionLight finLight) throws BObStatusException, IntegrationException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();

        if (finLight == null) {
            throw new BObStatusException("Cannot configure a null inspection");
        }
        if (finLight.getOccPeriodID() != 0 && finLight.getCecaseID() != 0) {
            throw new BObStatusException("Inspection has nonzero occ period ID and cecase ID! One and only one may be zero.");
        }
        // set our domain based on the IDs from the original FIN record
        if (finLight.getOccPeriodID() != 0) {
            finLight.setDomainEnum(EventRealm.OCCUPANCY);
        } else {
            finLight.setDomainEnum(EventRealm.CODE_ENFORCEMENT);
        }

        // build case/period ID string from our temp fields
        switch (finLight.getDomainEnum()) {
            case CODE_ENFORCEMENT -> {
                StringBuilder sb = new StringBuilder("CE case: ");
                sb.append(finLight.getCeCaseName());
                finLight.setContainingCECaseOccPeriodDescription(sb.toString());
            }
            case OCCUPANCY -> {
                StringBuilder sb = new StringBuilder("Permit file on Unit: ");
                sb.append(finLight.getOccPeriodUnitNo());
                finLight.setContainingCECaseOccPeriodDescription(sb.toString());
            }
        }
 
        int did = oii.getOccInspectionDispatchByInspection(finLight);
        if(did != 0){
            finLight.setDispatch(oii.getOccInspectionDispatch(oii.getOccInspectionDispatchByInspection(finLight)));
        }
        return finLight;
    }

    /**
     * Adaptor method for getting a list of heavy fins from a list of light fins
     *
     * @param finLightList
     * @return
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws BlobException
     */
    public List<FieldInspection> getFieldInspectionDataHeavyList(List<FieldInspectionLight> finLightList) throws IntegrationException, BObStatusException, BlobException {
        List<FieldInspection> finHeavyList = new ArrayList<>();

        if (finLightList != null && !finLightList.isEmpty()) {
            for (FieldInspectionLight finLight : finLightList) {
                finHeavyList.add(getFieldInspectionDataHeavy(finLight));
            }
        }
        return finHeavyList;
    }

    /**
     * Builds the data heavy variant of field inspections which contains the
     * list of inspected spaces and the checklists
     *
     * @param finLight that has been updated
     * @return
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws BlobException
     */
    public FieldInspection getFieldInspectionDataHeavy(FieldInspectionLight finLight) throws IntegrationException, BObStatusException, BlobException {

        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        BlobCoordinator bc = getBlobCoordinator();
        EventCoordinator ec = getEventCoordinator();

        FieldInspection fin = new FieldInspection(finLight);

        // The checklist template provides the raw material to build inspected spaces
        fin.setChecklistTemplate(getChecklistTemplate(fin.getChecklistTemplateID()));

        fin.setInspectedSpaceList(oii.getInspectedSpaceList(fin.getInspectionID()));
        if(fin.getInspectedSpaceList() != null && !fin.getInspectedSpaceList().isEmpty()){
            Collections.sort(fin.getInspectedSpaceList());
            Collections.reverse(fin.getInspectedSpaceList());
        }
        
        fin.setInspectedSpaceListVisible(new ArrayList<>());

        
        boolean allSpacesPassed = true;

        for (OccInspectedSpace inSpace : fin.getInspectedSpaceList()) {
            // setup the internals
            configureOccInspectedSpace(inSpace);
            if (inSpace.getStatus().getStatusEnum() == OccInspectionStatusEnum.VIOLATION
                    || inSpace.getStatus().getStatusEnum() == OccInspectionStatusEnum.NOTINSPECTED) {
                allSpacesPassed = false;
            }
        }
        fin.setReadyForPassedCertification(allSpacesPassed);

        fin.setBlobList(bc.getBlobLightList(fin));

        ec.configureEventLinkedLinkedEventList(fin);

        return fin;
    }

    /* ****************************************************************
     * ******  INSPECTION ACTIONS: Methods used while conducting inspections ***********
     * ****************************************************************
     */
    /**
     * 
     * Flagship pathway for writing a new field inspection to the DB, including
     * during a re-inspection process
     * 
     * Under that Supervises the creation of a new Occupancy Inspection object
     * in the database. The designed flow would be the backing bean calls
     * getOccInspectionSkeleton() and sets member variables on there and then
     * passes it into this method.
     *
     * @param finComing A skeleton of an FieldInspection without an ID number WHICH MAY HAVE
     * in its belly a re-inspection configuration object which if is the case will trigger
     * the necessary internal
     * @param tem can be null. if null, the given inspection must have its own Checklist
     * @param inspectable
     * @param inspector The current user who will become the Inspector CAN BE NULL, in which case the given skeleton must have an inspector
     * @param requestingUser cannot be null
     * @return An FieldInspection object with the ID given in the DB and a
     * configured Template inside
     * @throws InspectionException
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.BlobException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     * @throws com.tcvcog.tcvce.domain.EventException
     */
    public FieldInspection inspectionAction_commenceOccupancyInspection(FieldInspection finComing,
            IFace_inspectable inspectable,
            UserAuthorized requestingUser)
            throws InspectionException, IntegrationException, BObStatusException, BlobException, AuthorizationException, EventException {
        
        OccInspectionIntegrator oii = getOccInspectionIntegrator();

        // object status checks & permissions
        if(finComing == null){
            throw new BObStatusException("Cannot write null incoming inspection");
        }
        if(finComing.getInspector() == null){
            throw new BObStatusException("Cannot write null incoming inspector in fin");
        }
        if(finComing.getChecklistTemplate() == null){
            throw new BObStatusException("Cannot write null incoming checklist in fin");
        }
        if (inspectable == null || requestingUser == null) {
            throw new BObStatusException("cannot commence inspection with null inspectable or requesting user");
        }
        if (!permissionsCheckpointAddUpdateFieldInspectionMetadata(requestingUser)) {
            throw new AuthorizationException("Permission Denied: Current user lacks sufficient privileges to add editing field inspections according to permissionsCheckpointAddUpdateFieldInspectionMetadata");
        }
        
        FieldInspection freshInspection = null; // after DB write
        
        // we need this post object refresh since these are not db written yet
        boolean dispatchOnInit = finComing.isDispatchOnCreationCommit();
        OccInspectionDispatch dis = finComing.getDispatch();
        
        // we can make an inspection from scratch if we need to
        System.out.println("FieldInspectionBB.inspectionAction_commenceOccupancyInspection | passed permissions and object status checks");
        
        // configure our inspection parent -- i.e. its domain
        switch (inspectable.getDomainEnum()) {
            case OCCUPANCY -> {
                finComing.setOccPeriodID(inspectable.getHostPK());
                finComing.setDomainEnum(EventRealm.OCCUPANCY);
            }
            case CODE_ENFORCEMENT -> {
                finComing.setCecaseID(inspectable.getHostPK());
                finComing.setDomainEnum(EventRealm.CODE_ENFORCEMENT);
            }
            case UNIVERSAL ->
                throw new BObStatusException("Cannot initiate new inspection with domain enum set to UNIVERSAL");
        }
        System.out.println("FieldInspectionBB.inspectionAction_commenceOccupancyInspection | domain set: " + finComing.getDomainEnum().getTitle());

        // turned off until public info release is present
//        inspec.setPacc(generateControlCodeFromTime(inspector.getHomeMuniID()));

        if(finComing.getInspectionDate() == null){
            configureInspectionWithSensibleDateTimes(finComing);
        }
        
        // check for reinspection
        if(finComing.getReinspectionConfig() != null){
            System.out.println("FieldInspectionBB.inspectionAction_commenceOccupancyInspection | found re-inspection config; routing to setup inspection using these params");
            // this will write our reinspection ID and setup our pre-inspection notes with this info
            finComing = inspectionAction_setupReinspection(finComing);
        }
            // do the write to the DB and get our fresh one for return
        freshInspection = getFieldInspectionDataHeavy(getOccInspectionLight(oii.insertOccInspection(finComing)));
        System.out.println("FieldInspectionBB.inspectionAction_commenceOccupancyInspection | processed standalone FIN | wrote new FIN to DB with fresh ID: : " + freshInspection.getInspectionID() );
        
        // now that we have a DB written inspection, setup reinspected spaces if needed
        if(finComing.getReinspectionConfig() != null){
            finComing.getReinspectionConfig().setReInspection(freshInspection);
            configureReinspectionSpaces(finComing.getReinspectionConfig());
        }
            
        // if auto-dispatch, write our dispatch
        if(dispatchOnInit){
            if(dis == null){
               dis = getOccInspectionDispatchSkeleton(freshInspection, requestingUser);
            } 
            // configure and write our dispatch
            int freshDispatchID = insertOccInspectionDispatch(freshInspection, dis, requestingUser);
            System.out.println("FieldInspectionBB.inspectionAction_commenceOccupancyInspection | auto-dispatched with fresh ID: " + freshDispatchID );
            // reload
            freshInspection = getFieldInspectionDataHeavy(getOccInspectionLight(freshInspection.getInspectionID()));
        }

        // setup our spaces to auto-deploy and reload if we do in fact auto-deply
        // but ony do this for standalone inspections
        if(inspectionAction_setupAutoDeployableSpacesOnInspectionInit(freshInspection) && finComing.getReinspectionConfig() == null){
            freshInspection = getFieldInspectionDataHeavy(getOccInspectionLight(freshInspection.getInspectionID()));
        }
        System.out.println("FieldInspectionBB.inspectionAction_commenceOccupancyInspection | inspection creation init complete; returning updated object" );
        getOccInspectionCacheManager().flush(freshInspection);
        return freshInspection;
    }
    
    /**
     * Looks through the given inspection's checklist and automatically initiates
     * space inspections for any automatically addable spaces
     * @param fin  canot be null, must have a nonzero ID and must have a checklist and an inspector
     * @return if this method did in fact find a space to auto deploy and requires caller reload
     * inspection
     */
    private boolean inspectionAction_setupAutoDeployableSpacesOnInspectionInit(FieldInspection fin) throws BObStatusException, IntegrationException{
        if(fin == null || fin.getChecklistTemplate() == null || fin.getInspector() == null){
            throw new BObStatusException("Cannot auto-deploy spaces with null inspection, checklist, or inspector"); 
        }
        boolean autoaddedSpace = false;
        
        for(OccSpaceTypeChecklistified ost: fin.getChecklistTemplate().getOccSpaceTypeList()){
            if(ost.isAutoAddOnInspectionInit()){
                System.out.println("OccInspectionCoordinator.inspectionAction_setupAutoDeployableSpacesOnInspectionInit | autodeploying space type checklistified ID: " + ost.getChecklistSpaceTypeID());
                inspectionAction_commenceInspectionOfSpaceTypeChecklistified(fin, fin.getInspector(), ost, OccInspectionStatusEnum.NOTINSPECTED, null);
                autoaddedSpace = true;
            }
        }
        return autoaddedSpace;
        
    }
    
    /**
     * sets the given fin to date of today and time to now and end time to now plus one hour
     * @param fin
     * @return 
     */
    private FieldInspectionLight configureInspectionWithSensibleDateTimes(FieldInspectionLight fin){
        if(fin != null){
            fin.setInspectionDate(java.time.LocalDate.now());
        }
        return fin;
    }

    /**
     * Logic container for updating space element data on all elements in an
     * inspection
     *
     * @param osi
     * @param ua
     * @param oi
     * @param useDefaultFindings when true the ord's default findings will be
     * injected into each failed ord alongside existing findings
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public void inspectionAction_updateSpaceElementData(OccInspectedSpace osi, UserAuthorized ua, FieldInspection oi, boolean useDefaultFindings)
            throws IntegrationException, AuthorizationException, BObStatusException {
        if (osi != null && osi.getInspectedElementList() != null && !osi.getInspectedElementList().isEmpty()) {
            for (OccInspectedSpaceElement oise : osi.getInspectedElementList()) {
                inspectionAction_recordElementInspectionByStatusEnum(oise, ua, oi, useDefaultFindings);
            }
        }
    }

    /**
     * not in use JULY 2023 use inspectionAction_updateOccInspectionSpaceElement
     *
     * @Deprecated
     * @param oise
     * @param fin
     * @throws IntegrationException
     * @throws BObStatusException
     */
    public void inspectionAction_updateSpaceElement(OccInspectedSpaceElement oise, FieldInspection fin)
            throws IntegrationException, BObStatusException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        if (oise == null || fin == null) {
            throw new BObStatusException("Cannot update an inspectd space element with null element or inspection ");
        }
        updateInspectedSpaceElement(oise);
    }

    /**
     * Internal organ for calling on the OccInspectionIntegrator
     * updateInspectedSpaceElement
     * That everybody needs to use to ensure cache consistency
     * 
     * @param oise
     * @throws IntegrationException 
     */
    public void updateInspectedSpaceElement(OccInspectedSpaceElement oise) throws IntegrationException{
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        OccInspectionCacheManager oicm = getOccInspectionCacheManager();
        SystemCoordinator cc = getSystemCoordinator();
        
        // my rich text editor
        oise.setInspectionNotes(cc.cleanRichText(oise.getInspectionNotes()));
        
        oii.updateInspectedSpaceElement(oise);
        
        // dump our caches
        oicm.flushObjectFromCache(oise.getInspectedSpaceID());
        
        
    }
    
 
    
    /**
     * Called by the backing bean when the user selects a space to start
     * inspecting.Effectively creates a new OccInspectedSpace object, fills out
     * some fields automatically, adds it to the passed inspection object, and
     *
     *
     * @param inspection The current inspection
     * @param user The current user--not necessarily the official inspector of
     * the FieldInspection.
     * @param tpe The space type which will have a list of SpaceElements inside
     * it
     * @param initialStatus The initial status of the created OccInspectedSpace.
     * If this
     * @param locDesc A populated location descriptor for the new
     * OccInspectedSpace. Can be an existing location or a new one.
     *
     * @return The sapce type passed in turned into an inspected space with a DB
     * ID
     *
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public OccInspectedSpace inspectionAction_commenceInspectionOfSpaceTypeChecklistified(FieldInspection inspection,
            User user,
            OccSpaceTypeChecklistified tpe,
            OccInspectionStatusEnum initialStatus,
            OccLocationDescriptor locDesc)
            throws IntegrationException, BObStatusException {

        OccInspectionIntegrator oii = getOccInspectionIntegrator();

        if (inspection == null || tpe == null || user == null) {
            throw new BObStatusException("Cannot commence inspection of sapce with null inspection, type, or user");

        }

        // Default value for location descriptor if null
        if (locDesc == null) {
            locDesc = getOccLocationDescriptor(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("locationdescriptor_implyfromspacename")));
        }

        // Create new inspected space and populate fields
        OccInspectedSpace inspectedSpace = new OccInspectedSpace();

        inspectedSpace.setLocation(locDesc);

        inspectedSpace.setAddedToChecklistBy(user);
        // actually this is stamped by now() in db
//        inspectedSpace.setAddedToChecklistTS(LocalDateTime.now());

        // Wrap each CodeElement in an InspectedCodeElement blanket to keep it warm
        List<OccInspectedSpaceElement> inspectedElements;
        inspectedElements = tpe.getCodeElementList().stream().map(element -> {
            OccInspectedSpaceElement inspectedElement = new OccInspectedSpaceElement(element);

            switch (initialStatus) {
                case VIOLATION ->
                    inspectionAction_configureElementForInspectionNoCompliance(
                            inspectedElement,
                            user,
                            inspection,
                            COMMENCE_SPACE_WITH_DEF_FAIL_FINDINGS);
                case PASS ->
                    inspectionAction_configureElementForCompliance(inspectedElement, user, inspection);
                default ->
                    inspectionAction_configureElementForNotInspected(inspectedElement, user, inspection);
            }
            // this is a default setting that as of June 2024 is no longer honored during migration
            // meaning all violated ords will be eligible for migration and can be removed manually if needed
            inspectedElement.setMigrateToCaseOnFail(true);
            return inspectedElement;
        }).collect(Collectors.toList());

        inspectedSpace.setInspectedElementList(inspectedElements);
        inspectedSpace.setType(tpe);
        // With a fully built inspected space, we can record our start of inspection in the DB
        inspectedSpace = oii.recordCommencementOfSpaceInspection(inspectedSpace, inspection);
        System.out.println("OccupancyCoordinator.inspectionAction_commenceSpaceInspection | commenced inspecting of space");

        // now use our convenience method to initiate Inspection of the space's individual elements
        oii.recordInspectionOfSpaceElements(inspectedSpace, inspection);

        // check sequence by retrieving new inspected space and displaying info
//        inspectedSpace = oii.getInspectedSpace(inspectedSpace.getInspectedSpaceID());
        System.out.println("OccupancyCoordinator.inspectionAction_commenceSpaceInspection | retrievedInspectedSpaceID= " + inspectedSpace.getInspectedSpaceID());
        getOccInspectionCacheManager().flush(inspectedSpace);

        return inspectedSpace;
    }
    
   
    

    /**
     * Creates a "customized clone" of an OccInspectedSpace for use during the
     * reinspection process
     *
     * @param reInConfig
     * @param ois
     * @return the freshly Cloned OccInspectedSpace
     * @throws IntegrationException
     */
    public OccInspectedSpace inspectionAction_reInspectOccInspectedSpace(FieldInspectionReInspectionConfig reInConfig,
            OccInspectedSpace ois)
            throws IntegrationException {

        OccInspectionIntegrator oii = getOccInspectionIntegrator();

        // Create new inspected space and populate fields
        OccInspectedSpace reInspectedSpace = new OccInspectedSpace(ois);
        reInspectedSpace.setAddedToChecklistBy(reInConfig.getReInspection().getInspector());
        reInspectedSpace.setAddedToChecklistTS(reInConfig.getReInspection().getEffectiveDateOfRecord());

        // With a fully built inspected space, we can record our start of inspection in the DB
        reInspectedSpace = oii.recordCommencementOfSpaceInspection(reInspectedSpace, reInConfig.getReInspection());

        // Move over the memebers in our oises carefully, clearing the ID so we get a new one for our new OIS
        List<OccInspectedSpaceElement> idLessOISECloneList = new ArrayList();

        if (reInspectedSpace.getInspectedElementList() != null && !reInspectedSpace.getInspectedElementList().isEmpty()) {
            for (OccInspectedSpaceElement oise : reInspectedSpace.getInspectedElementList()) {
                oise.setInspectedSpaceElementID(0);

                switch (oise.getStatusEnum()) {
                    case NOTINSPECTED -> {
                        if (reInConfig.isMigrateUninspectedElements()) {
                            idLessOISECloneList.add(oise);
                        }
                    }
                    case PASS -> {
                        if (!reInConfig.isMigrateOnlyFailedElements()) {
                            oise.setLastInspectedBy(reInConfig.getReInspection().getInspector());
                            oise.setLastInspectedTS(reInConfig.getReInspection().getEffectiveDateOfRecord());
                            oise.setComplianceGrantedBy(reInConfig.getReInspection().getInspector());
                            oise.setComplianceGrantedTS(reInConfig.getReInspection().getEffectiveDateOfRecord());
                            idLessOISECloneList.add(oise);
                        }
                    }
                    case VIOLATION -> {
                        oise.setLastInspectedBy(reInConfig.getReInspection().getInspector());
                        oise.setLastInspectedTS(reInConfig.getReInspection().getEffectiveDateOfRecord());
                        idLessOISECloneList.add(oise);
                    }
                }
            }
        }

        // Build findings
        if (!idLessOISECloneList.isEmpty()) {
            for (OccInspectedSpaceElement oise : idLessOISECloneList) {
                if (reInConfig.isMigrateInspectionFindings()) {
                    if (!reInConfig.isLiteralFindingsCloneNoWrapper()) {
                        if (oise.getInspectionNotes() != null && !oise.getInspectionNotes().equals(Constants.EMPTY_STRING) && !oise.getInspectionNotes().equals(Constants.FMT_SPACE_LITERAL)) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("Original findings from inspection ID ");
                            sb.append(reInConfig.getSourceInspection().getInspectionID());
                            sb.append(" on ");
                            sb.append(reInConfig.getSourceInspection().getEffectiveDateOfRecord());
                            sb.append(" by ");
                            sb.append(reInConfig.getSourceInspection().getInspector().getHuman().getName());
                            sb.append(": '");
                            sb.append(oise.getInspectionNotes());
                            sb.append("'.");
                            oise.setInspectionNotes(sb.toString());
                        }
                    } else {
                        // keep notes the way they were on the original
                    }
                } else {
                    // wipe notes
                    oise.setInspectionNotes(null);
                }
            }
        }

        reInspectedSpace.setInspectedElementList(idLessOISECloneList);

        // now use our convenience method to initiate Inspection of the space's individual elements
        oii.recordInspectionOfSpaceElements(reInspectedSpace, reInConfig.getReInspection());

        // check sequence by retrieving new inspected space and displaying info
//        inspectedSpace = oii.getInspectedSpace(inspectedSpace.getInspectedSpaceID());

        getOccInspectionCacheManager().flush(ois);
        return reInspectedSpace;
    }

    /**
     * Sets the certified fields on an inspection and injects a determination;
     * Implements logic to ensure proper privileges and throws exceptions with
     * useful messages inside
     *
     * @param fin to finalize whose determination object is NOT NULL
     * @param followUpWindow the number of days from now() when Followup event
     * will be placed
     * @param ua doing the certification (must be OP manager or sys admin or
     * better)
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void inspectionAction_finalizeInspection(FieldInspection fin,
            int followUpWindow,
            UserAuthorized ua)
            throws IntegrationException, BObStatusException, AuthorizationException, SearchException, EventException, BlobException {

        if (fin == null || fin.getDetermination() == null || ua == null) {
            throw new BObStatusException("Cannot finalize with null inspection, determination, or user");
        }

        if (!permissionsCheckpointFinalizeFieldInspection(ua)) {
            throw new AuthorizationException("Permission Denied! User forbidden from inspection finalization");
        }

        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        EventCoordinator ec = getEventCoordinator();

        fin.setDeterminationBy(ua);
        fin.setDeterminationTS(LocalDateTime.now());
        fin.setLastUpdatedBy(ua);
        oii.updateOccInspectionFinalizationStatus(fin);

        // EVENT LOGGING CENTRAL
        // setup our inspection logging events and perhaps a failed FIN Follow-up event
        EventCategory evCatFinPass = ua.getMyCredential().getGoverningAuthPeriod().getMuniProfile().getEventCatFINPassed();
        EventCategory evCatFinFail = ua.getMyCredential().getGoverningAuthPeriod().getMuniProfile().getEventCatFINFailed();
        EventCategory evCatFinFailFollowup = ua.getMyCredential().getGoverningAuthPeriod().getMuniProfile().getEventCatFailedFINFollowUp();

        // if we're going to make at least one event, we'll need the event holder from the 
        // Field inspection
        IFace_EventHolder holder = null;
        if (evCatFinPass != null || evCatFinFail != null || evCatFinFailFollowup != null) {
            IFace_inspectable inspectable = extractInspectionParentInspectable(fin, ua);
            holder = ec.castAttemptInspectableToEventHolder(inspectable);

            List<EventCnF> evList = new ArrayList<>();

            StringBuilder sb = new StringBuilder();
            sb.append("Inspection by ");
            sb.append(fin.getInspector().getUserHuman().getName());
            sb.append(" on ");
            sb.append(getPrettyDateNoTime(fin.getEffectiveDateOfRecord()));

            // Create a logging event for passed inspections if the profile requests
            if (fin.getDetermination().isQualifiesAsPassed() && evCatFinPass != null) {
                EventCnF finPassEvent = ec.initEvent(holder, evCatFinPass);
                finPassEvent.configureEventTimesFromStartTime(fin.getEffectiveDateOfRecord(), 0);
                sb.append(" was finalized as PASS with determination: ");
                sb.append(fin.getDetermination().getTitle());
                finPassEvent.appendToDescription(sb.toString());
                evList.addAll(ec.addEvent(finPassEvent, holder, ua));
                // Create a logging event for failed inspections if the profile requests
            } else if (!fin.getDetermination().isQualifiesAsPassed() && evCatFinFail != null) {
                EventCnF finFailEvent = ec.initEvent(holder, evCatFinFail);
                finFailEvent.configureEventTimesFromStartTime(fin.getEffectiveDateOfRecord(), 0);
                sb.append(" was finalized as FAIL with determination: ");
                sb.append(fin.getDetermination().getTitle());
                finFailEvent.appendToDescription(sb.toString());
                evList.addAll(ec.addEvent(finFailEvent, holder, ua));
            }

            // Create a logging event for failed inspection follow up if the profile requests
            if (!fin.getDetermination().isQualifiesAsPassed() && evCatFinFailFollowup != null) {
                EventCnF finFollowup = ec.initEvent(holder, evCatFinFailFollowup);
                finFollowup.configureEventTimesFromStartTime(fin.getEffectiveDateOfRecord().plusDays(followUpWindow), 0);
                sb.append(" was finalized as FAIL with determination: ");
                sb.append(fin.getDetermination().getTitle());
                sb.append(" and now requires a follow-up action determined by the officer");
                finFollowup.appendToDescription(sb.toString());
                evList.addAll(ec.addEvent(finFollowup, holder, ua));
            }
            if (!evList.isEmpty()) {
                for (EventCnF ev : evList) {
                    ec.linkEventToEventLinked(ev, fin);
                }
            }
        }
        getOccInspectionCacheManager().flush(fin);
    }

    /**
     * Getter for OISEs
     *
     * @param eleID
     * @return
     * @throws BObStatusException
     * @throws IntegrationException
     */
    public OccInspectedSpaceElement getOccInspectedSpaceElement(int eleID) throws BObStatusException, IntegrationException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        CodeCoordinator cc = getCodeCoordinator();
        if (eleID == 0) {
            throw new BObStatusException("Cannot fetch element with ID 0");
        }
        OccInspectedSpaceElement oise = oii.getInspectedSpaceElement(eleID);
        oise.setInspectionNotes(sc.cleanRichText(oise.getInspectionNotes()));
        oise.setCannedFindingsCandidates(cc.assembleCannedFindingsByFindingsHolder(oise));
        return oise;

    }

    /**
     * Retrieves a single inspected space from the DB and populates its code elements
     *
     * @param inspectedSpaceID
     * @return
     */
    public OccInspectedSpace getOccInspectedSpace(int inspectedSpaceID) throws BObStatusException, IntegrationException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        OccInspectedSpace ois = null;
        if (inspectedSpaceID != 0) {
            ois = oii.getOccInspectedSpace(inspectedSpaceID);
            if(ois.getInspectedElementList() != null && !ois.getInspectedElementList().isEmpty()){
                for(OccInspectedSpaceElement oise: ois.getInspectedElementList()){
                    configureOccInspectedSpaceElement(oise);
                }
            }
            configureElementDisplay(ois);
        } else {
            throw new BObStatusException("Cannot fetch space with ID = 0");
        }
        return ois;

    }

    /**
     * Configures members and updates an existing record in the occinspection
     * table.The beast!
     *
     * @param oi with changed fields. This method injects lastupdatedby stuff
     * @param ua the user doing the updates
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void updateOccInspection(FieldInspection oi, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        OccInspectionCacheManager oicm = getOccInspectionCacheManager();
        
        if (oi == null || oi.getInspectionID() == 0 || ua == null) {
            throw new BObStatusException("Cannot update an inspection with null inspection or ID of 0 or null use");
        }
        if (!permissionsCheckpointAddUpdateFieldInspectionMetadata(ua)) {
            throw new AuthorizationException("Permission Denied! User forbidden from inspection edits");
        }

        auditInspectionDetails(oi, ua);
        oi.setLastUpdatedBy(ua);
        // database stamps update TS with now()
        oii.updateOccInspection(oi);
        oicm.flushObjectFromCache(oi);
    }

    /**
     * Checks that the start and end times are in chronological order and don't
     * conflict with the date of record, which also has a time;
     *
     * @param fin
     * @param ua
     */
    private void auditInspectionDetails(FieldInspection fin, UserAuthorized ua) throws BObStatusException {
        if (fin == null) {
            throw new BObStatusException("Null inspection cannot be audited");
        }
        if (fin.getInspectionDate()== null) {
            throw new BObStatusException("Inspections must have a date of record");
        }
      

    }

    /**
     * Logic container for ensuring that deactivation is allowed and then doing 
     * the actual deactivation
     *
     * @param ua doing the deactivating; must be either the inspector or OP
     * manager
     * @param oi to be deactivated
     * @param inspectable
     * @throws com.tcvcog.tcvce.domain.BObStatusException for null of any input
     * param
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException inputted ua must
     * be the manager or inspector or have sys admin or better permissions
     */
    public void deactivateOccInspection(UserAuthorized ua, FieldInspectionLight oi, IFace_inspectable inspectable)
            throws BObStatusException, IntegrationException, AuthorizationException {
        if (ua == null || oi == null || oi.getInspector() == null) {
            throw new BObStatusException("Cannot deactivate an inspection with null user, inspec, or period, or their manager/inspector!");
        }
        if (!permissionsCheckpointConductFieldInspection(ua)) {
            throw new AuthorizationException("Permission denied: user forbidden from deactivating inspections under the permissions switch for conducting");
        }
        if (oi.getDetermination() != null && !ua.getKeyCard().isHasSysAdminPermissions()) {
            throw new BObStatusException("A field inspection that has already been finalized cannot be deactivated. Remove finalization first if you must deactivate.");
        }
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        oi.setLastUpdatedBy(ua);
        oi.setDeactivatedBy(ua);
        oi.setDeactivatedTS(LocalDateTime.now());
        oii.deactivateOccInspection(oi);
        // deac any dispatches
        if(oi.getDispatch() != null){
            deactivateOccInspectionDispatch(oi.getDispatch(), ua);
            System.out.println("OccInspectionCoordinator.deactivateOccInspection | Deac of dispatch ID: " + oi.getDispatch().getDispatchID());
        }
        getOccInspectionCacheManager().flush(oi);
    }

    /**
     * Removes the values for the given FieldInspection's determination, detTS,
     * and DetUser
     *
     * @param ua
     * @param oi
     * @param inspectable
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void removeOccInspectionFinalization(UserAuthorized ua, FieldInspection oi, IFace_inspectable inspectable) throws IntegrationException, BObStatusException, AuthorizationException {
        System.out.println("OccInspectionCoordinator.removeOccInspectionFinalization | passed checks, about to remove finalization for inspection ID " + oi.getInspectionID());

        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        EventCoordinator ec = getEventCoordinator();

        if (!permissionsCheckpointFinalizeFieldInspection(ua)) {
            throw new AuthorizationException("Permission Denied! User forbidden from finalizing inspections therefore cannot remove finaliation");
        }

        oi.setDetermination(null);
        oi.setDeterminationBy(null);
        oi.setDeterminationTS(null);
        oii.updateOccInspectionFinalizationStatus(oi);

        IFace_EventHolder holder = ec.castAttemptInspectableToEventHolder(inspectable);

        //scrub our events that may have been created and linked to our now unfinalzied FIN
        ec.deactivateAllEventsLinkedToEventLinkedObject(oi, holder, true, false, ua);
        getOccInspectionCacheManager().flush(oi);

    }

    /**
     * Generator for dispatch objects less an ID which makes them skeletons
     *
     * @param fin I hope we can cope with skeleton fins!
     * @param ua
     * @return
     */
    public OccInspectionDispatch getOccInspectionDispatchSkeleton(FieldInspection fin, UserAuthorized ua) {
        OccInspectionDispatch oid = new OccInspectionDispatch();
        if (fin != null) {
            oid.setInspectionID(fin.getInspectionID());
        }
        return oid;
    }

    /**
     * Basic getter for dispatches
     *
     * @param did
     * @return
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public OccInspectionDispatch getOccInspectionDispatch(int did) throws IntegrationException, BObStatusException {
        OccInspectionIntegrator oic = getOccInspectionIntegrator();
        return oic.getOccInspectionDispatch(did);

    }

    /**
     * Logic intermediary for dispatch insertion
     *
     * @param fin which is being dispatched
     * @param oid
     * @param ua doing the actual dispatching
     * @return
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public int insertOccInspectionDispatch(FieldInspection fin, OccInspectionDispatch oid, UserAuthorized ua)
            throws IntegrationException,
            BObStatusException,
            AuthorizationException {
        
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        
        if (fin == null) {
            throw new IntegrationException("Cannot insert null inspection dispatch and cannot do so with null FIN");
        }
        if(oid == null && fin.getDispatch() == null){
            throw new BObStatusException("Missing dispatch in inspection and method parameter");
        }
        // deal with case in which we need our dispatch from inside our FIN
        if(oid == null){
            oid = fin.getDispatch();
        }
        if (!permissionsCheckpointAddUpdateFieldInspectionMetadata(ua)) {
            throw new AuthorizationException("Permission Denied! Current user is unauthorized to undertake insertOccInspectionDispatch by permissionsCheckpointAddUpdateFieldInspectionMetadata");
        }
        if (fin.getDispatch() != null && fin.getDispatch().getDispatchID() != 0) {
            throw new BObStatusException("The given field inspection contains a dispatch with an ID already!");
        }

        oid.setInspectionID(fin.getInspectionID());

        oid.setCreatedBy(ua);
        oid.setLastUpdatedBy(ua);
        int freshID = oii.insertOccInspectionDispatch(oid);
        getOccInspectionCacheManager().flush(fin);
        return freshID;
    }

    /**
     * Updates a record in the occinspectiondispatch table
     *
     * @param oid
     * @param ua
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void updateOccInspectionDispatch(OccInspectionDispatch oid, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException {
        if (oid == null || ua == null) {
            throw new BObStatusException("Cannot update dispatch with null dispatch or user");
        }

        OccInspectionIntegrator oii = getOccInspectionIntegrator();

        if (!permissionsCheckpointAddUpdateFieldInspectionMetadata(ua)) {
            throw new AuthorizationException("Permission Denied! Current user is unauthorized to undertake updateOccInspectionDispatch by permissionsCheckpointAddUpdateFieldInspectionMetadata");
        }
        oid.setLastUpdatedBy(ua);
        oii.updateOccInspectionDispatch(oid);
        getOccInspectionCacheManager().flush(oid);

    }
    
    

    /**
     * Logic block to set deactivation TS and user on a dispatch
     *
     * @param oid to deactivate
     * @param ua doing the deactivation
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void deactivateOccInspectionDispatch(OccInspectionDispatch oid, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException {
        if (oid == null || ua == null) {
            throw new BObStatusException("Cannot deactivate dispatch with null dispatch or user");
        }
        if (!permissionsCheckpointAddUpdateFieldInspectionMetadata(ua)) {
            throw new AuthorizationException("Permission Denied! Current user is unauthorized to undertake deactivateOccInspectionDispatch by permissionsCheckpointAddUpdateFieldInspectionMetadata");
        }
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        oid.setDeactivatedBy(ua);
        oid.setDeactivatedTS(LocalDateTime.now());
        oid.setLastUpdatedBy(ua);

        oii.updateOccInspectionDispatch(oid);
        
        getOccInspectionCacheManager().flush(oid);

    }

    /**
     * Convenience method for extracting the given inspection's parent
     * inspectable
     *
     * @param finLight
     * @param ua
     * @return
     * @throws IntegrationException
     * @throws BObStatusException
     */
    public IFace_inspectable extractInspectionParentInspectable(FieldInspectionLight finLight, UserAuthorized ua)
            throws IntegrationException, BObStatusException, SearchException, BlobException {
        if (finLight == null) {
            return null;
        }
        if (finLight.getOccPeriodID() != 0) {
            OccupancyCoordinator oc = getOccupancyCoordinator();
            return oc.assembleOccPeriodDataHeavy(oc.getOccPeriod(finLight.getOccPeriodID(), ua), ua);
        } else if (finLight.getCecaseID() != 0) {
            CaseCoordinator cc = getCaseCoordinator();
            return cc.cecase_assembleCECaseDataHeavy(cc.cecase_getCECase(finLight.getCecaseID(), ua), ua);
        }
        return null;
    }

    /**
     * Logic gate for attaching a field-initiated inspection to an actual cecase
     * or occ period. I'll check for case and period status alignment and throw
     * errors if target is closed or in another muni, etc.
     *
     * @param ins which was initiated on the Handheld app and has a non-null raw
     * property string
     * @param cse can be null: target for fin attachment--but this cannot be
     * null when occ period is alo null
     * @param period can be null: target for fin attachment--but this cannot be
     * null when case is also null
     * @param ua doing to attachment.
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void routeFieldInitiatedInspection(FieldInspectionLight ins, CECase cse, OccPeriod period, UserAuthorized ua)
            throws BObStatusException, IntegrationException, AuthorizationException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();

        if (ins == null || ua == null) {
            throw new BObStatusException("Cannot route inspection with null inspection or routing user");
        }

        if (cse == null && period == null) {
            throw new BObStatusException("Cannot attach a field inspection to neither a case nor period! This is a logic goof that devs must fix");
        }

        if (cse != null && period != null) {
            throw new BObStatusException("Cannot attach a field inspection to both a  case and period! This is a logic goof that devs must fix");
        }

        if (!permissionsCheckpointFieldInspectionRoute(ins, ua)) {
            throw new AuthorizationException("User lacks permissions to route this field inspection: user must be system admin, manager, or the inspector");
        }

        // wipe previous occ period
        ins.setOccPeriodID(0);

        if (cse != null) {
            ins.setCecaseID(cse.getCaseID());
        }
        if (period != null) {
            ins.setOccPeriodID(period.getPeriodID());
        }

        ins.setLastUpdatedBy(ua);
        ins.setRoutingByUser(ua);
        oii.updateOccInspectionFieldInitiatedRouting(ins);
        getOccInspectionCacheManager().flush(ins);

    }
    
      /**
     * Generates candidate target munis for an xmuni fin reversion
     * 
     * @param ua
     * @param fin
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public List<Municipality> assembleRervertFinToUnassignedPoolXMuniCandidateList(UserAuthorized ua, FieldInspection fin) throws BObStatusException, IntegrationException{
        MunicipalityCoordinator mc = getMuniCoordinator();
        List<Municipality> candidates = new ArrayList<>();
        
        if(ua == null || fin == null){
            return candidates;
        }
        
        candidates.addAll(mc.assembleMuniListUserIsCodeOfficer(ua));
        candidates.remove(ua.getMyCredential().getGoverningAuthPeriod().getMuni());
        
        return candidates;
    }

    /**
     * Controller method for turning an inspection into one that needs assignment/routing
     * a la field initiated inspections
     * @param ins inspection to revert back to unassigned pool
     * @param muni if NOT null, the given FIN will be routed to this muni's unassigned pool
     * @param ua doing the reverting
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BlobException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void revertFieldInspectionToUnassignedPool(FieldInspection ins, Municipality muni, UserAuthorized ua) 
            throws BObStatusException, IntegrationException, BlobException, AuthorizationException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        MunicipalityCoordinator mc = getMuniCoordinator();
        
        if(!permissionsCheckpointAllowInspectionRevert(ins, ua)){
            throw new AuthorizationException("User is not authorized to revert this inspection; Must be system admin, manager, or the inspector");
        }
        if (ins == null) {
            throw new BObStatusException("Cannot xcer an inspection to a new period with null Inspection or target ID of 0");
        }

        ins.setLastUpdatedBy(ua);
        
        if(muni == null){
            System.out.println("OccInspectionCoordinator.revertFieldInspectionToUnassignedPool | reverting to current muni");
            ins.setOccPeriodID(mc.getMuniDefaultOccPeriodID(ua.getKeyCard().getGoverningAuthPeriod().getMuni().getMuniCode()));
        } else {
            System.out.println("OccInspectionCoordinator.revertFieldInspectionToUnassignedPool | reverting to " + muni.getMuniName());
            ins.setOccPeriodID(mc.getMuniDefaultOccPeriodID(muni.getMuniCode()));
        }
        
        oii.updateOccInspectionRevertToUnassignedPool(ins);

    }
    
    

    /**
     * Logic for setting members on OccInspectedSpace on extraction from DB; I
     * also setup the element tree view
     *
     * @param inSpace
     * @return
     * @throws BObStatusException
     */
    private OccInspectedSpace configureOccInspectedSpace(OccInspectedSpace inSpace) throws BObStatusException {
        SystemIntegrator si = getSystemIntegrator();
        CodeCoordinator cc = getCodeCoordinator();
        boolean atLeastOneElementInspected = false;
        boolean allElementsPass = true;

        
        // organize A tree of ECEs here
        inSpace.setInspectedElementTree(cc.buildTreeFromCodeSet(inSpace.getInspectedElementList()));

        Map<OccInspectionStatusEnum, List<OccInspectedSpaceElement>> elbsm = new HashMap<>();
        elbsm.put(OccInspectionStatusEnum.PASS, new ArrayList<>());
        elbsm.put(OccInspectionStatusEnum.VIOLATION, new ArrayList<>());
        elbsm.put(OccInspectionStatusEnum.NOTINSPECTED, new ArrayList<>());

        for (OccInspectedSpaceElement inSpaceEle : inSpace.getInspectedElementList()) {
            configureOccInspectedSpaceElement(inSpaceEle);
            switch (inSpaceEle.getStatusEnum()) {
                case VIOLATION:
                    allElementsPass = false;
                    elbsm.get(OccInspectionStatusEnum.VIOLATION).add(inSpaceEle);
                    atLeastOneElementInspected = true;
                    break;
                case NOTINSPECTED:
                    elbsm.get(OccInspectionStatusEnum.NOTINSPECTED).add(inSpaceEle);
                    break;
                case PASS:
                    elbsm.get(OccInspectionStatusEnum.PASS).add(inSpaceEle);
                    atLeastOneElementInspected = true;
                    break;
            }
        }

        inSpace.setElementStatusMap(elbsm);

        int iconID = 0;
        if (!atLeastOneElementInspected) {

            inSpace.setStatus(new OccInspectableStatus(OccInspectionStatusEnum.NOTINSPECTED));
            iconID = Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                    .getString(OccInspectionStatusEnum.NOTINSPECTED.getIconPropertyLookup()));
            inSpace.getStatus().setIcon(si.getIcon(iconID));

        } else if (atLeastOneElementInspected && !allElementsPass) {

            inSpace.setStatus(new OccInspectableStatus(OccInspectionStatusEnum.VIOLATION));
            iconID = Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                    .getString(OccInspectionStatusEnum.VIOLATION.getIconPropertyLookup()));
            inSpace.getStatus().setIcon(si.getIcon(iconID));

        } else {

            inSpace.setStatus(new OccInspectableStatus(OccInspectionStatusEnum.PASS));
            iconID = Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                    .getString(OccInspectionStatusEnum.PASS.getIconPropertyLookup()));
            inSpace.getStatus().setIcon(si.getIcon(iconID));
        }

        // DEBUG LATER
//        Collections.sort(inSpace.getInspectedElementList());
        return inSpace;

    }

    /**
     * Builds a single generation tree of elements for display in an accordion
     * panel whose "folds" each has a list of logically grouped elements;
     * grouped by the code guide category that lives on the ordinance in its
     * unbooked code source state
     *
     * As of Fall 2023, we weren't using these groupings at all on the UI, just
     * a list and a tree
     *
     * @param ois containing all the CodeElements in its belly
     * @return the same ois but with a nice List of OccInsElementGroup objects!
     */
    public OccInspectedSpace configureElementDisplay(OccInspectedSpace ois) {
        if (ois != null) {
            // build a map first, keyed to code guide category name
            Map<String, List<OccInspectedSpaceElement>> oismap = new HashMap<>();

            if (!ois.getInspectedElementList().isEmpty()) {
                // Disable for debugging
                Collections.sort(ois.getInspectedElementList());
                for (OccInspectedSpaceElement oise : ois.getInspectedElementList()) {
                    String cat = null;
                    if (oise.getGuideEntry() != null) {
                        cat = oise.getGuideEntry().getCategory();
                    }
                    if (cat == null) {
                        cat = NO_ELEMENT_CATEGORY_TITLE;
                    }
                    if (oismap.containsKey(cat)) {
                        oismap.get(cat).add(oise);
                    } else {
                        List<OccInspectedSpaceElement> oisel = new ArrayList<>();
                        oisel.add(oise);
                        oismap.put(cat, oisel);
                    }
                }
            }

            // now unpack the map and build our groupings for display
            Set<String> catSet = oismap.keySet();
            List<String> catList = new ArrayList<>();
            catList.addAll(catSet);
            Collections.sort(catList);

            List<OccInsElementGroup> groupList = new ArrayList<>();
            for (String c : catList) {
                OccInsElementGroup grp = new OccInsElementGroup(c, oismap.get(c));
                groupList.add(grp);
            }
            ois.setInspectedElementGroupList(groupList);
        } // end if for non null input param
        return ois;
    }

    /**
     * Implements business logic to set the status of each space element
     *
     * @param inSpaceEle
     * @return
     * @throws BObStatusException
     */
    private OccInspectedSpaceElement configureOccInspectedSpaceElement(OccInspectedSpaceElement inSpaceEle) throws BObStatusException {
        SystemIntegrator si = getSystemIntegrator();
        BlobCoordinator bc = getBlobCoordinator();

        int iconID = 0;

        try {
            if (inSpaceEle == null) {
                throw new BObStatusException("Cannot configure a null OccInspectedSpaceElement...");
            }
            // Inject blobs
            inSpaceEle.setBlobList(bc.getBlobLightList(inSpaceEle));

            if (inSpaceEle.getLastInspectedBy() != null && inSpaceEle.getComplianceGrantedTS() == null) {

                inSpaceEle.setStatus(new OccInspectableStatus(OccInspectionStatusEnum.VIOLATION));
                iconID = Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                        .getString(OccInspectionStatusEnum.VIOLATION.getIconPropertyLookup()));
                inSpaceEle.getStatus().setIcon(si.getIcon(iconID));

//                System.out.println("OccupancyCoordinator.configureOccInspectedSpaceEleement | VIOLATION inspectedSpaceElementID: " + inSpaceEle.getInspectedSpaceID());
            } else if (inSpaceEle.getLastInspectedBy() != null && inSpaceEle.getComplianceGrantedTS() != null) {

                inSpaceEle.setStatus(new OccInspectableStatus(OccInspectionStatusEnum.PASS));
                iconID = Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                        .getString(OccInspectionStatusEnum.PASS.getIconPropertyLookup()));
                inSpaceEle.getStatus().setIcon(si.getIcon(iconID));
//                System.out.println("OccupancyCoordinator.configureOccInspectedSpaceEleement | PASS inspectedSpaceElementID: " + inSpaceEle.getInspectedSpaceID());

            } else {

                inSpaceEle.setStatus(new OccInspectableStatus(OccInspectionStatusEnum.NOTINSPECTED));
                iconID = Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                        .getString(OccInspectionStatusEnum.NOTINSPECTED.getIconPropertyLookup()));
                inSpaceEle.getStatus().setIcon(si.getIcon(iconID));
//                System.out.println("OccupancyCoordinator.configureOccInspectedSpaceEleement | NOT INSPECTED inspectedSpaceElementID: " + inSpaceEle.getInspectedSpaceID());

            }
        } catch (IntegrationException | BObStatusException | BlobException ex) {
            System.out.println(ex);
        }
        return inSpaceEle;
    }
    
    // *************************************************************************
    // ********************* OCC LOCATION DESCRIPTORS **************************
    // *************************************************************************
    
    
    
   

    /**
     * Factory for OccLocationDescriptors
     *
     * @return
     */
    public OccLocationDescriptor getOccLocationDescriptorSkeleton() {
        int defID = Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("locationdescriptor_implyfromspacename"));
        return new OccLocationDescriptor(defID);
    }

    /**
     * Logic intermediary for creating new occ location descriptors
     *
     * @param old
     * @return
     * @throws IntegrationException
     */
    public int addNewLocationDescriptor(OccLocationDescriptor old) throws IntegrationException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        int freshLocID = 0;
        freshLocID = oii.insertLocationDescriptor(old);

        return freshLocID;
    }
    
    
     /**
     * Extracts and configures an occ location descriptor.If this locdescriptor is the system default, 
     * the returned object will have its defaultLocation switch set to true.
     * @param locId
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public OccLocationDescriptor getOccLocationDescriptor(int locId) throws IntegrationException, BObStatusException{
        int defaultLocID = Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("locationdescriptor_implyfromspacename"));
        if(locId == 0){
            locId = defaultLocID;
        } 
        
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        OccLocationDescriptor loc = null;
        loc = oii.getLocationDescriptor(locId);
        // this is done IN OBJECT
//        if(loc.getLocationID() == locId){
//            loc.setDefaultLocation(true);
//        }
        
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        
        if(loc.getBuildingFloorNo() != 0){
            sb1.append("Floor ");
            sb2.append("Floor ");
            sb1.append(loc.getBuildingFloorNo());
            sb2.append(loc.getBuildingFloorNo());
        }
        
        if(!isStringEmptyIsh(loc.getLocationDescription())){
            sb1.append(", ");
            sb1.append(loc.getLocationDescription());
            sb2.append(Constants.FMT_HTML_BREAK);
            sb2.append(loc.getLocationDescription());
        }
        
        loc.setLocationString1Line(sb1.toString());
        loc.setLocationString2LineEscapeFalse(sb2.toString());
            
        return loc;
    }
    
    /**
     * Business logic gate for updates to location descriptions
     * @param loc
     * @param ua 
     */
    public void updateOccLocationDescriptor(OccLocationDescriptor loc, UserAuthorized ua) throws IntegrationException{
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        oii.updateLocationDescriptor(loc);
        getOccInspectionCacheManager().flush(loc);
        
        
    }
    

    /**
     * To update space location of inspected space
     *
     * @param inspection
     * @param spc
     * @param ua
     * @param locationDescriptor
     * @throws AuthorizationException
     * @throws IntegrationException
     */
    public void inspectionAction_updateSpaceLocation(FieldInspection inspection, OccInspectedSpace spc, UserAuthorized ua, OccLocationDescriptor locationDescriptor) throws AuthorizationException, IntegrationException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        if (!permissionsCheckpointConductFieldInspection(ua)) {
            throw new AuthorizationException("Permission Denied! Current user unauthorized to undertake inspectionAction_updateSpaceLocation by permissionsCheckpointConductFieldInspection");
        }
        spc.setLocation(locationDescriptor);
        oii.updateInspectedSpace(inspection, spc);
        getOccInspectionCacheManager().flush(spc);
    }
    
    /**
     * Writes inspected space attributes to the DB
     * @param ois
     * @param fin cannot be finalized. Must be inspector or manager or sys admin
     * @param ua 
     */
    public void inspectionAction_updateInspectedSpace(OccInspectedSpace ois, FieldInspection fin, UserAuthorized ua) throws BObStatusException, AuthorizationException, IntegrationException{
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        if(ois == null || fin == null || ua == null){
            throw new BObStatusException("Cannot update space with null space, inspection, or user");
        }
        
        if(!permissionsCheckpointConductFieldInspection(ua)){
            throw new AuthorizationException("User lacks privilege to edit space details on this inspection");
            
        }
        
        oii.updateInspectedSpace(fin, ois);
        getOccInspectionCacheManager().flush(ois);
    }

    /**
     * Coordinates removing a space from being part of a checklist,
     *
     * @param spc
     * @param ua
     * @param oi
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void inspectionAction_removeSpaceFromInspection(OccInspectedSpace spc, UserAuthorized ua, FieldInspection oi)
            throws IntegrationException, AuthorizationException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        OccInspectionCacheManager oicm = getOccInspectionCacheManager();
        
        if (!permissionsCheckpointConductFieldInspection(ua)) {
            throw new AuthorizationException("Permission Denied! Current user unauthorized to undertake inspectionAction_removeSpaceFromInspection by permissionsCheckpointConductFieldInspection");
        }
        
        System.out.println("Remove Space");
        spc.setDeactivatedBy(ua);
        oii.deactivateInspectedSpaceData(spc);
        
        // dump our caches
        oicm.flush(spc);
        
    }

    /**
     * Routing method for code elements as they are inspected. This method
     *
     * calls the respective methods inside this coordinator based on the value
     * of the OccInspectedSpaceElements statusEnum
     *
     * TODO: make sure add to code enf on fail gets recorded as as true by
     * default
     *
     * @param oise the element being inspected with a statusEnum set to the
     * desired state
     * @param ua the user doing the inspecting; must have CEO or better
     * permissions
     * @param oi the inspection in which the element lives
     * @param useDefFindOnFail if true is passed in, the default findings will
     * be appended to any findings
     * @return a reference to the same oise as was passed in, after the DB
     * writes have gone through
     * @throws AuthorizationException
     * @throws BObStatusException
     * @throws IntegrationException
     */
    public OccInspectedSpaceElement inspectionAction_recordElementInspectionByStatusEnum(OccInspectedSpaceElement oise,
            UserAuthorized ua,
            FieldInspection oi,
            boolean useDefFindOnFail) throws AuthorizationException, BObStatusException, IntegrationException {
        if (oise == null || ua == null || oi == null) {
            throw new BObStatusException("Cannot update code element status with null element, user, or inspection");
        }
        if (!permissionsCheckpointConductFieldInspection(ua)) {
            throw new AuthorizationException("Permission denied! Current user lacks authority to execute inspectionAction_recordElementInspectionByStatusEnum under permissionsCheckpointConductFieldInspection");
        }
        if (oise.getStatusEnum() == null) {
            throw new BObStatusException("Element does not have a status enum set and therefore cannot have its status written to DB.");
        }
        OccInspectionIntegrator oii = getOccInspectionIntegrator();

        switch (oise.getStatusEnum()) {
            case NOTINSPECTED:
                inspectionAction_configureElementForNotInspected(oise, ua, oi);
                break;
            case VIOLATION:
                inspectionAction_configureElementForInspectionNoCompliance(oise, ua, oi, useDefFindOnFail);
                break;
            case PASS:
                inspectionAction_configureElementForCompliance(oise, ua, oi);
                break;
        }
        // write changes to db
        updateInspectedSpaceElement(oise);
        
        return oise;

    }

    /**
     * Undertakes a batch operation for all inspected space elements in the
     * inspected space
     *
     * @param ois
     * @param oise
     * @param ua
     * @param oi
     * @param useDefFindOnFail when true the default findings are appended to
     * existing findings
     * @return
     * @throws BObStatusException
     * @throws IntegrationException
     */
    public OccInspectedSpace inspectionAction_batchConfigureInspectedSpace(OccInspectedSpace ois,
            OccInspectionStatusEnum oise,
            UserAuthorized ua,
            FieldInspection oi,
            boolean useDefFindOnFail) throws BObStatusException, IntegrationException, AuthorizationException {
        if (ois == null || oise == null || ua == null || oi == null) {
            throw new BObStatusException("Cannot batch update with null ois, user, or inspection");
        }
        if (!permissionsCheckpointConductFieldInspection(ua)) {
            throw new AuthorizationException("Permission denied! Current user lacks authority to execute inspectionAction_batchConfigureInspectedSpace under permissionsCheckpointConductFieldInspection");
        }
        List<OccInspectedSpaceElement> oisel = ois.getInspectedElementList();
        if (oisel != null && !oisel.isEmpty()) {
            OccInspectionIntegrator oii = getOccInspectionIntegrator();
            for (OccInspectedSpaceElement ele : oisel) {
                // only batch apply on not inspected
                if (ele.getStatusEnum() != null && ele.getStatusEnum() == OccInspectionStatusEnum.NOTINSPECTED) {
                    switch (oise) {
                        case NOTINSPECTED:
                            inspectionAction_configureElementForNotInspected(ele, ua, oi);
                            break;
                        case VIOLATION:
                            inspectionAction_configureElementForInspectionNoCompliance(ele, ua, oi, useDefFindOnFail);
                            break;
                        case PASS:
                            inspectionAction_configureElementForCompliance(ele, ua, oi);
                            break;
                    } // close switch
                    updateInspectedSpaceElement(ele);
                }
            } // close for over elements
        } // close not null
        getOccInspectionCacheManager().flush(ois);
        return ois;
    }

    /**
     * Route for updating an individual element in an inspection
     *
     * @param ois
     * @param oise
     * @param ua
     */
    public void inspectionAction_updateOccInspectedSpaceElement(OccInspectedSpace ois,
            OccInspectedSpaceElement oise,
            FieldInspection oi,
            UserAuthorized ua) throws BObStatusException, IntegrationException {

        if (ois == null || oise == null || oi == null || ua == null) {
            throw new BObStatusException("Cannot update OISE with null space, element, inspection, or user");
        }
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        switch (oise.getStatusEnum()) {
            case NOTINSPECTED:
                inspectionAction_configureElementForNotInspected(oise, ua, oi);
                break;
            case VIOLATION:
                inspectionAction_configureElementForInspectionNoCompliance(oise, ua, oi, false);
                break;
            case PASS:
                inspectionAction_configureElementForCompliance(oise, ua, oi);
                break;
        }
        // write changes to db
        updateInspectedSpaceElement(oise);

    }

    /**
     * Sets members on an OccInspectedSpaceElement
     *
     * @param oise
     * @param u
     * @param oi
     * @return with members properly set, ready for writing to db
     */
    public OccInspectedSpaceElement inspectionAction_configureElementForCompliance(OccInspectedSpaceElement oise,
            User u,
            FieldInspection oi) {

        oise.setComplianceGrantedBy(u);
        oise.setComplianceGrantedTS(LocalDateTime.now());
        oise.setLastInspectedTS(LocalDateTime.now());
        oise.setLastInspectedBy(u);
        return oise;
    }

    /**
     * Sets compliance and last inspected ts to NULL
     *
     * @param oise
     * @param u
     * @param oi
     * @return with members set
     */
    public OccInspectedSpaceElement inspectionAction_configureElementForNotInspected(OccInspectedSpaceElement oise,
            User u,
            FieldInspection oi) {

        oise.setComplianceGrantedBy(null);
        oise.setComplianceGrantedTS(null);
        oise.setLastInspectedTS(null);
        oise.setLastInspectedBy(null);

        return oise;
    }

    /**
     * Implements business rules for marking an element as inspected but not
     * with compliance. From a database and Java logic perspective, there's no
     * field that corresponds to "failure" but rather the failure of an element
     * during an inspection is derived from a timestamp of having been inspected
     * but not being flagged as in compliance.
     *
     * Also sets default violation severity to the default severity
     *
     * @param oise the element that has been inspected but is not in compliance
     * @param u
     * @param oi
     * @param useDefFindOnFail appends default findings to findings if true
     * @return with members set, ready for sending to DB
     */
    public OccInspectedSpaceElement inspectionAction_configureElementForInspectionNoCompliance(OccInspectedSpaceElement oise,
            User u,
            FieldInspection oi,
            boolean useDefFindOnFail) {

        oise.setComplianceGrantedBy(null);
        oise.setComplianceGrantedTS(null);
        oise.setLastInspectedTS(LocalDateTime.now());
        oise.setLastInspectedBy(u);

        if (useDefFindOnFail) {
            StringBuilder sb = new StringBuilder();
            if (oise.getInspectionNotes() != null) {
                sb.append(oise.getInspectionNotes());
                sb.append(SPACE);
            }
            if (oise.getDefaultViolationDescription() != null) {
                // To avoid appending of default findings multiple times
                if (!StringUtil.isStringContainsI(sb.toString(), oise.getDefaultViolationDescription())) {
                    sb.append(oise.getDefaultViolationDescription());
                }
            }
            oise.setInspectionNotes(sb.toString());
        }

        if (Objects.nonNull(oise.getDefaultViolationSeverity()) && Objects.isNull(oise.getFaillureSeverity())) {
            oise.setFaillureSeverity(oise.getDefaultViolationSeverity());
        }

        return oise;
    }

    /**
     * ************************************************************
     * ***************** HANDY DANDY UTILITIES ********************
     * ************************************************************
     */
    /**
     * Utility method for working with the property coordinator to extract the
     * property host of a field inspection.Implemented in order to give folks
     * the ability to select person links to display on a field inspection
     * report, and those live on a property itself
     *
     * @param inspectable
     * @param ua
     * @return the property host of the given inspectable
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BlobException
     * @throws com.tcvcog.tcvce.domain.SearchException
     */
    public PropertyDataHeavy getPropertyDataHeavy(IFace_inspectable inspectable, UserAuthorized ua)
            throws BObStatusException, IntegrationException, SearchException, BlobException {
        PropertyCoordinator pc = getPropertyCoordinator();
        PropertyDataHeavy pdh = null;
        try {
            if (inspectable instanceof CECaseDataHeavy cse) {
                pdh = pc.assemblePropertyDataHeavy(pc.getProperty(cse.getParcelKey()), ua);
            } else if (inspectable instanceof CECaseDataHeavy csedh) {
                pdh = pc.assemblePropertyDataHeavy(pc.getProperty(csedh.getParcelKey()), ua);
            } else if (inspectable instanceof OccPeriodDataHeavy opdh) {
                pdh = pc.assemblePropertyDataHeavy(pc.getProperty(opdh.getParentParcelKey()), ua);
            } else if (inspectable instanceof OccPeriod period) {
                pdh = pc.assemblePropertyDataHeavy(pc.getProperty(period.getParentParcelKey()), ua);
            } else {
                throw new BObStatusException("Cannot extract property from an inspectable that's not a cecase or occ period");
            }
        } catch (AuthorizationException ex) {
            throw new BObStatusException(ex.getMessage());
        }
        return pdh;
    }
    
    /**
     * Utility for returning the parent property of the given inspection.
     * @param finLight
     * @param ua
     * @return 
     */
    public Property getInspectionParentProperty(FieldInspectionLight finLight, UserAuthorized ua) throws BObStatusException, IntegrationException, SearchException, BlobException{
        if(finLight == null){
            throw new BObStatusException("Cannot get property from null finLight");
        }
        IFace_inspectable ins = extractInspectionParentInspectable(finLight, ua);
        return getPropertyDataHeavy(ins, ua);
        
    }

    /**
     * ************************************************************
     * ********************* REINSPECTIONS!! **********************
     * ************************************************************
     */
    /**
     * Factory for FieldInspectionReInspectionConfig objects Sets sensible
     * defaults
     *
     * @param source the field inspection we'd like to reinspect
     * @return with only the source inspection set; all ready for additional
     * params
     */
    public FieldInspectionReInspectionConfig getFieldInspectionReinspectionSettingsSkeleton(FieldInspection source) {
        FieldInspectionReInspectionConfig reinconfig = new FieldInspectionReInspectionConfig();
        reinconfig.setSourceInspection(source);

        reinconfig.setMaintainSameParentObject(true);

        reinconfig.setMigrateInspectionFindings(true);
        reinconfig.setLiteralFindingsCloneNoWrapper(false);

        reinconfig.setMigrateInspectedSpacesWithoutAnyInspectedItems(false);
        reinconfig.setMigrateInspectedSpacesWithoutAnyFailedItems(false);

        reinconfig.setMigrateBlobsOnFailedItems(false);
        reinconfig.setMigrateUninspectedElements(false);

        reinconfig.setMigrateBlobsOnSourceInspection(false);
        reinconfig.setMigrateBlobsOnFailedItems(false);
        reinconfig.setMigrateBlobsOnPassedItems(false);

        return reinconfig;
    }

    /**
     *
     * Logic controller for creating a new field inspection as a re-inspection
     * of a previous field inspection.By default, the created re-inspection will
     * be configured with the same checklist as the source inspection.Failed
     * items will migrate to the re-inspection, with notes appended to each
     * documenting where they came from
     *
     * @param reFin with a reinspection config in its belly
     * @return the fresh inspection that's a reinspection of the inputted
     * inspection object
     * @throws BObStatusException for null inputs, or non-finalized FIN
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.InspectionException
     * @throws com.tcvcog.tcvce.domain.BlobException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     * @throws com.tcvcog.tcvce.domain.EventException
     */
    public FieldInspection inspectionAction_setupReinspection(FieldInspection reFin)
            throws BObStatusException, IntegrationException, InspectionException, BlobException, AuthorizationException, EventException {
        if(reFin == null || reFin.getReinspectionConfig() == null){
            throw new BObStatusException("Cannot reinspect a null FIN or one without a re-inspection setting bundle");
        }
        FieldInspectionReInspectionConfig reSettings = reFin.getReinspectionConfig();
        
        if (reSettings.getSourceInspection() == null || reSettings.getRequestingUser() == null || reSettings.getInspectable() == null) {
            throw new BObStatusException("Cannot configure reinspection with null source inspection or user or occ period/case");
        }
        if (reSettings.getSourceInspection().getDetermination() == null) {
            throw new BObStatusException("Field inspection must be finalized before a reinspection will be created");
        }
        if(reFin.getNotesPreInspection() != null){
            reSettings.appendToReinspectionLog(reFin.getNotesPreInspection(), true);
        }
        
        reSettings.appendToReinspectionLog("Reinspection of inspection ID: " + reSettings.getSourceInspection().getInspectionID(), true);
        reSettings.appendToReinspectionLog("Original inspector: " + reSettings.getSourceInspection().getInspector().getUsername(), true);
        reSettings.appendToReinspectionLog("Original occperiodID: " + reSettings.getSourceInspection().getOccPeriodID(), true);
        reSettings.appendToReinspectionLog("Original cecaseID: " + reSettings.getSourceInspection().getCecaseID(), true);

        reFin.setFollowUpToInspectionID(reSettings.getSourceInspection().getInspectionID());
        reFin.setNotesPreInspection(reSettings.getLog());
        
        getOccInspectionCacheManager().flush(reFin);
        
        return reFin;

    }

    /**
     * Internal organ for setting up a reinspection's settings
     *
     * @param reSettings
     * @return the settings object
     */
    private FieldInspectionReInspectionConfig configureReinspectionSpaces(FieldInspectionReInspectionConfig reSettings) throws BObStatusException, IntegrationException {

        if (reSettings.getSourceInspection() == null
                || reSettings.getRequestingUser() == null
                || reSettings.getInspectable() == null
                || reSettings.getReInspection() == null
                || reSettings.getReInspection().getChecklistTemplate() == null) {
            throw new BObStatusException("OccInspectionCoordinator.configureReinspectionSpaces | Cannot configure reinspection with null inspection or user or occ period/case");
        }

        // Now setup inspected space elements
        List<OccInspectedSpace> inSpaceList = reSettings.getSourceInspection().getInspectedSpaceList();
        if (inSpaceList != null && !inSpaceList.isEmpty()) {
            // visit each space individually
            for (OccInspectedSpace inspc : inSpaceList) {
                OccInspectableStatus oisStatus = inspc.getStatus();

                reSettings.appendToReinspectionLog("Checking inspected space ID: " + inspc.getInspectedSpaceID(), true);

                if (oisStatus == null) {
                    throw new BObStatusException("Inspected space ID " + inspc.getInspectedSpaceID() + " has a null status: aborting reinspection logic");
                }
                boolean reinspectspace = true;
                switch (oisStatus.getStatusEnum()) {
                    case NOTINSPECTED:
                        // we'll never move over spaces that had zero marked elements
                        if (!reSettings.isMigrateInspectedSpacesWithoutAnyInspectedItems()) {
                            reinspectspace = false;
                        }
                        break;
                    case PASS:
                        // check what the reinspector wants
                        if (!reSettings.isMigrateInspectedSpacesWithoutAnyFailedItems()) {
                            reinspectspace = false;
                        }
                        break;
                    case VIOLATION:
                        // always reinspect 
                        break;
                }
                if (reinspectspace) {
                    inspectionAction_reInspectOccInspectedSpace(reSettings, inspc);
                }
            }
        }
        System.out.println("OccInspectionCoordinator.configureReinspectionSpaces | log dump post OIS config" + reSettings.getLog());
        return reSettings;
    }

    /**
     * Filters the inspectedElementList as per filter text filterText is matched
     * against ordinance header string and/or non IRC style header string
     *
     * @param eleList
     * @param filterText
     * @param searchFullOrdText
     * @return the filtered list, or if I didn't get a list, an empty list
     *
     */
    public List<OccInspectedSpaceElement> inspectionAction_configureInspectedElementListFiltered(
            List<OccInspectedSpaceElement> eleList,
            String filterText,
            boolean searchFullOrdText) {
        if (Objects.nonNull(eleList)) {
            if (Objects.nonNull(filterText) && !filterText.isEmpty()) {
                List<OccInspectedSpaceElement> filteredList = new ArrayList<>();
                for (OccInspectedSpaceElement oise : eleList) {
                    if (inspectionAction_isOrdinanceFilterMatches(filterText, oise, searchFullOrdText)) {
                        filteredList.add(oise);
                    }
                }
                return filteredList;
            } else {
                return eleList;
            }
        }
        return new ArrayList<>();
    }

    /**
     * Filters the inspectedElementGroupList as per filter text filterText is
     * matched against ordinance header string and/or non IRC style header
     * string
     *
     * @Deprecated as of July 11, 2023 during dreary and horrid inspection
     * process debugging and overhaul
     * @param inspectedElementGroupList
     * @param filterText
     * @return List<OccInsElementGroup> list of ordinances matching with
     * filterText
     */
    public List<OccInsElementGroup> inspectionAction_configureInspectedElementGroupListFiltered(
            List<OccInsElementGroup> inspectedElementGroupList,
            String filterText) {
        if (Objects.nonNull(inspectedElementGroupList)) {
            if (Objects.nonNull(filterText) && !filterText.isEmpty()) {
                List<OccInsElementGroup> inspectedElementGroupListFiltered = new ArrayList<>();
                inspectedElementGroupList.forEach(occInsElementGroup -> {
                    List<OccInspectedSpaceElement> filteredList = occInsElementGroup.getElementList().stream()
                            .filter(occInspectedSpaceElement -> inspectionAction_isOrdinanceFilterMatches(filterText, occInspectedSpaceElement, false)).toList();
                    if (!filteredList.isEmpty()) {
                        inspectedElementGroupListFiltered.add(new OccInsElementGroup(
                                occInsElementGroup.getGroupTitle(),
                                filteredList
                        ));
                    }
                });
                return inspectedElementGroupListFiltered;
            } else {
                return inspectedElementGroupList;
            }
        }
        return new ArrayList<>();
    }

    /**
     * To check ordinance filter text matches with header string or non IRC
     * Style Header String and full text
     *
     * @param filterText
     * @param oise
     * @return
     */
    private boolean inspectionAction_isOrdinanceFilterMatches(String filterText, OccInspectedSpaceElement oise, boolean includeFullOrdText) {
        StringBuilder sb = new StringBuilder();
        if (Objects.nonNull(oise.getHeaderString())) {
            sb.append(oise.getHeaderString());
        }
        if (Objects.nonNull(oise.getNonIrcStyleHeaderString())) {
            sb.append(oise.getNonIrcStyleHeaderString());
        }
        if (includeFullOrdText && Objects.nonNull(oise.getOrdTechnicalText())) {
            sb.append(oise.getOrdTechnicalText());
        }
        return StringUtil.isStringContainsI(sb.toString(), filterText);
    }

    /**
     * ************************************************************
     * ********************* CHECKLIST TEMPLATES ******************
     * ************************************************************
     */
    /**
     * Factory method of OccChecklistTemplate objects NO Database write here--ID
     * is zero
     *
     * @param muni injected into the new template.
     * @return skeleton--ID=0
     */
    public OccChecklistTemplate getOccChecklistTemplateSkeleton(Municipality muni) {
        OccChecklistTemplate oct = new OccChecklistTemplate();

        oct.setMuni(muni);
        return oct;
    }

    /**
     * Deactivates the given template
     *
     * @param ua doing the deactivation
     * @param oct to be deactivated
     */
    public void deactivateChecklistTemplate(UserAuthorized ua, OccChecklistTemplate oct) throws IntegrationException, BObStatusException, AuthorizationException {
        if (oct == null && ua == null) {
            throw new BObStatusException("OccInspectionCoordinator.deactivateChecklistTemplate: Cannot deactivate checklist with null inputs;");
        }
        if (!permissionsCheckpointChecklistEdit(oct, ua)) {
            throw new AuthorizationException("Permission Denied! User unauthorized to undertake deactivateChecklistTemplate by permissionsCheckpointChecklistEdit.");
        }
        SystemIntegrator si = getSystemIntegrator();
        if (oct != null) {
            oct.setDeactivatedBy_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
            si.deactivateUMAPTrackedEntity(oct);
        }
        getOccInspectionCacheManager().flush(oct);
    }

    /**
     * Logic container for retrieving a ChecklistTemplate, which is used to
     * create an actual OccupancyInspection
     *
     * @param checklistID
     * @return fully baked checklist template; null if ID == 0
     * @throws IntegrationException
     */
    public OccChecklistTemplate getChecklistTemplate(int checklistID) throws IntegrationException {
        OccChecklistTemplate oct = null;
        OccChecklistIntegrator oci = getOccChecklistIntegrator();
        if (checklistID != 0) {
            oct = oci.getChecklistTemplate(checklistID);
            if (oct != null && oct.getInspectionChecklistID() != 0) {
                oct.setOccSpaceTypeList(getOccSpaceTypeChecklistifiedList(oct));
            }
            return oct;
        }
        return null;
    }

    /**
     * Call me when the backing bean loads to get a list of possible inspections
     * to carry out such as "Commercial building" or "residential"
     *
     * @param muni
     * @param includeInactive
     * @return a list, possibly containing checklist template objects
     * @throws IntegrationException
     */
    public List<OccChecklistTemplate> getOccChecklistTemplateList(Municipality muni, boolean includeInactive) throws IntegrationException {
        OccChecklistIntegrator oci = getOccChecklistIntegrator();
        List<OccChecklistTemplate> templateList = new ArrayList<>();
        List<Integer> idl = oci.getOccChecklistTemplateList(muni, includeInactive);
        if (idl != null && !idl.isEmpty()) {
            for (Integer i : idl) {
                templateList.add(getChecklistTemplate(i));
            }
        }
        return templateList;
    }

    /**
     * Factory method for creating OccSpaceElement
     *
     * @return the Fresh Object
     */
    public OccSpaceElement getOccSpaceElementSkeleton() {
        return new OccSpaceElement();

    }

    /**
     * Extracts all occ space types for injection into the Template. Used during
     * the construction of an OccChecklistTemplate
     *
     * @param oct
     * @return
     * @throws IntegrationException
     */
    public List<OccSpaceTypeChecklistified> getOccSpaceTypeChecklistifiedList(OccChecklistTemplate oct) throws IntegrationException {
        OccChecklistIntegrator oci = getOccChecklistIntegrator();
        List<OccSpaceTypeChecklistified> ostcl = null;
        if (Objects.nonNull(oct)) {
            ostcl = new ArrayList<>();
            List<Integer> ostcidl = oci.getOccSpaceTypeChecklistifiedIDListByChecklist(oct.getInspectionChecklistID());
            for (Integer ostcid : ostcidl) {
                OccSpaceTypeChecklistified ostc = oci.getOccSpaceTypeChecklistified(ostcid);
                if (ostc != null && ostc.getDeactivatedTS() == null) {
                    ostcl.add(ostc);
                }
            }
            ostcl.sort(Comparator.comparing(OccSpaceType::getSpaceTypeTitle));
        }
        return ostcl;
    }

    /**
     * Logic intermedi Oary for creating a new checklist template in a given
     * muni
     *
     * @param plate
     * @param ua
     * @return the database PK of the new record in occhecklisttemplate
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public int insertChecklistTemplateMetadata(OccChecklistTemplate plate, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException {
        if (plate == null) {
            throw new BObStatusException("Cannot insert a null template");
        }

        if (!permissionsCheckpointChecklistEdit(plate, ua)) {
            throw new AuthorizationException("Permission Denied! User unauthorized to undertake insertChecklistTemplateMetadata by permissionsCheckpointChecklistEdit.");
        }
        OccChecklistIntegrator oci = getOccChecklistIntegrator();
        auditCauseAndMuniAlignment(plate);
        int freshID = oci.insertChecklistTemplateMetadata(plate);
        getOccInspectionCacheManager().flush(plate);
        return freshID;
    }

    /**
     * Undertakes an update operation on a ChecklistTemplate, including
     * deactivation
     *
     * @param plate
     * @param ua
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void updateChecklistTemplateMetadata(OccChecklistTemplate plate, UserAuthorized ua)
            throws IntegrationException, BObStatusException, AuthorizationException {
        if (plate == null || ua == null) {
            throw new BObStatusException("Cannot update a null checklist template or do so with null user");
        }
        if (!permissionsCheckpointChecklistEdit(plate, ua)) {
            throw new AuthorizationException("Permission Denied! User unauthorized to undertake deactivateChecklistTemplate by permissionsCheckpointChecklistEdit.");
        }

        OccChecklistIntegrator oci = getOccChecklistIntegrator();
        auditCauseAndMuniAlignment(plate);
        plate.setLastUpdatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
        oci.updateChecklistTemplateMetadata(plate);
        getOccInspectionCacheManager().flush(plate);
    }

    /**
     * Checks that the selected cause is registered in the template's muni
     * @param plate 
     */
    private void auditCauseAndMuniAlignment(OccChecklistTemplate plate) throws BObStatusException{
        if(plate == null){
            throw new BObStatusException("Cannot check cause and muni of null template");
        }
        if(plate.getMuni() != null && plate.getDefaultInspectionCause() != null && plate.getDefaultInspectionCause().getMuni() != null){
            if(plate.getMuni().getMuniCode() != plate.getDefaultInspectionCause().getMuni().getMuniCode()){
                throw new BObStatusException("The default cause for a checklist must be registered in the checklist's municipality");
            }
        }
    }
    
    /**
     * Fields requests for a non checklistified SpaceType by ID
     *
     * @return
     */
    public OccSpaceType getOccSpaceTypeSkeleton() {

        return new OccSpaceType();
    }

    /**
     * Fields requests for a non checklistified SpaceType by ID
     *
     * @param tpeID
     * @return
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public OccSpaceType getOccSpaceType(int tpeID) throws IntegrationException {
        OccChecklistIntegrator oci = getOccChecklistIntegrator();
        return oci.getOccSpaceType(tpeID);
    }

    /**
     * Extracts all space types from the database--muni agnostic
     *
     * @return
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public List<OccSpaceType> getOccSpaceTypeList() throws IntegrationException {
        List<Integer> stidl = getOccChecklistIntegrator().getOccSpaceTypeIDListComplete();
        List<OccSpaceType> spaceTypeList = new ArrayList<>();
        if (stidl != null && !stidl.isEmpty()) {
            for (Integer i : stidl) {
                spaceTypeList.add(getOccSpaceType(i));
            }
        }

        return spaceTypeList;
    }

    /**
     * Logic intermediary for creating a new space type; these are general
     * across all munis, so they are just a classification scheme: Kitchen, rear
     * exterior, etc.
     *
     * @param ost
     * @param ua
     * @return of the freshly inserted record into occspacetype
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public int insertSpaceType(OccSpaceType ost, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException {
        OccChecklistIntegrator oci = getOccChecklistIntegrator();
        if (ost == null) {
            throw new BObStatusException("Cannot insert null space type");
        }
        if (!permissionsCheckpointChecklistEditAncillaryObjects(ua)) {
            throw new AuthorizationException("Permission Denied! Current user is unauthorized to undertake insertSpaceType by permissionsCheckpointChecklistEditAncillaryObjects");
        }

        return oci.insertSpaceType(ost);
    }
    
    /**
     * Deactivates space type; Does not impact existing checklists
     * @param ost
     * @param ua 
     */
    public void deactivateOccSpaceType(OccSpaceType ost, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        if(ost == null || ua == null){
            throw new BObStatusException("Cannot deac with null inputted space type or user");
        }
        
        if(!permissionCheckpointChecklistEditGeneral(ua)){
            throw new AuthorizationException("User's credential is insufficient to deactivate space type");
        }
        
        OccChecklistIntegrator oci = getOccChecklistIntegrator();
        oci.deactivateSpaceType(ost);
        
    }

    /**
     * We want the user to be able to turn on or off required flags on the ECEs
     * before we link them to a space type, so this method wraps an arbitrary
     * list of ECEs in OccSpaceElement objects that are skeletons--no IDs--since
     * they haven't been in the DB yet
     *
     * @param ecel
     * @return
     */
    public List<OccSpaceElement> wrapECEListInOccSpaceElementWrapper(List<EnforceableCodeElement> ecel) {
        List<OccSpaceElement> osel = new ArrayList<>();
        if (ecel != null && !ecel.isEmpty()) {
            for (EnforceableCodeElement ece : ecel) {
                osel.add(new OccSpaceElement(ece));
            }
        }
        return osel;
    }

    /**
     * Undertakes an update operation on a space type
     *
     * @param ost
     * @param ua
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void updateSpaceType(OccSpaceType ost, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException {
        OccChecklistIntegrator oci = getOccChecklistIntegrator();
        if (ost == null) {
            throw new BObStatusException("Cannot update null space type");
        }
        if (!permissionsCheckpointChecklistEditAncillaryObjects(ua)) {
            throw new AuthorizationException("Permission Denied! Current user is unauthorized to undertake updateSpaceType by permissionsCheckpointChecklistEditAncillaryObjects");
        }
        oci.updateSpaceType(ost);
        getOccInspectionCacheManager().flush((OccChecklistTemplate) null);
    }

    /**
     * If not linked to something, removes an OccSpaceType from the DB
     *
     * @param ost
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void removeOccSpaceType(OccSpaceType ost, UserAuthorized ua)
            throws BObStatusException, IntegrationException, AuthorizationException {
        if (ost == null) {
            throw new BObStatusException("Cannot remove occ space type with null input");
        }
        if (!permissionsCheckpointChecklistEditAncillaryObjects(ua)) {
            throw new AuthorizationException("Permission Denied! Current user is unauthorized to undertake removeOccSpaceType by permissionsCheckpointChecklistEditAncillaryObjects");
        }
        getOccChecklistIntegrator().deleteSpaceType(ost);
        // no caching to dump on just the space type itself

    }

    /**
     * Updates a single SpaceType's connection to a Checklist
     *
     * @param ostc
     * @param ua
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void updateSpaceTypeChecklistified(OccSpaceTypeChecklistified ostc, UserAuthorized ua)
            throws BObStatusException, IntegrationException, AuthorizationException {
        OccChecklistIntegrator oci = getOccChecklistIntegrator();
        if (ostc == null) {
            throw new BObStatusException("Cannot update null space type");
        }
        if (!permissionsCheckpointChecklistEditAncillaryObjects(ua)) {
            throw new AuthorizationException("Permission Denied! Current user is unauthorized to undertake updateSpaceTypeChecklistified by permissionsCheckpointChecklistEditAncillaryObjects");
        }
        oci.updateSpaceTypeChecklistified(ostc);
        getOccInspectionCacheManager().flush(ostc);
    }

    /**
     * Does the switcheroo and passes it to the integrator for writing out
     *
     * @param ose
     * @param ua
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void toggleRequiredAndUpdateOccSpaceElement(OccSpaceElement ose, UserAuthorized ua)
            throws IntegrationException, AuthorizationException {
        OccChecklistIntegrator oci = getOccChecklistIntegrator();
        if (ose != null) {
            // do the flip
            if (!permissionsCheckpointChecklistEditAncillaryObjects(ua)) {
                throw new AuthorizationException("Permission Denied! Current user is unauthorized to undertake toggleRequiredAndUpdateOccSpaceElement by permissionsCheckpointChecklistEditAncillaryObjects");
            }
            ose.setRequiredForInspection(!ose.isRequiredForInspection());
            oci.updateOccSpaceElement(ose);
            getOccInspectionCacheManager().flush((OccChecklistTemplate) null);
        }

    }

    /**
     * Connects the OccSpaceType to
     * the given occChecklistTemplate.I'll create the data heavy subclass
     * OccSpaceTypeChecklistified from the given superclass OccSpaceType ost
     *
     * @param plate
     * @param ost
     * @param required
     * @param ua
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public OccSpaceTypeChecklistified insertAndLinkSpaceTypeChecklistifiedListToTemplate(OccChecklistTemplate plate, OccSpaceType ost, boolean required, UserAuthorized ua)
            throws BObStatusException, IntegrationException, AuthorizationException {

        if (plate == null || ost == null) {
            throw new BObStatusException("cannot insertAndLinkSpaceTypeChecklistifiedListToTemplate with null template or ostc list");
        }
        if (!permissionsCheckpointChecklistEdit(plate, ua)) {
            throw new AuthorizationException("Permission Denied! Current user is unauthorized to undertake insertAndLinkSpaceTypeChecklistifiedListToTemplate by permissionsCheckpointChecklistEditAncillaryObjects");
        }
        OccChecklistIntegrator oci = getOccChecklistIntegrator();

        OccSpaceTypeChecklistified ostchk = new OccSpaceTypeChecklistified(ost);
        ostchk.setRequired(required);
        ostchk.setChecklistParentID(plate.getInspectionChecklistID());
        oci.verifyUniqueChecklistSpaceTypeLink(ostchk);
        // write the new object and inject our DBKEY
        int freshOCSTCID = oci.insertOccChecklistSpaceTypeChecklistified(ostchk);
        ostchk.setChecklistSpaceTypeID(freshOCSTCID);
        
        getOccInspectionCacheManager().flush(plate);
        return ostchk;
    }

    /**
     * Utility method for downcasting
     *
     * @param ostchkl
     * @return
     */
    public List<OccSpaceType> downcastOccSpaceTypeChecklistified(List<OccSpaceTypeChecklistified> ostchkl) {
        List<OccSpaceType> ostl = new ArrayList<>();
        if (ostchkl != null && !ostchkl.isEmpty()) {
            for (OccSpaceTypeChecklistified ostchk : ostchkl) {
                ostl.add((OccSpaceType) ostchk);
            }
        }
        return ostl;
    }

    /**
     * Removes a link between a SpaceType and its parent checklist.When this
     * call is done, pulling this OSTC's parent checklist will no longer include
     * this SpaceType or any of that space type's elements
     *
     * @param ostc
     * @param plate
     * @param ua
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void detachOccSpaceTypeChecklistifiedFromTemplate(OccSpaceTypeChecklistified ostc,
            OccChecklistTemplate plate,
            UserAuthorized ua)
            throws BObStatusException,
            IntegrationException,
            AuthorizationException {
        if (ostc == null) {
            throw new BObStatusException("Cannot detach space type checklistified with null input");
        }
        if (!permissionsCheckpointChecklistEdit(plate, ua)) {
            throw new AuthorizationException("Permission Denied! Current user is unauthorized to undertake detachOccSpaceTypeChecklistifiedFromTemplate by permissionsCheckpointChecklistEdit");
        }
        OccChecklistIntegrator oci = getOccChecklistIntegrator();
        // first get rid of the element to occspacetype links
        if (ostc.getCodeElementList() != null && !ostc.getCodeElementList().isEmpty()) {
            for (OccSpaceElement ose : ostc.getCodeElementList()) {
                oci.detachOccSpaceElementFromOccSpaceTypeChecklistified(ose);
            }
        }

        // then nuke the space type
        oci.deactivateOccSpaceTypeChecklistified(ostc);
        getOccInspectionCacheManager().flush(ostc);
    }

    /**
     * Connects a given OccSpaceElement to the provided SpaceType that
     * has been connected to a given checklist. 
     *
     * @param ostc
     * @param ose
     * @param plate
     * @param ua
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException: for many reasons,
     * including unique mappings. Will complain with
     * BOBStatusException if the pairing is not unique on the space type.
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void insertAndLinkCodeElementsToSpaceType(   OccSpaceTypeChecklistified ostc,
                                                        OccSpaceElement ose,
                                                        OccChecklistTemplate plate,
                                                        UserAuthorized ua)
                                        throws  BObStatusException,
                                                IntegrationException,
                                                AuthorizationException {
        if (ostc == null || ose == null) {
            throw new BObStatusException("Cannot insert and link elements to space with null space type or ordinance");
        }
        OccChecklistIntegrator oci = getOccChecklistIntegrator();
        if (!permissionsCheckpointChecklistEdit(plate, ua)) {
            throw new AuthorizationException("Permission Denied! Current user is unauthorized to undertake permissionsCheckpointChecklistEdit by permissionsCheckpointChecklistEdit");
        }
        ose.setParentSpaceTypeID(ostc.getChecklistSpaceTypeID());
        oci.verifyUniqueCodeElementInSpaceType(ose);
        oci.attachCodeElementsToSpaceTypeInChecklist(ose);
        getOccInspectionCacheManager().flush(plate);

    }

    /**
     * Removes a link between a code element and an occ space type
     *
     * @param ose
     * @param plate
     * @param ua
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void detachCodeElementFromSpaceType(OccSpaceElement ose,
            OccChecklistTemplate plate,
            UserAuthorized ua)
            throws BObStatusException,
            IntegrationException,
            AuthorizationException {
        if (ose == null) {
            throw new BObStatusException("Cannot detach code element from space with null code element");

        }
        if (!permissionsCheckpointChecklistEdit(plate, ua)) {
            throw new AuthorizationException("Permission Denied! Current user is unauthorized to undertake detachCodeElementFromSpaceType by permissionsCheckpointChecklistEdit");
        }

        getOccChecklistIntegrator().detachOccSpaceElementFromOccSpaceTypeChecklistified(ose);
        getOccInspectionCacheManager().flush(plate);
    }

    /**
     * Implements logic to create a pseudo deep copy of a given checklist
     * template to the target muni
     *
     * @param sourceChecklist
     * @param targetChecklist
     * @param writeMissingOrdsInTargetChecklistMuniDefaultCodebook when true and
     * the user has sys admin permissions, ordinances that aren't in the target
     * muni's default codebook will be added during the clone operation
     * @param ua requesting the clone
     * @return the ID of the new checklist
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     * @throws com.tcvcog.tcvce.domain.EventException
     */
    public int cloneOccChecklistTemplate(OccChecklistTemplate sourceChecklist,
            OccChecklistTemplate targetChecklist,
            boolean writeMissingOrdsInTargetChecklistMuniDefaultCodebook,
            UserAuthorized ua)
            throws BObStatusException, IntegrationException, AuthorizationException, EventException {

        if (sourceChecklist == null || targetChecklist == null || sourceChecklist.getMuni() == null || ua == null) {
            throw new BObStatusException("cannot clone checklist with null source or target template or requesting user");
        }

        OccChecklistIntegrator oci = getOccChecklistIntegrator();
        MunicipalityCoordinator mc = getMuniCoordinator();
        MunicipalityDataHeavy targetMuniDH;
        try {
            targetMuniDH = mc.assembleMuniDataHeavy(targetChecklist.getMuni(), ua);
        } catch (BlobException ex) {
            throw new BObStatusException("Cannot get MuniDH because of blob exception");
        }
        CodeSet codeSet = targetMuniDH.getCodeSet();
        targetChecklist.setDescription("Clone of checklist ID " + sourceChecklist.getInspectionChecklistID() + " copied at " + LocalDateTime.now().toString());
        // write the target checklist to the DB so we have a linking target
        int freshChecklistID = oci.insertChecklistTemplateMetadata(targetChecklist);
        targetChecklist = oci.getChecklistTemplate(freshChecklistID);

        // setup clone operation tracking & note utilities
        StringBuilder cloneOpNotes = new StringBuilder();
        buildCloneOperationHeaderNotes(cloneOpNotes, sourceChecklist, targetChecklist, writeMissingOrdsInTargetChecklistMuniDefaultCodebook);
        int countCodeBookAdds = 0;
        int countCodeBookSkips = 0;
        int countTotalOSEs = 0;

        // Clone each spacetype and then its internal elements
        for (OccSpaceTypeChecklistified ostc : sourceChecklist.getOccSpaceTypeList()) {
            System.out.println("OccInspectionCoordinator.cloneOccChecklistTemplate | cloning ost ID: " + ostc.getChecklistSpaceTypeID());
            // reassign space type to the target checklist and write record to DB
            OccSpaceTypeChecklistified clonedOSTC = ostc.copy();
            clonedOSTC.setChecklistParentID(targetChecklist.getInspectionChecklistID());
            int freshOSTID = oci.insertOccChecklistSpaceTypeChecklistified(clonedOSTC);
            OccSpaceTypeChecklistified ostcCloned = oci.getOccSpaceTypeChecklistified(freshOSTID);

            // now link all the individual elements to the new space type in the new checklist
            for (OccSpaceElement ose : ostc.getCodeElementList()) {
                OccSpaceElement oseCloned = ose.copy();
                System.out.println("OccInspectionCoordinator.cloneOccChecklistTemplate | visiting ose ID: " + ose.getElementID());
                if (!codeSet.contains(ose)) {
                    System.out.println("OccInspectionCoordinator.cloneOccChecklistTemplate | not found in Codebook : " + codeSet.getCodeSetID() + " but inserting");
                    // and check logic switches for adjusting target muni codebook as we go
                    if (writeMissingOrdsInTargetChecklistMuniDefaultCodebook && permissionsCheckpointChecklistCloneAddMissingOrdsToTargetCodeBook(ua)) {
                        CodeCoordinator cc = getCodeCoordinator();
                        oseCloned.setMuniSpecificNotes(buildNoteStringForFreshlyInsertedECEDuringChecklistCloneOperation(oseCloned, ua, sourceChecklist));
                        cc.insertEnforcableCodeElement(oseCloned, codeSet, ua, cc.getEnforcableCodeElementSkeleton(oseCloned));
                        cloneOpNotes.append("Added elementID: ");
                        cloneOpNotes.append(oseCloned.getElementID());
                        cloneOpNotes.append(" to codebook: ");
                        cloneOpNotes.append(codeSet.getCodeSetName());
                        cloneOpNotes.append("; ID: ");
                        cloneOpNotes.append(codeSet.getCodeSetID());
                        cloneOpNotes.append("<br>");
                        countCodeBookAdds += 1;
                    } else {
                        System.out.println("OccInspectionCoordinator.cloneOccChecklistTemplate | not found in Codebook : " + codeSet.getCodeSetID() + " skipping");
                        // skip this element if it is not in the target muni's default code set and we don't have permission to add the ece to the code book
                        countCodeBookSkips += 1;
                        continue;
                    }
                }
                System.out.println("OccInspectionCoordinator.cloneOccChecklistTemplate | cloning ose: ");
                // go ahead and link this element to the current space
                oseCloned.setParentSpaceTypeID(ostcCloned.getChecklistSpaceTypeID());
                oci.attachCodeElementsToSpaceTypeInChecklist(oseCloned);
                countTotalOSEs += 1;
            }
        }
        // write our log to the new checklist notes
        buildCloneOperationSummaryNotes(cloneOpNotes, codeSet, countTotalOSEs, countCodeBookAdds, countCodeBookSkips);
        targetChecklist.setNotes(cloneOpNotes.toString());
        oci.updateChecklistTemplateMetadata(targetChecklist);

        return freshChecklistID;
    }

    /**
     * Assembles a string explaining where an enforceable code element came from
     * when added to the target muni's default code book during a checklist
     * clone operation
     *
     * @param ose being added to the target muni's code book
     * @param ua doing the cloning
     * @param sourceTemplate being cloned
     * @return a String ready to become the notes of the new ECE being added to
     * the target Muni's default code book
     */
    private String buildNoteStringForFreshlyInsertedECEDuringChecklistCloneOperation(OccSpaceElement ose, UserAuthorized ua, OccChecklistTemplate sourceTemplate) {
        if (ose == null || ua == null || sourceTemplate == null) {
            return "This ordinance was automatically added to this code book during a checklist clone operation because this code book did not previously contain the "
                    + "ordinance being added to a checklist in this code books's parent municipality";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("This ordinance was added automatically to this code book during the cloning of checklist: ");
        sb.append(sourceTemplate.getTitle());
        sb.append("(");
        sb.append(sourceTemplate.getInspectionChecklistID());
        sb.append(")");
        sb.append(" By User: ");
        sb.append(ua.getUsername());
        return sb.toString();
    }

    /**
     * Utility for building a header for the clone target checklist notes
     *
     * @param cloneOpNotes
     * @param source
     * @param addOrds
     */
    private void buildCloneOperationHeaderNotes(StringBuilder cloneOpNotes, OccChecklistTemplate source, OccChecklistTemplate target, boolean addOrds) {
        if (cloneOpNotes == null || source == null) {
            return;
        }
        cloneOpNotes.append("Notes from clone of Checklist ");
        cloneOpNotes.append(source.getTitle());
        cloneOpNotes.append(" (ID:");
        cloneOpNotes.append(source.getInspectionChecklistID());
        cloneOpNotes.append("; muni: ");
        cloneOpNotes.append(source.getMuni().getMuniName());
        cloneOpNotes.append("):<br>");
        cloneOpNotes.append("Target checklist name: ");
        cloneOpNotes.append(target.getTitle());
        cloneOpNotes.append(" (Muni: ");
        cloneOpNotes.append(target.getMuni().getMuniName());
        cloneOpNotes.append(")<br>");
        cloneOpNotes.append("Param: Add ords to target muni checklist: ");
        cloneOpNotes.append(addOrds);
        cloneOpNotes.append("<br>");
    }

    /**
     * Utility for building closer for the clone target checklist notes
     *
     * @param cloneOpNotes
     * @param cs
     * @param total
     * @param adds
     * @param skips
     */
    private void buildCloneOperationSummaryNotes(StringBuilder cloneOpNotes, CodeSet cs, int total, int adds, int skips) {
        if (cloneOpNotes == null || cs == null) {
            return;
        }
        cloneOpNotes.append("Clone operation summary:<br>");
        cloneOpNotes.append("Total ordinances added to new checklist: ");
        cloneOpNotes.append(total);
        cloneOpNotes.append("<br>");
        cloneOpNotes.append("Total ordinances added to target muni default checklist: ");
        cloneOpNotes.append(cs.getCodeSetName());
        cloneOpNotes.append("; ID: ");
        cloneOpNotes.append(cs.getCodeSetID());
        cloneOpNotes.append(": ");
        cloneOpNotes.append(adds);
        cloneOpNotes.append("(Skipped: ");
        cloneOpNotes.append(skips);
        cloneOpNotes.append(")");

    }

    /**
     * ************************************************************
     * ********************* MISC ******************
     * ************************************************************
     *
     * @param requireActive
     * @param muni all are returned if null
     * @return
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public List<OccInspectionCause> getOccInspectionCauseListComplete(boolean requireActive, Municipality muni) throws IntegrationException, BObStatusException {
        OccInspectionIntegrator integrator = getOccInspectionIntegrator();
        return integrator.getOccInspectionCauseListComplete(requireActive, muni);
    }

    /**
     * Extracts a single cause from the DB
     * @param causeID
     * @return 
     */
    public OccInspectionCause getOccInspectioncause(int causeID) throws IntegrationException{
        OccInspectionIntegrator oic = getOccInspectionIntegrator();
        return oic.getOccInspectionCause(causeID);
    }
    
    /**
     * Factory for OCcInspection Cause objects
     *
     * @param umap
     * @return new cause with id = 0
     */
    public OccInspectionCause getOccInspectionCauseSkeleton(UserMuniAuthPeriod umap) {
        OccInspectionCause inspectionCause = new OccInspectionCause();
        inspectionCause.setCreatedby_UMAP(umap);
        inspectionCause.setLastUpdatedby_UMAP(umap);
        return inspectionCause;
    }

    /**
     * Insertion point for OccInspection determination
     *
     * @param inspectionCause
     * @param umap
     * @return OccInspectionCause
     * @throws IntegrationException
     * @throws AuthorizationException
     * @throws BObStatusException
     */
    public OccInspectionCause insertOccInspectionCause(OccInspectionCause inspectionCause, UserMuniAuthPeriod umap) throws IntegrationException, BObStatusException, AuthorizationException {
        if (Objects.isNull(inspectionCause) || Objects.isNull(umap)) {
            throw new BObStatusException("Cannot insert OccInspection Cause with null inspectionCause or UMAP");
        }
        if (umap.getRole() != RoleType.SysAdmin) {
            throw new AuthorizationException("UMAP must be rank: Sys Admin to insert Cause");
        }
        inspectionCause.setCreatedby_UMAP(umap);
        inspectionCause.setLastUpdatedby_UMAP(umap);

        OccInspectionIntegrator integrator = getOccInspectionIntegrator();
        inspectionCause.setCauseID(integrator.insertOccInspectionCause(inspectionCause));
        return inspectionCause;
    }

    /**
     * To update Determination
     *
     * @param inspectionCause
     * @param umap
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws AuthorizationException
     */
    public void updateOccInspectionCause(OccInspectionCause inspectionCause, UserMuniAuthPeriod umap) throws IntegrationException, BObStatusException, AuthorizationException {
        if (Objects.isNull(inspectionCause) || Objects.isNull(umap)) {
            throw new BObStatusException("Cannot update OccCause with null inspectionCause or UMAP");
        }
        if (umap.getRole() != RoleType.SysAdmin) {
            throw new AuthorizationException("UMAP must be rank: Sys Admin to update cause");
        }
        inspectionCause.setLastUpdatedby_UMAP(umap);

        OccInspectionIntegrator integrator = getOccInspectionIntegrator();
        OccInspectionCacheManager oicm = getOccInspectionCacheManager();
        
        integrator.updateOccInspectionCause(inspectionCause);
        oicm.flush(inspectionCause);
        
    }

    /**
     * To deactivate OccInspection Determination
     *
     * @param inspectionCause
     * @param umap
     * @throws BObStatusException
     * @throws AuthorizationException
     * @throws IntegrationException
     */
    public void deactivateOccInspectionCause(OccInspectionCause inspectionCause, UserMuniAuthPeriod umap) throws BObStatusException, AuthorizationException, IntegrationException {
        if (Objects.isNull(inspectionCause) || Objects.isNull(umap)) {
            throw new BObStatusException("Cannot deactivate OccInspection Cause with null inspectionCause or UMAP");
        }
        if (umap.getRole() != RoleType.SysAdmin) {
            throw new AuthorizationException("UMAP must be rank: Sys Admin to deactivate OccInspection Cause");
        }
        inspectionCause.setDeactivatedBy_UMAP(umap);
        SystemIntegrator si = getSystemIntegrator();
        si.deactivateUMAPTrackedEntity(inspectionCause);
        getOccInspectionCacheManager().flush(inspectionCause);
    }

    /**
     * Retrieves all active determinations for use in selection boxes
     *
     * @param requireActive
     * @param muni all are returned if null
     * @return
     * @throws IntegrationException
     */
    public List<OccInspectionDetermination> getOccDeterminationList(boolean requireActive, Municipality muni) throws IntegrationException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        List<OccInspectionDetermination> detList = oii.getOccInspectionDeterminationListComplete(requireActive, muni);

        return detList;
    }

    /**
     * Logic gateway for OccInspectionDetermination objects
     *
     * @param requireActive
     * @param muni all are returned if null
     * @return
     * @throws IntegrationException
     * @throws BObStatusException
     */
    public List<OccInspectionDetermination> getOccInspectionDeterminationListComplete(boolean requireActive, Municipality muni) throws IntegrationException, BObStatusException {
        OccInspectionIntegrator cii = getOccInspectionIntegrator();
        return cii.getOccInspectionDeterminationListComplete(requireActive, muni);
    }

    /**
     * Factory for OCcInspection Determination objects
     *
     * @param umap
     * @return new determination with id = 0
     */
    public OccInspectionDetermination getOccInspectionDeterminationSkeleton(UserMuniAuthPeriod umap) {
        OccInspectionDetermination inspectionDetermination = new OccInspectionDetermination();
        inspectionDetermination.setCreatedby_UMAP(umap);
        inspectionDetermination.setLastUpdatedby_UMAP(umap);
        return inspectionDetermination;
    }

    /**
     * Generates a skeleton field inspection report
     *
     * @param insp
     * @param inspectable
     * @param ua
     * @return
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.SearchException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.BlobException
     */
    public ReportConfigOccInspection getOccInspectionReportConfigDefault(FieldInspection insp,
            IFace_inspectable inspectable,
            UserAuthorized ua)
            throws IntegrationException, SearchException, BObStatusException, BlobException {

        if(insp == null || inspectable == null || ua == null){
            throw new BObStatusException("OccInspectionCoordinator.getOccInspectionReportConfigDefault | cannot build report with null inspectable, inspection, or user");
            
        }
        SystemIntegrator si = getSystemIntegrator();
        PropertyCoordinator pc = getPropertyCoordinator();
        CaseCoordinator cc = getCaseCoordinator();
        OccupancyCoordinator oc = getOccupancyCoordinator();

        ReportConfigOccInspection rpt = new ReportConfigOccInspection();
        rpt.setGenerationTimestamp(LocalDateTime.now());
        if (inspectable instanceof OccPeriod op) {
        System.out.println("OccInspectionCoordinator.getOccInspectionReportConfigDefault | found OccPeriod inspectable");
            rpt.setOccPeriod(oc.assembleOccPeriodDataHeavy(op, ua));
            try {
                rpt.setInspectedProperty(pc.getProperty(op.getParentParcelKey()));
            } catch (AuthorizationException ex) {
                throw new BObStatusException(ex.getMessage());
            }
            PropertyUnit pu = pc.getPropertyUnit(op.getPropertyUnitID());
            if (!pc.isUnitDefault(pu)) {
                rpt.setInspectedUnit(pu);
            }
        } else if (inspectable instanceof CECaseDataHeavy csedh) {
        System.out.println("OccInspectionCoordinator.getOccInspectionReportConfigDefault | found CECase inspectable");

            rpt.setCeCase(csedh);
            try {
                System.out.println("OccInspectionCoordinator.getOccInspectionReportConfigDefault | Parcel key: " + csedh.getParcelKey());
                rpt.setInspectedProperty(pc.getProperty(csedh.getParcelKey()));
            } catch (AuthorizationException ex) {
                throw new BObStatusException(ex.getMessage());
            }
            // check for a unit and inject if we've got a case unit--which is NOT required in the CE domain
            if (csedh.getUnitID() != 0 && !pc.isUnitDefault(csedh.getUnitID())) {
                rpt.setInspectedUnit(pc.getPropertyUnit(csedh.getUnitID()));
            }
        }

        rpt.setInspection(insp);

        rpt.setTitle(getResourceBundle(Constants.MESSAGE_TEXT).getString("report_occinspection_default_title"));
        rpt.setCreator(ua);
        rpt.setMuni(getSessionBean().getSessMuni());

        rpt.setDefaultItemIcon(si.getIcon(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                .getString(OccInspectionStatusEnum.NOTINSPECTED.getIconPropertyLookup()))));

        rpt.setIncludeParentObjectInfoHeader(false);
        rpt.setIncludeInspectedSpaceLocation(true);

        rpt.setIncludeOwnerInfo(true);
        rpt.setIncludeOwnerPhones(true);
        rpt.setIncludeOwnerAddresses(true);
        rpt.setIncludeOwnerEmails(true);

        rpt.setIncludeBedBathOccCounts(false);

        rpt.setIncludePhotos_pass(true);
        rpt.setIncludePhotos_fail(true);
        
        rpt.setIncludeOverallInspectionPhotos(insp.getBlobList() != null && !insp.getBlobList().isEmpty());
        
        rpt.setIncludeOverallInspectionPhotos(false);

        rpt.setIncludePhotoIDs(true);
        rpt.setIncludePhotoTitles(true);
        rpt.setIncludePhotoOriginalFileNames(false);
        rpt.setIncludePhotoDescriptions(true);
        rpt.setUnifiedPhotoWidth(DEFAULT_PHOTO_WIDTH);

        rpt.setIncludeFullOrdText(true);
        rpt.setIncludeOrdinanceFindings(true);

        rpt.setIncludeOrdinanceInspectionTimestamps(true);

        rpt.setIncludeRemedyInfo(false);
        rpt.setIncludeSignature(false);

        rpt.setViewSetting(ViewOptionsOccChecklistItemsEnum.FAILED_PASSEDWPHOTOFINDING);
        return rpt;
    }

    /**
     * Logic intermediary for getter of determination
     *
     * @param currentDetermination
     * @return
     * @throws IntegrationException
     */
    public int determinationCheckForUse(OccInspectionDetermination currentDetermination) throws IntegrationException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        return oii.determinationCheckForUse(currentDetermination);
    }

    public OccInspectionDetermination getDetermination(int determinationID) throws IntegrationException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        return oii.getOccInspectionDetermination(determinationID);
    }

    /**
     * To deactivate OccInspection Determination
     *
     * @param determination
     * @param umap
     * @throws BObStatusException
     * @throws AuthorizationException
     * @throws IntegrationException
     */
    public void deactivateOccInspectionDetermination(OccInspectionDetermination determination, UserMuniAuthPeriod umap) throws BObStatusException, AuthorizationException, IntegrationException {
        if (Objects.isNull(determination) || Objects.isNull(umap)) {
            throw new BObStatusException("Cannot deactivate OccInspection Determination with null determination or UMAP");
        }
        if (umap.getRole() != RoleType.SysAdmin) {
            throw new AuthorizationException("UMAP must be rank: Sys Admin to deactivate OccInspection Determination");
        }
        determination.setDeactivatedBy_UMAP(umap);
        SystemIntegrator si = getSystemIntegrator();
        si.deactivateUMAPTrackedEntity(determination);
        getOccInspectionCacheManager().flush(determination);
    }

    /**
     * To update Determination
     *
     * @param determination
     * @param umap
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws AuthorizationException
     */
    public void updateOccInspectionDetermination(OccInspectionDetermination determination, UserMuniAuthPeriod umap) throws IntegrationException, BObStatusException, AuthorizationException {
        if (Objects.isNull(determination) || Objects.isNull(umap)) {
            throw new BObStatusException("Cannot update OccDetermination with null determination or UMAP");
        }
        if (umap.getRole() != RoleType.SysAdmin) {
            throw new AuthorizationException("UMAP must be rank: Sys Admin to update Determination");
        }
        determination.setLastUpdatedby_UMAP(umap);

        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        OccInspectionCacheManager oicm = getOccInspectionCacheManager();
        
        oii.updateOccInspectionDetermination(determination);
        
        // cache flushing
        oicm.flush(determination);
        
    }

    /**
     * Insertion point for OccInspection determination
     *
     * @param determination
     * @param umap
     * @return OccInspectionDetermination
     * @throws IntegrationException
     * @throws AuthorizationException
     * @throws BObStatusException
     */
    public OccInspectionDetermination insertOccInspectionDetermination(OccInspectionDetermination determination, UserMuniAuthPeriod umap) throws IntegrationException, BObStatusException, AuthorizationException {
        if (Objects.isNull(determination) || Objects.isNull(umap)) {
            throw new BObStatusException("Cannot insert OccInspection Determination with null determination or UMAP");
        }
        if (umap.getRole() != RoleType.SysAdmin) {
            throw new AuthorizationException("UMAP must be rank: Sys Admin to insert Determination");
        }
        determination.setCreatedby_UMAP(umap);
        determination.setLastUpdatedby_UMAP(umap);

        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        determination.setDeterminationID(oii.insertOccInspectionDetermination(determination));
        return determination;
    }

    public int requirementCheckForUse(OccInspectionRequirement currentRequirement) throws IntegrationException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        return oii.requirementCheckForUse(currentRequirement);
    }

    public OccInspectionRequirement getRequirement(int requirementID) throws IntegrationException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        return oii.getRequirement(requirementID);
    }

    public void deactivateRequirement(OccInspectionRequirement currentRequirement) throws IntegrationException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        oii.deactivateRequirement(currentRequirement);
    }

    public void updateRequirement(OccInspectionRequirement currentRequirement) throws IntegrationException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        oii.updateRequirement(currentRequirement);
    }

    public void insertRequirement(OccInspectionRequirement currentRequirement) throws IntegrationException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        oii.insertRequirement(currentRequirement);
    }

    public List<OccInspectionRequirement> getRequirementList() throws IntegrationException {
        OccInspectionIntegrator oii = getOccInspectionIntegrator();
        return oii.getRequirementList();
    }

    /**
     * Filters the spaceElementList as per filter text filterText is matched
     * against ordinance header string and/or non IRC style header string
     *
     * @param spaceElementList
     * @param filterText
     * @return
     */
    public List<OccSpaceElement> inspectionAction_configureOccSpaceElementListFiltered(
            List<OccSpaceElement> spaceElementList,
            String filterText) {
        if (Objects.nonNull(spaceElementList)) {
            if (Objects.nonNull(filterText) && !filterText.isEmpty()) {
                return spaceElementList.stream()
                        .filter(element -> inspectionAction_isOrdinanceFilterMatches(filterText, element))
                        .collect(Collectors.toList());
            } else {
                return spaceElementList;
            }
        }
        return new ArrayList<>();
    }

    /**
     * To check ordinance filter text matches with header string or non IRC
     * Style Header String
     *
     * @param filterText
     * @param spaceElement
     * @return
     */
    private boolean inspectionAction_isOrdinanceFilterMatches(String filterText, OccSpaceElement spaceElement) {
        StringBuilder sb = new StringBuilder();
        if (Objects.nonNull(spaceElement.getHeaderString())) {
            sb.append(spaceElement.getHeaderString());
        }
        if (Objects.nonNull(spaceElement.getNonIrcStyleHeaderString())) {
            sb.append(spaceElement.getNonIrcStyleHeaderString());
        }
        return StringUtil.isStringContainsI(sb.toString(), filterText);
    }

    public List<OccSpaceElement> getFilterSelectedList(List<OccSpaceElement> occSpaceElementListFiltered,
            Set<OccSpaceElement> occSpaceElementListPreviousSelected) {
        return occSpaceElementListFiltered.stream()
                .filter(occSpaceElementListPreviousSelected::contains)
                .collect(Collectors.toList());
    }

    public Set<OccSpaceElement> updatePreviousSelectedList( Set<OccSpaceElement> occSpaceElementListPreviousSelected,
                                                            List<OccSpaceElement> occSpaceElementListSelected,
                                                            List<OccSpaceElement> occSpaceElementListFiltered) {
        Set<OccSpaceElement> removedElements = new HashSet<>();

        occSpaceElementListFiltered.stream()
                .filter(occ -> !occSpaceElementListSelected.contains(occ))
                .forEach(occ
                        -> {
                    if (occSpaceElementListPreviousSelected.remove(occ)) {
                        removedElements.add(occ);
                    }
                });

        return removedElements;
    }

}
