/**
 * Copyright (C) 2017 Turtle Creek Valley
 * Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.coordinators;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.domain.*;
import com.tcvcog.tcvce.entities.*;
import com.tcvcog.tcvce.entities.occupancy.OccPeriod;
import com.tcvcog.tcvce.entities.occupancy.OccPeriodDataHeavy;
import com.tcvcog.tcvce.entities.occupancy.OccPeriodStatusEnum;
import com.tcvcog.tcvce.entities.search.QueryCEAR;
import com.tcvcog.tcvce.entities.search.QueryCEAREnum;
import com.tcvcog.tcvce.entities.search.QueryCECase;
import com.tcvcog.tcvce.entities.search.QueryCECaseEnum;
import com.tcvcog.tcvce.entities.search.QueryMailingCityStateZip;
import com.tcvcog.tcvce.entities.search.QueryMailingCityStateZipEnum;
import com.tcvcog.tcvce.integration.PropertyCacheManager;
import com.tcvcog.tcvce.integration.PropertyIntegrator;
import com.tcvcog.tcvce.integration.SystemIntegrator;
import com.tcvcog.tcvce.util.Constants;
import com.tcvcog.tcvce.util.MessageBuilderParams;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;

/**
 * Business logic container for All Things Property related
 * 
 * @author Dominic Pimpinella, MaRoSco, Ellen Bascomb (31Y)
 */
public class PropertyCoordinator extends BackingBeanUtils implements Serializable {

    final String DEFAULTUNITDESIGNATOR = "PRIMARY";
    final boolean DEFAULTRENTAL = false;
    final static String SPACE = " ";
    final static String COMMA_SPACE = ", ";
    final static String HTML_BR = "<br /> ";
    final static String SEMICOLON_SPACE = "; ";
    final static String POBOX = "PO BOX: ";
    final static String ATTN = "ATTN: ";
    final static int ZIPCODE_REQUIRED_LENGTH = 5;
     /**
     * Creates a new instance of PropertyUnitCoordinator
     */
    public PropertyCoordinator() {
    }

        
     /**
     * 
     * @param ua
     * @return 
     */
    public boolean permissionCheckpointEditparcelAndPropertyInfo(UserAuthorized ua){
        UserCoordinator uc = getUserCoordinator();
        if(ua == null){
            return false;
        }
        try {
            // sylvia gets an exception, regardless of UMAP status on parcel info
            if(ua.getUserID() == uc.user_getUserRobot().getUserID()){
                return true;
            }
        } catch (BObStatusException | IntegrationException ex){
            System.out.println(ex);
            return false;
        }
        if(ua.getCrossMuniViewAuthorizationTS() != null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        if(!ua.getKeyCard().isHasMuniStaffPermissions()){
            return false;
        }
        if(ua.getGoverningMuniProfile().isManagerRequiredAbandonment() && !ua.getKeyCard().isHasMuniManagerPermissions()){
            return false;
        }
        
        return true;
        
    }
    
    
    /**
     * Restricts property views and associated object views to inside the user's UMAP's
     * municipality
     * @param pcl
     * @param ua
     * @return 
     */
    public boolean permissionCheckpointViewParcel(Parcel pcl, UserAuthorized ua){
        if(pcl == null || ua == null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        if(!ua.getKeyCard().isHasMuniStaffPermissions()){
            return false;
        }
        if(ua.getGoverningMuniProfile().isManagerRequiredAbandonment() && !ua.getKeyCard().isHasMuniManagerPermissions()){
            return false;
        }
        
        return true;
    }
    
    /**
     * Permissions gate for Mailing streets; for no system admins, no links can exist for any address
     * on the the given street
     * @param street
     * @param ua
     * @return
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws AuthorizationException
     * @throws EventException 
     */
    public boolean permissionCheckpointDeacStreet(MailingStreet street, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException, EventException{
        if(street == null || ua == null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        // check for mailing addresses on this street with links\
        // if any addresses on this street have links, deny to non sys admins
        List<MailingAddress> addrList = getMailingAddressListByStreet(street);
        for(MailingAddress addr: addrList){
            List<MailingAddressLinkParentObjectInfoHeavy> linkList = getMailingAddressLinkList(addr, ua);
            if(linkList != null && !linkList.isEmpty()){
                System.out.println("PropertyCoordinator.permissionCheckpointDeacStreet | found address link; refuse to deac non-sysadmins");
                return false;
            }
            
        }
        
        return true;
    }
    
    /**
     * Permissions gate for deac of a mailing address; no links allowed to deac by non system admins
     * @param addr
     * @param ua
     * @return
     * @throws BObStatusException
     * @throws IntegrationException
     * @throws AuthorizationException
     * @throws EventException 
     */
    public boolean permissionCheckpointDeacMailingAddress(MailingAddress addr, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException, EventException {
        if(addr == null || ua == null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        // check for any linkages to this mailing address and deny if links are present
        List<MailingAddressLinkParentObjectInfoHeavy> linkList = getMailingAddressLinkList(addr, ua);
        if(linkList != null && !linkList.isEmpty()){
            System.out.println("PropertyCoordinator.permissionCheckpointDeacMailingAddress | found address link; refuse to deac non-sysadmins");
            return false;
        }
        
        return true;
    }
    
    /**
     * Checks permission to merge mailing streets or addresses
     * @param ua
     * @return 
     */
    public boolean permissionMergeStreetOrAddress(UserAuthorized ua){
        if(ua == null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
          if(!ua.getKeyCard().isHasMuniStaffPermissions()){
            return false;
        }
        return true;
        
    }
    
    
    
    /**
     *  ***************************************************
     *  ***************************************************
     *  ************* MAILING ADDRESS QUICK ADD ***********
     *  ***************************************************
     *  ***************************************************
     */
  
    /**
     * Factory for quick add bundles
     * @return the skeleton object
     */
    public MailingAddressQuickAddBundle getMailingAddressQuickAddBundleSkeleton(){
        return new MailingAddressQuickAddBundle();
    }
    
    /**
     * Searches the DB for matching addresses based on the building, street, and zip raw fields
     * in the given bundle
     * @param qaBundle must have non-null and non-emtpy building, street, and zip codes and CANNOT have a non-null
     * candidate list or chosen match
     * @return the given qa bundle
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public MailingAddressQuickAddBundle searchForMatchingAddressCandidates(MailingAddressQuickAddBundle qaBundle) throws BObStatusException, IntegrationException{
        SearchCoordinator sc = getSearchCoordinator();
        
        if(qaBundle == null){
            throw new BObStatusException("Cannot search for matches given null bundle input");
        }
        if(qaBundle.getMadCandidateList() != null || qaBundle.getChosenMADMatch() != null){
            throw new BObStatusException("malformed quick add bundle: candidate list cannot be non-null and chosen match cannot be non-null");
        }
        if(qaBundle.getQuickAddZIPCode() == null || qaBundle.getQuickAddZIPCode().length() != ZIPCODE_REQUIRED_LENGTH){
            throw new BObStatusException("ZIP code cannot be null and must be exactly five digits;");
        }
        if(isStringEmptyIsh(qaBundle.getQuickAddStreet())){
            throw new BObStatusException("Street cannot be empty or close to empty");
        }
        if(isStringEmptyIsh(qaBundle.getQuickAddBuilding())){
            throw new BObStatusException("Building cannot be empty or close to empty");
        }
        // okay we have a valid bundle for searching so scrub our building and street
        qaBundle.setQuickAddBuilding(scrubMailingAddressComponent(qaBundle.getQuickAddBuilding()));
        qaBundle.setQuickAddStreet(scrubMailingAddressComponent(qaBundle.getQuickAddStreet()));
        
        // go find a matching ZIP code first
        QueryMailingCityStateZip qzip = sc.initQuery(QueryMailingCityStateZipEnum.DEFAULT_RECORD_BY_ZIPCODE, getSessionBean().getSessUser().getKeyCard());
        qzip.getPrimaryParams().setZip_val(qaBundle.getQuickAddZIPCode());
        MailingCityStateZip locatedZIP = null;
        List<MailingCityStateZip> zipResultList;
        try {
            qaBundle.append("Looking for ZIP Code: ");
            qaBundle.append(qaBundle.getQuickAddZIPCode());
            zipResultList = sc.runQuery(qzip).getResults();
        } catch (SearchException ex) {
            qaBundle.append(Constants.FMT_HTML_BREAK);
            qaBundle.append("Error search for ZIP Code;");
            return qaBundle;
        }
        if(zipResultList != null && !zipResultList.isEmpty()){
            // we should only have one result here if any result since we're searching for default CSZ
            locatedZIP = zipResultList.get(0);
        } else {
            qaBundle.append(Constants.FMT_HTML_BREAK);
            qaBundle.append("Could not find a ZIP code candidate for: ");
            qaBundle.append(qaBundle.getQuickAddZIPCode());
            qaBundle.append(". Please revise your search;");
            // without a ZIP we abort
            return qaBundle;
        }
        // setup our candidate addresses
        qaBundle.setMadCandidateList(new ArrayList<>());
        // now look for a matching Street
        List<MailingStreet> locatedStreetList = null;
        try {
            // now look for a street in our found ZIP
            qaBundle.append(Constants.FMT_HTML_BREAK);
            qaBundle.append("Looking for street: ");
            qaBundle.append(qaBundle.getQuickAddStreet());
            locatedStreetList = searchForMailingStreet(qaBundle.getQuickAddStreet(), locatedZIP);
        } catch (IntegrationException ex) {
            qaBundle.append(Constants.FMT_HTML_BREAK);
            qaBundle.append("Error locating a street on your given ZIP Code: ");
            if(locatedZIP != null && locatedZIP.getZipCode() != null){
                qaBundle.append(locatedZIP.getZipCode());
            }
            // if we get an error we'll abort
            return qaBundle;
        }
        if(locatedStreetList != null && !locatedStreetList.isEmpty()){
            // if we don't have a matching building, this list of streets in the
            // bundle can help with the eventual new address write
            qaBundle.setToWriteStreetCandidateList(locatedStreetList);
            // we've got at least one street to work with so look for building on this street
            
            for(MailingStreet street: locatedStreetList){
                qaBundle.append(Constants.FMT_HTML_BREAK);
                qaBundle.append("Searching for address in located street ID: ");
                qaBundle.append(String.valueOf(street.getStreetID()));
                qaBundle.append(" named: ");
                qaBundle.append(street.getName());
                qaBundle.getMadCandidateList().addAll(findMatchingAddressesInStreetList(street, qaBundle.getQuickAddBuilding()));
            }
            
        } 
        
        qaBundle.append(Constants.FMT_HTML_BREAK);
        qaBundle.append("Preparing candidate address insert fields");
        // valid street search so let's gear for confirm insert
        qaBundle.setToWriteZIPCode(locatedZIP);
        qaBundle.setToWriteStreet(qaBundle.getQuickAddStreet().toUpperCase());
        qaBundle.setToWriteBuildingNo(qaBundle.getQuickAddBuilding().toUpperCase());
        qaBundle.setValidAddressFieldsMatchingComplete(LocalDateTime.now());
            
        return qaBundle;
    }
    
    /**
     * Removes all weird characters from a building number or street name, which basically
     * will only allow numbers, letters, spaces, and a dash -.
     * All others, including a period . and comma , will be removed before writing to DB or matching
     * 
     * @param comp
     * @return 
     */
    private String scrubMailingAddressComponent(String comp){
       if(comp != null){
           String regExpMatchingWeirdChars = "[^a-zA-Z0-9\\s\\-]";
           return comp.replaceAll(regExpMatchingWeirdChars, "");
       } 
       return comp;
    }
    
    /**
     * Utility for returning matched addresses in a given street
     * @param street if null empty list returned
     * @param buildingNo if null empty list returned
     * @return at least an empty list, and perhaps a non-empty list with MADs that match
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public List<MailingAddress> findMatchingAddressesInStreetList(MailingStreet street, String buildingNo) throws IntegrationException, BObStatusException{
        List<MailingAddress> addressMatches = new ArrayList<>();
        if(street != null && buildingNo != null){
            List<MailingAddress> candidateAddresses = getMailingAddressListByStreet(street);
            if(candidateAddresses != null && !candidateAddresses.isEmpty()){
                for(MailingAddress mad: candidateAddresses){
                    if(mad.getBuildingNo() != null && mad.getBuildingNo().contains(buildingNo)){
                        addressMatches.add(mad);
                    }
                }
            }
        }
        return addressMatches;
    }
    
    /**
     * Logic block to reset the candidate list but preserve form fields on a quick add bundle
     * @param qaBundle
     * @return
     * @throws BObStatusException 
     */
    public MailingAddressQuickAddBundle mailingAddressQuickAddClearSearch(MailingAddressQuickAddBundle qaBundle) throws BObStatusException{
        if(qaBundle == null){
            throw new BObStatusException("Cannot clear fields on null quick add bundle!");
        }
        qaBundle.setMadCandidateList(null);
        qaBundle.setChosenMADMatch(null);
        return qaBundle;
        
    }
    
    /**
     * Business logic block for writing a new MailingAddress to the DB using a 
     * QuickAddBundle.
     * @param qaBundle requires that the bundle has beeen configured through a call 
     * to this class's searchForMatchingAddressCandidates method
     * @param ua requesting the fresh write
     * @return the ID of the fresh MAD
     */
    public int mailingAddressQuickAddCommitNewAddress(MailingAddressQuickAddBundle qaBundle, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(ua == null){
            throw new BObStatusException("Cannot insert new quick add mad with null requesting user");
        }
        if(qaBundle == null){
            throw new BObStatusException("Cannot insert a null quick address bundle");
        }
        if(qaBundle.getMadCandidateList() == null){
            throw new BObStatusException("The given QuickAdd bundle must have a non-null candidate list to reflect a match search having been run");
        }
        
        MailingStreet str = null;
        // if we don't have any street candidates, write a new street
        if(qaBundle.getToWriteStreetCandidateList() != null && !qaBundle.getToWriteStreetCandidateList().isEmpty()){
            // if we do have one or more streets that match, we'll write the new address with the first street in the list
            str = qaBundle.getToWriteStreetCandidateList().get(0);
        } else {
            // write the street 
            str = getMailingStreetSkeleton(qaBundle.getToWriteZIPCode());
            str.setName(qaBundle.getToWriteStreet());
            int freshStr = insertMailingStreet(str, ua);
            str = getMailingStreet(freshStr);
        }
        MailingAddress mad = getMailingAddressSkeleton();
        mad.setUseBuildingNo(true);
        mad.setBuildingNo(qaBundle.getToWriteBuildingNo());
        mad.setStreet(str);
        // force an audit of our building number
        int freshAddrID = insertMailingAddress(mad, ua);
        
        return freshAddrID;
        
    }
    
    
    
    /**
     *  ***************************************************
     *  ***************************************************
     *  ************* MAILING ADDRESS CENTRAL *************
     *  ***************************************************
     *  ***************************************************
     */
    
    
    /**
     * Extracts linked MailingAddresses by implementer of the IFace_addressListHolder
     * which in March 2022 were Property and Person only
     * @param adlh
     * @return the MailingAddressLink list for injection
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public List<MailingAddressLink> getMailingAddressLinkList(IFace_addressListHolder adlh) throws BObStatusException, IntegrationException{
        if(adlh == null || adlh.getLinkedObjectSchemaEnum() == null){
            throw new BObStatusException("Cannot get AddressLinks with null list holder or schema");
        }
        PropertyIntegrator pi = getPropertyIntegrator();
        List<MailingAddressLink> madLinkList = pi.getMailingAddressLinks(adlh);
        Collections.sort(madLinkList);
        return madLinkList;
    }
    
    
    
    
    /**
     * Implements a "reverse search" on mailing addresses, meaning this method
     * finds all the links that point to the given mailing address.
     * @param mad cannot be null
     * @param ua
     * @return a heterogeneous list of mailing address links that all point to the given MAD
     * @throws BObStatusException
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.EventException 
     */
    public List<MailingAddressLinkParentObjectInfoHeavy> getMailingAddressLinkList(MailingAddress mad, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException, EventException{
        if(mad == null ){
            throw new BObStatusException("Cannot get AddressLinks with null address");
        }
        PropertyIntegrator pi = getPropertyIntegrator();
        
        List<MailingAddressLink> aggregatedMADLinkList = new ArrayList<>();
        List<MailingAddressLinkParentObjectInfoHeavy> madLinkParentHeavyList = new ArrayList<>();
        
        // build a list of all the schema enums that are in the mailing family.
        // This really should be its own enum and then i could just call .values()
        // but that's not how the link system was implemented many years ago.
        List<LinkedObjectSchemaEnum> schemaList = new ArrayList<>();
        schemaList.add(LinkedObjectSchemaEnum.ParcelMailingaddress);
        schemaList.add(LinkedObjectSchemaEnum.ParcelUnitMailingAddress);
        schemaList.add(LinkedObjectSchemaEnum.MailingaddressHuman);
        
        for(LinkedObjectSchemaEnum lose: schemaList){
            aggregatedMADLinkList.addAll(pi.getMailingAddressLinkListByMailingAddress(lose, mad));
            System.out.println("PropertyCoordinator.getMailingAddressLinkList | aggregate list size: " + aggregatedMADLinkList.size());
        }
        
        // flesh out our parent object info
        if(!aggregatedMADLinkList.isEmpty()){
            for(MailingAddressLink madLink: aggregatedMADLinkList){
                MailingAddressLinkParentObjectInfoHeavy madLinkPOIH  = new MailingAddressLinkParentObjectInfoHeavy(madLink);
                madLinkPOIH = configureMailingAddressLinkParentInfoHeavy(madLinkPOIH, ua);
                if(madLinkPOIH == null){
                    System.out.println("PropertyCoordinator.getMailingAddressLinkList | unable to generate addressListHolder fo mad ID: " + madLink.getAddressID() + " | linkID: " + madLink.getLinkID());
                } else {
                    madLinkParentHeavyList.add(madLinkPOIH);
                }
            }
        }
        
        return madLinkParentHeavyList;
    }
   
    
    /**
     * Hunts down the parent object of the given MailingAddressLink for use in
     * reverse address lookups using a switch on the schema enum
     * @param madLinkPOIH Must have its schema enum set and have parent object ID
     * @param ua
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public MailingAddressLinkParentObjectInfoHeavy configureMailingAddressLinkParentInfoHeavy(MailingAddressLinkParentObjectInfoHeavy madLinkPOIH, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException, EventException{
        if(madLinkPOIH == null 
                || madLinkPOIH.getLinkedObjectRole() == null 
                || madLinkPOIH.getLinkedObjectRole().getSchema().getLinkedObjectFamilyEnum() == null 
                || madLinkPOIH.getLinkedObjectRole().getSchema().getLinkedObjectFamilyEnum() != LinkedObjectFamilyEnum.MAILING){
            System.out.println("PropertyCoordinator.getAddressListHolder | failed attempt to build address list holder due to null or incorrect input params");
            return null;
        }
        System.out.println("PropertyCoordinator.configureMailingAddressLinkParentInfoHeavy | madLinkID: " + madLinkPOIH.getLinkID() + " | schema: " + madLinkPOIH.getLinkedObjectRole().getSchema().name());
        
        switch(madLinkPOIH.getLinkedObjectRole().getSchema()){
            case MailingaddressHuman -> {
                PersonCoordinator pc = getPersonCoordinator();
                Person parent = pc.getPersonByHumanID(madLinkPOIH.getTargetObjectPK());
                if(parent != null){
                    madLinkPOIH.setMuiniParent(null);
                    // persons dont really have a muni (although they can have a home muni which isn't really honored or set yet as of MAY 2024
                    madLinkPOIH.setAllowCrossMuniView(true);
                    madLinkPOIH.setExternalToSessionMuni(false);
                    madLinkPOIH.setParentObjectDescription(parent.getParentHumanFriendlyDescriptiveString());
                }
            }
            case ParcelMailingaddress -> {
                Property propParent = getProperty(madLinkPOIH.getTargetObjectPK());
                if(propParent != null){
                    madLinkPOIH.setMuiniParent(propParent.getMuni());
                    if(madLinkPOIH.getMuiniParent().equals(ua.getKeyCard().getGoverningAuthPeriod().getMuni().getMuniCode())){
                        // the prop is in our user's muni
                        madLinkPOIH.setExternalToSessionMuni(false);
                        madLinkPOIH.setAllowCrossMuniView(false);
                    } else {
                        madLinkPOIH.setExternalToSessionMuni(true);
                        madLinkPOIH.setAllowCrossMuniView(false);
                    }    
                    madLinkPOIH.setParentObjectDescription(propParent.getParentHumanFriendlyDescriptiveString());
                }
            }
            case ParcelUnitMailingAddress -> {
                PropertyUnitWithProp unitParent = getPropertyUnitWithProp(madLinkPOIH.getTargetObjectPK());
                if(unitParent != null){
                    madLinkPOIH.setMuiniParent(unitParent.getProperty().getMuni());
                    if(madLinkPOIH.getMuiniParent().equals(ua.getKeyCard().getGoverningAuthPeriod().getMuni().getMuniCode())){
                        // the prop is in our user's muni
                        madLinkPOIH.setExternalToSessionMuni(false);
                        madLinkPOIH.setAllowCrossMuniView(false);
                    } else {
                        madLinkPOIH.setExternalToSessionMuni(true);
                        madLinkPOIH.setAllowCrossMuniView(false);
                    }    
                    // persons dont really have a muni (although they can have a home muni which isn't really honored or set yet as of MAY 2024
                    PropertyUnitDataHeavy pudh = getPropertyUnitDataHeavy(unitParent, ua);
                    madLinkPOIH.setParentObjectDescription(pudh.getParentHumanFriendlyDescriptiveString());
                }
            }
            default -> {
                System.out.println("PropertyCoordinator.getAddressListHolder | Unsupported address holder");
                return null;
            }
        }
        return madLinkPOIH;
    }
    
    
    /**
     * Factory for mailing address links.
     * 
     * @param mad to which an object would like to link
     * @return the instantiated MAD link without a valid database ID
     */
    public MailingAddressLink getMailingAddressLinkSkeleton(MailingAddress mad){
        
        MailingAddressLink madLink = new MailingAddressLink(mad);
       
        
        return madLink;
    }
    
    /**
     * Getter for MAD links
     * @param holder
     * @param linkID
     * @return the MAD link
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public MailingAddressLink getMailingAddressLink(IFace_addressListHolder holder, int linkID) throws IntegrationException, BObStatusException{
        PropertyIntegrator pi = getPropertyIntegrator();
        return pi.getMailingAddressLink(holder, linkID);
        
    }
    
    
    /**
     * Creates a new link between an implementer of our interface for addressLists
     * and any old Mailing address. Will inject a default role if null.
     * @param adlh target of link
     * @param madLink to link
     * @param ua
     * @return the linkID of the fresh link. You should also be able to get this link
 just by calling getMailingAddressLinkListByMailingAddress
     */
    public int linkToMailingAddress(IFace_addressListHolder adlh, MailingAddressLink madLink, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(adlh == null || madLink == null || ua == null){
            throw new BObStatusException("Cannot deactivate a mailing link with null link or UA or address");
        }
        PropertyIntegrator pi = getPropertyIntegrator();
        SystemIntegrator si =getSystemIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        
        // make sure our address is not deactivated
        MailingAddressLink ml = getMailingAddressLink(adlh, madLink.getLinkID());
        if(ml != null){
            if(!ml.isActive()){
                throw new BObStatusException("Cannot link to a deactivated mailing address.");
            }
        }
        
        if(madLink.getLinkedObjectRole() == null){
            
            if(adlh instanceof Property){
                madLink.setLinkRole(si.getLinkedObjectRole(
                        Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                                .getString("default_linkedobjectrole_parcel_mailing"))));
            } else if (adlh instanceof Person){
                madLink.setLinkRole(si.getLinkedObjectRole(
                        Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                                .getString("default_linkedobjectrole_human_mailing"))));
            }
        } 
        
        madLink.setSource(sc.getBObSource(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                            .getString("bobsourcePropertyInternal"))));
        
        madLink.setLinkCreatedByUserID(ua.getUserID());
        madLink.setLinkLastUpdatedByUserID(ua.getUserID());
        int freshid = pi.linkMailingAddress(adlh, madLink);
        getPropertyCacheManager().flush(madLink);
        return  freshid;
        
    }
    
    /**
     * Logic intermediary for updates to a mailing address link
     * @param madLink
     * @param ua 
     */
    public void updateMailingAddressLink(MailingAddressLink madLink, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(madLink == null || ua == null){
            throw new BObStatusException("Cannot deactivate a mailing link with null link or UA");
        }
        
        PropertyIntegrator pi = getPropertyIntegrator();
        
        madLink.setLinkLastUpdatedByUserID(ua.getUserID());
        
        pi.updateMailingAddressLink(madLink);
        
        getPropertyCacheManager().flush(madLink);
        
    }

    /**
     * For use by the mailing address merging facility to rewire the given Link to the target MAD.
     * This method does not clone the link and relink, it actually will change the FK field.
     * 
     * USED WITH EXTREME CAUTION, well, maybe only some caution
     * 
     * @param madlpoih to be rerouted to point to the given target
     * @param target to which the given link should now point
     * @param ua requesting the rewire
     */
    private void relinkMailingAddressLink(MailingAddressLinkParentObjectInfoHeavy madlpoih, MailingAddress target, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(madlpoih == null || target == null || ua == null){
            throw new BObStatusException("Cannot rewire mad link with null link, target, or user");
        }
        SystemCoordinator sc = getSystemCoordinator();
        PropertyIntegrator pi = getPropertyIntegrator();
        
        StringBuilder sb = new StringBuilder("Original target address ID: ");
        sb.append(madlpoih.getAddressID());
        
        madlpoih.setAddressID(target.getAddressID());
        sb.append(". Updated to address ID: ");
        sb.append(target.getAddressID());
        
        madlpoih.setLastUpdatedBy(ua);
        MessageBuilderParams mbp = new MessageBuilderParams(madlpoih.getLinkNotes(), "Link rerouted during address merge", null, sb.toString(), ua, null);
        madlpoih.setLinkNotes(sc.appendNoteBlock(mbp));
        
        pi.relinkMailingAddressLink(madlpoih);
    }
    
  
     /**
     * Logic intermediary for updates to a mailing address link
     * @param madLink
     * @param hide when true I hide, when false, I unhide
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void hideUnhideMailingAddressLink(MailingAddressLink madLink, boolean hide, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(madLink == null || ua == null){
            throw new BObStatusException("Cannot hide a mailing link with null link or UA");
        }
        PropertyIntegrator pi = getPropertyIntegrator();
        if(hide){
            madLink.setPriority(Constants.HIDDEN_RECORD_PRIORITY);
        } else {
            madLink.setPriority(Constants.UNHIDDEN_RECORD_PRIORITY);
        }
        madLink.setLinkLastUpdatedByUserID(ua.getUserID());
        
        pi.hideUnhideMailingAddressLink(madLink);
        getPropertyCacheManager().flush(madLink);
        
    }
    
    
    /**
     * writes a timestamp and user to a mailing address link's linking table record, 
     * which is as close to deletion as we'll get
     * 
     * @param madLink
     * @param ua
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public void deactivateLinkToMailingAddress(MailingAddressLink madLink, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(madLink == null || ua == null){
            throw new BObStatusException("Cannot deactivate a mailing link with null link or UA");
        }
        PropertyIntegrator pi = getPropertyIntegrator();
        
        madLink.setLinkDeactivatedByUserID(ua.getUserID());
        madLink.setDeactivatedTS(LocalDateTime.now());
        madLink.setLinkLastUpdatedByUserID(ua.getUserID());
        
        pi.updateMailingAddressLink(madLink);
        getPropertyCacheManager().flush(madLink);
    }
    
    
    /**
     * Extracts the official ZIP codes for a given municipality
     * @param muni
     * @return the list of official Zips
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public List<MailingCityStateZip> getZipListByMunicipality(Municipality muni) throws IntegrationException, BObStatusException{
        PropertyIntegrator pi = getPropertyIntegrator();
        List<Integer> zipIDList = pi.getMailingCityStateZipListByMuni(muni);
        List<MailingCityStateZip> zipObList = new ArrayList<>();
        if(zipIDList != null && !zipIDList.isEmpty()){
            for(Integer i: zipIDList){
                zipObList.add(getMailingCityStateZip(i));
            }
        }
        return zipObList;
    }
    
   
    /**
     * Getter for MailingAddress objects
     * @param addressid the ID of the requested address, cannot be zero
     * @return the object
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public MailingAddress getMailingAddress(int addressid) throws IntegrationException, BObStatusException {
        PropertyIntegrator pi = getPropertyIntegrator();
        return configureMailingAddress(pi.getMailingAddress(addressid));
        
    }
    
    /**
     * Organ for extracting all active mailing addresses from DB for a filter search
     * @return the entire database's mailing address list - RAM beware
     */
    public List<MailingAddress> getMailingAddressListComplete() throws IntegrationException, BObStatusException{
        PropertyIntegrator pi = getPropertyIntegrator();
        List<Integer> addrIDList = pi.getMailingAddressListAll();
        List<MailingAddress> addrList = new ArrayList<>();
        for(Integer madid: addrIDList){
            addrList.add(getMailingAddress(madid));
        }
        return addrList;
    }
    
    /**
     * Internal logic for setting up fields on a mailing address
     * @param addr to configure
     * @return ref to the configured object
     */
    private MailingAddress configureMailingAddress(MailingAddress addr) throws BObStatusException, IntegrationException{
        if(addr == null){
            throw new BObStatusException("Cannot configure null mailing address");
        } 
        
        addr.setAddressPretty2LineEscapeFalse(buildPropertyAddressStrings(addr, true, true));
        addr.setAddressPretty1Line(buildPropertyAddressStrings(addr, false, false));
        return addr;
    }
    
    /**
     * Getter for Streets
     * @param streetid the street ID
     * @return the object
     */
    public MailingStreet getMailingStreet(int streetid) throws IntegrationException, BObStatusException{
        PropertyIntegrator pi = getPropertyIntegrator();
        return pi.getMailingStreet(streetid);
        
    }
    
    /**
     * Factory for MailingStreet objects
     * @param csz to be injected into the new street, can be null
     * @return the empty MailingStreet with id = 0
     */
    public MailingStreet getMailingStreetSkeleton(MailingCityStateZip csz){
        MailingStreet ms = new MailingStreet();
        ms.setCityStateZip(csz);
        return ms;
    }
    
    /**
     * Factory method for address objects
     * @return 
     */
    public MailingAddress getMailingAddressSkeleton(){
        MailingAddress ma = new MailingAddress();
        ma.setStreet(new MailingStreet());
        ma.setUseBuildingNo(true);
        ma.setUseSecondaryUnitIdentifier(false);
        ma.setUsePOBox(false);
        ma.setUseAttention(false);
        return ma;
    }
    
    
    /**
     * Logic container for inserting a new MailingStreet
     * @param street
     * @param ua
     * @return
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public int insertMailingStreet(MailingStreet street, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(street == null || street.getCityStateZip() == null || ua == null){
            throw new BObStatusException("Cannot insert street with null street, zip, or user");
        }
        
        PropertyIntegrator pi = getPropertyIntegrator();
        street.setCreatedBy(ua);
        street.setLastUpdatedBy(ua);
        return pi.insertMailingStreet(street);
        
    }
    
    
    /**
     * Logic intermediary for updating a MailingStreet
     * @param street
     * @param ua
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public void updateMailingStreet(MailingStreet street, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(street == null || street.getCityStateZip() == null || ua == null){
            throw new BObStatusException("Cannot update street with null street, zip, or user");
        }
        
        PropertyIntegrator pi = getPropertyIntegrator();
        street.setLastUpdatedBy(ua);
        pi.updateMailingStreet(street);
        getPropertyCacheManager().flush(street);
    }
    
    
    /**
     * Logic container for deactivating a street record
     * I will also deactivate all addresses linked to this 
     * street and all the links to those addresses!
     * @param street to deactivate
     * @param ua doing the deactivating
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public void deactivateMailingStreet(MailingStreet street, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException, EventException{
        if(street == null || street.getCityStateZip() == null || ua == null){
            throw new BObStatusException("Cannot deactivate street with null street, zip, or user");
        }
        if(!permissionCheckpointDeacStreet(street, ua)){
            throw new AuthorizationException("User forbidden from deactivation of streets with addresses that are linked to any other system object");
        }
        // start by deactivating the addresses on this street
        
        List<MailingAddress> mal = getMailingAddressListByStreet(street);
        if(mal != null && !mal.isEmpty()){
            for(MailingAddress ma: mal){
                // which will recursively delete all links on these mailing addresses if we've passed
                // permissions checks on this we'll just go and do so
                deactivateMailingAddress(ma, ua);
            }
        }
        
        PropertyIntegrator pi = getPropertyIntegrator();
        street.setLastUpdatedBy(ua);
        street.setDeactivatedBy(ua);
        pi.updateMailingStreet(street);
        getPropertyCacheManager().flush(street);
        
    }
    
    /**
     * Asks the integrator to fetch a list of MailingStreets given one, both or neither
     * of the Street name part and CityStateZip
     * @param streetName can be null
     * @param csz can be null
     * @return a list, perhaps with results
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public List<MailingStreet> searchForMailingStreet(String streetName, MailingCityStateZip csz) throws IntegrationException, BObStatusException{
        PropertyIntegrator pi = getPropertyIntegrator();
        return pi.searchForStreetsByNameAndZip(streetName, csz);
        
    }
    
    /**
     * Extracts mailing address objects by street
     * @param mstreet
     * @return a list, potentially of 1 or more addresses at the given street
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public List<MailingAddress> getMailingAddressListByStreet(MailingStreet mstreet) throws IntegrationException, BObStatusException{
        PropertyIntegrator pi = getPropertyIntegrator();
        return pi.getMailingAddressListByStreet(mstreet);
        
        
    }
    
    
    
     /**
     * Logic container for retrieving city/state/zip objects
     * @param cszid
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public MailingCityStateZip getMailingCityStateZip(int cszid) throws IntegrationException, BObStatusException{
        PropertyIntegrator pi = getPropertyIntegrator();
        return pi.getMailingCityStateZip(cszid);
    }

    
    /**
     * Logic stop for inserts of MailingAddress objects; This object MUST have a non-null and non-zero ID
     * street and city state zip and non emtpy building no.
     * @param addr to update; i'll inject the last updated by
     * @param ua doing the updating
     * @return the object ID of the freshly inserted record
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public int insertMailingAddress(MailingAddress addr, UserAuthorized ua) 
            throws BObStatusException, IntegrationException{
        if(addr == null || ua == null){
            throw new BObStatusException("Cannot update address with null incoming address or user");
        }
        
        if(addr.getStreet() == null || addr.getStreet().getCityStateZip() == null){
            throw new BObStatusException("cannot insert addres with null street or city/state/zip");
        }
        auditMailingAddress(addr);
        PropertyIntegrator pi = getPropertyIntegrator();
        addr.setCreatedBy(ua);
        addr.setLastUpdatedBy(ua);
        return pi.insertMailingAddress(addr);
    }
    
    /**
     * Business logic for switches and fields on a mailing address enforcing:
     * Standard building no addresses can have an attention and unit, but not PO
     * PO cannot have bldg no or unit, but could have attention
     * ATTN can always be on
     * UNIT requires building no but cannot have PO box
     * @param addr 
     */
    private void auditMailingAddress(MailingAddress addr) throws BObStatusException{
        if(addr == null){
            throw new BObStatusException("Cannot audit null address");
        }
        if(addr.isUseBuildingNo() && isStringEmptyIsh(addr.getBuildingNo())){
            throw new BObStatusException("If we're using a building number we must have a building number that isn't empty ish");
        }
        if(addr.isUseBuildingNo() && addr.isUsePOBox()){
            throw new BObStatusException("Failed address field audit: Use of building no. and use PO Box not allowed; Chooose one or the other");
        }
        
        if(addr.isUseSecondaryUnitIdentifier() && !addr.isUseBuildingNo()){
            throw new BObStatusException("Failed address field audit: Use of unit no. requires a building no.");
        }

        if(addr.isUsePOBox()){
            if(addr.isUseSecondaryUnitIdentifier()){
                throw new BObStatusException("Failed address field audit: Use of PO Box cannot also use a secondary unit identifier");
            }
        }
        
        
    }
    
    /**
     * Logic stop for updates to MailingAddress objects
     * @param addr to update; i'll inject the last updated by
     * @param ua doing the updating
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void updateMailingAddress(MailingAddress addr, UserAuthorized ua) 
            throws BObStatusException, IntegrationException{
        if(addr == null || ua == null){
            throw new BObStatusException("Cannot update address with null incoming address or user");
        }
        auditMailingAddress(addr);
        PropertyIntegrator pi = getPropertyIntegrator();
        addr.setLastUpdatedBy(ua);
        pi.updateMailingAddress(addr);
        getPropertyCacheManager().flush(addr);
    }
    
    /**
     * Deactivates a record in MailingAddress
     * @param addr
     * @param ua
     * @throws BObStatusException
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.EventException 
     */
    public void deactivateMailingAddress(MailingAddress addr, UserAuthorized ua) 
            throws BObStatusException, IntegrationException, AuthorizationException, EventException{
        if(addr == null || ua == null){
            throw new BObStatusException("Cannot update address with null incoming address or user");
        }
        if(!permissionCheckpointDeacMailingAddress(addr, ua)){
            throw new AuthorizationException("User Lacks permission to deac the given address; system admin required when links exist");
        }
        PropertyIntegrator pi = getPropertyIntegrator();
        // deactivate links to this mailing address first
        List<MailingAddressLinkParentObjectInfoHeavy> madLinks = getMailingAddressLinkList(addr, ua);
        if(madLinks != null && !madLinks.isEmpty()){
            for(MailingAddressLinkParentObjectInfoHeavy madlpoih: madLinks){
                System.out.println("Deactivating mad LINk ID: " + madlpoih.getLinkID());
                deactivateLinkToMailingAddress(madlpoih, ua);
            }
        }
        
        addr.setDeactivatedBy(ua);
        addr.setLastUpdatedBy(ua);
        addr.setDeactivatedTS(LocalDateTime.now());
        pi.updateMailingAddress(addr);
        getPropertyCacheManager().flush(addr);
    }
    
     /**
     * Extracts the house number and street name from the address field
     * and injects into separate members on Property
     * 
     * @param prop
     * @return 
     */
    private Property parseAddress(Property prop){
        if(prop.getAddress() != null && prop.getAddress().getAddressPretty1Line() != null){
            

            Pattern patNum = Pattern.compile("(?<num>\\d+[a-zA-Z]*)\\W+(?<street>\\w.*)");
            Matcher matNum = patNum.matcher(prop.getAddress().getAddressPretty1Line());

            while (matNum.find()){
                // Don't need this for humanization since we're already separating out num and street
//                prop.setAddressNum(matNum.group("num"));
//                prop.setAddressStreet(matNum.group("street"));
            }

        }
        return prop;
    }
        
    /**
     *  ***************************************************
     *  ***************************************************
     *  ************* MERGING FACILITY ********************
     *  ***************************************************
     *  ***************************************************
     */
    
    /**
     * Factory for street merge requests
     * 
     * @param ua
     * @return 
     */
     public BobMergeRequest<MailingStreet> getMailingStreetMergeRequestSkeleton(UserAuthorized ua){
         BobMergeRequest<MailingStreet> streetMergeReq = new BobMergeRequest<>();
         streetMergeReq.setRequestingUser(ua);
         
         return streetMergeReq;
     }
    
     /**
      * Checks for legitimacy of the merge request and stamps the reviewTS always and maybe flips mergeAllowed to true
      * @param req
      * @return 
      */
     public BobMergeRequest<MailingStreet> reviewMailingStreetMergeRequest(BobMergeRequest<MailingStreet> req){
         
         return req;
         
     }
     
     /**
      * Undertakes the Street merge operation and configures the request to contain the 
      * sentinel street that received all the other street links
      * @param req
      * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.EventException 
      */
     public BobMergeRequest<MailingStreet> mergeStreets(BobMergeRequest<MailingStreet> req) throws BObStatusException, IntegrationException, AuthorizationException, EventException{
         
         SystemCoordinator sc = getSystemCoordinator();
         PropertyIntegrator pi = getPropertyIntegrator();
         
         if(req == null){
             throw new BObStatusException("Cannot merge a null request");
         }
         if(req.getMergeTarget() == null){
             throw new BObStatusException("Cannot merge streets without target/sentinel street into which the street shall be merged");
         }
         if(req.getRequestingUser() == null){
             throw new BObStatusException("Cannot merge streets null requesting user");
         }
         if(req.getBobsToMerge() == null || req.getBobsToMerge().isEmpty()){
             throw new BObStatusException("Cannot merge streets without streets to merge");
         }
         
         req.appendToMergeLog("BEGIN STREET MERGE LOG", true);
         req.appendToMergeLog("Target street name: ", false);
         req.appendToMergeLog(req.getMergeTarget().getName(), false);
         req.appendToMergeLog("; ID: ", false);
         req.appendToMergeLog(req.getMergeTarget().getStreetID(), true);
         
         // assemble our merge target's address list so we can check for dupes
         List<MailingAddress> targetMadList = getMailingAddressListByStreet(req.getMergeTarget());
         
         // treat each street independently
         for(MailingStreet streetToMerge: req.getBobsToMerge()){
             if(streetToMerge.equals(req.getMergeTarget())){
                 req.appendToMergeLog("Shall not merge a street into itself; skipping", true);
                 continue;
             }
            req.appendToMergeLog("Merging in street ID: ", false);
            req.appendToMergeLog(streetToMerge.getStreetID(), false);
            req.appendToMergeLog(" | name: ", false);
            req.appendToMergeLog(streetToMerge.getName(), true);
            // find which addresses we have to move over
            List<MailingAddress> madsInStreetToMerge = getMailingAddressListByStreet(streetToMerge);
            req.appendToMergeLog("Number of addresses to collapse: " + madsInStreetToMerge.size(), true);
            
            //  look through this list of addresses. If a similar one exists on our
            // target street, do a MAD-MAD merge here, otherwise just re-link the 
            // MAD record to our sentinel target
            if(!madsInStreetToMerge.isEmpty()){
                // look at each address in this street to merge and decide if it will be unique in our target street
                for(MailingAddress mad: madsInStreetToMerge){
                    req.appendToMergeLog("Examining Address ID: " + mad.getAddressID() + " | bldg: " + mad.getBuildingNo(), true);
                    MailingAddress madMatchInTarget = searchForMatchingAddresses(targetMadList, mad);
                    // regular merge if no matching buildings in target street, meaning rewrite our street pointer
                    // on each address to our target
                    if(madMatchInTarget == null){
                        req.appendToMergeLog("No matching building found in target list so we'll redirect all addresses to our target Street", true);
                        // track our old one and throw it in the notes
                        MailingStreet oldStreetParent = mad.getStreet();
                        MessageBuilderParams mbp = new MessageBuilderParams(mad.getNotes(), "Merged this address into new street", null, "Former street parent street ID: " + oldStreetParent.getStreetID(), req.getRequestingUser(), null);
                        mad.setNotes(sc.appendNoteBlock(mbp));
                        // inject the new parent street
                        mad.setStreet(req.getMergeTarget());
                        // write our new updates using generic update method
                        mad.setLastUpdatedBy(req.getRequestingUser());
                        pi.updateMailingAddressStreetKeyOnly(mad);
                        
                                                
                    } else {
                        // do an address merge into the dupe on target street which has a building with effectively the same value
                        req.appendToMergeLog("Duplicate building number found in target street's address list, so merging those; Match Addr ID: " + madMatchInTarget.getAddressID(), true);
                        BobMergeRequest<MailingAddress> madmergeReq = getMailingAddressMergeRequestSkeleton(req.getRequestingUser());
                        madmergeReq.setMergeTarget(madMatchInTarget);
                        List<MailingAddress> mergeList = new ArrayList<>();
                        mergeList.add(mad);
                        madmergeReq.setBobsToMerge(mergeList);
                        madmergeReq = mergeAddresses(madmergeReq);
                        // tack on the internal address merge to the street merge log
                        req.appendToMergeLog(madmergeReq.getMergeLog(), true);
                       
                    } // close if/else routing for mad street rewire or merge dup streets
                } // end of dealing with each mailing address in the street
            } // end check for addresses to merge
            
            // we've dealt with all the addresses on our current street to merge, so write note and deac this street
            MessageBuilderParams mbp = new MessageBuilderParams(streetToMerge.getNotes(), "Deactivated during merge", null, "Merged into street ID: " + req.getMergeTarget().getStreetID(), req.getRequestingUser(), null);
            streetToMerge.setNotes(sc.appendNoteBlock(mbp));
            deactivateMailingStreet(streetToMerge, req.getRequestingUser());
         } // end iteration over each street to merge into target
         
         req.setMergeCompleteTS(LocalDateTime.now());
         req.appendToMergeLog("STREET MERGE ENDED at ", false);
         req.appendToMergeLog(req.getMergeCompleteTS(), true);
         
         // write our merge log to the target street's notes
         MessageBuilderParams mergeLogMBP = new MessageBuilderParams(req.getMergeTarget().getNotes(), "Merge log", null, req.getMergeLog(), req.getRequestingUser(), null);
         req.getMergeTarget().setNotes(sc.appendNoteBlock(mergeLogMBP));
         sc.writeNotes(req.getMergeTarget(), req.getRequestingUser());
         
         // return a merge request with a fresh merge target
         req.setMergeTarget(getMailingStreet(req.getMergeTarget().getStreetID()));
         // flush streets
         getPropertyCacheManager().flushAllAddressConnected();
         
         return req;
     }
     
     /**
      * Looks for logical matches on stripped lowercase building numbers in a given list
      * @param madCandidates
      * @param mad
      * @return 
      */
     private MailingAddress searchForMatchingAddresses(List<MailingAddress> madCandidates, MailingAddress mad){
         if(mad == null || madCandidates == null || madCandidates.isEmpty()){
             return null;
         }
         
         String buildingToMatch = mad.getBuildingNo().strip().toLowerCase();
         for(MailingAddress madcan: madCandidates){
             if(madcan.getBuildingNo().strip().toLowerCase().equals(buildingToMatch)){
                 return madcan;
             }
         }
         return null;
     }
     
     
    /**
     * Factory for address merge requests
     * 
     * @param ua
     * @return 
     */
     public BobMergeRequest<MailingAddress> getMailingAddressMergeRequestSkeleton(UserAuthorized ua){
         BobMergeRequest<MailingAddress> addrMergeReq = new BobMergeRequest<>();
         addrMergeReq.setRequestingUser(ua);
         
         return addrMergeReq;
     }
    
     /**
      * Checks for legitimacy of the merge request and stamps the reviewTS always and maybe flips mergeAllowed to true
      * @param req
      * @return 
      */
     public BobMergeRequest<MailingAddress> reviewMailingAddressMergeRequest(BobMergeRequest<MailingAddress> req) throws BObStatusException, AuthorizationException{
         if(req == null || req.getRequestingUser() == null){
             throw new BObStatusException("Cannot review a null merge request or one with a null requesting user");
         }
         if(!permissionMergeStreetOrAddress(req.getRequestingUser())){
             throw new AuthorizationException("User lacks sufficient authority to merge streets or addresses");
         }
         
         req.setMergeReviewTS(LocalDateTime.now());
         List<MailingAddress> finList = new ArrayList();
         if(req.getBobsToMerge() != null){
            for(MailingAddress mad: req.getBobsToMerge()){
                if(!mad.isActive()){
                    req.appendToMergeLog("Cannot merge an inactive address ID:", false);
                    req.appendToMergeLog(String.valueOf(mad.getAddressID()), false);
                    req.appendToMergeLog("; Removing from merge request", true);
                    continue;
                }
                finList.add(mad);
            }
         }
         
         req.setBobsToMerge(finList);
         
         if(req.getBobsToMerge() == null || req.getBobsToMerge().size() < 2){
             req.appendToMergeLog("Merge rquest does not contain a list of objects to merge or list is only 1 Address; two or more are required", true);
             return req;
         }
         
         req.setMergeAllowed(true);
         req.appendToMergeLog("Merge approved; go forth and merge.", true);
         return req;
     }
     
     /**
      * Undertakes the address merge operation
      * @param req which must contain a non null and non-empty bobsToMerge list
      * 
      * @return with the merge target refreshed
      */
     public BobMergeRequest<MailingAddress> mergeAddresses(BobMergeRequest<MailingAddress> req) throws BObStatusException, IntegrationException, AuthorizationException, EventException{
        if(req == null || req.getRequestingUser() == null){
            throw new BObStatusException("Cannot merge a null request or with requesting user");
        }
        
        SystemCoordinator sc = getSystemCoordinator();
        
        if(req.getMergeTarget() != null){
            req.appendToMergeLog("Detected merge target AddressID: ", false);
            req.appendToMergeLog(String.valueOf(req.getMergeTarget().getAddressID()), true);
            
        } else {
            if(req.getBobsToMerge() != null && req.getBobsToMerge().size() > 1){
                req.setMergeTarget(req.getBobsToMerge().get(0));
                req.appendToMergeLog("No sentinel given so first address in list was assigned with ID: ", false);
                req.appendToMergeLog(String.valueOf(req.getMergeTarget().getAddressID()), true);
            } else {
                req.appendToMergeLog("Cannot merge with fewer than two address. Aborting", true);
                return req;
            }
        }
        
        // take out our sentinel if it's in there
        req.getBobsToMerge().remove(req.getMergeTarget());
        
        for(MailingAddress madToMerge: req.getBobsToMerge()){
            req.appendToMergeLog("Merging address id: ", false);
            req.appendToMergeLog(String.valueOf(madToMerge.getAddressID()), false);
            // get all the links we need to move over
            List<MailingAddressLinkParentObjectInfoHeavy> linksToMergee = getMailingAddressLinkList(madToMerge, req.getRequestingUser());
            req.appendToMergeLog("; Transferring link count: ", false);
            req.appendToMergeLog(String.valueOf(linksToMergee.size()), true);
            if(!linksToMergee.isEmpty()){
                for(MailingAddressLinkParentObjectInfoHeavy madlpoih: linksToMergee){
                    relinkMailingAddressLink(madlpoih, req.getMergeTarget(), req.getRequestingUser());
                }
            } // nothing left to move over if there no links
            // setup our deac notes and do the deac
            MessageBuilderParams mbp = new MessageBuilderParams(madToMerge.getNotes(), 
                                                                "Deactivated during merge", 
                                                                null, 
                                                                "Merged into address ID " + req.getMergeTarget().getAddressID(), 
                                                                req.getRequestingUser(), 
                                                                null);
            madToMerge.setNotes(sc.appendNoteBlock(mbp));
            madToMerge.setDeactivatedBy(req.getRequestingUser());
            deactivateMailingAddress(madToMerge, req.getRequestingUser());
        }
        
        // we've made it to the end
        req.setMergeCompleteTS(LocalDateTime.now());
        
        // write our merge log to the notes on our target MAD
        MessageBuilderParams logMBP = new MessageBuilderParams(req.getMergeTarget().getNotes(), "This address was the target of a merge operation with this log: ", null, req.getMergeLog(), req.getRequestingUser(), null);
        req.getMergeTarget().setNotes(sc.appendNoteBlock(logMBP));
        sc.writeNotes(req.getMergeTarget(), req.getRequestingUser());
        
        // send back a merge request with the fields nicely updated
        req.setMergeTarget(getMailingAddress(req.getMergeTarget().getAddressID()));
        getPropertyCacheManager().flushAllAddressConnected();
        
        return req;
         
     }
        
    /**
     *  ***************************************************
     *  ***************************************************
     *  ************* PROPERTY GENERAL ********************
     *  ***************************************************
     *  ***************************************************
     */
    
    /**
     * Pass through for getting a property count by municode
     * @param muniCode
     * @return
     * @throws IntegrationException 
     */
    public int computeTotalProperties(int muniCode) throws IntegrationException{
        PropertyIntegrator pi = getPropertyIntegrator();
        return pi.getPropertyCount(muniCode);
    }
    
    
    /**
     * Logic container for initializing members on the Property subclass
     * PropertyWithLists; Unlike other assembleXXX methods, I will 
     * get a fresh copy of this property from the DB before injeting into the data heavy
     *  
     *
     * @param prop
     * @param ua
     * @return the data heavy subclass
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws com.tcvcog.tcvce.domain.SearchException
     */
    public PropertyDataHeavy assemblePropertyDataHeavy(Property prop, UserAuthorized ua) throws IntegrationException, BObStatusException, SearchException, BlobException {

        CaseCoordinator cc = getCaseCoordinator();
        EventCoordinator ec = getEventCoordinator();
        SearchCoordinator sc = getSearchCoordinator();
        PropertyDataHeavy pdh = null;
        BlobCoordinator bc = getBlobCoordinator();
        PersonCoordinator pc = getPersonCoordinator();
        
        if(prop != null && ua != null){
            // if we've been given a skeleton, just inject it into data heavy subclass
            if(prop.getParcelKey() == 0){
                pdh = new PropertyDataHeavy(prop);
            } else {

               try {
                    pdh = new PropertyDataHeavy(getProperty(prop.getParcelKey()));

                   // CECase list
                   
                   pdh.setCeCaseList(cc.cecase_assembleCECaseDataHeavyList(getCECaseListByProperty(prop, ua), ua));
                   
                   // action requests
                   QueryCEAR qcear = sc.initQuery(QueryCEAREnum.PROPERTY, ua.getKeyCard());
                   qcear.getPrimaryParams().setProperty_val(prop);
                   pdh.setCearList(sc.runQuery(qcear).getResults());

                   // UnitDataHeavy list
                   // remember that units data heavy contain all our occ periods, inspections, and PropertyUnitChangeOrders
                   if (pdh.getUnitList() != null && !pdh.getUnitList().isEmpty()) {
                       pdh.setUnitWithListsList(getPropertyUnitDataHeavyList(pdh.getUnitList(), ua));
                   }

                   // Person list
                   pdh.sethumanLinkList(pc.getHumanLinkList(pdh));
                   
                   // Broadview photo
                    if(pdh.getBroadviewPhotoID() == 0){
                        pdh.setBroadviewPhoto(bc.getDefaultBroadviewPhoto());
                    } else {
                        pdh.setBroadviewPhoto(bc.getBlobLight(pdh.getBroadviewPhotoID()));
                    }
                   
                   pdh.setBlobList(bc.getBlobLightList(pdh));
                   // external data\
                   // PARCEL INFO NOW LIVES on the parcel itself and uses the parcelinfo table
                   
//                   pdh.setExtDataList(fetchExternalDataRecords(pi.getPropertyExternalDataRecordIDs(pdh.getParcelKey())));
                    
                    // events
                    pdh.setEventList(ec.getEventList(pdh));

                    pdh = configurePropertyDataHeavy(pdh, ua);
               } catch (EventException | AuthorizationException | BObStatusException | BlobException | IntegrationException | SearchException ex) {
                   System.out.println(ex);
                   System.out.println();
               }
            }
        } 
        return pdh;
    }
    
    /**
     * Logic block for setting members nicely on the data heavy variant of properties.
     * @param pdh
     * @param ua
     * @return 
     */
    private PropertyDataHeavy configurePropertyDataHeavy(PropertyDataHeavy pdh, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(pdh == null || ua == null){
            throw new BObStatusException("cannot configure null property DH or with  null user");
        }
        SystemCoordinator sc = getSystemCoordinator();
        EventCoordinator ec = getEventCoordinator();
        pdh.setOwnerLink(extractHumanLinkByRole(pdh, sc.getLinkedObjectRole(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("property_owner_lorid")))));
        pdh.setManagerLink(extractHumanLinkByRole(pdh, sc.getLinkedObjectRole(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("property_manager_lorid")))));
        pdh = configurePropertyStatusEnums(pdh);
        pdh.setEventListCumulative(ec.filterEventListForViewFloor(pdh.extractCumulativeEventList(), ua.getRole()));
        ec.configurePropertyAlerts(pdh);
        
        
        return pdh;
    }
    
    /**
     * Sorts through to find owner of property
     * @param pdh
     * @throws BObStatusException 
     */
    private HumanLink extractHumanLinkByRole(PropertyDataHeavy pdh, LinkedObjectRole lor) throws BObStatusException, IntegrationException{
        if(pdh == null || lor == null){
            return null;
        }
        if(pdh.gethumanLinkList() != null && !pdh.gethumanLinkList().isEmpty()){
            for(HumanLink link: pdh.gethumanLinkList()){
                if(link.getLinkedObjectRole().equals(lor)){
                    return link;
                }
            }
        }
        return null;
    }
    
    /**
     * Examines the lists of occ periods and cecases and based on their statuses, 
     * sets the master status markers on the property.
     * 
     * This method is fill with gobs of ugly, awful, terrible code that casts
     * doubt on Ellen's general state of mental composure and ability to solve 
     * software problems.
     * 
     * @param pdh
     * @return 
     */
    private PropertyDataHeavy configurePropertyStatusEnums(PropertyDataHeavy pdh) throws BObStatusException{
        if(pdh == null){
            throw new BObStatusException("Cannot configure PDH status with null property");
        }
        
        // start with occ periods
        List<OccPeriodDataHeavy> opdhList = pdh.getCompletePeriodList();
        List<OccPeriodStatusEnum> periodStatusList = new ArrayList<>();
        if(opdhList != null && !opdhList.isEmpty()){
            for(OccPeriodDataHeavy opdh: opdhList){
                if(opdh.getStatus() != null){
                    periodStatusList.add(opdh.getStatus().getStatusEnum());
                }
            }
            if(!periodStatusList.isEmpty()){
                Collections.sort(periodStatusList, OccPeriodStatusEnum.severityComparator);
                Collections.reverse(periodStatusList);
                pdh.setMaxStatusOccupancyPeriods(periodStatusList.get(0));
            }
        } else {
            pdh.setMaxStatusOccupancyPeriods(null);
        }
         
        // TODO: refactor this to use inheritance and a single logic block, not this
        // repeated code rubbish
        List<CECaseDataHeavy> cseList = pdh.getCeCaseList();
        List<PriorityEnum> caseStatusList = new ArrayList<>();
        if(cseList != null && !cseList.isEmpty()){
            for(CECaseDataHeavy cse: cseList){
                if(cse.getPriority()!= null ){
                    caseStatusList.add(cse.getPriority());
                }
            }
            if(!caseStatusList.isEmpty()){
                Collections.sort(caseStatusList, PriorityEnum.severityComparator);
                Collections.reverse(caseStatusList);
                pdh.setMaxCECasePriority(caseStatusList.get(0));
            }
        } else {
            pdh.setMaxCECasePriority(null);
        }
        
        
        return pdh;
    }
    
    /**
     * Queries the case table based on property
     * @param prop
     * @param ua
     * @return
     * @throws SearchException 
     */
    public List<CECase> getCECaseListByProperty(Property prop, UserAuthorized ua) throws SearchException{
        SearchCoordinator sc = getSearchCoordinator();
        QueryCECase qcse = sc.initQuery(QueryCECaseEnum.PROPERTY, ua.getKeyCard());
        qcse.getPrimaryParams().setProperty_val(prop);
        return sc.runQuery(qcse, ua).getResults();
        
    }
    
    /**
     * Utility for getting propert list from a list of IDs
     * @param idl
     * @return
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public List<Property> getPropertyListFromIDList(List<Integer> idl) throws IntegrationException, BObStatusException, AuthorizationException{
        List<Property> plist = new ArrayList<>();
        if(idl != null && !idl.isEmpty()){
            for(Integer i: idl){
                plist.add(getProperty(i));
            }
        }
        return plist;
        
        
    }
    
    

   
    /**
     * Logic pass through for acquiring a PropertyUnitWithProp for OccPeriods
     * and such that need a property address but only have a unit ID on them
     *
     * @param unitid
     * @return
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException if property doesn't have a parcel info that generation process can throw this ex
     */
    public PropertyUnitWithProp getPropertyUnitWithProp(int unitid) throws IntegrationException, BObStatusException, AuthorizationException {
        PropertyIntegrator pi = getPropertyIntegrator();
        PropertyUnitWithProp puwp = new PropertyUnitWithProp(getPropertyUnit(unitid));
        puwp.setProperty(getProperty(pi.getPropertyIDByUnitID(unitid)));
        
        return puwp;

    }

    /**
     *
     * @param propUnitList
     * @param ua
     * @return
     * @throws IntegrationException
     * @throws EventException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public List<PropertyUnitDataHeavy> getPropertyUnitDataHeavyList(List<PropertyUnit> propUnitList, UserAuthorized ua) throws IntegrationException, EventException, EventException, AuthorizationException, BObStatusException {
        List<PropertyUnitDataHeavy> puwll = new ArrayList<>();
        Iterator<PropertyUnit> iter = propUnitList.iterator();
        while (iter.hasNext()) {
            PropertyUnit pu = iter.next();
            puwll.add(getPropertyUnitDataHeavy(pu, ua));
            
        }
        return puwll;
    }

    /**
     * Organizes fields on our data heavy version of the property unit.
     * Caller must pass in a current version of the PropertyUnit base class
     * 
     * @param propUnit; must be the fresh copy from the DB
     * @param ua
     * @return the assembled object
     * @throws IntegrationException
     * @throws EventException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public PropertyUnitDataHeavy getPropertyUnitDataHeavy(PropertyUnit propUnit, UserAuthorized ua) 
            throws IntegrationException, EventException, EventException, AuthorizationException, BObStatusException {
        PropertyIntegrator pi = getPropertyIntegrator();
        if(propUnit == null){
            throw new BObStatusException("Cannot get property unit with lists given null prop unit or credential");
        }
        try {
            PropertyUnitDataHeavy pudh = new PropertyUnitDataHeavy(propUnit);
            return configurePropertyUnitDataHeavy(pudh, ua);
        } catch (ViolationException | AuthorizationException | BObStatusException | BlobException ex) {
            System.out.println(ex);
        }
        return new PropertyUnitDataHeavy(propUnit); //just in case something goes horribly wrong
    }

    /**
     * Accepts a List of PropertyUnit objects, validates the input to make sure
     * it is acceptable, sanitizes it, then tosses it back.
     *
     * @param input
     * @return
     * @throws BObStatusException if the input is not acceptable.
     */
    public List<PropertyUnit> sanitizePropertyUnitList(List<PropertyUnit> input) throws BObStatusException {

        int duplicateNums; //The above boolean is a flag to see if there is more than 1 of  Unit Number. The int to the left stores how many of a given number the loop below finds.

        if (input.isEmpty()) {
            throw new BObStatusException("Please add at least one unit.");
        }

        //use a numeric for loop instead of iterating through the objects so that we can store 
        //the sanitized units
        for (int index = 0; index < input.size(); index++) {
            duplicateNums = 0;

            for (PropertyUnit secondUnit : input) {
                if (input.get(index).getUnitNumber().compareTo(secondUnit.getUnitNumber()) == 0) {
                    duplicateNums++;
                }
            }

            if (duplicateNums > 1) {
                throw new BObStatusException("Some Units have the same Number");

            }

            input.set(index, sanitizePropertyUnit(input.get(index)));
        }

        return input;

    }

    /**
     * Accepts a PropertyUnit object, validates the input to make sure it is
     * acceptable, sanitizes it, then tosses it back.
     *
     * @param input
     * @return
     * @throws BObStatusException if the input is not acceptable.
     */
    public PropertyUnit sanitizePropertyUnit(PropertyUnit input) throws BObStatusException {

        input.setUnitNumber(input.getUnitNumber().replaceAll("(?i)unit", ""));

        if (input.getUnitNumber().compareTo("") == 0) {
            throw new BObStatusException("All units must have a Unit Number");
        }

        if (input.getUnitNumber().compareTo("-1") == 0) {
            if (input.getNotes() == null || input.getNotes().compareTo("robot-generated unit representing the primary habitable dwelling on a property") != 0) {
                throw new BObStatusException("The unit number -1 is used for default property units. Please use another number or \'-[space]1\'.");
            }
            throw new BObStatusException("Please change the robot-generated unit to something more meaningful.");

        }

        return input;

    }

    /**
     * Logic container for Property setting configuration
     *
     * @param p
     * @return
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public Property configureProperty(Property p) throws IntegrationException, BObStatusException {
        PropertyIntegrator pi = getPropertyIntegrator();
        UserCoordinator uc = getUserCoordinator();
        
        if(p != null){
            
            p.setUnitList(getPropertyUnitList(p));
            // Add a primary unit if we don't have at least one unit
            if(p.getUnitList() != null && p.getUnitList().isEmpty() && p.getParcelKey() != 0){
                System.out.println("PropertyCoordinator.configureProperty | no unit found, adding PRIMARY unit on parcel key: " + p.getParcelKey());
                PropertyUnit pu = getPropertyUnitSkeleton(p);
                pu.setUnitNumber(DEFAULTUNITDESIGNATOR);
                insertPropertyUnit(pu, p, uc.user_getUserRobot());
                // refresh our list
                p.setUnitList(getPropertyUnitList(p));
            }
            p.setMailingAddressLinkList(getMailingAddressLinkList(p));
        }
        // Don't need this for humanization
//        parseAddress(p);
        return p;
    }
    
    /**
     * Extracts all units associated with a given property
     * @param p
     * @return a list, perhaps with property units inside
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public List<PropertyUnit> getPropertyUnitList(Property p) throws IntegrationException, BObStatusException{
        PropertyIntegrator pi = getPropertyIntegrator();
        List<PropertyUnit> ul = new ArrayList<>();
        List<Integer> unitidl = pi.getPropertyUnitList(p);
        if(unitidl != null && !unitidl.isEmpty()){
            for(Integer i: unitidl){
                ul.add(getPropertyUnit(i));
            }
        }
        return ul;
    }
    
    /**
     * Assembles an address for pretty printing
     * @param addr for which to generate the address 
     * @param use2Lines if true, a <br /> will be inserted for double line 
     * conventional address printing
     * @return the String for injection into the property
     */
    private String buildPropertyAddressStrings(MailingAddress addr, boolean use2Lines, boolean includeCSZ){
        StringBuilder addrStr = new StringBuilder();
        if(addr != null){
            try {
                auditMailingAddress(addr);
            } catch (BObStatusException ex) {
                return addrStr.append(ex.getMessage()).toString();
            }
            // if the address includes an attention, this method will insert it along with
            // either a ; or a line break depending on the fag used
            appendAttnIfPresent(addr, addrStr, use2Lines);
            // if our incoming address fails the audit, make the address the failure message
            // If we've got a PO box, that's the next line
            if(addr.isUsePOBox()){
                addrStr.append(POBOX);
                addrStr.append(addr.getPoBox());
                // and maybe an attention line
            // NOT a PO Box, so we should have a building number and street   
            } else {
                if(addr.getBuildingNo() != null){
                    addrStr.append(addr.getBuildingNo());
                }
                // we need a street to get City, State, Zip too; If it's a PO box address
                // we need THE sentinel PO Box street which exists for each ZIP code
                if(addr.getStreet() != null){
                    addrStr.append(SPACE);
                    addrStr.append(addr.getStreet().getName());
                    if(addr.isUseSecondaryUnitIdentifier()){
                        addrStr.append(SPACE);
                        addrStr.append(addr.getSecondaryUnitIdentifier());
                    }
                }
            }
                
            // now do last line for all addresses: city state, ZIP Code
            if(addr.getStreet().getCityStateZip() != null && includeCSZ){
                if(use2Lines){
                    addrStr.append(HTML_BR);
                } else {
                    addrStr.append(SPACE);
                }
                addrStr.append(addr.getStreet().getCityStateZip().getCity());
                addrStr.append(COMMA_SPACE);
                addrStr.append(addr.getStreet().getCityStateZip().getState());
                addrStr.append(SPACE);
                addrStr.append(addr.getStreet().getCityStateZip().getZipCode());
            }
        } else {
            addrStr.append("null address");
        }
        return addrStr.toString();
    }
    
    /**
     * Utility for adding a newline and then the attention line with ATTN: prepended
     * @param addr
     * @param sb 
     */
    private void appendAttnIfPresent(MailingAddress addr, StringBuilder sb, boolean use2Lines){
        if(addr != null && addr.isUseAttention() && addr.getAttn() != null){
            sb.append(ATTN);
            sb.append(addr.getAttn());
            if(use2Lines){
                sb.append(HTML_BR);
            } else {
                sb.append(SEMICOLON_SPACE);
            }
        }
    }
    
    public LocalDateTime configureDateTime(Date date) {
        return new java.sql.Timestamp(date.getTime()).toLocalDateTime();
    }

    /**
     * Logic container for checking and setting properties on
     * PropertyUnitDataHeavy objects
     *
     * @param pudh
     * @param ua
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @return
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     * @throws com.tcvcog.tcvce.domain.EventException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.ViolationException
     */
    public PropertyUnitDataHeavy configurePropertyUnitDataHeavy(PropertyUnitDataHeavy pudh, UserAuthorized ua) 
            throws IntegrationException, AuthorizationException, EventException, BObStatusException, ViolationException, BlobException {
        
        OccupancyCoordinator oc = getOccupancyCoordinator();
        PersonCoordinator pc = getPersonCoordinator();
        
        try {
            pudh.setPeriodList(oc.getOccPeriodDataHeavyList(oc.getOccPeriodList(pudh, ua),ua));
            if(pudh.getPeriodList() != null && !pudh.getPeriodList().isEmpty()){
                Collections.sort(pudh.getPeriodList());
                Collections.reverse(pudh.getPeriodList());
            }
        } catch (SearchException ex) {
            throw new BObStatusException("could not search during occ period data heavy assembly");
        }
        pudh.sethumanLinkList(pc.getHumanLinkList(pudh));
        pudh.setMailingAddressLinkList(getMailingAddressLinkList(pudh));
        return pudh;
    }
    
    /**
     * Primary pathway for the creation of new records in the parcel
     * table 
     *
     * @param pcl
     * @param ua
     * @return
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public int addParcel(Parcel pcl, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException {
        if(pcl == null || ua == null || pcl.getParcelInfo() == null){
            throw new BObStatusException("Cannot insert new parcel with null parcel or user");
        }
        PropertyIntegrator pi = getPropertyIntegrator();
        SystemIntegrator si = getSystemIntegrator();
        
        if(!permissionCheckpointEditparcelAndPropertyInfo(ua)){
            throw new AuthorizationException("Operation not allowed! Failed permissions check in addParcel by permissionCheckpointEditparcelAndPropertyInfo");
        }
        
        if(pcl.getParcelInfo().isNonAddressable()){
            pcl.setCountyParcelID(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                        .getString("nonaddressable_parcelid"));
        }
        
        pcl.setCreatedBy(ua);
        pcl.setLastUpdatedBy(ua);
        pcl.setSource(si.getBOBSource(
                Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                        .getString("bobsourcePropertyInternal"))));
        
        // this controller class passes the new property to insert
        // over to the data model to be written into the Database
        return pi.insertParcel(pcl);
        

    }

    /**
     * Logic container for property updates
     *
     * @param pcl
     * @param ua
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void updateParcel(Parcel pcl, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException {
        PropertyIntegrator pi = getPropertyIntegrator();
        
        if(!permissionCheckpointEditparcelAndPropertyInfo(ua)){
            throw new AuthorizationException("Operation not allowed! Failed permissions check in updateParcel by permissionCheckpointEditparcelAndPropertyInfo");
        }
        pcl.setLastUpdatedBy(ua);
        pi.updateParcel(pcl);
        PropertyCacheManager pcm = getPropertyCacheManager();
        pcm.flush(pcl);

    }
    
    /**
     * Builds a note and appends it to the passed in parcel 
     * @param pcl
     * @param noteText
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void updateParcelNotes(Parcel pcl, String noteText, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(pcl == null || noteText == null || ua == null){
            throw new BObStatusException("Cannot append note to null parcel, or with null notes or null user");
        }
        PropertyIntegrator pi = getPropertyIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        
        MessageBuilderParams mpb = new MessageBuilderParams(pcl.getNotes(), "PARCEL NOTE", null, noteText, ua, null);
        pcl.setNotes(sc.appendNoteBlock(mpb));
        pi.updateParcelNotes(pcl);
        System.out.println("PropertyCoordinator.updateParcelNotes | finished appending: total parcel notes are " + pcl.getNotes());
        getPropertyCacheManager().flush(pcl);
        
    }
    
    
    /**
     * Writes note field on parcel to DB: Client is 100% responsible for 
     * managing the note field properly using message builder params, hopefully
     * @param pcl
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void updateParcelNotes(Parcel pcl, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(pcl == null || ua == null){
            throw new BObStatusException("Cannot append note to null parcel, or with null notes or null user");
        }
        PropertyIntegrator pi = getPropertyIntegrator();
        pi.updateParcelNotes(pcl);
        System.out.println("PropertyCoordinator.updateParcelNotes | finished appending: total parcel notes are " + pcl.getNotes());
        getPropertyCacheManager().flush(pcl);
        
        
    }
    
    

    /**
     * Updates a parcel's broadview photo for reporting and profile
     * @param pdh
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void updatePropertyDataHeavyBroadviewPhoto(PropertyDataHeavy pdh, UserAuthorized ua) throws BObStatusException, IntegrationException {
        PropertyIntegrator pi = getPropertyIntegrator();
        
        if(pdh.getBroadviewPhoto() == null){
            throw new BObStatusException("Cannot update a parcel's broadview photo to null");
        }
        if(!pdh.getBroadviewPhoto().getType().isBrowserViewable()){
            throw new BObStatusException("Cannot update the broadview photo to a BLOB that's not browser viewable");
        }
        pdh.setLastUpdatedBy(ua);
        
        pi.updatePropertyDataHeavyBroadviewPhoto(pdh);
        getPropertyCacheManager().flush(pdh);
        
    }
    
   
    
    /**
     * Logic passthrough for insertions into the parcelinfo table
     * @param info
     * @param ua
     * @return the ID of the fresh record
     * @throws BObStatusException
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public int insertParcelInfoRecord(ParcelInfo info, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        PropertyIntegrator pi =getPropertyIntegrator();
        if(info == null || ua == null){
            throw new BObStatusException("cannot insert a parcel info record with null info or UA");
        }
        
        if(!permissionCheckpointEditparcelAndPropertyInfo(ua)){
            throw new AuthorizationException("Operation not allowed! Failed permissions check in insertParcelInfoRecord by permissionCheckpointEditparcelAndPropertyInfo");
        }
        
        info.setCreatedBy(ua);
        info.setLastUpdatedBy(ua);
        
        return pi.insertParcelInfo(info);
    }
    
    /**
     * Logic pass through for updates to the parcelinfo table
     * @param info
     * @param ua doing the updating
     * @throws BObStatusException
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void updateParcelInfoRecord(ParcelInfo info, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        PropertyIntegrator pi = getPropertyIntegrator();
        if(info == null || ua == null || ua.getGoverningMuniProfile() == null){
            throw new BObStatusException("cannot insert a parcel info record with null info or UA");
        }
        if(info.getParcelParcelKey() == 0){
            throw new BObStatusException("Found parcel KEY of 0 on parcelinfo ID " + info.getParcelInfoID());
        }
         
        if(!permissionCheckpointEditparcelAndPropertyInfo(ua)){
            throw new AuthorizationException("Authorization exception: Muni profile " + ua.getGoverningMuniProfile().getProfileID() 
                    + " requires manager rank to update parcel info and user " + ua.getUserHuman() + " is not rank: manager");
        }
        info.setLastUpdatedBy(ua);
        pi.updateParcelInfo(info);
        PropertyCacheManager pcm = getPropertyCacheManager();
        pcm.flushAllAddressConnected();
        
        
    }
    
    /**
     * Deactivates a record in the parcelinfo table
     * @param info
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void deactivateParcelInfo(ParcelInfo info, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        
        PropertyIntegrator pi = getPropertyIntegrator();
        if(info == null || ua == null){
            throw new BObStatusException("cannot insert a parcel info record with null info or UA");
        }
        
        if(!permissionCheckpointEditparcelAndPropertyInfo(ua)){
            throw new AuthorizationException("Operation not allowed! Failed permissions check in insertParcelInfoRecord by permissionCheckpointEditparcelAndPropertyInfo"); 
        }
        info.setDeactivatedBy(ua);
        info.setDeactivatedTS(LocalDateTime.now());
        info.setLastUpdatedBy(ua);
        pi.updateParcelInfo(info);
        getPropertyCacheManager().flush(info);
    }
    
    
    /**
     * Checks for open permit files and cases and only if they are not active
     * is deac allowed
     * @param pdh
     * @return the same property but with its deactivationAuditLog written and 
     * perhaps allowDeactivation set to true
     */
    public PropertyDataHeavy deactivateParcelAudit(PropertyDataHeavy pdh) throws BObStatusException{
        if(pdh == null){
            throw new BObStatusException("Cannot check deac with null input property");
        }
        boolean allowDeac = true;
        StringBuilder sb = new StringBuilder();
        // cases cannot be open
        if(pdh.getCeCaseList() != null && !pdh.getCearList().isEmpty()){
            allowDeac = false;
            for(CECase cse: pdh.getCeCaseList()){
                sb.append("Cannot deac with open case ID: ");
                sb.append(cse.getCaseID());
                sb.append(Constants.FMT_HTML_BREAK);
            }
        }
        // permit files cannot be open
        if(pdh.getUnitWithListsList() != null && !pdh.getUnitWithListsList().isEmpty()){
            for(PropertyUnitDataHeavy pudh: pdh.getUnitWithListsList()){
                if(pudh.getPeriodList() != null && !pudh.getPeriodList().isEmpty()){
                    for(OccPeriod op: pudh.getPeriodList()){
                        sb.append("Cannot deac property with active permit file ID: ");
                        sb.append(op.getPeriodID());
                        sb.append(" In unit number: ");
                        sb.append(pudh.getUnitNumber());
                        sb.append(Constants.FMT_HTML_BREAK);
                        allowDeac = false;
                    }
                }
            }
        }
        
        if(allowDeac){
            sb.append("This parce has no active cases or permit files and IS READY to be deactivated!");
        }
        
        pdh.setAllowDeactivation(allowDeac);
        pdh.setDeactivationAuditLog(sb.toString());
        return pdh;
    }
    
    /**
     * Deactivates a parcel
     * @param pcl to deac
     * @param ua requesting deac, must be sys admin
     * @throws com.tcvcog.tcvce.domain.BObStatusException for null inputs
     * @throws com.tcvcog.tcvce.domain.AuthorizationException user without sys admin permissions
     */
    public void deactivateParcel(Parcel pcl, UserAuthorized ua) throws BObStatusException, AuthorizationException, IntegrationException{
        if(pcl == null || ua == null){
            throw new BObStatusException("Cannot deac parcel with null parcel or user");
        }
        
        if(!permissionCheckpointEditparcelAndPropertyInfo(ua)){
            throw new AuthorizationException("Operation not allowed! Failed permissions check in insertParcelInfoRecord by permissionCheckpointEditparcelAndPropertyInfo");
        }
        PropertyIntegrator pi = getPropertyIntegrator();
        
        pcl.setDeactivatedBy(ua);
        
        // Deac sub objects
        
        // db uses its now() for deac timestamp
        pi.deactivateParcel(pcl);
        getPropertyCacheManager().flush(pcl);
        
    }
    
    public boolean checkAllDates(Property prop) {
        boolean unfit, vacant, abandoned;

        // TODO: humanization: these values now live on the propertydata records, not direclty on
        // a parcel
//        unfit = checkStartDTisBeforeEndDT(prop.getUnfitDateStart(), prop.getUnfitDateStop());
//        vacant = checkStartDTisBeforeEndDT(prop.getVacantDateStart(), prop.getVacantDateStop());
//        abandoned = checkStartDTisBeforeEndDT(prop.getAbandonedDateStart(), prop.getAbandonedDateStop());
//        if (unfit && vacant && abandoned) {
//            return true;
//        } else {
//            return false;
//        }

        // short circuit logic
        return true;
    }

    public boolean checkStartDTisBeforeEndDT(LocalDateTime start, LocalDateTime end) {

        if (start == null) {
            return end == null; //If start is null, then end must also be null. Otherwise we have a end without a beginning!

        } else if (end != null) { //If start is not null and end is not null, then we can run the following code block
            if (end.isBefore(start) || end.equals(start)) {
                return false;
            } else {
                return true;
            }
        }

        return true; //If start is not null, but end is null, no check is necessary. Also, checking would cause a null pointer error.
    }

    /**
     * Adapter method for folks who need a PropertyDataHeavy but who only have
     * an int propertyID; first it gets a simple property from the ID and then
     * beefs it up to a PropertyDataHeavy
     *
     * @param propID
     * @param ua
     * @return
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws AuthorizationException
     * @throws EventException
     * @throws com.tcvcog.tcvce.domain.SearchException
     */
    public PropertyDataHeavy getPropertyDataHeavy(int propID, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException, EventException, SearchException, BlobException {
        System.out.println("PropertyCoordinator.getPropertyDataHeavy | " + propID);
        return assemblePropertyDataHeavy(getProperty(propID), ua);
    }

    /**
     * Adapter method for folks who need a PropertyDataHeavy but who only have
     * an int propertyID; first it gets a simple property from the ID and then
     * beefs it up to a PropertyDataHeavy
     *
     * @param propUnitID
     * @param ua
     * @return
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws AuthorizationException
     * @throws EventException
     * @throws com.tcvcog.tcvce.domain.SearchException
     */
    public PropertyDataHeavy getPropertyDataHeavyByUnit(int propUnitID, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException, EventException, SearchException, BlobException {
        PropertyIntegrator pi = getPropertyIntegrator();
        return assemblePropertyDataHeavy(getPropertyUnitWithProp(propUnitID).getProperty(), ua);
    }

    /**
     * Logic intervention method for retrievals of simple properties from the DB
     * Updated to reflect parcelization in which there's no "propertyID" but 
     * rather a parcel key that's internal to codeNforce and a countyParcelID
     * 
     * NOTE: we only have support for one parcel info record--we take the most recent record
     * in the DB ordered by last updated timestamp and inject that one into the parcel
     * 
     * @param parcelID
     * @return always a parcel object, perhaps an empty one to avoid null pointers, meaning ID = 0
     * @throws IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
     public Property getProperty(int parcelID) throws IntegrationException, BObStatusException, AuthorizationException {
        if(parcelID == 0){
            return null;
        }
        PropertyIntegrator pi = getPropertyIntegrator();
        Parcel par = pi.getParcel(parcelID);
        if(par == null){
            System.out.println("PropertyCoordinator.getProperty | NULL parcel from integrator | incoming parcelID: " + parcelID);
            par = new Parcel();
        }
      
        par.setParcelInfoList(configureParcelInfoAndAssignGoverningRecord(par));

        // make us a property, which has addresses
        Property p = new Property(par);
        
        return configureProperty(p);

    }
     
     /**
      * Gets all the parcel infos from the DB and sets the most recent as the governing profile.
      * As of March 2024, this method will also create a new parcel record if the given parcel does not have one
      * @param pcl
      * @return 
      */
    private List<ParcelInfo> configureParcelInfoAndAssignGoverningRecord(Parcel pcl) throws IntegrationException, BObStatusException, AuthorizationException{
         UserCoordinator uc = getUserCoordinator();
        PropertyIntegrator pi = getPropertyIntegrator();
        List<Integer> infoIDL = pi.getParcelInfoByParcel(pcl);
        List<ParcelInfo> infoList = new ArrayList<>();
        if(infoIDL != null && !infoIDL.isEmpty()){
            for(Integer id: infoIDL){
                infoList.add(pi.getParcelInfo(id));
            }
        }
        // all parcels need a parcel info
        if(infoList.isEmpty()){
            // but this needs a DB key, not just a java skeleton
            int freshParcelInfo = insertParcelInfoRecord(new ParcelInfo(pcl.getParcelKey()), uc.auth_getRobotUserAuthPeriod(RoleType.MuniStaff));
            System.out.println("Found no parcel info on parcel ID " + pcl.getParcelKey() + " | fresh ParcelInfo ID: " + freshParcelInfo);
            infoList.add(getParcelInfo(freshParcelInfo));
        }
        Collections.sort(infoList);
        Collections.reverse(infoList);
        // set the most recent as our governing info
        pcl.setParcelInfo(infoList.get(0));
         
        return infoList;
     }
    
    /**
     * Logic pass through for parcel info objects
     * @param infoID
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public ParcelInfo getParcelInfo(int infoID) throws IntegrationException, BObStatusException{
        PropertyIntegrator pi = getPropertyIntegrator();
        if(infoID != 0){
            return pi.getParcelInfo(infoID);
        }
        return null;
        
    }
             
       

    /**
     * Logic intermediary for retrieval of a PropertyUnit by id
     *
     * @param unitID
     * @return
     * @throws IntegrationException
     */
    public PropertyUnit getPropertyUnit(int unitID) throws IntegrationException, BObStatusException {
        PropertyIntegrator pi = getPropertyIntegrator();
        return pi.getPropertyUnit(unitID);

    }

    /**
     * Logic intermediary for extracting a complete list of PropertyUseType
     * objects from the DB
     *
     * @return
     */
    public List<PropertyUseType> getPropertyUseTypeList() {
        PropertyIntegrator pi = getPropertyIntegrator();
        List<PropertyUseType> putList = null;
        try {
            putList = pi.getPropertyUseTypeList();
        } catch (IntegrationException ex) {
            System.out.println(ex);
        }

        return putList;
    }

    public List<PropertyUseType> getPutList() throws IntegrationException {
        return getPutList(false);
    }

    public List<PropertyUseType> getPutList(boolean includeDeactivated) throws IntegrationException {
        SystemIntegrator si = getSystemIntegrator();
        return si.getPutList(includeDeactivated);
    }
    
    /**
     * To update Property Use Type
     *
     * @param useType 
     * @param umap
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws AuthorizationException
     */
    public void updatePropertyUseType(PropertyUseType useType, UserMuniAuthPeriod umap) throws IntegrationException, BObStatusException, AuthorizationException {
        if (Objects.isNull(useType) || Objects.isNull(umap)) {
            throw new BObStatusException("Cannot update Property Use Type with null determination or UMAP");
        }
        if (umap.getRole() != RoleType.SysAdmin) {
            throw new AuthorizationException("UMAP must be rank: Sys Admin to update Property Use Type");
        }
        useType.setLastUpdatedby_UMAP(umap);

        PropertyIntegrator pi = getPropertyIntegrator();
        pi.updatePropertyUseType(useType);
        getPropertyCacheManager().flush(useType);
    }
    
    /**
     * Factory for Property Use Type objects
     * @param umap
     * @return new useType with id = 0
     */
    public PropertyUseType getPropertyUseTypeSkeleton(UserMuniAuthPeriod umap) {
        PropertyUseType useType = new PropertyUseType();
        useType.setCreatedby_UMAP(umap);
        useType.setLastUpdatedby_UMAP(umap);
        return useType;
    }
    
    /**
     * Insertion point for Property Use Type
     *
     * @param useType  
     * @param umap
     * @return OccInspectionDetermination
     * @throws IntegrationException
     * @throws AuthorizationException
     * @throws BObStatusException
     */
    public PropertyUseType insertPropertyUseType(PropertyUseType useType, UserMuniAuthPeriod umap) throws IntegrationException, BObStatusException, AuthorizationException {
        if (Objects.isNull(useType) || Objects.isNull(umap)) {
            throw new BObStatusException("Cannot insert useType with null useType or UMAP");
        }
        if (umap.getRole() != RoleType.SysAdmin) {
            throw new AuthorizationException("UMAP must be rank: Sys Admin to insert useType");
        }
        useType.setCreatedby_UMAP(umap);
        useType.setLastUpdatedby_UMAP(umap);

        PropertyIntegrator pi = getPropertyIntegrator();
        useType.setTypeID(pi.insertPropertyUseType(useType));
        return useType;
    }
    
     /**
     * To deactivate Property Use Type
     * @param useType 
     * @param umap
     * @throws BObStatusException
     * @throws AuthorizationException
     * @throws IntegrationException
     */
     public void deactivatePropertyUseType(PropertyUseType useType, UserMuniAuthPeriod umap) throws BObStatusException, AuthorizationException, IntegrationException {
         if (Objects.isNull(useType) || Objects.isNull(umap)) {
             throw new BObStatusException("Cannot deactivate property use type with null use type or UMAP");
         }
         if (umap.getRole() != RoleType.SysAdmin) {
             throw new AuthorizationException("UMAP must be rank: Sys Admin to deactivate property use type");
         }
         useType.setDeactivatedBy_UMAP(umap);
         SystemIntegrator si = getSystemIntegrator();
         si.deactivateUMAPTrackedEntity(useType);
         getPropertyCacheManager().flush(useType);
         
     }

     /**
      * Adaptor method for extracting a property by a unit ID
      * @param unitID
      * @return
      * @throws IntegrationException
      * @throws BObStatusException 
      */
    public Property getPropertyByPropUnitID(int unitID) throws IntegrationException, BObStatusException, AuthorizationException {
        PropertyIntegrator pi = getPropertyIntegrator();
        
        return getProperty(pi.getPropertyIDByUnitID(unitID));
    }

    /**
     * Implements a cascade of logic to determine a best suited startup property
     * for a given session. When a user has no Property history, it extracts the
     * office property of the municipality into which a user is switching
     *
     * ecd DEC-19
     *
     * @param ua
     * @return
     */
    public PropertyDataHeavy selectDefaultProperty(UserAuthorized ua) {

        PropertyIntegrator pi = getPropertyIntegrator();
        MunicipalityCoordinator mc = getMuniCoordinator();
        PropertyCoordinator pc = getPropertyCoordinator();

        if (ua != null) {
            try {
                MunicipalityDataHeavy mdh = mc.assembleMuniDataHeavy(ua.getKeyCard().getGoverningAuthPeriod().getMuni(), ua);
                if (mdh.getMuniOfficePropertyId() != 0) {
                    return pc.assemblePropertyDataHeavy(pc.getProperty(mdh.getMuniOfficePropertyId()), ua);
                }
            } catch (IntegrationException | AuthorizationException | BObStatusException | EventException | SearchException | BlobException ex) {
                System.out.println(ex);
            }
        }
        return null;
    }
    
    
   
    /**
     *
     * 
     * 
     * @param cred
     * @return
     */
    public List<Property> assemblePropertyHistoryList(Credential cred) throws BObStatusException {
        PropertyIntegrator pi = getPropertyIntegrator();
        List<Property> propList = new ArrayList<>();
        List<Integer> propIDList = new ArrayList<>();

        if (cred != null) {
            try {
                propIDList.addAll(pi.getPropertyHistoryList(cred));
                while (!propIDList.isEmpty() && propIDList.size() <= Constants.MAX_BOB_HISTORY_SIZE) {
                    
                    Municipality m = cred.getGoverningAuthPeriod().getMuni();
                    for (Property pr : propList) {
                        if (pr.getMuni().getMuniCode() == m.getMuniCode()) {
                            propList.add(pr);
                        }
                    }
                }
            } catch (IntegrationException  ex) {
                System.out.println(ex);
            }
        }
        return propList;
    }

   
    
    /**
     * TODO: Update for parcelization
     * Generator method for Property objects
     * @param muni
     * @return the skelton property object with only muni set and ID of 0
     */
    public Property generatePropertySkeleton(Municipality muni) {
        Property prop = new Property(new Parcel());
        // don't make this until we pull it out for the first time
//        prop.setParcelInfo(new ParcelInfo());
        prop.setParcelInfo(new ParcelInfo());
        prop.setParcelKey(0);
        prop.setMuni(muni);
        return prop;
    }

    
        
    /**
     *  ***************************************************
     *  ***************************************************
     *  ************* UNITS UNITS UNITS *******************
     *  ***************************************************
     *  ***************************************************
     */
    
     /**
      * Checks if the given property unit is the default one which, 
      * in April 2023 is called "PRIMARY"
      * @param pu
      * @return true if the unit is PRIMARY, false for null or any other unit number
      */
    public boolean isUnitDefault(PropertyUnit pu){
        if(pu == null){
            return false;
        }
        if(pu.getUnitNumber().equals(DEFAULTUNITDESIGNATOR)){
            return true;
        }
        return false;
    }
    
    /**
     * Adaptor method for determining default unit properties
     * @param unitID
     * @return 
     */
    public boolean isUnitDefault(int unitID) throws BObStatusException, IntegrationException{
        if(unitID != 0){
            return isUnitDefault(getPropertyUnit(unitID));
        }
        else {
            return false;
        }
    }
    
    
    /**
     * This method generates a skeleton PropertyUnit with logical, preset
     * defaults, including empty lists.
     *
     * @param p
     * @return
     */
    public PropertyUnit getPropertyUnitSkeleton(Property p) {
        PropertyUnit propUnit = new PropertyUnit();
        propUnit.setParcelKey(p.getParcelKey());
        propUnit.setUnitNumber(Constants.TEMP_UNIT_NUM);
        return propUnit;
    }
    
    /**
     * Logic check for inserts of property units
     * @param unit
     * @param prop
     * @param u
     * @return the PK of the new unit
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public int insertPropertyUnit(PropertyUnit unit, Property prop, User u) throws BObStatusException, IntegrationException{
        PropertyIntegrator pi = getPropertyIntegrator();
        
        if(unit == null || prop == null || u == null){
            throw new BObStatusException("Cannot insert unit with null unit, property, or user");
        }
        unit.setCreatedBy(u);
        unit.setLastUpdatedBy(u);
        unit.setParcelKey(prop.getParcelKey());
        
        return pi.insertPropertyUnit(unit);
    }
    
    /**
     * Logic check for updates to a property unit
     * @param unit
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void updatePropertyUnit(PropertyUnit unit, UserAuthorized ua) throws BObStatusException, IntegrationException{
        PropertyIntegrator pi = getPropertyIntegrator();
        
        if(unit == null || ua == null){
            throw new BObStatusException("Cannot insert unit with null unit, property, or user");
        }
        unit.setLastUpdatedBy(ua);
        // updatedts by now()
        pi.updatePropertyUnit(unit);
        getPropertyCacheManager().flush(unit);
        
    }
    
    
    /**
     * DEAC is Equivalent to deleting a property unit
     * @param unit
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.EventException 
     */
    public void deactivatePropertyUnit(PropertyUnit unit, UserAuthorized ua) 
            throws BObStatusException, IntegrationException, AuthorizationException, EventException{
        PropertyIntegrator pi = getPropertyIntegrator();
        
        if(unit == null || ua == null){
            throw new BObStatusException("Cannot deac unit with null unit, property, or user");
        }
        if(!permissionCheckpointEditparcelAndPropertyInfo(ua)){
            throw new AuthorizationException("User does not have permission to deac a unit");
        }
        // get data heavy version and forbid 
        PropertyUnitDataHeavy pudh = getPropertyUnitDataHeavy(unit, ua);
        if(pudh.getPeriodList() != null && !pudh.getPeriodList().isEmpty()){
            throw new BObStatusException("Cannot deactivate a property unit that contains one or more permit files");
        }
        if(pudh.gethumanLinkList() != null && !pudh.gethumanLinkList().isEmpty()){
            throw new BObStatusException("Cannot deactivate a property unit with person links attached");
        }
        
        unit.setLastUpdatedBy(ua);
        // lastUpdatedTS by now()
        unit.setDeactivatedBy(ua);
        unit.setDeactivatedTS(LocalDateTime.now());
        
        pi.deactivatePropertyUnit(unit);
        getPropertyCacheManager().flush(unit);
    }
    
   

    /**
     * A method that takes a unit list and compares it to the database and
     * applies any changes (deactivations and insertions too) BUT BYPASSES THE
     * UNITCHANGEORDER WORKFLOW, so it should only be used internally.
     *
     * @Deprecated Nathan old code
     * @param unitList the edited unit list we would like to compare with the
     * DB's list
     * @param prop the property we would like to compare it with. Does not use
     * the built in list, but fetches it from the DB
     * @return the new list grabbed from the Database!
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public List<PropertyUnit> applyUnitList(List<PropertyUnit> unitList, Property prop)
            throws BObStatusException, IntegrationException, AuthorizationException {

        PropertyIntegrator pi = getPropertyIntegrator();

        sanitizePropertyUnitList(unitList);

        for (PropertyUnit unit : unitList) {

            // decide if we're updating a unit or inserting it based on initial value
            // newly created units don't have an ID, just a default unit number
            unit.setParcelKey(prop.getParcelKey());

            if (unit.getUnitID() == 0) {
                pi.insertPropertyUnit(unit);
            } else {
                pi.updatePropertyUnit(unit);
            }
        }

//        List<PropertyUnit> listTwo = pi.getPropertyUnitList(prop);

//        prop.setUnitList(listTwo);

        // mark parent property as updated now
        updateParcel(prop, getSessionBean().getSessUser());

        return null;

    }

    /**
     * Implements an existing change order and update its corresponding property
     * unit, also deactivates and updates the change order to record who
     * approved the transaction
     *
     * @param uc
     * @throws IntegrationException
     */
    public void implementPropertyUnitChangeOrder(PropertyUnitChangeOrder uc) throws IntegrationException, BObStatusException {

        PropertyIntegrator pi = getPropertyIntegrator();

        //If the user added the unit, their changes will already be in the database. No need to update
        if (!uc.isAdded()) {

            PropertyUnit skeleton = getPropertyUnit(uc.getUnitID());

            if (uc.isRemoved()) {
                 skeleton.setDeactivatedTS(LocalDateTime.now()); 
            } else {
                if (uc.getUnitNumber() != null) {
                    skeleton.setUnitNumber(uc.getUnitNumber());
                }

                /* TODO: What is this?
                if (uc.getOtherKnownAddress() != null) {
                    skeleton.setOtherKnownAddress("Updated");
                } else {
                    skeleton.setOtherKnownAddress("Updated");
                }*/
                

                if (uc.getNotes() != null) {
                    skeleton.setNotes(uc.getNotes());
                }

                if (uc.getRentalNotes() != null) {
                    skeleton.setRentalNotes(uc.getRentalNotes());
                }
            }

            pi.updatePropertyUnit(skeleton);
        }

        //Time to update the change order
        uc.setActive(false);
        uc.setApprovedOn(Timestamp.valueOf(LocalDateTime.now()));
        uc.setApprovedBy(getSessionBean().getSessUser());

        pi.updatePropertyUnitChange(uc);

    }
}
