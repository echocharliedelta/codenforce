/*
 * Copyright (C) 2017 cedba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.coordinators;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.BlobLight;
import java.io.Serializable;
import com.tcvcog.tcvce.entities.Credential;
import com.tcvcog.tcvce.entities.Human;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.Person;
import com.tcvcog.tcvce.entities.RoleType;
import com.tcvcog.tcvce.entities.User;
import com.tcvcog.tcvce.entities.UserMuniAuthPeriod;
import com.tcvcog.tcvce.entities.UserAuthorized;
import com.tcvcog.tcvce.entities.UserAuthorizedForConfig;
import com.tcvcog.tcvce.entities.UserMuniAuthPeriodInsertion;
import com.tcvcog.tcvce.entities.UserMuniAuthPeriodLogEntry;
import com.tcvcog.tcvce.entities.UserMuniAuthPeriodLogEntryCatEnum;
import com.tcvcog.tcvce.integration.MunicipalityIntegrator;
import com.tcvcog.tcvce.integration.UserIntegrator;
import com.tcvcog.tcvce.util.Constants;
import com.tcvcog.tcvce.util.MessageBuilderParams;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * *** SECURITY SENSITIVE CLASS ***
 * Primary business logic container for all things User and access related
 * All changes to this class should be made by knowledgeable folks only;
 * System are in place to check various organ function multiple times
 * before allowing changes. As such, AuthorizationExceptionsa are thrown
 * to client methods a bunch, most of which live in the UserConfigBB
 * 
 * Note the method prefix schema divides the methods roughly between 
 * auth_ which are methods pertaining mostly to authorization as a security event whose client
 * is the SessionInitializer 
 * <br>
 * user_ which are methods governing the creation, editing, and management of the User
 * object world, which is both a data concept and a security-backing class
 * <br>
 * <br>
 * McCandless Signature upgrade:
 * Special tables have their own version of createdts/user, updatedts/user, and deactivatedts/user
 * which are signed by the user doing those operations. 
 * 
 * The signature is:
 * The hash of that user's USER and UMAP record field dump <SEP>epoch in miliseconds
 * 
 * General principle: UMAP signature versus record signature, which is the UMAP sig +
 * hash of its field dump
 * 
 * the UMAP and its hash are never supposed to change, and those hashes are stored 
 * with each UMAP when it's made, and then in a separate table that's controlled
 * by a separate DB user (eventually)
 * 
 * Hashes are the same length, which is very handy
 * 
 * 
 * @author Ellen Bascomb (Apartment: 31Y)
 */
public class UserCoordinator extends BackingBeanUtils implements Serializable {
    
    final int MIN_PSSWD_LENGTH = 8;
    final int DEFAULT_USERMUNIAUTHPERIODLENGTHYEARS = 1;
    final int DEFAULT_ASSIGNMENT_RANK = 1;
    final int PERIOD_VALIDITYBUFFERMINUTES = 10;
    final RoleType MIN_ROLE_TO_FORCE_RESET = RoleType.SysAdmin;
    
    /**
     * Creates a new instance of UserCoordinator
     */
    public UserCoordinator(){
    
    }    
    
    /**
     * Pass-through method to the converter method on the Integrator which
     * returns a userid from a String username. Used only on first session auth
     * when JBoss gives us a username from the login system
     * 
     * @param userName
     * @return
     * @throws IntegrationException 
     */
    public int auth_getUserID(String userName) throws IntegrationException{
         UserIntegrator ui = getUserIntegrator();
         return ui.getUserID(userName);
     }
    
   
    
    /**
     * Builds a password used for temporary user management and password resets
     * @return 
     */
    public String user_generateRandomPassword_SECURITYCRITICAL(){
        Random rand = new Random();
        double d = rand.nextDouble();
        double mult = 1000000000;
        double l = d * mult;
        String pw = String.valueOf(l);
        System.out.println("UserCoordinator.user_generateRandomPassword_SECURITYCRITICAL | pw " + pw);
        String finalpw;
        if(pw.length() >= 6){
            finalpw = pw.substring(0, 8);
        } else {
            finalpw = "994786425";
        }
        System.out.println("UserCoordinator.user_generateRandomPassword_SECURITYCRITICAL | " + finalpw);
        return finalpw;
        
    }
    
    /**
     * Legacy method for random new passwords, producing really ugly base 64 chars
     * with dashes
     * @return 
     */
    private String user_generateUglyRandomPassword(){
        java.math.BigInteger bigInt = new BigInteger(1024, new Random());
        String randB64 = Base64.getEncoder().encodeToString(bigInt.toByteArray());
        System.out.println("Randomly generated BigInt: " + randB64);
        StringBuilder sb = new StringBuilder();
        sb.append(randB64.substring(0,3));
        sb.append("-");
        sb.append(randB64.substring(4,7));
        sb.append("-");
        sb.append(randB64.substring(randB64.length()-3,randB64.length()));
        return sb.toString();
        
    }
    
    
     /**
     * Primary user retrieval method: Note that there aren't as many checks here
     * since the jboss container is managing the lookup of authenticated users. 
     * We are pulling the login name from the already authenticated jboss session user 
     * and grabbing their list of authorized periods
     * 
     * Client methods are system login backing beans; NOT config
     * 
     * @param ua
     * @param umapReq with complete Muni-UMAP map in place
     * @return the fully baked cog user
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException occurs if the user
     * has been retrieved from the database but their access has been toggled off
     */
    public UserAuthorized auth_authorizeUser_SECURITYCRITICAL(UserAuthorized ua, UserMuniAuthPeriod umapReq ) throws AuthorizationException, IntegrationException{
        
        if(ua != null && umapReq != null){
            
            if(umapReq.getRecorddeactivatedTS() != null){
                throw new AuthorizationException("Cannot credentialize a deactivated authorization period!");
            }
            
            // ************************************************************
            // ******* GENERATE AND INJECT CREDENTIAL FOR CHOSEN MUNI *****
            // ************************************************************
            Credential cr = auth_generateCredential_SECURITYCRITICAL(umapReq);
            ua.setMyCredential(cr);
        } else {
            throw new AuthorizationException("UserAuthorized and requested UMAP required for auth");
            
        }

            // finally, inject the muniPeriodMap into the UA whose credential is set
        return ua;
    }
    
    /**
     * Provides its sole client of the SessionInitializer with a UserAuthorized
     * minus a credential but only valid UMAPs keyed by Muni
     * @param usr
     * @return
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public UserAuthorized auth_prepareUserForSessionChoice(User usr) throws IntegrationException, BObStatusException{
        UserIntegrator ui = getUserIntegrator();
        
        UserAuthorized ua = ui.getUserAuthorizedNoAuthPeriods(usr);
        ua.setMuniAuthPeriodsMap(auth_cleanMuniUMAPMap(auth_assembleMuniUMAPMapRaw(usr)));
        return ua;
    }
    
    
   
    
    /**
     * Serves the userConfigBB as its sole client who needs to display both valid
     * and invalid UMAPS to the admin user
     * @param u
     * @return 
     */
    private Map<Municipality, List<UserMuniAuthPeriod>> auth_assembleMuniUMAPMapRaw(User u){
        
        UserIntegrator ui = getUserIntegrator();
        Map<Municipality, List<UserMuniAuthPeriod>> umapMapRaw = null;
        List<UserMuniAuthPeriod> rawList = null;
        
        try {
            umapMapRaw = auth_buildMuniUMAPMap(ui.getUserMuniAuthPeriodsRaw(u.getUserID()));
        } catch (IntegrationException ex) {
            System.out.println(ex);
        }
        
        return umapMapRaw;
        
        
    }
    
    /**
     * SEURITY CRITICAL
     * 
     * Writes a timestamp to the crossMuniAuthTS field for checking in the muni fencing security system
     * 
     * @param ua
     * @return 
     */
    public UserAuthorized auth_authorizeUserForCrossMuniView(UserAuthorized ua) throws BObStatusException{
        if(ua == null){
            throw new BObStatusException("Cannot authorize xmuni view with null user");
        }
        System.out.println("UserCoordinator.auth_authorizeUserForCrossMuniView | username " + ua.getUsername());
        ua.setCrossMuniViewAuthorizationTS(LocalDateTime.now());
        return ua;
    }
    
    /**
     * SECURITY CRITICAL 
     * 
     * Removes the timestamp for cross muni view auth;
     * 
     * @param ua
     * @return
     * @throws BObStatusException 
     */
    public UserAuthorized auth_deauthorizeCrossMuniView(UserAuthorized ua) throws BObStatusException{
        if(ua == null){
            throw new BObStatusException("Cannot deauthorize xmuni view with null user");
        }
        ua.setCrossMuniViewAuthorizationTS(null);
        return ua;
        
        
    }
    
    /**
     * Utility method for creating a mapping of Municipality objects to 
     * lists of UMAPs for that muni
     * 
     * @param umapListRaw
     * @return 
     */
    private Map<Municipality, List<UserMuniAuthPeriod>> auth_buildMuniUMAPMap(List<UserMuniAuthPeriod> umapListRaw){
        
        Map<Municipality, List<UserMuniAuthPeriod>> tempMap = new HashMap<>();
        List<UserMuniAuthPeriod> tempUMAPList;
        
        if(umapListRaw != null && !umapListRaw.isEmpty()){
            for(UserMuniAuthPeriod umap: umapListRaw){
                Municipality mu = umap.getMuni();
                if(tempMap.containsKey(mu)){
                    tempUMAPList = tempMap.get(mu); // pull out our authorized peridos
                    tempUMAPList.add(umap); // add our new one
                    try{
                        Collections.sort(tempUMAPList); // sort based first on role rank, then assignment order ACROSS munis
                    } catch (IllegalArgumentException ex){
                        System.out.println("UserCoordinator.auth_buildMuniUMAPMap | Sort error in compareTo");
                        System.out.println(ex);
                    }
                } else {
                    // no existing record for that muni, so make a list, inject, and put
                    tempUMAPList = new ArrayList<>();
                    tempUMAPList.add(umap);
                }
                tempMap.put(umap.getMuni(), tempUMAPList);
            } // close for over periods
        }
        return tempMap;
    }
    
  
    
    /**
     * Extracts all UMAP objects from the DB, valid or not
     * @param u
     * @return
     * @throws IntegrationException 
     */
    private List<UserMuniAuthPeriod> user_auth_getUMAPListRaw(User u) throws IntegrationException{
        UserIntegrator ui = getUserIntegrator();
        return ui.getUserMuniAuthPeriodsRaw(u.getUserID());
        
    }
    
  
    
    /**
     * Creates a list of Users for use by search criterias on various pages
     * so the user can search by Users who have some past connection to any of the 
     * given Municipality objects passed into the method.
     * 
     * TODO: Customize list based on muni connections
     * NOTED in github issues by ECD 25-may-2020
     * 
     * @param usr
     * @return An assembled list of users for authorization
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public List<User> user_assembleUserListForSearch(User usr) throws BObStatusException{
        // we do nothing with user
        UserIntegrator ui = getUserIntegrator();
        
        List<User> ulst = new ArrayList<>();
        
        try {
            for(Integer i: ui.getSystemUserIDList()){
                ulst.add(user_getUser(i));
            }
        } catch (IntegrationException ex) {
            System.out.println(ex);
        }
        return ulst;
        
    }
    
    /**
     * Container for business logic surrounding user period Validation
     * 
     * Returns a Period which has been evaluated for validity. The client method
     * is usually going to assess a Period as Valid if the return value of
     * getValidatedTS() is not null
     * 
     * @param uap the Period to be evaluated for validation
     * @return 
     */
    private UserMuniAuthPeriod auth_validateUserMuniAuthPeriod_SECURITYCRITICAL(UserMuniAuthPeriod uap){
        // they all get evaluated and stamped
        LocalDateTime syncNow = LocalDateTime.now();
        uap.setValidityEvaluatedTS(syncNow);
        if( 
                uap.getRecorddeactivatedTS() != null 
            ||  uap.getStartDate().isAfter(LocalDateTime.now())
            ||  uap.getStopDate().isBefore(LocalDateTime.now())
        ){
            return uap;
        }
        // since we have a valid period, git it the extra valid stamp
        uap.setValidatedTS(syncNow);
        return uap;
    }
    
    
    
    /**
     * Internal untility method for rebuild a Muni-UMAP map with only valid UMAPS
     * @param umapMapRaw
     * @return 
     */
    private Map<Municipality, List<UserMuniAuthPeriod>> auth_cleanMuniUMAPMap(Map<Municipality, List<UserMuniAuthPeriod>> umapMapRaw){
        List<UserMuniAuthPeriod> tempUMAPList = new ArrayList<>();
        List<Municipality> tempMuniList;
        Map<Municipality, List<UserMuniAuthPeriod>> tempMap = new HashMap<>();

            if(umapMapRaw != null && !umapMapRaw.isEmpty()){ 
                Set<Municipality> muniSet = umapMapRaw.keySet();
                if(!muniSet.isEmpty()){
                    tempMuniList = new ArrayList(muniSet);
                    for(Municipality muni: tempMuniList){
                        tempUMAPList = umapMapRaw.get(muni);
                        tempMap.put(muni, auth_cleanUserMuniAuthPeriodList(tempUMAPList));
                    }

                }
            }
            return tempMap;
        }
    
    /**
     * Convenience adaptor method for checking the validity of a generic list of raw UMAPs
     * and only returning valid periods. This method also calls Collections.sort on its inputted umap list
     * so that the highest ranked valid period is first
     * 
     * @param rawUMAPList
     * @return the list of only valid UMAPs
     */
    private List<UserMuniAuthPeriod> auth_cleanUserMuniAuthPeriodList(List<UserMuniAuthPeriod> rawUMAPList){
        List<UserMuniAuthPeriod> cleanList = null; 
        //make sure we have a valid list
        if(rawUMAPList != null && !rawUMAPList.isEmpty()){
            cleanList = new ArrayList<>();
            for(UserMuniAuthPeriod umap: rawUMAPList){
                if(auth_validateUserMuniAuthPeriod_SECURITYCRITICAL(umap).getValidatedTS() != null){
                    cleanList.add(umap);
                }
            }
            /// sort our list before returning
            // commented out: violates general contract of compare
            if(!cleanList.isEmpty()){
                try{
                    Collections.sort(cleanList);
                } catch (IllegalArgumentException ex){
                    System.out.println("UserCoordinator.auth_cleanUserMuniAuthPeriodList | Sort error in compareTO");
                    System.out.println(ex);
                }
            }
        }
        return cleanList;
   }
    
    
    
    
    /**
     * *** SECURITY CRITICAL METHOD ***
     * Throws an exception if the given UserAuthorized does meet or exceed
     * the rank of the supplied role type
     * @param ua
     * @param rt 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void auth_verifyUserAuthorizedRank_MeetOrExceed_SECURITYCRITICAL(UserAuthorized ua, RoleType rt) 
            throws BObStatusException, AuthorizationException{
        if(ua == null || rt == null){
            throw new BObStatusException("Cannot verify rank (meet or exceed) with null User or Role");
            
        }
        if(ua.getKeyCard().getGoverningAuthPeriod().getRole().getRank() < rt.getRank()){
            StringBuilder sb = new StringBuilder();
            sb.append("User with rank: ");
            sb.append(ua.getKeyCard().getGoverningAuthPeriod().getRole().getLabel());
            sb.append("(rank ");
            sb.append(ua.getKeyCard().getGoverningAuthPeriod().getRole().getRank());
            sb.append(") fails to meet or exceed required rank: ");
            sb.append(rt.getLabel());
            sb.append("(rank");
            sb.append(rt.getRank());
            sb.append(");");
            
            throw new AuthorizationException(sb.toString());
        }
    }
   
   
    /**
     * Generates a list of what role types a given user can assign to new users 
     * they create. As of JAN 2022, users can add other UMAPs of lesser or equal roles 
     * @param user
     * @return 
     */
    public List<RoleType> auth_getPermittedRoleTypesToGrant(UserAuthorized user){
        List<RoleType> rtl;
        List<RoleType> rtlAuthorized = new ArrayList<>();
        rtl = new ArrayList<>(Arrays.asList(RoleType.values()));
        for(RoleType rt: rtl){
            // exclusion logic could go here, but this was disabled in JAN 2023
            // was resurrected in Jan 2024...
            if(user.getRole().getRank() >= rt.getRank()){
                rtlAuthorized.add(rt);
            }
        }
        return rtlAuthorized;
    }
    
    /**
     * Logic container for sifting through a User's permissions and returning only
     * a list of UMAPs that meet or exceed the given RoleType threshold
     * @param ua
     * @param rt the minimum rank the user must have in an authorized muni to get filtered
     * @return
     * @throws BObStatusException 
     */
    protected List<UserMuniAuthPeriod> auth_user_buildUMAPListMinRank(UserAuthorized ua, RoleType rt) throws BObStatusException{
        if(ua == null || rt == null){
            throw new BObStatusException("Cannot build list with null UA or RoleType");
        }
        
        Map<Municipality, List<UserMuniAuthPeriod>> umapList = ua.getMuniAuthPeriodsMap();
        
        return null;
        
    }
    
    
    /**
     * Fetching method for UMAPs that are NOT validation stamped, and therefore cannot
     * be used for permissions checks, only for injecting into objects tracked
     * by UMAPs, namely those which extend UMAPTrackedEntity abstract Superclass.
     * 
     * December 2023 Notes: this notion of a NotValidated UMAP should really be
     * manifest in the class hieararchy of UMAPS not these silly methods that do or dont' set
     * fields
     * 
     * @param umapID
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public UserMuniAuthPeriod auth_getUMAPNotValidated(int umapID) throws BObStatusException, IntegrationException{
        if(umapID == 0){
            throw new BObStatusException("Cannot fetch a UMAP with an ID = 0");
        }
        UserIntegrator ui = getUserIntegrator();
        UserMuniAuthPeriod umap = ui.getUserMuniAuthPeriod(umapID);
                        // Commented out looking for leak
//        umap.setPeriodActivityLogBook(ui.getMuniAuthPeriodLogEntryList(umap));
        return umap;
        
    }
    
    /**
     * Logic pass through for querying the auth log table
     * @param start
     * @param end
     * @return 
     */
    public List<UserMuniAuthPeriodLogEntry> getUMAPLog(LocalDateTime start, LocalDateTime end) throws BObStatusException, IntegrationException{
        if(start == null || end == null){
            throw new BObStatusException("UMAP Log start and end dates cannot be null");
        }
        if(start.isAfter(end)){
            throw new BObStatusException("UMAP log query start date cannot be after the end date");
        }
        UserIntegrator ui = getUserIntegrator();
        // no longer logging to monitor system stability
//        return new ArrayList<>();
        
        return ui.getMuniAuthPeriodLogEntryList(start, end);
    }
    
    /**
     * Utility method for creating a loaded up object for logging the authorization
     * process results to the DB
     * @param ua
     * @param cat
     * @return 
     */
    public UserMuniAuthPeriodLogEntry auth_assembleUserMuniAuthPeriodLogEntrySkeleton(
                                        UserAuthorized ua, 
                                        UserMuniAuthPeriodLogEntryCatEnum cat){
        
        UserMuniAuthPeriodLogEntry skel = new UserMuniAuthPeriodLogEntry();
        skel.setCategory(cat.toString());
        // this is being set here in the skeleton factory and should stay here
        // the redundant injection in the coordinator should be a check instead
        skel.setUserMuniAuthPeriodID(ua.getMyCredential().getGoverningAuthPeriod().getUserMuniAuthPeriodID());
        
        return skel;
    }
    
    /**
     * Utility method for logging the use of a credential to access a restricted
     * object of many kinds
     * 
     * @param entry
     * @param umap
     * @throws IntegrationException
     * @throws AuthorizationException 
     */
    public void auth_logCredentialInvocation(UserMuniAuthPeriodLogEntry entry, UserMuniAuthPeriod umap) throws IntegrationException, AuthorizationException{
        UserIntegrator ui = getUserIntegrator();
        if(umap != null && umap.getUserMuniAuthPeriodID() != 0){
            entry.setUserMuniAuthPeriodID(umap.getUserMuniAuthPeriodID());
            ui.insertUserMuniAuthPeriodLogEntry(entry);
        } else {
            throw new AuthorizationException("Credentials must be logged with a valid periodid");
        }
    }
    
    /**
     * *** SECURITY CRITICAL METHOD ***
     * <br>
     * Creates a base AuthorizationPeriod which the admin user configures 
     * for injection into the DB, allowing user to access that Muni's functions
     * 
     * @param requestor
     * @param userCandidate
     * @param m
     * @return
     * @throws AuthorizationException 
     */
    public UserMuniAuthPeriodInsertion auth_initializeUserMuniAuthPeriod_SECURITYCRITICAL( UserAuthorized requestor, 
                                                            UserAuthorized userCandidate, 
                                                            Municipality m) throws AuthorizationException{
        UserMuniAuthPeriodInsertion umap = null;
        // check that the requestor has at least SysAdmin or better in the requested Muni
        if(requestor != null && userCandidate != null){
            System.out.println("UserCoordinator.auth_initializeUserMuniAuthPeriod_SECURITYCRITICAL | New UMAP init by " + requestor.getUsername());
            umap = new UserMuniAuthPeriodInsertion(m);
            umap.setUserID(userCandidate.getUserID());
            umap.setStartDate(LocalDateTime.now());
            umap.setStopDate(LocalDateTime.now().plusYears(DEFAULT_USERMUNIAUTHPERIODLENGTHYEARS));
            umap.setCreatedByUserID(requestor.getCreatedByUserId());
            umap.setAssignmentRelativeOrder(DEFAULT_ASSIGNMENT_RANK);
            umap.setNotes("");
        } else {
            System.out.println("UserCoordinator.auth_initializeUserMuniAuthPeriod_SECURITYCRITICAL | New UMAP init error: null requestor");
            throw new AuthorizationException("Requesting user is not authorized to add auth periods in this muni");
        }
        return umap;
    }
    
    /**
     * *** SECURITY CRITICAL METHOD ***
     * 
     * Insertion point and logic intermediary for UMAPS
     * 
     * @param requestingUser
     * @param usee
     * @param uap
     * @throws AuthorizationException
     * @throws IntegrationException 
     */
    public void auth_insertUserMuniAuthorizationPeriod_SECURITYCRITICAL(User requestingUser, User usee, UserMuniAuthPeriodInsertion uap) throws AuthorizationException, IntegrationException{
        UserIntegrator ui = getUserIntegrator();
        if(uap != null && requestingUser != null && usee != null && uap.getMuni() != null){
            if(uap.getStartDate() != null && uap.getStartDate().isBefore(uap.getStopDate())){
                if(uap.getStopDate() != null && uap.getStopDate().isAfter(LocalDateTime.now())){
                    configureUMAPForPermissionsHierarchyFidelity(uap);
                    uap.setCreatedByUserID(requestingUser.getUserID());
                    uap.setUserID(usee.getUserID());
                    ui.insertUserMuniAuthorizationPeriod(uap);
                    getSystemMuniCacheManager().getCacheUser().invalidate(usee.getCacheKey());
                } else {
                    throw new AuthorizationException("Stop date must be not null and in the future");
                }
            } else {
                throw new AuthorizationException("Start date must not be or dated more than a year past");
            }
        } else {
            throw new AuthorizationException("One or more required objects is null");
        }
    }
    
    /**
     * During permissions upgrade March 2023, Manager and Sys admins must also have 
     * CEO flag on; This method checks for this and adjusts ceo flag to reflect this 
     * configuration.
     * @param umap 
     */
    private void configureUMAPForPermissionsHierarchyFidelity(UserMuniAuthPeriodInsertion umap) throws AuthorizationException{
        if(umap == null){
            throw new AuthorizationException("Cannot configure null UMAP"); 
        }
        // all managers need code officer permissions
        if(umap.getRole() == RoleType.MuniManager){
            if(!umap.isCodeOfficer()){
                umap.setInsertOfficer(true);
            }
        }
        // all sys admins need code officer permissions
        if(umap.getRole() == RoleType.SysAdmin){
            if(!umap.isCodeOfficer()){
                umap.setInsertOfficer(true);
            }
        }
    }
    
    
    
     /**
     * *** SECURITY CRITICAL METHOD ***
     * 
     * Container for all access control mechanism authorization switches
     * 
     * Design goal: have boolean type getters on users for use by the View
     * to turn on and off rendering and enabling as needed.
     * 
     * NOTE: This is the ONLY method system wide that calls any setters for
     * access permissions
     * 
     * @param rt
     * @return a User object whose access controls switches are configured
     */
    private Credential auth_generateCredential_SECURITYCRITICAL(UserMuniAuthPeriod uap) throws AuthorizationException{
        Credential cred = null;
        
        switch(uap.getRole()){
            
            case SysAdmin:
                cred = new Credential(          uap,
                                                true,   // sysadmin
                                                true,   // cogstaff
                                                true,   // muniStaff
                                                true);  // muniReader
               break;               
               
            case MuniManager:
                cred = new Credential(          uap,
                                                false,   // sysadmin
                                                true,   // cogstaff
                                                true,   // muniStaff
                                                true);  // muniReader
               break;               
                
            case MuniStaff:
                cred = new Credential(          uap,
                                                false,   // sysadmin
                                                false,   // cogstaff
                                                true,   // muniStaff
                                                true);  // muniReader
               break;
               
            case MuniReader:
                cred = new Credential(          uap,
                                                false,   // sysadmin
                                                false,   // cogstaff
                                                false,   // muniStaff
                                                true);  // muniReader
               break;  
            case Public: 
                cred = new Credential(      uap,
                                                false,   // sysadmin
                                                false,   // cogstaff
                                                false,   // muniStaff
                                                false);  // muniReader
                break;
            default:
                cred = null;
        }        
        return cred;
    }    
    
   
    
    /**
     * Insertion point for new User objects.NOTE that this is NOT a 
 security critical method since a User by itself cannot even login
     * @param ua
     * @param usr
     * @return
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public int user_insertNewUser(UserAuthorized ua, User usr) throws IntegrationException, AuthorizationException{
        int newUserID = 0;
        UserIntegrator ui = getUserIntegrator();
        if(usr != null && usr.getUsername() != null && ua != null){
            if(user_checkUsernameAllowedForInsert(usr.getUsername())){
                usr.setLastUpdatedByUserID(ua.getUserID());
                usr.setCreatedByUserId(ua.getUserID());
                newUserID = ui.insertUser(usr);
            } else {
                throw new AuthorizationException("Non-unique username!");
            }
        } else {
            throw new AuthorizationException("Cannot create new user from Null or without username");
        }
        return newUserID;
    }
    
    /**
     * Updates only the user's signature blob
     * 
     * @param ua for updating
     * @param bl to inject into the user
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
     public void user_updateUserAuthorizedSignatureBlob(UserAuthorized ua, BlobLight bl) throws BObStatusException, IntegrationException{
        if(ua == null || bl == null){
            throw new BObStatusException("Cannot update a user's sig blob with null user or blob");
        }
        UserIntegrator ui = getUserIntegrator();
        ua.setSignatureBlob(bl);
        ui.updateUserSignatureBlob(ua);
        getSystemMuniCacheManager().flush(ua);
        
    }
    
    /**
     * Utility method for querying for an existing username
     * @param uname
     * @return true if the username is unique and user insert can proceed; false 
     * if name is not unique--abort insert
     */
    public boolean user_checkUsernameAllowedForInsert(String uname){
        UserIntegrator ui = getUserIntegrator();
        if(uname == null){
            return false;
        }
        boolean allowed = false;
        try {
            int checkID = ui.getUserID(uname);
            if (checkID == 0) {
                System.out.println("UserCoordinator.user_checkUsernameAllowedForInsert: no user found with name " + uname);
                allowed = true; 
            } else {
                System.out.println("UserCoordinator.user_checkUsernameAllowedForInsert: DUPLICATE NAME: " + uname);
                
            }
        } catch (IntegrationException ex) {
            System.out.println(ex);
        }
                
        return allowed;
        
    }
    
    
    /**
     * Supplies clients with a UserAuthorized carrying a valid credential
     * backed by a valid UserAuthorizationPeriod required for carrying out many
     * Coordinator tasks
     * @return A UserAuthorized with the lowest possible rank
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public UserAuthorized auth_getPublicUserAuthorized() throws IntegrationException, BObStatusException{
        UserAuthorized ua = null;
        int publicUserID = Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                    .getString("publicuserid"));

        User u = user_getUser(publicUserID);
       
        if(u != null){
            
             ua = auth_prepareUserForSessionChoice(u);
        }
        // why this is commented out ECD doesn't know as of 3 DEC 2023
//        UserMuniAuthPeriod umap = ui.getUserMuniAuthPeriod(publicUserUMAPID);
        
        try {
            if(ua != null && ua.getMuniAuthPeriodsMap() != null && !ua.getMuniAuthPeriodsMap().isEmpty()){
                Map<Municipality, List<UserMuniAuthPeriod>> umapMapTemp = ua.getMuniAuthPeriodsMap();
                // by design, we have only one muni, COGland for this user
                List<Municipality> muniList  =new ArrayList<>();
                muniList.addAll(umapMapTemp.keySet());
                if(!muniList.isEmpty() && umapMapTemp.containsKey(muniList.get(0))){
                    List<UserMuniAuthPeriod> umapList = umapMapTemp.get(muniList.get(0));
                    if(umapList != null && !umapList.isEmpty()){
                        ua = auth_authorizeUser_SECURITYCRITICAL(ua, umapList.get(0));
                    }
                }
            }
        } catch (AuthorizationException ex) {
            System.out.println(ex);
        }
        
        return ua;
    }
    
    /**
     * Utility for checking if a given User is the public user, which is done by
     * comparing the given user's ID to the system's sentinel public user ID which
     * lives in the dbFixedValueLookup.properties file
     * 
     * @param candidate
     * @return true if the user is the public user
     */
    public boolean determineIfUserIsPublicUser(User candidate){
        if(candidate != null){
            int publicUserID = Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                    .getString("publicuserid"));
            if(candidate.getUserID() == publicUserID){
                return true;
            }
        }
        return false;
    }
    
    /**
     * The COGBot is a user that exists only in cyberspace and is used 
     * as the owner of events created by the public and can also make requests
     * to users at various times for various reasons.No human should ever
 attempt to take on the role of the COGBot for risk of becoming a cyborg
 is, indeed, very great.
     * @return
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public User user_getUserRobot() throws IntegrationException, BObStatusException{
        UserCoordinator uc = getUserCoordinator();
        User u;
        u = uc.user_getUser(Integer.parseInt(
                getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                        .getString("cogRobotUserID")));
        return u;
    }
    
    /**
     * Builds a UserAuthorized for Sylvia for DB write purposes.
     * @param requestedRole as of March 2024 only MuniStaff, MuniManager, and SysAdmin are supported
     * @return sylvia as an authorized user suitable for DB writes, not Java operations
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public UserAuthorized auth_getRobotUserAuthPeriod(RoleType requestedRole) throws BObStatusException, IntegrationException{
        UserIntegrator ui = getUserIntegrator();
        String umapLookupKey = null;
        if(requestedRole != null){
            switch(requestedRole){
                case MuniStaff -> {
                    umapLookupKey = "cogRobotStaffNoCEOUMAPID";
                }
                
                case MuniManager -> {
                    umapLookupKey = "cogRobotManagerCEOUMAPID";
                    
                }
                case SysAdmin -> {
                    umapLookupKey = "cogRobotSysAdminUMAPID";
                }
            }
        }
        
        if(umapLookupKey == null){
            throw new BObStatusException("Unable to get robot UMAP");
        }
       
        return ui.getUserAuthorizedByUMAPID(Integer.parseInt(
                getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                        .getString(umapLookupKey)));
    }
 
    /**
     * Mini logic container to check a list of UMAPs for dev credentials
     * @param umapList
     * @return true if no dev ranked UMAPs were found
     */
    private RoleType auditUMAPList_determineHighestRank(List<UserMuniAuthPeriod> umapList){
         
        RoleType highestRole = RoleType.Public;
        if(umapList != null && !umapList.isEmpty()){
            
            for(UserMuniAuthPeriod umap: umapList){
                        if(umap.getRole().getRank() > highestRole.getRank()){
                            highestRole = umap.getRole();
                        }
                    }
        }
        
        return highestRole;
    }
    
    
    /**
     * Factory method for User objects
     * 
     * @param u the requesting user
     * @return 
     */
    public User user_getUserSkeleton(User u){
        User skel = new User();
        if(u != null){
            skel.setHomeMuniID(u.getHomeMuniID());
            skel.setCreatedByUserId(u.getUserID());
        }
        return skel;
    }
   
    
    

     /**
      * Adds a timestamp to the period's invalidation. Normally, a period is not
      * directly invalidated but rather expires and is updated with a new one
      * @param aup
      * @param requestingUser
      * @param note
      * @throws IntegrationException
      * @throws AuthorizationException 
      */
    public void auth_invalidateUserAuthPeriod(UserMuniAuthPeriod aup, UserAuthorized requestingUser, String note) throws IntegrationException, AuthorizationException{
        SystemCoordinator sc = getSystemCoordinator();
        UserIntegrator ui = getUserIntegrator();
        if(aup.getUserMuniAuthPeriodID() == requestingUser.getMyCredential().getGoverningAuthPeriod().getUserMuniAuthPeriodID()){
            throw new AuthorizationException("You are unauthorized to invalidate your current authorization period");
        }
        aup.setNotes(sc.appendNoteBlock(new MessageBuilderParams(aup.getNotes(), "INVALIDATION OF AUTH PERIOD", "", note, requestingUser, requestingUser.getMyCredential())));
        ui.invalidateUserAuthRecord(aup);
        getSystemMuniCacheManager().flush(aup);
    }
    
    
    /**
     * This is NOT a UserAuthorized so we're just passing out objects here
     * @param userID
     * @return
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public User user_getUser(int userID) throws IntegrationException, BObStatusException{
        if(userID == 0){
//            System.out.println("UserCoordinator.user_getUser | input ID 0");
            return null;
        }
        UserIntegrator ui = getUserIntegrator();
        return user_configureUser(ui.getUser(userID));
    }
    
    
    /**
     * Internal logic setter for simple user objects. Most importantly, 
     * I inject the User's official human!
     * @param usr
     * @return
     * @throws BObStatusException 
     */
    private User user_configureUser(User usr) throws BObStatusException, IntegrationException{
        if(usr == null){
            throw new BObStatusException("Cannot configure a null user");
        }
        PersonCoordinator pc = getPersonCoordinator();
        
        usr.setUserHuman(pc.getHuman(usr.getHumanID()));
        
        return usr;
    }
    
    /**
     * November 2022 version that grabs all system users and all their UMAPS, with 
     * validity checked, but no Credentials
     * @param userRequestor
     * @return the users to display on the user config screen
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public List<UserAuthorizedForConfig> user_auth_assembleCompleteUserListForConfig(UserAuthorized userRequestor) 
            throws AuthorizationException, IntegrationException, BObStatusException {
         
        UserIntegrator ui = getUserIntegrator();
        List<User> usersForConfig = null;
        
        // Check for sys admin or better for user list
        if(userRequestor != null){
            if(!userRequestor.getKeyCard().isHasSysAdminPermissions()){
                throw new AuthorizationException("User " + userRequestor.getUsername() + " does not have sys admin or better permissions to view user list");
            }
        }
        List<UserAuthorizedForConfig> ualist = new ArrayList<>();
        
        usersForConfig = ui.getUsersByHomeMuni(null);
         if(usersForConfig != null && !usersForConfig.isEmpty()){
            for(User usr: usersForConfig){
                ualist.add(user_transformUserToUserAuthorizedForConfig(usr));
            }
        }
         
         return ualist;
        
        
    }
    
    
    
    /**
     * Generates a User list that represents allowable users to engage with
     * for a given muni. This method doesn't return fully-fledged users.
     * This method asks the inputted UserAuthorized for all of its
     * (VALID) UMAPS and gets all the other Users who share ANY muni in that list
     * @param userRequestor
     * @return the users for the userConfig page
     * @throws IntegrationException
     * @throws AuthorizationException 
     */
    public List<UserAuthorizedForConfig> user_auth_assembleUserListForConfig(UserAuthorized userRequestor) 
            throws IntegrationException, AuthorizationException, BObStatusException{
        
        UserIntegrator ui = getUserIntegrator();
        List<User> usersForConfig = null;
        
        if(userRequestor != null){
            usersForConfig = new ArrayList<>();
            // build a list of Users who have a valid auth period in any Municipality in which
            // the passed in adminUser has SysAdmin RoleType
            for(Municipality mu: userRequestor.getAuthMuniList()){
                // if the admin's own auth map for the iterated municipality includes a record
                // with SysAdmin or higher (which it should, since they're on the userConfig.xhtml page
                if( userRequestor.getMuniAuthPeriodsMap().get(mu) != null 
                        &&
                    !userRequestor.getMuniAuthPeriodsMap().get(mu).isEmpty()
                        &&
                    (userRequestor.getMuniAuthPeriodsMap().get(mu).get(0).getRole().getRank() >= RoleType.SysAdmin.getRank())
                ){
                    // is this supposed to be addAll()?
                    usersForConfig = user_auth_assembleUserListForConfig(mu,userRequestor);
                } 
            } //close loop over authmunis 
            
            // add any users who don't have any auth periods in that muni but whose home muni is the user's auth muni
            List<User> usersInHomeMuni = ui.getUsersByHomeMuni(userRequestor.getKeyCard().getGoverningAuthPeriod().getMuni());
            List<User> usersToAddNoAuthPeriod = new ArrayList<>();
            for(User usr: usersInHomeMuni){
                if(!usersForConfig.contains(usr)){
                    usersToAddNoAuthPeriod.add(usr);
                }
            }
            if(!usersToAddNoAuthPeriod.isEmpty()){
                usersForConfig.addAll(usersToAddNoAuthPeriod);
            }
            
        } // close param not null check
        
        List<UserAuthorizedForConfig> ualist = new ArrayList<>();
        if(usersForConfig != null && !usersForConfig.isEmpty()){
            for(User usr: usersForConfig){
                ualist.add(user_transformUserToUserAuthorizedForConfig(usr));
            }
        }
        return ualist;
    }
    
    
    
    /**
     * Assembled Users are those who have had any auth period, 
     * currently valid OR NOT in the passed in municipality.
     * This is basically an adapter to convert the raw user IDs that
     * come from the integrator's list of UMAPs into fully-baked 
     * User objects. As of OCT 2019 at this method's birth, 
     * no additional logic is implemented other than the User existing.
     * 
     * @param m
     * @return 
     */
    private List<User> user_auth_assembleUserListForConfig(Municipality m, UserAuthorized uq) throws AuthorizationException, IntegrationException, BObStatusException{
        UserIntegrator ui = getUserIntegrator();
        
        List<UserMuniAuthPeriod> umapList;
        List<Integer> userIDList = null;
        List<User> userList = null;
        
        if(m != null){
            userList = new ArrayList<>();
            
            umapList = ui.getUserMuniAuthPeriodsRaw(m);
            
            if(umapList != null && !umapList.isEmpty()){
                userIDList = new ArrayList<>();
               
                // Consider adding a step here to remove invalid periods,
                // meaning we can restrict to only seeing "active users"
                // in your municipality to say those Users ranked Dev or better
//                if(uq.getRole().getRank() == RoleType.SysAdmin.getRank()){
//                    umapList = cleanUserMuniAuthPeriodList(umapList);
//                }
                for(UserMuniAuthPeriod umap: umapList){
                    if(!userIDList.contains(umap.getUserID())){
                        userIDList.add(umap.getUserID());
                    }
                }
            } // close build list of user IDs to fetch for passed in muni
            
            // as long as we have a userID for fetching
            if(userIDList != null && !userIDList.isEmpty()){
                for(Integer i: userIDList){
                    userList.add(ui.getUser(i));
                }
            } // we have a list of Users to return!
        }
        return userList;


    }
    
    
    
    /**
     * Implements logic to assemble a list of code officer users only.NOTE that 
     * this list will select only users who have a valid UMAP
     * in the given muni 
     * As of FEB 2024 oath timestamps are NOT considered. INVALID UMAPS are not used to assemble this list.
     * @param muni
     * @param includeSysAdmins when true code officer users who are at system admin rank will be allowed into list
     * @return all users in the given muni who have a non-null oathts
     */
    public List<User> user_assembleUserListOfficerRequired(Municipality muni, boolean includeSysAdmins) throws IntegrationException, AuthorizationException, BObStatusException{
        UserIntegrator ui = getUserIntegrator();
        List<User> candidateUsers = new ArrayList<>();
        if(muni != null){
            List<UserMuniAuthPeriod> umapl = ui.getUserMuniAuthPeriodsRaw(muni);
            umapl = auth_cleanUserMuniAuthPeriodList(umapl);
            if(umapl != null && !umapl.isEmpty()){
                for(UserMuniAuthPeriod u: umapl){
                    if(u.isCodeOfficer()){
                        // skip sys admins if specified
                        if(u.getRole() != null && u.getRole() == RoleType.SysAdmin && !includeSysAdmins){
                            continue;
                        }
                        candidateUsers.add(user_getUser(u.getUserID()));
                    }
                }
            } // we have umaps
        } // null input check
        
        return candidateUsers;
    }
    
    /**
     * Implements logic to assemble a list users who, as of now(), 
     * have a valid UMAP of MUNI staff or greater rank in the given muicipality. 
     * @param muni
     * @param rankFloor minimum rank required to be enlisted; null passes through all users with valid umaps in the muni
     * @return all users in the given muni who have a non-null oathts
     */
    public List<User> user_assembleUserListMuniActiveRankFloored(Municipality muni, RoleType rankFloor) throws IntegrationException, AuthorizationException, BObStatusException{
        UserIntegrator ui = getUserIntegrator();
        List<User> muniUsers = new ArrayList<>();
        if(muni != null){
            List<UserMuniAuthPeriod> umapl = ui.getUserMuniAuthPeriodsRaw(muni);
            umapl = auth_cleanUserMuniAuthPeriodList(umapl);
            if(umapl != null && !umapl.isEmpty()){
                for(UserMuniAuthPeriod u: umapl){
                    User usr = user_getUser(u.getUserID());
                    if(rankFloor != null){
                        if(u.getRole().getRank() >= rankFloor.getRank()){
                            if(!muniUsers.contains(usr)){
                                muniUsers.add(usr);
                            }
                        }
                    } else { // with null floor, all valid users down through public are allowed
                        muniUsers.add(usr);
                    }
                }
            } // we have umaps
        } // null input check
        
        return muniUsers;
    }
    
    
    /**
     * Logic bundle for building a UserAuthorized without a credential for
     * configuration purposes; Credential objects are only inserted 
     * when that UserAuthorized is backing a system session
     * @param userToConfigure
     * @return
     * @throws AuthorizationException
     * @throws IntegrationException 
     */
    public UserAuthorizedForConfig user_transformUserToUserAuthorizedForConfig(User userToConfigure) throws AuthorizationException, IntegrationException, BObStatusException{
        UserAuthorizedForConfig uafc = null;
        UserIntegrator ui = getUserIntegrator();
        MunicipalityCoordinator mc = getMuniCoordinator();
        if(userToConfigure != null){
            User utemp;
            // existing users get goodies
            if(userToConfigure.getUserID() != 0){
                utemp = user_getUser(userToConfigure.getUserID());
                
                uafc = new UserAuthorizedForConfig(ui.getUserAuthorizedNoAuthPeriods(utemp));

                uafc.setMuniAuthPeriodsMap(auth_assembleMuniUMAPMapRaw(userToConfigure));
                uafc.setUmapList(buildUMAPListForConfigWithValidityCheck(utemp));
                uafc.setHomeMuni(mc.getMuni(uafc.getHomeMuniID()));
            } else {
                // skeletons just get wrapped in empty objects 
                utemp = userToConfigure;
                uafc = new UserAuthorizedForConfig(new UserAuthorized(utemp));
            }
            
            System.out.println("UserCoordinator.user_transformUserToUserAuthorizedForConfig: Transformed username " + userToConfigure.getUsername());
        }
        return uafc;
    }
    
    
    /**
     * Internal organ for creating a list of UMAPs with a validity check
     * @param u whose UMAPs should be assembled
     * @return the UMAPs
     */
    private List<UserMuniAuthPeriod> buildUMAPListForConfigWithValidityCheck(User u) throws IntegrationException{
        UserIntegrator ui = getUserIntegrator();
        List<UserMuniAuthPeriod> umapl = ui.getUserMuniAuthPeriodsRaw(u.getUserID());
        List<UserMuniAuthPeriod> umapValidated = new ArrayList<>();
        if(umapl != null && !umapl.isEmpty()){
            for(UserMuniAuthPeriod umap: umapl){
                umapValidated.add(auth_validateUserMuniAuthPeriod_SECURITYCRITICAL(umap));
            }
        }
        return umapValidated;
        
    }


    /**
     * Convenience method for upcasting UserAuthorized to simple User and putting
     * them in a list
     * @param uaList
     * @return 
     */
    public List<User> user_upcastUsersFromUserAuthorized(List<UserAuthorized> uaList){
        List<User> uList = null;
        if(uaList != null && !uaList.isEmpty()){
            uList = new ArrayList<>();
            for(UserAuthorized ua: uaList){
                uList.add( (User) ua);
            }
        }
        return uList;
    }
    
    
   
    
    
    /* ************************************************************ */
    /* ***************** USER UPDATE METHODS ********************** */
    /* ************************************************************ */
    
    /**
     * Updates the User's Person and username only.Note -- does not update the password which is updated separately
     * @param ua doing the updating
     * @param hostUser the user whose human link should be updated to the passed in human
     * @param targetHuman the proposed new human link for passed in user
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void user_updateUserHumanLink(UserAuthorized ua, User hostUser, Human targetHuman) throws IntegrationException, AuthorizationException, 
            BObStatusException{
        UserIntegrator ui = getUserIntegrator();
        if(hostUser != null && targetHuman != null && ua != null){
            // keep a handle to the outgoing human
            Human oldHuman = hostUser.getHuman();
            hostUser.setUserHuman(targetHuman);
            ui.updateUser_humanLink(hostUser);
            getSystemMuniCacheManager().getCacheUser().invalidate(hostUser.getCacheKey());

            // get the DB's copy for validation
            User updatedUser = user_getUser(hostUser.getUserID());

            MessageBuilderParams mbp = new MessageBuilderParams(updatedUser.getNotes());

            mbp.setUser(ua);
            mbp.setHeader("PERSON LINK UPDATED");

            StringBuilder sb = new StringBuilder();
            sb.append("Logging user updated this user's linked human from  ");
            if(oldHuman != null){
                sb.append(oldHuman.getName());
                sb.append(" (ID:");
                sb.append(oldHuman.getHumanID());
                sb.append(")");
            } else {
                sb.append("NULL HUMAN");
            }

            sb.append(" to ");
            sb.append(updatedUser.getHuman().getName());
            sb.append(" (ID:");
            sb.append(updatedUser.getHuman().getHumanID());
            sb.append(")");

            mbp.setNewMessageContent(sb.toString());
            user_appendNoteToUser(hostUser, mbp);
                
                
        } else {
            throw new BObStatusException("Cannot update user with null user, no human, or a humanID of 0");
            
        }
    }
    
    /**
     * Updates the User's Person and username only.Note -- does not update the password which is updated separately
     * @param ua
     * @param u
     * @param homeTarget
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public void user_updateUserHomeMuni(UserAuthorized ua, User u, Municipality homeTarget) throws IntegrationException, AuthorizationException, 
            BObStatusException{
        UserIntegrator ui = getUserIntegrator();
        if(u != null && u.getHomeMuniID() != 0 && homeTarget != null){
            int muniIDOld = u.getHomeMuniID();
            
            u.setHomeMuniID(homeTarget.getMuniCode());
            
            ui.updateUser_homeMuni(u);
            getSystemMuniCacheManager().getCacheUser().invalidate(u.getCacheKey());
                
            // get the DB's copy for validation
            User updatedUser = user_getUser(u.getUserID());

            MessageBuilderParams mbp = new MessageBuilderParams(updatedUser.getNotes());

            mbp.setUser(ua);
            mbp.setHeader("HOME MUNI UPDATED");

            StringBuilder sb = new StringBuilder();
            sb.append("Logging user updated this user's home municipality from  ");
            sb.append(muniIDOld);
            sb.append(" to ");
            sb.append(updatedUser.getHomeMuniID());

            mbp.setNewMessageContent(sb.toString());
            user_appendNoteToUser(updatedUser, mbp);
            getSystemMuniCacheManager().flush(ua);
            
                
        } else {
            throw new BObStatusException("Cannot update user's home muni with with null user or home muni id 0");
            
        }
    }
    
    /**
     * Checks for password match with String#equals()
     * @param psswd
     * @param passwdRepeatEntry
     * @param usr 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException  for empty inputs or non-matching passwords
     */
    public void user_checkPasswordPairForCompliance(String psswd, String passwdRepeatEntry) throws AuthorizationException{
        if(psswd == null || passwdRepeatEntry == null){
            throw new AuthorizationException("One or both user password proposals are null");
        }
        if(!psswd.equals(passwdRepeatEntry)){
            throw new AuthorizationException("Passwords submitted are not the same");
        }
        
    }
    
    
    /**
     * Logic intermediary for forcing password reset on next login
     
     * 
     * @param ua the user forcing the reset
     * @param u to have a forced password reset TS written to DB
     * @throws IntegrationException 
     */
    public void user_forcePasswordReset(UserAuthorized ua, User u) throws IntegrationException, BObStatusException, AuthorizationException{
        if(ua == null || u == null){
            throw new BObStatusException("Cannot force password reset with null resetter or resettee");
        }
        
        if(ua.getKeyCard().isHasSysAdminPermissions()){

            UserIntegrator ui = getUserIntegrator();
            u.setForcedPasswordResetByUserID(ua.getUserID());

            ui.updateUser_forcePasswordReset(u);

            // get the DB's copy for validation
            User updatedUser = user_getUser(u.getUserID());

            MessageBuilderParams mbp = new MessageBuilderParams(updatedUser.getNotes());

            mbp.setUser(ua);
            mbp.setHeader("PASSWORD RESET FORCED");

            StringBuilder sb = new StringBuilder();
            sb.append("Logging user has stamped this user for forced password reset trigger: ");
            if(updatedUser.getForcePasswordResetTS() != null){
                sb.append(updatedUser.getForcePasswordResetTS().toString());
            } else {
                sb.append(" *** ERROR FORCING PASSWORD RESET *** ");
            }
            mbp.setNewMessageContent(sb.toString());
            user_appendNoteToUser(updatedUser, mbp);
        } else {
            throw new AuthorizationException("Must have sys admin or better to force resets");
        }
        
    }
    
    
    
    /**
     * *** SECURITY CRITICAL METHOD ***
     * Writes a new password to the DB
     * 
     * @param ua writing the new password
     * @param u whose password is being reset
     * @param pw cleartext password for hashing and writing
     * @throws IntegrationException
     * @throws AuthorizationException for passwords equal or below MIN_PSSWD_LENGTH
     */
    public void user_updateUserPassword_SECURITYCRITICAL(UserAuthorized ua, User u, String pw) throws IntegrationException, AuthorizationException, BObStatusException{
         if(ua == null || u == null || pw == null){
            throw new BObStatusException("Cannot update password with null updator, updatee, or new password String");
        }
        UserIntegrator ui = getUserIntegrator();
        if(pw.length() >= MIN_PSSWD_LENGTH){
            ui.setUserPassword_SECURITYCRITICAL(u, pw);
            getSystemMuniCacheManager().getCacheUser().invalidate(u.getCacheKey());
            // get the DB's copy for validation
            User updatedUser = user_getUser(u.getUserID());

            MessageBuilderParams mbp = new MessageBuilderParams(updatedUser.getNotes());

            mbp.setUser(ua);
            mbp.setHeader("PASSWORD RESET COMPLETED");
            StringBuilder sb = new StringBuilder();
            sb.append("Logging user has written a new password for this user: ");
            
            mbp.setNewMessageContent(sb.toString());
            user_appendNoteToUser(updatedUser, mbp);
            
            // check if this was a forced reset
            if(u.getForcePasswordResetTS() != null){
                ui.updateUserWipeForcePasswordReset(u);
                // get the DB's copy for validation
                updatedUser = user_getUser(u.getUserID());

                mbp = new MessageBuilderParams(updatedUser.getNotes());

                mbp.setUser(ua);
                mbp.setHeader("FORCED PASSWORD RESET WIPED");
                sb = new StringBuilder();
                sb.append("This user's password was forced to change, and was, in fact, changed, so the forced reset timestamp and forcer ID were both wiped");

                mbp.setNewMessageContent(sb.toString());
                user_appendNoteToUser(updatedUser, mbp);

            }

        } else {
            throw new AuthorizationException("Password must be at least " + MIN_PSSWD_LENGTH + " characters");
        }
    } 
    
    /**
     * Utility method for attaching notes
     * Given an arbitrary MBP instance;
     * 
     * @param u to whom you want the MBP's notes appended
     * @param mbp with all fields ready to ship to SystemCoordinator
     * @throws IntegrationException 
     */
    public void user_appendNoteToUser(User u, MessageBuilderParams mbp) throws IntegrationException{
        SystemCoordinator sc = getSystemCoordinator();
        UserIntegrator ui = getUserIntegrator();
        u.setNotes(sc.appendNoteBlock(mbp));
        ui.updateUser_notes(u);
        getSystemMuniCacheManager().getCacheUser().invalidate(u.getCacheKey());
        
    }
    
    
    /**
     * Logic container for checking permissibility of User record deactivation.
     * Key role is to prohibit any UI-based deactivation of developer users :)
     * @param ua requesting user
     * @param ufcToDeac 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void user_deactivateUser(UserAuthorized ua, UserAuthorizedForConfig ufcToDeac) throws AuthorizationException, IntegrationException{
        System.out.println("UserCoordinator.user_deactivateUser");
        UserIntegrator ui = getUserIntegrator();
        if(ufcToDeac != null && ua != null){
            if(ua.getUserID() == ufcToDeac.getUserID()){
                throw new AuthorizationException("Users cannot deactivate themselves!");
            }
            // users can only deactivate users whose highest UMAP rank is less than theirs, or have sys admin rank
            if(ua.getMyCredential().isHasSysAdminPermissions()){
    
                ufcToDeac.setDeactivatedByUserID(ua.getUserID());
                ufcToDeac.setDeactivatedTS(LocalDateTime.now());
                
                ui.updateUser_deactivate(ufcToDeac);
                getSystemMuniCacheManager().getCacheUser().invalidate(ufcToDeac.getCacheKey());
                
                MessageBuilderParams mbp = new MessageBuilderParams(ufcToDeac.getNotes());
                mbp.setUser(ua);
                mbp.setHeader("Deactivated");
                mbp.setNewMessageContent("Logger deactivated this user.");
                user_appendNoteToUser(ua, mbp);
                
            } else {
                throw new AuthorizationException("Users must have sys admin permissions");
            }
        } else {
            throw new AuthorizationException("Cannot deactivate a null user!");
        }
    }
       
} // close class
