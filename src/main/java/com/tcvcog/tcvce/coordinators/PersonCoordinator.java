/*
 * Copyright (C) 2017 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.coordinators;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.BlobException;
import com.tcvcog.tcvce.domain.DatabaseFetchRuntimeException;
import com.tcvcog.tcvce.domain.EventException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.domain.SearchException;
import com.tcvcog.tcvce.entities.CEActionRequest;
import com.tcvcog.tcvce.entities.CECase;
import com.tcvcog.tcvce.entities.CECaseDataHeavy;
import com.tcvcog.tcvce.entities.Citation;
import com.tcvcog.tcvce.entities.CitationDocketRecord;
import com.tcvcog.tcvce.entities.ContactEmail;
import com.tcvcog.tcvce.entities.ContactPhone;
import com.tcvcog.tcvce.entities.ContactPhoneType;
import com.tcvcog.tcvce.entities.Credential;
import com.tcvcog.tcvce.entities.EventCnF;
import com.tcvcog.tcvce.entities.Human;
import com.tcvcog.tcvce.entities.HumanAlias;
import com.tcvcog.tcvce.entities.HumanAliasRoleEnum;
import com.tcvcog.tcvce.entities.HumanLink;
import com.tcvcog.tcvce.entities.HumanLinkParentObjectInfoHeavy;
import com.tcvcog.tcvce.entities.IFace_addressListHolder;
import com.tcvcog.tcvce.entities.IFace_humanListHolder;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.Person;
import com.tcvcog.tcvce.entities.PersonLinkHeavy;
import com.tcvcog.tcvce.entities.PersonType;
import com.tcvcog.tcvce.entities.PersonWithChanges;
import com.tcvcog.tcvce.entities.User;
import com.tcvcog.tcvce.entities.UserAuthorized;
import com.tcvcog.tcvce.entities.search.QueryCECase;
import com.tcvcog.tcvce.entities.search.QueryCECaseEnum;
import com.tcvcog.tcvce.integration.PersonIntegrator;
import com.tcvcog.tcvce.integration.PropertyIntegrator;
import com.tcvcog.tcvce.integration.SystemIntegrator;
import com.tcvcog.tcvce.util.Constants;
import com.tcvcog.tcvce.util.MessageBuilderParams;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.tcvcog.tcvce.entities.LinkedObjectFamilyEnum;
import com.tcvcog.tcvce.entities.LinkedObjectRole;
import com.tcvcog.tcvce.entities.LinkedObjectSchemaEnum;
import com.tcvcog.tcvce.entities.MailingAddress;
import com.tcvcog.tcvce.entities.MailingAddressLink;
import com.tcvcog.tcvce.entities.MergeMatchLevelEnum;
import com.tcvcog.tcvce.entities.MunicipalityDataHeavy;
import com.tcvcog.tcvce.entities.NoticeOfViolation;
import com.tcvcog.tcvce.entities.Parcel;
import com.tcvcog.tcvce.entities.PersonMergeLog;
import com.tcvcog.tcvce.entities.PersonMergeRequest;
import com.tcvcog.tcvce.entities.PropertyDataHeavy;
import com.tcvcog.tcvce.entities.PropertyUnit;
import com.tcvcog.tcvce.entities.PropertyUnitDataHeavy;
import com.tcvcog.tcvce.entities.occupancy.OccPeriod;
import com.tcvcog.tcvce.entities.occupancy.OccPeriodDataHeavy;
import com.tcvcog.tcvce.entities.occupancy.OccPermit;
import com.tcvcog.tcvce.entities.occupancy.OccPermitApplication;
import com.tcvcog.tcvce.integration.PersonCacheManager;
import com.tcvcog.tcvce.integration.UserIntegrator;
import jakarta.faces.application.FacesMessage;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 * The master controller class for Humans and their Java incarnation called
 * Person, which is a human with a list of its Addresses, Emails, and Phone numbers
 * 
 * @author ellen bascomb of apt 31y
 */
public class PersonCoordinator extends BackingBeanUtils implements Serializable{

    private final PersonType[] personTypes;
    
    
      /**
     * Permissions gate for updating a person, which requires muni staff or greater
     * only; As of March 2024 person characteristics aren't checked her
     * @param ua
     * @return 
     */
    public boolean permissionCheckpointUpdatePerson(UserAuthorized ua){
        if(ua == null){
            return false;
        }
        if(ua.getKeyCard().isHasMuniStaffPermissions()){
            return true;
        }

        return false;
    }
    
    
    
    
    /**
     * Creates a new instance of PersonCoordinator
     */
    public PersonCoordinator() {
        personTypes = PersonType.values();
    }
    
    /**
     * Primary retrieval method for extracting Humans from the DB
     * Remember a Human has a name, DOB, and stuff like that, but NO
     * Addresses, Emails, or Phone numbers--get a Person instead
     * 
     * @param humanID
     * @return the Human object 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public Human getHuman(int humanID) throws IntegrationException{
        PersonIntegrator pi = getPersonIntegrator();
        if(humanID == 0){
            return null;
        }
        Human h = pi.getHuman(humanID);
        h = configureHuman(h);
         return h;
    }
    
    /**
     * Creates a HumanLink from a given Human PRIOR to being
     * written in the database. Skeleton objects are those which
     * have an ID of 0
     * @param hu to turn into a HumanLink
     * @return the given Human wrapped in a Link. Null if input is null.
     */
    public HumanLink createHumanLinkSkeleton(Human hu){
        if(hu != null){
            return new HumanLink(new Person(hu));
        } else {
            return null;
        }
    }
    
    
    /**
     * Access point for retrieving and injecting a list of Human objects
     * associated with a Business Object
     * 
     * @param hlh BOB implementing this interface
     * @return The BOB with Humans and their link metatadata already assembled.
     * Note the caller will probably want to cast back to the original type
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public List<HumanLink> getHumanLinkList(IFace_humanListHolder hlh) throws IntegrationException, BObStatusException{
        PersonIntegrator pi = getPersonIntegrator();
        List<HumanLink> hll = null;
        
        if(hlh != null){
            hll = pi.getHumanLinks(hlh);
        }
        Collections.sort(hll);
        return hll;
    }
    
    
    
    /**
     * Special method for extracting a subset of human links by link ID from 
     * the humanoccperiod table; Used by the dynamic occ permit facility to 
     * save the in-process linking of persons to permits prior to
     * permit finalization
     * 
     * @param links
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public List<HumanLink> getHumanOccPermitLinkList(Integer[] links) throws IntegrationException, BObStatusException{
        PersonIntegrator pi = getPersonIntegrator();
        OccupancyCoordinator oc = getOccupancyCoordinator();
        
        if(links == null || links.length == 0){
            return new ArrayList<>();
        }
        List<HumanLink> hll = new ArrayList<>();
        for(int i: links){
            hll.add(pi.getHumanLink(i , LinkedObjectSchemaEnum.OccPermitHuman));
        }
        Collections.sort(hll);
        return hll;
    }
    
    /**
     * Convenience method for getting an Integer[] from a list of HumanLinks;
     * @param linkList
     * @return 
     */
    public Integer[] getHumanLinkIntegerArrayFromLinkList(List<HumanLink> linkList){
        Integer[] ids = null;
        if(linkList != null && !linkList.isEmpty()){
            ids = new Integer[linkList.size()];
            for(int i = 0; i < linkList.size(); i++){
                ids[i] = linkList.get(i).getLinkID();
            }
        }
         return ids;
    }
    
    /**
     * Refreshes the given human link list
     * @param linkList
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public List<HumanLink> refreshHumanLinkList(List<HumanLink> linkList) throws BObStatusException, IntegrationException{
        List<HumanLink> freshList = new ArrayList<>();
        if(linkList != null && !linkList.isEmpty()){
            for(HumanLink hl: linkList){
                freshList.add(getHumanLink(hl.getLinkID(), hl.getLinkedObjectSchemaEnum()));
            }
        }
        return freshList;
    }
    
    /**
     * retrieves a single human link from the db.
     * Human Link IDs are not universal, so to get one from the DB requires the linking schema
     * which contains the actual DB table from which to SELECT the ID and build the object
     * from the Referenced records, such as a human and a parcel
     * @param linkID
     * @param lose
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
   public HumanLink getHumanLink(int linkID, LinkedObjectSchemaEnum lose) throws BObStatusException, IntegrationException{
       if(linkID == 0 || lose == null){
           throw new BObStatusException("Cannot get link without nonzero ID and non null link enum");
       }
       
       System.out.println("PersonCoordinator.getHumanLink: linkid: " + linkID + " | schema: " + lose.getRoleSChemaTypeString());
       PersonIntegrator pi = getPersonIntegrator();
       // flush our person caches
       
       HumanLink hl = pi.getHumanLink(linkID, lose);
       
       if(hl == null){
           throw new IntegrationException("PersonCoordinator.getHumanLink: could not fetch a human link; check your schema");
       }
       
       return hl;
   }
   
   /**
    * Based on the inputted HumanLink's role embodied by its schema the parent
    * object is instantiated and returned
    * 
    * @param hl
     * @param ua
    * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.SearchException 
     * @throws com.tcvcog.tcvce.domain.BlobException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
    */
   public IFace_humanListHolder getParentObjectOfHumanLink(HumanLink hl, UserAuthorized ua) 
           throws BObStatusException, IntegrationException, SearchException, BlobException, AuthorizationException, EventException{
        switch (hl.getLinkedObjectRole().getSchema()) {
            case CEACtionRequestHuman -> {
                CaseCoordinator cc = getCaseCoordinator();
                return cc.cear_getCEActionRequest(hl.getParentObjectID());
            }
            
            case CECaseHuman -> {
                CaseCoordinator cc = getCaseCoordinator();
                return cc.cecase_assembleCECaseDataHeavy(cc.cecase_getCECase(hl.getParentObjectID(), ua), ua);
            }
            
            case CitationHuman -> {
                CaseCoordinator cc = getCaseCoordinator();
                return cc.citation_getCitation(hl.getParentObjectID());
            }
            
            case CitationDocketHuman -> {
                CaseCoordinator cc = getCaseCoordinator();
                return cc.citation_getCitationDocketRecord(hl.getParentObjectID());
            }
            
            case EventHuman -> {
                EventCoordinator ec = getEventCoordinator();
                return ec.getEvent(hl.getParentObjectID());
            }
            
            case MuniHuman -> {
                MunicipalityCoordinator mc = getMuniCoordinator();
                return mc.assembleMuniDataHeavy(mc.getMuni(hl.getParentObjectID()), ua);
            }
            
            case OccPeriodHuman -> {
                OccupancyCoordinator oc = getOccupancyCoordinator();
                return oc.assembleOccPeriodDataHeavy(oc.getOccPeriod(hl.getParentObjectID(), ua), ua);
            }
            
            case ParcelHuman -> {
                PropertyCoordinator pc = getPropertyCoordinator();                
                return pc.assemblePropertyDataHeavy(pc.getProperty(hl.getParentObjectID()), ua); 
            }
            
            case ParcelUnitHuman -> {
                PropertyCoordinator pc = getPropertyCoordinator();
                return pc.getPropertyUnitDataHeavy(pc.getPropertyUnit(hl.getParentObjectID()), ua);
            }
            
            case OccPermitHuman -> {
                OccupancyCoordinator occcoor = getOccupancyCoordinator();
                return occcoor.getOccPermit(hl.getParentObjectID(), ua);
            }
            
           
            default -> throw new BObStatusException("HumanLink parent incarnation not yet supported");
        }
   }
   
   /**
    * Queries to get the parent object for the given HumanLink to determine cross-muni viewing
    * and build a meaningful descriptive string that includes 
    * fields custom to each parent class, such as the case name, or event category/type
    * @param hl
     * @param ua
    * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
    */
   public HumanLinkParentObjectInfoHeavy assembleHumanLinkParentObjectInfoHeavy(HumanLink hl, UserAuthorized ua) throws BObStatusException{
       if(hl == null || hl.getLinkedObjectSchemaEnum() == null){
           throw new BObStatusException("cannot get parent info heavy human link with null link or null schema enum");
       }
       HumanLinkParentObjectInfoHeavy hlpoih = new HumanLinkParentObjectInfoHeavy(hl);
        try {
            IFace_humanListHolder hlh = getParentObjectOfHumanLink(hl, ua);
            // set our two flags on the subtype: a description and a flag for cross muni viewing
            hlpoih.setParentObjectDescription(hlh.getDescriptionString());
            // our parent object MIGHT NOT BE SET 
            hlpoih = configureLinkParentInfoAndCrossMuniStatus(hlpoih, hlh, ua);
        } catch (IntegrationException | AuthorizationException | BObStatusException | BlobException | EventException | SearchException ex) {
            System.out.println(ex);
            throw new BObStatusException("PersonCoordinator.assembleHumanLinkParentObjectInfoHeavy | Could not get a human list holder from human link due to any number of one gazillion exceptions");
        } 
       return hlpoih;
   } 
   
   /**
    * Utility method for looping over a list of HumanLink objects and getting an InfoHeavy version
    * @param hll
     * @param ua
    * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
    */
   public List<HumanLinkParentObjectInfoHeavy> getHumanLinkParentObjectInfoHeavyList(List<HumanLink> hll, UserAuthorized ua) 
           throws BObStatusException{
       List<HumanLinkParentObjectInfoHeavy> hlpoihl = new ArrayList<>();
       if(hll != null && !hll.isEmpty()){
           for(HumanLink hl: hll){
               // skip links without navigable parents, like human-human links--they'll be managed separately
               if(!hl.getLinkedObjectRole().getSchema().isHAS_NAVIGABLE_PARENT()){
                   continue;
               }
               // don't add to the list of we don't get lovely parent info, but log instead
               HumanLinkParentObjectInfoHeavy hlpoih = assembleHumanLinkParentObjectInfoHeavy(hl, ua); 
               if(hlpoih.getMuiniParent() != null){
                    hlpoihl.add(hlpoih);
               } else{
                   StringBuilder sb = new StringBuilder("PersonCoordinator.getHumanLinkParentObjectInfoHeavyList | failed to generate a muni parent for human with ID: ");
                   sb.append(hl.getHumanID());
                   sb.append(" | With LINK ID: ");
                   sb.append(hl.getLinkID());
                   sb.append(" | And schema enum: ");
                   sb.append(hl.getLinkedObjectRole().getSchema().getRoleSChemaTypeString());
                   System.out.println(sb.toString());
               }
           }
       }
       return hlpoihl;
   }
   
   /**
    * Casts the HLH into its native type and figures out its muni and compares that to the UA's
    * credentialed muni. Also sets the  muni parent member on the info heavy human links
    * 
    * @param hlh Whose parent
    * @param ua
    * @return the configured object for functional designs
    */
   private HumanLinkParentObjectInfoHeavy configureLinkParentInfoAndCrossMuniStatus(HumanLinkParentObjectInfoHeavy hlpioh, IFace_humanListHolder hlh, UserAuthorized ua) 
           throws BObStatusException, IntegrationException, BlobException, EventException, SearchException, AuthorizationException{
       if(hlpioh == null || hlh == null || ua == null){
           throw new BObStatusException("Cannot determine muni external status with null list holder or user");
       }
       int sessionMuniCode = ua.getKeyCard().getGoverningAuthPeriod().getMuni().getMuniCode();
       
       Municipality muniParent = null;
       // Action Requests
       if(hlh instanceof CEActionRequest cear){
       // CE Case
           muniParent = cear.getMuni();
       } else if(hlh instanceof CECaseDataHeavy csedh){
           muniParent = csedh.getMuni();
       // Citation
       } else if(hlh instanceof Citation cit){
           CaseCoordinator cc = getCaseCoordinator();
           PropertyCoordinator pc = getPropertyCoordinator();
           muniParent = pc.getProperty(cc.cecase_getCECase(cit.getCecaseID(), ua).getParcelKey()).getMuni();
       // Docket
       } else if(hlh instanceof CitationDocketRecord cdr){
           CaseCoordinator cc = getCaseCoordinator();
           PropertyCoordinator pc = getPropertyCoordinator();
           muniParent = pc.getProperty(cc.cecase_getCECase(cc.citation_getCitation(cdr.getCitationID()).getCecaseID(), ua).getParcelKey()).getMuni();
       // Event
       } else if(hlh instanceof EventCnF ev){
           PropertyCoordinator pc = getPropertyCoordinator();
           EventCoordinator ec = getEventCoordinator();
           muniParent = pc.getProperty(ec.getEventParcelKey(ev)).getMuni();
       // Muni
       } else if(hlh instanceof MunicipalityDataHeavy mdh){
           muniParent = (Municipality) mdh;
       // Property
       } else if(hlh instanceof PropertyDataHeavy pdh){
           muniParent = pdh.getMuni();
       // Property unit
       } else if(hlh instanceof PropertyUnitDataHeavy pudh){
           PropertyCoordinator pc = getPropertyCoordinator();
           muniParent = pc.getProperty(pudh.getParcelKey()).getMuni();
        // Occ Period
       } else if(hlh instanceof OccPeriodDataHeavy opdh){
           muniParent = opdh.getGoverningMunicipality();
       } else if(hlh instanceof OccPermit permit){
           OccupancyCoordinator oc = getOccupancyCoordinator();
           muniParent = oc.getMuniParentOfOccPermit(permit, ua);
       }
       hlpioh.setMuiniParent(muniParent);
       if(hlpioh.getMuiniParent() != null){
            hlpioh.setExternalToSessionMuni(sessionMuniCode != hlpioh.getMuiniParent().getMuniCode());
       } 
       // removed exception from getting thrown if the  munui parent is null. Figure out how to make the
       // caller give a meaningful message
       return hlpioh;
   }
    
    /**
     * Grand staircase entrance for connecting a human holder to a human
     * @param hlh
     * @param hlink if all you have is a human (or Person subclass), use the 
     * convenience method createHumanLink method
     * @param ua the user doing the linking
     * @return the link ID for the freshly inserted link
     * @throws BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public int insertHumanLink(IFace_humanListHolder hlh, HumanLink hlink, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        PersonIntegrator pi = getPersonIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        if(hlh == null || hlink == null || ua == null){
            throw new BObStatusException("Cannot link human with null human or human holder or null user");
        }
        if(!permissionCheckpointUpdatePerson(ua)){
            throw new AuthorizationException("User lacks permissions to insert/update person related data");
        }
        
        hlink.setLinkCreatedByUserID(ua.getUserID());
        hlink.setLinkLastUpdatedByUserID(ua.getUserID());
        if(hlink.getLinkSource() == null){
            hlink.setLinkSource(sc.getBObSource(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                                .getString("bobSourceHumanLinkDefault"))));
        }
        if(hlink.getLinkedObjectRole() == null){
            determineAndSetDefaultLinkedObjectRole(hlh, hlink);
        }

        return pi.insertHumanLink(hlh, hlink);
    }
    
       /**
     * Convenience method for calling insertHumanLink(...) on all our links
     * 
     *
     * @param holder
     * @param hlist
     * @param ua 
     * @return the new link IDs 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public Integer[] insertHumanLinks(IFace_humanListHolder holder, List<HumanLink> hlist, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        Integer[] freshIDArray = null;
        if(hlist != null && !hlist.isEmpty()){
            freshIDArray = new Integer[hlist.size()];
            for(int i = 0; i < hlist.size(); i++){
                HumanLink hl = hlist.get(i);
                freshIDArray[i] = insertHumanLink(holder, hl, ua);
            }
        }
        return freshIDArray;
    }
    
    
    
    
    /**
     * Connects the given NOV's recipeint to the given case.Basic logic 
     * As of July 2023, NoticeOfViolation held a single human ID to track their recipient
     * and did not use the standard human linking infrastructure that makes use of a
     * separate linking table with a link object role.Updated in March 2024 to no re-link a person as an NOV recipient a second (or more) times 
     * 
     * 
     * @param cse
     * @param nov
     * @param ua
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void linkNOVHumanToParentCECase(CECaseDataHeavy cse, NoticeOfViolation nov, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        if(cse == null || nov == null){
            throw new BObStatusException("Cannot link NOV to case with null nov or case");
        }
        if(nov.getRecipient() == null){
            throw new BObStatusException("Cannot link NOV recipeint to case with null NOV recipient");
        }
        
        SystemCoordinator sc = getSystemCoordinator();
        LinkedObjectRole novRecipientLOR = sc.getLinkedObjectRole(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                            .getString("nov_recipient_lorid")));
        
        // look for duplicates, and if found, don't do any linking 
        if(cse.gethumanLinkList() != null && !cse.gethumanLinkList().isEmpty()){
            for(HumanLink hl: cse.gethumanLinkList()){
                if(hl.getHumanID() == nov.getRecipient().getHumanID() && hl.getLinkedObjectRole().getRoleID() == novRecipientLOR.getRoleID()){
                    System.out.println("NoticeOfVioolationBB.linkNOVHumanToParentCECase | Found duplicate person link role, skipping linking process");
                    return;
                }
            }
        }
        
        HumanLink hl = createHumanLinkSkeleton(nov.getRecipient());
        hl.setLinkRole(novRecipientLOR);
        insertHumanLink(cse, hl, ua);
    }
    
    /**
     * Logic block for human ink updates
     * @param hl
     * @param hlh
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void updateHumanLink(IFace_humanListHolder hlh, HumanLink hl, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        if(hl == null || ua == null){
            throw new BObStatusException("Cannot update human link with null link or user");
        }
        if(!permissionCheckpointUpdatePerson(ua)){
            throw new AuthorizationException("User lacks permissions to insert/update person related data");
        }
        
        PersonIntegrator pi = getPersonIntegrator();
        hl.setLinkLastUpdatedByUserID(ua.getUserID());
        pi.updateHumanLink(hlh, hl);
        getPersonCacheManager().flush(hl);
        
    }
    
    /**
     * Uses instanceof to check what type of human holder we have and sets the default
     * role code so the DB record is complete.
     * @param hlink
     * @return 
     */
    private HumanLink determineAndSetDefaultLinkedObjectRole(IFace_humanListHolder hlh, HumanLink hlink) throws BObStatusException, IntegrationException{
        if(hlh == null || hlink == null){
            throw new BObStatusException("Cannot determine default linked object role with null list holder or human linke");
        }
        
        SystemIntegrator si = getSystemIntegrator();
        
        if(hlh instanceof OccPermitApplication){
            hlink.setLinkRole(si.getLinkedObjectRole(
                    Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                            .getString("default_linkedobjectrole_human_occapp"))));
            
        } else if (hlh instanceof CECase){
            hlink.setLinkRole(si.getLinkedObjectRole(
                    Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                            .getString("default_linkedobjectrole_human_cecase"))));
        } else if (hlh instanceof OccPeriod){
            hlink.setLinkRole(si.getLinkedObjectRole(
                    Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                            .getString("default_linkedobjectrole_human_occperiod"))));
            
        } else if (hlh instanceof Parcel){
            hlink.setLinkRole(si.getLinkedObjectRole(
                    Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                            .getString("default_linkedobjectrole_human_parcel"))));
            
        } else if (hlh instanceof PropertyUnit){
            hlink.setLinkRole(si.getLinkedObjectRole(
                    Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                            .getString("default_linkedobjectrole_human_parcelunit"))));
            
        } else if (hlh instanceof Citation){
            hlink.setLinkRole(si.getLinkedObjectRole(
                    Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                            .getString("default_linkedobjectrole_human_citation"))));
            
        } else if (hlh instanceof CitationDocketRecord){
            hlink.setLinkRole(si.getLinkedObjectRole(
                    Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                            .getString("default_linkedobjectrole_human_docket"))));
            
        } else if (hlh instanceof EventCnF){
            hlink.setLinkRole(si.getLinkedObjectRole(
                    Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                            .getString("default_linkedobjectrole_human_event"))));
            
        } else if (hlh instanceof Municipality){
            hlink.setLinkRole(si.getLinkedObjectRole(
                    Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                            .getString("default_linkedobjectrole_human_muni"))));
            
        } else if (hlh instanceof MailingAddress){
            hlink.setLinkRole(si.getLinkedObjectRole(
                    Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                            .getString("default_linkedobjectrole_human_mailing"))));
            
        } else {
            throw new BObStatusException("Cannot determine a default linked object role for given list holder");
        }
        
        return hlink;
        
    }
    
    
    /**
     * Grand staircase entrance for deactivating a human holder and one of its humans
     * @param hl to deactivate
     * @param ua the user doing the deactivating
     * @throws BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void deactivateHumanLink(HumanLink hl, UserAuthorized ua) 
            throws BObStatusException, IntegrationException, AuthorizationException{
        PersonIntegrator pi = getPersonIntegrator();
        
        if(ua == null || hl == null){
            throw new BObStatusException("Cannot deactivate human link with null user or link");
        }
        if(!permissionCheckpointUpdatePerson(ua)){
            throw new AuthorizationException("User lacks permissions to insert/update person related data");
        }
        hl.setLinkLastUpdatedByUserID(ua.getUserID());
        hl.setLinkDeactivatedByUserID(ua.getUserID());
        pi.deactivateHumanLink(hl);
        getPersonCacheManager().flush(hl);
    }
    
    /**
     * Special pathway for deleting human links on occ permits which are temporarily
     * stored during dynamic field configuration
     * 
     * @param permit
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void deleteOccPermitHumanLinks(OccPermit permit) throws IntegrationException{
       PersonIntegrator pi = getPersonIntegrator();
       
       pi.deleteOccPermitHumanLink(permit);
       
    }
    
    /**
     * Convenience method for calling deactivateHumanLink(...) on all our links
     *
     * @param hlist
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void deactivateHumanLinks(List<HumanLink> hlist, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        if(!permissionCheckpointUpdatePerson(ua)){
            throw new AuthorizationException("User lacks permissions to insert/update person related data");
        }
        if(hlist != null && !hlist.isEmpty()){
            for(HumanLink hl: hlist){
                deactivateHumanLink(hl, ua);
            }
        }
    }
    
    /**
     * Takes in a human link and builds the note infrastructure and sends
     * to be integrated
     * @param hl without note appended, just the old notes in the notes field
     * @param noteToAppend the string value of the note to append
     * @param u doing the noting
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public void appendNoteToHumanLink(HumanLink hl, String noteToAppend, UserAuthorized u) 
            throws BObStatusException, IntegrationException{
        SystemCoordinator sc = getSystemCoordinator();
        PersonIntegrator pi =getPersonIntegrator();
        MessageBuilderParams mbp = new MessageBuilderParams();
        if(hl  != null){
            mbp.setExistingContent(hl.getLinkNotes());
            mbp.setUser(u);
            mbp.setNewMessageContent(noteToAppend);
            mbp.setHeader("LINK NOTE");
            hl.setLinkNotes(sc.appendNoteBlock(mbp));
            pi.updateHumanLinkNotes(hl);
            getPersonCacheManager().flush(hl);
        }
    }
    
    
    /**
     * Logic container for configuring a Human object =
     * @param hum
     * @return 
     */
    private Human configureHuman(Human hum){
        // config logic for Human's go heres
        return hum;
    }
    
    /**
     * Access point for getting a person 
 Convenience method for calling getPersonByHumanID that takes 
 a human object
     * @param humanID
     * @return
     * @throws IntegrationException
     * @throws BObStatusException 
     */
    public Person getPersonByHumanID(int humanID) throws IntegrationException, BObStatusException{
        Human h = getHuman(humanID);
        Person p = getPerson(h);
        return p;
        
    }
    
    /**
     * A Person is a Human with lists of MailingAddresses, phone numbers, and emails
     * @param hum
     * @return the fully-baked human (i.e. a person)
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public Person getPerson(Human hum) throws IntegrationException, BObStatusException{
        if(hum == null){
            return null;
        }
        Person p = new Person(hum);
        p = (Person) configurePerson(p);
        return p;
    }
    
    /**
     * Utility method for building a list of Persons given a list of IDs
     * @param humanIDList
     * @return
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public List<Person> getPersonList(List<Integer> humanIDList) throws IntegrationException, BObStatusException{
        List<Person> pList = new ArrayList<>();
        if(humanIDList != null && !humanIDList.isEmpty()){
            for(Integer i: humanIDList){
                pList.add(getPerson(getHuman(i)));
            }
        }
        return pList;
    }
    
    
    /**
     * Utility method for iterating over a list of HumanLinks and getting
     * Persons and shoving them all in a list
     * 
     * @param humanLinkList     
     * @return return a list, possibly with Person objs in it!
     * @throws IntegrationException 
     */
    public List<Person> getPersonListFromHumanLinkList(List<HumanLink> humanLinkList) throws IntegrationException, BObStatusException{
        List<Person> pl = new ArrayList<>();
        if(humanLinkList != null && !humanLinkList.isEmpty()){
            for(HumanLink hl: humanLinkList){
                pl.add(getPerson(hl));
            }
        }
        return pl;
        
    }
    
  
    /**
     * Builds a data heavy version of a person
     * Implements logic depending on if the person is a skeleton or not
     * @param pers which should be refreshed by caller; this method doesn't reacquire the person
     * @return the fully baked person with a link list in its belly
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public PersonLinkHeavy assemblePersonLinkHeavy(Person pers) throws IntegrationException, BObStatusException{
        PersonLinkHeavy persLinkHeavy = null;
        PersonIntegrator pi = getPersonIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        if(pers == null){
            throw new BObStatusException("Cannot assemble person link heavy with null input person");
        }
        if(pers.getPersonID() == 0){
            return new PersonLinkHeavy(pers);
        }
        persLinkHeavy = new PersonLinkHeavy(pers);
        List<HumanLink> hnhll = new ArrayList<>();
        List<LinkedObjectSchemaEnum> schemaList = sc.assembleLinkedObjectSchemaEnumListByFamily(LinkedObjectFamilyEnum.HUMAN);
        if(schemaList != null && !schemaList.isEmpty()){
            // iterate over all the enums that describe possible human links
            // and get their linked object IDs, injecting them to the master list
            // for display in the UI
            // This is the unqiue listing of a given Human's outoing Human-OtherNonHumanObject links
            // for display in the person profile only
            for(LinkedObjectSchemaEnum lose: schemaList){
                if(lose.isACTIVELINK()){
//                    System.out.println("PersonCoordinator.assemblePersonLinkHeavy | Checking enum " + lose.getLinkingTableName());
                    List<HumanLink> hllinternal = pi.getHumanLinksByLinkedObjectEnum(lose, persLinkHeavy);
//                    System.out.println("PersonCoordinator.assemblePersonLinkHeavy | Links:  " + hllinternal.size());
                    hnhll.addAll(hllinternal);
                }
            }
        }       
        // i.e. Parcel-humans "Sharon VanEtten owns 2115 GARRICK" and "Sharon VanEtten is a Witness in citation 33223"
        // Outgoing links, i.e. this human is the SOURCE
        persLinkHeavy.setHumanNonHumanLinkList(hnhll);
        // i.e. human-human links "Sharon VanEtten owns ETTEN LLC (also a human object)" or "Sharon VanEtten is the daughter of Mildred VanEtten"
        // also outgoing Links, i.e. this human is the SOURCE
        persLinkHeavy.sethumanLinkList(getHumanLinkList(persLinkHeavy));
        // get links of which Sharon VanEtten is the TARGET
        // i.e. "Sharon VanEtten is managed by Indie Records LLC (also a human object)
        persLinkHeavy.setOutgoingHumanLinksFromThisHuman(pi.getHumanLinkOutgoingGivenSource(persLinkHeavy));
        
        persLinkHeavy.setMergeLogs(getPersonMergeLogs(pers));
        
        return persLinkHeavy;
    }
    
    
    /**
     * Logic container for Person object assembly
     * from an underlying human record
     * 
     * @param p
     * @return the configured person
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public Person configurePerson(Person p) throws IntegrationException, BObStatusException{
        PersonIntegrator pi = getPersonIntegrator();
        PropertyCoordinator pc = getPropertyCoordinator();
        
        if(p == null){
            throw new BObStatusException("Cannot configure null person");
        }
        
        // person-person links and reverses
        p.setAliasList(getHumanAliasList(p));
        
        p.setMailingAddressLinkList(pc.getMailingAddressLinkList(p));
        List<ContactPhone> phl = pi.getContactPhoneList(p.getHumanID());
        if(phl != null && phl.size() >= 2){
            Collections.sort(phl);
        }
        p.setPhoneList(phl);
        List<ContactEmail> eml = pi.getContactEmailList(p.getHumanID());
        if(eml != null && eml.size() >= 2){
            Collections.sort(eml);
        }
        p.setEmailList(eml);
        
        generateMADLinkListPretty(p);
        generatePhoneListPretty(p);
        generateEmailListPretty(p);
        return p;
    }
    
    
    
    /**
     * Extracts a list of aliases by human
     * @param h
     * @return
     * @throws BObStatusException 
     */
    public List<HumanAlias> getHumanAliasList(Human h) throws BObStatusException, IntegrationException{
        if(h == null || h.getHumanID() == 0){
            throw new BObStatusException("Cannot get alias list with null incoming human or with human ID of 0");
        }
        PersonIntegrator pi =getPersonIntegrator();
        
        List<HumanAlias> aliasList = new ArrayList<>();
        List<Integer> aliasIDList = pi.getHumanAliasList(h);
       
        if(aliasIDList != null && !aliasIDList.isEmpty()){
            for(Integer id: aliasIDList){
                aliasList.add(getHumanAlias(id));
            }
        }
            
        return aliasList;
    }
    
    /**
     * Factory for aliases
     * @param ua
     * @return 
     */
    public HumanAlias getHumanAliasSkeleton(Human h, UserAuthorized ua) throws BObStatusException, IntegrationException{
        if(h == null){
            throw new BObStatusException("cannot init alias with null human to alias");
        }
        SystemCoordinator sc = getSystemCoordinator();
        
        HumanAlias ha = new HumanAlias();
        ha.setHumanID(h.getHumanID());
        ha.setCreatedBy(ua);
        ha.setLastUpdatedBy(ua);
        ha.setAliasRoleEnum(HumanAliasRoleEnum.GeneralAlias);
        // bobsourcePersonInternal
        ha.setSource(sc.getBObSource(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                                .getString("bobsourcePersonInternal"))));
        
        return ha;
    }
    
    /**
     * getter for human aliases
     * @param aliasID
     * @return
     * @throws IntegrationException 
     */
    public HumanAlias getHumanAlias(int aliasID) throws IntegrationException{
        PersonIntegrator pi = getPersonIntegrator();
        return pi.getHumanAlias(aliasID);
    }
    
    /**
     * Writes a new alias
     * @param alias
     * @param h
     * @param ua
     * @return 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public int insertHumanAlias(HumanAlias alias, Human h, UserAuthorized ua) throws AuthorizationException, BObStatusException{
        
        PersonIntegrator pi = getPersonIntegrator();
        if(alias == null || alias.getAliasID() != 0 || alias.getAliasName() == null || alias.getAliasRoleEnum() == null || h == null || h.getHumanID() == 0){
            throw new BObStatusException("Cannot insert a null alias or one with a nonzero ID or one without an alias or role");
        }
        if(!permissionCheckpointUpdatePerson(ua)){
            throw new AuthorizationException("User lacks sufficient privileges to write new alias");
        }
        if(alias.getHumanID() != h.getHumanID()){
            throw new BObStatusException("Incoming human and incoming alias human ID do not match!");
        }
        alias.setCreatedBy(ua);
        alias.setLastUpdatedBy(ua);
        int freshID = pi.insertHumanAlias(alias);
        getPersonCacheManager().flush(h);
        
        return freshID;
    }
    
    
    /**
     * Updates the given alias
     * @param alias
     * @param ua
     * @throws AuthorizationException
     * @throws BObStatusException 
     */
    public void updateHumanAlias(HumanAlias alias, UserAuthorized ua) throws AuthorizationException, BObStatusException{
        
        PersonIntegrator pi = getPersonIntegrator();
        if(alias == null || alias.getAliasID() == 0 ){
            throw new BObStatusException("Cannot update a null alias or one with a nonzero ID or one without an alias or role");
        }
        if(!permissionCheckpointUpdatePerson(ua)){
            throw new AuthorizationException("User lacks sufficient privileges to update new alias");
        }
        
        alias.setLastUpdatedBy(ua);
        
        pi.updateHumanAlias(alias);
        getPersonCacheManager().flush(alias);
        
    }
    
   
    
    /**
     * Deactivates the given alias
     * @param alias
     * @param ua
     * @throws AuthorizationException
     * @throws BObStatusException 
     */
    public void deactivateHumanAlias(HumanAlias alias, UserAuthorized ua) throws AuthorizationException, BObStatusException{
        
        PersonIntegrator pi = getPersonIntegrator();
        if(alias == null || alias.getAliasID() == 0 ){
            throw new BObStatusException("Cannot deac a null alias or one with a nonzero ID or one without an alias or role");
        }
        if(!permissionCheckpointUpdatePerson(ua)){
            throw new AuthorizationException("User lacks sufficient privileges to deac an alias");
        }
        
        alias.setLastUpdatedBy(ua);
        alias.setDeactivatedBy(ua);
        
        pi.deactivatetHumanAlias(alias);
        getPersonCacheManager().flush(alias);
    }
    
    
    
    
    /**
     * Internal logic for building an HTML string
     * of the mailing addresses linked to this person
     * @param p 
     */
    public void generateMADLinkListPretty(Person p){
        if(p != null && p.getMailingAddressLinkList() != null && !p.getMailingAddressLinkList().isEmpty()){
            StringBuilder sb = new StringBuilder("");
            for(MailingAddressLink madlink: p.getMailingAddressLinkList()){
                // negative priorities are flags for hiding this entry
                if(madlink.getPriority() >= 0){
                    // removed roles to remove gunk on permits
                    //sb.append(madlink.getLinkedObjectRole().getTitle());
                    sb.append(madlink.getAddressPretty2LineEscapeFalse());
                    sb.append(Constants.FMT_HTML_BREAK);
                }
            }
            p.setMailingAddressListPretty(sb.toString());
        }
    }
    
    /**
     * Internal logic for making a nice phone list with
     * breaks and type, etc.
     * @param p 
     */
    private void generatePhoneListPretty(Person p){
        if(p != null && p.getPhoneList() != null && !p.getPhoneList().isEmpty()){
            StringBuilder sb = new StringBuilder("");
            for(ContactPhone ph: p.getPhoneList()){
                // Negative priorities are HIDDEN, meaning not displayed
                // in pretty printed contact strings
                if(ph.getPriority() >= 0){
                    sb.append(ph.getPhoneNumber());
                    if(ph.getPhoneType() != null && !isStringEmptyIsh(ph.getPhoneNumber())){
                        sb.append(" (");
                        sb.append(ph.getPhoneType().getTitle());
                        sb.append(")");
                    }
                    if(ph.getExtension() != 0){
                        sb.append("[ext. ");
                        sb.append(ph.getExtension());
                        sb.append("] ");
                    }
                   sb.append(Constants.FMT_HTML_BREAK);
                }
            }
            p.setPhoneListPretty(sb.toString());
        }
    }
    
    
    /**
     * Internal logic for generating an HTML string 
     * of a person's email list.
     * @param p 
     */
    private void generateEmailListPretty(Person p){
         if(p != null && p.getEmailList()!= null && !p.getEmailList().isEmpty()){
            StringBuilder sb = new StringBuilder("");
            for(ContactEmail ce: p.getEmailList()){
                if(ce.getPriority() >= 0){
                    sb.append(ce.getEmailaddress());
                    sb.append(Constants.FMT_HTML_BREAK);
                }
            }
            p.setEmailListPretty(sb.toString());
        }
    }
    
    
    /**
     * Convenience method for converting a list of human links into a single
     * HTML string with a double break (empty line) in between each link
     * @param linkList
     * @return the String version of this list
     */
    public String buildHumanLinkListString(List<HumanLink> linkList){
        StringBuilder sb = new StringBuilder();
        if(linkList != null && !linkList.isEmpty()){
            for(int idx = 0; idx < linkList.size(); idx++){
                sb.append(linkList.get(idx).getName());
                sb.append(Constants.FMT_HTML_BREAK);
                sb.append(linkList.get(idx).getPrettyContactStringComplete());
                // don't add a double break after last link
                if(idx < linkList.size()-1){
                    sb.append(Constants.FMT_HTML_BREAK);
                    sb.append(Constants.FMT_HTML_BREAK);
                }
            }
        }
        return sb.toString();
    }
    
    /**
     * Logic intermediary for Updates to the human listing
     * @param h
     * @param ua
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void updateHuman(Human h, UserAuthorized ua) throws IntegrationException, AuthorizationException{
        PersonIntegrator pi = getPersonIntegrator();
        if(h != null && ua != null){
            if(!permissionCheckpointUpdatePerson(ua)){
                throw new AuthorizationException("User does not have permissions to update a person record");
            }
            h.setLastUpdatedBy(ua);
            pi.updateHuman(h);
            getPersonCacheManager().flush(h);
            
        }
    }

    
    /**
     * Master deactivation pathway for an entire human/person. This method will
     * convert the given human into its full link heavy person and then proceed to 
     * deactivate all the human links, addresses links, phones, emails, and underlying human record.
     * 
     * USERS BEWARE
     * 
     * This is NOT A FAIL SAFE method and can get jammed up part way through and there 
     * will be no automated recovery besides fishing out the records manually 
     * and either continuing the deac cascade or reverting back. 
     * 
     * @param h
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException if human has links external to muni
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void deativateHuman(Human h, UserAuthorized ua) throws BObStatusException, AuthorizationException, IntegrationException{
        PersonIntegrator pi = getPersonIntegrator();
        
        if(h == null || ua == null){
            throw new BObStatusException("Cannot deac a human with null human or requesting user");
        }
        if(!permissionCheckpointUpdatePerson(ua)){
            throw new AuthorizationException("Requesting user lacks permission to deactivate a human");
        }
        // non system admin users can only deac humans with links to objects whose parent muni 
        // is the given user's muni
        // and let's make sure we have the most current person 
        PersonLinkHeavy plh = assemblePersonLinkHeavy(getPerson(getHuman(h.getHumanID())));
        
        if(!ua.getKeyCard().isHasSysAdminPermissions() && checkForLinksExternalToUserAuthorizedMuni(plh, ua)){
            // this logic check was disabled August 2024 since most persons will have external muni links 
            // and this is throwing a wrench in merging operations. Keep off until a better system is developed
//            throw new AuthorizationException("The requested person cannot be deactivated by the requesting user "
//                    + "because the person has links to objects outside the requesting user's session muni and "
//                    + "the requesting user is not a system admin");
//            
        }
        
        // deac the outgoing human links first
        for(HumanLink hl: plh.getHumanNonHumanLinkList()){
            deactivateHumanLink(hl, ua);
            System.out.println("PersonCoordinator.deactivateHuman | deac human-nonhuman link ID: " + hl.getLinkID());
        }
        // deac the incoming human human links next
        for(HumanLink hl: plh.gethumanLinkList()){
            deactivateHumanLink(hl, ua);
            System.out.println("PersonCoordinator.deactivateHuman | deac human-human link ID: " + hl.getLinkID());
        }
        
        // now deac our address links, phones, and emails
        if(plh.getMailingAddressLinkList() != null && !plh.getMailingAddressLinkList().isEmpty()){
            PropertyCoordinator pc = getPropertyCoordinator();
            for(MailingAddressLink madlink: plh.getMailingAddressLinkList()){
                pc.deactivateLinkToMailingAddress(madlink, ua);
                System.out.println("PersonCoordinator.deactivateHuman | deac mailing adddress link ID: " + madlink.getLinkID());
            }
        }
        
        if(plh.getPhoneList() != null && !plh.getPhoneList().isEmpty()){
            for(ContactPhone phone: plh.getPhoneList()){
                contactPhoneDeactivate(phone, ua);
                System.out.println("PersonCoordinator.deactivateHuman | deac phone ID: " + phone.getPhoneID());
            }
        }
        
        if(plh.getEmailList() != null && !plh.getEmailList().isEmpty()){
            for(ContactEmail phone: plh.getEmailList()){
                contactEmailDeactivate(phone, ua);
                System.out.println("PersonCoordinator.deactivateHuman | deac email ID: " + phone.getEmailID());
            }
        }
        
        // now deac our base human record
        plh.setDeactivatedBy(ua);
        plh.setLastUpdatedBy(ua);
        pi.deactivateHuman(h);
        getPersonCacheManager().flush(h);
    }
    
    /**
     * Scans through the list of linked objects for exogenous connections
     * @param plh whose links will be checked
     * @param ua whose muni will be matched against the linked object list
     * @return true if the given person link's heavy link list contains objects whose parent muni is not the given UAs
     */
    public boolean checkForLinksExternalToUserAuthorizedMuni(PersonLinkHeavy plh, UserAuthorized ua) throws BObStatusException{
        if(plh == null || ua == null){
            throw new BObStatusException("Cannot check for links with null person link heavy or user");
        }
        for(HumanLink hl: plh.getHumanNonHumanLinkList()){
            HumanLinkParentObjectInfoHeavy hlpioh = assembleHumanLinkParentObjectInfoHeavy(hl, ua);
            if(hlpioh.isExternalToSessionMuni()){
                return true;
            }
        }
        return false;
    }
    
    /**
     * Factory method for Human objects not in the DB
     * @return an empty Skeleton of a Human
     */
    public Human humanInit(){
        Human h = new Human();
        return h;
    }
    
    /**
     * Creates skeleton or starter person for new person creation
     * TODO: Finish my guts
     * 
     * @param m
     * @return 
     */
    public PersonLinkHeavy createPersonSkeleton(Municipality m){
        Person newP = new Person(humanInit()); 
        newP.setBusinessEntity(false);
        PersonLinkHeavy plh = new PersonLinkHeavy(newP);
        return plh;
        
    }
    
    /**
     * Checks logic of incoming person objects and passes write off to Integrator
     * @param h
     * @param ua
     * @return 
     * @returnthe completed human as refreshed from the DB with a db key and everything!
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public Person insertHuman(Human h, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException{
        SystemCoordinator sc = getSystemCoordinator();
        PersonIntegrator pi = getPersonIntegrator();
       
        if(!permissionCheckpointUpdatePerson(ua)){
            throw new AuthorizationException("User lacks permissions to insert/update person related data");
        }
        
        h.setLastUpdatedBy(ua);
        h.setCreatedBy(ua);
        h.setSource( sc.getBObSource(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                                .getString("bobsourcePersonInternal"))));
        return getPersonByHumanID(pi.insertHuman(h));
        
    }
    
    
   
    
    /**
     * Queries DB for all humans who have been linked to a User in the table login
     * @return a list, perhaps with the Humans mapped to hers
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public List<Human> getHumansMappedToUsers() throws IntegrationException{
        PersonIntegrator pi = getPersonIntegrator();
        List<Human> hl = new ArrayList<>();
        List<Integer> idl = pi.getAllHumansDesignatedAsUserHumans();
        if(!idl.isEmpty()){
            for(Integer i: idl){
                hl.add(getHuman(i));
            }
        }
        return hl;
    }
    
    // *******************************************************
    // *****************  PERSON MERGING *********************
    // *******************************************************
    
    /**
     * Factory for merge requests
     * @return 
     */
    public PersonMergeRequest getPersonMergeRequestSkeleton(){
        PersonMergeRequest req = new PersonMergeRequest();
        req.setMatchLevel(MergeMatchLevelEnum.MODERATE);
        req.setAliasSubordinateName(true);
        req.setTransferHiddenContactObjects(true);
        req.setUnhideAllHiddenContactObjects(true);
        return req;
    }

    
    /**
     * Extracts a merge log by ID
     * @param mergeID
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public PersonMergeLog getPersonMergeLog(int mergeID) throws BObStatusException, DatabaseFetchRuntimeException, IntegrationException{
        PersonIntegrator pi = getPersonIntegrator();
        if(mergeID == 0){
            throw new BObStatusException("Cannot get merge log with ID = 0");
        }
        return pi.getMergeLog(mergeID);
        
        
    }
    
    /**
     * Extracts all merge logs for the given human who may be the
     * primary merge person on zero or more merges
     * 
     * @param h the primary human whose incoming merge records caller seeks
     * @return all merge logs
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public List<PersonMergeLog> getPersonMergeLogs(Human h) throws IntegrationException, DatabaseFetchRuntimeException, BObStatusException {
        PersonIntegrator pi = getPersonIntegrator();
        return pi.getMergeLogs(h);
        
    }
    
    /**
     * Implements logic to verify if a pair of persons can be merged in the direction 
     * given.Both must be active.
     * 
     * @param req
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public void checkPersonMergeAuthorization(PersonMergeRequest req) throws BObStatusException, IntegrationException{
        if(req.getPersonPrimary() == null || req.getPersonSubordinate() == null){
            throw new BObStatusException("Cannot merge null persons");
        }
        
        if(!req.getPersonPrimary().isActive() || !req.getPersonSubordinate().isActive()){
            throw new BObStatusException("Cannot merge deactivated primary or subordinate persons");
        }
        
        if(req.getPersonPrimary().getHumanID() == req.getPersonSubordinate().getHumanID()){
            throw new BObStatusException("Cannot merge a person into itself. Choose two different persons.");
        }
        // check that our sub is not a user linked person
        UserIntegrator ui = getUserIntegrator();
        if(ui.getUserIDsOfUsersLinkedToHuman(req.getPersonSubordinate()) != null){
            throw new BObStatusException("Subordinate user is linked to a user record and therefore cannot be merged");
        }
        
       
    }
    
    /**
     * PersonBB compatible method for checking merge init compatibility
     * @param req 
     */
    public void marshallCheckPersonMergeAuth(PersonMergeRequest req){
       if(req != null){
           try {
               checkPersonMergeAuthorization(req);
                req.setInitialCheckMergeAllowed(true);
                req.setInitialMergeCheckMessage("Preliminary merge compatibility approved!");
           } catch (BObStatusException | IntegrationException  ex) {
               System.out.println(ex);
               req.setInitialCheckMergeAllowed(false);
               req.setInitialMergeCheckMessage(ex.getMessage());
           } 
           req.setInitialCheckMergeTS(LocalDateTime.now());
       }
    }
    

    /**
     * Primary committing pathway for merging two person records.The Primary Person
     * will remain active, and the subordinate will be deactivated and its inner bits
     * rewired to mesh as requested with the primary record.
     * 
     * The returned PersonMergeLog will contain excruciatingly detailed record of the
     * merge process. Generally speaking, this method will not throw exceptions during the
     * merge process itself, only during preliminary checks that prevent the merging from 
     * being attempted. The caller (and ultimately) the user will need to untangle
     * buggered merges as this method will not attempt to undo partial re-linking operations.
     * 
     * When merging mailing addresses, phones, and emails new link records will be written meaning
     * fresh linkIDs will be generated in the DB, allowing the subordinate person's records to all
     * be completely deactivated without any negative impact on our primary person.
     * 
     * At the end of the merging, the subordinate person will be deactivated. 
    * 
     * @param mergeRequest The primary and subordinate person must be present
     * @param ua requesting the merge.
     * @return a new instance of PersonMergeLog reflecting the completed merge operation
     * complete with a merge ID and merge log
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public PersonMergeLog mergePersons(PersonMergeRequest mergeRequest, UserAuthorized ua) throws BObStatusException, DatabaseFetchRuntimeException, IntegrationException, AuthorizationException{
        PersonIntegrator pi = getPersonIntegrator();
        PropertyCoordinator propc = getPropertyCoordinator();
        
        if(!permissionCheckpointUpdatePerson(ua)){
            throw new AuthorizationException("Requesting user lack permissions sufficient to merge person records");
        }
        if(mergeRequest == null || ua == null || mergeRequest.getRequestingUser() == null){
            throw new BObStatusException("cannot merge person with null request or user");
        }
        if(mergeRequest.getPersonPrimary() == null || mergeRequest.getPersonSubordinate() == null){
            throw new BObStatusException("Malformed merge request: null primary or subordinate person");
        }
        if(!mergeRequest.getPersonSubordinate().isActive()){
            throw new BObStatusException("Cannot merge a deactivated person into any other person");
        }
        
        checkPersonMergeAuthorization(mergeRequest);
        
        // basic setup
        PersonMergeLog log = new PersonMergeLog(mergeRequest);
        int freshLogID;
        log.setMergingUser(ua);
        initPersonMergeLog(log, mergeRequest, ua);
        
        // call our logic chunks to do the actual writes
        // these all act in isolation and will catch their own exceptions, log those exceptions
        // and return to normal call flow
        // these methods SHOULD NOT throw any exception that would short circuit the logging
        // because we plan to write a Merge Log regardless of outcome
        mergeAliasingSubordinate(log, mergeRequest);
        mergeMailingAddresses(log, mergeRequest);
        mergePhoneNumbers(log, mergeRequest);
        mergeEmails(log, mergeRequest);
        mergeHumanNonHumanLinks(log, mergeRequest);
        mergeHumanHumanLinks(log, mergeRequest);
        if(log.getFatalFailureTS() == null){
            mergeDeacSubordinateRecords(log, mergeRequest);
            log.setSuccessTS(LocalDateTime.now());
        } else {
            log.appendToMergeLog("Detected fatal condition; skipping deactivation of subordinate person.", true);
        }
       
        // okay, we have a successful merge
        // cleanup and logging
        freshLogID = pi.insertPersonMergeRecord(log);
        // getting the merge log will implicitly get our two fresh persons, the subordinate
        // being deactivated
        return getPersonMergeLog(freshLogID);
    }
    
    
    /**
     * Writes the initial log lines in merging log
     * @param mergeLog CANNOT BE NULL
     */
    private void initPersonMergeLog(PersonMergeLog log, PersonMergeRequest req, UserAuthorized ua){
        
        log.appendToMergeLog("BEGIN MERGE LOG AT ");
        log.appendToMergeLog(LocalDateTime.now().toString());
        log.appendToMergeLog(" by username ");
        log.appendToMergeLog(ua.getUsername(), true);
        log.appendToMergeLog("Primary person: ");
        log.appendToMergeLog(req.getPersonPrimary().getName());
        log.appendToMergeLog(" | ID: ");
        log.appendToMergeLog(String.valueOf(req.getPersonPrimary().getHumanID()), true);
        log.appendToMergeLog("Subordinate person: ");
        log.appendToMergeLog(req.getPersonSubordinate().getName());
        log.appendToMergeLog(" | ID: ");
        log.appendToMergeLog(String.valueOf(req.getPersonSubordinate().getHumanID()), true);
    }
    
    /**
     * Chunked merge logic to handle aliasing the subordinate's name to primary if requested.
     * AND i also check the subs aliases and alias those as well
     * @param log cannot be null
     * @param req cannot be null
     */
    private void mergeAliasingSubordinate(PersonMergeLog log, PersonMergeRequest req){
         // aliasing
        if(req.isAliasSubordinateName()){
            try {
                List<String> names = new ArrayList<>();
                // build names to alias candidate list
                names.add(req.getPersonSubordinate().getName());
                if(req.getPersonSubordinate().getAliasList() != null && !req.getPersonSubordinate().getAliasList().isEmpty()){
                    for(HumanAlias ha: req.getPersonSubordinate().getAliasList()){
                        names.add(ha.getAliasName());
                    }
                }
                for(String nameToAlias: names){
                    // logging
                    log.appendToMergeLog("Preparing to add subordinate name as alias to primary if unique | name to alias: ");
                    log.appendToMergeLog(nameToAlias, true);
                    // checking for duplicate alias or name
                    if(mergeAliasCheckForUniqueness(nameToAlias, req)){

                        HumanAlias alias = getHumanAliasSkeleton(req.getPersonPrimary(), req.getRequestingUser());
                        alias.setAliasName(nameToAlias);
                        alias.setAliasRoleEnum(HumanAliasRoleEnum.MergedSubordianteName);
                        int aliasid = insertHumanAlias(alias, req.getPersonPrimary(), req.getRequestingUser());
                        log.appendToMergeLog("Alias write success | fresh alias ID: ");
                        log.appendToMergeLog(String.valueOf(aliasid), true);
                        } else {
                            log.appendToMergeLog("Duplicate name found on primary; skipping aliasing on primary", true);
                        }
                }
            } catch (BObStatusException | AuthorizationException | IntegrationException ex) {
                System.out.println(ex);
                log.appendToMergeLog("Exception during aliasing | ");
                log.appendToMergeLog(ex.getMessage(), true);
                log.setFatalFailureTS(LocalDateTime.now());
            }
        }
    }
    
    /**
     * Checks for the existence of a duplicate name on the primary person in the merge
     * and if a duplicate exists, sub name shall not be aliased on primary
     * @param aliasCandidate
     * @param req
     * @return 
     */
    private boolean mergeAliasCheckForUniqueness(String aliasCandidate, PersonMergeRequest req){
        if(aliasCandidate != null && req != null && req.getPersonPrimary() != null){
            if(aliasCandidate.equals(req.getPersonPrimary().getName())){
                return false;
            }
            if(req.getPersonPrimary().getAliasList() != null && !req.getPersonPrimary().getAliasList().isEmpty()){
                for(HumanAlias halias: req.getPersonPrimary().getAliasList()){
                    if(aliasCandidate.equals(halias.getAliasName())){
                        return false;
                    }
                }
            }
        }
        return true;
    }
    
    /**
     * Chunked merge logic for mailing addresses only
     * @param log cannot be null
     * @param req cannot be null
     */
    private void mergeMailingAddresses(PersonMergeLog log, PersonMergeRequest req){
         
        PropertyCoordinator pc = getPropertyCoordinator();
        // address links
        if(req.getPersonSubordinate().getMailingAddressLinkList() != null && !req.getPersonSubordinate().getMailingAddressLinkList().isEmpty()){
            // Examine each incoming MAD Link: Check for dupes, and if unique make new link to primary, deac subordinate
            for(MailingAddressLink madLink: req.getPersonSubordinate().getMailingAddressLinkList()){
                // merge configuration param check logic
                if(madLink.isHidden() && !req.isTransferHiddenContactObjects()){
                    continue;
                }
                // skip dupes, just deac and log
                if(!req.getPersonPrimary().getMailingAddressLinkList().contains(madLink)){
                    MailingAddressLink freshLink = pc.getMailingAddressLinkSkeleton(madLink);
                    freshLink.setLinkedObjectRole(madLink.getLinkedObjectRole());
                    try {
                        log.appendToMergeLog("Linking subordinate MAD Link ID: ");
                        log.appendToMergeLog(String.valueOf(madLink.getLinkID()));
                        log.appendToMergeLog(" to primary person ... | ");

                        // okay do the actual linking
                        int freshMADLinkID = pc.linkToMailingAddress(req.getPersonPrimary(), madLink, req.getRequestingUser());
                        // now log shit
                        log.appendToMergeLog(" Success! Fresh MAD Link ID: ");
                        log.appendToMergeLog(String.valueOf(freshMADLinkID), true);
                    } catch (BObStatusException | IntegrationException ex) {
                        System.out.println(ex);
                        log.appendToMergeLog("EXCEPTION while attempting to link subordinate mad link | ");
                        log.appendToMergeLog(ex.getMessage(), true);
                        log.setFatalFailureTS(LocalDateTime.now());
                    } 
                } else { // deac and log dupes
                    
                }
            }
        } else {
            log.appendToMergeLog("Found no mailing addresses to merge");
            log.appendToMergeLog(Constants.FMT_HTML_BREAK);
        }
    }
    
    /**
     * Looks through phone numbers and merges as required by merge level
     * @param log cannot be null
     * @param req cannot be null
     */
    private void mergePhoneNumbers(PersonMergeLog log, PersonMergeRequest req){
        SystemCoordinator sc = getSystemCoordinator();
        if(req.getPersonSubordinate().getPhoneList() != null && !req.getPersonSubordinate().getPhoneList().isEmpty()){
            for(ContactPhone subPhone: req.getPersonSubordinate().getPhoneList()){
                log.appendToMergeLog("Considering subordinate phone: ");
                log.appendToMergeLog(subPhone.getPhoneNumber());
                log.appendToMergeLog(" | ID: ");
                log.appendToMergeLog(String.valueOf(subPhone.getPhoneID()));
                if(subPhone.isHidden() && !req.isTransferHiddenContactObjects()){
                    log.appendToMergeLog(" | hidden and config set to NOT xfer hidden | no action");
                    continue;
                }
                // skip dupes
                if(!checkForMatchRecordsInList(req.getPersonPrimary().getPhoneList(), subPhone, req.getMatchLevel())){
                    // the candidate subordinate phone does NOT have a match on the primary, so let's link the primary to this phone
                    log.appendToMergeLog(" | no match found in primary ");
                    // rewire the unique phone number to point to the primary person
                    subPhone.setHumanID(req.getPersonPrimary().getHumanID());
                    StringBuilder sb = new StringBuilder();
                    sb.append("Previous human ID: ");
                    sb.append(subPhone.getHumanID());
                    sb.append(" (");
                    sb.append(req.getPersonSubordinate().getName());
                    sb.append(") | new humanID: ");
                    sb.append(req.getPersonPrimary().getHumanID());
                    sb.append(" (");
                    sb.append(req.getPersonPrimary().getName());
                    sb.append(")");
                    MessageBuilderParams mbp = new MessageBuilderParams(subPhone.getNotes(), "Transfer during merge", null, sb.toString(), req.getRequestingUser(), null);
                    subPhone.setNotes(sc.appendNoteBlock(mbp));
                    try {
                        // attempt the actual rewriring of the phone
                        contactPhoneUpdate(subPhone, req.getRequestingUser());
                        sc.writeNotes(subPhone, req.getRequestingUser());
                    } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
                        log.appendToMergeLog("Exception rewiring phone | ex.message: ");
                        log.appendToMergeLog(ex.getMessage());
                        log.appendToMergeLog(" | attempted note: ");
                        log.appendToMergeLog(sb.toString(), true);
                        log.setFatalFailureTS(LocalDateTime.now());
                    }
                } else {
                    // we found a match, so there's no need do any link updating
                    log.appendToMergeLog(" | matching phone found in primary; no further action to take", true);
                } // end matching check
            } // end looping over sub phone list
        } // close check for sub phones to evaluate
    } // close merge phones
    
    
    /**
     * Looks through emails and merges as required by merge level
    * @param log cannot be null
     * @param req cannot be null
     */
    private void mergeEmails(PersonMergeLog log, PersonMergeRequest req){
        SystemCoordinator sc = getSystemCoordinator();
        
        if(req.getPersonSubordinate().getEmailList() != null && !req.getPersonSubordinate().getEmailList().isEmpty()){
            for(ContactEmail subEmail: req.getPersonSubordinate().getEmailList()){
                log.appendToMergeLog("Considering subordinate email: ");
                log.appendToMergeLog(subEmail.getEmailaddress());
                log.appendToMergeLog(" | ID: ");
                log.appendToMergeLog(String.valueOf(subEmail.getEmailID()));
                if(subEmail.isHidden() && !req.isTransferHiddenContactObjects()){
                    log.appendToMergeLog(" | hidden and config set to NOT xfer hidden | no action");
                    continue;
                }
                // skip dupes
                if(!checkForMatchRecordsInList(req.getPersonPrimary().getEmailList(), subEmail, req.getMatchLevel())){
                    // the candidate subordinate phone does NOT have a match on the primary, so let's link the primary to this phone
                    log.appendToMergeLog(" | no match found in primary ");
                    // rewire the unique email to point to the primary person
                    subEmail.setHumanID(req.getPersonPrimary().getHumanID());
                    StringBuilder sb = new StringBuilder();
                    sb.append("Previous human ID: ");
                    sb.append(subEmail.getHumanID());
                    sb.append(" (");
                    sb.append(req.getPersonSubordinate().getName());
                    sb.append(") | new humanID: ");
                    sb.append(req.getPersonPrimary().getHumanID());
                    sb.append(" (");
                    sb.append(req.getPersonPrimary().getName());
                    sb.append(")");
                    MessageBuilderParams mbp = new MessageBuilderParams(subEmail.getNotes(), "Transfer during merge", null, sb.toString(), req.getRequestingUser(), null);
                    subEmail.setNotes(sc.appendNoteBlock(mbp));
                    try {
                        // attempt the actual rewriring of the email
                        contactEmailUpdate(subEmail, req.getRequestingUser());
                        sc.writeNotes(subEmail, req.getRequestingUser());
                    } catch (IntegrationException | AuthorizationException | BObStatusException ex) {
                        log.appendToMergeLog("Exception rewiring email | ex.message: ");
                        log.appendToMergeLog(ex.getMessage());
                        log.appendToMergeLog(" | attempted note: ");
                        log.appendToMergeLog(sb.toString(), true);
                        log.setFatalFailureTS(LocalDateTime.now());
                    }
                } else {
                    // we found a match, so there's no need do any link updating
                    log.appendToMergeLog(" | matching email found in primary; no further action to take", true);
                } // end matching check
            } // end looping over sub phone list
        } // close check for sub phones to evaluate
    } // close merge emails
    
    /**
     * Analyzes object links on the sub and rebuilds the links on the primary and deactivates
     * all subordinate person links
     * @param log cannot be null
     * @param req cannot be null
     */
    private void mergeHumanNonHumanLinks(PersonMergeLog log, PersonMergeRequest req){
        PersonIntegrator pi = getPersonIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        // start with human-nonhuman links
        if(req.getPersonSubordinate().getHumanNonHumanLinkList() != null && !req.getPersonSubordinate().getHumanNonHumanLinkList().isEmpty()){
            for(HumanLink hnhlink: req.getPersonSubordinate().getHumanNonHumanLinkList()){
                log.appendToMergeLog("Considering human-nonhuman link on subordinate with link ID: ");
                log.appendToMergeLog(String.valueOf(hnhlink.getLinkID()));
                log.appendToMergeLog( " | role title : ");
                log.appendToMergeLog(hnhlink.getLinkedObjectRole().getTitle());
                log.appendToMergeLog( " | schema: ");
                log.appendToMergeLog(hnhlink.getLinkedObjectRole().getSchema().name());
                if(!checkForMatchingRecordsInList(req.getPersonPrimary().getHumanNonHumanLinkList(), hnhlink, req.getMatchLevel())){
                    // no match, so let's write a new link
                    StringBuilder sb = new StringBuilder();
                    sb.append("Link created from human merge | subordinate human ID: ");
                    sb.append(req.getPersonSubordinate().getHumanID());
                    sb.append(" | previous Link ID: ");
                    sb.append((hnhlink.getLinkID()));
                    MessageBuilderParams mbp = new MessageBuilderParams(hnhlink.getLinkNotes(), "Link created from person merge", null, sb.toString(), req.getRequestingUser(), null);
                    
                    // now we are breaking all our rules and rewiring a our subordinate's human link for writing to the DB linked to our primary person!!
                    hnhlink.setLinkNotes(sc.appendNoteBlock(mbp));                    
                    // rewire our subordinate's human link for direct insert into DB with the same parent object but different human
                    hnhlink.setHumanID(req.getPersonPrimary().getHumanID());
                    try {
                        int freshLinkID = pi.insertHumanLink(hnhlink);
                        log.appendToMergeLog(" | wrote new link with ID: ");
                        log.appendToMergeLog(String.valueOf(freshLinkID), true);
                    } catch (IntegrationException | BObStatusException ex) {
                        log.appendToMergeLog("Exception rewiring link | ex.message: ");
                        log.appendToMergeLog(ex.getMessage());
                        log.appendToMergeLog(" | attempted note: ");
                        log.appendToMergeLog(sb.toString(), true);
                        log.setFatalFailureTS(LocalDateTime.now());
                    } 
                } else {
                    log.appendToMergeLog("Match found on primary; no link to write", true);
                } // close existing link branching
            } // close loop over sub's human links
        } // close non-human human links to check 
    } // close merge links
    
    /**
     * migrates human-human links
     * @param log
     * @param req 
     */
    private void mergeHumanHumanLinks(PersonMergeLog log, PersonMergeRequest req){
        PersonIntegrator pi = getPersonIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        // start with human-nonhuman links
        if(req.getPersonSubordinate().gethumanLinkList() != null && !req.getPersonSubordinate().gethumanLinkList().isEmpty()){
            for(HumanLink hhLink: req.getPersonSubordinate().gethumanLinkList()){
                log.appendToMergeLog("Considering human-human link on subordinate with link ID: ");
                log.appendToMergeLog(String.valueOf(hhLink.getLinkID()));
                log.appendToMergeLog( " | schema: ");
                log.appendToMergeLog(hhLink.getLinkedObjectRole().getTitle());
                if(!checkForMatchingRecordsInList(req.getPersonPrimary().getHumanNonHumanLinkList(), hhLink, req.getMatchLevel())){
                    // no match, so let's write a new link
                    StringBuilder sb = new StringBuilder();
                    sb.append("Link created from human merge | subordinate human ID: ");
                    sb.append(req.getPersonSubordinate().getHumanID());
                    sb.append(" | previous Link ID: ");
                    sb.append((hhLink.getLinkID()));
                    MessageBuilderParams mbp = new MessageBuilderParams(hhLink.getLinkNotes(), "Link created from person merge", null, sb.toString(), req.getRequestingUser(), null);
                    
                    // now we are breaking all our rules and rewiring a our subordinate's human link for writing to the DB linked to our primary person!!
                    hhLink.setLinkNotes(sc.appendNoteBlock(mbp));                    
                    // rewire our subordinate's human link for direct insert into DB with the same parent object but different human
                    hhLink.setHumanID(req.getPersonPrimary().getHumanID());
                    try {
                        int freshLinkID = pi.insertHumanLink(hhLink);
                        log.appendToMergeLog(" | wrote new human-human link with ID: ");
                        log.appendToMergeLog(String.valueOf(freshLinkID), true);
                    } catch (IntegrationException | BObStatusException ex) {
                        log.appendToMergeLog("Exception rewiring link | ex.message: ");
                        log.appendToMergeLog(ex.getMessage());
                        log.appendToMergeLog(" | attempted note: ");
                        log.appendToMergeLog(sb.toString(), true);
                        log.setFatalFailureTS(LocalDateTime.now());
                    } 
                } else {
                    log.appendToMergeLog("Match found on primary; no link to write", true);
                } // close existing link branching
            } // close loop over sub's human links
        } // close non-human human links to check 
    } // close merge links
    
    /**
     * Sets our sub to be a deactivated human
     * @param log cannot be null
     * @param req  cannot be null
     */
    private void mergeDeacSubordinateRecords(PersonMergeLog log, PersonMergeRequest req){
        log.appendToMergeLog("Completed sub record merging | deactivating subordinate human ID: ");
        log.appendToMergeLog(String.valueOf(req.getPersonSubordinate().getHumanID()));
        try {
            deativateHuman(req.getPersonSubordinate(), req.getRequestingUser());
        } catch (BObStatusException | AuthorizationException | IntegrationException  ex) {
            System.out.println(ex.getMessage());
            log.appendToMergeLog("Exception during final sub deac | ex.message: ");
            log.appendToMergeLog(ex.getMessage());
            log.setFatalFailureTS(LocalDateTime.now());
        } 
    }
    
    
    /**
     * examines the given human link candidate for duplicates in the given link list;
     * @param linkList
     * @param candidate
     * @param matchLevelFloor
     * @return if a match exists in the list at or above the given floor
     */
    public boolean checkForMatchingRecordsInList(List<HumanLink> linkList, HumanLink candidate, MergeMatchLevelEnum matchLevelFloor){
        MergeMatchLevelEnum highestMatchLevel = MergeMatchLevelEnum.NO_MATCH;
        if(linkList != null && !linkList.isEmpty()){
            for(HumanLink hl: linkList){
                // parents must match
                if(hl.getParentObjectID() == candidate.getParentObjectID()){
                    if(hl.getLinkedObjectRole().getRoleID() == candidate.getLinkedObjectRole().getRoleID()){
                        // same parent with same role means perfect match
                        highestMatchLevel = MergeMatchLevelEnum.PERFECT_EQID;
                    } else {
                        // same parent, but different role, which will be a moderate link
                        if(highestMatchLevel.getRank() < MergeMatchLevelEnum.MODERATE.getRank()){
                            highestMatchLevel = MergeMatchLevelEnum.MODERATE;
                        }
                    }
                } // close parents match
            } // close looping over links
            
        } // not empty list
        return highestMatchLevel.getRank() >= matchLevelFloor.getRank();
    }
    
    
    // *******************************************************
    // *********  CONTACT PHONE AND EMAIL STUFF **************
    // *******************************************************
    
    /**
     * Custom logic for assessing a match between a given list of phones and 
     * a particular phone number. Strict matching will require the same number and the same extension;
     * moderate will tolerate differing extensions. Logic will not attempt to sync extensions
     * 
     * @param phoneList possible matches to the candidate
     * @param candidate phone who caller wonders if matches a phone in the list
     * @param matchLevelFloor minimum threshold for declaring the two phones a match; logic specific to phone numbers is implemented here
     * @return  true if the given list contains a phone number whose match level is equal to or higher than the given floor
     */
    public boolean checkForMatchRecordsInList(List<ContactPhone> phoneList, ContactPhone candidate, MergeMatchLevelEnum matchLevelFloor){
        MergeMatchLevelEnum highestMatchLevel = MergeMatchLevelEnum.NO_MATCH;
        // false default; trigger true match
        if(phoneList != null && !phoneList.isEmpty()){
            for(ContactPhone phone: phoneList){
                // the strictest level considers this a match
                // this would technically be impossible to reach for phones from two different humans since a 
                // phone number record is keyed directly to a single human, so 
                // this is a check in case the merge tries to merge two of the same humans??
                if(phone.getPhoneID() == candidate.getPhoneID()){
                    // we have highest level match
                    highestMatchLevel  = MergeMatchLevelEnum.PERFECT_EQID;
                    // can can stop here since no phone can exceed this match level
                    break;
                }
                if(phone.getPhoneNumber().equals(candidate.getPhoneNumber())){
                    // okay the numbers are the same, check for extensions
                    if(phone.getExtension() != 0 && phone.getExtension() == candidate.getExtension()){
                        //same extension, so we've got a strict match
                        if(highestMatchLevel.getRank() < MergeMatchLevelEnum.STRICT.getRank()){
                            highestMatchLevel = MergeMatchLevelEnum.STRICT;
                        }
                    } else {
                        // different extension, so we've got a moderate match
                        if(highestMatchLevel.getRank() < MergeMatchLevelEnum.MODERATE.getRank()){
                            highestMatchLevel = MergeMatchLevelEnum.MODERATE;
                        }
                    }
                } // no match so keep our highest match level unchanged; nothing left to do; keep looking
            } // close iteration over phones
        } // close check for actual list to check
        // if any phone in the list is equal or higher in match quality than the threshold, then we say yes: match!
        return highestMatchLevel.getRank() >= matchLevelFloor.getRank();
    }
    
    /**
     * Implements logic to check for email matching. Emails are simple since we are just checking IDs for Perfect match
     * and String#equals for moderate (and strict)
     * @param emailList
     * @param candidate
     * @param matchLevelFloor
     * @return 
     */
    public boolean checkForMatchRecordsInList(List<ContactEmail> emailList, ContactEmail candidate, MergeMatchLevelEnum matchLevelFloor){
        MergeMatchLevelEnum highestMatchLevel = MergeMatchLevelEnum.NO_MATCH;
        // false default; trigger true match
        if(emailList != null && !emailList.isEmpty()){
            for(ContactEmail email: emailList){
                // the strictest level considers this a match
                // this would technically be impossible to reach for emails from two different humans since a 
                // email number record is keyed directly to a single human, so 
                // this is a check in case the merge tries to merge two of the same humans??
                if(email.getEmailID() == candidate.getEmailID()){
                    // we have highest level match
                    highestMatchLevel  = MergeMatchLevelEnum.PERFECT_EQID;
                    // can can stop here since no email can exceed this match level
                    break;
                }
                if(email.getEmailaddress().equals(candidate.getEmailaddress())){
                    // matching email meets the moderate threshold for sure
                    if(highestMatchLevel.getRank() < MergeMatchLevelEnum.MODERATE.getRank()){
                        highestMatchLevel = MergeMatchLevelEnum.MODERATE;
                    }
                } // no match so keep our highest match level unchanged; nothing left to do; keep looking
            } // close iteration over enauks
        } // close check for actual list to check
        // if any email in the list is equal or higher in match quality than the threshold, then we say yes: match!
        return highestMatchLevel.getRank() >= matchLevelFloor.getRank();
    }
    
    
    /**
     * Factory method for ContactPhone objects
     * @return 
     */
    public ContactPhone createContactPhoneSkeleton(){
        return new ContactPhone();
    }
    
    public List<ContactPhone> getContactPhoneList(List<Integer> phidl) throws IntegrationException{
        List<ContactPhone> phoneList = new ArrayList<>();
        if(phidl != null && !phidl.isEmpty()){
            for(Integer i: phidl){
                phoneList.add(getContactPhone(i));
            }
        }
       return phoneList;
    }

    /**
     * Type safe getter
     * @param ph
     * @return 
     */
    public ContactPhone getContactPhone(ContactPhone ph) throws IntegrationException{
        if(ph != null){
            return getContactPhone(ph.getPhoneID());
        } else {
            return null;
        }
    }
    
    /**
     * Extracts a phone by ID from the DB
     * @param phid
     * @return
     * @throws IntegrationException 
     */
    public ContactPhone getContactPhone(int phid) throws IntegrationException{
        if(phid != 0){
            PersonIntegrator pi = getPersonIntegrator();
            return configureContactPhone(pi.getContactPhone(phid));
        } else {
            throw new IntegrationException("cannot fetch phone with ID of 0");
            
        }
        
    }
    
    /**
     * Logic configuration intermediary for ContactPhone objects
     * @param ph
     * @return the passed in object that has been configured
     */
    private ContactPhone configureContactPhone(ContactPhone ph) throws IntegrationException{
        // if we don't have a type get the default type
        if(ph.getPhoneType() == null){
            ph.setPhoneType(getDefaultPhoneType());
        }
        // nothing to do here yet
        return ph;
    }
    
    /**
     * Logic intermediary for updating phone numbers
     * @param phone
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void contactPhoneUpdate(ContactPhone phone, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException{
        PersonIntegrator pi = getPersonIntegrator();
        if(phone == null || ua == null){
            throw new BObStatusException("Cannot update phone witn null phone or user");
        }
        if(!permissionCheckpointUpdatePerson(ua)){
            throw new AuthorizationException("User lacks permissions to insert/update person related data");
        }
        phone.setLastUpdatedBy(ua);
        pi.updateContactPhone(phone);
        getPersonCacheManager().flush(phone);
    }
    
    /**
     * Hiding a contact phone is accomplished by setting the priority to a negative value
     * @param phone
     * @param hide true for hide, false for unhide
     * @param ua
     * @throws IntegrationException
     * @throws BObStatusException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void contactPhoneHideUnhide(ContactPhone phone, boolean hide, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException{
        PersonIntegrator pi = getPersonIntegrator();
        if(phone == null || ua == null){
            throw new BObStatusException("Cannot hide phone witn null phone or user");
        }
        if(!permissionCheckpointUpdatePerson(ua)){
            throw new AuthorizationException("User lacks permissions to insert/update person related data");
        }
        if(hide){
            phone.setPriority(Constants.HIDDEN_RECORD_PRIORITY);
        } else {
            phone.setPriority(Constants.UNHIDDEN_RECORD_PRIORITY);
        }
        phone.setLastUpdatedBy(ua);
        pi.hideContactPhone(phone);
        getPersonCacheManager().flush(phone);
    }
    
    /**
     * Logic block for deactivating a phone number record
     * @param phone to deactivate
     * @param ua doing the deactivation
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void contactPhoneDeactivate(ContactPhone phone, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        if(phone == null || ua == null){
            throw new BObStatusException("Cannot deactivate phone with null phone or user");
        }
        if(!permissionCheckpointUpdatePerson(ua)){
            throw new AuthorizationException("User lacks permissions to insert/update person related data");
        }
        PersonIntegrator pi = getPersonIntegrator();
        phone.setDeactivatedBy(ua);
        phone.setDeactivatedTS(LocalDateTime.now());
        phone.setLastUpdatedBy(ua);
        pi.updateContactPhone(phone);
        getPersonCacheManager().flush(phone);
        
        
    }
    
    
    /**
     * Logic intermediary for adding a new phone number to the DB
     * @param phone
     * @param p
     * @param ua
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public ContactPhone contactPhoneAdd(ContactPhone phone, Person p, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException{
        PersonIntegrator pi = getPersonIntegrator();
        if(phone == null || p == null || ua == null){
            throw new BObStatusException("Cannot insert phone with null phone, person, or user");
        }
        if(!permissionCheckpointUpdatePerson(ua)){
            throw new AuthorizationException("User lacks permissions to insert/update person related data");
        }
        if(phone.getPhoneType() == null){
            // assign "other" type to unspecified type
            phone.setPhoneType(getDefaultPhoneType());
        }
        phone.setCreatedBy(ua);
        phone.setLastUpdatedBy(ua);
        phone.setHumanID(p.getHumanID());
        ContactPhone freshPhone = getContactPhone(pi.insertContactPhone(phone));
        getPersonCacheManager().flush(p);
        return freshPhone;
    }
    
    /**
     * Looks up and returns the default phone type
     * @return
     * @throws IntegrationException 
     */
    public ContactPhoneType getDefaultPhoneType() throws IntegrationException{
        PersonIntegrator pi = getPersonIntegrator();
        return pi.getContactPhoneType(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE)
                                .getString("default_phone_type")));
    }
    
    /** EMAILS ****/
    
    /**
     * Factory method for ContactEmail objects
     * @return 
     */
    public ContactEmail createContactEmailSkeleton(){
        return new ContactEmail();
        
    }
    
    /**
     * Convenience method for extracting an entire list of emails
     * from the database, all configured and ready for injection into a Person!
     * @param emids
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public List<ContactEmail> getContactEmailList(List<Integer> emids) throws IntegrationException{
        List<ContactEmail> emailList = new ArrayList<>();
        if(emids != null && !emids.isEmpty()){
            for(Integer i: emids){
                emailList.add(getContactEmail(i));
            }
        }
        return emailList;
    }
    
    /**
     * Type safe email getter
     * @param ce
     * @return
     * @throws IntegrationException 
     */
    public ContactEmail getContactEmail(ContactEmail ce) throws IntegrationException{
        if(ce != null){
            return getContactEmail(ce.getEmailID());
        } else {
            return null;
        }
    }
    
    
    /**
     * Retrieves a single ContactEmail from the DB
     * @param emid
     * @return 
     */
    public ContactEmail getContactEmail(int emid) throws IntegrationException{
        PersonIntegrator pi = getPersonIntegrator();
        return configureContactEmail(pi.getContactEmail(emid));
    }
    
    /**
     * Internal logic method for configuring the ContactEmail objects
     * @param ce
     * @return 
     */
    private ContactEmail configureContactEmail(ContactEmail ce){
        // Nothing to do here yet
        return ce;
    }
    
    
    /**
     * Logic intermediary for updating email records 
     * @param em
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void contactEmailUpdate(ContactEmail em, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException{
        PersonIntegrator pi = getPersonIntegrator();
        if(em == null || ua == null){
            throw new BObStatusException("Cannot update email with null email, or user");
        }
        if(!permissionCheckpointUpdatePerson(ua)){
            throw new AuthorizationException("User lacks permissions to insert/update person related data");
        }
        em.setLastUpdatedBy(ua);
        pi.updateContactEmail(em);
        getPersonCacheManager().flush(em);
        
        
    }
    
   
    
    /**
     * Hiding an email is done by setting the priority to a negative value
     * and this email should then no show up on pretty printing
     *
     * @param em
     * @param hide true for hide, false for unhide
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void contactEmailHideUnhide(ContactEmail em, boolean hide, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        
        PersonIntegrator pi = getPersonIntegrator();
        if(em == null || ua == null){
            throw new BObStatusException("Cannot hide email with null email, or user");
        }
        if(!permissionCheckpointUpdatePerson(ua)){
            throw new AuthorizationException("User lacks permissions to insert/update person related data");
        }
        if(hide){
            em.setPriority(Constants.HIDDEN_RECORD_PRIORITY);
        } else {
            em.setPriority(Constants.UNHIDDEN_RECORD_PRIORITY);
        }
        em.setLastUpdatedBy(ua);
        pi.hideContactEmail(em);
        getPersonCacheManager().flush(em);
        
        
    }
    
    /**
     * Logic intermediary for adding new email addresses to the DB
     * @param em
     * @param p
     * @param ua 
     * @return the new ContactEmail with a DB key
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public ContactEmail contactEmailAdd(ContactEmail em, Person p, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException{
        PersonIntegrator pi = getPersonIntegrator();
        if(em == null || p == null || ua == null){
            throw new BObStatusException("Cannot write new email with null email, person, or user");
        }
        if(!permissionCheckpointUpdatePerson(ua)){
            throw new AuthorizationException("User lacks permissions to insert/update person related data");
        }
        em.setCreatedBy(ua);
        em.setLastUpdatedBy(ua);
        em.setHumanID(p.getPersonID());
        ContactEmail freshEmail = getContactEmail(pi.insertContactEmail(em));
        getPersonCacheManager().flush(p);
        return freshEmail;
    }
    
    /**
     * Logic block for deactivating an email
     * @param em to deac
     * @param ua doing the deactivation
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public void contactEmailDeactivate(ContactEmail em, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        if(em == null || ua == null){
            throw new BObStatusException("Cannot Deac an email with null email or user");
        }
        if(!permissionCheckpointUpdatePerson(ua)){
            throw new AuthorizationException("User lacks permissions to insert/update person related data");
        }
        PermissionsCoordinator permCoor = getPermissionsCoordinator();
        PersonIntegrator pi = getPersonIntegrator();
        
        permCoor.permissionsCheckpointCreatorEditDeac(em, ua);
        em.setDeactivatedBy(ua);
        em.setDeactivatedTS(LocalDateTime.now());
        em.setLastUpdatedBy(ua);
        pi.updateContactEmail(em);
        getPersonCacheManager().flush(em);
        
    }
    
   
    
    
    /**
     * Logic container method for creating a Ghost from a given Person--which
     * is a copy of a person at a given time to be used in recreating official documents
     * with addresses and such, like Citations, NOVs, etc.
     * @param p of which you would like to create a Ghost
     * @param u doing the connecting
     * @return the database identifier of the sent in Person's very own ghost
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public int createChostPerson(Person p, User u) throws IntegrationException, BObStatusException{
        PersonIntegrator pi = getPersonIntegrator();
        int newGhostID = pi.createGhost(p, u);
        return newGhostID;        
        
    }
    
    /**
     * Logic container for creating a clone of a person, which is an exact copy of 
     * a Person at a given point in time that is used to safely edit person info 
     * without allowing certain levels of users access to the primary Person record
     * from which there is no recovery of core info
     * 
     * Ghosts are friendly and invited; clones are an unedesirable manifestation
     * of the modern biotechnological era
     * 
     * @param p of which you would like to create a clone
     * @param u doing the creating of clone
     * @return the database identifer of the inputted Person's clone
     * @throws IntegrationException 
     */
    public int createClonedPerson(Person p, User u) throws IntegrationException{
        PersonIntegrator pi = getPersonIntegrator();
        int newCloneID = pi.createClone(p, u);
        return newCloneID;
    }
    
    
    /**
     * Logic container for choosing a default person if the SessionInitializer
     * does not have a session List to work from. Currently it just grabs
     * the UserAuthorized's Person
     * @param cred
     * @return the selected person proposed for becoming the sessionPerson
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public Person selectDefaultPerson(Credential cred) throws IntegrationException{
        UserCoordinator uc = getUserCoordinator();
        PersonCoordinator pc = getPersonCoordinator();
        try {
            User u = uc.user_getUser(cred.getGoverningAuthPeriod().getUserID());
            System.out.println("PersonCoordinator.selectDefaultPerson: " + u);
            return pc.getPerson(u.getHuman());
        } catch (BObStatusException ex) {
            throw new IntegrationException(ex.getMessage());
        }
        
    }

    
    /**
     * Intermediary logic unit for configuring histories of Person object views
 given an authorization context
 * 
 *  TODO: Update for humanization
 * 
     * @param cred
     * @return
     * @throws IntegrationException 
     */
    public List<Human> assembleHumanHistory(Credential cred) throws IntegrationException{
        PersonIntegrator pi = getPersonIntegrator();
        List<Human> pl = new ArrayList<>();
        List<Integer> idList = null;
        if(cred != null){
            idList = pi.getPersonHistory(cred.getGoverningAuthPeriod().getUserID());
            while(!idList.isEmpty() && pl.size() <= Constants.MAX_BOB_HISTORY_SIZE){
//                pl.add(pi.getPersonByHumanID(idList.remove(0)));
            }
        }
        return pl;
    }   
    
    /**
     *
     * @param hidl A list of person IDs from the database.
     * @return
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public List<Human> assembleHumanList(List<Integer> hidl) throws IntegrationException{
        
        List<Human> skeletonHorde = new ArrayList<>();
        PersonIntegrator pi = getPersonIntegrator();
        
        for (int idNum : hidl){
            skeletonHorde.add(pi.getHuman(idNum));
        }
        return skeletonHorde;
    }
    
    /**
     * Utility method for getting a list of Person objects from a list of
     * Human objs
     * @param hl
     * @return a list, possibly not null, of Person objs
     * @throws IntegrationException 
     */
    public List<Person> assemblePersonListFromHumanList(List<Human> hl) throws IntegrationException, BObStatusException{
        List<Person> pl = new ArrayList<>();
        if(hl != null && !hl.isEmpty()){
            for(Human h: hl){
                pl.add(getPerson(h));
            }
        }
        return pl;
        
    }

    /**
     * Utility method for dumping PersonType values in an Enum to an array
     * @return the personTypes
     */
    public PersonType[] getPersonTypes() {
        
        return personTypes;
    }
    
    /**
     * Returns the master list of all contact phone types
     * e.g. cell phone, home, work, etc. 
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public List<ContactPhoneType> getContactPhoneTypeList() throws IntegrationException{
        PersonIntegrator pi = getPersonIntegrator();
        return pi.getContactPhoneTypeList();
        
    }

    
    /**
     * TODO: Finsih for humanization
     * @param personList
     * @return 
     */
    public List<Person> anonymizePersonList(List<Person> personList) {
        for (Person person:personList){
//            anonymizePersonData(person);
        }
        return personList; 
    }
    

    /**
     * Anonymizes Person object member variables, for use in the case of a public search. The 
     * anonymized fields should still be recognizable if one knows what they are likely to be, but
     * someone without that knowledge should not be able to guess the address, phone number, email etc.
     *
     * TODO: Upgrade for Humanization
     * 
     * 
     * @param person
     * @return 
     */
    public Person anonymizePersonData(Person person){
        
        
        // anonymize all but first two characters of last name
        if(person.getLastName() != null) {
            StringBuilder last = new StringBuilder(person.getLastName());
            for (int i = 2; i < last.length() && i >= 0; i++){
                last.setCharAt(i, '*');
            }
            person.setName(last.toString());
        }
        
        // anonymize all but first three characters and the domain of an email address
        if (person.getEmail() != null) {
            StringBuilder email = new StringBuilder(person.getEmail());
            for (int i = 3; i < email.length() &&  email.charAt(i) != '@' && i >= 0; i++){
                email.setCharAt(i, '*');
            }
//            person.setEmail(email.toString());
        }
        
        // anonymize a
        return person;
    }
    
 
    /**
     * Attempt at archiving state of a pre-human person
     * to track changes to field names; come up with new approach
     * Post humanization
     * 
     * @Deprecated 
     * @param p
     * @return 
     */
    public String dumpPerson(Person p){
        SystemCoordinator sc = getSystemCoordinator();
        StringBuilder sb = new StringBuilder();
        
        sb.append(Constants.FMT_HTML_BREAK);
        sb.append(Constants.FMT_HTML_BREAK);
        sb.append(Constants.FMT_NOTE_SEP_INTERNAL);
        sb.append("Field dump of Person ID: ");
        sb.append(p.getHumanID());
        sb.append(Constants.FMT_HTML_BREAK);
        sb.append("Timestamp: ");
        sb.append(sc.stampCurrentTimeForNote());
        sb.append(Constants.FMT_HTML_BREAK);
        
        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getPersonType().getLabel());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getMuniCode());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getMuniName());
        sb.append(Constants.FMT_HTML_BREAK);

        if(p.getSource() != null){
            sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
            sb.append(p.getSource().getTitle());
            sb.append(Constants.FMT_HTML_BREAK);
        }

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getCreatorUserID());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getCreationTimeStamp());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
        sb.append(p.getFirstName());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
        sb.append(p.getLastName());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.isCompositeLastName());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
        sb.append(p.isBusinessEntity());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
        sb.append(p.getJobTitle());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getPhoneCell());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getPhoneHome());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getPhoneWork());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
        sb.append(p.getEmail());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
        sb.append(p.getAddressStreet());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
        sb.append(p.getAddressCity());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
        sb.append(p.getAddressZip());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
        sb.append(p.getAddressState());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.isUseSeparateMailingAddress());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getMailingAddressStreet());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getMailingAddressThirdLine());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getMailingAddressCity());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getMailingAddressZip());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getMailingAddressState());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
        sb.append(p.getNotes());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getLastUpdated());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getLastUpdatedPretty());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.isCanExpire());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getExpiryDate());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getExpireString());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getExpiryNotes());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
        sb.append(p.isActive());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getLinkedUserID());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
        sb.append(p.isUnder18());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getVerifiedByUserID());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.isReferencePerson());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getGhostCreatedDate());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getGhostCreatedDatePretty());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getGhostOf());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getGhostCreatedByUserID());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getCloneCreatedDate());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getCloneCreatedDatePretty());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getCloneOf());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getCloneCreatedByUserID());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getGhostsList());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getCloneList());
        sb.append(Constants.FMT_HTML_BREAK);

        sb.append(Constants.FMT_FIELDKVSEP_WSPACE);
//        sb.append(p.getMergedList());
        sb.append(Constants.FMT_HTML_BREAK);
        
        return sb.toString();
    }
    
    /**
     * TODO Upgrade for humanization
     * @param personID
     * @return
     * @throws IntegrationException 
     */
    public PersonWithChanges getPersonWithChanges(int personID) throws IntegrationException{
        
//        PersonWithChanges skeleton = new PersonWithChanges(getPersonByHumanID(personID));
        
        PersonIntegrator pi = getPersonIntegrator();
        
//        skeleton.setChangeOrderList(pi.getPersonChangeOrderListAll(personID));
        
        return null;
//        return skeleton;
        
    }
    
    public List<PersonWithChanges> getPersonWithChangesList(List<Person> personList) throws IntegrationException{
        
        List<PersonWithChanges> skeletonHorde = new ArrayList<>();
        
        for(Person input : personList){
            
            skeletonHorde.add(getPersonWithChanges(input.getHumanID()));
            
        }
        
        return skeletonHorde;
        
    }
    
    public List<PersonWithChanges> getPersonWithChangesListUsingID(List<Integer> personIDList) throws IntegrationException{
        
        List<PersonWithChanges> skeletonHorde = new ArrayList<>();
        
        for(Integer input : personIDList){
            skeletonHorde.add(getPersonWithChanges(input));
        }
        
        return skeletonHorde;
    }
}
