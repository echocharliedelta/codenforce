/*
 * Copyright (C) 2023 Technology Rediscovery LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.coordinators;

import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.entities.CECase;
import com.tcvcog.tcvce.entities.CECase;
import com.tcvcog.tcvce.entities.IFace_PermissionsCreatorRightsPreserved;
import com.tcvcog.tcvce.entities.IFace_PermissionsMuniFenced;
import com.tcvcog.tcvce.entities.PermissionsGovernedOperationEnum;
import com.tcvcog.tcvce.entities.UserAuthorized;
import com.tcvcog.tcvce.entities.IFace_PermissionsGoverned;
import com.tcvcog.tcvce.entities.Property;
import jakarta.security.auth.message.AuthException;

/**
 * Created during the 1.4.1 permissions release to hold cross-subsystem permissions
 * rules.
 * 
 * Home to processing methods for permissions related interfaces, notably these two:
 * IFace_PermissionsCreatorRightsPreserved
 * IFace_PermissionsMuniFenced
 * 
 * @author Ellen Bascomb of Apartment 31Y
 */
public class PermissionsCoordinator {
    
    
    /**
     * Permissions logic for forbidding users without sys admin permissions from viewing
     * objects outside their muni which are fenced in by municipality, such as properties, 
     * but not codeelements.
     * @param muniFenced
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void enforceMuniViewFence(IFace_PermissionsMuniFenced muniFenced, UserAuthorized ua) 
            throws BObStatusException, AuthorizationException {
        if(muniFenced == null || muniFenced.getGoverningMunicipality() == null || ua == null ){
            throw new BObStatusException("Cannot enforce muni fence on null fenced object or user");
        }
        // The exception for authorized cross muni object views which as of June 2023 included
        // viewing CE cases of persons who have been linked to a citation
        if(ua.getCrossMuniViewAuthorizationTS() != null){
            return;
        }
        if(!ua.getKeyCard().isHasSysAdminPermissions()){
            if(!muniFenced.getGoverningMunicipality().equals(ua.getKeyCard().getGoverningAuthPeriod().getMuni())){
                throw new AuthorizationException("Permission denied: Your current user rank does not allow cross-muni object views");
            }
        }
    }
    
    
    
    /**
     * Permissions logic for edit and deac of a family of objects whose creators hold
     * special downstream permissions
     * 
     * @param opTarget
     * @param ua
     * @return 
     */
    public boolean permissionsCheckpointCreatorEditDeac(IFace_PermissionsCreatorRightsPreserved opTarget, UserAuthorized ua){
        if(ua == null || opTarget == null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        if(opTarget.getCreatorUserID() == ua.getUserID()){
            return true;
        }
        return false;
        
    }
    
    /**
     * Returns true if inputted user has system admin permissions
     * @param ua
     * @return true if ua is system admin
     */
    public boolean permissionsCheckpointSystemAdminRank(UserAuthorized ua){
        if(ua == null){
            return false;
        }
        return ua.getKeyCard().isHasMuniStaffPermissions();
    }
    
     
    /**
     * Single point logic holder enforcing a muni staff role floor on all
     * object edits used for internal PermissionsGoverned instances
     * @param ua
     * @return 
     */
    private boolean checkPermissionsGovernedEditFloor(UserAuthorized ua){
        if(ua != null && ua.getKeyCard() != null){
            return ua.getKeyCard().isHasMuniStaffPermissions();
        }
        return false;
    }
    
   
    
    /**
     * Single point logic holder enforcing a muni staff role floor on all
     * object edits. 
     * @param ua
     * @return true if the user has muni staff or better permissions; Always false during 
     * xmuni mode
     */
    public boolean verifyGeneralEditFloor(UserAuthorized ua){
        if(ua == null){
            return false;
        }
        if(ua.getCrossMuniViewAuthorizationTS() != null){
            return false;
        }
        if(ua.getKeyCard() != null){
            return ua.getKeyCard().isHasMuniStaffPermissions();
        }
        return false;
    }
    
    
    /**
     * Single point logic holder enforcing the lowest floor, 
     * muni reader only. 

     * @param ua
     * @return 
     */
    public boolean verifyReadOnlyStatus(UserAuthorized ua){
        if(ua != null && ua.getKeyCard() != null){
            return ua.getKeyCard().isHasMuniReaderPermissions();
        }
        return false;
    }
    
    
    
    
    /**
     * Grand entrance for all permissions related checkpoint logic
     * 
     * @Deprecated overbuilding; never put into use
     * 
     * @param governed
     * @param op
     * @param ua
     * @return if operation is allowed
     * @throws BObStatusException 
     */
    public boolean checkOperationPermission(   IFace_PermissionsGoverned governed, 
                                                PermissionsGovernedOperationEnum op, 
                                                UserAuthorized ua) throws BObStatusException{
        if(op == null || ua == null){
            throw new BObStatusException("Cannot check operation permission with operation enum, or requesting user");
        }
       
       // central system admin pass through before ANY logic 
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        
        switch(op){
            case INIT -> {
                return permissionCheckInit(governed, ua);
            }
            case VIEW -> {
                return permissionCheckView(governed, ua);
            }
            case EDIT -> {
                permissionCheckEdit(governed, ua);
            }
            case DEAC -> {
                permissionCheckDeac(governed, ua);
            }
        }
     
        
        return false;
    }
    
    /**
     * Router for permissions checks for all INIT operations
     * @Deprecated overbuilding; never put into use
     * @param governed
     * @param ua
     * @return 
     */
    private boolean permissionCheckInit(IFace_PermissionsGoverned governed, UserAuthorized ua){
        
        if(checkPermissionsGovernedEditFloor(ua)){
            
        } 
        return false;
    }
    
    /**
     * Router for permissions checks on all VIEW operations
     * @Deprecated overbuilding; never put into use
     * @param governed
     * @param ua
     * @return 
     */
    private boolean permissionCheckView(IFace_PermissionsGoverned governed, UserAuthorized ua){
        
        if(checkPermissionsGovernedEditFloor(ua)){
            if(governed instanceof CECase cse){
              
            } 
        } 
        return false;
    }
    
    /**
     * Router for permissions checks on all EDIT operations
     * @Deprecated overbuilding; never put into use
     * @param governed
     * @param ua
     * @return 
     */
    private boolean permissionCheckEdit(IFace_PermissionsGoverned governed, UserAuthorized ua){
        
        if(checkPermissionsGovernedEditFloor(ua)){
            if(governed instanceof CECase cse){
            } 
        } 
        return false;
    }
    
    /**
     * Router for permissions checks on all DEAC operations
     * @Deprecated overbuilding; never put into use
     * @param governed
     * @param ua
     * @return 
     */
    private boolean permissionCheckDeac(IFace_PermissionsGoverned governed, UserAuthorized ua){
        
        if(checkPermissionsGovernedEditFloor(ua)){
            if(governed instanceof CECase cse){
            } 
        } 
        return false;
    }

   
    
    
}
