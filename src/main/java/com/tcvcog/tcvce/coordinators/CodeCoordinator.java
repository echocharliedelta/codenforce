/*
 * Copyright (C) 2017 Turtle Creek Valley
Council of Governments, PA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.coordinators;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.domain.AuthorizationException;
import com.tcvcog.tcvce.domain.BObStatusException;
import com.tcvcog.tcvce.domain.DatabaseFetchRuntimeException;
import com.tcvcog.tcvce.domain.IntegrationException;
import com.tcvcog.tcvce.entities.CodeElement;
import com.tcvcog.tcvce.entities.CodeElementGuideEntry;
import com.tcvcog.tcvce.entities.CodeSet;
import com.tcvcog.tcvce.entities.CodeSource;
import com.tcvcog.tcvce.entities.CodeViolation;
import com.tcvcog.tcvce.entities.EnforceableCodeElement;
import com.tcvcog.tcvce.entities.HumanLink;
import com.tcvcog.tcvce.entities.IFaceFindingsHolder;
import com.tcvcog.tcvce.entities.IntensitySchema;
import com.tcvcog.tcvce.entities.LinkedObjectRole;
import com.tcvcog.tcvce.entities.MuniProfile;
import com.tcvcog.tcvce.entities.Municipality;
import com.tcvcog.tcvce.entities.OrdinanceHierarchyEnum;
import com.tcvcog.tcvce.entities.TextBlock;
import com.tcvcog.tcvce.entities.TreeNodeOrdinanceWrapper;
import com.tcvcog.tcvce.entities.UserAuthorized;
import com.tcvcog.tcvce.entities.occupancy.OccInspectedSpaceElement;
import com.tcvcog.tcvce.integration.CECaseCacheManager;
import com.tcvcog.tcvce.integration.CodeIntegrator;
import com.tcvcog.tcvce.integration.MunicipalityIntegrator;
import com.tcvcog.tcvce.integration.OccInspectionCacheManager;
import com.tcvcog.tcvce.integration.SystemIntegrator;
import com.tcvcog.tcvce.util.Constants;
import com.tcvcog.tcvce.util.MessageBuilderParams;
import com.tcvcog.tcvce.util.StringUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 * The controller class for all things code element (i.e. ordinances)
 * @author ellen bascomb of apt 31y
 */
public class CodeCoordinator extends BackingBeanUtils implements Serializable {
    
    final int ECE_DEFAULT_PENALTY_MAX = 1000;
    final int ECE_DEFAULT_PENALTY_NORM = 50;
    final int ECE_DEFAULT_PENALTY_MIN = 10;
    final String ECE_DEFAULT_PENALTY_NOTES = "Default values";
    final int ECE_DEFAULT_DAYS_TO_COMPLY = 30;
    final String ECE_DEFAULT_DAYSTOCOMPLY_NOTES = "Default values";
    final static String VIOLATION_SEVERITY_SCHEMA = "violationseverity";
    
    
    final static String FMT_SECTION_ENTITY = "&#167;";
    
    final static String FMT_CH = "ch.";
    final static String FMT_SPACE = " ";
    final static String FMT_PAREN_L = "(";
    final static String FMT_PAREN_R = ")";
    final static String FMT_DASH = "-";
    final static String FMT_COMMA = ",";
    final static String FMT_COLON = ":";
    final static String EMPTY_STRING = "";
    
    
    /**
     * Creates a new instance of CodeCoordinator
     */
    public CodeCoordinator() {
    }
    
    // *************************************************************
    // ******               PERMISSIONS                    *********
    // *************************************************************
   
    
    /**
     * Permissions logic for adding & editing code source metadata and
     * a source's associated CodeElements.
     * 
     * Note that code sourced marked as cross muni will require rank: sys admin
     * for any editing. 
     * 
     * Relaxed permissions for any non-ready only user during beta testing
     * 
     * @param src can be null, in which case permissions for creation are returned
     * @param ua
     * @return if current user can manage code sources and their CodeElements
     */
     public boolean permissionsCheckpointManageCodeSource(CodeSource src, UserAuthorized ua){
        if(ua == null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        if(!ua.getKeyCard().isHasMuniStaffPermissions()){
            return false;
        }
        if(src != null && src.getMuniCodeFlattened() != 0 ){
            // sys admin rank required for editing cross-muni code sources
            if(src.isCrossMuni() && !ua.getKeyCard().isHasSysAdminPermissions()){
                return false;
            }
            if(src.getMuniCodeFlattened() != ua.getKeyCard().getGoverningAuthPeriod().getMuni().getMuniCode()){
                return false;
            }
        }
        if(ua.getGoverningMuniProfile().isManagerRequiredCodeSourceManage() && !ua.getKeyCard().isHasMuniManagerPermissions()){
            return false;
        }
        return true;
    }
    
   
    
     /**
      * Permissions logic bundle for code book management, optionally restricted to
      * users with rank: manager+ by the muniprofile
      * 
      * @param codeSet
      * @param ua
      * @return if the user can manage code books and their CodeSetElements
      */
     public boolean permissionsCheckpointManageCodeBook(CodeSet codeSet, UserAuthorized ua){
        if(ua == null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        if(!ua.getKeyCard().isHasMuniStaffPermissions()){
            return false;
        }
        if(codeSet != null && codeSet.getMuniCodeFlattened()!= 0){
            if(codeSet.getMuniCodeFlattened() != ua.getKeyCard().getGoverningAuthPeriod().getMuni().getMuniCode()){
                return false;
            }
        }
        if(ua.getGoverningMuniProfile().isManagerRequiredCodeBookManage() && !ua.getKeyCard().isHasMuniManagerPermissions()){
            return false;
        }
        return true;
    }
    
     /**
      * Permissions logic bundle for code book (codeset) deactivation, 
      * 
      * @param codeSet
      * @param ua
      * @return if the user can manage code books and their CodeSetElements
      */
     public boolean permissionsCheckpointDeacCodeSet(CodeSet codeSet, UserAuthorized ua){
        if(ua == null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        if(!ua.getKeyCard().isHasMuniStaffPermissions()){
            return false;
        }
       
        return true;
    }
     
     
     /**
      * Permissions check for canned findings
      * @param ua
      * @return 
      */
     public boolean permissionsCheckpointManageCannedFindings(UserAuthorized ua){
        if(ua == null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        if(!ua.getKeyCard().isHasMuniStaffPermissions()){
            return false;
        }
       
        return true;
    }
    
     /**
      * Permissions logic bundle for code source and sub-objects deactivation, 
      * 
     * @param src
      * @param ua
      * @return if the user can manage code books and their CodeSetElements
      */
     public boolean permissionsCheckpointDeacCodeSource(CodeSource src, UserAuthorized ua){
        if(ua == null){
            return false;
        }
        if(ua.getKeyCard().isHasSysAdminPermissions()){
            return true;
        }
        if(!ua.getKeyCard().isHasMuniStaffPermissions()){
            return false;
        }
        
        return true;
    }
    
    // *************************************************************
    // *********************CODE SOURCES****************************
    // *************************************************************
        
    /**
     * Factory for CodeSource objects
     * @return 
     */
    public CodeSource getCodeSourceSkeleton(){
        return new CodeSource();
    }
    
    /**
     * Retrieval method for code sources
     * @param sourceID
     * @return
     * @throws IntegrationException 
     */
    public CodeSource getCodeSource(int sourceID) throws IntegrationException, BObStatusException{
        CodeIntegrator ci = getCodeIntegrator();
        CodeSource src = ci.getCodeSource(sourceID);
        src = configureCodeSource(src);
        return src;
        
    }
    
    private CodeSource configureCodeSource(CodeSource src){
        if(src != null){
            // undertake configuration options here
        }
        return src;
    }
   
    
     /**
     * Primary getter for lists of CodeSource objects
     * @param includeDeacs
     * @return
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public List<CodeSource> getCodeSourceList(boolean includeDeacs) throws IntegrationException, BObStatusException{
        CodeIntegrator integrator = getCodeIntegrator();
        return getCodeSourceList(integrator.getCompleteCodeSourceIDList(includeDeacs));
    }
    
    /**
     * Utility for assembling a list of code sources by ID list
     * @param idList
     * @return 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public List<CodeSource> getCodeSourceList(List<Integer> idList) throws IntegrationException, BObStatusException{
        
        List<CodeSource> sourceList = new ArrayList<>();
        if(idList != null && !idList.isEmpty()){
            for(Integer i: idList){
                sourceList.add(getCodeSource(i));
            }
        }
        return sourceList;
        
    }
    
    /**
     * Utility for converting a List of CodeSource objects to an Integer array of
     * their IDs for injection into an array field in the DB
     * @param sources
     * @return 
     */
    public Integer[] getCodeSourceIDArrayFromSourceList(List<CodeSource> sources){
        Integer[] ids = null;
        if(sources != null && !sources.isEmpty()){
            ids = (Integer[]) sources.stream().map(CodeSource::getSourceID).toArray(Integer[]::new);
        }
        return ids;
    
    }
    
    /**
     * Logic pass through for insertion of CodeSource objects
     * @param source
     * @param ua
     * @param profile
     * @return 
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public int addNewCodeSource(CodeSource source, UserAuthorized ua, MuniProfile profile) 
            throws IntegrationException, BObStatusException, AuthorizationException{
        if(source == null || ua == null || profile == null){
            throw new BObStatusException("Cannot insert new code source with null source, user, or profile");
        } 
        if(!permissionsCheckpointManageCodeSource(source, ua)){
            throw new AuthorizationException("Permission denied to update code source metadata"); 
        }
        source.setCreatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
        source.setLastUpdatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
        return getCodeIntegrator().insertCodeSource(source);
    }
    
    /**
     * Logic pass through for updates to code sources
     * @param source
     * @param ua
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void updateCodeSourceMetadata(CodeSource source, UserAuthorized ua) 
            throws IntegrationException, BObStatusException, AuthorizationException{
        if(source == null || ua == null){
            throw new BObStatusException("Cannot update code source with null source or user"); 
        }
        CodeIntegrator ci = getCodeIntegrator();
        if(!permissionsCheckpointManageCodeSource(source, ua)){
            throw new AuthorizationException("Permission denied to update code source metadata");  
        }
        source.setLastUpdatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
        ci.updateCodeSourceMetadata(source);
        getCodeCacheManager().flushCodeElementConnectedCaches();
        
    }
    
    /**
     * Logic pass through for deactivation of a code source. This method 
     * doesn't touch any of the underlying objects inside a code source but we probably
     * do want to deac the underlying elements, too.
     * @param source
     * @param ua
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void deactivateCodeSource(CodeSource source, UserAuthorized ua) 
            throws IntegrationException, BObStatusException, AuthorizationException{
        CodeIntegrator ci = getCodeIntegrator();
        if(source == null || ua == null){
            throw new BObStatusException("Cannot deac code source with null source or user"); 
        }
         if(!permissionsCheckpointManageCodeSource(source, ua)){
            throw new AuthorizationException("Permission denied to update code source metadata"); 
        }
        source.setDeactivatedBy_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
        ci.deactivateCodeSource(source);
        getCodeCacheManager().flush(source);
    }
    
    
    /**
     * Deep copies the given cloneParent into a new source
     * @param cloneParent fully baked code source with an ID which will become the genetic material for the clone
     * @param cloneSkeleton source without an ID (id == 0) but with the meta data that the clone should have
     * @param ua requesting the clone
     * @return the cloned source, with an ID and all the parent's cloned elements in its belly
     */
    public CodeSource cloneCodeSource(CodeSource cloneParent, CodeSource cloneSkeleton, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        SystemCoordinator sc = getSystemCoordinator();
        
        if(cloneParent == null || cloneSkeleton == null || ua == null){
            throw new BObStatusException("cannot clone source with null parent, skeleton, or requesting user");
        }
        if(cloneSkeleton.getSourceID() != 0){
            throw new BObStatusException("Skeleton input source must have an ID of zero");
        }
        // start by inserting the new code source
        int freshSourceID = addNewCodeSource(cloneSkeleton, ua, ua.getGoverningMuniProfile());
        // get our fresh source ready for the cloned elements
        CodeSource clonedSource = getCodeSource(freshSourceID);
        
        // build a note about this source's cloned origins
        StringBuilder sb = new StringBuilder();
        sb.append("This code source was created as a clone of code source: ");
        sb.append(cloneParent.getSourceName());
        if(cloneParent.getSourceYear() != 0){
            sb.append(" (").append(cloneParent.getSourceYear()).append(" )");
        }
        sb.append(" | ID: ").append(cloneParent.getSourceID()).append(Constants.FMT_HTML_BREAK);
        
        List<CodeElement> elementListToClone = getCodeElemements(cloneParent);
        
        if(elementListToClone != null && !elementListToClone.isEmpty()){
            int cloneCount = 0;
            sb.append("Count of ordinances to clone: ").append(elementListToClone.size()).append(Constants.FMT_HTML_BREAK);
            for(CodeElement sourceElement: elementListToClone){
                try{
                    System.out.println("CodeCoordinator.cloneCodeSource | cloning element ID: " + sourceElement.getElementID());
                    CodeElement clonedEle = new CodeElement(sourceElement);
                    // give our fresh element object its new parent
                    clonedEle.setSource(clonedSource);
                    int freshEleID = insertCodeElement(clonedEle, ua);
                    getCodeElement(freshEleID);
                    cloneCount++;
                } catch (AuthorizationException | BObStatusException | IntegrationException ex){
                    System.out.println(ex);
                    sb.append("Error cloning ordinance ID: ").append(sourceElement.getElementID()).append(" with header: ").append(sourceElement.getHeaderString()).append(Constants.FMT_HTML_BREAK);
                }
            }
            sb.append("Successfully cloned ").append(cloneCount).append(" ordinances! ").append(Constants.FMT_HTML_BREAK);
        } else {
            sb.append("Source to clone contained zero ordinances to clone; done with clone!");
        }
               
        // tidy up our notes
        MessageBuilderParams mbp = new MessageBuilderParams(null, "This source is a clone!","", sb.toString(), ua, null);
        clonedSource.setNotes(sc.appendNoteBlock(mbp));
        sc.writeNotes(clonedSource, ua);
        return getCodeSource(freshSourceID);
        
    }
    
    
    // *************************************************************
    // **************CODE ELEMENTS (ORDINANCES)*********************
    // *************************************************************
    
    /**
     * Primary factory method for CodeElement objects
     * @param src the parent of this element
     * @return 
     */
    public CodeElement getCodeElementSkeleton(CodeSource src){
        CodeElement ele = new CodeElement();
        ele.setSource(src);
        return ele;
    }
    
      
    /**
     * Primary getter for all code elements system-wide
     * @param eleid
     * @return
     * @throws IntegrationException 
     */
    public CodeElement getCodeElement(int eleid) throws IntegrationException{
        CodeIntegrator ci = getCodeIntegrator();
        try {
            return configureCodeElement(ci.getCodeElement(eleid));
        } catch (DatabaseFetchRuntimeException ex) {
            throw new IntegrationException(ex.getMessage());
        }
    }
    
    /**
     * Used to create a new code element based on an existing one; This method
     * clones the chapter, sec, sub sec, and sub sub sec numbers but not the sub-sub title
     * if it exists
     * @param source
     * @param target
     * @return 
     */
    public CodeElement cloneCodeElementChapSec(CodeElement source, CodeElement target) throws BObStatusException{
        if(source == null || target == null){
            throw new BObStatusException("Cannot clone element with null inputs");
        }
        
        target.setOrdchapterNo(source.getOrdchapterNo());
        target.setOrdchapterTitle(source.getOrdchapterTitle());
        
        target.setOrdSecNum(source.getOrdSecNum());
        target.setOrdSecTitle(source.getOrdSecTitle());
        
        target.setOrdSubSecNum(source.getOrdSubSecNum());
        target.setOrdSubSecTitle(source.getOrdSubSecTitle());
        
        target.setOrdSubSubSecNum(source.getOrdSubSubSecNum());
        target.setOrdSubSubSecTitle(source.getOrdSubSubSecTitle());
        
        target.setNotes("Ordinance based on clone of element ID: " + source.getElementID());
        
        return target;
        
    }

    /**
     * Logic bundle for code elements; 
     * @param ce
     * @return 
     */
    private CodeElement configureCodeElement(CodeElement ce) {
            StringBuilder hlog = new StringBuilder();
            ce.setHeaderString(buildOrdinanceHeaderString(ce, hlog, false, true));
            ce.setNonIrcStyleHeaderString(buildNonIrcStyleHeaderString(ce, hlog, true)); 
            ce.setHeaderStringLog(hlog.toString());
            
        return ce;
    }
    
     /**
     * Uses logic generators in this method to write the header string of each given element, 
     * returning the same list with same objects but with their headers written.
     * 
     * DOES NOT WRITE TO DB and does not read from DB and doesn't pass ord through 
     * the standard system-wide config for code elements
     * @param eleList
     * @param forceIRC assumeIRCRecurstion
     * @param includeSource 
     * @return  
     */
    public List<CodeElement> generateHeaderStringForElementList(List<CodeElement> eleList, boolean forceIRC, boolean includeSource){
        if(eleList != null && !eleList.isEmpty()){
            for(CodeElement ele: eleList){
                generateHeaderForCodeElement(ele, forceIRC, includeSource);
            }
        }
        return eleList;
    }
    
     /**
      * Transfers the element's static headers to its candidate member
     * @param eleList
     * @return  
     */
    public List<CodeElement> generateHeaderStringCandidates(List<CodeElement> eleList){
        if(eleList != null && !eleList.isEmpty()){
            for(CodeElement ele: eleList){
                ele.setHeaderStringCandidate(ele.getHeaderStringStatic());
            }
        }
        return eleList;
    }
    
    /**
     * updates a single ordinance's header string
     * 
     * * DOES NOT WRITE TO DB and does not read from DB and doesn't pass ord through 
     * the standard system-wide config for code elements
     * 
     * @param ele
     * @param forceIRC
     * @param includeSource
     * @return 
     */
    public CodeElement generateHeaderForCodeElement(CodeElement ele, boolean forceIRC, boolean includeSource){
        StringBuilder log = new StringBuilder();
        if(forceIRC){
            ele.setHeaderStringCandidate(buildOrdinanceHeaderString(ele, log, true, includeSource));
        } else {
            ele.setHeaderStringCandidate(buildNonIrcStyleHeaderString(ele, log, includeSource));
        }
        ele.setHeaderStringLog(log.toString());
        return ele;
    }
    
    /**
     * Implements logic to create a title for a code element; Designed to be injected
     * into the code elements ordinance
     * 
     * TODO: Finish my guts
     * @param ce
     * @param hlog
     * @param forceIRC if true, no matter what the logic finds, the method will 
     * build a header assuming pure IRC recursive
     * @param includeSource format will prepend ordinance header string with source name and optionally year if nonzero
     * @return 
     */
    public String buildOrdinanceHeaderString(CodeElement ce, StringBuilder hlog, boolean forceIRC, boolean includeSource){
        StringBuilder sb = new StringBuilder();
         if(ce != null){
            // Prefix with source and year
            if(includeSource){
                appendCodeSourceAndMaybeYear(ce, sb, hlog);
            }
            
            // If we have a standard IPMC listing, the chapter and section
            // numbers are actually in the sub-section number, so just use that number
            // such as 302.1.1: Chapter 3, section 2, subsec 1, subsubsec 1
           
            // check if section number is in sub-section number
            if(checkForPureIRCRecursiveListing(ce) || forceIRC){
                sb.append(FMT_CH);
                sb.append(ce.getOrdchapterNo());
                sb.append(FMT_SPACE);
                sb.append(FMT_SECTION_ENTITY);
                sb.append(FMT_SPACE);
                if(containsPrintableContent(ce.getOrdSubSubSecNum())){
                    hlog.append("ICC: Found section no. in sub section no. <br />");
                    // eg 302.1.1
                    sb.append(ce.getOrdSubSubSecNum());
                    sb.append(FMT_SPACE);
                } else { // we don't have a recursive sub-sub sec number, so use sub-sec number
                    hlog.append("ICC: did not find recursive sub-sub sec no.<br />");
                    sb.append(FMT_SPACE);
                    sb.append(ce.getOrdSubSecNum());
                }
                // now insert the titles
                // NOTE we are not using chapter titles in the standard IRC
                // they are implied by the section and subsection titles
                
                if(containsPrintableContent(ce.getOrdSecTitle())){
                    hlog.append("ICC: found non-null sec title <br />");
                    sb.append(FMT_SPACE);
                    sb.append(ce.getOrdSecTitle());
                }
                if(containsPrintableContent(ce.getOrdSubSecTitle())){
                    sb.append(FMT_SPACE);
                    sb.append(FMT_DASH);
                    sb.append(FMT_SPACE);
                    hlog.append("ICC: found non-null sub sec title <br />");
                    sb.append(ce.getOrdSubSecTitle());
                }
                if(containsPrintableContent(ce.getOrdSubSubSecTitle())){
                    sb.append(FMT_SPACE);
                    sb.append(FMT_DASH);
                    sb.append(FMT_SPACE);
                    hlog.append("ICC: found non-null sub sub sec title <br />");
                    sb.append(ce.getOrdSubSubSecTitle());
                }
                
            // ----------- > BEGIN NON-IRC STYLE LISTING FORMATTING < ------------------
            } else { // We do not have a pure IRC, so piece the header together straight across
                sb.append(buildNonIrcStyleHeaderString(ce, hlog, includeSource));
            } // end outer if check on standard IRC 
//            System.out.println("CodeCoordinator.buildOrdinanceHeaderString | eleID:  " + ce.getElementID() );
//            System.out.println("CodeCoordinator.buildOrdinanceHeaderString | headerString:  " + sb.toString() );
        
        } // element not null
        return sb.toString();
    }
    
    /**
     * Extracts the code source year from the element and if not null, its name will be appended
     * to the given string builder. If year is not zero, the year will be included in (parens)
     * @param ele
     * @param sb
     * @return 
     */
    private void appendCodeSourceAndMaybeYear(CodeElement ce, StringBuilder sb, StringBuilder hlog){
        if(ce != null && sb != null && hlog != null){
            sb.append(ce.getSource().getSourceName());
            hlog.append("Found codesource <br />");
            if(ce.getSource().getSourceYear() != 0){
                hlog.append("Found source year <br />");
                sb.append(FMT_SPACE);
                sb.append(FMT_PAREN_L);
                sb.append(ce.getSource().getSourceYear());
                sb.append(FMT_PAREN_R);
                sb.append(FMT_COLON);
            }
            sb.append(FMT_SPACE);
        } 
    }

    /**
     * Implements login to build non IRC Style header string
     * @param ce
     * @param includeSource  
     * @param hlog
     * @return nonIrcStyleHeaderString
     */
    public String buildNonIrcStyleHeaderString(CodeElement ce, StringBuilder hlog, boolean includeSource) {
        StringBuilder sb = new StringBuilder();
        // we have a non-zero chapter number
        if(includeSource){
            appendCodeSourceAndMaybeYear(ce, sb, hlog);
        }
        if (ce.getOrdchapterNo() != 0) {
            hlog.append("Found nonzero ch num<br />");
            sb.append(FMT_CH);
            sb.append(ce.getOrdchapterNo());
            sb.append(FMT_COLON);
            sb.append(FMT_SPACE);
        }
        // REQUIRED FIELD (Not yet enforced on this end as of march 2023)
        // this is our required component, but we'll only enforce that on the way in
        if (containsPrintableContent(ce.getOrdchapterTitle())) {
            hlog.append("Found non-null ch title <br />");
            sb.append(ce.getOrdchapterTitle());
        }
        // setup our section
        // if we have a section number, put it in
        if (containsPrintableContent(ce.getOrdSecNum())) {
            hlog.append("Found non-null sec num<br />");
            sb.append(FMT_SPACE);
            sb.append(FMT_SECTION_ENTITY);
            sb.append(FMT_SPACE);
            sb.append(ce.getOrdSecNum());
            // if we have a sub sec number, it follows in parens ex: (a)
            if (containsPrintableContent(ce.getOrdSubSecNum())) {
                hlog.append("Found non-null sub sec num<br />");
                sb.append(FMT_PAREN_L);
                sb.append(ce.getOrdSubSecNum());
                sb.append(FMT_PAREN_R);
                // if we have a sub sub sec number, it follows in parens ex: (1)
                if (containsPrintableContent(ce.getOrdSubSubSecNum())) {
                    hlog.append("Found non-null sub sub sec num<br />");
                    sb.append(FMT_PAREN_L);
                    sb.append(ce.getOrdSubSubSecNum());
                    sb.append(FMT_PAREN_R);
                }
            }
        } // done with section numbers, now for section titles which follow the (3)(a) tokens

        if (containsPrintableContent(ce.getOrdSecTitle())) {
            hlog.append("Found non-null sec title<br />");
            sb.append(FMT_SPACE);
            sb.append(ce.getOrdSecTitle());
        }
        //and sub section
        if (containsPrintableContent(ce.getOrdSubSecTitle())) {
            hlog.append("Found non-null sub sec title<br />");
            sb.append(FMT_SPACE);
            sb.append(FMT_DASH);
            sb.append(FMT_SPACE);
            sb.append(ce.getOrdSubSecTitle());
        }
        if (containsPrintableContent(ce.getOrdSubSubSecTitle())) {
            hlog.append("Found non-null sub sub sec title<br />");
            sb.append(FMT_SPACE);
            sb.append(FMT_DASH);
            sb.append(FMT_SPACE);
            sb.append(ce.getOrdSubSubSecTitle());
        }
        return sb.toString();
    }
    
    /**
     * Utility for checking for null, empty string, and single space
     * @param s can be null
     * @return true if the input is not null, not empty, and not a single space
     */
    private boolean containsPrintableContent(String s){
        return Objects.nonNull(s) && !s.equals(EMPTY_STRING) && !s.equals(FMT_SPACE);
    }
    
    /**
     * Logic method for checking if a code element has been entered 
     * into the system using standard IRC recursive listing
     * such that the chapter and section numbers are in the sub-section
     * number, e.g. Chapter 7, Section 703, subsection 703.1: We only 
     * want to use 703.1 in the display, since inclusion of the ch & sec
     * is redundant
     * @param ele to check
     * @return true if the ch + sec are in the sub-section, and if
     * sub-sub is not null, the sub-section is in the sub-sub section
     */
    public boolean checkForPureIRCRecursiveListing(CodeElement ce){
        
        boolean isPureIRCFormat = true;
        
        // nulls or zero for ch, sec, and subsec violates purity
        if(ce.getOrdchapterNo() == 0 
                || ce.getOrdSecNum() == null
                || ce.getOrdSubSecNum() == null){
            return false;
        }
        
        // no ch, sec, or sub-sec titles violates purity
        if(ce.getOrdchapterTitle() == null
                || ce.getOrdSecTitle() == null
                || ce.getOrdSubSecTitle() == null){
            return false;
        }
            
        // chapter No. should be in our section #
        if(!ce.getOrdSecNum().contains(String.valueOf(ce.getOrdchapterNo()))){
            return false;    
        }
        
        // sec number should be in our sub-section #
        if(!ce.getOrdSubSecNum().contains(ce.getOrdSecNum())){
            return false;
        }
        
        // if there is a sub-sub number, section should be in there, too
        if(ce.getOrdSubSubSecNum() != null && !ce.getOrdSubSecNum().equals("''")){
            if(!ce.getOrdSubSubSecNum().contains(ce.getOrdSubSecNum())){
                return false;
            }
        }
        ce.setPureIRCFOrmat(isPureIRCFormat);
        return isPureIRCFormat;
    }
    
    /**
     * Concatenates all ordinance citation members (ch, sec, sub sec no and title)
     * along with the ord's technical text for searching
     * @param ele
     * @return the complete string, including any HTML that's in the technical text
     */
    public String buildSearchableString(CodeElement ele){
        StringBuilder sb = new StringBuilder();
        sb.append(ele.getOrdchapterNo());
        sb.append(Constants.FMT_SPACE_LITERAL);
        sb.append(ele.getOrdchapterTitle());
        sb.append(Constants.FMT_SPACE_LITERAL);
        sb.append(ele.getOrdSecNum());
        sb.append(Constants.FMT_SPACE_LITERAL);
        sb.append(ele.getOrdSecTitle());
        sb.append(Constants.FMT_SPACE_LITERAL);
        sb.append(ele.getOrdSubSecNum());
        sb.append(Constants.FMT_SPACE_LITERAL);
        sb.append(ele.getOrdSubSecTitle());
        sb.append(Constants.FMT_SPACE_LITERAL);
        sb.append(ele.getOrdSubSubSecNum());
        sb.append(Constants.FMT_SPACE_LITERAL);
        sb.append(ele.getOrdSubSubSecTitle());
        sb.append(Constants.FMT_SPACE_LITERAL);
        sb.append(ele.getOrdTechnicalText());
        sb.append(Constants.FMT_SPACE_LITERAL);
        sb.append(ele.getOrdHumanFriendlyText());
        
        return sb.toString();
    }
    
    /**
     * Updates ONLY the code element's static header string in the DB
     * @param ele 
     * @param src 
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void updateCodeElementHeaderStringStatic(CodeElement ele, CodeSource src, UserAuthorized ua) throws IntegrationException, AuthorizationException{
        if(!permissionsCheckpointManageCodeSource(src, ua)){
            throw new AuthorizationException("User does not have permission to edit elements in this code source");
        }
        CodeIntegrator ci = getCodeIntegrator();
        ele.setLastUpdatedBy(ua);
        ci.updateCodeElementHeaderStringStatic(ele);
        getCodeCacheManager().flushCodeElementConnectedCaches();
    }
    
    /**
     * UPdates all the header strings of the given source
     * @param eleList     
     * @param src     
     * @param ua     
     * @throws com.tcvcog.tcvce.domain.IntegrationException     
     */
    public void updateCodeElementListHeaderString(List<CodeElement> eleList, CodeSource src, UserAuthorized ua) throws IntegrationException, AuthorizationException{
        if(eleList != null && !eleList.isEmpty()){
            for(CodeElement ele: eleList){
                updateCodeElementHeaderStringStatic(ele, src, ua);
            }
        }
    }
    
    
   
    
    /**
     * Extracts all CodeElements in a given Source
     * @param src
     * @return of CodeElements in that source
     * @throws IntegrationException 
     */
    public List<CodeElement> getCodeElemements(CodeSource src) throws IntegrationException{
        if(src ==  null){
            return new ArrayList<>();
        }
        CodeIntegrator ci = getCodeIntegrator();
        try {
            return ci.getCodeElements(src.getSourceID());
        } catch (BObStatusException ex) {
            throw new IntegrationException(ex.getMessage());
        }
    }
    
   /**
    * Logic intermediary for insertion of new CodeElements
    * 
    * @param ele to be inserted; must contain a valid parent codeSource
    * @param ua doing the insertion
    * @return the ID of the freshly inserted record
    * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
    */
    public int insertCodeElement(CodeElement ele, UserAuthorized ua) 
            throws IntegrationException, BObStatusException, AuthorizationException, BObStatusException{
        CodeIntegrator ci = getCodeIntegrator();
        
        int freshId = 0;
        if(ua == null || ele == null){
            throw new BObStatusException("cannot insert new code element with null input ele or user");  
        }
        if(!permissionsCheckpointManageCodeSource(ele.getSource(), ua)){
            throw new AuthorizationException("Permission denied: User lacks privileges to add code elements in a source"); 
        }
        if(ele.getOrdchapterTitle() == null){
            throw new BObStatusException("Ordinances must contain a non-null chapter title!");
        }
            
        ele.setCreatedBy(ua);
        ele.setLastUpdatedBy(ua);
        prepareCodeElementForInsertUpdateAndAuditFields(ele);
        freshId = ci.insertCodeElement(ele);
        return freshId;
    }
    
    
    /**
     * Logic intermediary for updates to a code element
     * 
     * @param ele with fields updated
     * @param ua doing the updating
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void updateCodeElement(CodeElement ele, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException{
        CodeIntegrator ci = getCodeIntegrator();
        if(ua == null || ele == null) {
            throw new BObStatusException("Cannot update code element with null element or user");
        }
        if(!permissionsCheckpointManageCodeSource(ele.getSource(), ua)){
            throw new AuthorizationException("Permission denied: User lacks privileges to update code elements in a source"); 
        }
        ele.setLastUpdatedBy(ua);
        prepareCodeElementForInsertUpdateAndAuditFields(ele);
        
        ci.updateCodeElement(ele);
        getCodeCacheManager().flushCodeElementConnectedCaches();
        
    }
    
   
    /**
     * Trims white space from all non-null ordinance components
     * @param ele 
     */
    private void prepareCodeElementForInsertUpdateAndAuditFields(CodeElement ele) throws BObStatusException{
        if(ele != null){
            // look for malformed sec, sub sec, and sub sub secs
            if(!isStringEmptyIsh(ele.getOrdSecTitle()) && isStringEmptyIsh(ele.getOrdSecNum())){
                throw new BObStatusException("Ordinances that have a section title must have a section number");
            }
            if(!isStringEmptyIsh(ele.getOrdSubSecTitle()) && isStringEmptyIsh(ele.getOrdSubSecNum())){
                throw new BObStatusException("Ordinances that have a sub section title must have a section number");
            }
            if(!isStringEmptyIsh(ele.getOrdSubSubSecTitle()) && isStringEmptyIsh(ele.getOrdSubSubSecNum())){
                throw new BObStatusException("Ordinances that have a sub sub section title must have a section number");
            }
            
            // chpter title only
            if(ele.getOrdchapterTitle() != null){
                ele.setOrdchapterTitle(ele.getOrdchapterTitle().trim());
            }
            // ord section
            if(ele.getOrdSecNum() != null){
                ele.setOrdSecNum(ele.getOrdSecNum().trim());
            }
            if(ele.getOrdSecTitle() != null){
                ele.setOrdSecTitle(ele.getOrdSecTitle().trim());
            }
            // ord sub section
            if(ele.getOrdSubSecNum() != null){
                ele.setOrdSubSecNum(ele.getOrdSubSecNum().trim());
            }
            if(ele.getOrdSubSecTitle() != null){
                ele.setOrdSubSecTitle(ele.getOrdSubSecTitle().trim());
            }
            // ord sub sub sec
            if(ele.getOrdSubSubSecNum() != null){
                ele.setOrdSubSubSecNum(ele.getOrdSubSubSecNum().trim());
            }
            if(ele.getOrdSubSubSecTitle() != null){
                ele.setOrdSubSubSecTitle(ele.getOrdSubSubSecTitle().trim());
            }
        }
    }
    
    
    /**
     * Updates only the ord's technical text
     * @param ele
     * @param ua
     * @throws IntegrationException
     * @throws BObStatusException
     * @throws AuthorizationException 
     */
    public void updateCodeElementTechnicalTextOnly(CodeElement ele, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException{
        CodeIntegrator ci = getCodeIntegrator();
        if(ua == null || ele == null) {
            throw new BObStatusException("Cannot update code element with null element or user");
        }
        if(!permissionsCheckpointManageCodeSource(ele.getSource(), ua)){
            throw new AuthorizationException("Permission denied: User lacks privileges to update code elements in a source"); 
        }
        ele.setLastUpdatedBy(ua);
        ci.updateCodeElementTechnicalTextOnly(ele);
        getCodeCacheManager().flushCodeElementConnectedCaches();
    }
    
    
  
    /**
     * Logic intermediary for deactivation of CodeElements
     * 
     * @param ele to deactivate
     * @param ua doing the deactivation
     * @throws IntegrationException 
     */
    public void deactivateCodeElement(CodeElement ele, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException{
        
        CodeIntegrator ci = getCodeIntegrator();
        if(ua == null || ele == null){
            throw new BObStatusException("Cannot deactivate code element with null input or user");
            
        }
        if(!permissionsCheckpointManageCodeSource(ele.getSource(), ua)){
            throw new AuthorizationException("Permission denied: User lacks privileges to deactivate code elements in a source"); 
        }
        ele.setDeactivatedBy(ua);
        
        ci.deactivateCodeElement(ele);
        getCodeCacheManager().flushCodeElementConnectedCaches();
    }
    
    // *************************************************************
    // **************CODE SETS (CODE BOOKS)*************************
    // *************************************************************
    
    /**
     * Retrieval portal for CodeSet objects
     * @param setID not zero
     * @return the code set object
     * @throws BObStatusException if setID is zero
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     */
    public CodeSet getCodeSet(int setID) throws BObStatusException, IntegrationException{
        if(setID == 0){

            throw new BObStatusException("0 is an invalid code set ID");
        }
        CodeIntegrator ci = getCodeIntegrator();
        return configureCodeSet(ci.getCodeSetBySetID(setID));
    }
    
    /**
     * Logic intermediary for code sets
     * @param codeSet
     * @return 
     */
    private CodeSet configureCodeSet(CodeSet codeSet) throws BObStatusException{
        if(codeSet != null){
            codeSet.setTreeView(buildTreeFromCodeSet(codeSet.getEnfCodeElementList()));
        }
        return codeSet;
    }
    
    /**
     * Factory method for CodeSet objects
     * @return the object, with no injected values
     */
    public CodeSet getCodeSetSkeleton(){
        
        return new CodeSet();
    }
    
    /**
     * Constructs a hierarchical data structure from the given codeset.
     * First attempt at some serious generic action. I need a list of ECEs or subclasses
     * @param <E>
     * @param eleList
     * @return 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     */
    public <E extends CodeElement> TreeNode<TreeNodeOrdinanceWrapper<E>> buildTreeFromCodeSet(List<E> eleList) throws BObStatusException{
        if(eleList == null || eleList.isEmpty()){
            throw new BObStatusException("Cannot build tree from null or empty code list");
        }
        
        TreeNode<TreeNodeOrdinanceWrapper<E>> root = new DefaultTreeNode<>(new TreeNodeOrdinanceWrapper(OrdinanceHierarchyEnum.ROOT, "Codes", null), null);
        // algo doesn't require any particular sort order, but we'll sort I guess??
        try{
            Collections.sort(eleList);
        } catch (IllegalArgumentException ex){
            System.out.println("CodeCoordinator.buildTreeFromCodeSet | sort error in compareTo codeElement");
            System.out.println(ex);
        }
        
        // setup our map for retrieval of all nodes
        Map<String, TreeNode<TreeNodeOrdinanceWrapper<E>>> nodeMap = new HashMap<>();
        
        /**
         * Master iterator over each ECE, and we'll build the tree as we go
         */
        for(E ele: eleList){
            
            // determine the generation of the current element
            OrdinanceHierarchyEnum generation = determineElementGeneration(ele);
            String displayString = buildDisplayString(ele, generation);
            
            // Build our ordinance node of the proper generation and inject its header string
            TreeNodeOrdinanceWrapper wrappedOrd = new TreeNodeOrdinanceWrapper(generation, displayString, ele);
            
            // All ece's are ordinance nodes, but each one's parent may or may not be an ordinance node
            TreeNode<TreeNodeOrdinanceWrapper<E>> parent = null;
            TreeNode<TreeNodeOrdinanceWrapper<E>> grandParent = root;
            
            // Build or locate down to this ECE's direct parent using the list of its ancestor generations
            // After this for loop, parent will have a ref to this ord's parent
            for(OrdinanceHierarchyEnum incrementalGeneration: buildAncestorList(generation) ){
                String genKey = buildDisplayString(ele, incrementalGeneration);
                // see if we have a this node in our tree; the displayable string will be the same and this is why its our key
                if(nodeMap.containsKey(genKey)){
                    parent = nodeMap.get(genKey);
                    // deal with out-of-order tree building by checking to see if our current ord
                    // actually belongs in the belly of a previously created branching node
                    if(parent.getData().getOrdinance() != null && parent.getData().getOrdinance().getElementID() == ele.getElementID()){
                        // This is me! I belong here!
                        parent.getData().setOrdinance(ele);
                        // and we're done! -- our node existed and now it has the ord in its belly
                        break;
                    }
                } else {
                    // make a branching node at this key; no ord in its belly--
                   TreeNodeOrdinanceWrapper freshlyWrapped = new TreeNodeOrdinanceWrapper(incrementalGeneration, genKey, null);
                   TreeNode branchNode = new DefaultTreeNode(freshlyWrapped, grandParent);
                   parent = branchNode;
                   nodeMap.put(genKey, branchNode);
                }
                // move down a generation
                grandParent = parent;
            } // end for loop over ancestry
            // so our parent should be all set up for building our ordinance node
            if(parent != null){
                TreeNode<TreeNodeOrdinanceWrapper<E>> freshNode = new DefaultTreeNode<>(wrappedOrd, parent);
                freshNode.setExpanded(true);
                nodeMap.put(displayString, freshNode);
            } else {
                System.out.println("buildTreeFromCodeSource | Parent Logic error");
            }
        }
        return root;
    }
    
    /**
     * THis is a duplicate method that is also on TreeNodeOrdinanceWrapper and for the life of me
     * I cannot figure out why my tree builder for loop wouldn't accept the return type of the
     * member method on the object itself, but the compiler likes it here in its own class??
     * @param hiearchyEnum
     * @return 
     */
     private List<OrdinanceHierarchyEnum> buildAncestorList(OrdinanceHierarchyEnum hiearchyEnum){
        List<OrdinanceHierarchyEnum> anList = new ArrayList<>();
        if(hiearchyEnum != null){
            switch (hiearchyEnum) {
                case CODE_SOURCE -> {
                    // source parent is the root
                }
                case CHAPTER -> {
                    anList.add(OrdinanceHierarchyEnum.CODE_SOURCE);
                }
                case SECTION -> {
                    anList.add(OrdinanceHierarchyEnum.CODE_SOURCE);
                    anList.add(OrdinanceHierarchyEnum.CHAPTER);
                }
                case SUB_SECTION -> {
                    anList.add(OrdinanceHierarchyEnum.CODE_SOURCE);
                    anList.add(OrdinanceHierarchyEnum.CHAPTER);
                    anList.add(OrdinanceHierarchyEnum.SECTION);
                }
                case SUB_SUB_SECTION -> {
                    anList.add(OrdinanceHierarchyEnum.CODE_SOURCE);
                    anList.add(OrdinanceHierarchyEnum.CHAPTER);
                    anList.add(OrdinanceHierarchyEnum.SECTION);
                    anList.add(OrdinanceHierarchyEnum.SUB_SECTION);
                }
                default -> {
                    // no enums to add
                }
            }
            // parent is the root
            // nothing to do
                    }
        
        return anList;
    }
    
    static final String CHAPTER_PREFIX = "Ch. ";
    static final String SECTION_PREFIX = "Sec. ";
    static final String SUBSECTION_PREFIX = "Sub sec. ";
    static final String SUBSUBSECTION_PREFIX = "Sub sub sec. ";
    static final String SPACE = " ";
    
    /**
     * Looks at this object's generation and builds a string and injects it
     * @param ele
     * @return 
     */
    private String buildDisplayString(CodeElement ele, OrdinanceHierarchyEnum gen){
        StringBuilder sb = new StringBuilder();
        if(ele == null || gen == null){
            return "Unknown Display String";
        }
        switch (gen){
            case CODE_SOURCE -> {
                if(ele.getSource() != null){
                    sb.append(ele.getSource().toString());
                }
            }
            case CHAPTER -> {
                sb.append(CHAPTER_PREFIX);
                sb.append(ele.getOrdchapterNo());
                sb.append(SPACE);
                sb.append(ele.getOrdchapterTitle());
            }
            case SECTION -> {
                sb.append(SECTION_PREFIX);
                sb.append(ele.getOrdSecNum());
                sb.append(SPACE);
                sb.append(ele.getOrdSecTitle());
            }
            case SUB_SECTION -> {
                sb.append(SUBSECTION_PREFIX);
                sb.append(ele.getOrdSubSecNum());
                sb.append(SPACE);
                sb.append(ele.getOrdSubSecTitle());
            }
            case SUB_SUB_SECTION -> {
                sb.append(SUBSUBSECTION_PREFIX);
                sb.append(ele.getOrdSubSubSecNum());
                sb.append(SPACE);
                sb.append(ele.getOrdSubSubSecTitle());
            }
            default -> {
            }
        } // close switch
        return sb.toString();
    }
    
    /**
     * Ascertains the generation of the given element.
     * TODO add logic to prevent malformed ords from making it into the table.
     * 
     * @param element
     * @return 
     */
    private OrdinanceHierarchyEnum determineElementGeneration(CodeElement element){
        if(element == null){
            return null;
        }
        if(!isStringEmptyIsh(element.getOrdSubSubSecNum())){
            return OrdinanceHierarchyEnum.SUB_SUB_SECTION;
        } else if(!isStringEmptyIsh(element.getOrdSubSecNum())){
            return OrdinanceHierarchyEnum.SUB_SECTION;
        } else if(!isStringEmptyIsh(element.getOrdSecTitle())){
            return OrdinanceHierarchyEnum.SECTION;
        } else if(element.getOrdchapterNo() != 0){
            return OrdinanceHierarchyEnum.CHAPTER;
        } else {
            return null;
        }
        
    }
    
    /**
     * Logic intermediary for insertion of new code sets
     * @param set with the variables set
     * @param ua
     * @return the PK of the new set
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     * @throws com.tcvcog.tcvce.domain.AuthorizationException
     */
    public int insertCodeSet(CodeSet set, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException{
        if(set == null || ua == null){
            throw new BObStatusException("Cannot insert code set with null input set or user");
        }
        if(!permissionsCheckpointManageCodeBook(set, ua)){
            throw new AuthorizationException("Permission Denied: insertCodeSet by permissionsCheckpointManageCodeBook");
        }
        CodeIntegrator ci = getCodeIntegrator();
        return ci.insertCodeSetMetadata(set);
    }
    
    /**
     * Logic intermediary for updates to CodeSet objects
     * @param set 
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void updateCodeSetMetadata(CodeSet set, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException{
        if(set == null || ua == null){
            throw new BObStatusException("Cannot update code set with null input set or user");
        }
        if(!permissionsCheckpointManageCodeBook(set, ua)){
            throw new AuthorizationException("Permission Denied: updateCodeSetMetadata by permissionsCheckpointManageCodeBook");
        }
        CodeIntegrator ci = getCodeIntegrator();
        ci.updateCodeSetMetadata(set);
        getCodeCacheManager().getCacheCodeSet().invalidate(set.getCacheKey());
        
    }
    
    /**
     * Maps a municipality to a given code set, making it the muni's active set
     * @param set
     * @param muni 
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void activateCodeSetAsMuniDefault(CodeSet set, Municipality muni, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        MunicipalityIntegrator mi = getMunicipalityIntegrator();
        if(set == null || muni == null || ua == null){
            throw new BObStatusException("Cannot activate code set in muni with null input set or muni or user");
        }
        if(!permissionsCheckpointManageCodeBook(set, ua)){
            throw new AuthorizationException("Permission Denied: activateCodeSetAsMuniDefault by permissionsCheckpointManageCodeBook");
        }
        mi.mapCodeSetAsMuniDefault(set, muni);
    }
    
    /**
     * Logic Intermediary for deactivating a code set; checks to make sure
     * this code set is not the default for any muni
     * @param set
     * @param ua
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void deactivateCodeSet(CodeSet set, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException{
        if(set == null || ua == null){
            throw new BObStatusException("Cannot activate code set in muni with null input set or muni or user");
        }
        if(!permissionsCheckpointDeacCodeSet(set, ua)){
            throw new AuthorizationException("Permission Denied: deactivateCodeSet by permissionsCheckpointManageCodeBook");
        }
        CodeIntegrator ci = getCodeIntegrator();
        HashMap<Municipality, CodeSet> muniSetMap = ci.getMuniDefaultCodeSetMap();
        if(muniSetMap.containsValue(set)){

            throw new BObStatusException("Cannot deactivate a code set which is a default in ANY muni");
        } else {
            ci.deactivateCodeSet(set);
        }
        getCodeCacheManager().getCacheCodeSet().invalidate(set.getCacheKey());
    }
    
    /**
     * Extracts a mapping of municipalities and their default code sets 
     * @return
     * @throws IntegrationException 
     */
    public Map<Municipality, CodeSet> getMuniCodeSetDefaultMap() throws IntegrationException{
        CodeIntegrator ci = getCodeIntegrator();
        
        try {
            return ci.getMuniDefaultCodeSetMap();
        } catch (BObStatusException ex) {
            throw new IntegrationException(ex.getMessage());
        }
    }
    
    /**
     * Extracts a complete list of code sets from DB for configuration
     * @return
     * @throws IntegrationException 
     */
    public List<CodeSet> getCodeSetListComplete() throws IntegrationException{
        CodeIntegrator ci = getCodeIntegrator();
        List<CodeSet> setList;
        try {
            setList = ci.getCodeSets();
        } catch (BObStatusException ex) {
            throw new IntegrationException(ex.getMessage());
        }
        return setList;
    }
    
    /**
     * Extracts CodeSets by muni
     * @param muniCode
     * @return 
     */
    public List<CodeSet> getCodeSetsFromMuniID(int muniCode) {
        
        CodeIntegrator integrator = getCodeIntegrator();
        
        try {
            return integrator.getCodeSets(muniCode);
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex.toString());
        }
        
        return new ArrayList<>();
    }
    
    /**
     * Factory method for ECE skeletons
     * @param ele will be injected into ECE constructor if not null
     * @return the skeleton, possibly with injected Element, if passed in
     * and default values hard-coded into coordinator injected
     */
    public EnforceableCodeElement getEnforcableCodeElementSkeleton(CodeElement ele) throws IntegrationException{
        SystemCoordinator sc = getSystemCoordinator();
        
        EnforceableCodeElement ece = null;
        if(ele != null){
            ece = new EnforceableCodeElement(ele);
        } else {
            ece = new EnforceableCodeElement();
        }
        
        ece.setMaxPenalty(ECE_DEFAULT_PENALTY_MAX);
        ece.setNormPenalty(ECE_DEFAULT_PENALTY_NORM);
        ece.setMinPenalty(ECE_DEFAULT_PENALTY_MIN);
        
        ece.setNormDaysToComply(ECE_DEFAULT_DAYS_TO_COMPLY);
        ece.setDaysToComplyNotes(ECE_DEFAULT_DAYSTOCOMPLY_NOTES);
        
        IntensitySchema is = sc.getIntensitySchemaWithClasses(VIOLATION_SEVERITY_SCHEMA, getSessionBean().getSessMuni());
        if(is != null && is.getClassList() != null && !is.getClassList().isEmpty()){
            //grab the end of the list which ecd thinks is the lower severity
            ece.setDefaultViolationSeverity(is.getClassList().get(is.getClassList().size()-1));
        }
        
        return ece;
    }
    
 
    /**
     * Logic pass through for insertion of an ECE
     * 
     * @param ece to insert
     * @param cs to which the ECE should be linked
     * @param ua doing the insertion
     * @param dece containing default values for insertion
     * @return ID of the fresh object in DB
     * @throws BObStatusException
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public int insertEnforcableCodeElement(EnforceableCodeElement ece, 
            CodeSet cs, 
            UserAuthorized ua,
            EnforceableCodeElement dece) 
            throws BObStatusException, IntegrationException, AuthorizationException{
        CodeIntegrator ci = getCodeIntegrator();
        if(ece == null || ua == null || cs == null){
            throw new BObStatusException("Cannot insert ECE with null ECE or User");
        }
        if(!permissionsCheckpointManageCodeBook(cs, ua)){
            throw new AuthorizationException("Permission Denied: insertEnforcableCodeElement by permissionsCheckpointManageCodeBook");
        }
        
        ece.setCodeSetID(cs.getCodeSetID());
        //Take skeleton enforceable values and inject them into defaults before adding to database
        ece.setMaxPenalty(dece.getMaxPenalty());
        ece.setMinPenalty(dece.getMinPenalty());
        ece.setNormPenalty(dece.getNormPenalty());
        ece.setPenaltyNotes(dece.getPenaltyNotes());
        ece.setNormDaysToComply(dece.getNormDaysToComply());
        ece.setDaysToComplyNotes(dece.getDaysToComplyNotes());
        ece.setMuniSpecificNotes(dece.getMuniSpecificNotes());
        ece.setDefaultViolationSeverity(dece.getDefaultViolationSeverity());
        ece.setFeeList(dece.getFeeList());
        ece.setDefaultViolationDescription(dece.getDefaultViolationDescription());
        
        ece.setEceCreatedByUserID(ua.getUserID());
        ece.setEceLastUpdatedByUserID(ua.getUserID());
        int freshID = ci.insertEnforcableCodeElementToCodeSet(ece);
        
        getCodeCacheManager().getCacheCodeSet().invalidate(cs.getCacheKey());
        
        return freshID;
    }
    
    /**
     * Logic pass through for updates to an ECE
     * 
     * @param ece with updated fields populated
     * @param ua doing the updating
     * @throws BObStatusException
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void updateEnforcableCodeElement(EnforceableCodeElement ece, UserAuthorized ua) 
            throws BObStatusException, IntegrationException, AuthorizationException{
        CodeIntegrator ci = getCodeIntegrator();
        if(ece == null || ua == null){
            throw new BObStatusException("Cannot update ECE with null ECE or UA");
        }
        if(!permissionsCheckpointManageCodeBook(getCodeSet(ece.getCodeSetID()), ua)){
            throw new AuthorizationException("Permission Denied: updateEnforcableCodeElement by permissionsCheckpointManageCodeBook");
        }
        
        ece.setEceLastUpdatedByUserID(ua.getUserID());
        ece.setLastUpdatedBy(ua);
        ci.updateEnforcableCodeElement(ece);
        getCodeCacheManager().getCacheEnforcableCodeElement().invalidate(ece.getCacheKey());
        getCodeCacheManager().flushCodeElementConnectedCaches();
        
    }
    
    
      /**
     * Internal operational code to determine which muni's the user has
     * enforcement official permissions (or better), and update their codebooks
     * that contain this same ordinance with default findings
     *
     * @param defFindings the new string to become the default findings
     * @param ece into which I'll inject the new text before updating
     * @param ua
     * @throws com.tcvcog.tcvce.domain.IntegrationException
     * @throws com.tcvcog.tcvce.domain.BObStatusException
     */
    public void updateEnfCodeElementMakeFindingsDefault(String defFindings, EnforceableCodeElement ece, UserAuthorized ua) throws IntegrationException, BObStatusException, AuthorizationException {
        if(defFindings == null || ece == null || ua == null){
            throw new BObStatusException("Cannot update findings with null inputs");
        }
        System.out.println("CodeCoordinator.updateEnfCodeElementMakeFindingsDefault | updating ECE ID: " + ece.getCodeSetElementID() + " findings to " + defFindings);
        if(!permissionsCheckpointManageCodeBook(getCodeSet(ece.getCodeSetID()), ua)){
            throw new AuthorizationException("Permission Denied: updateEnfCodeElementMakeFindingsDefault by permissionsCheckpointManageCodeBook");
        }
        CodeIntegrator ci = getCodeIntegrator();
        SystemCoordinator sc = getSystemCoordinator();
        ece.setDefaultViolationDescription(defFindings);
        MessageBuilderParams mbp = new MessageBuilderParams();
        mbp.setUser(ua);
        mbp.setExistingContent(ece.getNotes());

        StringBuilder sb = new StringBuilder();
        sb.append("Default findings changed to: ");
        sb.append(defFindings);
        sb.append(" from: ");
        sb.append(ece.getDefaultViolationDescription());
        mbp.setNewMessageContent(sb.toString());

        ece.setMuniSpecificNotes(sc.appendNoteBlock(mbp));
        ece.setDefaultViolationDescription(defFindings);
        ece.setLastUpdatedBy(ua);
        ci.updateEnforcableCodeElement(ece);
        getCodeCacheManager().getCacheEnforcableCodeElement().invalidate(ece.getCacheKey());
        getCodeCacheManager().flushCodeElementConnectedCaches();
    }
    
    /**
     * Logic pass through for deactivation of ECEs
     * 
     * @param ece to deactivate
     * @param ua doing the deactivation
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public void deactivateEnforcableCodeElement(EnforceableCodeElement ece, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        CodeIntegrator ci = getCodeIntegrator();
        if(ece == null || ua == null){
            throw new BObStatusException("Cannot deactivate an ECE with null ECE or UA");
        }
        if(!permissionsCheckpointManageCodeBook(getCodeSet(ece.getCodeSetID()), ua)){
            throw new AuthorizationException("Permission Denied: deactivateEnforcableCodeElement by permissionsCheckpointManageCodeBook");
        }
        
        ece.setEceDeactivatedByUserID(ua.getUserID());
        System.out.println("CodeCoordinator.deacECE");
        ci.deactivateEnforcableCodeElement(ece);
        getCodeCacheManager().flushCodeElementConnectedCaches();
    }
    
    
    /**
     * Logic passthrough for retrieving ECEs
     * @param eceID
     * @return
     * @throws IntegrationException 
     */
    public EnforceableCodeElement getEnforcableCodeElement(int eceID) throws IntegrationException, BObStatusException{
        if(eceID == 0){
            throw new BObStatusException("Cannot retrieve ECE with an ID of zero");
        }
        
        CodeIntegrator ci = getCodeIntegrator();
        EnforceableCodeElement ece = null;
        try {
            ece = ci.getEnforcableCodeElement(eceID);
            if(ece != null){
                ece.setCannedFindingsList(ci.getDefaultFindingsByEnforceableCodeElement(ece));
            }
        } catch (DatabaseFetchRuntimeException ex) {
            throw new IntegrationException(ex.getMessage());
        }
        
        return ece;
    }
    
    /**
     * Extracts ECEs in a code set (code book)
     * @param setID
     * @return 
     */
    public List<EnforceableCodeElement> getCodeElementsFromCodeSetID(int setID){
        
        CodeIntegrator integrator = getCodeIntegrator();
        
        try {
            return integrator.getEnforcableCodeElementList(setID);
        } catch (IntegrationException | BObStatusException ex) {
            System.out.println(ex.toString());
        }
        return new ArrayList<>();
    }
    
    // *************************************************************************
    // ****************  Canned / default ordinance findings  ******************
    // *************************************************************************
    
    /**
     * Assembles a list of text blocks, perhaps with some canned remarks in it
     * @param holder
     * @return 
     */
    public List<TextBlock> assembleCannedFindingsByFindingsHolder(IFaceFindingsHolder holder) throws IntegrationException, BObStatusException{
        CodeIntegrator ci = getCodeIntegrator();
        if(holder != null){
            return ci.getDefaultFindingsByEnforceableCodeElement(holder.getEnforceableCodeElement());
        }
        return new ArrayList<>();
    } 
    
    /**
     * Determines the type of the given holder and gets a fresh copy
     * @param holder
     * @return 
     */
    public IFaceFindingsHolder refreshFindingsHolder(IFaceFindingsHolder holder) throws BObStatusException, IntegrationException{
        if(holder == null){
            throw new BObStatusException("Cannot refresh null holder"); 
        }
        IFaceFindingsHolder freshHolder = null;
        if(holder instanceof OccInspectedSpaceElement oise){
            OccInspectionCoordinator oic = getOccInspectionCoordinator();
            freshHolder = oic.getOccInspectedSpaceElement(oise.getInspectedSpaceElementID());
            getOccInspectionCacheManager().flushObjectFromCache(oise.getInspectedSpaceID());
        } else if (holder instanceof CodeViolation cv){
            CaseCoordinator cc = getCaseCoordinator();
            freshHolder = cc.violation_getCodeViolation(cv.getViolationID());
        }
        
        return freshHolder;
    }
    
    
    /**
     * Factory for TextBlocks that will be canned  violation findings
     * @return 
     */
    public TextBlock getCannedFindingTextBlockSkeleton() throws IntegrationException{
        SystemCoordinator sc = getSystemCoordinator();
        TextBlock block = new TextBlock();
        block.setCategory(sc.getTextBlockCategory(Integer.parseInt(getResourceBundle(Constants.DB_FIXED_VALUE_BUNDLE).getString("textblock_cat_cannedviolationfinding"))));
        return block;
    }
    
    
    /**
     * connects an ece with a text block as a canned finding
     * @param ece
     * @param block 
     * @param ua  the id of the freshly inserted text block
     */
    public int linkEnforceableCodeElementToTextBlock(EnforceableCodeElement ece, TextBlock block, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        if(ece == null || block == null){
            throw new BObStatusException("Cannot link null ece or null block together!");
        }
        SystemCoordinator sc = getSystemCoordinator();
        CodeIntegrator ci = getCodeIntegrator();
        if(!permissionsCheckpointManageCannedFindings(ua)){
            throw new AuthorizationException("User lacks authorization to manage canned findings");
        }
        
        int freshBlock = 0;
        if(block.getBlockID() == 0){
            freshBlock = sc.insertTextBlock(block, ua);
            block = sc.getTextBlock(freshBlock);
        }
        ci.linkEnforceableCodeElementToTextBlock(ece, block);
        System.out.println("CodeCoordinator.linkEnforceableCodeElementToTextBlock | linked ece ID " + ece.getCodeSetElementID() + " to block ID " + block.getBlockID());
        getCodeCacheManager().flush(ece);
        return freshBlock;
    }
    
    /**
     * Removes a link between and ECE and a TextBlock -- a canned finding
     * @param ece
     * @param block
     * @param ua
     * @throws BObStatusException
     * @throws AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void deleteEnforcableCodeElementToTextBlockLink(EnforceableCodeElement ece, TextBlock block, UserAuthorized ua) throws BObStatusException, AuthorizationException, IntegrationException{
        if(ece == null || block == null || ua == null){
            throw new BObStatusException("Cannot delete ece-block links with null inputs");
        }
        if(!permissionsCheckpointManageCannedFindings(ua)){
            throw new AuthorizationException("User lacks authorization to manage canned findings");
        }
        CodeIntegrator ci = getCodeIntegrator();
        
        ci.deleteEnforceableCodeElementLinkToTextBlock(ece, block);
        System.out.println("CodeCoordinator.deleteEnforcableCodeElementToTextBlockLink | linked ece ID " + ece.getCodeSetElementID() + " | block ID " + block.getBlockID());
        getCodeCacheManager().flush(ece);
    }
    
    
    /**
     * Updates a text block functioning as a canned finding
     * @param ece
     * @param block
     * @param ua
     * @throws BObStatusException
     * @throws AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void updateEnforcableCodeElementTextBlock(EnforceableCodeElement ece, TextBlock block, UserAuthorized ua) throws BObStatusException, AuthorizationException, IntegrationException{
        if(ece == null || block == null || ua == null){
            throw new BObStatusException("Cannot update ece-block links with null inputs");
        }
        if(!permissionsCheckpointManageCannedFindings(ua)){
            throw new AuthorizationException("User lacks authorization to manage canned findings");
        }
        SystemCoordinator sc = getSystemCoordinator();
        sc.updateTextBlock(block, ua);
        System.out.println("CodeCoordinator.updateEnforcableCodeElementTextBlock | linked ece ID " + ece.getCodeSetElementID() + " | block ID " + block.getBlockID());
        getCodeCacheManager().flush(ece);
    }
    
    
    // *************************************************************
    // ************************ CODE GUIDE *************************
    // *************************************************************
    
    /**
     * Factory method for guide entry skeletons (i.e. ID = 0)
     * @return 
     */
    public CodeElementGuideEntry getCodeElementGuideEntrySkeleton(){
        return new CodeElementGuideEntry();
    }
    
    /**
     * Logic intermediary for CodeElementGuideEntry object inserts
     * @param cege
     * @param ua
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public int insertCodeElementGuideEntry(CodeElementGuideEntry cege, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        CodeIntegrator ci = getCodeIntegrator();
        PermissionsCoordinator permCoor = getPermissionsCoordinator();
        
        if(cege == null || ua == null){
            throw new BObStatusException("Cannot insert null guide entry");
        }
        if(!permCoor.verifyGeneralEditFloor(ua)){
            throw new AuthorizationException("Permission Denied: insertCodeElementGuideEntry by verifyGeneralEditFloor");
        }
        cege.setCreatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
        cege.setLastUpdatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
        return ci.insertCodeElementGuideEntry(cege);
    }
    
    /**
     * Logic intermediary for CodeElementGuideEntry objects
     * @param cegeid
     * @return
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public CodeElementGuideEntry getCodeElementGuideEntry(int cegeid) throws BObStatusException, IntegrationException{
        CodeIntegrator ci = getCodeIntegrator();
        if(cegeid == 0){
            throw new BObStatusException("Cannot get entry with id Zero");
        }
        return ci.getCodeElementGuideEntry(cegeid);
    }
    
    /**
     * Extracts all code guide entries from the DB
     * @return
     * @throws IntegrationException 
     */
    public List<CodeElementGuideEntry> getCodeElementGuideEntryListComplete() throws IntegrationException, BObStatusException{
        CodeIntegrator ci = getCodeIntegrator();
        List<CodeElementGuideEntry> guideList = ci.getCodeElementGuideEntries();
        java.util.Collections.sort(guideList);
        return guideList;
    }
    
    /**
     * Logic intermediary for updates to CodeGuideEntry objects
     * @param cege
     * @param ua
     * @throws BObStatusException
     * @throws IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void updateCodeElementGuideEntry(CodeElementGuideEntry cege, UserAuthorized ua) 
            throws BObStatusException, IntegrationException, AuthorizationException{
        CodeIntegrator ci = getCodeIntegrator();
        PermissionsCoordinator permCoor = getPermissionsCoordinator();
        if(cege == null || ua == null){
            throw new BObStatusException("Cannot update entry with null input");
        }
        if(!permCoor.verifyGeneralEditFloor(ua)){
            throw new AuthorizationException("Permission Denied: updateCodeElementGuideEntry by verifyGeneralEditFloor");
        }
        cege.setLastUpdatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
        ci.updateCodeElementGuideEntry(cege);
        getCodeCacheManager().getCacheCodeElementGuideEntry().invalidate(cege.getCacheKey());
        getCodeCacheManager().flushCodeElementConnectedCaches();
    }
    
    /**
     * Convenience method for linking a batch of CodeElements to a single guide entry.I iterate and call linkCodeElementToGuideEntry
     * @param cel
     * @param cege 
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     */
    public void linkCodeElementListToCodeGuideEntry(List<CodeElement> cel, CodeElementGuideEntry cege, UserAuthorized ua) 
            throws BObStatusException, IntegrationException, AuthorizationException{
        if(cel == null || cel.isEmpty() || cege == null || ua == null){
            throw new BObStatusException("Cannot link code elements to guide with null or empty list or null guide entry");
        }
        PermissionsCoordinator permCoor = getPermissionsCoordinator();
        SystemIntegrator si = getSystemIntegrator();
        if(!permCoor.verifyGeneralEditFloor(ua)){
            throw new AuthorizationException("Permission Denied: linkCodeElementListToCodeGuideEntry by verifyGeneralEditFloor");
        }
        for(CodeElement ce: cel){
            ce.setGuideEntry(cege);
            linkCodeElementToGuideEntry(ce, ua);
            cege.setLastUpdatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
            si.updateStampUMAPTrackedEntity(cege);
            
        }
    }
    
    /**
     * Iterates over teh list of CodeElements; if they have a non-null
     * code guide entry, write that to the DB.If null, remove any link
     * @param eleList 
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void updateAllCodeGuideEntryLinks(List<CodeElement> eleList, UserAuthorized ua) throws BObStatusException, IntegrationException, AuthorizationException{
        if(eleList == null || eleList.isEmpty() || ua == null){
            throw new BObStatusException("Cannot update code guide entries with null or empty element list");
        }
        PermissionsCoordinator permCoor = getPermissionsCoordinator();
        SystemIntegrator si = getSystemIntegrator();
        if(!permCoor.verifyGeneralEditFloor(ua)){
            throw new AuthorizationException("Permission Denied: updateAllCodeGuideEntryLinks by verifyGeneralEditFloor");
        }
        for(CodeElement ele: eleList){
            if(ele.getGuideEntry() != null){
                linkCodeElementToGuideEntry(ele, ua);
                // redundant update
//                CodeElementGuideEntry cege = ele.getGuideEntry();
//                cege.setLastUpdatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
//                si.updateStampUMAPTrackedEntity(cege);
            }
           
        }
        getCodeCacheManager().flushCodeElementConnectedCaches();
    }
    
    
    /**
     * LInks or unlinks CodeElement to guide entry so the code element must have the guide entry in its belly
     * @param ce
     * @param ua
     * @throws BObStatusException
     * @throws IntegrationException 
     */
    public void linkCodeElementToGuideEntry(CodeElement ce, UserAuthorized ua) 
            throws BObStatusException, IntegrationException, AuthorizationException{
        CodeIntegrator ci = getCodeIntegrator();
        if(ce == null || ua == null || ce.getGuideEntry() == null){
            throw new BObStatusException("Cannot link code element to guide with null element or user");
        }
        PermissionsCoordinator permCoor = getPermissionsCoordinator();
        SystemIntegrator si = getSystemIntegrator();
        if(!permCoor.verifyGeneralEditFloor(ua)){
            throw new AuthorizationException("Permission Denied: linkCodeElementToGuideEntry by verifyGeneralEditFloor"); 
        }
        ce.setLastUpdatedBy(ua);
        ci.linkElementToCodeGuideEntry(ce);
        if(ce.getGuideEntry() != null){
            CodeElementGuideEntry cege = ce.getGuideEntry();
            cege.setLastUpdatedby_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
            si.updateStampUMAPTrackedEntity(cege);
        }
        getCodeCacheManager().flushCodeElementConnectedCaches();
    }
    
    /**
     * Logic container for removing a code guide entry
     * @param cege 
     * @param ua 
     * @throws com.tcvcog.tcvce.domain.BObStatusException 
     * @throws com.tcvcog.tcvce.domain.AuthorizationException 
     * @throws com.tcvcog.tcvce.domain.IntegrationException 
     */
    public void removeCodeElementGuideEntry(CodeElementGuideEntry cege, UserAuthorized ua) 
            throws BObStatusException, IntegrationException, AuthorizationException{
        if(cege == null){
            throw new BObStatusException("Cannot remove a null guide entry");
        }
        PermissionsCoordinator permCoor = getPermissionsCoordinator();
        SystemIntegrator si = getSystemIntegrator();
        if(!permCoor.verifyGeneralEditFloor(ua)){
            throw new AuthorizationException("Permission Denied: removeCodeElementGuideEntry by verifyGeneralEditFloor"); 
        }
        cege.setDeactivatedBy_UMAP(ua.getKeyCard().getGoverningAuthPeriod());
        si.deactivateUMAPTrackedEntity(cege);
        getCodeCacheManager().flushCodeElementConnectedCaches();
        
    }
    
    /**
     * Filters the codeElementList as per filter text filterText is matched
     * against ordinance header string and/or non IRC style header string
     *
     * @param codeElementList
     * @param filterText
     * @return
     */
    public List<CodeElement> codeElementAction_configureCodeElementListFiltered(
            List<CodeElement> codeElementList,
            String filterText) {
        if (Objects.nonNull(codeElementList))
        {
            if (Objects.nonNull(filterText) && !filterText.isEmpty())
            {
                return codeElementList.stream()
                        .filter(element -> codeElementAction_isOrdinanceFilterMatches(filterText, element))
                        .collect(Collectors.toList());
            } else
            {
                return codeElementList;
            }
        }
        return new ArrayList<>();
    }

    /**
     * To check ordinance filter text matches with header string or non IRC
     * Style Header String
     *
     * @param filterText
     * @param codeElement
     * @return
     */
    private boolean codeElementAction_isOrdinanceFilterMatches(String filterText, CodeElement codeElement) {
        StringBuilder sb = new StringBuilder();
        if (Objects.nonNull(codeElement.getHeaderString()))
        {
            sb.append(codeElement.getHeaderString());
        }
        if (Objects.nonNull(codeElement.getNonIrcStyleHeaderString()))
        {
            sb.append(codeElement.getNonIrcStyleHeaderString());
        }
        return StringUtil.isStringContainsI(sb.toString(), filterText);
    }
    
    /**
     * Filters the codeElementList as per filter text filterText is matched
     * against ordinance header string and/or non IRC style header string
     *
     * @param codeElementList
     * @param filterText
     * @return
     */
    public List<EnforceableCodeElement> codeSetAction_configureEnforcableCodeElementListFiltered(
            List<EnforceableCodeElement> codeElementList,
            String filterText) {
        if (Objects.nonNull(codeElementList))
        {
            if (Objects.nonNull(filterText) && !filterText.isEmpty())
            {
                return codeElementList.stream()
                        .filter(element -> codeSetAction_isOrdinanceFilterMatches(filterText, element))
                        .collect(Collectors.toList());
            } else
            {
                return codeElementList;
            }
        }
        return new ArrayList<>();
    }

    /**
     * To check ordinance filter text matches with header string or non IRC
     * Style Header String
     *
     * @param filterText
     * @param codeElement
     * @return
     */
    private boolean codeSetAction_isOrdinanceFilterMatches(String filterText, EnforceableCodeElement codeElement) {
        StringBuilder sb = new StringBuilder();
        if (Objects.nonNull(codeElement.getHeaderString()))
        {
            sb.append(codeElement.getHeaderString());
        }
        if (Objects.nonNull(codeElement.getNonIrcStyleHeaderString()))
        {
            sb.append(codeElement.getNonIrcStyleHeaderString());
        }
        return StringUtil.isStringContainsI(sb.toString(), filterText);
    }
}
