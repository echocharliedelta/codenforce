/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.system;

import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;
import jakarta.enterprise.inject.spi.CDI;
import org.quartz.Job;
import org.quartz.SchedulerException;
import org.quartz.simpl.SimpleJobFactory;
import org.quartz.spi.JobFactory;
import org.quartz.spi.TriggerFiredBundle;

/**
 * Created with guidance from ChatGPT version 40
 * @author pierre15
 */
public class CDIJobFactory implements JobFactory{
    
    private SeContainer cdiContainer;

    public CDIJobFactory() {
        try {
            cdiContainer = SeContainerInitializer.newInstance().initialize();
        } catch (Exception e) {
            // Handle initialization exception if necessary
        }
    }
    @Override
    public Job newJob(TriggerFiredBundle tfb, org.quartz.Scheduler schdlr) throws SchedulerException {
        System.out.println("CDIJobFactory.newJob");
         try {
            if (CDI.current() == null && cdiContainer != null) {
                cdiContainer.select(tfb.getJobDetail().getJobClass()).get();
            }
            return CDI.current().select(tfb.getJobDetail().getJobClass()).get();
        } catch (IllegalStateException e) {
            throw new SchedulerException("Unable to access CDI", e);
        }
    }
}
