/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.system;

import com.tcvcog.tcvce.session.SessionConductor;
import jakarta.faces.context.FacesContext;
import jakarta.faces.event.PhaseEvent;
import jakarta.faces.event.PhaseId;
import jakarta.faces.event.PhaseListener;
import java.util.Map;

/**
 * Utility Class for clearing view scoped beans during session reinit
 * @author Ellen Bascomb of Apartment 31Y
 */
public class ViewScopeFlushingPhaseListener implements PhaseListener{

    @Override
    public void afterPhase(PhaseEvent pe) {
        // nothing toDO
    }

    @Override
    public void beforePhase(PhaseEvent pe) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        if(facesContext != null){
            SessionConductor sessCond = facesContext.getApplication().evaluateExpressionGet(facesContext, "#{sessionConductor}", SessionConductor.class);
            if(sessCond != null && sessCond.isFlushViewScopedBeans()){
                System.out.println("ViewScopeFlushingPhaseListener.beforePhase | flushing all view scoped beans");
                Map<String, Object> viewMap = facesContext.getViewRoot().getViewMap();
                // Iterate over the view map keys and remove each entry
                for (String key : viewMap.keySet()) {
                    viewMap.remove(key);
                }
                sessCond.resetFlushViewScopedBean();
            }
        }
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RENDER_RESPONSE;
    }
}
