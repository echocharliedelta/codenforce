/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.system;

import com.tcvcog.tcvce.application.BackingBeanUtils;
import com.tcvcog.tcvce.coordinators.SystemCoordinator;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;

import static org.quartz.CronScheduleBuilder.dailyAtHourAndMinute;
import org.quartz.*;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

/**
 * Sets up scheduled jobs
 * @author pierre15
 */
@WebListener
public class QuartzJobInitializerListener extends BackingBeanUtils implements ServletContextListener{
 
    private Scheduler scheduler;
    
    static final int CACHE_DUMP_HOUROFDAY = 00;
    static final int CACHE_DUMP_MINOFDAY = 20;
    
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            scheduler = StdSchedulerFactory.getDefaultScheduler();
            scheduler.setJobFactory(new CDIJobFactory());
            
            JobDetail dumpCECaseCache = JobBuilder.newJob(CacheFlushJob.class)
                    .withIdentity("cacheFlushJob", "group1")
                    .build();

            Trigger trigger = TriggerBuilder.newTrigger()
                    .withIdentity("cacheFlushTrigger", "group1")
                    .withSchedule(dailyAtHourAndMinute(CACHE_DUMP_HOUROFDAY, CACHE_DUMP_MINOFDAY))
                    .forJob("cacheFlushJob", "group1")
                    .build();

//             CDI not working yet
//            
            scheduler.scheduleJob(dumpCECaseCache, trigger);
            scheduler.start();
            System.out.println("QuartzJobInitializerListener.contextInitialized | Flush Job Scheduled");
        } catch (SchedulerException e) {
            System.out.println(e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        try {
            if (scheduler != null) {
                scheduler.shutdown();
            }
        } catch (SchedulerException e) {
            System.out.println(e);
        }
    }
    
}
