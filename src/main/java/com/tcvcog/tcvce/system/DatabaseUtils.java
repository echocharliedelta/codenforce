/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.system;

import com.tcvcog.tcvce.util.Constants;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.security.RolesAllowed;
import jakarta.ejb.Stateless;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Core stateless bean for DB Connection generation
 * Code help from ChatGPT in July 2024
 * @author pierre15
 */
//@Stateless
public class DatabaseUtils {
     private DataSource dataSource;

    @PostConstruct
    public void init() {
        try {
            Context initContext = new InitialContext();
            Context envCtx = (Context) initContext.lookup("java:jboss/");
            String jndiName = getResourceBundle(Constants.DB_CONNECTION_PARAMS).getString("jndi_name");
            dataSource = (DataSource) envCtx.lookup(jndiName);
        } catch (NamingException ex) {
            System.err.println("DatabaseConnectionFactory.init | ERROR during JNDI lookup!");
            System.out.println(ex);
        }
    }

    public Connection getConnection()  {
        Connection con =  null;
        System.out.println("DatabaseUtils.getConnection");
        try {
            con = dataSource.getConnection();
        } catch (SQLException ex){
            System.out.println("DatabaseUtils.getConnection | FATAL SQL ERROR");
            System.out.println(ex);
        }
        return con;
    }

    private ResourceBundle getResourceBundle(String bundleName) {
        return ResourceBundle.getBundle(bundleName);
    }
}
