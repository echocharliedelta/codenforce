/*
 * Copyright (C) 2025 echocdelta
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tcvcog.tcvce.rest;

import com.tcvcog.tcvce.integration.OccInspectionCacheManager;
import jakarta.inject.Inject;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * Listens for requests from Spring to dump inspection cache records here in 
 * Wildfly land
 * @author echocdelta
 */
@Path("/cache")
public class DumpCacheInspectionService {
    
    @Inject
    private OccInspectionCacheManager occInspectionCacheManager;
    
    
    @POST
    @Path("/invalidatefin")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response invalidateFINCache(InspectionNotification payload){
        System.out.println("DumpCacheInspectionService.invalidateFINCache | payload : " + payload);
        
        if(occInspectionCacheManager != null){
            if(payload.getInspectionID() != 0){
                System.out.println("DumpCacheInspectionService.invalidateFINCache | CDI worked! Huzzah!");
                occInspectionCacheManager.flushKey(payload.getInspectionID());
            } else {
                System.out.println("DumpCacheInspectionService.invalidateFINCache | shall not dump FIN with ID == 0!");
            }
        } else {
            System.out.println("DumpCacheInspectionService.invalidateFINCache | CDI failed");
        }
        return Response.ok("{\"status\":\"invalidated\"}").build();
    }
}
