/*
 * Copyright (C) 2024 pierre15
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package playground;

import com.tcvcog.tcvce.domain.BObStatusException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.text.similarity.LevenshteinDistance;

/**
 *
 * @author pierre15
 */
public class DistanceTest {
    
    public static void main(String[] args) {
        String s1 = "asdfasdfy";
        String s2 = "asdfasDFfs  1";
//            System.out.println(computeLevenshteinDistance(s2, s1));
        
    }
    
      public static int computeLevenshteinDistance(String text1, String text2) throws BObStatusException {
        if(text1 == null || text1 == null){
            throw new BObStatusException("Cannot compute distance between 1 or more empty strings");
        }
        String cleanString1 = text1.strip();
        String cleanString2 = text2.strip();
        int levenDist = LevenshteinDistance.getDefaultInstance().apply(cleanString1, cleanString2);
        return levenDist;
        
    }
    
}
